import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConfigService {
    config: Config;

    constructor(private http: HttpClient) { }

    loadConfig() {
        return this.http
            .get<Config>('./assets/config.json')
            .toPromise()
            .then(config => {
                this.config = config;
                console.log(this.config);
            });
    }
}


export class Config {
    socket:string;
    baseUrl:string;
    javaReportApiServiceUrl:string;
    javaApiServiceUrl:string;
    db:string;
    dmsBaseUrl:string;
    activateDmsSessionAlternativeMethod:boolean;
    setDmsSessionTime:number;
}