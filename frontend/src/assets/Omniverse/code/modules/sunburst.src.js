
'use strict';
(function (factory) {
    if (typeof module === 'object' && module.exports) {
        factory['default'] = factory;
        module.exports = factory;
    } else if (typeof define === 'function' && define.amd) {
        define('Omniverse/modules/sunburst', ['Omniverse'], function (Omniverse) {
            factory(Omniverse);
            factory.Omniverse = Omniverse;
            return factory;
        });
    } else {
        factory(typeof Omniverse !== 'undefined' ? Omniverse : undefined);
    }
}(function (Omniverse) {
    var _modules = Omniverse ? Omniverse._modules : {};
    function _registerModule(obj, path, args, fn) {
        if (!obj.hasOwnProperty(path)) {
            obj[path] = fn.apply(null, args);
        }
    }
    _registerModule(_modules, 'mixins/draw-point.js', [], function () {
        var isFn = function (x) {
            return typeof x === 'function';
        };
        
        var draw = function draw(params) {
            var component = this, graphic = component.graphic, animatableAttribs = params.animatableAttribs, onComplete = params.onComplete, css = params.css, renderer = params.renderer;
            if (component.shouldDraw()) {
                if (!graphic) {
                    component.graphic = graphic =
                        renderer[params.shapeType](params.shapeArgs)
                            .add(params.group);
                }
                graphic
                    .css(css)
                    .attr(params.attribs)
                    .animate(animatableAttribs, params.isNew ? false : undefined, onComplete);
            }
            else if (graphic) {
                var destroy = function () {
                    component.graphic = graphic = graphic.destroy();
                    if (isFn(onComplete)) {
                        onComplete();
                    }
                };
                // animate only runs complete callback if something was animated.
                if (Object.keys(animatableAttribs).length) {
                    graphic.animate(animatableAttribs, undefined, function () {
                        destroy();
                    });
                }
                else {
                    destroy();
                }
            }
        };

        var drawPoint = function drawPoint(params) {
            var point = this, attribs = params.attribs = params.attribs || {};
            // Assigning class in dot notation does go well in IE8
            // eslint-disable-next-line dot-notation
            attribs['class'] = point.getClassName();
            // Call draw to render component
            draw.call(point, params);
        };

        return drawPoint;
    });
    _registerModule(_modules, 'mixins/tree-series.js', [_modules['parts/Globals.js'], _modules['parts/Utilities.js']], function (H, U) {

        var extend = U.extend, isArray = U.isArray, isNumber = U.isNumber, isObject = U.isObject, pick = U.pick;
        var isBoolean = function (x) {
            return typeof x === 'boolean';
        }, isFn = function (x) {
            return typeof x === 'function';
        }, merge = H.merge;

        var setTreeValues = function setTreeValues(tree, options) {
            var before = options.before, idRoot = options.idRoot, mapIdToNode = options.mapIdToNode, nodeRoot = mapIdToNode[idRoot], levelIsConstant = (isBoolean(options.levelIsConstant) ?
                options.levelIsConstant :
                true), points = options.points, point = points[tree.i], optionsPoint = point && point.options || {}, childrenTotal = 0, children = [], value;
            extend(tree, {
                levelDynamic: tree.level - (levelIsConstant ? 0 : nodeRoot.level),
                name: pick(point && point.name, ''),
                visible: (idRoot === tree.id ||
                    (isBoolean(options.visible) ? options.visible : false))
            });
            if (isFn(before)) {
                tree = before(tree, options);
            }
            // First give the children some values
            tree.children.forEach(function (child, i) {
                var newOptions = extend({}, options);
                extend(newOptions, {
                    index: i,
                    siblings: tree.children.length,
                    visible: tree.visible
                });
                child = setTreeValues(child, newOptions);
                children.push(child);
                if (child.visible) {
                    childrenTotal += child.val;
                }
            });
            tree.visible = childrenTotal > 0 || tree.visible;
            // Set the values
            value = pick(optionsPoint.value, childrenTotal);
            extend(tree, {
                children: children,
                childrenTotal: childrenTotal,
                isLeaf: tree.visible && !childrenTotal,
                val: value
            });
            return tree;
        };
  
        var getColor = function getColor(node, options) {
            var index = options.index, mapOptionsToLevel = options.mapOptionsToLevel, parentColor = options.parentColor, parentColorIndex = options.parentColorIndex, series = options.series, colors = options.colors, siblings = options.siblings, points = series.points, getColorByPoint, chartOptionsChart = series.chart.options.chart, point, level, colorByPoint, colorIndexByPoint, color, colorIndex;
     
            function variation(color) {
                var colorVariation = level && level.colorVariation;
                if (colorVariation) {
                    if (colorVariation.key === 'brightness') {
                        return H.color(color).brighten(colorVariation.to * (index / siblings)).get();
                    }
                }
                return color;
            }
            if (node) {
                point = points[node.i];
                level = mapOptionsToLevel[node.level] || {};
                getColorByPoint = point && level.colorByPoint;
                if (getColorByPoint) {
                    colorIndexByPoint = point.index % (colors ?
                        colors.length :
                        chartOptionsChart.colorCount);
                    colorByPoint = colors && colors[colorIndexByPoint];
                }
                // Select either point color, level color or inherited color.
                if (!series.chart.styledMode) {
                    color = pick(point && point.options.color, level && level.color, colorByPoint, parentColor && variation(parentColor), series.color);
                }
                colorIndex = pick(point && point.options.colorIndex, level && level.colorIndex, colorIndexByPoint, parentColorIndex, options.colorIndex);
            }
            return {
                color: color,
                colorIndex: colorIndex
            };
        };

        var getLevelOptions = function getLevelOptions(params) {
            var result = null, defaults, converted, i, from, to, levels;
            if (isObject(params)) {
                result = {};
                from = isNumber(params.from) ? params.from : 1;
                levels = params.levels;
                converted = {};
                defaults = isObject(params.defaults) ? params.defaults : {};
                if (isArray(levels)) {
                    converted = levels.reduce(function (obj, item) {
                        var level, levelIsConstant, options;
                        if (isObject(item) && isNumber(item.level)) {
                            options = merge({}, item);
                            levelIsConstant = (isBoolean(options.levelIsConstant) ?
                                options.levelIsConstant :
                                defaults.levelIsConstant);
                            // Delete redundant properties.
                            delete options.levelIsConstant;
                            delete options.level;
                            // Calculate which level these options apply to.
                            level = item.level + (levelIsConstant ? 0 : from - 1);
                            if (isObject(obj[level])) {
                                extend(obj[level], options);
                            }
                            else {
                                obj[level] = options;
                            }
                        }
                        return obj;
                    }, {});
                }
                to = isNumber(params.to) ? params.to : 1;
                for (i = 0; i <= to; i++) {
                    result[i] = merge({}, defaults, isObject(converted[i]) ? converted[i] : {});
                }
            }
            return result;
        };

        var updateRootId = function (series) {
            var rootId, options;
            if (isObject(series)) {
                // Get the series options.
                options = isObject(series.options) ? series.options : {};
                // Calculate the rootId.
                rootId = pick(series.rootNode, options.rootId, '');
                // Set rootId on series.userOptions to pick it up in exporting.
                if (isObject(series.userOptions)) {
                    series.userOptions.rootId = rootId;
                }
                // Set rootId on series to pick it up on next update.
                series.rootNode = rootId;
            }
            return rootId;
        };
        var result = {
            getColor: getColor,
            getLevelOptions: getLevelOptions,
            setTreeValues: setTreeValues,
            updateRootId: updateRootId
        };

        return result;
    });
    _registerModule(_modules, 'modules/treemap.src.js', [_modules['parts/Globals.js'], _modules['mixins/tree-series.js'], _modules['mixins/draw-point.js'], _modules['parts/Utilities.js']], function (H, mixinTreeSeries, drawPoint, U) {
     
        var defined = U.defined, extend = U.extend, isArray = U.isArray, isNumber = U.isNumber, isObject = U.isObject, isString = U.isString, objectEach = U.objectEach, pick = U.pick;
        /* eslint-disable no-invalid-this */
        var seriesType = H.seriesType, seriesTypes = H.seriesTypes, addEvent = H.addEvent, merge = H.merge, error = H.error, noop = H.noop, fireEvent = H.fireEvent, getColor = mixinTreeSeries.getColor, getLevelOptions = mixinTreeSeries.getLevelOptions, 
        // @todo Similar to eachObject, this function is likely redundant
        isBoolean = function (x) {
            return typeof x === 'boolean';
        }, Series = H.Series, stableSort = H.stableSort, color = H.Color, 
        // @todo Similar to recursive, this function is likely redundant
        eachObject = function (list, func, context) {
            context = context || this;
            objectEach(list, function (val, key) {
                func.call(context, val, key, list);
            });
        }, 
        // @todo find correct name for this function.
        // @todo Similar to reduce, this function is likely redundant
        recursive = function (item, func, context) {
            var next;
            context = context || this;
            next = func.call(context, item);
            if (next !== false) {
                recursive(next, func, context);
            }
        }, updateRootId = mixinTreeSeries.updateRootId;

        seriesType('treemap', 'scatter'

        , {
            
            
            allowTraversingTree: false,
            animationLimit: 250,
 
            showInLegend: false,

            marker: false,

            colorByPoint: false,
  
            dataLabels: {
                /** @ignore-option */
                defer: false,
                /** @ignore-option */
                enabled: true,
                // eslint-disable-next-line valid-jsdoc
                /** @ignore-option */
                formatter: function () {
                    var point = this && this.point ?
                        this.point :
                        {}, name = isString(point.name) ? point.name : '';
                    return name;
                },
                /** @ignore-option */
                inside: true,
                /** @ignore-option */
                verticalAlign: 'middle'
            },
            tooltip: {
                headerFormat: '',
                pointFormat: '<b>{point.name}</b>: {point.value}<br/>'
            },

            ignoreHiddenPoint: true,

            layoutAlgorithm: 'sliceAndDice',

            layoutStartingDirection: 'vertical',

            alternateStartingDirection: false,

            levelIsConstant: true,

            drillUpButton: {
    
                position: {

                    align: 'right',
   
                    x: -10,
  
                    y: 10
                }
            },
    
            traverseUpButton: {
 
                position: {
                    
                    align: 'right',
  
                    x: -10,
   
                    y: 10
                }
            },
         
            borderColor: '#e6e6e6',

            borderWidth: 1,
            colorKey: 'colorValue',

            opacity: 0.15,

            states: {

                hover: {
      
                    borderColor: '#999999',

                    brightness: seriesTypes.heatmap ? 0 : 0.1,
   
                    halo: false,
   
                    opacity: 0.75,
       
                    shadow: false
                }
            }
            // Prototype members
        }, {
            pointArrayMap: ['value'],
            directTouch: true,
            optionalAxis: 'colorAxis',
            getSymbol: noop,
            parallelArrays: ['x', 'y', 'value', 'colorValue'],
            colorKey: 'colorValue',
            trackerGroups: ['group', 'dataLabelsGroup'],
          
            getListOfParents: function (data, existingIds) {
                var arr = isArray(data) ? data : [], ids = isArray(existingIds) ? existingIds : [], listOfParents = arr.reduce(function (prev, curr, i) {
                    var parent = pick(curr.parent, '');
                    if (prev[parent] === undefined) {
                        prev[parent] = [];
                    }
                    prev[parent].push(i);
                    return prev;
                }, {
                    '': [] // Root of tree
                });
                // If parent does not exist, hoist parent to root of tree.
                eachObject(listOfParents, function (children, parent, list) {
                    if ((parent !== '') && (ids.indexOf(parent) === -1)) {
                        children.forEach(function (child) {
                            list[''].push(child);
                        });
                        delete list[parent];
                    }
                });
                return listOfParents;
            },
            // Creates a tree structured object from the series points
            getTree: function () {
                var series = this, allIds = this.data.map(function (d) {
                    return d.id;
                }), parentList = series.getListOfParents(this.data, allIds);
                series.nodeMap = [];
                return series.buildNode('', -1, 0, parentList, null);
            },
            // Define hasData function for non-cartesian series.
            // Returns true if the series has points at all.
            hasData: function () {
                return !!this.processedXData.length; // != 0
            },
            init: function (chart, options) {
                var series = this, colorMapSeriesMixin = H.colorMapSeriesMixin;
                // If color series logic is loaded, add some properties
                if (colorMapSeriesMixin) {
                    this.colorAttribs = colorMapSeriesMixin.colorAttribs;
                }
                // Handle deprecated options.
                addEvent(series, 'setOptions', function (event) {
                    var options = event.userOptions;
                    if (defined(options.allowDrillToNode) &&
                        !defined(options.allowTraversingTree)) {
                        options.allowTraversingTree = options.allowDrillToNode;
                        delete options.allowDrillToNode;
                    }
                    if (defined(options.drillUpButton) &&
                        !defined(options.traverseUpButton)) {
                        options.traverseUpButton = options.drillUpButton;
                        delete options.drillUpButton;
                    }
                });
                Series.prototype.init.call(series, chart, options);
                if (series.options.allowTraversingTree) {
                    addEvent(series, 'click', series.onClickDrillToNode);
                }
            },
            buildNode: function (id, i, level, list, parent) {
                var series = this, children = [], point = series.points[i], height = 0, node, child;
                // Actions
                ((list[id] || [])).forEach(function (i) {
                    child = series.buildNode(series.points[i].id, i, (level + 1), list, id);
                    height = Math.max(child.height + 1, height);
                    children.push(child);
                });
                node = {
                    id: id,
                    i: i,
                    children: children,
                    height: height,
                    level: level,
                    parent: parent,
                    visible: false // @todo move this to better location
                };
                series.nodeMap[node.id] = node;
                if (point) {
                    point.node = node;
                }
                return node;
            },
            setTreeValues: function (tree) {
                var series = this, options = series.options, idRoot = series.rootNode, mapIdToNode = series.nodeMap, nodeRoot = mapIdToNode[idRoot], levelIsConstant = (isBoolean(options.levelIsConstant) ?
                    options.levelIsConstant :
                    true), childrenTotal = 0, children = [], val, point = series.points[tree.i];
                // First give the children some values
                tree.children.forEach(function (child) {
                    child = series.setTreeValues(child);
                    children.push(child);
                    if (!child.ignore) {
                        childrenTotal += child.val;
                    }
                });
                // Sort the children
                stableSort(children, function (a, b) {
                    return a.sortIndex - b.sortIndex;
                });
                // Set the values
                val = pick(point && point.options.value, childrenTotal);
                if (point) {
                    point.value = val;
                }
                extend(tree, {
                    children: children,
                    childrenTotal: childrenTotal,
                    // Ignore this node if point is not visible
                    ignore: !(pick(point && point.visible, true) && (val > 0)),
                    isLeaf: tree.visible && !childrenTotal,
                    levelDynamic: (tree.level - (levelIsConstant ? 0 : nodeRoot.level)),
                    name: pick(point && point.name, ''),
                    sortIndex: pick(point && point.sortIndex, -val),
                    val: val
                });
                return tree;
            },
         
            calculateChildrenAreas: function (parent, area) {
                var series = this, options = series.options, mapOptionsToLevel = series.mapOptionsToLevel, level = mapOptionsToLevel[parent.level + 1], algorithm = pick((series[(level && level.layoutAlgorithm)] &&
                    level.layoutAlgorithm), options.layoutAlgorithm), alternate = options.alternateStartingDirection, childrenValues = [], children;
                // Collect all children which should be included
                children = parent.children.filter(function (n) {
                    return !n.ignore;
                });
                if (level && level.layoutStartingDirection) {
                    area.direction = level.layoutStartingDirection === 'vertical' ?
                        0 :
                        1;
                }
                childrenValues = series[algorithm](area, children);
                children.forEach(function (child, index) {
                    var values = childrenValues[index];
                    child.values = merge(values, {
                        val: child.childrenTotal,
                        direction: (alternate ? 1 - area.direction : area.direction)
                    });
                    child.pointValues = merge(values, {
                        x: (values.x / series.axisRatio),
                        width: (values.width / series.axisRatio)
                    });
                    // If node has children, then call method recursively
                    if (child.children.length) {
                        series.calculateChildrenAreas(child, child.values);
                    }
                });
            },
            setPointValues: function () {
                var series = this, xAxis = series.xAxis, yAxis = series.yAxis;
                series.points.forEach(function (point) {
                    var node = point.node, values = node.pointValues, x1, x2, y1, y2, crispCorr = 0;
                    // Get the crisp correction in classic mode. For this to work in
                    // styled mode, we would need to first add the shape (without x,
                    // y, width and height), then read the rendered stroke width
                    // using point.graphic.strokeWidth(), then modify and apply the
                    // shapeArgs. This applies also to column series, but the
                    // downside is performance and code complexity.
                    if (!series.chart.styledMode) {
                        crispCorr = ((series.pointAttribs(point)['stroke-width'] || 0) % 2) / 2;
                    }
                    // Points which is ignored, have no values.
                    if (values && node.visible) {
                        x1 = Math.round(xAxis.translate(values.x, 0, 0, 0, 1)) - crispCorr;
                        x2 = Math.round(xAxis.translate(values.x + values.width, 0, 0, 0, 1)) - crispCorr;
                        y1 = Math.round(yAxis.translate(values.y, 0, 0, 0, 1)) - crispCorr;
                        y2 = Math.round(yAxis.translate(values.y + values.height, 0, 0, 0, 1)) - crispCorr;
                        // Set point values
                        point.shapeArgs = {
                            x: Math.min(x1, x2),
                            y: Math.min(y1, y2),
                            width: Math.abs(x2 - x1),
                            height: Math.abs(y2 - y1)
                        };
                        point.plotX =
                            point.shapeArgs.x + (point.shapeArgs.width / 2);
                        point.plotY =
                            point.shapeArgs.y + (point.shapeArgs.height / 2);
                    }
                    else {
                        // Reset visibility
                        delete point.plotX;
                        delete point.plotY;
                    }
                });
            },
            // Set the node's color recursively, from the parent down.
            setColorRecursive: function (node, parentColor, colorIndex, index, siblings) {
                var series = this, chart = series && series.chart, colors = chart && chart.options && chart.options.colors, colorInfo, point;
                if (node) {
                    colorInfo = getColor(node, {
                        colors: colors,
                        index: index,
                        mapOptionsToLevel: series.mapOptionsToLevel,
                        parentColor: parentColor,
                        parentColorIndex: colorIndex,
                        series: series,
                        siblings: siblings
                    });
                    point = series.points[node.i];
                    if (point) {
                        point.color = colorInfo.color;
                        point.colorIndex = colorInfo.colorIndex;
                    }
                    // Do it all again with the children
                    (node.children || []).forEach(function (child, i) {
                        series.setColorRecursive(child, colorInfo.color, colorInfo.colorIndex, i, node.children.length);
                    });
                }
            },
            algorithmGroup: function (h, w, d, p) {
                this.height = h;
                this.width = w;
                this.plot = p;
                this.direction = d;
                this.startDirection = d;
                this.total = 0;
                this.nW = 0;
                this.lW = 0;
                this.nH = 0;
                this.lH = 0;
                this.elArr = [];
                this.lP = {
                    total: 0,
                    lH: 0,
                    nH: 0,
                    lW: 0,
                    nW: 0,
                    nR: 0,
                    lR: 0,
                    aspectRatio: function (w, h) {
                        return Math.max((w / h), (h / w));
                    }
                };
                this.addElement = function (el) {
                    this.lP.total = this.elArr[this.elArr.length - 1];
                    this.total = this.total + el;
                    if (this.direction === 0) {
                        // Calculate last point old aspect ratio
                        this.lW = this.nW;
                        this.lP.lH = this.lP.total / this.lW;
                        this.lP.lR = this.lP.aspectRatio(this.lW, this.lP.lH);
                        // Calculate last point new aspect ratio
                        this.nW = this.total / this.height;
                        this.lP.nH = this.lP.total / this.nW;
                        this.lP.nR = this.lP.aspectRatio(this.nW, this.lP.nH);
                    }
                    else {
                        // Calculate last point old aspect ratio
                        this.lH = this.nH;
                        this.lP.lW = this.lP.total / this.lH;
                        this.lP.lR = this.lP.aspectRatio(this.lP.lW, this.lH);
                        // Calculate last point new aspect ratio
                        this.nH = this.total / this.width;
                        this.lP.nW = this.lP.total / this.nH;
                        this.lP.nR = this.lP.aspectRatio(this.lP.nW, this.nH);
                    }
                    this.elArr.push(el);
                };
                this.reset = function () {
                    this.nW = 0;
                    this.lW = 0;
                    this.elArr = [];
                    this.total = 0;
                };
            },
            algorithmCalcPoints: function (directionChange, last, group, childrenArea) {
                var pX, pY, pW, pH, gW = group.lW, gH = group.lH, plot = group.plot, keep, i = 0, end = group.elArr.length - 1;
                if (last) {
                    gW = group.nW;
                    gH = group.nH;
                }
                else {
                    keep = group.elArr[group.elArr.length - 1];
                }
                group.elArr.forEach(function (p) {
                    if (last || (i < end)) {
                        if (group.direction === 0) {
                            pX = plot.x;
                            pY = plot.y;
                            pW = gW;
                            pH = p / pW;
                        }
                        else {
                            pX = plot.x;
                            pY = plot.y;
                            pH = gH;
                            pW = p / pH;
                        }
                        childrenArea.push({
                            x: pX,
                            y: pY,
                            width: pW,
                            height: H.correctFloat(pH)
                        });
                        if (group.direction === 0) {
                            plot.y = plot.y + pH;
                        }
                        else {
                            plot.x = plot.x + pW;
                        }
                    }
                    i = i + 1;
                });
                // Reset variables
                group.reset();
                if (group.direction === 0) {
                    group.width = group.width - gW;
                }
                else {
                    group.height = group.height - gH;
                }
                plot.y = plot.parent.y + (plot.parent.height - group.height);
                plot.x = plot.parent.x + (plot.parent.width - group.width);
                if (directionChange) {
                    group.direction = 1 - group.direction;
                }
                // If not last, then add uncalculated element
                if (!last) {
                    group.addElement(keep);
                }
            },
            algorithmLowAspectRatio: function (directionChange, parent, children) {
                var childrenArea = [], series = this, pTot, plot = {
                    x: parent.x,
                    y: parent.y,
                    parent: parent
                }, direction = parent.direction, i = 0, end = children.length - 1, group = new this.algorithmGroup(// eslint-disable-line new-cap
                parent.height, parent.width, direction, plot);
                // Loop through and calculate all areas
                children.forEach(function (child) {
                    pTot =
                        (parent.width * parent.height) * (child.val / parent.val);
                    group.addElement(pTot);
                    if (group.lP.nR > group.lP.lR) {
                        series.algorithmCalcPoints(directionChange, false, group, childrenArea, plot // @todo no supported
                        );
                    }
                    // If last child, then calculate all remaining areas
                    if (i === end) {
                        series.algorithmCalcPoints(directionChange, true, group, childrenArea, plot // @todo not supported
                        );
                    }
                    i = i + 1;
                });
                return childrenArea;
            },
            algorithmFill: function (directionChange, parent, children) {
                var childrenArea = [], pTot, direction = parent.direction, x = parent.x, y = parent.y, width = parent.width, height = parent.height, pX, pY, pW, pH;
                children.forEach(function (child) {
                    pTot =
                        (parent.width * parent.height) * (child.val / parent.val);
                    pX = x;
                    pY = y;
                    if (direction === 0) {
                        pH = height;
                        pW = pTot / pH;
                        width = width - pW;
                        x = x + pW;
                    }
                    else {
                        pW = width;
                        pH = pTot / pW;
                        height = height - pH;
                        y = y + pH;
                    }
                    childrenArea.push({
                        x: pX,
                        y: pY,
                        width: pW,
                        height: pH
                    });
                    if (directionChange) {
                        direction = 1 - direction;
                    }
                });
                return childrenArea;
            },
            strip: function (parent, children) {
                return this.algorithmLowAspectRatio(false, parent, children);
            },
            squarified: function (parent, children) {
                return this.algorithmLowAspectRatio(true, parent, children);
            },
            sliceAndDice: function (parent, children) {
                return this.algorithmFill(true, parent, children);
            },
            stripes: function (parent, children) {
                return this.algorithmFill(false, parent, children);
            },
            translate: function () {
                var series = this, options = series.options, 
                // NOTE: updateRootId modifies series.
                rootId = updateRootId(series), rootNode, pointValues, seriesArea, tree, val;
                // Call prototype function
                Series.prototype.translate.call(series);
                // @todo Only if series.isDirtyData is true
                tree = series.tree = series.getTree();
                rootNode = series.nodeMap[rootId];
                series.renderTraverseUpButton(rootId);
                series.mapOptionsToLevel = getLevelOptions({
                    from: rootNode.level + 1,
                    levels: options.levels,
                    to: tree.height,
                    defaults: {
                        levelIsConstant: series.options.levelIsConstant,
                        colorByPoint: options.colorByPoint
                    }
                });
                if (rootId !== '' &&
                    (!rootNode || !rootNode.children.length)) {
                    series.setRootNode('', false);
                    rootId = series.rootNode;
                    rootNode = series.nodeMap[rootId];
                }
                // Parents of the root node is by default visible
                recursive(series.nodeMap[series.rootNode], function (node) {
                    var next = false, p = node.parent;
                    node.visible = true;
                    if (p || p === '') {
                        next = series.nodeMap[p];
                    }
                    return next;
                });
                // Children of the root node is by default visible
                recursive(series.nodeMap[series.rootNode].children, function (children) {
                    var next = false;
                    children.forEach(function (child) {
                        child.visible = true;
                        if (child.children.length) {
                            next = (next || []).concat(child.children);
                        }
                    });
                    return next;
                });
                series.setTreeValues(tree);
                // Calculate plotting values.
                series.axisRatio = (series.xAxis.len / series.yAxis.len);
                series.nodeMap[''].pointValues = pointValues = {
                    x: 0,
                    y: 0,
                    width: 100,
                    height: 100
                };
                series.nodeMap[''].values = seriesArea = merge(pointValues, {
                    width: (pointValues.width * series.axisRatio),
                    direction: (options.layoutStartingDirection === 'vertical' ? 0 : 1),
                    val: tree.val
                });
                series.calculateChildrenAreas(tree, seriesArea);
                // Logic for point colors
                if (!series.colorAxis &&
                    !options.colorByPoint) {
                    series.setColorRecursive(series.tree);
                }
                // Update axis extremes according to the root node.
                if (options.allowTraversingTree) {
                    val = rootNode.pointValues;
                    series.xAxis.setExtremes(val.x, val.x + val.width, false);
                    series.yAxis.setExtremes(val.y, val.y + val.height, false);
                    series.xAxis.setScale();
                    series.yAxis.setScale();
                }
                // Assign values to points.
                series.setPointValues();
            },
        
            drawDataLabels: function () {
                var series = this, mapOptionsToLevel = series.mapOptionsToLevel, points = series.points.filter(function (n) {
                    return n.node.visible;
                }), options, level;
                points.forEach(function (point) {
                    level = mapOptionsToLevel[point.node.level];
                    // Set options to new object to avoid problems with scope
                    options = { style: {} };
                    // If not a leaf, then label should be disabled as default
                    if (!point.node.isLeaf) {
                        options.enabled = false;
                    }
                    // If options for level exists, include them as well
                    if (level && level.dataLabels) {
                        options = merge(options, level.dataLabels);
                        series._hasPointLabels = true;
                    }
                    // Set dataLabel width to the width of the point shape.
                    if (point.shapeArgs) {
                        options.style.width = point.shapeArgs.width;
                        if (point.dataLabel) {
                            point.dataLabel.css({
                                width: point.shapeArgs.width + 'px'
                            });
                        }
                    }
                    // Merge custom options with point options
                    point.dlOptions = merge(options, point.options.dataLabels);
                });
                Series.prototype.drawDataLabels.call(this);
            },
            // Over the alignment method by setting z index
            alignDataLabel: function (point, dataLabel, labelOptions) {
                var style = labelOptions.style;
                // #8160: Prevent the label from exceeding the point's
                // boundaries in treemaps by applying ellipsis overflow.
                // The issue was happening when datalabel's text contained a
                // long sequence of characters without a whitespace.
                if (!defined(style.textOverflow) &&
                    dataLabel.text &&
                    dataLabel.getBBox().width > dataLabel.text.textWidth) {
                    dataLabel.css({
                        textOverflow: 'ellipsis',
                        // unit (px) is required when useHTML is true
                        width: style.width += 'px'
                    });
                }
                seriesTypes.column.prototype.alignDataLabel.apply(this, arguments);
                if (point.dataLabel) {
                    // point.node.zIndex could be undefined (#6956)
                    point.dataLabel.attr({ zIndex: (point.node.zIndex || 0) + 1 });
                }
            },
            // Get presentational attributes
            pointAttribs: function (point, state) {
                var series = this, mapOptionsToLevel = (isObject(series.mapOptionsToLevel) ?
                    series.mapOptionsToLevel :
                    {}), level = point && mapOptionsToLevel[point.node.level] || {}, options = this.options, attr, stateOptions = (state && options.states[state]) || {}, className = (point && point.getClassName()) || '', opacity;
                // Set attributes by precedence. Point trumps level trumps series.
                // Stroke width uses pick because it can be 0.
                attr = {
                    'stroke': (point && point.borderColor) ||
                        level.borderColor ||
                        stateOptions.borderColor ||
                        options.borderColor,
                    'stroke-width': pick(point && point.borderWidth, level.borderWidth, stateOptions.borderWidth, options.borderWidth),
                    'dashstyle': (point && point.borderDashStyle) ||
                        level.borderDashStyle ||
                        stateOptions.borderDashStyle ||
                        options.borderDashStyle,
                    'fill': (point && point.color) || this.color
                };
                // Hide levels above the current view
                if (className.indexOf('Omniverse-above-level') !== -1) {
                    attr.fill = 'none';
                    attr['stroke-width'] = 0;
                    // Nodes with children that accept interaction
                }
                else if (className.indexOf('Omniverse-internal-node-interactive') !== -1) {
                    opacity = pick(stateOptions.opacity, options.opacity);
                    attr.fill = color(attr.fill).setOpacity(opacity).get();
                    attr.cursor = 'pointer';
                    // Hide nodes that have children
                }
                else if (className.indexOf('Omniverse-internal-node') !== -1) {
                    attr.fill = 'none';
                }
                else if (state) {
                    // Brighten and hoist the hover nodes
                    attr.fill = color(attr.fill)
                        .brighten(stateOptions.brightness)
                        .get();
                }
                return attr;
            },
            // Override drawPoints
            drawPoints: function () {
                var series = this, chart = series.chart, renderer = chart.renderer, points = series.points, styledMode = chart.styledMode, options = series.options, shadow = styledMode ? {} : options.shadow, borderRadius = options.borderRadius, withinAnimationLimit = chart.pointCount < options.animationLimit, allowTraversingTree = options.allowTraversingTree;
                points.forEach(function (point) {
                    var levelDynamic = point.node.levelDynamic, animate = {}, attr = {}, css = {}, groupKey = 'level-group-' + levelDynamic, hasGraphic = !!point.graphic, shouldAnimate = withinAnimationLimit && hasGraphic, shapeArgs = point.shapeArgs;
                    // Don't bother with calculate styling if the point is not drawn
                    if (point.shouldDraw()) {
                        if (borderRadius) {
                            attr.r = borderRadius;
                        }
                        merge(true, // Extend object
                        // Which object to extend
                        shouldAnimate ? animate : attr, 
                        // Add shapeArgs to animate/attr if graphic exists
                        hasGraphic ? shapeArgs : {}, 
                        // Add style attribs if !styleMode
                        styledMode ?
                            {} :
                            series.pointAttribs(point, (point.selected && 'select')));
                        // In styled mode apply point.color. Use CSS, otherwise the
                        // fill used in the style sheet will take precedence over
                        // the fill attribute.
                        if (series.colorAttribs && styledMode) {
                            // Heatmap is loaded
                            extend(css, series.colorAttribs(point));
                        }
                        if (!series[groupKey]) {
                            series[groupKey] = renderer.g(groupKey)
                                .attr({
                                // @todo Set the zIndex based upon the number of
                                // levels, instead of using 1000
                                zIndex: 1000 - levelDynamic
                            })
                                .add(series.group);
                            series[groupKey].survive = true;
                        }
                    }
                    // Draw the point
                    point.draw({
                        animatableAttribs: animate,
                        attribs: attr,
                        css: css,
                        group: series[groupKey],
                        renderer: renderer,
                        shadow: shadow,
                        shapeArgs: shapeArgs,
                        shapeType: 'rect'
                    });
                    // If setRootNode is allowed, set a point cursor on clickables &
                    // add drillId to point
                    if (allowTraversingTree && point.graphic) {
                        point.drillId = options.interactByLeaf ?
                            series.drillToByLeaf(point) :
                            series.drillToByGroup(point);
                    }
                });
            },
            // Add drilling on the suitable points
            onClickDrillToNode: function (event) {
                var series = this, point = event.point, drillId = point && point.drillId;
                // If a drill id is returned, add click event and cursor.
                if (isString(drillId)) {
                    point.setState(''); // Remove hover
                    series.setRootNode(drillId, true, { trigger: 'click' });
                }
            },
            
            drillToByGroup: function (point) {
                var series = this, drillId = false;
                if ((point.node.level - series.nodeMap[series.rootNode].level) ===
                    1 &&
                    !point.node.isLeaf) {
                    drillId = point.id;
                }
                return drillId;
            },
            
            drillToByLeaf: function (point) {
                var series = this, drillId = false, nodeParent;
                if ((point.node.parent !== series.rootNode) &&
                    point.node.isLeaf) {
                    nodeParent = point.node;
                    while (!drillId) {
                        nodeParent = series.nodeMap[nodeParent.parent];
                        if (nodeParent.parent === series.rootNode) {
                            drillId = nodeParent.id;
                        }
                    }
                }
                return drillId;
            },
            drillUp: function () {
                var series = this, node = series.nodeMap[series.rootNode];
                if (node && isString(node.parent)) {
                    series.setRootNode(node.parent, true, { trigger: 'traverseUpButton' });
                }
            },
            // TODO remove this function at a suitable version.
            drillToNode: function (id, redraw) {
                error('WARNING: treemap.drillToNode has been renamed to treemap.' +
                    'setRootNode, and will be removed in the next major version.');
                this.setRootNode(id, redraw);
            },
            
            setRootNode: function (id, redraw, eventArguments) {
                var series = this, eventArgs = extend({
                    newRootId: id,
                    previousRootId: series.rootNode,
                    redraw: pick(redraw, true),
                    series: series
                }, eventArguments);
                
                var defaultFn = function (args) {
                    var series = args.series;
                    // Store previous and new root ids on the series.
                    series.idPreviousRoot = args.previousRootId;
                    series.rootNode = args.newRootId;
                    // Redraw the chart
                    series.isDirty = true; // Force redraw
                    if (args.redraw) {
                        series.chart.redraw();
                    }
                };
                // Fire setRootNode event.
                fireEvent(series, 'setRootNode', eventArgs, defaultFn);
            },
            renderTraverseUpButton: function (rootId) {
                var series = this, nodeMap = series.nodeMap, node = nodeMap[rootId], name = node.name, buttonOptions = series.options.traverseUpButton, backText = pick(buttonOptions.text, name, '< Back'), attr, states;
                if (rootId === '') {
                    if (series.drillUpButton) {
                        series.drillUpButton =
                            series.drillUpButton.destroy();
                    }
                }
                else if (!this.drillUpButton) {
                    attr = buttonOptions.theme;
                    states = attr && attr.states;
                    this.drillUpButton = this.chart.renderer
                        .button(backText, null, null, function () {
                        series.drillUp();
                    }, attr, states && states.hover, states && states.select)
                        .addClass('Omniverse-drillup-button')
                        .attr({
                        align: buttonOptions.position.align,
                        zIndex: 7
                    })
                        .add()
                        .align(buttonOptions.position, false, buttonOptions.relativeTo || 'plotBox');
                }
                else {
                    this.drillUpButton.placed = false;
                    this.drillUpButton.attr({
                        text: backText
                    })
                        .align();
                }
            },
            buildKDTree: noop,
            drawLegendSymbol: H.LegendSymbolMixin.drawRectangle,
            getExtremes: function () {
                // Get the extremes from the value data
                Series.prototype.getExtremes.call(this, this.colorValueData);
                this.valueMin = this.dataMin;
                this.valueMax = this.dataMax;
                // Get the extremes from the y data
                Series.prototype.getExtremes.call(this);
            },
            getExtremesFromAll: true,
            bindAxes: function () {
                var treeAxis = {
                    endOnTick: false,
                    gridLineWidth: 0,
                    lineWidth: 0,
                    min: 0,
                    dataMin: 0,
                    minPadding: 0,
                    max: 100,
                    dataMax: 100,
                    maxPadding: 0,
                    startOnTick: false,
                    title: null,
                    tickPositions: []
                };
                Series.prototype.bindAxes.call(this);
                extend(this.yAxis.options, treeAxis);
                extend(this.xAxis.options, treeAxis);
            },
            
            setState: function (state) {
                this.options.inactiveOtherPoints = true;
                Series.prototype.setState.call(this, state, false);
                this.options.inactiveOtherPoints = false;
            },
            utils: {
                recursive: recursive
            }
            /* eslint-enable no-invalid-this, valid-jsdoc */
        }, {
            draw: drawPoint,
            setVisible: seriesTypes.pie.prototype.pointClass.prototype.setVisible,
            /* eslint-disable no-invalid-this, valid-jsdoc */
            getClassName: function () {
                var className = H.Point.prototype.getClassName.call(this), series = this.series, options = series.options;
                // Above the current level
                if (this.node.level <= series.nodeMap[series.rootNode].level) {
                    className += ' Omniverse-above-level';
                }
                else if (!this.node.isLeaf &&
                    !pick(options.interactByLeaf, !options.allowTraversingTree)) {
                    className += ' Omniverse-internal-node-interactive';
                }
                else if (!this.node.isLeaf) {
                    className += ' Omniverse-internal-node';
                }
                return className;
            },
            
            isValid: function () {
                return this.id || isNumber(this.value);
            },
            setState: function (state) {
                H.Point.prototype.setState.call(this, state);
                // Graphic does not exist when point is not visible.
                if (this.graphic) {
                    this.graphic.attr({
                        zIndex: state === 'hover' ? 1 : 0
                    });
                }
            },
            shouldDraw: function () {
                var point = this;
                return isNumber(point.plotY) && point.y !== null;
            }
            /* eslint-enable no-invalid-this, valid-jsdoc */
        });

    });
    _registerModule(_modules, 'modules/sunburst.src.js', [_modules['parts/Globals.js'], _modules['parts/Utilities.js'], _modules['mixins/draw-point.js'], _modules['mixins/tree-series.js']], function (H, U, drawPoint, mixinTreeSeries) {
        
        var extend = U.extend, isNumber = U.isNumber, isObject = U.isObject, isString = U.isString;
        var CenteredSeriesMixin = H.CenteredSeriesMixin, Series = H.Series, getCenter = CenteredSeriesMixin.getCenter, getColor = mixinTreeSeries.getColor, getLevelOptions = mixinTreeSeries.getLevelOptions, getStartAndEndRadians = CenteredSeriesMixin.getStartAndEndRadians, isBoolean = function (x) {
            return typeof x === 'boolean';
        }, merge = H.merge, noop = H.noop, rad2deg = 180 / Math.PI, seriesType = H.seriesType, seriesTypes = H.seriesTypes, setTreeValues = mixinTreeSeries.setTreeValues, updateRootId = mixinTreeSeries.updateRootId;
        // TODO introduce step, which should default to 1.
        var range = function range(from, to) {
            var result = [], i;
            if (isNumber(from) && isNumber(to) && from <= to) {
                for (i = from; i <= to; i++) {
                    result.push(i);
                }
            }
            return result;
        };
        
        var calculateLevelSizes = function calculateLevelSizes(levelOptions, params) {
            var result, p = isObject(params) ? params : {}, totalWeight = 0, diffRadius, levels, levelsNotIncluded, remainingSize, from, to;
            if (isObject(levelOptions)) {
                result = merge({}, levelOptions);
                from = isNumber(p.from) ? p.from : 0;
                to = isNumber(p.to) ? p.to : 0;
                levels = range(from, to);
                levelsNotIncluded = Object.keys(result).filter(function (k) {
                    return levels.indexOf(+k) === -1;
                });
                diffRadius = remainingSize = isNumber(p.diffRadius) ? p.diffRadius : 0;
                // Convert percentage to pixels.
                // Calculate the remaining size to divide between "weight" levels.
                // Calculate total weight to use in convertion from weight to pixels.
                levels.forEach(function (level) {
                    var options = result[level], unit = options.levelSize.unit, value = options.levelSize.value;
                    if (unit === 'weight') {
                        totalWeight += value;
                    }
                    else if (unit === 'percentage') {
                        options.levelSize = {
                            unit: 'pixels',
                            value: (value / 100) * diffRadius
                        };
                        remainingSize -= options.levelSize.value;
                    }
                    else if (unit === 'pixels') {
                        remainingSize -= value;
                    }
                });
                // Convert weight to pixels.
                levels.forEach(function (level) {
                    var options = result[level], weight;
                    if (options.levelSize.unit === 'weight') {
                        weight = options.levelSize.value;
                        result[level].levelSize = {
                            unit: 'pixels',
                            value: (weight / totalWeight) * remainingSize
                        };
                    }
                });
                // Set all levels not included in interval [from,to] to have 0 pixels.
                levelsNotIncluded.forEach(function (level) {
                    result[level].levelSize = {
                        value: 0,
                        unit: 'pixels'
                    };
                });
            }
            return result;
        };
        
        var getEndPoint = function getEndPoint(x, y, angle, distance) {
            return {
                x: x + (Math.cos(angle) * distance),
                y: y + (Math.sin(angle) * distance)
            };
        };
        var layoutAlgorithm = function layoutAlgorithm(parent, children, options) {
            var startAngle = parent.start, range = parent.end - startAngle, total = parent.val, x = parent.x, y = parent.y, radius = ((options &&
                isObject(options.levelSize) &&
                isNumber(options.levelSize.value)) ?
                options.levelSize.value :
                0), innerRadius = parent.r, outerRadius = innerRadius + radius, slicedOffset = options && isNumber(options.slicedOffset) ?
                options.slicedOffset :
                0;
            return (children || []).reduce(function (arr, child) {
                var percentage = (1 / total) * child.val, radians = percentage * range, radiansCenter = startAngle + (radians / 2), offsetPosition = getEndPoint(x, y, radiansCenter, slicedOffset), values = {
                    x: child.sliced ? offsetPosition.x : x,
                    y: child.sliced ? offsetPosition.y : y,
                    innerR: innerRadius,
                    r: outerRadius,
                    radius: radius,
                    start: startAngle,
                    end: startAngle + radians
                };
                arr.push(values);
                startAngle = values.end;
                return arr;
            }, []);
        };
        var getDlOptions = function getDlOptions(params) {
            // Set options to new object to avoid problems with scope
            var point = params.point, shape = isObject(params.shapeArgs) ? params.shapeArgs : {}, optionsPoint = (isObject(params.optionsPoint) ?
                params.optionsPoint.dataLabels :
                {}), optionsLevel = (isObject(params.level) ?
                params.level.dataLabels :
                {}), options = merge({
                style: {}
            }, optionsLevel, optionsPoint), rotationRad, rotation, rotationMode = options.rotationMode;
            if (!isNumber(options.rotation)) {
                if (rotationMode === 'auto') {
                    if (point.innerArcLength < 1 &&
                        point.outerArcLength > shape.radius) {
                        rotationRad = 0;
                    }
                    else if (point.innerArcLength > 1 &&
                        point.outerArcLength > 1.5 * shape.radius) {
                        rotationMode = 'parallel';
                    }
                    else {
                        rotationMode = 'perpendicular';
                    }
                }
                if (rotationMode !== 'auto') {
                    rotationRad = (shape.end -
                        (shape.end - shape.start) / 2);
                }
                if (rotationMode === 'parallel') {
                    options.style.width = Math.min(shape.radius * 2.5, (point.outerArcLength + point.innerArcLength) / 2);
                }
                else {
                    options.style.width = shape.radius;
                }
                if (rotationMode === 'perpendicular' &&
                    point.series.chart.renderer.fontMetrics(options.style.fontSize).h > point.outerArcLength) {
                    options.style.width = 1;
                }
                // Apply padding (#8515)
                options.style.width = Math.max(options.style.width - 2 * (options.padding || 0), 1);
                rotation = (rotationRad * rad2deg) % 180;
                if (rotationMode === 'parallel') {
                    rotation -= 90;
                }
                // Prevent text from rotating upside down
                if (rotation > 90) {
                    rotation -= 180;
                }
                else if (rotation < -90) {
                    rotation += 180;
                }
                options.rotation = rotation;
            }
            // NOTE: alignDataLabel positions the data label differntly when rotation is
            // 0. Avoiding this by setting rotation to a small number.
            if (options.rotation === 0) {
                options.rotation = 0.001;
            }
            return options;
        };
        var getAnimation = function getAnimation(shape, params) {
            var point = params.point, radians = params.radians, innerR = params.innerR, idRoot = params.idRoot, idPreviousRoot = params.idPreviousRoot, shapeExisting = params.shapeExisting, shapeRoot = params.shapeRoot, shapePreviousRoot = params.shapePreviousRoot, visible = params.visible, from = {}, to = {
                end: shape.end,
                start: shape.start,
                innerR: shape.innerR,
                r: shape.r,
                x: shape.x,
                y: shape.y
            };
            if (visible) {
                // Animate points in
                if (!point.graphic && shapePreviousRoot) {
                    if (idRoot === point.id) {
                        from = {
                            start: radians.start,
                            end: radians.end
                        };
                    }
                    else {
                        from = (shapePreviousRoot.end <= shape.start) ? {
                            start: radians.end,
                            end: radians.end
                        } : {
                            start: radians.start,
                            end: radians.start
                        };
                    }
                    // Animate from center and outwards.
                    from.innerR = from.r = innerR;
                }
            }
            else {
                // Animate points out
                if (point.graphic) {
                    if (idPreviousRoot === point.id) {
                        to = {
                            innerR: innerR,
                            r: innerR
                        };
                    }
                    else if (shapeRoot) {
                        to = (shapeRoot.end <= shapeExisting.start) ?
                            {
                                innerR: innerR,
                                r: innerR,
                                start: radians.end,
                                end: radians.end
                            } : {
                            innerR: innerR,
                            r: innerR,
                            start: radians.start,
                            end: radians.start
                        };
                    }
                }
            }
            return {
                from: from,
                to: to
            };
        };
        var getDrillId = function getDrillId(point, idRoot, mapIdToNode) {
            var drillId, node = point.node, nodeRoot;
            if (!node.isLeaf) {
                // When it is the root node, the drillId should be set to parent.
                if (idRoot === point.id) {
                    nodeRoot = mapIdToNode[idRoot];
                    drillId = nodeRoot.parent;
                }
                else {
                    drillId = point.id;
                }
            }
            return drillId;
        };
        var cbSetTreeValuesBefore = function before(node, options) {
            var mapIdToNode = options.mapIdToNode, nodeParent = mapIdToNode[node.parent], series = options.series, chart = series.chart, points = series.points, point = points[node.i], colors = (series.options.colors || chart && chart.options.colors), colorInfo = getColor(node, {
                colors: colors,
                colorIndex: series.colorIndex,
                index: options.index,
                mapOptionsToLevel: options.mapOptionsToLevel,
                parentColor: nodeParent && nodeParent.color,
                parentColorIndex: nodeParent && nodeParent.colorIndex,
                series: options.series,
                siblings: options.siblings
            });
            node.color = colorInfo.color;
            node.colorIndex = colorInfo.colorIndex;
            if (point) {
                point.color = node.color;
                point.colorIndex = node.colorIndex;
                // Set slicing on node, but avoid slicing the top node.
                node.sliced = (node.id !== options.idRoot) ? point.sliced : false;
            }
            return node;
        };
        
        var sunburstOptions = {
           
           
           
            center: ['50%', '50%'],
            colorByPoint: false,
    
            opacity: 1,
    
            dataLabels: {
                /** @ignore-option */
                allowOverlap: true,
                /** @ignore-option */
                defer: true,
                /** @ignore-option */
                rotationMode: 'auto',
                /** @ignore-option */
                style: {
                    textOverflow: 'ellipsis'
                }
            },
     
            rootId: undefined,
     
            levelIsConstant: true,
           
            levelSize: {
                
                value: 1,
               
                unit: 'weight'
            },
            
            slicedOffset: 10
        };
        // Properties of the Sunburst series.
        var sunburstSeries = {
            drawDataLabels: noop,
            drawPoints: function drawPoints() {
                var series = this, mapOptionsToLevel = series.mapOptionsToLevel, shapeRoot = series.shapeRoot, group = series.group, hasRendered = series.hasRendered, idRoot = series.rootNode, idPreviousRoot = series.idPreviousRoot, nodeMap = series.nodeMap, nodePreviousRoot = nodeMap[idPreviousRoot], shapePreviousRoot = nodePreviousRoot && nodePreviousRoot.shapeArgs, points = series.points, radians = series.startAndEndRadians, chart = series.chart, optionsChart = chart && chart.options && chart.options.chart || {}, animation = (isBoolean(optionsChart.animation) ?
                    optionsChart.animation :
                    true), positions = series.center, center = {
                    x: positions[0],
                    y: positions[1]
                }, innerR = positions[3] / 2, renderer = series.chart.renderer, animateLabels, animateLabelsCalled = false, addedHack = false, hackDataLabelAnimation = !!(animation &&
                    hasRendered &&
                    idRoot !== idPreviousRoot &&
                    series.dataLabelsGroup);
                if (hackDataLabelAnimation) {
                    series.dataLabelsGroup.attr({ opacity: 0 });
                    animateLabels = function () {
                        var s = series;
                        animateLabelsCalled = true;
                        if (s.dataLabelsGroup) {
                            s.dataLabelsGroup.animate({
                                opacity: 1,
                                visibility: 'visible'
                            });
                        }
                    };
                }
                points.forEach(function (point) {
                    var node = point.node, level = mapOptionsToLevel[node.level], shapeExisting = point.shapeExisting || {}, shape = node.shapeArgs || {}, animationInfo, onComplete, visible = !!(node.visible && node.shapeArgs);
                    if (hasRendered && animation) {
                        animationInfo = getAnimation(shape, {
                            center: center,
                            point: point,
                            radians: radians,
                            innerR: innerR,
                            idRoot: idRoot,
                            idPreviousRoot: idPreviousRoot,
                            shapeExisting: shapeExisting,
                            shapeRoot: shapeRoot,
                            shapePreviousRoot: shapePreviousRoot,
                            visible: visible
                        });
                    }
                    else {
                        // When animation is disabled, attr is called from animation.
                        animationInfo = {
                            to: shape,
                            from: {}
                        };
                    }
                    extend(point, {
                        shapeExisting: shape,
                        tooltipPos: [shape.plotX, shape.plotY],
                        drillId: getDrillId(point, idRoot, nodeMap),
                        name: '' + (point.name || point.id || point.index),
                        plotX: shape.plotX,
                        plotY: shape.plotY,
                        value: node.val,
                        isNull: !visible // used for dataLabels & point.draw
                    });
                    point.dlOptions = getDlOptions({
                        point: point,
                        level: level,
                        optionsPoint: point.options,
                        shapeArgs: shape
                    });
                    if (!addedHack && visible) {
                        addedHack = true;
                        onComplete = animateLabels;
                    }
                    point.draw({
                        animatableAttribs: animationInfo.to,
                        attribs: extend(animationInfo.from, (!chart.styledMode && series.pointAttribs(point, (point.selected && 'select')))),
                        onComplete: onComplete,
                        group: group,
                        renderer: renderer,
                        shapeType: 'arc',
                        shapeArgs: shape
                    });
                });
                // Draw data labels after points
                // TODO draw labels one by one to avoid addtional looping
                if (hackDataLabelAnimation && addedHack) {
                    series.hasRendered = false;
                    series.options.dataLabels.defer = true;
                    Series.prototype.drawDataLabels.call(series);
                    series.hasRendered = true;
                    // If animateLabels is called before labels were hidden, then call
                    // it again.
                    if (animateLabelsCalled) {
                        animateLabels();
                    }
                }
                else {
                    Series.prototype.drawDataLabels.call(series);
                }
            },
            pointAttribs: seriesTypes.column.prototype.pointAttribs,
            // The layout algorithm for the levels
            layoutAlgorithm: layoutAlgorithm,
            // Set the shape arguments on the nodes. Recursive from root down.
            setShapeArgs: function (parent, parentValues, mapOptionsToLevel) {
                var childrenValues = [], level = parent.level + 1, options = mapOptionsToLevel[level], 
                // Collect all children which should be included
                children = parent.children.filter(function (n) {
                    return n.visible;
                }), twoPi = 6.28; // Two times Pi.
                childrenValues = this.layoutAlgorithm(parentValues, children, options);
                children.forEach(function (child, index) {
                    var values = childrenValues[index], angle = values.start + ((values.end - values.start) / 2), radius = values.innerR + ((values.r - values.innerR) / 2), radians = (values.end - values.start), isCircle = (values.innerR === 0 && radians > twoPi), center = (isCircle ?
                        { x: values.x, y: values.y } :
                        getEndPoint(values.x, values.y, angle, radius)), val = (child.val ?
                        (child.childrenTotal > child.val ?
                            child.childrenTotal :
                            child.val) :
                        child.childrenTotal);
                    // The inner arc length is a convenience for data label filters.
                    if (this.points[child.i]) {
                        this.points[child.i].innerArcLength = radians * values.innerR;
                        this.points[child.i].outerArcLength = radians * values.r;
                    }
                    child.shapeArgs = merge(values, {
                        plotX: center.x,
                        plotY: center.y + 4 * Math.abs(Math.cos(angle))
                    });
                    child.values = merge(values, {
                        val: val
                    });
                    // If node has children, then call method recursively
                    if (child.children.length) {
                        this.setShapeArgs(child, child.values, mapOptionsToLevel);
                    }
                }, this);
            },
            translate: function translate() {
                var series = this, options = series.options, positions = series.center = getCenter.call(series), radians = series.startAndEndRadians = getStartAndEndRadians(options.startAngle, options.endAngle), innerRadius = positions[3] / 2, outerRadius = positions[2] / 2, diffRadius = outerRadius - innerRadius, 
                // NOTE: updateRootId modifies series.
                rootId = updateRootId(series), mapIdToNode = series.nodeMap, mapOptionsToLevel, idTop, nodeRoot = mapIdToNode && mapIdToNode[rootId], nodeTop, tree, values, nodeIds = {};
                series.shapeRoot = nodeRoot && nodeRoot.shapeArgs;
                // Call prototype function
                Series.prototype.translate.call(series);
                // @todo Only if series.isDirtyData is true
                tree = series.tree = series.getTree();
                // Render traverseUpButton, after series.nodeMap i calculated.
                series.renderTraverseUpButton(rootId);
                mapIdToNode = series.nodeMap;
                nodeRoot = mapIdToNode[rootId];
                idTop = isString(nodeRoot.parent) ? nodeRoot.parent : '';
                nodeTop = mapIdToNode[idTop];
                mapOptionsToLevel = getLevelOptions({
                    from: nodeRoot.level > 0 ? nodeRoot.level : 1,
                    levels: series.options.levels,
                    to: tree.height,
                    defaults: {
                        colorByPoint: options.colorByPoint,
                        dataLabels: options.dataLabels,
                        levelIsConstant: options.levelIsConstant,
                        levelSize: options.levelSize,
                        slicedOffset: options.slicedOffset
                    }
                });
                // NOTE consider doing calculateLevelSizes in a callback to
                // getLevelOptions
                mapOptionsToLevel = calculateLevelSizes(mapOptionsToLevel, {
                    diffRadius: diffRadius,
                    from: nodeRoot.level > 0 ? nodeRoot.level : 1,
                    to: tree.height
                });
                // TODO Try to combine setTreeValues & setColorRecursive to avoid
                //  unnecessary looping.
                setTreeValues(tree, {
                    before: cbSetTreeValuesBefore,
                    idRoot: rootId,
                    levelIsConstant: options.levelIsConstant,
                    mapOptionsToLevel: mapOptionsToLevel,
                    mapIdToNode: mapIdToNode,
                    points: series.points,
                    series: series
                });
                values = mapIdToNode[''].shapeArgs = {
                    end: radians.end,
                    r: innerRadius,
                    start: radians.start,
                    val: nodeRoot.val,
                    x: positions[0],
                    y: positions[1]
                };
                this.setShapeArgs(nodeTop, values, mapOptionsToLevel);
                // Set mapOptionsToLevel on series for use in drawPoints.
                series.mapOptionsToLevel = mapOptionsToLevel;
                // #10669 - verify if all nodes have unique ids
                series.data.forEach(function (child) {
                    if (nodeIds[child.id]) {
                        H.error(31, false, series.chart);
                    }
                    // map
                    nodeIds[child.id] = true;
                });
                // reset object
                nodeIds = {};
            },
            // Animate the slices in. Similar to the animation of polar charts.
            animate: function (init) {
                var chart = this.chart, center = [
                    chart.plotWidth / 2,
                    chart.plotHeight / 2
                ], plotLeft = chart.plotLeft, plotTop = chart.plotTop, attribs, group = this.group;
                // Initialize the animation
                if (init) {
                    // Scale down the group and place it in the center
                    attribs = {
                        translateX: center[0] + plotLeft,
                        translateY: center[1] + plotTop,
                        scaleX: 0.001,
                        scaleY: 0.001,
                        rotation: 10,
                        opacity: 0.01
                    };
                    group.attr(attribs);
                    // Run the animation
                }
                else {
                    attribs = {
                        translateX: plotLeft,
                        translateY: plotTop,
                        scaleX: 1,
                        scaleY: 1,
                        rotation: 0,
                        opacity: 1
                    };
                    group.animate(attribs, this.options.animation);
                    // Delete this function to allow it only once
                    this.animate = null;
                }
            },
            utils: {
                calculateLevelSizes: calculateLevelSizes,
                range: range
            }
        };
        // Properties of the Sunburst series.
        var sunburstPoint = {
            draw: drawPoint,
            shouldDraw: function shouldDraw() {
                return !this.isNull;
            },
            isValid: function isValid() {
                return true;
            }
        };
        
    
        seriesType('sunburst', 'treemap', sunburstOptions, sunburstSeries, sunburstPoint);

    });
    _registerModule(_modules, 'masters/modules/sunburst.src.js', [], function () {


    });
}));