/* *
 *
 *  (c) 2010-2019 Torstein Honsi
 *
 *  #
 *
 *  !!!!!!! SOURCE GETS TRANSPILED BY TYPESCRIPT. EDIT TS FILE ONLY. !!!!!!!
 *
 * */
'use strict';
import H from '../parts/Globals.js';
import U from '../parts/Utilities.js';
var defined = U.defined;
var noop = H.noop, seriesTypes = H.seriesTypes;
/**
 * Mixin for maps and heatmaps
 *
 * @private
 * @mixin Omniverse.colorMapPointMixin
 */
H.colorMapPointMixin = {
    dataLabelOnNull: true,
    /* eslint-disable valid-jsdoc */
    /**
     * Color points have a value option that determines whether or not it is
     * a null point
     * @private
     * @function Omniverse.colorMapPointMixin.isValid
     * @return {boolean}
     */
    isValid: function () {
        // undefined is allowed
        return (this.value !== null &&
            this.value !== Infinity &&
            this.value !== -Infinity);
    },
    /**
     * @private
     * @function Omniverse.colorMapPointMixin.setState
     * @param {string} state
     * @return {void}
     */
    setState: function (state) {
        H.Point.prototype.setState.call(this, state);
        if (this.graphic) {
            this.graphic.attr({
                zIndex: state === 'hover' ? 1 : 0
            });
        }
    }
    /* eslint-enable valid-jsdoc */
};
/**
 * @private
 * @mixin Omniverse.colorMapSeriesMixin
 */
H.colorMapSeriesMixin = {
    pointArrayMap: ['value'],
    axisTypes: ['xAxis', 'yAxis', 'colorAxis'],
    trackerGroups: ['group', 'markerGroup', 'dataLabelsGroup'],
    getSymbol: noop,
    parallelArrays: ['x', 'y', 'value'],
    colorKey: 'value',
    pointAttribs: seriesTypes.column.prototype.pointAttribs,
    /* eslint-disable valid-jsdoc */
    /**
     * Get the color attibutes to apply on the graphic
     * @private
     * @function Omniverse.colorMapSeriesMixin.colorAttribs
     * @param {Omniverse.Point} point
     * @return {Omniverse.SVGAttributes}
     */
    colorAttribs: function (point) {
        var ret = {};
        if (defined(point.color)) {
            ret[this.colorProp || 'fill'] = point.color;
        }
        return ret;
    }
};
