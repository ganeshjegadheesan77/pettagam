
'use strict';
import H from '../parts/Globals.js';
import U from '../parts/Utilities.js';
var defined = U.defined;
import '../parts/Series.js';
var Series = H.Series, addEvent = H.addEvent, noop = H.noop;

var derivedSeriesMixin = {
    hasDerivedData: true,
    
    init: function () {
        Series.prototype.init.apply(this, arguments);
        this.initialised = false;
        this.baseSeries = null;
        this.eventRemovers = [];
        this.addEvents();
    },
   
    setDerivedData: noop,
  
    setBaseSeries: function () {
        var chart = this.chart, baseSeriesOptions = this.options.baseSeries, baseSeries = (defined(baseSeriesOptions) &&
            (chart.series[baseSeriesOptions] ||
                chart.get(baseSeriesOptions)));
        this.baseSeries = baseSeries || null;
    },
  
    addEvents: function () {
        var derivedSeries = this, chartSeriesLinked;
        chartSeriesLinked = addEvent(this.chart, 'afterLinkSeries', function () {
            derivedSeries.setBaseSeries();
            if (derivedSeries.baseSeries && !derivedSeries.initialised) {
                derivedSeries.setDerivedData();
                derivedSeries.addBaseSeriesEvents();
                derivedSeries.initialised = true;
            }
        });
        this.eventRemovers.push(chartSeriesLinked);
    },
   
    addBaseSeriesEvents: function () {
        var derivedSeries = this, updatedDataRemover, destroyRemover;
        updatedDataRemover = addEvent(derivedSeries.baseSeries, 'updatedData', function () {
            derivedSeries.setDerivedData();
        });
        destroyRemover = addEvent(derivedSeries.baseSeries, 'destroy', function () {
            derivedSeries.baseSeries = null;
            derivedSeries.initialised = false;
        });
        derivedSeries.eventRemovers.push(updatedDataRemover, destroyRemover);
    },

    destroy: function () {
        this.eventRemovers.forEach(function (remover) {
            remover();
        });
        Series.prototype.destroy.apply(this, arguments);
    }
    
};
export default derivedSeriesMixin;
