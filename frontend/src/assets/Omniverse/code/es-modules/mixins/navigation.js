
'use strict';
var chartNavigation = {
  
    initUpdate: function (chart) {
        if (!chart.navigation) {
            chart.navigation = {
                updates: [],
                update: function (options, redraw) {
                    this.updates.forEach(function (updateConfig) {
                        updateConfig.update.call(updateConfig.context, options, redraw);
                    });
                }
            };
        }
    },

    addUpdate: function (update, chart) {
        if (!chart.navigation) {
            this.initUpdate(chart);
        }
        chart.navigation.updates.push({
            update: update,
            context: chart
        });
    }
};
export default chartNavigation;
