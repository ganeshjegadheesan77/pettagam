
'use strict';
import H from '../parts/Globals.js';
import U from '../parts/Utilities.js';
var objectEach = U.objectEach;


H.ajax = function (attr) {
    var options = H.merge(true, {
        url: false,
        type: 'get',
        dataType: 'json',
        success: false,
        error: false,
        data: false,
        headers: {}
    }, attr), headers = {
        json: 'application/json',
        xml: 'application/xml',
        text: 'text/plain',
        octet: 'application/octet-stream'
    }, r = new XMLHttpRequest();
   
    function handleError(xhr, err) {
        if (options.error) {
            options.error(xhr, err);
        }
        else {
            // @todo Maybe emit a Omniverse error event here
        }
    }
    if (!options.url) {
        return false;
    }
    r.open(options.type.toUpperCase(), options.url, true);
    if (!options.headers['Content-Type']) {
        r.setRequestHeader('Content-Type', headers[options.dataType] || headers.text);
    }
    objectEach(options.headers, function (val, key) {
        r.setRequestHeader(key, val);
    });
    // @todo lacking timeout handling
    r.onreadystatechange = function () {
        var res;
        if (r.readyState === 4) {
            if (r.status === 200) {
                res = r.responseText;
                if (options.dataType === 'json') {
                    try {
                        res = JSON.parse(res);
                    }
                    catch (e) {
                        return handleError(r, e);
                    }
                }
                return options.success && options.success(res);
            }
            handleError(r, r.responseText);
        }
    };
    try {
        options.data = JSON.stringify(options.data);
    }
    catch (e) {
        // empty
    }
    r.send(options.data || true);
};

H.getJSON = function (url, success) {
    H.ajax({
        url: url,
        success: success,
        dataType: 'json',
        headers: {
            // Override the Content-Type to avoid preflight problems with CORS
            // in the Omniverse demos
            'Content-Type': 'text/plain'
        }
    });
};
