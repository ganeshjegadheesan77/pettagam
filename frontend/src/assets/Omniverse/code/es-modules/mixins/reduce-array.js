
'use strict';
import H from '../parts/Globals.js';
import '../parts/Utilities.js';
var reduce = H.reduce;
var reduceArrayMixin = {
  
    minInArray: function (arr, index) {
        return reduce(arr, function (min, target) {
            return Math.min(min, target[index]);
        }, Number.MAX_VALUE);
    },
 
    maxInArray: function (arr, index) {
        return reduce(arr, function (max, target) {
            return Math.max(max, target[index]);
        }, -Number.MAX_VALUE);
    },
   
    getArrayExtremes: function (arr, minIndex, maxIndex) {
        return reduce(arr, function (prev, target) {
            return [
                Math.min(prev[0], target[minIndex]),
                Math.max(prev[1], target[maxIndex])
            ];
        }, [Number.MAX_VALUE, -Number.MAX_VALUE]);
    }
};
export default reduceArrayMixin;
