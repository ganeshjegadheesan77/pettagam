
'use strict';
import H from '../parts/Globals.js';
import '../parts/Utilities.js';
var error = H.error;
/* eslint-disable no-invalid-this, valid-jsdoc */
var requiredIndicatorMixin = {

    isParentLoaded: function (indicator, requiredIndicator, type, callback, errMessage) {
        if (indicator) {
            return callback ? callback(indicator) : true;
        }
        error(errMessage || this.generateMessage(type, requiredIndicator));
        return false;
    },
    generateMessage: function (indicatorType, required) {
        return 'Error: "' + indicatorType +
            '" indicator type requires "' + required +
            '" indicator loaded before. Please read docs: ' +
            'https://#' +
            indicatorType;
    }
};
export default requiredIndicatorMixin;
