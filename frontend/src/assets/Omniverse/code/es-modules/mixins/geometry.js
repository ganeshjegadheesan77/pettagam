
var getCenterOfPoints = function getCenterOfPoints(points) {
    var sum = points.reduce(function (sum, point) {
        sum.x += point.x;
        sum.y += point.y;
        return sum;
    }, { x: 0, y: 0 });
    return {
        x: sum.x / points.length,
        y: sum.y / points.length
    };
};

var getDistanceBetweenPoints = function getDistanceBetweenPoints(p1, p2) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
};

var getAngleBetweenPoints = function getAngleBetweenPoints(p1, p2) {
    return Math.atan2(p2.x - p1.x, p2.y - p1.y);
};
var geometry = {
    getAngleBetweenPoints: getAngleBetweenPoints,
    getCenterOfPoints: getCenterOfPoints,
    getDistanceBetweenPoints: getDistanceBetweenPoints
};
export default geometry;
