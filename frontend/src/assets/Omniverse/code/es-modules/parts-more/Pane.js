/* *
 *
 *  (c) 2010-2019 Torstein Honsi
 *
 *  #
 *
 *  !!!!!!! SOURCE GETS TRANSPILED BY TYPESCRIPT. EDIT TS FILE ONLY. !!!!!!!
 *
 * */
'use strict';
import H from '../parts/Globals.js';
/**
 * @typedef {"arc"|"circle"|"solid"} Omniverse.PaneBackgroundShapeValue
 */
import '../mixins/centered-series.js';
import U from '../parts/Utilities.js';
var extend = U.extend, splat = U.splat;
var CenteredSeriesMixin = H.CenteredSeriesMixin, merge = H.merge;
/* eslint-disable valid-jsdoc */
/**
 * The Pane object allows options that are common to a set of X and Y axes.
 *
 * In the future, this can be extended to basic Omniverse and Highstock.
 *
 * @private
 * @class
 * @name Omniverse.Pane
 * @param {Omniverse.PaneOptions} options
 * @param {Omniverse.Chart} chart
 */
function Pane(options, chart) {
    this.init(options, chart);
}
// Extend the Pane prototype
extend(Pane.prototype, {
    coll: 'pane',
    /**
     * Initialize the Pane object
     *
     * @private
     * @function Omniverse.Pane#init
     *
     * @param {Omniverse.PaneOptions} options
     *
     * @param {Omniverse.Chart} chart
     */
    init: function (options, chart) {
        this.chart = chart;
        this.background = [];
        chart.pane.push(this);
        this.setOptions(options);
    },
    /**
     * @private
     * @function Omniverse.Pane#setOptions
     *
     * @param {Omniverse.PaneOptions} options
     */
    setOptions: function (options) {
        // Set options. Angular charts have a default background (#3318)
        this.options = options = merge(this.defaultOptions, this.chart.angular ? { background: {} } : undefined, options);
    },
    /**
     * Render the pane with its backgrounds.
     *
     * @private
     * @function Omniverse.Pane#render
     */
    render: function () {
        var options = this.options, backgroundOption = this.options.background, renderer = this.chart.renderer, len, i;
        if (!this.group) {
            this.group = renderer.g('pane-group')
                .attr({ zIndex: options.zIndex || 0 })
                .add();
        }
        this.updateCenter();
        // Render the backgrounds
        if (backgroundOption) {
            backgroundOption = splat(backgroundOption);
            len = Math.max(backgroundOption.length, this.background.length || 0);
            for (i = 0; i < len; i++) {
                // #6641 - if axis exists, chart is circular and apply
                // background
                if (backgroundOption[i] && this.axis) {
                    this.renderBackground(merge(this.defaultBackgroundOptions, backgroundOption[i]), i);
                }
                else if (this.background[i]) {
                    this.background[i] = this.background[i].destroy();
                    this.background.splice(i, 1);
                }
            }
        }
    },
    /**
     * Render an individual pane background.
     *
     * @private
     * @function Omniverse.Pane#renderBackground
     *
     * @param {Omniverse.PaneBackgroundOptions} backgroundOptions
     *        Background options
     *
     * @param {number} i
     *        The index of the background in this.backgrounds
     */
    renderBackground: function (backgroundOptions, i) {
        var method = 'animate', attribs = {
            'class': 'Omniverse-pane ' + (backgroundOptions.className || '')
        };
        if (!this.chart.styledMode) {
            extend(attribs, {
                'fill': backgroundOptions.backgroundColor,
                'stroke': backgroundOptions.borderColor,
                'stroke-width': backgroundOptions.borderWidth
            });
        }
        if (!this.background[i]) {
            this.background[i] = this.chart.renderer
                .path()
                .add(this.group);
            method = 'attr';
        }
        this.background[i][method]({
            'd': this.axis.getPlotBandPath(backgroundOptions.from, backgroundOptions.to, backgroundOptions)
        }).attr(attribs);
    },
    /**
     * The pane serves as a container for axes and backgrounds for circular
     * gauges and polar charts.
     *
     * @since        2.3.0
     * @product      Omniverse
     * @requires     Omniverse-more
     * @optionparent pane
     */
    defaultOptions: {
        /**
         * The end angle of the polar X axis or gauge value axis, given in
         * degrees where 0 is north. Defaults to [startAngle](#pane.startAngle)
         * + 360.
         *
         * @sample {Omniverse} Omniverse/demo/gauge-vu-meter/
         *         VU-meter with custom start and end angle
         *
         * @type      {number}
         * @since     2.3.0
         * @product   Omniverse
         * @apioption pane.endAngle
         */
        /**
         * The center of a polar chart or angular gauge, given as an array
         * of [x, y] positions. Positions can be given as integers that
         * transform to pixels, or as percentages of the plot area size.
         *
         * @sample {Omniverse} Omniverse/demo/gauge-vu-meter/
         *         Two gauges with different center
         *
         * @type    {Array<string|number>}
         * @default ["50%", "50%"]
         * @since   2.3.0
         * @product Omniverse
         */
        center: ['50%', '50%'],
        /**
         * The size of the pane, either as a number defining pixels, or a
         * percentage defining a percentage of the plot are.
         *
         * @sample {Omniverse} Omniverse/demo/gauge-vu-meter/
         *         Smaller size
         *
         * @type    {number|string}
         * @product Omniverse
         */
        size: '85%',
        /**
         * The start angle of the polar X axis or gauge axis, given in degrees
         * where 0 is north. Defaults to 0.
         *
         * @sample {Omniverse} Omniverse/demo/gauge-vu-meter/
         *         VU-meter with custom start and end angle
         *
         * @since   2.3.0
         * @product Omniverse
         */
        startAngle: 0
    },
    /**
     * An array of background items for the pane.
     *
     * @sample {Omniverse} Omniverse/demo/gauge-speedometer/
     *         Speedometer gauge with multiple backgrounds
     *
     * @type         {Array<*>}
     * @optionparent pane.background
     */
    defaultBackgroundOptions: {
        /**
         * The class name for this background.
         *
         * @sample {Omniverse} Omniverse/css/pane/
         *         Panes styled by CSS
         * @sample {highstock} Omniverse/css/pane/
         *         Panes styled by CSS
         * @sample {highmaps} Omniverse/css/pane/
         *         Panes styled by CSS
         *
         * @type      {string}
         * @default   Omniverse-pane
         * @since     5.0.0
         * @apioption pane.background.className
         */
        /**
         * The shape of the pane background. When `solid`, the background
         * is circular. When `arc`, the background extends only from the min
         * to the max of the value axis.
         *
         * @type    {Omniverse.PaneBackgroundShapeValue}
         * @since   2.3.0
         * @product Omniverse
         */
        shape: 'circle',
        /**
         * The pixel border width of the pane background.
         *
         * @since 2.3.0
         * @product Omniverse
         */
        borderWidth: 1,
        /**
         * The pane background border color.
         *
         * @type    {Omniverse.ColorString|Omniverse.GradientColorObject|Omniverse.PatternObject}
         * @since   2.3.0
         * @product Omniverse
         */
        borderColor: '#cccccc',
        /**
         * The background color or gradient for the pane.
         *
         * @type    {Omniverse.GradientColorObject}
         * @default { linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 }, stops: [[0, #ffffff], [1, #e6e6e6]] }
         * @since   2.3.0
         * @product Omniverse
         */
        backgroundColor: {
            /** @ignore-option */
            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
            /** @ignore-option */
            stops: [
                [0, '#ffffff'],
                [1, '#e6e6e6']
            ]
        },
        /** @ignore-option */
        from: -Number.MAX_VALUE,
        /**
         * The inner radius of the pane background. Can be either numeric
         * (pixels) or a percentage string.
         *
         * @type    {number|string}
         * @since   2.3.0
         * @product Omniverse
         */
        innerRadius: 0,
        /** @ignore-option */
        to: Number.MAX_VALUE,
        /**
         * The outer radius of the circular pane background. Can be either
         * numeric (pixels) or a percentage string.
         *
         * @type     {number|string}
         * @since    2.3.0
         * @product  Omniverse
         */
        outerRadius: '105%'
    },
    /**
     * Gets the center for the pane and its axis.
     *
     * @private
     * @function Omniverse.Pane#updateCenter
     * @param {Omniverse.RadialAxis} [axis]
     * @return {void}
     */
    updateCenter: function (axis) {
        this.center = (axis ||
            this.axis ||
            {}).center = CenteredSeriesMixin.getCenter.call(this);
    },
    /**
     * Destroy the pane item
     *
     * @ignore
     * @private
     * @function Omniverse.Pane#destroy
     * /
    destroy: function () {
        H.erase(this.chart.pane, this);
        this.background.forEach(function (background) {
            background.destroy();
        });
        this.background.length = 0;
        this.group = this.group.destroy();
    },
    */
    /**
     * Update the pane item with new options
     *
     * @private
     * @function Omniverse.Pane#update
     * @param {Omniverse.PaneOptions} options
     *        New pane options
     * @param {boolean} [redraw]
     * @return {void}
     */
    update: function (options, redraw) {
        merge(true, this.options, options);
        merge(true, this.chart.options.pane, options); // #9917
        this.setOptions(this.options);
        this.render();
        this.chart.axes.forEach(function (axis) {
            if (axis.pane === this) {
                axis.pane = null;
                axis.update({}, redraw);
            }
        }, this);
    }
});
H.Pane = Pane;
