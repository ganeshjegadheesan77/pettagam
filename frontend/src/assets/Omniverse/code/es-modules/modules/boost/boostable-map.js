
import boostables from './boostables.js';
// These are the series we allow boosting for.
var boostableMap = {};
boostables.forEach(function (item) {
    boostableMap[item] = 1;
});
export default boostableMap;
