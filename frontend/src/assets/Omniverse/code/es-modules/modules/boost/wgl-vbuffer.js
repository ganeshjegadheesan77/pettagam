
'use strict';

function GLVertexBuffer(gl, shader, dataComponents
) {
    var buffer = false, vertAttribute = false, components = dataComponents || 2, preAllocated = false, iterator = 0, 
    // farray = false,
    data;
    // type = type || 'float';

    function destroy() {
        if (buffer) {
            gl.deleteBuffer(buffer);
            buffer = false;
            vertAttribute = false;
        }
        iterator = 0;
        components = dataComponents || 2;
        data = [];
    }
    
    function build(dataIn, attrib, dataComponents) {
        var farray;
        data = dataIn || [];
        if ((!data || data.length === 0) && !preAllocated) {
            // console.error('trying to render empty vbuffer');
            destroy();
            return false;
        }
        components = dataComponents || components;
        if (buffer) {
            gl.deleteBuffer(buffer);
        }
        if (!preAllocated) {
            farray = new Float32Array(data);
        }
        buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferData(gl.ARRAY_BUFFER, preAllocated || farray, gl.STATIC_DRAW);
        // gl.bindAttribLocation(shader.program(), 0, 'aVertexPosition');
        vertAttribute = gl.getAttribLocation(shader.program(), attrib);
        gl.enableVertexAttribArray(vertAttribute);
        // Trigger cleanup
        farray = false;
        return true;
    }
   
    function bind() {
        if (!buffer) {
            return false;
        }
        // gl.bindAttribLocation(shader.program(), 0, 'aVertexPosition');
        // gl.enableVertexAttribArray(vertAttribute);
        // gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.vertexAttribPointer(vertAttribute, components, gl.FLOAT, false, 0, 0);
        // gl.enableVertexAttribArray(vertAttribute);
    }
   
    function render(from, to, drawMode) {
        var length = preAllocated ? preAllocated.length : data.length;
        if (!buffer) {
            return false;
        }
        if (!length) {
            return false;
        }
        if (!from || from > length || from < 0) {
            from = 0;
        }
        if (!to || to > length) {
            to = length;
        }
        drawMode = drawMode || 'points';
        gl.drawArrays(gl[drawMode.toUpperCase()], from / components, (to - from) / components);
        return true;
    }
    
    function push(x, y, a, b) {
        if (preAllocated) { // && iterator <= preAllocated.length - 4) {
            preAllocated[++iterator] = x;
            preAllocated[++iterator] = y;
            preAllocated[++iterator] = a;
            preAllocated[++iterator] = b;
        }
    }
    
    function allocate(size) {
        size *= 4;
        iterator = -1;
        preAllocated = new Float32Array(size);
    }
    // /////////////////////////////////////////////////////////////////////////
    return {
        destroy: destroy,
        bind: bind,
        data: data,
        build: build,
        render: render,
        allocate: allocate,
        push: push
    };
}
export default GLVertexBuffer;
