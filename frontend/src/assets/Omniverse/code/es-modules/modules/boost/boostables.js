
// These are the series we allow boosting for.
var boostables = [
    'area',
    'arearange',
    'column',
    'columnrange',
    'bar',
    'line',
    'scatter',
    'heatmap',
    'bubble',
    'treemap'
];
export default boostables;
