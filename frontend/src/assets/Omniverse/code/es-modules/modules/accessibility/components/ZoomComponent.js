
'use strict';

import H from '../../../parts/Globals.js';
import U from '../../../parts/Utilities.js';
var extend = U.extend;

import AccessibilityComponent from '../AccessibilityComponent.js';
import KeyboardNavigationHandler from '../KeyboardNavigationHandler.js';

H.Axis.prototype.panStep = function (direction, granularity) {
    var gran = granularity || 3,
        extremes = this.getExtremes(),
        step = (extremes.max - extremes.min) / gran * direction,
        newMax = extremes.max + step,
        newMin = extremes.min + step,
        size = newMax - newMin;

    if (direction < 0 && newMin < extremes.dataMin) {
        newMin = extremes.dataMin;
        newMax = newMin + size;
    } else if (direction > 0 && newMax > extremes.dataMax) {
        newMax = extremes.dataMax;
        newMin = newMax - size;
    }
    this.setExtremes(newMin, newMax);
};

var ZoomComponent = function () {};
ZoomComponent.prototype = new AccessibilityComponent();
extend(ZoomComponent.prototype, {
    init: function () {
        var component = this,
            chart = this.chart;
        [
            'afterShowResetZoom', 'afterDrilldown', 'drillupall'
        ].forEach(function (eventType) {
            component.addEvent(chart, eventType, function () {
                component.updateProxyOverlays();
            });
        });
    },
    onChartUpdate: function () {
        var chart = this.chart,
            component = this;

        // Make map zoom buttons accessible
        if (chart.mapNavButtons) {
            chart.mapNavButtons.forEach(function (button, i) {
                component.unhideElementFromScreenReaders(button.element);
                button.element.setAttribute('tabindex', -1);
                button.element.setAttribute('role', 'button');
                button.element.setAttribute(
                    'aria-label',
                    chart.langFormat(
                        'accessibility.mapZoom' + (i ? 'Out' : 'In'),
                        { chart: chart }
                    )
                );
            });
        }
    },

    onChartRender: function () {
        this.updateProxyOverlays();
    },
    updateProxyOverlays: function () {
        var component = this,
            chart = this.chart,
            proxyButton = function (buttonEl, buttonProp, groupProp, label) {
                component.removeElement(component[groupProp]);
                component[groupProp] = component.addProxyGroup();
                component[buttonProp] = component.createProxyButton(
                    buttonEl,
                    component[groupProp],
                    {
                        'aria-label': label,
                        tabindex: -1
                    }
                );
            };

        // Always start with a clean slate
        component.removeElement(component.drillUpProxyGroup);
        component.removeElement(component.resetZoomProxyGroup);

        if (chart.resetZoomButton) {
            proxyButton(
                chart.resetZoomButton, 'resetZoomProxyButton',
                'resetZoomProxyGroup', chart.langFormat(
                    'accessibility.resetZoomButton',
                    { chart: chart }
                )
            );
        }

        if (chart.drillUpButton) {
            proxyButton(
                chart.drillUpButton, 'drillUpProxyButton',
                'drillUpProxyGroup', chart.langFormat(
                    'accessibility.drillUpButton',
                    {
                        chart: chart,
                        buttonText: chart.getDrilldownBackText()
                    }
                )
            );
        }
    },
    getMapZoomNavigation: function () {
        var keys = this.keyCodes,
            chart = this.chart,
            component = this;

        return new KeyboardNavigationHandler(chart, {
            keyCodeMap: [
                // Arrow keys
                [[
                    keys.up, keys.down, keys.left, keys.right
                ], function (keyCode) {
                    chart[
                        keyCode === keys.up || keyCode === keys.down ?
                            'yAxis' : 'xAxis'
                    ][0].panStep(
                        keyCode === keys.left || keyCode === keys.up ? -1 : 1
                    );
                    return this.response.success;
                }],

                // Tabs
                [[
                    keys.tab
                ], function (keyCode, e) {
                    var button;

                    // Deselect old
                    chart.mapNavButtons[
                        component.focusedMapNavButtonIx
                    ].setState(0);

                    // Trying to go somewhere we can't?
                    if (
                        e.shiftKey && !component.focusedMapNavButtonIx ||
                        !e.shiftKey && component.focusedMapNavButtonIx
                    ) {
                        chart.mapZoom(); // Reset zoom
                        // Nowhere to go, go to prev/next module
                        return this.response[e.shiftKey ? 'prev' : 'next'];
                    }

                    // Select other button
                    component.focusedMapNavButtonIx += e.shiftKey ? -1 : 1;
                    button = chart.mapNavButtons[
                        component.focusedMapNavButtonIx
                    ];
                    chart.setFocusToElement(button.box, button.element);
                    button.setState(2);

                    return this.response.success;
                }],

                // Press button
                [[
                    keys.space, keys.enter
                ], function () {
                    component.fakeClickEvent(
                        chart.mapNavButtons[
                            component.focusedMapNavButtonIx
                        ].element
                    );
                    return this.response.success;
                }]
            ],

            // Only run this module if we have map zoom on the chart
            validate: function () {
                return (
                    chart.mapZoom &&
                    chart.mapNavButtons &&
                    chart.mapNavButtons.length === 2
                );
            },

            // Make zoom buttons do their magic
            init: function (direction) {
                var zoomIn = chart.mapNavButtons[0],
                    zoomOut = chart.mapNavButtons[1],
                    initialButton = direction > 0 ? zoomIn : zoomOut;
                chart.setFocusToElement(
                    initialButton.box, initialButton.element
                );
                initialButton.setState(2);
                component.focusedMapNavButtonIx = direction > 0 ? 0 : 1;
            }
        });
    },
    simpleButtonNavigation: function (buttonProp, proxyProp, onClick) {
        var keys = this.keyCodes,
            component = this,
            chart = this.chart;

        return new KeyboardNavigationHandler(chart, {
            keyCodeMap: [
                // Arrow/tab just move
                [[
                    keys.tab, keys.up, keys.down, keys.left, keys.right
                ], function (keyCode, e) {
                    return this.response[
                        keyCode === this.tab && e.shiftKey ||
                        keyCode === keys.left || keyCode === keys.up ?
                            'prev' : 'next'
                    ];
                }],

                // Select to click
                [[
                    keys.space, keys.enter
                ], function () {
                    onClick(chart);
                    return this.response.success;
                }]
            ],

            // Only run if we have the button
            validate: function () {
                return chart[buttonProp] && chart[buttonProp].box &&
                    component[proxyProp];
            },

            // Focus button initially
            init: function () {
                chart.setFocusToElement(
                    chart[buttonProp].box, component[proxyProp]
                );
            }
        });
    },
    getKeyboardNavigation: function () {
        return [
            this.simpleButtonNavigation(
                'resetZoomButton',
                'resetZoomProxyButton',
                function (chart) {
                    chart.zoomOut();
                }
            ),
            this.simpleButtonNavigation(
                'drillUpButton',
                'drillUpProxyButton',
                function (chart) {
                    chart.drillUp();
                }
            ),
            this.getMapZoomNavigation()
        ];
    }

});

export default ZoomComponent;
