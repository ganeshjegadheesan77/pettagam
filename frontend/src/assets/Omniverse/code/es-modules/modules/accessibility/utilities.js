

'use strict';

function escapeStringForHTML(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#x27;')
        .replace(/\//g, '&#x2F;');
}

var Utilities = {

   
    stripHTMLTagsFromString: function (str) {
        return typeof str === 'string' ?
            str.replace(/<\/?[^>]+(>|$)/g, '') : str;
    },

    makeHTMLTagFromText: function (tag, text) {
        return '<' + tag + '>' + escapeStringForHTML(text) + '</' + tag + '>';
    },


    escapeStringForHTML: escapeStringForHTML

};

export default Utilities;
