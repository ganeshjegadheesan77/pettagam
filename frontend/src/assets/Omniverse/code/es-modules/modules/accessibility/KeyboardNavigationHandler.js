
'use strict';

import H from '../../parts/Globals.js';
var find = H.find;

function KeyboardNavigationHandler(chart, options) {
    this.chart = chart;
    this.keyCodeMap = options.keyCodeMap || [];
    this.validate = options.validate;
    this.init = options.init;
    this.terminate = options.terminate;
    // Response enum
    this.response = {
        success: 1, // Keycode was handled
        prev: 2, // Move to prev module
        next: 3, // Move to next module
        noHandler: 4, // There is no handler for this keycode
        fail: 5 // Handler failed
    };
}
KeyboardNavigationHandler.prototype = {
    run: function (e) {
        var keyCode = e.which || e.keyCode,
            response = this.response.noHandler,
            handlerCodeSet = find(this.keyCodeMap, function (codeSet) {
                return codeSet[0].indexOf(keyCode) > -1;
            });

        if (handlerCodeSet) {
            response = handlerCodeSet[1].call(this, keyCode, e);
        } else if (keyCode === 9) {
            // Default tab handler, move to next/prev module
            response = this.response[e.shiftKey ? 'prev' : 'next'];
        } else if (keyCode === 27) {
            // Default esc handler, hide tooltip
            if (this.chart && this.chart.tooltip) {
                this.chart.tooltip.hide(0);
            }
            response = this.response.success;
        }

        return response;
    }

};

export default KeyboardNavigationHandler;
