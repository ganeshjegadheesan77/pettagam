

'use strict';

import H from '../../parts/Globals.js';
import U from '../../parts/Utilities.js';
var pick = U.pick;

import utilities from './utilities.js';

// Defaults for the instrument options
// NOTE: Also change defaults in Omniverse.PointInstrumentOptionsObject if
//       making changes here.
var defaultInstrumentOptions = {
    minDuration: 20,
    maxDuration: 2000,
    minVolume: 0.1,
    maxVolume: 1,
    minPan: -1,
    maxPan: 1,
    minFrequency: 220,
    maxFrequency: 2200
};

function pointSonify(options) {
    var point = this,
        chart = point.series.chart,
        dataExtremes = options.dataExtremes || {},
        // Get the value to pass to instrument.play from the mapping value
        // passed in.
        getMappingValue = function (
            value, makeFunction, allowedExtremes, allowedValues
        ) {
            // Fixed number, just use that
            if (typeof value === 'number' || value === undefined) {
                return value;
            }
            // Function. Return new function if we try to use callback,
            // otherwise call it now and return result.
            if (typeof value === 'function') {
                return makeFunction ?
                    function (time) {
                        return value(point, dataExtremes, time);
                    } :
                    value(point, dataExtremes);
            }
            // String, this is a data prop.
            if (typeof value === 'string') {
                // Find data extremes if we don't have them
                dataExtremes[value] = dataExtremes[value] ||
                    utilities.calculateDataExtremes(
                        point.series.chart, value
                    );
                // Find the value
                return utilities.virtualAxisTranslate(
                    pick(point[value], point.options[value]),
                    dataExtremes[value],
                    allowedExtremes,
                    allowedValues
                );
            }
        };

    // Register playing point on chart
    chart.sonification.currentlyPlayingPoint = point;

    // Keep track of instruments playing
    point.sonification = point.sonification || {};
    point.sonification.instrumentsPlaying =
        point.sonification.instrumentsPlaying || {};

    // Register signal handler for the point
    var signalHandler = point.sonification.signalHandler =
        point.sonification.signalHandler ||
        new utilities.SignalHandler(['onEnd']);

    signalHandler.clearSignalCallbacks();
    signalHandler.registerSignalCallbacks({ onEnd: options.onEnd });

    // If we have a null point or invisible point, just return
    if (point.isNull || !point.visible || !point.series.visible) {
        signalHandler.emitSignal('onEnd');
        return;
    }

    // Go through instruments and play them
    options.instruments.forEach(function (instrumentDefinition) {
        var instrument = typeof instrumentDefinition.instrument === 'string' ?
                H.sonification.instruments[instrumentDefinition.instrument] :
                instrumentDefinition.instrument,
            mapping = instrumentDefinition.instrumentMapping || {},
            extremes = H.merge(
                defaultInstrumentOptions,
                instrumentDefinition.instrumentOptions
            ),
            id = instrument.id,
            onEnd = function (cancelled) {
                // Instrument on end
                if (instrumentDefinition.onEnd) {
                    instrumentDefinition.onEnd.apply(this, arguments);
                }

                // Remove currently playing point reference on chart
                if (
                    chart.sonification &&
                    chart.sonification.currentlyPlayingPoint
                ) {
                    delete chart.sonification.currentlyPlayingPoint;
                }

                // Remove reference from instruments playing
                if (
                    point.sonification && point.sonification.instrumentsPlaying
                ) {
                    delete point.sonification.instrumentsPlaying[id];

                    // This was the last instrument?
                    if (
                        !Object.keys(
                            point.sonification.instrumentsPlaying
                        ).length
                    ) {
                        signalHandler.emitSignal('onEnd', cancelled);
                    }
                }
            };

        // Play the note on the instrument
        if (instrument && instrument.play) {
            point.sonification.instrumentsPlaying[instrument.id] = instrument;
            instrument.play({
                frequency: getMappingValue(
                    mapping.frequency,
                    true,
                    { min: extremes.minFrequency, max: extremes.maxFrequency }
                ),
                duration: getMappingValue(
                    mapping.duration,
                    false,
                    { min: extremes.minDuration, max: extremes.maxDuration }
                ),
                pan: getMappingValue(
                    mapping.pan,
                    true,
                    { min: extremes.minPan, max: extremes.maxPan }
                ),
                volume: getMappingValue(
                    mapping.volume,
                    true,
                    { min: extremes.minVolume, max: extremes.maxVolume }
                ),
                onEnd: onEnd,
                minFrequency: extremes.minFrequency,
                maxFrequency: extremes.maxFrequency
            });
        } else {
            H.error(30);
        }
    });
}

function pointCancelSonify(fadeOut) {
    var playing = this.sonification && this.sonification.instrumentsPlaying,
        instrIds = playing && Object.keys(playing);

    if (instrIds && instrIds.length) {
        instrIds.forEach(function (instr) {
            playing[instr].stop(!fadeOut, null, 'cancelled');
        });
        this.sonification.instrumentsPlaying = {};
        this.sonification.signalHandler.emitSignal('onEnd', 'cancelled');
    }
}


var pointSonifyFunctions = {
    pointSonify: pointSonify,
    pointCancelSonify: pointCancelSonify
};

export default pointSonifyFunctions;
