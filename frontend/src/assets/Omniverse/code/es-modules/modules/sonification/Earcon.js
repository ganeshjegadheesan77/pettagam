
'use strict';

import H from '../../parts/Globals.js';
import U from '../../parts/Utilities.js';
var pick = U.pick;

function Earcon(options) {
    this.init(options || {});
}
Earcon.prototype.init = function (options) {
    this.options = options;
    if (!this.options.id) {
        this.options.id = this.id = H.uniqueKey();
    }
    this.instrumentsPlaying = {};
};



Earcon.prototype.sonify = function (options) {
    var playOptions = H.merge(this.options, options);

    // Find master volume/pan settings
    var masterVolume = pick(playOptions.volume, 1),
        masterPan = playOptions.pan,
        earcon = this,
        playOnEnd = options && options.onEnd,
        masterOnEnd = earcon.options.onEnd;

    // Go through the instruments and play them
    playOptions.instruments.forEach(function (opts) {
        var instrument = typeof opts.instrument === 'string' ?
                H.sonification.instruments[opts.instrument] : opts.instrument,
            instrumentOpts = H.merge(opts.playOptions),
            instrOnEnd,
            instrumentCopy,
            copyId;

        if (instrument && instrument.play) {
            if (opts.playOptions) {
                // Handle master pan/volume
                if (typeof opts.playOptions.volume !== 'function') {
                    instrumentOpts.volume = pick(masterVolume, 1) *
                        pick(opts.playOptions.volume, 1);
                }
                instrumentOpts.pan = pick(masterPan, instrumentOpts.pan);

                // Handle onEnd
                instrOnEnd = instrumentOpts.onEnd;
                instrumentOpts.onEnd = function () {
                    delete earcon.instrumentsPlaying[copyId];
                    if (instrOnEnd) {
                        instrOnEnd.apply(this, arguments);
                    }
                    if (!Object.keys(earcon.instrumentsPlaying).length) {
                        if (playOnEnd) {
                            playOnEnd.apply(this, arguments);
                        }
                        if (masterOnEnd) {
                            masterOnEnd.apply(this, arguments);
                        }
                    }
                };

                // Play the instrument. Use a copy so we can play multiple at
                // the same time.
                instrumentCopy = instrument.copy();
                copyId = instrumentCopy.id;
                earcon.instrumentsPlaying[copyId] = instrumentCopy;
                instrumentCopy.play(instrumentOpts);
            }
        } else {
            H.error(30);
        }
    });
};

Earcon.prototype.cancelSonify = function (fadeOut) {
    var playing = this.instrumentsPlaying,
        instrIds = playing && Object.keys(playing);

    if (instrIds && instrIds.length) {
        instrIds.forEach(function (instr) {
            playing[instr].stop(!fadeOut, null, 'cancelled');
        });
        this.instrumentsPlaying = {};
    }
};


export default Earcon;
