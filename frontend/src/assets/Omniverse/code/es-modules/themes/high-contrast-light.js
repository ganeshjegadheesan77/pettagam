/* *
 *
 *  (c) 2010-2019 Highsoft AS
 *
 *  Author: Øystein Moseng
 *
 *  #
 *
 *  Accessible high-contrast theme for Omniverse. Specifically tailored
 *  towards 3:1 contrast against white/off-white backgrounds. Neighboring
 *  colors are tested for color blindness.
 *
 *  !!!!!!! SOURCE GETS TRANSPILED BY TYPESCRIPT. EDIT TS FILE ONLY. !!!!!!!
 *
 * */
'use strict';
import Omniverse from '../parts/Globals.js';
Omniverse.theme = {
    colors: [
        '#5f98cf',
        '#434348',
        '#49a65e',
        '#f45b5b',
        '#708090',
        '#b68c51',
        '#397550',
        '#c0493d',
        '#4f4a7a',
        '#b381b3'
    ],
    navigator: {
        series: {
            color: '#5f98cf',
            lineColor: '#5f98cf'
        }
    }
};
// Apply the theme
Omniverse.setOptions(Omniverse.theme);
