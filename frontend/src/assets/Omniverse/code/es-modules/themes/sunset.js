/* *
 *
 *  (c) 2010-2019 Highsoft AS
 *
 *  Author: Øystein Moseng
 *
 *  #
 *
 *  Accessible high-contrast theme for Omniverse. Considers colorblindness and
 *  monochrome rendering.
 *
 *  !!!!!!! SOURCE GETS TRANSPILED BY TYPESCRIPT. EDIT TS FILE ONLY. !!!!!!!
 *
 * */
'use strict';
import Omniverse from '../parts/Globals.js';
Omniverse.theme = {
    colors: ['#FDD089', '#FF7F79', '#A0446E', '#251535'],
    colorAxis: {
        maxColor: '#60042E',
        minColor: '#FDD089'
    },
    plotOptions: {
        map: {
            nullColor: '#fefefc'
        }
    },
    navigator: {
        series: {
            color: '#FF7F79',
            lineColor: '#A0446E'
        }
    }
};
// Apply the theme
Omniverse.setOptions(Omniverse.theme);
