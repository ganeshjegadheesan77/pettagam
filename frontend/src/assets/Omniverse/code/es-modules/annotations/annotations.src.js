
'use strict';

import H from '../parts/Globals.js';

import U from '../parts/Utilities.js';
var defined = U.defined,
    destroyObjectProperties = U.destroyObjectProperties,
    erase = U.erase,
    extend = U.extend,
    pick = U.pick,
    splat = U.splat;

import '../parts/Chart.js';
import controllableMixin from './controllable/controllableMixin.js';
import ControllableRect from './controllable/ControllableRect.js';
import ControllableCircle from './controllable/ControllableCircle.js';
import ControllablePath from './controllable/ControllablePath.js';
import ControllableImage from './controllable/ControllableImage.js';
import ControllableLabel from './controllable/ControllableLabel.js';
import eventEmitterMixin from './eventEmitterMixin.js';
import MockPoint from './MockPoint.js';
import ControlPoint from './ControlPoint.js';

var merge = H.merge,
    addEvent = H.addEvent,
    fireEvent = H.fireEvent,
    find = H.find,
    reduce = H.reduce,
    chartProto = H.Chart.prototype;


var Annotation = H.Annotation = function (chart, userOptions) {
    var labelsAndShapes;


    this.chart = chart;


    this.points = [];


    this.controlPoints = [];

    this.coll = 'annotations';


    this.labels = [];


    this.shapes = [];


    this.options = merge(this.defaultOptions, userOptions);


    this.userOptions = userOptions;

    // Handle labels and shapes - those are arrays
    // Merging does not work with arrays (stores reference)
    labelsAndShapes = this.getLabelsAndShapesOptions(
        this.options,
        userOptions
    );
    this.options.labels = labelsAndShapes.labels;
    this.options.shapes = labelsAndShapes.shapes;

  

    this.init(chart, this.options);
};


merge(
    true,
    Annotation.prototype,
    controllableMixin,
    eventEmitterMixin,
    /** @lends Annotation# */
    {

        
        nonDOMEvents: ['add', 'afterUpdate', 'drag', 'remove'],

        
        defaultOptions: {

          
            visible: true,

         
            draggable: 'xy',

            
            labelOptions: {

           
                align: 'center',

            
                allowOverlap: false,

              
                backgroundColor: 'rgba(0, 0, 0, 0.75)',

           
                borderColor: 'black',

           
                borderRadius: 3,

            
                borderWidth: 1,

               
                className: '',

            
                crop: false,


                formatter: function () {
                    return defined(this.y) ? this.y : 'Annotation label';
                },

                overflow: 'justify',


                padding: 5,


                shadow: false,


                shape: 'callout',

                style: {
                    /** @ignore */
                    fontSize: '11px',
                    /** @ignore */
                    fontWeight: 'normal',
                    /** @ignore */
                    color: 'contrast'
                },


                useHTML: false,


                verticalAlign: 'bottom',


                x: 0,


                y: -16
            },





            shapeOptions: {

                
                stroke: 'rgba(0, 0, 0, 0.75)',

                strokeWidth: 1,


                fill: 'rgba(0, 0, 0, 0.75)',


                r: 0,

 
                snap: 2
            },


            controlPointOptions: {



                symbol: 'circle',
                width: 10,
                height: 10,
                style: {
                    stroke: 'black',
                    'stroke-width': 2,
                    fill: 'white'
                },
                visible: false,
                events: {}
            },

   
            events: {},

   
            zIndex: 6

        },


        init: function () {
            this.linkPoints();
            this.addControlPoints();
            this.addShapes();
            this.addLabels();
            this.addClipPaths();
            this.setLabelCollector();
        },

        getLabelsAndShapesOptions: function (baseOptions, newOptions) {
            var mergedOptions = {};

            ['labels', 'shapes'].forEach(function (name) {
                if (baseOptions[name]) {
                    mergedOptions[name] = splat(newOptions[name]).map(
                        function (basicOptions, i) {
                            return merge(baseOptions[name][i], basicOptions);
                        }
                    );
                }
            });

            return mergedOptions;
        },

        addShapes: function () {
            (this.options.shapes || []).forEach(function (shapeOptions, i) {
                var shape = this.initShape(shapeOptions, i);

                merge(true, this.options.shapes[i], shape.options);
            }, this);
        },

        addLabels: function () {
            (this.options.labels || []).forEach(function (labelsOptions, i) {
                var labels = this.initLabel(labelsOptions, i);

                merge(true, this.options.labels[i], labels.options);
            }, this);
        },

        addClipPaths: function () {
            this.setClipAxes();

            if (this.clipXAxis && this.clipYAxis) {
                this.clipRect = this.chart.renderer.clipRect(
                    this.getClipBox()
                );
            }
        },

        setClipAxes: function () {
            var xAxes = this.chart.xAxis,
                yAxes = this.chart.yAxis,
                linkedAxes = reduce(
                    (this.options.labels || [])
                        .concat(this.options.shapes || []),
                    function (axes, labelOrShape) {
                        return [
                            xAxes[
                                labelOrShape &&
                                labelOrShape.point &&
                                labelOrShape.point.xAxis
                            ] || axes[0],
                            yAxes[
                                labelOrShape &&
                                labelOrShape.point &&
                                labelOrShape.point.yAxis
                            ] || axes[1]
                        ];
                    },
                    []
                );

            this.clipXAxis = linkedAxes[0];
            this.clipYAxis = linkedAxes[1];
        },

        getClipBox: function () {
            return {
                x: this.clipXAxis.left,
                y: this.clipYAxis.top,
                width: this.clipXAxis.width,
                height: this.clipYAxis.height
            };
        },

        setLabelCollector: function () {
            var annotation = this;

            annotation.labelCollector = function () {
                return annotation.labels.reduce(
                    function (labels, label) {
                        if (!label.options.allowOverlap) {
                            labels.push(label.graphic);
                        }

                        return labels;
                    },
                    []
                );
            };

            annotation.chart.labelCollectors.push(
                annotation.labelCollector
            );
        },

 
        setOptions: function (userOptions) {
            this.options = merge(this.defaultOptions, userOptions);
        },

        redraw: function (animation) {
            this.linkPoints();

            if (!this.graphic) {
                this.render();
            }

            if (this.clipRect) {
                this.clipRect.animate(this.getClipBox());
            }

            this.redrawItems(this.shapes, animation);
            this.redrawItems(this.labels, animation);


            controllableMixin.redraw.call(this, animation);
        },

 
        redrawItems: function (items, animation) {
            var i = items.length;

            // needs a backward loop
            // labels/shapes array might be modified
            // due to destruction of the item
            while (i--) {
                this.redrawItem(items[i], animation);
            }
        },

        render: function () {
            var renderer = this.chart.renderer;

            this.graphic = renderer
                .g('annotation')
                .attr({
                    zIndex: this.options.zIndex,
                    visibility: this.options.visible ?
                        'visible' :
                        'hidden'
                })
                .add();

            this.shapesGroup = renderer
                .g('annotation-shapes')
                .add(this.graphic)
                .clip(this.chart.plotBoxClip);

            this.labelsGroup = renderer
                .g('annotation-labels')
                .attr({
                    // hideOverlappingLabels requires translation
                    translateX: 0,
                    translateY: 0
                })
                .add(this.graphic);

            if (this.clipRect) {
                this.graphic.clip(this.clipRect);
            }

            this.addEvents();

            controllableMixin.render.call(this);
        },

     
        setVisibility: function (visibility) {
            var options = this.options,
                visible = pick(visibility, !options.visible);

            this.graphic.attr(
                'visibility',
                visible ? 'visible' : 'hidden'
            );

            if (!visible) {
                this.setControlPointsVisibility(false);
            }

            options.visible = visible;
        },

        setControlPointsVisibility: function (visible) {
            var setItemControlPointsVisibility = function (item) {
                item.setControlPointsVisibility(visible);
            };

            controllableMixin.setControlPointsVisibility.call(
                this,
                visible
            );

            this.shapes.forEach(setItemControlPointsVisibility);
            this.labels.forEach(setItemControlPointsVisibility);
        },

        destroy: function () {
            var chart = this.chart,
                destroyItem = function (item) {
                    item.destroy();
                };

            this.labels.forEach(destroyItem);
            this.shapes.forEach(destroyItem);

            this.clipXAxis = null;
            this.clipYAxis = null;

            erase(chart.labelCollectors, this.labelCollector);

            eventEmitterMixin.destroy.call(this);
            controllableMixin.destroy.call(this);

            destroyObjectProperties(this, chart);
        },

  
        remove: function () {
            // Let chart.update() remove annoations on demand
            return this.chart.removeAnnotation(this);
        },

        update: function (userOptions) {
            var chart = this.chart,
                labelsAndShapes = this.getLabelsAndShapesOptions(
                    this.userOptions,
                    userOptions
                ),
                userOptionsIndex = chart.annotations.indexOf(this),
                options = H.merge(true, this.userOptions, userOptions);

            options.labels = labelsAndShapes.labels;
            options.shapes = labelsAndShapes.shapes;

            this.destroy();
            this.constructor(chart, options);

            // Update options in chart options, used in exporting (#9767):
            chart.options.annotations[userOptionsIndex] = options;

            this.isUpdating = true;
            this.redraw();
            this.isUpdating = false;
            fireEvent(this, 'afterUpdate');
        },

        
        initShape: function (shapeOptions, index) {
            var options = merge(
                    this.options.shapeOptions,
                    {
                        controlPointOptions: this.options.controlPointOptions
                    },
                    shapeOptions
                ),
                shape = new Annotation.shapesMap[options.type](
                    this,
                    options,
                    index
                );

            shape.itemType = 'shape';

            this.shapes.push(shape);

            return shape;
        },

  
        initLabel: function (labelOptions, index) {
            var options = merge(
                    this.options.labelOptions,
                    {
                        controlPointOptions: this.options.controlPointOptions
                    },
                    labelOptions
                ),
                label = new ControllableLabel(
                    this,
                    options,
                    index
                );

            label.itemType = 'label';

            this.labels.push(label);

            return label;
        },

      
        redrawItem: function (item, animation) {
            item.linkPoints();

            if (!item.shouldBeDrawn()) {
                this.destroyItem(item);
            } else {
                if (!item.graphic) {
                    this.renderItem(item);
                }

                item.redraw(
                    pick(animation, true) && item.graphic.placed
                );

                if (item.points.length) {
                    this.adjustVisibility(item);
                }
            }
        },

        

        adjustVisibility: function (item) { // #9481
            var hasVisiblePoints = false,
                label = item.graphic;

            item.points.forEach(function (point) {
                if (
                    point.series.visible !== false &&
                    point.visible !== false
                ) {
                    hasVisiblePoints = true;
                }
            });

            if (!hasVisiblePoints) {
                label.hide();

            } else if (label.visibility === 'hidden') {
                label.show();
            }
        },

       
        destroyItem: function (item) {
            // erase from shapes or labels array
            erase(this[item.itemType + 's'], item);
            item.destroy();
        },

     
        renderItem: function (item) {
            item.render(
                item.itemType === 'label' ?
                    this.labelsGroup :
                    this.shapesGroup
            );
        }
    }
);


Annotation.shapesMap = {
    'rect': ControllableRect,
    'circle': ControllableCircle,
    'path': ControllablePath,
    'image': ControllableImage
};

Annotation.types = {};

Annotation.MockPoint = MockPoint;
Annotation.ControlPoint = ControlPoint;

H.extendAnnotation = function (
    Constructor,
    BaseConstructor,
    prototype,
    defaultOptions
) {
    BaseConstructor = BaseConstructor || Annotation;

    merge(
        true,
        Constructor.prototype,
        BaseConstructor.prototype,
        prototype
    );

    Constructor.prototype.defaultOptions = merge(
        Constructor.prototype.defaultOptions,
        defaultOptions || {}
    );
};


extend(chartProto,  {
    initAnnotation: function (userOptions) {
        var Constructor =
            Annotation.types[userOptions.type] || Annotation,
            annotation = new Constructor(this, userOptions);

        this.annotations.push(annotation);

        return annotation;
    },

    
    addAnnotation: function (userOptions, redraw) {
        var annotation = this.initAnnotation(userOptions);

        this.options.annotations.push(annotation.options);

        if (pick(redraw, true)) {
            annotation.redraw();
        }

        return annotation;
    },


    removeAnnotation: function (idOrAnnotation) {
        var annotations = this.annotations,
            annotation = idOrAnnotation.coll === 'annotations' ?
                idOrAnnotation :
                find(
                    annotations,
                    function (annotation) {
                        return annotation.options.id === idOrAnnotation;
                    }
                );

        if (annotation) {
            fireEvent(annotation, 'remove');
            erase(this.options.annotations, annotation.options);
            erase(annotations, annotation);
            annotation.destroy();
        }
    },

    drawAnnotations: function () {
        this.plotBoxClip.attr(this.plotBox);

        this.annotations.forEach(function (annotation) {
            annotation.redraw();
        });
    }
});

// Let chart.update() update annotations
chartProto.collectionsWithUpdate.push('annotations');

// Let chart.update() create annoations on demand
chartProto.collectionsWithInit.annotations = [chartProto.addAnnotation];

chartProto.callbacks.push(function (chart) {
    chart.annotations = [];

    if (!chart.options.annotations) {
        chart.options.annotations = [];
    }

    chart.plotBoxClip = this.renderer.clipRect(this.plotBox);

    chart.controlPointsGroup = chart.renderer
        .g('control-points')
        .attr({ zIndex: 99 })
        .clip(chart.plotBoxClip)
        .add();

    chart.options.annotations.forEach(function (annotationOptions, i) {
        var annotation = chart.initAnnotation(annotationOptions);

        chart.options.annotations[i] = annotation.options;
    });

    chart.drawAnnotations();
    addEvent(chart, 'redraw', chart.drawAnnotations);
    addEvent(chart, 'destroy', function () {
        chart.plotBoxClip.destroy();
        chart.controlPointsGroup.destroy();
    });
});

H.wrap(
    H.Pointer.prototype,
    'onContainerMouseDown',
    function (proceed) {
        if (!this.chart.hasDraggedAnnotation) {
            proceed.apply(this, Array.prototype.slice.call(arguments, 1));
        }
    }
);
