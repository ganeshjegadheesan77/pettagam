import H from '../parts/Globals.js';

import U from '../parts/Utilities.js';
var defined = U.defined,
    extend = U.extend;

import '../parts/Axis.js';
import '../parts/Series.js';



function MockPoint(chart, target, options) {
 
    this.series = {
        visible: true,
        chart: chart,
        getPlotBox: H.Series.prototype.getPlotBox
    };
    this.target = target || null;
    this.options = options;

    this.applyOptions(this.getOptions());
}


MockPoint.fromPoint = function (point) {
    return new MockPoint(point.series.chart, null, {
        x: point.x,
        y: point.y,
        xAxis: point.series.xAxis,
        yAxis: point.series.yAxis
    });
};


MockPoint.pointToPixels = function (point, paneCoordinates) {
    var series = point.series,
        chart = series.chart,
        x = point.plotX,
        y = point.plotY,
        plotBox;

    if (chart.inverted) {
        if (point.mock) {
            x = point.plotY;
            y = point.plotX;
        } else {
            x = chart.plotWidth - point.plotY;
            y = chart.plotHeight - point.plotX;
        }
    }

    if (series && !paneCoordinates) {
        plotBox = series.getPlotBox();
        x += plotBox.translateX;
        y += plotBox.translateY;
    }

    return {
        x: x,
        y: y
    };
};


MockPoint.pointToOptions = function (point) {
    return {
        x: point.x,
        y: point.y,
        xAxis: point.series.xAxis,
        yAxis: point.series.yAxis
    };
};

extend(MockPoint.prototype,  {
    
    mock: true,
    hasDynamicOptions: function () {
        return typeof this.options === 'function';
    },
    getOptions: function () {
        return this.hasDynamicOptions() ?
            this.options(this.target) :
            this.options;
    },
    applyOptions: function (options) {
        this.command = options.command;

        this.setAxis(options, 'x');
        this.setAxis(options, 'y');

        this.refresh();
    },
    setAxis: function (options, xOrY) {
        var axisName = xOrY + 'Axis',
            axisOptions = options[axisName],
            chart = this.series.chart;

        this.series[axisName] =
            axisOptions instanceof H.Axis ?
                axisOptions :
                defined(axisOptions) ?
                    chart[axisName][axisOptions] || chart.get(axisOptions) :
                    null;
    },
    toAnchor: function () {
        var anchor = [this.plotX, this.plotY, 0, 0];

        if (this.series.chart.inverted) {
            anchor[0] = this.plotY;
            anchor[1] = this.plotX;
        }

        return anchor;
    },
    getLabelConfig: function () {
        return {
            x: this.x,
            y: this.y,
            point: this
        };
    },
    isInsidePane: function () {
        var plotX = this.plotX,
            plotY = this.plotY,
            xAxis = this.series.xAxis,
            yAxis = this.series.yAxis,
            isInside = true;

        if (xAxis) {
            isInside = defined(plotX) && plotX >= 0 && plotX <= xAxis.len;
        }

        if (yAxis) {
            isInside =
                isInside &&
                defined(plotY) &&
                plotY >= 0 && plotY <= yAxis.len;
        }

        return isInside;
    },
    refresh: function () {
        var series = this.series,
            xAxis = series.xAxis,
            yAxis = series.yAxis,
            options = this.getOptions();

        if (xAxis) {
            this.x = options.x;
            this.plotX = xAxis.toPixels(options.x, true);
        } else {
            this.x = null;
            this.plotX = options.x;
        }

        if (yAxis) {
            this.y = options.y;
            this.plotY = yAxis.toPixels(options.y, true);
        } else {
            this.y = null;
            this.plotY = options.y;
        }

        this.isInside = this.isInsidePane();
    },


    translate: function (cx, cy, dx, dy) {
        if (!this.hasDynamicOptions()) {
            this.plotX += dx;
            this.plotY += dy;

            this.refreshOptions();
        }
    },

    scale: function (cx, cy, sx, sy) {
        if (!this.hasDynamicOptions()) {
            var x = this.plotX * sx,
                y = this.plotY * sy,
                tx = (1 - sx) * cx,
                ty = (1 - sy) * cy;

            this.plotX = tx + x;
            this.plotY = ty + y;

            this.refreshOptions();
        }
    },

    rotate: function (cx, cy, radians) {
        if (!this.hasDynamicOptions()) {
            var cos = Math.cos(radians),
                sin = Math.sin(radians),
                x = this.plotX,
                y = this.plotY,
                tx,
                ty;

            x -= cx;
            y -= cy;

            tx = x * cos - y * sin;
            ty = x * sin + y * cos;

            this.plotX = tx + cx;
            this.plotY = ty + cy;

            this.refreshOptions();
        }
    },

    refreshOptions: function () {
        var series = this.series,
            xAxis = series.xAxis,
            yAxis = series.yAxis;

        this.x = this.options.x = xAxis ?
            this.options.x = xAxis.toValue(this.plotX, true) :
            this.plotX;

        this.y = this.options.y = yAxis ?
            yAxis.toValue(this.plotY, true) :
            this.plotY;
    }
});

export default MockPoint;
