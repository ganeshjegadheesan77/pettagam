import H from './../parts/Globals.js';
import U from './../parts/Utilities.js';
var extend = U.extend,
    pick = U.pick;

import eventEmitterMixin from './eventEmitterMixin.js';

function ControlPoint(chart, target, options, index) {
    this.chart = chart;
    this.target = target;
    this.options = options;
    this.index = pick(options.index, index);
}


extend(
    ControlPoint.prototype,
    eventEmitterMixin
);


ControlPoint.prototype.nonDOMEvents = ['drag'];


ControlPoint.prototype.setVisibility = function (visible) {
    this.graphic.attr('visibility', visible ? 'visible' : 'hidden');

    this.options.visible = visible;
};


ControlPoint.prototype.render = function () {
    var chart = this.chart,
        options = this.options;

    this.graphic = chart.renderer
        .symbol(
            options.symbol,
            0,
            0,
            options.width,
            options.height
        )
        .add(chart.controlPointsGroup)
        .css(options.style);

    this.setVisibility(options.visible);
    this.addEvents();
};


ControlPoint.prototype.redraw = function (animation) {
    this.graphic[animation ? 'animate' : 'attr'](
        this.options.positioner.call(this, this.target)
    );
};



ControlPoint.prototype.destroy = function () {
    eventEmitterMixin.destroy.call(this);

    if (this.graphic) {
        this.graphic = this.graphic.destroy();
    }

    this.chart = null;
    this.target = null;
    this.options = null;
};


ControlPoint.prototype.update = function (userOptions) {
    var chart = this.chart,
        target = this.target,
        index = this.index,
        options = H.merge(true, this.options, userOptions);

    this.destroy();
    this.constructor(chart, target, options, index);
    this.render(chart.controlPointsGroup);
    this.redraw();
};

export default ControlPoint;
