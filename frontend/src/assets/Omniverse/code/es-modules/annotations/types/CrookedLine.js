'use strict';
import H from '../../parts/Globals.js';
import '../../parts/Utilities.js';

var Annotation = H.Annotation,
    MockPoint = Annotation.MockPoint,
    ControlPoint = Annotation.ControlPoint;

function CrookedLine() {
    Annotation.apply(this, arguments);
}

H.extendAnnotation(
    CrookedLine,
    null,
    {
        
        setClipAxes: function () {
            this.clipXAxis = this.chart.xAxis[this.options.typeOptions.xAxis];
            this.clipYAxis = this.chart.yAxis[this.options.typeOptions.yAxis];
        },
        getPointsOptions: function () {
            var typeOptions = this.options.typeOptions;

            return typeOptions.points.map(function (pointOptions) {
                pointOptions.xAxis = typeOptions.xAxis;
                pointOptions.yAxis = typeOptions.yAxis;

                return pointOptions;
            });
        },

        getControlPointsOptions: function () {
            return this.getPointsOptions();
        },

        addControlPoints: function () {
            this.getControlPointsOptions().forEach(
                function (pointOptions, i) {
                    var controlPoint = new ControlPoint(
                        this.chart,
                        this,
                        H.merge(
                            this.options.controlPointOptions,
                            pointOptions.controlPoint
                        ),
                        i
                    );

                    this.controlPoints.push(controlPoint);

                    pointOptions.controlPoint = controlPoint.options;
                },
                this
            );
        },

        addShapes: function () {
            var typeOptions = this.options.typeOptions,
                shape = this.initShape(
                    H.merge(typeOptions.line, {
                        type: 'path',
                        points: this.points.map(function (point, i) {
                            return function (target) {
                                return target.annotation.points[i];
                            };
                        })
                    }),
                    false
                );

            typeOptions.line = shape.options;
        }
    },

   
    {

       
        typeOptions: {
            xAxis: 0, 
            yAxis: 0,
            line: {
                fill: 'none'
            }
        },
        controlPointOptions: {
            positioner: function (target) {
                var graphic = this.graphic,
                    xy = MockPoint.pointToPixels(target.points[this.index]);

                return {
                    x: xy.x - graphic.width / 2,
                    y: xy.y - graphic.height / 2
                };
            },

            events: {
                drag: function (e, target) {
                    if (
                        target.chart.isInsidePlot(
                            e.chartX - target.chart.plotLeft,
                            e.chartY - target.chart.plotTop
                        )
                    ) {
                        var translation = this.mouseMoveToTranslation(e);

                        target.translatePoint(
                            translation.x,
                            translation.y,
                            this.index
                        );

                        // Update options:
                        target.options.typeOptions.points[this.index].x =
                            target.points[this.index].x;
                        target.options.typeOptions.points[this.index].y =
                            target.points[this.index].y;

                        target.redraw(false);
                    }
                }
            }
        }
    }
);

Annotation.types.crookedLine = CrookedLine;

export default CrookedLine;
