import H from '../../parts/Globals.js';
import '../../parts/Utilities.js';
import controllableMixin from './controllableMixin.js';
import ControllablePath from './ControllablePath.js';


function ControllableRect(annotation, options, index) {
    this.init(annotation, options, index);
    this.collection = 'shapes';
}


ControllableRect.attrsMap = H.merge(ControllablePath.attrsMap, {
    width: 'width',
    height: 'height'
});

H.merge(
    true,
    ControllableRect.prototype,
    controllableMixin,  {
       
        type: 'rect',

        translate: controllableMixin.translateShape,

        render: function (parent) {
            var attrs = this.attrsFromOptions(this.options);

            this.graphic = this.annotation.chart.renderer
                .rect(0, -9e9, 0, 0)
                .attr(attrs)
                .add(parent);

            controllableMixin.render.call(this);
        },

        redraw: function (animation) {
            var position = this.anchor(this.points[0]).absolutePosition;

            if (position) {
                this.graphic[animation ? 'animate' : 'attr']({
                    x: position.x,
                    y: position.y,
                    width: this.options.width,
                    height: this.options.height
                });
            } else {
                this.attr({
                    x: 0,
                    y: -9e9
                });
            }

            this.graphic.placed = Boolean(position);

            controllableMixin.redraw.call(this, animation);
        }
    }
);

export default ControllableRect;
