'use strict';
import H from './../../parts/Globals.js';

import U from './../../parts/Utilities.js';
var isObject = U.isObject,
    isString = U.isString,
    splat = U.splat;

import './../../parts/Tooltip.js';
import ControlPoint from './../ControlPoint.js';
import MockPoint from './../MockPoint.js';


var controllableMixin = {

    init: function (annotation, options, index) {
        this.annotation = annotation;
        this.chart = annotation.chart;
        this.options = options;
        this.points = [];
        this.controlPoints = [];
        this.index = index;

        this.linkPoints();
        this.addControlPoints();
    },


    attr: function () {
        this.graphic.attr.apply(this.graphic, arguments);
    },



    getPointsOptions: function () {
        var options = this.options;

        return options.points || (options.point && splat(options.point));
    },


    attrsFromOptions: function (options) {
        var map = this.constructor.attrsMap,
            attrs = {},
            key,
            mappedKey,
            styledMode = this.chart.styledMode;

        for (key in options) {
            mappedKey = map[key];

            if (
                mappedKey &&
                (
                    !styledMode ||
                    ['fill', 'stroke', 'stroke-width']
                        .indexOf(mappedKey) === -1
                )
            ) {
                attrs[mappedKey] = options[key];
            }
        }

        return attrs;
    },

   
    anchor: function (point) {
        var plotBox = point.series.getPlotBox(),

            box = point.mock ?
                point.toAnchor() :
                H.Tooltip.prototype.getAnchor.call({
                    chart: point.series.chart
                }, point),

            anchor = {
                x: box[0] + (this.options.x || 0),
                y: box[1] + (this.options.y || 0),
                height: box[2] || 0,
                width: box[3] || 0
            };

        return {
            relativePosition: anchor,
            absolutePosition: H.merge(anchor, {
                x: anchor.x + plotBox.translateX,
                y: anchor.y + plotBox.translateY
            })
        };
    },

   
    point: function (pointOptions, point) {
        if (pointOptions && pointOptions.series) {
            return pointOptions;
        }

        if (!point || point.series === null) {
            if (isObject(pointOptions)) {
                point = new MockPoint(
                    this.chart,
                    this,
                    pointOptions
                );
            } else if (isString(pointOptions)) {
                point = this.chart.get(pointOptions) || null;
            } else if (typeof pointOptions === 'function') {
                var pointConfig = pointOptions.call(point, this);

                point = pointConfig.series ?
                    pointConfig :
                    new MockPoint(
                        this.chart,
                        this,
                        pointOptions
                    );
            }
        }

        return point;
    },

   
    linkPoints: function () {
        var pointsOptions = this.getPointsOptions(),
            points = this.points,
            len = (pointsOptions && pointsOptions.length) || 0,
            i,
            point;

        for (i = 0; i < len; i++) {
            point = this.point(pointsOptions[i], points[i]);

            if (!point) {
                points.length = 0;

                return;
            }

            if (point.mock) {
                point.refresh();
            }

            points[i] = point;
        }

        return points;
    },


    addControlPoints: function () {
        var controlPointsOptions = this.options.controlPoints;

        (controlPointsOptions || []).forEach(
            function (controlPointOptions, i) {
                var options = H.merge(
                    this.options.controlPointOptions,
                    controlPointOptions
                );

                if (!options.index) {
                    options.index = i;
                }

                controlPointsOptions[i] = options;

                this.controlPoints.push(
                    new ControlPoint(this.chart, this, options)
                );
            },
            this
        );
    },

   
    shouldBeDrawn: function () {
        return Boolean(this.points.length);
    },

   
    render: function () {
        this.controlPoints.forEach(function (controlPoint) {
            controlPoint.render();
        });
    },

  
    redraw: function (animation) {
        this.controlPoints.forEach(function (controlPoint) {
            controlPoint.redraw(animation);
        });
    },


    transform: function (transformation, cx, cy, p1, p2) {
        if (this.chart.inverted) {
            var temp = cx;

            cx = cy;
            cy = temp;
        }

        this.points.forEach(function (point, i) {
            this.transformPoint(transformation, cx, cy, p1, p2, i);
        }, this);
    },

   
    transformPoint: function (transformation, cx, cy, p1, p2, i) {
        var point = this.points[i];

        if (!point.mock) {
            point = this.points[i] = MockPoint.fromPoint(point);
        }

        point[transformation](cx, cy, p1, p2);
    },

   
    translate: function (dx, dy) {
        this.transform('translate', null, null, dx, dy);
    },

   
    translatePoint: function (dx, dy, i) {
        this.transformPoint('translate', null, null, dx, dy, i);
    },

  
    translateShape: function (dx, dy) {
        var chart = this.annotation.chart,
            // Annotation.options
            shapeOptions = this.annotation.userOptions,
            // Chart.options.annotations
            annotationIndex = chart.annotations.indexOf(this.annotation),
            chartOptions = chart.options.annotations[annotationIndex];

        this.translatePoint(dx, dy, 0);

        // Options stored in:
        // - chart (for exporting)
        // - current config (for redraws)
        chartOptions[this.collection][this.index].point = this.options.point;
        shapeOptions[this.collection][this.index].point = this.options.point;
    },


    rotate: function (cx, cy, radians) {
        this.transform('rotate', cx, cy, radians);
    },

  
    scale: function (cx, cy, sx, sy) {
        this.transform('scale', cx, cy, sx, sy);
    },

  
    setControlPointsVisibility: function (visible) {
        this.controlPoints.forEach(function (controlPoint) {
            controlPoint.setVisibility(visible);
        });
    },

    destroy: function () {
        if (this.graphic) {
            this.graphic = this.graphic.destroy();
        }

        if (this.tracker) {
            this.tracker = this.tracker.destroy();
        }

        this.controlPoints.forEach(function (controlPoint) {
            controlPoint.destroy();
        });

        this.chart = null;
        this.points = null;
        this.controlPoints = null;
        this.options = null;

        if (this.annotation) {
            this.annotation = null;
        }
    },


    update: function (newOptions) {
        var annotation = this.annotation,
            options = H.merge(true, this.options, newOptions),
            parentGroup = this.graphic.parentGroup;

        this.destroy();
        this.constructor(annotation, options);
        this.render(parentGroup);
        this.redraw();
    }
};

export default controllableMixin;
