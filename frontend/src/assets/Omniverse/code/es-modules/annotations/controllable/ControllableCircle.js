'use strict';
import H from './../../parts/Globals.js';
import './../../parts/Utilities.js';
import controllableMixin from './controllableMixin.js';
import ControllablePath from './ControllablePath.js';


function ControllableCircle(annotation, options, index) {
    this.init(annotation, options, index);
    this.collection = 'shapes';
}


ControllableCircle.attrsMap = H.merge(ControllablePath.attrsMap, {
    r: 'r'
});

H.merge(
    true,
    ControllableCircle.prototype,
    controllableMixin,  {

        type: 'circle',

        translate: controllableMixin.translateShape,

        render: function (parent) {
            var attrs = this.attrsFromOptions(this.options);

            this.graphic = this.annotation.chart.renderer
                .circle(0, -9e9, 0)
                .attr(attrs)
                .add(parent);

            controllableMixin.render.call(this);
        },

        redraw: function (animation) {
            var position = this.anchor(this.points[0]).absolutePosition;

            if (position) {
                this.graphic[animation ? 'animate' : 'attr']({
                    x: position.x,
                    y: position.y,
                    r: this.options.r
                });
            } else {
                this.graphic.attr({
                    x: 0,
                    y: -9e9
                });
            }

            this.graphic.placed = Boolean(position);

            controllableMixin.redraw.call(this, animation);
        },

 
        setRadius: function (r) {
            this.options.r = r;
        }
    }
);

export default ControllableCircle;
