var data1;
let colors0 = [];
let colors1 = ["#f9d02d", "#9eb335", "#db6736"];
let colors2 = ["#664da0", "#5bcbfe", "#39a8d9", "#5895ff", "#0267f4"];
let colors3 = [
  "#7cb5ec",
  "#f7a35c",
  "#90ee7e",
  "#7798BF",
  "#aaeeee",
  "#ff0066",
  "#eeaaee",
  "#55BF3B",
  "#DF5353",
  "#7798BF",
  "#aaeeee",
];
async function fetchUniverseData() {
  let res = await fetch(location.origin + "/assets/config.json");
  let urls = await res.json();
  console.log("res urls", urls);
  // console.log(location.href);

  let response = await fetch(
    urls.baseUrl + `auditUniverse/getUniverseData`, { headers: new Headers({ 'authorization': JSON.parse(localStorage.getItem('token')) }) }
  );
  await console.log("Response ::", response);

  var data = await response.json();
  //console.log(data);
  return data;
}
async function drawUniverse() {
  var data = await fetchUniverseData();
  // await console.log(data);
  // console.log(dummyData);
  Omniverse.getOptions().colors.splice(0, 0, "transparent");

  await Omniverse.chart("container", {
    colors: colors2,
    chart: {
      height: "100%",
      events: {
        load: function () {
          setTimeout(function () {
            let element = document.getElementsByTagName("title");
            for (index = element.length - 1; index >= 0; index--) {
              element[index].parentNode.removeChild(element[index]);
            }
          }, 100);
        },
      },
    },
    title: {
      text: null,
    },

    series: [
      {
        type: "sunburst",
        data: data,
        allowDrillToNode: true,
        cursor: "pointer",
        dataLabels: {
          format: "{point.name}",
          filter: {
            property: "innerArcLength",
            operator: ">",
            value: 16,
          },
        },
        levels: [
          {
            level: 1,
            levelIsConstant: false,
            dataLabels: {
              filter: {
                property: "outerArcLength",
                operator: ">",
                value: 64,
              },
            },
          },
          {
            level: 2,
            colorByPoint: true,
          },
          {
            level: 3,
            colorVariation: {
              key: "brightness",
              to: -0.5,
            },
          },
        ],
      },
    ],
    tooltip: {
      headerFormat: "",
      pointFormat: "<h4>{point.name}</h4>",
    },
  });

  setTimeout(() => {
    document.title = "EnIA: Audit Universe";
  }, 1500);

}