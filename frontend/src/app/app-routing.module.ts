import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardLayoutComponent } from './layout/dashboard-layout/dashboard-layout.component';
import { LoginComponent } from './featured/authentication/components/login/login.component';
import { AuditeeChatRoomComponent } from './featured/authentication/components/auditee-chat-room/auditee-chat-room.component';
import { ProjectElementsComponent } from './layout/project-elements/project-elements.component';
import { NotificationService } from './notification.service'
import { AuthGuard } from './core/guards/auth.guard';
import { AuditeePbcFormComponent } from './featured/authentication/components/auditee-pbc-form/auditee-pbc-form.component';
const routes: Routes = [
  // {
  //   path: '',
  //   component: AuthLayoutComponent,
  //   children: [
  //     // {
  //     //   path: '',
  //     //   loadChildren: () => import('./featured/landing/').then(m => m.LandingModule)
  //     // },
  //     {
  //      path: 'login',
  //       loadChildren: () => import('./featured/authentication/authentication.module').then(m => m.AuthenticationModule)
  //     }
  //   ]
  // },
  {
    path: "", redirectTo: "login", pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent, data: { title:"EnIA: Login"}
  },
  {
    path: "auditee-chatroom",
    component: AuditeeChatRoomComponent, data: { title:"EnIA: Auditee Chat Room"}
  },
  {
    path: "auditee-pbc-form",
    component: AuditeePbcFormComponent, data: { title:"EnIA: Auditee PBC Form"}
  },
  {
    path: 'rcm',
    component: DashboardLayoutComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'agile-audit-space', loadChildren: () => import('./featured/admin/admin.module').then(m => m.AdminModule) },
      { path: 'admin/company-structure', loadChildren: () => import('./featured/admin/components/company-structure/company-structure.module').then(m => m.CompanyStructureModule) },
      { path: 'internal-audit', loadChildren: () => import('./featured/internal-audit/internal-audit.module').then(m => m.InternalAuditModule) },
      { path: 'risk-and-control-management', loadChildren: () => import('./featured/risk-and-control-management/risk-and-control-management.module').then(m => m.RiskAndControlManagementModule) },
      { path: 'risk-assessment', loadChildren: () => import('./featured/risk-assessment/risk-assessment.module').then(m => m.RiskAssessmentModule) },
      { path: 'control-testing', loadChildren: () => import('./featured/control-testing/control-testing.module').then(m => m.ControlTestingModule) },
      { path: 'case-management', loadChildren: () => import('./featured/case-management/case-management.module').then(m => m.CaseManagementModule), data: { action: 'action' } },
      { path: 'risk-library', loadChildren: () => import('./featured/risk-library/risk-library.module').then(m => m.RiskLibraryModule) },
      { path: 'dashboard', loadChildren: () => import('./featured/dashboard/dashboard.module').then(m => m.DashboardModule) },
      { path: 'project-element', component: ProjectElementsComponent,data:{title:"Project Elements"} },
      { path: 'new-form-control', loadChildren: () => import('./featured/new-form/new-control-form/new-control-form.module').then(m => m.NewControlFormModule) },
      { path: 'new-form-risk', loadChildren: () => import('./featured/new-form/new-risk-form/new-risk-form.module').then(m => m.NewRiskFormModule) }
    ],
    canActivate: [AuthGuard]
  },


  {
    path: '**',
    pathMatch: 'full',
    loadChildren: () => import('./featured/error/error.module').then(m => m.ErrorModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [NotificationService]
})
export class AppRoutingModule {

}
