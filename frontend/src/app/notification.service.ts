import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
@Injectable()
export class NotificationService{
    private actionDetail = new BehaviorSubject<any>([]);
    currentmessage= this.actionDetail.asObservable();
    
    constructor(){}
    changeMessage(dataObject:any){
        this.actionDetail.next(dataObject)
    }

}