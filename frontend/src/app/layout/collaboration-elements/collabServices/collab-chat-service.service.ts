import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import io from "socket.io-client";
import { HttpHeaders } from "@angular/common/http";
import { ConfigService } from 'src/config.service';

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
  }),
};

@Injectable({
  providedIn: "root",
})
export class CollabChatServiceService {
  private socket;
  private socketUrl;

  constructor(private configService:ConfigService) {
    this.socketUrl = configService.config.socket;
    this.socket = io(this.socketUrl);
  }
  private subject = new Subject<any>();
  sendAuditId(auditId) {
    // this.disconnectSocket();
    // this.connectSocket();
    this.subject.next(auditId);
  }
  getAuditId(): Observable<any> {
    return this.subject.asObservable();
  }

  getCollabChatRoom(auditId) {
    this.socket.emit("getCollabChatRoom", auditId, httpOptions);
  }
  //user online
  markUserOnline(onlineData) {
    this.socket.emit("userIsOnline", onlineData, httpOptions);
  }

  getUserOnline() {
    const observable = new Observable<{ data: string }>((observer) => {
      this.socket.on("userOnline", (data) => {
        console.log("FE user data rcvd:", data);
        observer.next(data);
      });
    });
    return observable;
  }

  userOnlineStatus(user) {
    this.socket.emit("setUserOnline", user, httpOptions);
  }

  roomDataFetched() {
    const observable = new Observable<{ data: string }>((observer) => {
      this.socket.on("getCollabChatRoom", (data) => {
        console.log("getCollabChatRoom -> " + JSON.stringify(data));
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  // message
  putAuditChatroomMessage(msg) {
    this.socket.emit("putAuditChatroomMessage", msg, httpOptions);
  }

  getAuditChatroomMessage() {
    const observable = new Observable<{ data: string }>((observer) => {
      this.socket.on("putAuditChatroomMessage", (data) => {
        observer.next(data);
      });
    });
    return observable;
  }

  setUserJoinedMessage(joinedMessage) {
    console.log("userJoined:", joinedMessage);
    this.socket.emit("setUserJoinedMessage", joinedMessage, httpOptions);
  }

  //get all room messages
  getAllAuditChatroomMessages(auditId) {
    this.socket.emit("getAllAuditChatroomMessages", auditId, httpOptions);
  }

  gotAllAuditChatroomMessages() {
    const observable = new Observable<{ data: string }>((observer) => {
      this.socket.on("getAllAuditChatroomMessages", (data) => {
        observer.next(data);
      });
    });
    return observable;
  }
  //get all host names from Users collection
  readHostList() {
    let observable = new Observable<[]>((observer) => {
      this.socket.on("readHostList", (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getIntrayFiles(Obj) {
    this.socket.emit("getIntrayFiles", Obj, httpOptions);
  }

  gotIntrayFiles() {
    let observable = new Observable((observer) => {
      this.socket.on("gotIntrayFiles", (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  connectSocket() {
    this.socket = io(this.socketUrl);    
  }

  disconnectSocket() {
    if (this.socket) {
      this.socket.disconnect();
    }
  }
}
