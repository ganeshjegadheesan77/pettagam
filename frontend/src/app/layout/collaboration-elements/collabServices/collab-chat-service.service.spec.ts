import { TestBed } from '@angular/core/testing';

import { CollabChatServiceService } from './collab-chat-service.service';

describe('CollabChatServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CollabChatServiceService = TestBed.get(CollabChatServiceService);
    expect(service).toBeTruthy();
  });
});
