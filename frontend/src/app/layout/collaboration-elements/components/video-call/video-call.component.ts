import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'src/app/core/data-services/local-storage.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-call',
  templateUrl: './video-call.component.html',
  styleUrls: ['./video-call.component.less']
})
export class VideoCallComponent implements OnInit {
  disableTextbox = false;
  constructor(private http: HttpClient, private storageService: LocalStorageService, private sanitizer: DomSanitizer, private router: Router) { }
  public sessionProgress = false;
  loadVideo = false;
  videoUrl;
  username = '';
  sessionName = '';
  disableControls = false;
  @Input() auditId;
  @Input() windowType;
  @Input() auditeeMailId;

  ngOnInit() {
    this.username = localStorage.getItem('username');

    console.log("@@ video call init");

    this.sessionName = 'Auditor-' + this.auditId;

    if (this.windowType === 'auditee') {
      this.sessionName = 'Auditee-' + this.auditId;
    }
    if (this.router.url.split("?")[0] == "/auditee-chatroom") {
      this.disableControls = true;
    }
    if (this.auditeeMailId) {
      this.username = this.auditeeMailId;
    }
  }

  joinSession() {
    // var sessionName = (<HTMLInputElement>document.getElementById('sessionName')).value;
    this.loadVideo = true;
    
    var name = this.username.includes("@") ? this.username.split("@")[0] : this.username;
    
    if(name.length > 10){
     name = name.slice(0,10);
    }
    var url = 'https://115.112.185.58:8080/web/?sessionName=' + this.sessionName + '&user=' + name + '&auditId=' + this.auditId;
    var dmsToken = localStorage.getItem('dmsToken')
    if (dmsToken) {
      url += '&dmsSessionId=' + dmsToken;
    }
    if (this.windowType == 'auditee' && this.router.url.split("?")[0] == '/auditee-chatroom') {
      url += '&windowType=auditee'
    }
    else {
      url += '&windowType=auditor'
    }
    window.open(url, '_blank', 'location=yes,scrollbars=yes,status=yes');
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url + this.sessionName + '&user=' + this.username)
    this.disableTextbox = true;
    this.sessionProgress = true;
  }
  clickHere() {
    this.loadVideo = false;
    this.disableTextbox = false;
    this.sessionProgress = false;
  }

  copySessionId(sessionId) {
    sessionId.select();
    document.execCommand('copy');
    sessionId.setSelectionRange(0, 0);
  }
}
