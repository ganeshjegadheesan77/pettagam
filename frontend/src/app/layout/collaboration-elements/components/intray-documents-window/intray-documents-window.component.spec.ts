import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrayDocumentsWindowComponent } from './intray-documents-window.component';

describe('IntrayDocumentsWindowComponent', () => {
  let component: IntrayDocumentsWindowComponent;
  let fixture: ComponentFixture<IntrayDocumentsWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntrayDocumentsWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrayDocumentsWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
