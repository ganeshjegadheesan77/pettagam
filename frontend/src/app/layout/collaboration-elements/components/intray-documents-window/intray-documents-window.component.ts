import {
  Component,
  OnInit,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  Input,
  ViewChild,
  ElementRef} from "@angular/core";
// import { ChatroomService } from "../chat-system/services/chatroom.service";
import { InTrayService } from "../in-tray/services/in-tray.service";
import { HttpService } from "../../../../core/http-services/http.service";
// import { FilePreviewComponent } from "../in-tray/file-preview/file-preview.component";
import { BsModalService } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { environment } from "src/environments/environment";

import * as moment from "moment";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
// import { IDropdownSettings } from "ng-multiselect-dropdown";
import { ConstUrls } from "src/app/config/const-urls";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { CollabChatServiceService } from "../../collabServices/collab-chat-service.service";
import { Router } from "@angular/router";
import  { v4 as uuidv4 } from "uuid";
import { ConfigService } from 'src/config.service';
@Component({
  selector: "app-intray-documents-window",
  templateUrl: "./intray-documents-window.component.html",
  styleUrls: ["./intray-documents-window.component.less"],
})
export class IntrayDocumentsWindowComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input("auditId") _auditId;
  @Input() roomName;
  @ViewChild('takeInput', { static: false })

  NewChatRoomValidateFlag: string = "";

  uri;
  // dmsBaseUrl = environment.dmsBaseUrl;
  dmsBaseUrl
  rooms: any = [];
  room: any;
  files: any;
  modalRef: any;
  roomTitle: string = "";
  roomId: any;
  owner: any;
  initialState: any;
  title: string;
  fileCountFlag: boolean;
  previewMode: boolean;
  fullPath: string;
  isFileOpen: boolean = false;
  modalRef3: any;
  public saveasWorkPaper = false;
  public renameDoc = false;
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  searchshow: boolean = false;
  status: boolean = false;
  successMsg = false;
  show = true;
  intrayFileList = [];

  disableSearch = false;
  globalSearchData = "";
  searchData = "";
  documentNo = "";
  workPaperNumber: "";
  auditId: any;
  auditName = "";
  arr = [];
  myfileName;
  subscription: any;
  inputFile = ''

  currentUser = "";
  renameFileName: any;
  renameFileExtension: any;
  comments: string;
  docNo: any;
  InputVar: ElementRef;
  oldFileName: any;
  chkpayload: any;
  checkInBy: any;
  userType: any;
  searchForAuditor: boolean;

  constructor(
    public inTrayService: InTrayService,
    // public chatroomService: ChatroomService,
    public httpService: HttpService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private collabChatServiceService: CollabChatServiceService,
    public router: Router,
    private configService:ConfigService

  ) {
    this.uri = configService.config.baseUrl;

    // this.chatroomService.getValue().subscribe((data) => {
    //   var id = data;
    //   if (id == "") {
    //     this.owner = localStorage.getItem("username");
    //     this.chatroomService.getRoomsData(this.owner);
    //     this.getConstructorData();
    //   } else {
    //     this.openRoom(id);
    //   }
    // });
    // this.collabChatServiceService
    //   .getAuditId()
    //   .subscribe((auditId) => {
    //     this.auditId = auditId;
    //     this.getAllDmsDocs();
    //   })

    //REMOVE 30 minute auto-dms-session function
    // if( this.router.url.split("?")[0] == '/rcm/agile-audit-space' && environment.activateDmsSessionAlternativeMethod){
    //   setInterval(()=> {
    //     console.log('5 minutes completed...')
    //     this.generateDmsToken();
    //     },environment.setDmsSessionTime);
    // }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.auditId = this._auditId;
    console.log('**********DMS TOKEN' ,localStorage.getItem("dmsToken"));
    //close dms search function for auditee
    console.log('route: ',this.router.url.split("?")[0])
    if(this.router.url.split("?")[0] == '/auditee-chatroom'){
      this.searchForAuditor = false;
    }else{
      this.searchForAuditor = true;

    }
    this.getAllDmsDocs();
  }
  generateDmsToken(){
    let payload = {username:localStorage.getItem("username")};
    this.httpService.post(ConstUrls.generateDmsSessionForAuditor ,payload).subscribe((res:any)=>{
      if(res){
        console.log('res', res)
        if(localStorage.getItem("dmsToken")){
          localStorage.removeItem('dmsToken')
          
          localStorage.setItem('dmsToken',res.dmsSession)
        }else{
          localStorage.setItem('dmsToken',res.dmsSession)
        }
      }
    })
  }
  ngOnDestroy() {
    if (this.isFileOpen) {
      let roomName = this.title;
      this.inTrayService.updateFileStatus({
        roomId: this.room._id,
        name: roomName,
        isOpen: false,
      });
    }
    if (this.previewMode) {
      let payload = {
        checkedinDate: moment().format("L"),
        dmsSessionId: localStorage.getItem("dmsToken"),
        documentNo: this.documentNo,
        comments: `document number ${
          this.documentNo
          } has been checked in by ${localStorage.getItem("username")}`,
      };
      this.httpService
        .post(ConstUrls.checkInDocument, payload)
        .subscribe((x) => { });
    }
  }

  ngOnInit() {
    this.currentUser = localStorage.getItem("username");
    this.mScrollbarService.initScrollbar(".multiSearchFilters", {
      axis: "y",
      theme: "metro",
    });
    console.log("@@@@@@@@@@@ audit id", this.auditId);
    console.log("roomname", this.roomName);

    // this.getAllDmsDocs();
    // this.collabChatServiceService.getIntrayFiles(this.auditId);
    // this.generateDmsToken();
    setTimeout(
      function () {
        this.show = false;
      }.bind(this),
      6000
    );
    this.pbcDoc();
    console.log("token", localStorage.getItem("dmsToken"));
    this.dropdownListAll = [];

    this.dropdownFirmList;
    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };

    window.onbeforeunload = (ev) => {
      if (this.isFileOpen) {
        let roomName = this.title;
        this.inTrayService.updateFileStatus({
          roomId: this.room._id,
          name: roomName,
          isOpen: false,
        });
      }
      if (this.previewMode) {
        let payload = {
          checkedinDate: moment().format("L"),
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: this.documentNo,
          comments: `document number ${
            this.documentNo
            } has been checked in by ${localStorage.getItem("username")}`,
        };
        this.httpService
          .post(ConstUrls.checkInDocument, payload)
          .subscribe((x) => { });
      }
    };
    // this.chatroomService.gotRoomsData().subscribe((data) => {
    //   this.rooms = data || [];
    //   this.room = data[0];
    //   this.roomId = this.room._id;
    //   this.files = this.room.fileName;
    //   this.fileCountFlag = this.files.length ? true : false;
    // });
  }

  pbcDoc() {
    for (let i = 0; i < 6; i++) {
      if (i % 2 == 0) {
        this.dropdownList.push({
          documentNo: i,
          fileName: "Enrico_Master_template(" + i + ").xlsx",
        });
      } else {
        this.dropdownList.push({
          documentNo: i,
          fileName: "Enrico_Master_template(" + i + ").docx",
        });
      }
    }
  }

  // openRoom(id) {
  //   this.chatroomService.getChatRoomDetails(id);
  //   this.chatroomService.gotChatRoomDetails().subscribe((data) => {
  //     this.room = data[0];
  //     this.files = this.room.fileName;
  //     this.fileCountFlag = this.files.length ? true : false;
  //   });
  // }
  // async getConstructorData() {
  //   await this.chatroomService.gotRoomsData().subscribe((data) => {
  //     if (data) {
  //       this.rooms = data || [];
  //       this.room = data[0];
  //       this.roomId = this.room._id;
  //     }
  //   });
  // }
  openSaveAsWP(fileObj) {
    // this.isFileOpen = true;
    // this.NewChatRoomValidateFlag = 'ValidatorToken';
    if (this.previewMode) {
      this.myCommonAlert("Success", "Please close existing open file first.");
      return;
    }
    this.previewMode = false;
    this.title = fileObj.fileName;
    this.documentNo = fileObj.documentNo;
    this.fullPath = `${this.configService.config.dmsBaseUrl}?dmsSession=${localStorage.getItem(
      "dmsToken"
    )}&urlToken=${fileObj.urlToken}`; //this.uri + 'uploads/inTray/' + fileObj.name;
    this.workPaperNumber = fileObj.workPaperNumber;
    this.auditId = fileObj.auditId;
    this.auditName = fileObj.auditName;
    this.checkInBy = fileObj.checkedInBy;
    this.userType = fileObj.userType;
    let payLoad = {
      documentNo: this.documentNo,
      dmsSessionId: localStorage.getItem("dmsToken"),
      checkoutType: "2",
      checkOutBy: localStorage.getItem("username"),
      comments: `document number ${
        this.documentNo
        } has been checked out by ${localStorage.getItem("username")}`,
    };
    if ((fileObj.checkOutBy == this.currentUser) && (fileObj.checkoutStatus == "2")) {
      this.previewMode = true;
    } else {
      this.httpService
        .post(ConstUrls.checkOutDocument, payLoad)
        .subscribe((checkOutResult: any) => {
          if (checkOutResult.errorCode == 0) {
            this.previewMode = true;
            // this.getAllDmsDocs();
            this.collabChatServiceService.getIntrayFiles({
              auditId: this.auditId,
              roomName: this.roomName
            });
          } else if (checkOutResult.errorCode == -2) {
            this.myCommonAlert("Success", checkOutResult.message);
          } else {
            this.myCommonAlert(
              "Success",
              "Something goes wrong while checking out file."
            );
          }
        });
    }
    // this.inTrayService.updateFileStatus({ "roomId": this.room._id, name: myFile.name, isOpen: true });
    // setTimeout(() => {
    //   this.openRoom(this.room._id);
    // }, 500)
    // this.saveasWorkPaper = true;
  }
  closeSaveAsWP() {
    this.saveasWorkPaper = false;
  }
  renameDocument(fileInfo) {
    this.oldFileName = fileInfo.fileName;
    this.docNo = fileInfo.documentNo;
    this.renameFileName = fileInfo.fileName.split(".")[0];
    this.renameFileExtension = fileInfo.fileName.substr(
      fileInfo.fileName.lastIndexOf(".")
    );
    this.checkInBy = fileInfo.checkedInBy;
    this.userType = fileInfo.userType;
    this.comments = "";
    console.log("**********fileInfo", fileInfo);

    this.renameDoc = true;
  }
  upadateFileName() {
    if (
      this.renameFileName.length == 0 ||
      this.countDotsInFileName(this.renameFileName, "\\.") > 0
    ) {
      this.myCommonAlert("Error", "Please enter valid file name.");
    } else {
      let fullFileName = this.renameFileName + this.renameFileExtension;
      let data = {
        dmsSessionId: localStorage.getItem("dmsToken"),
        documentNo: this.docNo,
        fileName: fullFileName,
        renameBy:localStorage.getItem("username"),
        comments: this.comments,
      };
      console.log("******data", data);
      this.httpService
        .post(ConstUrls.renameFile, data)
        .subscribe((res: any) => {
          if (res) {
            let payload = {
              auditId: this.auditId,
              currentUser: this.checkInBy,
              oldfileName: this.oldFileName,
              newFileName: fullFileName,
              userType: this.userType,
              roomType: this.roomName
            }
            this.httpService.post(ConstUrls.setFileNameCollab, payload).subscribe((x) => {
              this.myCommonAlert("Success", "File rename successfully.");
              this.closeRenameDoc();
              // this.getAllDmsDocs();
              this.collabChatServiceService.getIntrayFiles({
                auditId: this.auditId,
                roomName: this.roomName
              });
            })

          }
        });
    }
  }

  countDotsInFileName(s1, letter) {
    return (s1.match(RegExp(letter, "g")) || []).length;
  }
  omit_special_char(event) {
    var k;
    var kcode = event.charCode; //         k = event.keyCode;  (Both can be used)
    if (
      kcode == 8 ||
      kcode == 9 ||
      kcode == 46 ||
      kcode == 95 ||
      (kcode > 47 && kcode < 58) ||
      (kcode > 64 && kcode < 91) ||
      (kcode > 96 && kcode < 123)
    ) {
      return true;
    } else {
      return false;
    }
  }

  closeRenameDoc() {
    this.docNo = "";
    this.renameFileName = "";
    this.renameFileExtension = "";
    this.comments = "";
    this.renameDoc = false;
    this.checkInBy = '';
  }

  deleteFile(file) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to delete ${file.fileName}?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: file.documentNo,
          fileName: file.fileName,
          _id: file.item_id,
          deletedBy:localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
        };
        console.log("*****payload:", payload);
        this.httpService
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res.errorCode == 0) {
              var type = "auditor";
              if (this.router.url.split("?")[0] == "/auditee-chatroom") {
                type = "auditee";
              }
              let pay = {
                userType: type,
                username: file.checkedInBy,
                roomName: this.roomName,
                auditId: this.auditId,
                fileName: file.fileName,
              };
              console.log('saved')
              this.httpService
                .post(ConstUrls.deleteFileIntray, pay)
                .subscribe((resp: any) => {
                  if (resp) {
                    // this.getAllDmsDocs();
                    this.collabChatServiceService.getIntrayFiles({
                      auditId: this.auditId,
                      roomName: this.roomName
                    });
                    const initialState = {
                      title: "Success",
                      content: "Document Deleted Successfully",
                      link: "Ok",
                    };
                    this.modalRef3 = this.modalService.show(
                      DefaultModalComponent,
                      {
                        initialState,
                        class: "success-class",
                        backdrop: "static",
                        keyboard: false,
                      }
                    );
                  }
                });

              // this.myCommonAlert("Success", "Document deleted successfully.");
            } else {
              this.myCommonAlert("Eroor", res.message);
            }
          });
        // var data = { id: this.room._id, fileName: file.name };
        // //var removeIndex = this.files.indexOf(data.fileName)
        // let removeIndex = this.files.findIndex((obj) => obj.name == data.fileName)
        // this.files.splice(removeIndex, 1);
        // this.inTrayService.deleteFileroom(data);
        // setTimeout(() => {
        //   this.openRoom(this.room._id);
        // }, 500)
      }
    });
  }

  getSelectedDocs() {
    if(this.selectedItems.length == 0){
      this.myCommonAlert('Error' , 'Please select documents to upload')
    }else{
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.selectedItems[i]['metaData']=`Module-Name=Collaboration,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=${uuidv4()}`
        // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
        
         this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
        if (this.router.url.split("?")[0] == '/auditee-chatroom') {
        this.selectedItems[i]['userType'] = 'auditee';
        }else{
        this.selectedItems[i]['userType'] = 'auditor';
        }
        this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
        this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
        this.selectedItems[i]['roomType']=this.roomName;
        this.selectedItems[i]['routerLink']='';
        this.selectedItems[i]['subModuleName']='In-Tray';
        this.selectedItems[i]['auditYear']=new Date().getFullYear();
        this.selectedItems[i]['auditId']=this.auditId;
        this.selectedItems[i]['auditName']='name';
        this.selectedItems[i]['moduleName']='Collaboration';
        this.selectedItems[i]['checkedinDate']= moment().format("L");
        
      }
    
    }
    console.log("Selected files: ", this.selectedItems);
    
    let payloads ={items:this.selectedItems};
    this.httpService.post(ConstUrls.DmsSaveAsDocument ,payloads).subscribe((res:any)=>{
      if(res){
            console.log('*********res', res)
            var type = 'auditor'
            if (this.router.url.split("?")[0] == '/auditee-chatroom') {
              type = 'auditee';
            }
            let payload = {
              auditId: this.auditId,
              username: localStorage.getItem("username"),
              userType: type,
              fileList: this.selectedItems.map((item) => {
                return { fileName: item["fileName"], room: this.roomName };
              }),
            };
          
            this.httpService.post(ConstUrls.updateIntrayDoc, payload).subscribe((resp: any) => {
                if (resp) {
                  this.collabChatServiceService.getIntrayFiles({auditId: this.auditId,roomName: this.roomName});
                  this.myCommonAlert('Success' , 'Document uploaded Successfully In Room')
                }
              });

      }
    })
    
    

 
  }

  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185" , searchBy:localStorage.getItem('username') };

      this.httpService
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            this.dropdownFirmList = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    // this.dropdownListAll = [
    //   { item_id: 1, item_text: 'Mumbaisssssssssss' },
    //   { item_id: 2, item_text: 'Bangaluru' },
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' },
    //   { item_id: 5, item_text: 'New Delhi' },
    //   { item_id: 6, item_text: 'Mumbai' },
    //   { item_id: 7, item_text: 'Bangaluru' },
    //   { item_id: 8, item_text: 'Pune' },
    //   { item_id: 9, item_text: 'Navsari' },
    //   { item_id: 10, item_text: 'New Delhi' },
    //   { item_id: 11, item_text: 'Mumbai' },
    //   { item_id: 12, item_text: 'Bangaluru' },
    //   { item_id: 13, item_text: 'Pune' },
    //   { item_id: 14, item_text: 'Navsari' },
    //   { item_id: 15, item_text: 'New Delhi' }
    // ];
  }

  intrayFilteredList = [];
  onSearchKeyPress() {
    this.intrayFilteredList = this.intrayFileList;
    if (this.searchData.length >= 3) {
      this.intrayFilteredList = this.intrayFileList.filter((f) =>
        f.fileName.toLowerCase().includes(this.searchData.toLowerCase())
      );
    }
  }
  validateMail(mailId){
    let regexp = new RegExp(/^(([^!#$%&*'<>()\[\]\\.,;:\s@"]+(\.[^!#$%&*'<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    // const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result = regexp.test(mailId);
    console.log('result', result);
    return result;

  }
  getAllDmsDocs() {
    // let payload = {
    //   auditId: this.auditId,
    //   roomName: this.roomName
    // };
    if (this.router.url.split("?")[0] == '/auditee-chatroom') {
      this.collabChatServiceService.getIntrayFiles({
        auditId: this.auditId,
        roomName: this.roomName,
        userType: 'auditee',
        username: localStorage.getItem('username')
      });
    }
    else {
      this.collabChatServiceService.getIntrayFiles({
        auditId: this.auditId,
        roomName: this.roomName,
        userType: 'auditor',
        username: localStorage.getItem('username')

      });
    }

    this.collabChatServiceService.gotIntrayFiles().subscribe((response) => {
      console.log("List", response);
      var arr = [];
      let isAuditee = this.validateMail(localStorage.getItem('username'));
      response[0]["FileData"].forEach((f) => {
       
        var format = f.fileName.split(".").slice(-1)[0];

        let downldUrl = `${this.configService.config.dmsBaseUrl}?dmsSession=${this.router.url.split("?")[0] == '/auditee-chatroom' ? localStorage.getItem("auditeeDmsToken") : localStorage.getItem("dmsToken")}&urlToken=${f.urlToken}`;
        arr.push({
          isAuditee : isAuditee,
          item_id: f._id,
          item_text: f.file,
          fileName: f.fileName,
          downldUrl: downldUrl,
          format: format,
          documentNo: f.documentNo,
          urlToken: f.urlToken,
          workPaperNumber: f.workPaperNumber,
          auditId: f.auditId,
          auditName: f.auditName,
          checkoutStatus: f.checkoutStatus,
          checkOutBy: f.checkOutBy,
          checkedInBy: f.checkedInBy,
          userType: f.userType,
          roomType: f.roomType
        });
      });
      this.intrayFileList = arr;
      this.intrayFilteredList = this.intrayFileList;
    });
  }

  onFilechanged(event) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      console.log("fil anme", files);
      this.myfileName = files[0]["name"];
      if (this.intrayFilteredList.length === 0) {
        formData.append("file", files[0], files[0]["name"]);
        var type = 'auditor';
        if (this.router.url.split("?")[0] == '/auditee-chatroom') {
          type = 'auditee';
          formData.append("dmsSessionId", localStorage.getItem("auditeeDmsToken"));
          formData.append("userType", 'auditee');
        }
        else {
          formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
          formData.append("userType", 'auditor');
        }
        // var data = { roomId: this.room._id, fileName: files[0]['name'] }
        formData.append("checkedInBy", localStorage.getItem("username")),
          formData.append("metadata", "Module-Name=Collaboration,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019"),
          formData.append("roomType", this.roomName);
        formData.append("routerLink", ""),
          formData.append("subModuleName", "In-Tray"),
          formData.append("auditYear", new Date().getFullYear()),
          formData.append("auditId", this.auditId),
          formData.append("auditName", "name"),
          formData.append("moduleName", "Collaboration"),
          formData.append("checkedinDate", moment().format("L"));

        this.httpService.post(ConstUrls.DmsUpload, formData).subscribe((files) => {
          let payload = {
            auditId: this.auditId, username: localStorage.getItem("username"),
            userType: type,
            fileList: [{ fileName: this.myfileName, room: this.roomName }],
          };
          this.httpService.post(ConstUrls.updateIntrayDoc, payload).subscribe((res: any) => {
            if (res) {
              this.getAllDmsDocs();
              this.myCommonAlert('Success', 'Document uploaded Successfully');
            }
          });
          // { item_id: files[0]._id, item_text: files[0].file }
          this.dropdownFirmList = this.dropdownFirmList.concat({
            item_id: files[0]._id,
            item_text: files[0].file,
          });
          // this.inTrayService.updateFileroomDetails(data);
          // setTimeout(() => {
          //   this.openRoom(this.room._id);
          // }, 500)
        });
        $("#myFile").val('');
      } else {

        if (this.router.url.split("?")[0] == '/auditee-chatroom') {
          this.chkpayload = { auditId: this.auditId, fileName: this.myfileName, roomType: 'auditee' };
        }
        else {
          this.chkpayload = { auditId: this.auditId, fileName: this.myfileName, roomType: 'auditor' };
        }

        this.httpService.post(ConstUrls.DmsCheckFileExist, this.chkpayload).subscribe((res: any) => {
          if (res) {
            if (res.message === 'filename not found') {//upload new file

              formData.append("file", files[0], files[0]["name"]);
              var type = 'auditor';
              if (this.router.url.split("?")[0] == '/auditee-chatroom') {
                type = 'auditee';
                formData.append("dmsSessionId", localStorage.getItem("auditeeDmsToken"));
                formData.append("userType", 'auditee');
              }
              else {
                //  formData.append("dmsSessionId", '');

                 formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
                formData.append("userType", 'auditor');
              }
              // var data = { roomId: this.room._id, fileName: files[0]['name'] }
              formData.append("checkedInBy", localStorage.getItem("username")),
                formData.append("metadata", "Module-Name=Collaboration,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019"),
                formData.append("roomType", this.roomName);
              formData.append("routerLink", ""),
                formData.append("subModuleName", "In-Tray"),
                formData.append("auditYear", new Date().getFullYear()),
                formData.append("auditId", this.auditId),
                formData.append("auditName", "name"),
                formData.append("moduleName", "Collaboration"),
                formData.append("checkedinDate", moment().format("L"));

              this.httpService.post(ConstUrls.DmsUpload, formData).subscribe((files) => {
                let payload = {
                  auditId: this.auditId, username: localStorage.getItem("username"),
                  userType: type,
                  fileList: [{ fileName: this.myfileName, room: this.roomName }],
                };
                console.log("Payload: ", payload);
                this.httpService.post(ConstUrls.updateIntrayDoc, payload).subscribe((res: any) => {
                  if (res) {
                    this.getAllDmsDocs();
                    this.myCommonAlert('Success', 'Document uploaded Successfully');
                  }
                });
                // { item_id: files[0]._id, item_text: files[0].file }
                this.dropdownFirmList = this.dropdownFirmList.concat({
                  item_id: files[0]._id,
                  item_text: files[0].file,
                });
                // this.inTrayService.updateFileroomDetails(data);
                // setTimeout(() => {
                //   this.openRoom(this.room._id);
                // }, 500)
              },error=>{
                console.log('***********error', error)
                let errorMsg = JSON.parse(JSON.stringify(error.error));
                this.myCommonAlert('Error' , errorMsg.message)
              });
              $("#myFile").val('');



            } else if (res.message === 'filename already exist') {

              const initialState = {
                title: "Confirmation",
                content: `${res.message} Do you want to upload again?`,
                link: "Confirm",
                action: "Cancel",
                confirmFlag: false,
              };
              this.modalRef = this.modalService.show(ConfirmPopupComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.modalRef.content.onClose.subscribe((r) => {
                if (r) {
                  console.log('Response: ', res.data._id)
                  formData.append("file", files[0], files[0]["name"]);
                  var type = 'auditor';
                  if (this.router.url.split("?")[0] == '/auditee-chatroom') {
                    type = 'auditee';
                    formData.append("dmsSessionId", localStorage.getItem("auditeeDmsToken"));
                    formData.append("userType", 'auditee');
                  }
                  else {
                    formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
                    formData.append("userType", 'auditor');
                  }
                  formData.append("filename", files[0]["name"]);
                  formData.append("workpaperNo", res['data']['workPaperNumber']),
                    formData.append("docId", res['data']['documentNo']),
                    formData.append("doctype", 'DATAFILE'),
                    formData.append("comment", 'Existing document replaced by ' + localStorage.getItem('username')),

                    this.httpService.post(ConstUrls.replaceDocuments, formData).subscribe((res: any) => {
                      if (res.errorCode === 0) {
                        this.myCommonAlert('Success', 'Document Uploaded successfully')
                        console.log('res', res)

                      } else {
                        this.myCommonAlert('Error', 'Failed to upload')

                      }
                    });
                  $("#myFile").val('');
                }
              });

            } else if (res.message === 'audit does not exist') {
              this.myCommonAlert('Error', res.message);
            } else {
              this.myCommonAlert('Error', res.message);
            }
          }
        })
      }
      return;

    } else {
      this.myCommonAlert('Error', 'Invalid file');
      $("#myFile").val('');
    }
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    console.log('extension', extension)
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }

  downloadFile(fileUrl, filename) {
    fetch(fileUrl)
      .then(resp => resp.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        // alert('your file has downloaded!'); // or you know, something with better UX...
      })
      .catch(() => 
      alert('oh no!')
      );
  }
  ////////////////////////
  // }
  // // var data = { roomId: this.room._id, fileName: files[0]['name'] }
  // formData.append("checkedInBy", localStorage.getItem("username")),
  //   formData.append(
  //     "metadata",
  //     "Module-Name=Collaboration,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019"
  //   ),
  //   formData.append("roomType", this.roomName);
  // formData.append("routerLink", ""),
  //   formData.append("subModuleName", "In-Tray"),
  //   formData.append("auditYear", new Date().getFullYear()),
  //   formData.append("auditId", this.auditId),
  //   formData.append("auditName", "name"),
  //   formData.append("moduleName", "Collaboration"),
  //   // formData.append('audit', 'Collaboration'),
  //   formData.append("checkedinDate", moment().format("L"));

  /////////////////////////
  // userType: "auditor",
  //   userType: type,
  //   fileList: [{ fileName: this.myfileName, room: this.roomName }],
  // };
  // console.log("Payload: ", payload);
  // this.httpService
  //   .post(ConstUrls.updateIntrayDoc, payload)
  //   .subscribe((res: any) => {
  //     if (res) {
  //       // this.getAllDmsDocs();
  //       this.collabChatServiceService.getIntrayFiles({
  //         auditId: this.auditId,
  //         roomName: this.roomName
  //       });
  //       const initialState = {
  //         title: "Success",
  //         content: "Document uploaded Successfully",
  //         link: "Ok",
  //       };
  //       this.modalRef3 = this.modalService.show(DefaultModalComponent, {
  //         initialState,
  //         class: "success-class",
  //         backdrop: "static",
  //         keyboard: false,
  //       });
  //     }
  //   });

  // NewChatRoomBtn(myFile) {
  //   if (!myFile.isOpen && !this.isFileOpen) {
  //     this.isFileOpen = true;
  //     this.NewChatRoomValidateFlag = "ValidatorToken";
  //     this.previewMode = false;
  //     this.title = myFile.name;
  //     this.fullPath = this.uri + "uploads/inTray/" + myFile.name;
  //     this.previewMode = true;
  //     this.inTrayService.updateFileStatus({
  //       roomId: this.room._id,
  //       name: myFile.name,
  //       isOpen: true,
  //     });
  //     setTimeout(() => {
  //       this.openRoom(this.room._id);
  //     }, 500);
  //   } else if (myFile.isOpen) {
  //     this.myCommonAlert("Success", "File is already in use.");
  //   } else if (this.isFileOpen) {
  //     this.myCommonAlert("Success", "Please close existing open file first.");
  //   }
  // }

  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }

  onClose(close: boolean) {
    if (close) {
      //this.close();
      let payload = {
        checkedinDate: moment().format("L"),
        dmsSessionId: localStorage.getItem("dmsToken"),
        documentNo: this.documentNo,
        checkedInBy:localStorage.getItem("username"),
        comments: `document number ${
          this.documentNo
          } has been checked in by ${localStorage.getItem("username")}`,
      };
      this.httpService
        .post(ConstUrls.checkInDocument, payload)
        .subscribe((x) => {

          let auditId = this.auditId;
          let roomName = this.roomName;
          this.collabChatServiceService.getIntrayFiles({
            auditId,
            roomName
          });
        });
      this.previewMode = false;

    } else {
      // this.getAllDmsDocs();

    }
  }

  NewCloseDocRoom(){
    
  }
  // NewCloseDocRoom() {
  //   this.NewChatRoomValidateFlag = "";
  //   this.previewMode = false;
  //   this.isFileOpen = false;
  //   let roomName = this.title;
  //   this.inTrayService.updateFileStatus({
  //     roomId: this.room._id,
  //     name: roomName,
  //     isOpen: false,
  //   });
  //   this.title = "";
  //   setTimeout(() => {
  //     this.openRoom(this.room._id);
  //   }, 500);
  // }
  toggleSearch() {
    this.searchshow = !this.searchshow;
    this.status = !this.status;
    this.renameDoc = false;
  }
  onUploadFile() {
    this.searchshow = false;
    this.successMsg = true;
    this.status = false;
    setTimeout(
      function () {
        this.successMsg = false;
      }.bind(this),
      3000
    );
  }
}
