import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhiteboardNewComponent } from './whiteboard-new.component';

describe('WhiteboardNewComponent', () => {
  let component: WhiteboardNewComponent;
  let fixture: ComponentFixture<WhiteboardNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhiteboardNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteboardNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
