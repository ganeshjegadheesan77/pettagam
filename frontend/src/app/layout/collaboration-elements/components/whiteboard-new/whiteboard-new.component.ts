import { Component, OnInit } from '@angular/core';
declare const boardNew:any;

@Component({
  selector: 'app-whiteboard-new',
  templateUrl: './whiteboard-new.component.html',
  styleUrls: ['./whiteboard-new.component.less']
})
export class WhiteboardNewComponent implements OnInit {

  constructor() { }

  ngOnInit() 
  {  
    boardNew();
  }
  closeModal(){}
}
