import { Injectable } from "@angular/core";
import io from "socket.io-client";
import {BehaviorSubject, Observable } from "rxjs";
import { HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({
  providedIn: "root"
})
export class ChatroomService {
  private socket;

  constructor(private configService:ConfigService) {
    this.socket = io(configService.config.socket);
  }
  private audit=new BehaviorSubject<string>("");


  loginUser(user) {
    this.socket.emit("logInUser", user, httpOptions);
  }

  logOutUser(user) {
    this.socket.emit("logOutUser", user, httpOptions);
  }

  getRoomsData(username) {
    this.socket.emit("getRoomsList", username, httpOptions);
  }
  getMessageData(roomName) {
    this.socket.emit("messageList", roomName, httpOptions);
  }
  public setValue(value:string):void{
    this.audit.next(value)
}
public getValue(): Observable<string>{
    return this.audit;
}
  gotRoomsData() {
    let observable = new Observable<{ data: string }>(observer => {
      this.socket.on("getRoomsList", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  createRoom(room) {
    this.socket.emit("createRoom", room, httpOptions);
  }
  deleteRoom(room) {
    this.socket.emit("deleteRoom", room, httpOptions);
  }
  deleteUser(user, roomId) {
    //console.log("user deleteUser", user);
    let userDetails = { userName: user, id: roomId };
    this.socket.emit("deleteUser", userDetails, httpOptions);
  }

  getChatRoomDetails(roomId) {
    this.socket.emit("getRoomDetails", roomId, httpOptions);
  }

  gotChatRoomDetails() {
    let observable = new Observable<{ data: string }>(observer => {
      this.socket.on("getRoomDetails", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  readInitUserList() {
    this.socket.emit("userList", httpOptions);
  }

  readUserList() {
    let observable = new Observable<[]>(observer => {
      this.socket.on("userList", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  getUser() {
    let observable = new Observable<{ data: string }>(observer => {
      this.socket.on("logInUser", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  addUsersToRoom(username, groupId) {
    this.socket.emit("addUserToRoom", { username, groupId }, httpOptions);
  }

  getChatMessages(roomId) {
    this.socket.emit("messageList", roomId, httpOptions);
  }

  getOnlineStatus(usernames) {
    this.socket.emit("getOnlineUsers", usernames, httpOptions);
  }

  gotOnlineStatus() {
    let observable = new Observable<Object>(observer => {
      this.socket.on("onlineUsers", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  readMessages() {
    let observable = new Observable<Object>(observer => {
      this.socket.on("messageList", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  newMessage(msg) {
    this.socket.emit("newMessage", msg, httpOptions);
  }
}
