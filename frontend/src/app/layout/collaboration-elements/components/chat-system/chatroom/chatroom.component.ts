import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ChatroomService } from "../services/chatroom.service";

@Component({
  selector: "app-chatroom",
  templateUrl: "./chatroom.component.html",
  styleUrls: ["../chat-system.component.less"]
})
export class ChatroomComponent implements OnInit {
  @Input() roomId: string;
  message = "";
  rooms: any = [];
  messages: any = [];
  usernames: any = [];
  selectedUsers: any = [];
  selectedUsersModel: any = [];
  roomDetails: any = [];
  owner = "";
  onlineUsers: any = [];
  viewRoom = false;
  viewChatroom = true;
  private groupName;
  constructor(
    public chatroomService: ChatroomService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.owner = localStorage.getItem("username");
    this.route.paramMap.subscribe(params => {});
  }

  room = {
    title: ""
  };
  openRooms() {
    this.viewRoom = true;
    this.viewChatroom = false;
  }
  closeDialog() {
    // this.dialogRef.close();
  }
  selectUsers(username) {
    this.chatroomService.addUsersToRoom(username, this.groupName);
  }

  submitMessage(message) {
    if (message != "" && message != " ") {
      let msg = {
        username: localStorage.getItem("username"),
        message,
        groupId: this.groupName
      };
      this.chatroomService.newMessage(msg);
      this.message = "";
    }
  }
  removeUser(user, roomId) {
    this.usernames.push(user);
    this.chatroomService.deleteUser(user.username, roomId);
  }

  ngOnInit(): void {
    this.groupName = this.roomId;
    this.chatroomService.getChatRoomDetails(this.roomId);
    this.chatroomService.getChatMessages(this.roomId);
    this.chatroomService.gotChatRoomDetails().subscribe(data => {
      this.roomDetails = data[0];

      this.chatroomService.getOnlineStatus(data[0].usernames);
      if (data[0].owner == localStorage.getItem("username")) {
        this.chatroomService.readInitUserList();
      }
    });

    this.chatroomService.gotOnlineStatus().subscribe(data => {
      this.onlineUsers = data;
    });

    this.chatroomService.readUserList().subscribe(data => {
      let users = [];
      if (this.roomDetails && this.roomDetails.usernames.length) {
        users = data.filter(ds => {
          let rs: any = ds;
          return this.roomDetails.usernames.indexOf(rs.username) == -1;
        });
        users = users.filter(us => us.username != this.roomDetails.owner);
        this.usernames = users;
      } else {
        this.usernames = data;
      }
    });

    this.chatroomService.readMessages().subscribe(data => {
      this.messages = data;
      setTimeout(() => {
        let objDiv = document.getElementById("message_div");
        objDiv.scrollTop = objDiv.scrollHeight;
      }, 100);
    });
  }
}
