import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionInTrayComponent } from './action-in-tray.component';

describe('ActionInTrayComponent', () => {
  let component: ActionInTrayComponent;
  let fixture: ComponentFixture<ActionInTrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionInTrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionInTrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
