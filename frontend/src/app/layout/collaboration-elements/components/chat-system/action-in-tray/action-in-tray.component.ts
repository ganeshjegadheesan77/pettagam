import { Component, OnInit } from "@angular/core";
import { ChatroomService } from "../services/chatroom.service";
import { InTrayService } from "../../in-tray/services/in-tray.service";
import { HttpService } from '../../../../../core/http-services/http.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmPopupComponent } from 'src/app/layout/modal/confirm-popup/confirm-popup.component';
import { environment } from 'src/environments/environment';
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConfigService } from 'src/config.service';

@Component({
  selector: "app-action-in-tray",
  templateUrl: "./action-in-tray.component.html",
  styleUrls: ["./action-in-tray.component.less"]
})
export class ActionInTrayComponent implements OnInit {
  uri ;

  rooms: any = [];
  room: any;
  files: any;
  modalRef: any;
  modalRef1: any;
  modalRef2: any;
  modalRef3: any;
  roomTitle: string = "";
  onlineUsers: any = [];
  roomId: any;
  usernames: any;
  owner: any;
  initialState: any;
  previewMode: boolean;
  title: string = '';
  fullPath: any;
  close: any;
  isFileOpen: boolean = false;
  constructor(
    public inTrayService: InTrayService,
    public chatroomService: ChatroomService,
    public httpService: HttpService,
    private modalService: BsModalService,
    private configService:ConfigService


  )
   {
    this.uri = configService.config.baseUrl;

    this.chatroomService.getValue().subscribe(data => {
      var id = data;
      if (id == "") {
        this.owner = localStorage.getItem("username");
        this.chatroomService.readInitUserList();
        this.chatroomService.getRoomsData(localStorage.getItem("username"));
        this.chatroomService.readUserList().subscribe((d) => {
          this.usernames = d;
        });

        this.getConstructorData();
      }
      else {
        this.openRoom(id);
      }
    });
  }
  async getConstructorData() {
    await this.chatroomService.gotRoomsData().subscribe((data) => {
      if (data) {
        this.rooms = data || [];
        this.room = data[0];
        if(this.room){
        this.roomId = this.room._id;
        this.onlineUsers = this.room.usernames;
        }
      }
    });
    await this.chatroomService.readUserList().subscribe((data) => {
      let users = [];
      if (this.room && this.room.usernames.length) {
        users = data.filter((ds) => {
          let rs: any = ds;
          return this.room.usernames.indexOf(rs.username) == -1;
        });
        users = users.filter((us) => us.username != this.room.owner);
        this.usernames = users;
      } else {
        this.usernames = data;
      }
    });
  }
  ngOnInit() {
    window.onbeforeunload = (ev) => {
      if (this.isFileOpen) {
        let roomName = this.title;
        this.inTrayService.updateFileStatus({ "roomId": this.room._id, name: roomName, isOpen: false });
      }
    };

    this.chatroomService.gotRoomsData().subscribe(data => {
      // console.log("Rooms:", this.rooms);
      this.rooms = data || [];
      this.room = data[0];
      this.roomId = this.room._id;
      this.onlineUsers = this.room.usernames;
      this.files = this.room.fileName;
    });
  }
  openRoom(id) {
    this.chatroomService.getChatRoomDetails(id);
    this.chatroomService.gotChatRoomDetails().subscribe(data => {
      //console.log(data);
      this.room = data[0];
      this.files = this.room.fileName; this.onlineUsers = this.room.usernames;
      this.chatroomService.getOnlineStatus(data[0].usernames);
      if (data[0].owner == localStorage.getItem("username")) {
        this.chatroomService.readInitUserList();
      }
      //console.log(data);
    });
  }
  closeFile(fileData) {
    this.inTrayService.updateFileStatus({ "roomId": this.room._id, name: fileData, isOpen: false });
    this.title = '';
    setTimeout(() => {
      this.openRoom(this.room._id);
    }, 500)
  }
  onFilechanged(event) {
    //console.log(event.target.files)
    var files = [];
    const formData: any = new FormData();
    files = event.target.files;
    formData.append("file", files[0], files[0]['name']);
    var data = { roomId: this.room._id, fileName: files[0]['name'] }
    this.httpService.securePost('singleuploadfile', formData).subscribe(f => {
      const initialState = {
        title: "Success",
        content: "Document uploaded Successfully",
        link: "Ok",
      };
      this.modalRef3 = this.modalService.show(
        DefaultModalComponent,
        {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        }
      );
      this.inTrayService.updateFileroomDetails(data);
      setTimeout(() => {
        this.openRoom(this.room._id);
      }, 500)
    })

  }
  onClose(close: boolean) {
    if (close) {
      //this.close();
      this.previewMode = false;
    }
  }
  openAttachment(myFile) {
    if (!myFile.isOpen && !this.isFileOpen) {
      this.isFileOpen = true;
      this.previewMode = false;
      this.title = myFile.name
      this.fullPath = this.uri + 'uploads/inTray/' + myFile.name;
      this.previewMode = true;
      // console.log("params for updatefile-->", { "roomId": this.room._id, name: myFile.name, isOpen: true });
      this.inTrayService.updateFileStatus({ "roomId": this.room._id, name: myFile.name, isOpen: true });
      setTimeout(() => {
        this.openRoom(this.room._id);
      }, 500)
    } 
    // else if (this.isFileOpen && this.title == myFile.name ) {

    // } 
    else if (myFile.isOpen) {
      this.myCommonAlert("Success", "File is already in use.");
    }
    else if (this.isFileOpen ) {
      this.myCommonAlert("Success", "Please close existing open file first.");
    } 
  }
  deleteFile(file) {

    const initialState = {
      title: 'Confirmation',
      content: `Do you want to delete ${file}?`,
      link: 'Confirm',
      action: 'Cancel',
      confirmFlag: false
    };

    this.modalRef = this.modalService.show(ConfirmPopupComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
    this.modalRef.content.onClose.subscribe(r => {
      if (r) {
        let data = { id: this.room._id, fileName: file };
        let removeIndex = this.files.findIndex((obj) => obj.name == data.fileName);
        this.files.splice(removeIndex, 1);
        this.inTrayService.deleteFileroom(data);
      }
    })

  }
  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }

  addRoom(r) {
    if (r != ' ' && r != '') {
      let ro = { title: r, owner: localStorage.getItem("username") };
      this.chatroomService.createRoom(ro);
      this.roomTitle = "";
      this.chatroomService.getRoomsData(localStorage.getItem("username"));
    } else {
      this.roomTitle = "";
    }
  }
  removeChatroom(id, title) {
    const initialState = {
      title: 'Confirmation',
      content: `Do you want to delete ${title} room?`,
      link: 'Confirm',
      action: 'Cancel',
      confirmFlag: false
    };
    this.modalRef1 = this.modalService.show(ConfirmPopupComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
    this.modalRef1.content.onClose.subscribe(r => {
      if (r) {
        var data = this.rooms.filter(function (item) { return item.title == title; })
        //console.log(data)
        var removeIndex = this.rooms.indexOf(data[0])
        this.rooms.splice(removeIndex, 1);
        this.chatroomService.deleteRoom(id);
      }
    })

  }
  removeUser(user) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove ${user} from current room?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef2 = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef2.content.onClose.subscribe((r) => {
      if (r) {
        this.usernames.push(user);
        const index: number = this.onlineUsers.indexOf(user);
        if (index !== -1) {
          this.onlineUsers.splice(index, 1);
        }
        this.chatroomService.deleteUser(user, this.roomId);
        this.openRoom(this.roomId)
        //  this.chatroomService.addUsersToRoom(user, this.roomId);
      }
    });
    // this.chatroomService.getRoomsData(localStorage.getItem("username"));
  }
  selectUsers(username) {
    if (username === "Add User") return;
    this.onlineUsers.push(username);
    this.chatroomService.addUsersToRoom(username, this.roomId);
  }
  closeAlreadyOpenFile(flag: boolean) {
    this.isFileOpen = flag;
  }
}
