import { Component, OnInit } from "@angular/core";
import { ChatroomService } from "../services/chatroom.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

@Component({
  selector: "app-chat-popup",
  templateUrl: "./chat-popup.component.html",
  styleUrls: ["./chat-popup.component.less"],
})
export class ChatPopupComponent implements OnInit {
  rooms: any = [];
  roomTitle: string = "";
  roomId: any;
  selectedRoom: any = [];
  private groupName;
  roomDetails: any = [];
  onlineUsers: any = [];
  usernames: any = [];
  messages: any = [];
  message = "";
  owner = "";
  modalRef: any;

  constructor(
    public chatroomService: ChatroomService,
    private modalService: BsModalService
  ) {
    this.chatroomService.getValue().subscribe(data=>{
      var id= data;
      if(id==""){
          this.owner = localStorage.getItem("username");
    this.chatroomService.readInitUserList();
    this.chatroomService.getRoomsData(localStorage.getItem("username"));
    this.chatroomService.readUserList().subscribe((d) => {
      this.usernames = d;
    });

    this.getConstructorData();
      }
     else{
      this.openChatroom(id);
     }
    });
  
  }

  async getConstructorData() {
    await this.chatroomService.gotRoomsData().subscribe((data) => {
      if (data) {
        this.rooms = data || [];
        this.roomDetails = data[0];
        this.roomId = this.roomDetails._id;
        this.onlineUsers = this.roomDetails.usernames;
        this.chatroomService.getChatMessages(this.roomId);
        setTimeout(() => {
          let objDiv = document.getElementById("message_div");
          objDiv.scrollTop = objDiv.scrollHeight;
        }, 100);
      }
    });
    await this.chatroomService.readMessages().subscribe((data) => {
      this.messages = data;
    });
    await this.chatroomService.getChatMessages(this.roomDetails._id);
    await this.chatroomService.readUserList().subscribe((data) => {
      let users = [];
      if (this.roomDetails && this.roomDetails.usernames && this.roomDetails.usernames.length) {
        users = data.filter((ds) => {
          let rs: any = ds;
          return this.roomDetails.usernames.indexOf(rs.username) == -1;
        });
        users = users.filter((us) => us.username != this.roomDetails.owner);
        this.usernames = users;
      } else {
        this.usernames = data;
      }
    });
  }

  ngOnInit() {
    //get Room Details
    this.chatroomService.gotRoomsData().subscribe((data) => {
      if (data) {
        this.rooms = data || [];
        this.roomDetails = data[0];
        this.roomId = this.roomDetails._id;
        this.onlineUsers = this.roomDetails.usernames;
      }
    });
    //get chat messages
    this.chatroomService.getChatMessages(this.roomDetails._id);
    this.chatroomService.readMessages().subscribe((data) => {
      this.messages = data;
    });
  }
  //Remove Chatroom
  removeChatroom(id, title) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to delete ${title} room?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        var data = this.rooms.filter(function (item) {
          return item.title == title;
        });
        //console.log(data);
        var removeIndex = this.rooms.indexOf(data[0]);
        this.rooms.splice(removeIndex, 1);
        this.chatroomService.deleteRoom(id);
      }
    });
  }
  //remove user from chatroom
  removeUser(user) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove ${user} from current room?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.usernames.push(user);
        const index: number = this.onlineUsers.indexOf(user);
        if (index !== -1) {
          this.onlineUsers.splice(index, 1);
        }
        this.chatroomService.deleteUser(user, this.roomId);
        this.openChatroom(this.roomId)
      }
    });
    // this.chatroomService.getRoomsData(localStorage.getItem("username"));
  }
  //Create New Chatroom
  addRoom(r) {
    if (r != " " && r != "") {
      let ro = { title: r, owner: localStorage.getItem("username") };
      this.chatroomService.createRoom(ro);
      this.roomTitle = "";
      this.chatroomService.getRoomsData(localStorage.getItem("username"));
    } else {
      this.roomTitle = "";
    }
  }
  // Open Chatroom
  openChatroom(id) {
    this.roomId = id;
    this.chatroomService.getChatRoomDetails(id);
    this.chatroomService.getChatMessages(id);
    this.chatroomService.gotChatRoomDetails().subscribe((data) => {

      this.roomDetails = data[0];
      this.onlineUsers = this.roomDetails.usernames;
      this.chatroomService.getOnlineStatus(data[0].usernames);
      if (data[0].owner == localStorage.getItem("username")) {
        this.chatroomService.readInitUserList();
      }
    });
    this.chatroomService.readMessages().subscribe((data) => {
      this.messages = data;
      setTimeout(() => {
        let objDiv = document.getElementById("message_div");
        objDiv.scrollTop = objDiv.scrollHeight;
      }, 100);
    });
  }
  // Select User
  selectUsers(username) {
    if (username === "Add User") return;
    this.onlineUsers.push(username);
    this.chatroomService.addUsersToRoom(username, this.roomId);
  }
  //send Message
  submitMessage(message) {
    if (message != "" && message != " ") {
      let msg = {
        username: localStorage.getItem("username"),
        message,
        groupId: this.roomDetails._id,
      };
      this.chatroomService.newMessage(msg);
      this.message = "";
      setTimeout(function(){ 
        $('#message_div').scrollTop($('#message_div')[0].scrollHeight);
      }, 500);
    }
  }

  closeModal() {
    $(".chat-popup-maincontainer").addClass("d-none");
  }
}
