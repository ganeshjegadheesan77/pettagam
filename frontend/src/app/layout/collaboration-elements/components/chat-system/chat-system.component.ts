import { Component, OnInit } from "@angular/core";
import { ChatroomService } from "./services/chatroom.service";

@Component({
  selector: "app-chat-system",
  templateUrl: "./chat-system.component.html",
  styleUrls: ["./chat-system.component.less"]
})
export class ChatSystemComponent implements OnInit {
  roomList: any = [];
  messages: any = [];
  message: any = "";
  roomId: any = "";
  success: any;
  constructor(public chatroomService: ChatroomService) {
    this.chatroomService.getRoomsData(localStorage.getItem("username"));
    this.chatroomService.getMessageData(this.roomId);
  }

  ngOnInit() {
    this.chatroomService.readMessages().subscribe(data => {
      if (data && data[0]) {
        if (this.roomId == data[0].groupId) {
          this.messages = data;
          setTimeout(() => {
            let objDiv = document.getElementById("message_div_col");
            objDiv.scrollTop = objDiv.scrollHeight;
          }, 100);
        }
      }
    });

    this.chatroomService.gotRoomsData().subscribe(data => {
      this.roomList = data;
      if (this.roomId == "") {
        this.roomId = data && data[0] ? data[0]._id : "";
        this.chatroomService.getMessageData(this.roomId);
        setTimeout(() => {
          let objDiv = document.getElementById("message_div_col");
          objDiv.scrollTop = objDiv.scrollHeight;
        }, 100);
      }
    });
  }
  selectRooms(room) {
    let ro = this.roomList.filter(r => room === r.title);
    this.roomId = ro[0]._id;
    this.chatroomService.getChatMessages(ro[0]._id);
  }
  submitMessage(msg) {
    if (msg != " " && msg != "") {
      let msgs = {
        username: localStorage.getItem("username"),
        message: msg,
        groupId: this.roomId
      };
      this.chatroomService.newMessage(msgs);
      this.message = "";
    } else {
      this.message = "";
    }
  }
}
