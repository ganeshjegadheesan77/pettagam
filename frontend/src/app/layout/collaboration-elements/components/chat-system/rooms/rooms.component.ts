
import {ChangeDetectorRef,ElementRef,NgZone,OnInit,Component,} from '@angular/core';
import { fromEvent, Subject, of as observableOf } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { ChatroomService } from "../services/chatroom.service";


@Component({
  selector: "app-rooms",
  templateUrl: "./rooms.component.html",
  styleUrls: ["../chat-system.component.less"]
})
export class RoomsComponent implements OnInit {
  viewRoom = true;
  viewChatroom = false;
  roomID: any;
  rooms: any = [];
  roomTitle: string = "";
    // Element to be dragged
    private _target: HTMLElement;
    // dialog container element to resize
    private _container: HTMLElement;
    // Drag handle
    private _handle: HTMLElement;
    private _delta = {x: 0, y: 0};
    private _offset = {x: 0, y: 0};
    private _destroy$ = new Subject<void>();
    private _isResized = false;
  constructor(public chatroomService: ChatroomService,   private _elementRef: ElementRef,
    private _zone: NgZone,
    private _cd: ChangeDetectorRef,) {
    this.chatroomService.getRoomsData(localStorage.getItem("username"));
  }
  ngOnInit(): void {
    this.chatroomService.gotRoomsData().subscribe(data => {
      this.rooms = data || [];
    });
    this._elementRef.nativeElement.style.cursor = 'default';
    this._handle = this._elementRef.nativeElement.parentElement.parentElement.parentElement;
    this._target = this._elementRef.nativeElement.parentElement.parentElement.parentElement;
    this._container = this._elementRef.nativeElement.parentElement.parentElement;
    this._container.style.resize = 'both';
    this._container.style.overflow = 'hidden';
    this._setupEvents();
  }
  closeDialog() {
    // this.dialogRef.close();
  }
  openChatroom(id) {
    this.roomID = id;
    this.viewRoom = false;
    this.viewChatroom = true;
  }
  addRoom(r) {
    if(r != ' ' && r != ''){
      let ro = { title: r, owner: localStorage.getItem("username") };
      //console.log('ro', ro);
      this.chatroomService.createRoom(ro);
      this.roomTitle = "";
    }else{
      this.roomTitle = "";
    }
  }
  removeChatroom(id){
    var removeIndex = this.rooms.map(function(item) { return item.title; }).indexOf(id);
    this.rooms.splice(removeIndex, 1);
    this.chatroomService.deleteRoom(id);
    // alert('removeIndex:'+removeIndex);
  }
  private _setupEvents() {
    this._zone.runOutsideAngular(() => {
      const mousedown$ = fromEvent(this._handle, 'mousedown');
      const mousemove$ = fromEvent(document, 'mousemove');
      const mouseup$ = fromEvent(document, 'mouseup');
      const mousedrag$ = mousedown$.pipe(
        switchMap((event: MouseEvent) => {
          const startX = event.clientX;
          const startY = event.clientY;
          const rectX = this._container.getBoundingClientRect();
          if (
            // if the user is clicking on the bottom-right corner, he will resize the dialog
            startY > rectX.bottom - 15 &&
            startY <= rectX.bottom &&
            startX > rectX.right - 15 &&
            startX <= rectX.right
          ) {
            this._isResized = true;
            return observableOf(null);
          }
          return mousemove$.pipe(
            map((innerEvent: MouseEvent) => {
              innerEvent.preventDefault();
              this._delta = {
                x: innerEvent.clientX - startX,
                y: innerEvent.clientY - startY,
              };
            }),
            takeUntil(mouseup$),
          );
        }),
        takeUntil(this._destroy$),
      );
      mousedrag$.subscribe(() => {
        if (this._delta.x === 0 && this._delta.y === 0) {
          return;
        }
        this._translate();
      });
      mouseup$.pipe(takeUntil(this._destroy$)).subscribe(() => {
        if (this._isResized) {
          this._handle.style.width = 'auto';
        }
        this._offset.x += this._delta.x;
        this._offset.y += this._delta.y;
        this._delta = {x: 0, y: 0};
        this._cd.markForCheck();
      });
    });
  }
  private _translate() {
    // this._target.style.left = `${this._offset.x + this._delta.x}px`;
    // this._target.style.top = `${this._offset.y + this._delta.y}px`;
    // this._target.style.position = 'relative';
    requestAnimationFrame(() => {
      this._target.style.transform = `
        translate(${this._offset.x + this._delta.x}px,
                  ${this._offset.y + this._delta.y}px)
      `;
    });
  }

}
