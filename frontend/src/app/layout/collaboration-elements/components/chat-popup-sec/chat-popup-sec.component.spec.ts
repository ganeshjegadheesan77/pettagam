import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatPopupSecComponent } from './chat-popup-sec.component';

describe('ChatPopupSecComponent', () => {
  let component: ChatPopupSecComponent;
  let fixture: ComponentFixture<ChatPopupSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatPopupSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatPopupSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
