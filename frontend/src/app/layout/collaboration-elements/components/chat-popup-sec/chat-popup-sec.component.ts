import {
  Component,
  OnInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
  ElementRef,
  HostListener,
} from "@angular/core";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ChatroomService } from "../chat-system/services/chatroom.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CollabChatServiceService } from "../../collabServices/collab-chat-service.service";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as jsPDF from "jspdf";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import * as moment from "moment";

@Component({
  selector: "app-chat-popup-sec",
  templateUrl: "./chat-popup-sec.component.html",
  styleUrls: ["./chat-popup-sec.component.less"],
})
export class ChatPopupSecComponent implements OnInit, OnChanges {
  @ViewChild("staticTabs", { static: false }) staticTabs: TabsetComponent;
  @ViewChild("_message_div", { static: false }) _message_div: ElementRef;
  @ViewChild("_message_div1", { static: false }) _message_div1: ElementRef;
  invalidMail: boolean = false;
  @HostListener("window:beforeunload", ["$event"])
  beforeunloadHandler(event) {
    let usr = {
      roomId: this.auditId,
      username: localStorage.getItem("username"),
      status: false,
    };
    this.collabChatServiceService.userOnlineStatus(usr);
  }

  // @Input("urlAuditId") urlAuditId: string;
  // @Input("urlEmail") urlEmail: string;
  urlToken;
  modalRef: BsModalRef;
  public validateFlag: string;
  public validateActionBtnFlag: string;
  public validateFlagMinimize: string;
  public validateFlagMaximize: string;
  NewHoverBarValidateFlag: string;
  isShown = false;
  auditgp = false;
  gropClose = false;
  gropmain = true;
  firstImg = true;
  chatBall = true;
  secoundImg = false;
  resultSend = false;
  resultMessage;
  inputDetail = true;
  auditorChat = true;
  auditeeChat = true;
  auditId = "";
  NewValidateFlag: string;
  NewVedioValidateFlag: string;
  NewInTrayValidateFlag: string;
  NewWhiteBoardValidateFlag: string;
  NewChatRoomValidateFlag: string;
  owner: any = [];
  rooms: any = [];
  roomId: any = [];
  roomDetails: any = [];
  messages: any = "";
  message: any = "";
  subscription: any;
  sub: any;
  collabChatRoomData: any = {};
  completeChatHistory: any = {};
  auditeeChatHistory: any = [];
  auditorChatHistory: any = [];
  activeTab: any;
  createdAt = new Date();
  onlineUsers: any = [];
  keyword = "username";
  hostList: any = [];
  auditeePendingTasks = 0;
  auditorPendingTasks = 0;
  inviteMailId = "";
  doc = new jsPDF();
  sessionEndedMSG: "This session has ended";
  endSession = false;
  windowType = "auditor";
  auditorView = true;
  constructor(
    private chatroomService: ChatroomService,
    private collabChatServiceService: CollabChatServiceService,
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private httpservice: HttpService,
    private modalService: BsModalService
  ) {
    // if (this.urlAuditId) {
    //   this.collabChatServiceService.getCollabChatRoom(this.urlAuditId);
    //   console.log(this.urlAuditId);
    // }
    // if (!this.urlAuditId) {
    this.subscription = collabChatServiceService
      .getAuditId()
      .subscribe((auditId) => {
        if (auditId) {
          console.log("---- Subcription for getAuditId() " + auditId);
          this.auditId = auditId;
          if (this.activeTab != auditId) {
            this.activeTab = auditId;
            this.auditorChatHistory = [];
            this.auditeeChatHistory = [];
            let usr = {
              roomId: this.auditId,
              username: localStorage.getItem("username"),
              status: true,
            };
            this.collabChatServiceService.userOnlineStatus(usr);
            this.collabChatServiceService.getCollabChatRoom(auditId);
            this.collabChatServiceService.getAllAuditChatroomMessages(auditId);
          }
        }
      });

    this.sub = collabChatServiceService.getUserOnline().subscribe((user) => {
      user["createdAt"] = new Date();
      user["message"] = null;
      // user["toHide"] = true;
      this.auditeeChatHistory.push(user);
      const localUser = localStorage.getItem("usersAddedOnline");
      if (localUser) {
        const usr = JSON.parse(localUser);
        let usrExists = false;
        usr.forEach((localStorageUser) => {
          if (localStorageUser.username === user["username"]) {
            usrExists = true;
          }
        });
        if (!usrExists) {
          usr.push(user);
          localStorage.setItem("usersAddedOnline", JSON.stringify(usr));
        }
      } else {
        const usersAddedOnline = [];
        usersAddedOnline.push(user);
        localStorage.setItem(
          "usersAddedOnline",
          JSON.stringify(usersAddedOnline)
        );
      }
    });
    // }
    // this.collabChatServiceService.readHostList().subscribe((data) => {
    //   this.hostList = data;
    // });
  }

  roomName = "auditor";

  ngOnInit() {
    this.roomName = "auditor";
    if (this.router.url.split("?")[0] == "/auditee-chatroom") {
      this.roomName = "auditee";
      this.auditorChat = false;
      this.auditeeChat = true;
      this.urlTokenMethod();
      this.NewOpenPopupChat();
    }

    //new code send new message
    this.owner = localStorage.getItem("username");
    this.collabChatServiceService
      .getAuditChatroomMessage()
      .subscribe((chatHistory) => {
        if (chatHistory) {
          this.completeChatHistory = chatHistory["messages"];
          this.owner = localStorage.getItem("username");
          this.auditorChatHistory = [];
          this.auditeeChatHistory = [];

          this.completeChatHistory.forEach((element) => {
            if (element.masterRoom) {
              console.log("this.auditorChatHistory:", this.auditorChatHistory);
              this.auditorChatHistory.push(element);
            } else {
              this.auditeeChatHistory.push(element);
            }
          });
        }
        let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
        if (userArr) {
          userArr.forEach((usr) => {
            this.auditeeChatHistory.push(usr);
          });
        }
      });
    //get chat history
    this.collabChatServiceService
      .gotAllAuditChatroomMessages()
      .subscribe((data) => {
        if (data && data["messages"]) {
          this.auditorChatHistory = [];
          this.auditeeChatHistory = [];
          this.completeChatHistory = data["messages"];
          this.completeChatHistory.forEach((element) => {
            if (element.masterRoom) {
              this.auditorChatHistory.push(element);
            } else {
              this.auditeeChatHistory.push(element);
            }
          });
        }
        let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
        if (userArr) {
          userArr.forEach((usr) => {
            this.auditeeChatHistory.push(usr);
          });
        }
      });

    this.collabChatServiceService.roomDataFetched().subscribe((roomData) => {
      if (roomData) {
        this.collabChatRoomData = roomData;

        //get all messages

        this.collabChatServiceService.getAllAuditChatroomMessages(
          roomData["roomId"]
        );
      }
    });
  }
  urlTokenMethod() {
    console.log("chat popup sec called");
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params["urlToken"]) {
        this.urlToken = params["urlToken"];
        this.httpservice
          .post(ConstUrls.checkAuditeeToken, { Token: this.urlToken })
          .subscribe(
            (res) => {
              this.auditId = res["auditId"];
              const onlineData = {
                roomId: res["auditId"],
                username: localStorage.getItem("username"),
                status: true,
              };
              this.collabChatServiceService.markUserOnline(onlineData);
              console.log(res["auditId"], res["emailId"], "JOINED");
              this.newUserJoined(res["auditId"], res["emailId"]);
              this.collabChatServiceService.getCollabChatRoom(res["auditId"]);
              this.collabChatServiceService.getAllAuditChatroomMessages(
                res["auditId"]
              );

              this.owner = res["emailId"];
              localStorage.setItem("username", res["emailId"]);
              this.getAuditeeDmsToken();
            },
            (err) => {
              this.openAlert("Failure", err.error.message, "Ok");
            }
          );
      } else {
        // localStorage.clear();
        localStorage.removeItem("username");
        localStorage.removeItem("token");
        localStorage.removeItem("dmsToken");
        this.router.navigate(["/login"]);
      }
    });
  }
  newUserJoined(id, username) {
    if (id) {
      let msgObj = {
        roomId: id,
        messages: {
          username: username,
          createdAt: Date(),
          message: `${username} Joined`,
          masterRoom: false,
          newJoined: true,
          usertype: "participant",
        },
      };
      this.collabChatServiceService.setUserJoinedMessage(msgObj);
    }
  }
  getAuditeeDmsToken() {
    let payload = { username: localStorage.getItem("username") };
    this.httpservice
      .post(ConstUrls.generateDmsSessionForAuditee, payload)
      .subscribe((res: any) => {
        if (res) {
          localStorage.setItem("auditeeDmsToken", res.dmsSession);
          console.log("GENERATE TOKEN FOR DMS");
        }
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.auditId.currentValue) {
      // this.initForm();
      this.auditorChatHistory = [];
      this.auditeeChatHistory = [];
      this.collabChatServiceService
        .getAuditChatroomMessage()
        .subscribe((chatHistory) => {
          if (chatHistory) {
            this.completeChatHistory = chatHistory["messages"];
            this.completeChatHistory.forEach((element) => {
              if (element.masterRoom) {
                console.log(
                  "this.auditorChatHistory:",
                  this.auditorChatHistory
                );
                this.auditorChatHistory.push(element);
              } else {
                this.auditeeChatHistory.push(element);
              }
            });
          }
          let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
          if (userArr) {
            userArr.forEach((usr) => {
              this.auditeeChatHistory.push(usr);
            });
          }
        });
      //get chat history
      this.collabChatServiceService
        .gotAllAuditChatroomMessages()
        .subscribe((data) => {
          if (data["messages"]) {
            this.completeChatHistory = data["messages"];
            this.completeChatHistory.forEach((element) => {
              if (element.masterRoom) {
                this.auditorChatHistory.push(element);
              } else {
                this.auditeeChatHistory.push(element);
              }
            });
            // .filter((msg) => {
            // return msg.usertype === "participant";
            // });
            // console.log("RoomsData:", this.collabChatHistory);
          }
          let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
          if (userArr) {
            userArr.forEach((usr) => {
              this.auditeeChatHistory.push(usr);
            });
          }
        });
    }
  }

  repaint() {
    this.auditorChatHistory = [];
    this.auditeeChatHistory = [];
    this.collabChatServiceService
      .getAuditChatroomMessage()
      .subscribe((chatHistory) => {
        if (chatHistory["messages"]) {
          this.completeChatHistory = chatHistory["messages"];
          this.completeChatHistory.forEach((element) => {
            if (element.masterRoom) {
              console.log("this.auditorChatHistory:", this.auditorChatHistory);
              this.auditorChatHistory.push(element);
            } else {
              this.auditeeChatHistory.push(element);
            }
          });
        }
        let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
        if (userArr) {
          userArr.forEach((usr) => {
            this.auditeeChatHistory.push(usr);
          });
        }
      });
    //get chat history
    this.collabChatServiceService
      .gotAllAuditChatroomMessages()
      .subscribe((data) => {
        if (data["messages"]) {
          this.completeChatHistory = data["messages"];
          this.completeChatHistory.forEach((element) => {
            if (element.masterRoom) {
              this.auditorChatHistory.push(element);
            } else {
              this.auditeeChatHistory.push(element);
            }
          });
          // .filter((msg) => {
          // return msg.usertype === "participant";
          // });
          // console.log("RoomsData:", this.collabChatHistory);
        }
        let userArr = JSON.parse(localStorage.getItem("usersAddedOnline"));
        if (userArr) {
          userArr.forEach((usr) => {
            this.auditeeChatHistory.push(usr);
          });
        }
      });
  }

  checkUrlToken() {}

  openAlert(title, content, link) {
    const initialState = { title: title, content: content, link: link };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  HoverBarBtn() {
    this.NewHoverBarValidateFlag = "ValidatorToken";
    $(".img-f1").addClass("in-active");
    $(".new-grpCloseIco").addClass("active");
  }
  HoverCloseBarBtn() {
    this.NewHoverBarValidateFlag = "";
    $(".new-grpCloseIco").removeClass("active");
    $(".img-f1").removeClass("in-active");
  }
  removeUser(user) {
    // const initialState = {
    //   title: "Confirmation",
    //   content: `Do you want to remove ${user} from current room?`,
    //   link: "Confirm",
    //   action: "Cancel",
    //   confirmFlag: false,
    // };
    // this.modalRef = this.modalService.show(ConfirmPopupComponent, {
    //   initialState,
    //   class: "success-class",
    //   backdrop: "static",
    //   keyboard: false,
    // });
    // this.modalRef.content.onClose.subscribe((r) => {
    //   if (r) {
    //     this.hostList.push(user);
    //     const index: number = this.onlineUsers.indexOf(user);
    //     if (index !== -1) {
    //       this.onlineUsers.splice(index, 1);
    //     }
    //     this.chatroomService.deleteUser(user, this.roomId);
    //     this.openChatroom(this.roomId);
    //   }
    // });
  }
  // openSearch() {
  //   this.validateFlag = "ValidatorToken";
  // }
  closeSearch() {
    this.validateFlag = "";
  }
  selectEvent(item) {
    this.onlineUsers.push(item.username);
    this.chatroomService.addUsersToRoom(item.username, this.roomId);
    this.closeSearch();
  }
  onChangeSearch(val: string) {}

  exportAuditorChat() {
    if (!this.roomId) {
      alert("Please select an audit to continue");
      return;
    }
    // let DATA = this._message_div.nativeElement;
    // let doc = new jsPDF("p", "pt", "a4");

    // let handleElement = {
    //   "#editor": function (element, renderer) {
    //     return true;
    //   },
    // };
    // doc.fromHTML(DATA.innerHTML, 15, 15, {
    //   width: 200,
    //   elementHandlers: handleElement,
    // });

    let DATA = this._message_div.nativeElement;
    let doc = new jsPDF("p", "pt", "a4");
    doc.fromHTML(DATA.innerHTML, 15, 15);
    doc.output("dataurlnewwindow");
    const date = new Date();
    // doc.save(
    //   `${this.collabChatRoomData.title.replace(" ", "-")}-${date.getDate()}-${
    //     date.getMonth() + 1
    //   }-${date.getFullYear()}.pdf`
    // );
    const formData: any = new FormData();
    formData.append(
      "file",
      doc.output(
        `${this.collabChatRoomData.title.replace(" ", "-")}-${date.getDate()}-${
          date.getMonth() + 1
        }-${date.getFullYear()}`
      ),
      "ChatHistory.pdf"
    );

    // var data = { roomId: this.room._id, fileName: files[0]['name'] }
    formData.append("checkedinBy", localStorage.getItem("username")),
      formData.append(
        "metadata",
        "Module-Name=Collaboration,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019"
      ),
      formData.append("routerLink", "/rcm/controlmgmt"),
      formData.append("subModule", "Audit-Chatroom"),
      formData.append("auditId", this.auditId),
      formData.append("module", "Collaboration"),
      formData.append("checkedinDate", moment().format("L"));
    formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
    this.httpservice
      .securePost(ConstUrls.DmsUpload, formData)
      .subscribe((files) => {
        alert("Chat History Uploaded!");
      });
  }
  exportAuditeeChat() {
    if (!this.roomId) {
      alert("Please select an audit to continue");
      return;
    }
    let DATA = this._message_div1.nativeElement;
    let doc = new jsPDF("p", "pt", "a4");

    let handleElement = {
      "#editor": function (element, renderer) {
        return true;
      },
    };
    doc.fromHTML(DATA.innerHTML, 15, 15, {
      width: 200,
      elementHandlers: handleElement,
    });

    // let DATA = this._message_div.nativeElement;
    // let doc = new jsPDF("p", "pt", "a4");
    // doc.fromHTML(DATA.innerHTML, 15, 15);
    // doc.output("dataurlnewwindow");
    const date = new Date();

    doc.save(
      `Auditee-${this.collabChatRoomData.title.replace(
        " ",
        "-"
      )}-${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}.pdf`
    );
  }
  // onFilechanged(event) {
  //   console.log(event.target.files);
  //   var files = [];
  //   const formData: any = new FormData();
  //   files = event.target.files;
  //   console.log("fil anme", files);
  //   formData.append("file", files[0], files[0]["name"]);

  //   // var data = { roomId: this.room._id, fileName: files[0]['name'] }
  //   formData.append("checkedinBy", localStorage.getItem("username")),
  //     formData.append(
  //       "metadata",
  //       "Module-Name=Collaboration,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019"
  //     ),
  //     formData.append("routerLink", "/rcm/controlmgmt"),
  //     formData.append("subModule", "Audit-Chatroom"),
  //     formData.append("auditId", this.auditId),
  //     formData.append("module", "Collaboration"),
  //     formData.append("checkedinDate", moment().format("L"));
  //   formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
  //   this.httpservice
  //     .securePost(ConstUrls.DmsUpload, formData)
  //     .subscribe((files) => {
  //       console.log("Chat History Uploaded!", files);
  //     });
  // }
  submitMessageAuditor(msg) {
    if (msg != "" && msg != " ") {
      if (this.collabChatRoomData) {
        let msgObj = {
          roomId: this.collabChatRoomData.roomId,
          messages: [
            {
              username: this.owner,
              createdAt: Date(),
              message: msg,
              masterRoom: true,
              usertype: "host",
            },
          ],
        };

        this.collabChatServiceService.putAuditChatroomMessage(msgObj);
        this.collabChatServiceService.getAllAuditChatroomMessages(
          this.collabChatRoomData.roomId
        );
        this.message = "";
      }
    }
    //putAuditChatroomMessage
  }
  submitMessageAuditee(msg) {
    if (msg != "" && msg != " ") {
      if (this.collabChatRoomData) {
        let msgObj = {
          roomId: this.collabChatRoomData.roomId,
          messages: [
            {
              username: this.owner,
              createdAt: Date(),
              message: msg,
              masterRoom: false,
              usertype: "host",
            },
          ],
        };

        this.collabChatServiceService.putAuditChatroomMessage(msgObj);
        this.collabChatServiceService.getAllAuditChatroomMessages(
          this.collabChatRoomData.roomId
        );
        this.message = "";
      }
    }
    //putAuditChatroomMessage
  }
  tabOne() {
    setTimeout(function () {
      console.log(
        "[tabOne()] Scrolling to " + $("#_message_div")[0].scrollHeight
      );
      $("#_message_div").scrollTop($("#_message_div")[0].scrollHeight);
    }, 200);
  }

  NewOpenPopupChat() {
    var thisref = this;
    $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
      var target = $(e.target).attr("href"); // activated tab
      if (target == "#nav-home") {
        thisref.roomName = "auditor";
      } else if (target == "#nav-profile") {
        thisref.roomName = "auditee";
      }

      thisref.NewCloseVedioCall();
    });

    this.chatBall = false;
    this.NewValidateFlag = "ValidatorToken";
    setTimeout(function () {
      console.log(
        "[NewOpenPopupChat()] Scrolling to " +
          $("#_message_div")[0].scrollHeight
      );
      $("#_message_div").scrollTop($("#_message_div")[0].scrollHeight);
    }, 100);
  }

  NewClosePopupChat() {
    this.chatBall = true;
    this.NewValidateFlag = "";
    this.NewCloseInTray();
    this.NewCloseVedioCall();
    this.NewCloseWhiteBoard();
  }

  NewMinimizeActionBtn() {
    $(".new-chat-tray").addClass("minimize-tray");
  }

  NewMaximizeActionBtn() {
    $(".new-chat-tray").removeClass("minimize-tray");
  }

  NewVedioCallBtn(userType) {
    this.windowType = "auditor";
    if (userType == "auditee") {
      this.windowType = userType;
    }

    this.NewVedioValidateFlag = "ValidatorToken";
    $(".new-vedio-call").addClass("active-vedio");

    this.NewInTrayValidateFlag = "";
    this.NewWhiteBoardValidateFlag = "";
    $(".new-in-tray").removeClass("active-InTray");
    $(".new-white-board").removeClass("active-WhiteBoard");
  }

  NewCloseVedioCall() {
    this.NewVedioValidateFlag = "";
    $(".new-vedio-call").removeClass("active-vedio");
  }

  NewInTrayBtn() {
    this.NewInTrayValidateFlag = "ValidatorToken";
    $(".new-in-tray").addClass("active-InTray");

    this.NewVedioValidateFlag = "";
    this.NewWhiteBoardValidateFlag = "";
    $(".new-vedio-call").removeClass("active-vedio");
    $(".new-white-board").removeClass("active-WhiteBoard");
  }

  NewCloseInTray() {
    this.NewInTrayValidateFlag = "";
    $(".new-in-tray").removeClass("active-InTray");
  }

  NewWhiteBoardBtn() {
    this.NewWhiteBoardValidateFlag = "ValidatorToken";
    $(".new-white-board").addClass("active-WhiteBoard");

    this.NewVedioValidateFlag = "";
    this.NewInTrayValidateFlag = "";
    $(".new-vedio-call").removeClass("active-vedio");
    $(".new-in-tray").removeClass("active-InTray");
    this.collabChatServiceService.sendAuditId(this.auditId);
  }

  NewCloseWhiteBoard() {
    this.NewWhiteBoardValidateFlag = "";
    $(".new-white-board").removeClass("active-WhiteBoard");
  }

  // NewChatRoomBtn() {
  //   this.NewChatRoomValidateFlag = 'ValidatorToken';
  // }

  NewCloseDocRoom() {
    this.NewChatRoomValidateFlag = "";
  }

  openPopupChat() {
    this.validateFlag = "ValidatorToken";
  }

  closePopupChat() {
    this.validateFlag = "";
  }

  openActionBtnExport() {}

  closeActionBtnExport() {
    this.validateActionBtnFlag = "";
    $(".quick-action-btns").removeClass("minimize");
    $(".chat-popup").removeClass("minimize");
  }

  minimizeActionBtnExport() {
    this.validateFlagMinimize = "ValidatorToken";
    this.validateFlagMaximize = "ValidatorToken";
    $(".quick-action-btns").addClass("minimize");
  }

  maximizeActionBtnExport() {
    this.validateFlagMinimize = "";
    this.validateFlagMaximize = "";
    $(".quick-action-btns").removeClass("minimize");
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
    this.validateFlag = "";
    this.validateActionBtnFlag = "ValidatorToken";
    $(".chat-popup").addClass("minimize");
    setTimeout(function () {
      console.log(
        "[selectTab()] Scrolling to " + $("#_message_div")[0].scrollHeight
      );
      $("#_message_div").scrollTop($("#_message_div")[0].scrollHeight);
    }, 200);
  }

  showMail() {
    this.isShown = true;
    this.firstImg = false;
    this.secoundImg = true;
    this.inputDetail = true;
    this.auditgp = false;
    this.gropClose = false;
    this.gropmain = true;
  }
  auditeGroupBtn() {
    this.auditgp = true;
    this.gropClose = true;
    this.gropmain = false;
    this.isShown = false;
    this.firstImg = true;
    this.secoundImg = false;
  }
  CloseauditeGroupBtn() {
    this.auditgp = false;
    this.gropClose = false;
    this.gropmain = true;
  }
  hideMail() {
    this.isShown = false;
    this.firstImg = true;
    this.secoundImg = false;
    this.inputDetail = true;
    this.auditgp = false;
  }
  sendMail() {
    console.log("this.inviteMailId", this.inviteMailId);

    if (this.validateMail(this.inviteMailId)) {
      //change this static payload
      var payload = {
        invited_by: this.owner,
        auditee_mail_id: this.inviteMailId,
        audit_id: this.auditId,
      };
      console.log("payload", payload);
      // this.openAlert('Mail sent',payload.auditee_mail_id,'Copy Link');
      this.httpservice
        .post(ConstUrls.generateAuditeeToken, payload)
        .subscribe((res) => {
          this.collabChatServiceService.getCollabChatRoom(this.auditId);
          console.log("sendmail", res);
          this.inviteMailId = "";
          this.resultMessage = res["message"];
          this.openAlert(
            "Mail sent",
            `http://${location.host}/auditee-chatroom?urlToken=${res["token"]}`,
            "Copy Link"
          );
          this.inputDetail = false;
          this.resultSend = true;
          this.invalidMail = false;
          this.secoundImg = false;
          this.firstImg = true;
          $(".invite-people").addClass("send-detail");
          setTimeout(
            function () {
              this.resultSend = false;
              this.isShown = false;
              $(".invite-people").removeClass("send-detail");
            }.bind(this),
            3000
          );
        });
    } else {
      // setTimeout(function () {
      //   $(".error-mesg").fadeTo(500, 500).slideUp(500, function () {
      //   $(".error-mesg").remove();
      //   });
      // }, 1000);//1000=1 seconds
      this.inputDetail = true;
      this.resultSend = false;
      this.inviteMailId = "";
      this.invalidMail = true;
    }
  }

  validateMail(mailId) {
    let regexp = new RegExp(
      /^(([^!#$%&*'<>()\[\]\\.,;:\s@"]+(\.[^!#$%&*'<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    // const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result = regexp.test(mailId);
    console.log("result", result);
    return result;
  }
  //close session
  closeSession() {
    this.httpservice
      .securePost(ConstUrls.closeAuditeeSession, { audit_id: this.auditId })
      .subscribe(
        (res) => {
          this.endSession = true;
          this.collabChatServiceService.getCollabChatRoom(this.auditId);
          console.log("Session closed successfully");
        },
        (err) => {
          console.log("Unable to close session, try again");
        }
      );
  }
}
