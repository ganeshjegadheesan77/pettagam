import { Component, OnInit } from '@angular/core';
// import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { ConfigService } from 'src/config.service';
export interface DialogData {
  fileName: string;
  type: string;
}
@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.less']
})
export class FilePreviewComponent implements OnInit {
  uri;
  fullPath: any;
    item : any;
    title: any;
    folder:any;
    pdftype = false;
    constructor(
    private configService:ConfigService

    ) {
    this.uri = configService.config.baseUrl;

    }       
      closeDialog(): void {
    }
    ngOnInit() {
      //console.log('title:',this.title)
      if(this.folder=='controlTest'){
        this.fullPath=this.uri+'uploads/controlTest/'+this.title; 
      }
      else if(this.folder=='caseMgmt'){
          // this.fullPath=this.uri+'uploads/v/'+this.title; 
            }
             else{
              this.fullPath=this.uri+'uploads/inTray/'+this.title;
              }       

      }
  }
  