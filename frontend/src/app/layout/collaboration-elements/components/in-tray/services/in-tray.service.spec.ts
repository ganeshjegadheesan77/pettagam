import { TestBed } from '@angular/core/testing';

import { InTrayService } from './in-tray.service';

describe('InTrayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InTrayService = TestBed.get(InTrayService);
    expect(service).toBeTruthy();
  });
});
