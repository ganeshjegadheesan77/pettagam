import { Injectable } from "@angular/core";
import io from "socket.io-client";
import { Observable, BehaviorSubject } from "rxjs";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};
@Injectable({
  providedIn: "root"
})
export class InTrayService {
  inTray = true;
  viewFileRoom = false;
  private socket;
  counter: any;
  count: BehaviorSubject<boolean>;
  newFileName: BehaviorSubject<string>;

  constructor(private http: HttpClient,private configService:ConfigService) {
    this.count = new BehaviorSubject(this.counter);
    this.socket = io(configService.config.socket);
  }
  createFileRoom(room) {
    this.socket.emit("createFileRoom", room, httpOptions);
  }
  /* 
  data = {
    roomId : ''.
    name : '',
    isOpen : ''
  }
  */
  updateFileStatus(data) {
    this.socket.emit("updateFile", data, httpOptions);
  }

  // roomFileUpdate() {
  //   let observable = new Observable<{ data: string }>(observer => {
  //     this.socket.on("updatedFile", data => {
  //       observer.next(data);
  //     });
  //     return () => {
  //       this.socket.disconnect();
  //     };
  //   });
  //   return observable;
  // }
  updateFileroomDetails(roomDetails) {
    this.socket.emit("addFile", roomDetails, httpOptions);
  }
  fileroomDetails() {
    let observable = new Observable<{ data: string }>(observer => {
      this.socket.on("getFileRoomDetails", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  addUsersToFileroom(username, roomId) {
    this.socket.emit("addUsersToFileroom", { username, roomId }, httpOptions);
  }
  gotFileRoomsData() {
    let observable = new Observable<{ data: string }>(observer => {
      this.socket.on("getFileRoomsList", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  readUserList() {
    let observable = new Observable<[]>(observer => {
      this.socket.on("fileUserList", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  getRoomDetails() {
    let observable = new Observable<[]>(observer => {
      this.socket.on("getRoomDetails", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  gotSystemUsers() {
    let observable = new Observable<[]>(observer => {
      this.socket.on("getSystemUsers", data => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }
  getFileRoomsData(username) {
    this.socket.emit("getFileRoomsList", username, httpOptions);
  }
  getRoomDetailsById(fileroomId) {
    this.socket.emit("getRoomDetailsById", fileroomId, httpOptions);
  }
  getSystemUsers() {
    this.socket.emit("getSystemUsers", httpOptions);
  }
  /* data definitation
  data = {
    id : d65062sdhj15ga763s,
    fileName : "test.pdf"
  }
  */  deleteFileroom(data) {
    this.socket.emit("deleteFileroom", data, httpOptions);
  }
  deleteFile(fileName) {
    //console.log('file came in service to delete!')
    this.socket.emit("deleteFileroom", fileName, httpOptions);

  }
}
