import { Component, OnInit } from "@angular/core";
import { FilePreviewModel } from "ngx-awesome-uploader";
import { BsModalService } from "ngx-bootstrap/modal";
import { FilePickerAdapterClass } from "../tray/file-upload.adapter";
import { ValidationError } from "ngx-awesome-uploader";
import { HttpClient } from "@angular/common/http";
import { FilePreviewComponent } from "../file-preview/file-preview.component";
import { InTrayService } from "../services/in-tray.service";
import { ChatroomService } from "../../chat-system/services/chatroom.service";
import { ConfigService } from 'src/config.service';
@Component({
  selector: "app-tray",
  templateUrl: "./tray.component.html",
  styleUrls: ["./tray.component.less"]
})
export class TrayComponent implements OnInit {
  public myFiles: FilePreviewModel[] = [];
  fullScreen = false;
  fileNames: any[];
  systemUsers: any[];
  inTray = true;
  viewFileRoom = false;
  usernames: any = [];
  fileroomDetails: any = [];
uri;
  roomDetails: any = {};
  /////fileUpload
  showdiv = false;
  private modals: any[] = [];
  slateArr = [];
  // myFiles: any;
  itemTemplate;
  onlineUsers: any = [];
  //room Details
  constructor(
    private http: HttpClient,
    private modalService: BsModalService,
    public chatroomService: ChatroomService,
    public inTrayService: InTrayService,
    private configService:ConfigService
  ) {

    this.uri = configService.config.baseUrl;

    this.inTrayService.getSystemUsers();
  }
  adapter = new FilePickerAdapterClass(this.http ,this.configService);
  closeDialog() {
    this.fullScreen = false;
  }
  ngOnInit() {
    this.inTrayService.gotSystemUsers().subscribe(data => {
      this.systemUsers = data;
    });
    this.inTrayService.fileroomDetails().subscribe(data =>{
      this.roomDetails = data[0];
    });
  }
  openAttachment(myFile) {
    const initialState = {
      title: myFile,
      class: "modal-xl"
    };
    this.modalService.show(FilePreviewComponent, { initialState });
  }
  roomsOpen() {
    this.inTray = false;
    this.viewFileRoom = true;
  }

  selectUsers(username) {
    //console.log("user selected:", username, this.roomDetails._id);

    this.inTrayService.addUsersToFileroom(username, this.roomDetails._id);
  }
 
  async onFileAdded(file: FilePreviewModel, roomId) {
    let newFile = {
      fileName: await file.fileName,
      roomId: roomId
    };
    //console.log("In tray: newFile:", newFile);
    await this.inTrayService.updateFileroomDetails(newFile);
  }

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }
  removeFile(myFile){
    let data = {
      id:this.roomDetails._id,
      fileName:myFile 
    }
    this.inTrayService.deleteFile(data);
  }
}
