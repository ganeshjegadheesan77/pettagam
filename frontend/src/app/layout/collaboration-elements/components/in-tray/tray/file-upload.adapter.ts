import { FilePreviewModel } from "ngx-awesome-uploader";
import {
  HttpRequest,
  HttpClient,
  HttpEvent,
  HttpEventType
} from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { FilePickerAdapter } from "ngx-awesome-uploader";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

export class FilePickerAdapterClass extends FilePickerAdapter {
  uri;
  constructor(private http: HttpClient,private configService:ConfigService) {
       
    super();
    this.uri = configService.config.baseUrl;

  }
  public uploadFile(fileItem: FilePreviewModel) {
    const form = new FormData();
    form.append("file", fileItem.file);
    const api = `${this.configService.config.baseUrl}multipleuploadfile`;
    const req = new HttpRequest("POST", api, form, { reportProgress: true });
    return this.http.request(req).pipe(
      map((res: HttpEvent<any>) => {
        if (res.type === HttpEventType.Response) {
          return res.body.id.toString();
        } else if (res.type === HttpEventType.UploadProgress) {
          // Compute and show the % done:
          const UploadProgress = +Math.round((100 * res.loaded) / res.total);
          return UploadProgress;
        }
      })
    );
  }
  public removeFile(fileItem): Observable<any> {
    const removeApi = `${this.configService.config.baseUrl}removeImage/${fileItem.fileName}`;
    return this.http.get(removeApi, {});
  }
}
