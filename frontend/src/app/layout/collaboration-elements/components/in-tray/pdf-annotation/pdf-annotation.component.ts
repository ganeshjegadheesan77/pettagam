import { Component, ViewChild, OnInit, ElementRef, SimpleChanges, AfterViewInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
declare const WebViewer: any;
import { HttpService } from '../../../../../core/http-services/http.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { SaveDocumentPopupComponent } from "src/app/layout/modal/save-document-popup/save-document-popup.component";
import { ConstUrls } from "src/app/config/const-urls";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

@Component({
  selector: 'app-pdf-annotation',
  templateUrl: './pdf-annotation.component.html',
  styleUrls: ['./pdf-annotation.component.less']
})
export class PdfAnnotationComponent implements OnInit, AfterViewInit, OnChanges {
  uri;

  constructor(public httpService: HttpService, private modalService: BsModalService,
    private configService:ConfigService
    
    ) { 
    this.uri = configService.config.baseUrl;

    }
  // tslint:disable-next-line: no-input-rename
  @Input('path') path;
  @Input('filename') filename;
  @Input('closeForm') closeForm;
  @Input('documentNo') documentNo;
  @Input('auditName') auditName: any;
  @Input('wpNumber') wpNumber;
  @Input('auditId') auditId;
  @Input('roomName') roomName;
  @Input('checkInBy') checkInByUser;
  @Input('userType') userType;
  // @Input('closeOpenFIle') closeOpenFIle : boolean;
  modalRef: any;
  @Output() close = new EventEmitter<boolean>();
  @Output() closeOpenFileFlag = new EventEmitter<boolean>();
  @Output() closefile = new EventEmitter();
  @ViewChild('viewer', { static: false }) viewer: ElementRef;
  workPaperNumber: string;
  currentUser: string;
  blobObj: any;
  show = true;
  test = '';
  extension = '';

  closeDialog() {
    this.close.emit(true);
    this.closefile.emit(this.filename);
    this.closeOpenFileFlag.emit(false);
  }
  ngOnChanges(changes: SimpleChanges) {
    let fileArr = this.filename.split('.');
    this.extension = fileArr[fileArr.length - 1];
    if (!this.wpNumber) {
      this.httpService.secureGet(ConstUrls.getWorkpaperNumber, { prefix: 'WP' }).subscribe(obj => {
        let incrementedNumber = String(Number(obj[0]["lastId"]) + 1)
        this.workPaperNumber = "WP-" + incrementedNumber;
        this.httpService.securePost(ConstUrls.setWorkPaperNumber, { prefix: 'WP', lastId: incrementedNumber }).subscribe(o => { });
      });
    }

  }
  commonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  addWpNumberOnPdf(Annotations, annotManager, docViewer, instance) {
    docViewer.one('documentLoaded', async () => {
      try {
        if (this.workPaperNumber) {
          this.show = true;
          const freeText = new Annotations.FreeTextAnnotation();
          freeText.PageNumber = 1;
          freeText.X = 437;
          freeText.Y = 15;
          freeText.Width = 125;
          freeText.Height = 44;
          freeText.setPadding(new Annotations.Rect(0, 0, 0, 0));
          freeText.setContents(`WP-${this.workPaperNumber} \nCreated By: ${this.currentUser}\nApproved By:      `);
          freeText.FillColor = new Annotations.Color(255, 255, 255);
          freeText.FontSize = '10pt';

          annotManager.addAnnotation(freeText);
          annotManager.redrawAnnotation(freeText);
          const doc = docViewer.getDocument();
          const xfdfString = await annotManager.exportAnnotations();
          const options = { xfdfString, flatten: true };
          this.show = false;
          const data = await doc.getFileData(options);
          const arr = new Uint8Array(data);
          const blob = new Blob([arr], { type: 'application/pdf' });
          // docViewer.off('documentLoaded');
          instance.loadDocument(blob, {
            extension: 'pdf',
          });
        }
      } catch (e) {
        console.log(e);
      }
    })

  }
  ngAfterViewInit(): void {
    this.currentUser = localStorage.getItem("username");
    WebViewer({
      path: '../../../../../../lib',
      // initialDoc: this.path,
      // extension: this.extension,
      backendType: 'ems',
      fullAPI: true,
      ui: 'legacy'
      // licenseKey:'true'
    }, document.getElementById('viewer')).then(instance => {
      try {
        instance.setFitMode(instance.FitMode.FitWidth);
        const { CoreControls, docViewer, Annotations, annotManager } = instance;
        this.createCustomRuberStamp(docViewer);
        annotManager.setCurrentUser(this.currentUser);
        if (this.wpNumber) {
          this.show = false;
          instance.loadDocument(this.path, { extension: this.extension });
        } else {
          if (this.extension == "pdf") {
            this.show = false;
            instance.loadDocument(this.path, { extension: this.extension });
            this.addWpNumberOnPdf(Annotations, annotManager, docViewer, instance);
          } else {
            //l is undefined for now(licence)
            CoreControls.office2PDFBuffer(this.path, { l: undefined, extension: this.extension }).then(buffer => {
              const arr = new Uint8Array(buffer);
              const blob = new Blob([arr], { type: 'application/pdf' });
              this.show = false;
              instance.loadDocument(blob, { filename: 'myfile.pdf' });
              this.addWpNumberOnPdf(Annotations, annotManager, docViewer, instance);
            })
          }

          // this.convertOfficeToPDF(PDFNet, CoreControls, this.path, instance);
        }
        // Add header button that will get file data on click
        instance.setHeaderItems(header => {
          header.push({
            type: 'actionButton',
            title: 'Upload Changes to PDF',
            img: '../../../../../assets/img/pdf-ico.svg',
            onClick: async () => {
              const doc = docViewer.getDocument();
              const xfdfString = await annotManager.exportAnnotations();
              const data = await doc.getFileData({
                // saves the document with annotations in it
                xfdfString
              });
              const arr = new Uint8Array(data);
              const blob = new Blob([arr], { type: 'application/pdf' });
              let name;
              if (this.wpNumber) {
                name = this.filename;
                this.uploadChangesToPdf(blob, name);
              } else {
                const initialState = {
                  title: "Save As Workpaper",
                  content: `Save as`,
                  link: "Save",
                  action: "Cancel",
                  confirmFlag: false,
                  workPaperNumber: this.workPaperNumber,
                  auditId: this.auditId,
                  fileName: this.filename.split('.')[0]
                };
                this.modalRef = this.modalService.show(SaveDocumentPopupComponent, {
                  initialState,
                  class: "success-class",
                  backdrop: "static",
                  keyboard: false,
                });
                this.modalRef.content.onClose.subscribe((newName) => {
                  if (newName) {
                    name = `${this.workPaperNumber}_${this.auditId}_${newName}.pdf`
                    this.uploadChangesToPdf(blob, name);
                  }
                });
              }
            }
          });
        });
      } catch (error) {
        console.log("error happens----->", error);
        this.show = false;
      }
    })
  }
  uploadChangesToPdf(blob, name) {
    const formData: any = new FormData();
    formData.append("file", blob, name);
    formData.append("dmsSessionId", localStorage.getItem('dmsToken'));
    formData.append("docId", this.documentNo);
    formData.append("doctype", "workPaper");
    formData.append("filename", name);
    formData.append("workPaperNumber", this.wpNumber ? this.wpNumber : this.workPaperNumber);
    formData.append("comments", `${this.currentUser} changes file from ${this.filename} to ${name}`);
    this.httpService.securePost(ConstUrls.replaceDocuments, formData).subscribe((x) => {
      let payload = {
        auditId: this.auditId,
        currentUser: this.checkInByUser,
        oldfileName: this.filename,
        newFileName: name,
        userType: this.userType,
        roomType: this.roomName
      }
      this.httpService.securePost(ConstUrls.setFileNameCollab, payload).subscribe((s) => {
        this.close.emit(true);
        setTimeout(() => {
          let message = this.wpNumber ? 'Document updated Successfully.' : 'Work paper created successfully.'
          this.commonAlert("Success", message);
        }, 1000);

      })
    })
  }
  createCustomRuberStamp(docViewer) {
    const tool = docViewer.getTool('AnnotationCreateRubberStamp');
    tool.setStandardStamps([
      `${this.configService.config.baseUrl}uploads/Minor_Execptions_Noted.png`,
      `${this.configService.config.baseUrl}uploads/No_Exeptions_Noticed.png`,
      `${this.configService.config.baseUrl}uploads/Observed.png`,
      `${this.configService.config.baseUrl}uploads/Queries_Outstanding.png`,
      `${this.configService.config.baseUrl}uploads/Reperformance_Done.png`,
      `${this.configService.config.baseUrl}uploads/Walkthourgh_Done.png`,
      `${this.configService.config.baseUrl}uploads/Walkthourgh_Not_Done.png`,
      `${this.configService.config.baseUrl}uploads/legendChart.png`
    ]);
  }
  // annotateBackground = async (PDFNet, inputPath, instance) => {
  //   const main = async () => {
  //     try {
  //       const ret = 0;
  //       let doc;
  //       if (typeof inputPath === "string") {
  //         doc = await PDFNet.PDFDoc.createFromURL(inputPath);
  //       } else {
  //         doc = await PDFNet.PDFDoc.createFromBuffer(inputPath);
  //       }
  //       doc.initSecurityHandler();
  //       doc.lock();
  //       await PDFNet.startDeallocateStack();  // start stack-based deallocation. All objects will be deallocated by end of function
  //       // await AnnotationLowLevelAPI(doc);
  //       const stamper = await PDFNet.Stamper.create(PDFNet.Stamper.SizeType.e_relative_scale, 0.15, 0.15); // Stamp size is relative to the size of the crop box of the destination page
  //       stamper.setAlignment(PDFNet.Stamper.HorizontalAlignment.e_horizontal_right, PDFNet.Stamper.VerticalAlignment.e_vertical_top);
  //       const redColorPt = await PDFNet.ColorPt.init(1, 0, 0);
  //       stamper.setFontColor(redColorPt);
  //       const pgSet = await PDFNet.PageSet.createRange(1, 1); // for all page print ==> await doc.getPageCount()
  //       stamper.stampText(doc, `${this.workPaperNumber}   \nCreated By : ${this.currentUser}              \nReviewed By : `, pgSet);
  //       await PDFNet.endDeallocateStack();
  //       const docbuf = await doc.saveMemoryBuffer(PDFNet.SDFDoc.SaveOptions.e_linearized);
  //       const blob = new Blob([docbuf], { type: 'application/pdf' });
  //       this.blobObj = blob;
  //       this.show = false;
  //       instance.loadDocument(blob, {
  //         filename: `${this.workPaperNumber}_${this.auditId}_${this.auditName}.pdf`
  //       });
  //       return ret;
  //     } catch (err) {
  //       this.show = false;
  //       console.log("error while annotating", err);
  //     }
  //   };
  //   // add your own license key as the second parameter, e.g. PDFNet.runWithCleanup(main, 'YOUR_LICENSE_KEY')
  //   PDFNet.runWithCleanup(main);
  // };
  // convertOfficeToPDF(PDFNet, CoreControls, inputUrl, instance, l?) {
  //   if (this.extension == "pdf") {
  //     this.annotateBackground(PDFNet, inputUrl, instance);
  //   } else {
  //     CoreControls.office2PDFBuffer(inputUrl, { l, extension: this.extension }).then(buffer => {
  //       this.annotateBackground(PDFNet, buffer, instance);
  //     })
  //   }

  // }
  ngOnInit() {

  }


}