import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfAnnotationComponent } from './pdf-annotation.component';

describe('PdfAnnotationComponent', () => {
  let component: PdfAnnotationComponent;
  let fixture: ComponentFixture<PdfAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
