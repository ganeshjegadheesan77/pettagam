import { Component, OnInit } from "@angular/core";
import { FilePickerAdapterClass } from "../tray/file-upload.adapter";
import { ValidationError } from "ngx-awesome-uploader";
import { HttpClient } from "@angular/common/http";
import { FilePreviewModel } from "ngx-awesome-uploader";
import { ConfigService } from 'src/config.service';

@Component({
  selector: "app-file-upload",
  templateUrl: "./file-upload.component.html",
  styleUrls: ["./file-upload.component.less"]
})
export class FileUploadComponent implements OnInit {
  showdiv = false;
  private modals: any[] = [];
  slateArr = [];
  myFiles: any;
  itemTemplate;
  constructor(private http: HttpClient,
    private configService:ConfigService
    ) {}
  adapter = new FilePickerAdapterClass(this.http,this.configService);
  ngOnInit() {}
  async onFileAdded(file: FilePreviewModel) {
    // let fileName = await file.fileName;
    // await this.dialogRef.close({ data: fileName });
  }
  // drop(event: CdkDragDrop<string[]>) {
  //   moveItemInArray(this.slateArr, event.previousIndex, event.currentIndex);
  // }

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }
}
