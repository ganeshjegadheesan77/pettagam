import { Component, OnInit } from "@angular/core";
// import { MatDialogRef } from "@angular/material";
// import { ChatroomService } from "../../chat-system/services/chatroom.service";
import { InTrayService } from "../services/in-tray.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ChatroomService } from '../../chat-system/services/chatroom.service';

@Component({
  selector: "app-file-room",
  templateUrl: "./file-room.component.html",
  styleUrls: ["./file-room.component.less"]
})
export class FileRoomComponent implements OnInit {
  roomTitle;
  rooms: any = [];
  viewFileRoom = true;
  inTray = false;
  fileroomId: any = "";
  fileroomDetails: any = { fileName: [] };
  //Constructor
  constructor(
    public inTrayService: InTrayService,
    private modalService: BsModalService,
    public chatroomService: ChatroomService,

  ) {
    // this.inTrayService.getFileRoomsData(localStorage.getItem("username"));
    this.chatroomService.getRoomsData(localStorage.getItem("username"));
  }

  ngOnInit() {

    this.inTrayService.fileroomDetails().subscribe(data => {
      this.fileroomDetails = data[0];
    });

    this.chatroomService.gotRoomsData().subscribe(data => {
      this.rooms = data || [];
    });
  }
  
  openFileroom(id) {
    this.inTrayService.getRoomDetailsById(id);
    this.viewFileRoom = false;
    this.inTray = true;
  }
  addRoom(r) {
    if (r != " " && r != "") {
      let ro = { title: r, owner: localStorage.getItem("username") };
      //console.log("ro", ro);
      this.inTrayService.createFileRoom(ro);
      this.roomTitle = "";
    } else {
      this.roomTitle = "";
    }
  }

}
