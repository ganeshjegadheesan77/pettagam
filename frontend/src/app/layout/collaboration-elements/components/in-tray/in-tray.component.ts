import { Component, OnInit } from "@angular/core";
import { InTrayService } from "./services/in-tray.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { FilePreviewComponent } from "./file-preview/file-preview.component";
import { ChatroomService } from '../chat-system/services/chatroom.service';

@Component({
  selector: "app-in-tray",
  templateUrl: "./in-tray.component.html",
  styleUrls: ["./in-tray.component.less"]
})
export class InTrayComponent implements OnInit {
  fileroomList: any = [];
  fileroomId: any = "";
  fileroomDetails: any = { fileName: [] };
  constructor(
    public inTrayService: InTrayService,
    private modalService: BsModalService,
    public chatroomService: ChatroomService,

  ) {
    // this.inTrayService.getFileRoomsData(localStorage.getItem("username"));
    this.chatroomService.getRoomsData(localStorage.getItem("username"));

  }

  ngOnInit() {
    this.chatroomService.gotRoomsData().subscribe(data => {
      this.fileroomList = data || [];
      if (this.fileroomId == "") {
        this.fileroomId = data && data[0] ? data[0]._id : "";
        this.fileroomDetails = data[0];
        localStorage.setItem("fileroomId", this.fileroomId);
      }
    });
   
    this.inTrayService.fileroomDetails().subscribe(data => {
      this.fileroomDetails = data[0];
    });
  }
  selectFilerooms(fileroomName) {
    let ro = this.fileroomList.filter(r => fileroomName === r.title);
    this.fileroomId = ro[0]._id;
    localStorage.setItem("fileroomId", this.fileroomId);
    this.inTrayService.getRoomDetailsById(ro[0]._id);
  }
  openAttachment(myFile) {
    const initialState = {
      title: myFile,
      class: "modal-xl"
    };
    this.modalService.show(FilePreviewComponent, { initialState });
  }
}
