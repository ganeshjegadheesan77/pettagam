import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditChatPopupSecComponent } from './audit-chat-popup-sec.component';

describe('AuditChatPopupSecComponent', () => {
  let component: AuditChatPopupSecComponent;
  let fixture: ComponentFixture<AuditChatPopupSecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditChatPopupSecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditChatPopupSecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
