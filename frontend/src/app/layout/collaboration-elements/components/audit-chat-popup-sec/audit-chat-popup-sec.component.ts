import { Component, OnInit } from "@angular/core";
import { ChatroomService } from "../chat-system/services/chatroom.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { CollabChatServiceService } from "../../collabServices/collab-chat-service.service";

@Component({
  selector: "app-audit-chat-popup-sec",
  templateUrl: "./audit-chat-popup-sec.component.html",
  styleUrls: ["./audit-chat-popup-sec.component.less"],
})
export class AuditChatPopupSecComponent implements OnInit {
  roomDetails: any = [];
  rooms: any = [];
  roomId: any = [];
  onlineUsers: any = [];
  messages: any = [];
  usernames: any = [];
  owner: any = [];
  message: string = "";
  validateFlag: string;
  keyword = "username";
  modalRef: any;
  NewHoverBarValidateFlag: string;

  constructor(
    public chatroomService: ChatroomService,
    private modalService: BsModalService,
    private collabChatServiceService: CollabChatServiceService
  ) {
    this.chatroomService.getValue().subscribe((id) => {
      if (id == "") {
        this.owner = localStorage.getItem("username");
        this.chatroomService.readInitUserList();
        this.chatroomService.getRoomsData(this.owner);
        this.chatroomService.readUserList().subscribe((data) => {
          this.usernames = data;
        });

        this.getConstructorData();
      } else {
        this.openChatroom(id);
      }
    });
  }

  ngOnInit() {}

  HoverBarBtn() {
    this.NewHoverBarValidateFlag = "ValidatorToken";
    $(".img-f1").addClass("in-active");
    $(".new-grpCloseIco").addClass("active");
  }

  HoverCloseBarBtn() {
    this.NewHoverBarValidateFlag = "";
    $(".new-grpCloseIco").removeClass("active");
    $(".img-f1").removeClass("in-active");
  }

  openSearch() {
    this.validateFlag = "ValidatorToken";
  }

  closeSearch() {
    this.validateFlag = "";
  }
  //add user on select event
  selectEvent(item) {
    this.onlineUsers.push(item.username);
    this.chatroomService.addUsersToRoom(item.username, this.roomId);
    this.closeSearch();
  }

  onChangeSearch(val: string) {}

  submitMessage(msg) {
    if (msg != "" && msg != " ") {
      let msgObj = {
        roomId: "OP-036",
        messages: [
          {
            username: "randomemail99@gmail.com",
            createdAt: Date(),
            message: msg,
          },
        ],
      };

      this.collabChatServiceService.putAuditChatroomMessage(msgObj);
      this.message = "";
    }
    //putAuditChatroomMessage
  }

  //remove user from chatroom
  removeUser(user) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove ${user} from current room?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.usernames.push(user);
        const index: number = this.onlineUsers.indexOf(user);
        if (index !== -1) {
          this.onlineUsers.splice(index, 1);
        }
        this.chatroomService.deleteUser(user, this.roomId);
        this.openChatroom(this.roomId);
      }
    });
  }

  openChatroom(id) {
    this.roomId = id;
    this.chatroomService.getChatRoomDetails(id);
    this.chatroomService.getChatMessages(id);
    this.chatroomService.gotChatRoomDetails().subscribe((data) => {
      this.roomDetails = data[0];
      this.onlineUsers = this.roomDetails.usernames;
      this.chatroomService.getOnlineStatus(data[0].usernames);
      if (data[0].owner == localStorage.getItem("username")) {
        this.chatroomService.readInitUserList();
      }
    });
    this.chatroomService.readMessages().subscribe((data) => {
      this.messages = data;
      // setTimeout(() => {
      //   let objDiv = document.getElementById("message_div");
      //   objDiv.scrollTop = objDiv.scrollHeight;
      // }, 100);
    });
  }

  async getConstructorData() {
    this.chatroomService.gotRoomsData().subscribe((data) => {
      if (data) {
        this.rooms = data || [];
        this.roomDetails = data[0];
        this.roomId = this.roomDetails._id;
        this.onlineUsers = this.roomDetails.usernames;
        this.chatroomService.getChatMessages(this.roomId);
        // setTimeout(() => {
        //   let objDiv = document.getElementById("message_div");
        //   objDiv.scrollTop = objDiv.scrollHeight;
        // }, 100);
      }
    });
    await this.chatroomService.readMessages().subscribe((data) => {
      this.messages = data;
    });
    // await this.chatroomService.getChatMessages(this.roomDetails._id);
    await this.chatroomService.readUserList().subscribe((data) => {
      let users = [];
      if (
        this.roomDetails &&
        this.roomDetails.usernames &&
        this.roomDetails.usernames.length
      ) {
        users = data.filter((ds) => {
          let rs: any = ds;
          return this.roomDetails.usernames.indexOf(rs.username) == -1;
        });
        users = users.filter((us) => us.username != this.roomDetails.owner);
        this.usernames = users;
      } else {
        this.usernames = data;
      }
    });
  }
}
