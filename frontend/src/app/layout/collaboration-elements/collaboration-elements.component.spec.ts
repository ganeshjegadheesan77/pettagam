import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationElementsComponent } from './collaboration-elements.component';

describe('CollaborationElementsComponent', () => {
  let component: CollaborationElementsComponent;
  let fixture: ComponentFixture<CollaborationElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
