import { Component, OnInit } from "@angular/core";
import { WhiteboardComponent } from "./components/whiteboard/whiteboard.component";
import { BsModalService } from "ngx-bootstrap/modal";
import { RoomsComponent } from "./components/chat-system/rooms/rooms.component";
import { ChatroomService } from "./components/chat-system/services/chatroom.service";
import { FileRoomComponent } from "./components/in-tray/file-room/file-room.component";

@Component({
  selector: "app-collaboration-elements",
  templateUrl: "./collaboration-elements.component.html",
  styleUrls: ["./collaboration-elements.component.less"],
})
export class CollaborationElementsComponent implements OnInit {
  roomList: any = [];
  fileroomList: any = [];
  messages: any = [];
  message: any = "";
  roomId: any = "";
  fileroomId: any = "";
  fileroomDetails: any = { fileName: [] };
  success: any;
  loadChat = false;

  constructor(
    private modalService: BsModalService,
    public chatroomService: ChatroomService
  ) {}

  ngOnInit() {}
  //   this.inTrayService.fileroomDetails().subscribe(data => {
  //     this.fileroomDetails = data[0];
  //   });
  openWhiteboard() {
    this.modalService.show(WhiteboardComponent, { class: "modal-lg" });
  }
  openChatroom() {
    this.modalService.show(RoomsComponent, { class: "modal-lg" });
  }
  openAttachment() {
    this.modalService.show(FileRoomComponent, { class: "modal-lg" });
  }
  showChat() {
    this.loadChat = true;
    $(".chat-popup-maincontainer").removeClass("d-none");
  }
  showScreenshot() {}
}
