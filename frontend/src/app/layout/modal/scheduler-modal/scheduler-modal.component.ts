import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Item, Period, Section, Events, NgxTimeSchedulerService } from 'ngx-time-scheduler';
import * as moment from 'moment';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';
import { CalendarDataService } from 'src/app/featured/admin/calendar-data.service';
@Component({
  selector: 'app-scheduler-modal',
  templateUrl: './scheduler-modal.component.html',
  styleUrls: ['./scheduler-modal.component.less']
})
export class SchedulerModalComponent implements OnInit {
  events: Events = new Events();
  periods: Period[];
  sections: Section[];
  items: Item[];
  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  confirmFlag: Boolean;
  public onClose: Subject<Boolean>;
  name = "...";
  constructor(private bsModalRef: BsModalRef,
    private router: Router, private service: NgxTimeSchedulerService, private calendarDataService: CalendarDataService) { }
  ngOnInit() {
    this.calendarDataService.data.subscribe(d => {
      this.name = d;
    });

    this.calendarDataService.calendarData.subscribe(cal => {
      if (cal.items) {
        this.items = cal.items;
        this.periods = cal.periods;
        this.sections = cal.sections;
        this.service.refresh();
      }
    });

    // this.periods = [
    //   {
    //     name: '1 week',
    //     timeFrameHeaders: ['ddd DD'],
    //     classes: '',
    //     timeFrameOverall: 1440 * 7,
    //     timeFramePeriod: 1440,
    //   }
    // ];

    // this.sections = [{
    //   name: 'Sharon',
    //   id: 1
    // }, {
    //   name: 'Jacob',
    //   id: 2
    // }, {
    //   name: 'Emily',
    //   id: 3
    // }, {
    //   name: 'Sirio',
    //   id: 4
    // }];

    // this.items = [{
    //   id: 1,
    //   sectionID: 1,
    //   name: 'Audit Planning',
    //   start: moment().add(0, 'days').startOf('day'),
    //   end: moment().add(4, 'days').endOf('day'),
    //   classes: 'schedule-green'
    // }, {
    //   id: 2,
    //   sectionID: 2,
    //   name: 'Test case generation',
    //   start: moment().add(1, 'days').startOf('day'),
    //   end: moment().add(5, 'days').endOf('day'),
    //   classes: 'schedule-orange'
    // }, {
    //   id: 3,
    //   sectionID: 3,
    //   name: 'Audit planning',
    //   start: moment().add(2, 'days').startOf('day'),
    //   end: moment().add(7, 'days').endOf('day'),
    //   classes: 'schedule-blue'
    // },
    // {
    //   id: 4,
    //   sectionID: 4,
    //   name: 'Issue identification',
    //   start: moment().add(1, 'days').startOf('day'),
    //   end: moment().add(6, 'days').endOf('day'),
    //   classes: 'schedule-red'
    // }];
    this.onClose = new Subject();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  public confirm(): void {
    this.onClose.next(true);
    this.bsModalRef.hide();
  }
  public close() {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

}
