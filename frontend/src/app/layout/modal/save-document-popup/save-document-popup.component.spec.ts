import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveDocumentPopupComponent } from './save-document-popup.component';

describe('SaveDocumentPopupComponent', () => {
  let component: SaveDocumentPopupComponent;
  let fixture: ComponentFixture<SaveDocumentPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveDocumentPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveDocumentPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
