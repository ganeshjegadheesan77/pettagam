import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';
declare var $;

@Component({
  selector: 'app-save-document-popup',
  templateUrl: './save-document-popup.component.html',
  styleUrls: ['./save-document-popup.component.less']
})
export class SaveDocumentPopupComponent implements OnInit {
  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  workPaperNumber: any;
  auditId: any;
  fileName: any;
  confirmFlag: Boolean;
  public onClose: Subject<any>;
  constructor(private bsModalRef: BsModalRef,
    private router: Router) { }

  ngOnInit() {
    this.onClose = new Subject();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  public confirm(): void {
    let fileName = $("#fileName").val();
    this.onClose.next(fileName);
    this.bsModalRef.hide();
  }
  public close() {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }
}
