import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmRejectWorkstepComponent } from './confirm-reject-workstep.component';

describe('ConfirmRejectWorkstepComponent', () => {
  let component: ConfirmRejectWorkstepComponent;
  let fixture: ComponentFixture<ConfirmRejectWorkstepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmRejectWorkstepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmRejectWorkstepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
