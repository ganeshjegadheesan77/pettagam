import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';
@Component({
  selector: 'app-confirm-reject-workstep',
  templateUrl: './confirm-reject-workstep.component.html',
  styleUrls: ['./confirm-reject-workstep.component.less']
})
export class ConfirmRejectWorkstepComponent implements OnInit {

  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  confirmFlag: boolean;
  rejectionComments: string = '';
  public onClose: Subject<string>;
  constructor(private bsModalRef: BsModalRef,
    private router: Router) { }

  ngOnInit() {
    this.onClose = new Subject();
    this.rejectionComments = '';
  }

  public close() {
    this.onClose.next(null);
    this.bsModalRef.hide();
  }

  confirm() {
    this.onClose.next(this.rejectionComments);
    this.bsModalRef.hide();
  }

}
