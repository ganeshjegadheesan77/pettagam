import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';

@Component({
  selector: 'app-confirm-popup',
  templateUrl: './confirm-popup.component.html',
  styleUrls: ['./confirm-popup.component.less']
})
export class ConfirmPopupComponent implements OnInit {
  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  confirmFlag:boolean;
public onClose:Subject<boolean>;
  constructor(private bsModalRef: BsModalRef,
    private router: Router) { }
  ngOnInit() {
    this.onClose= new Subject();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  public confirm():void{
this.onClose.next(true);
this.bsModalRef.hide();
  }
  public close() {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }
 
}
