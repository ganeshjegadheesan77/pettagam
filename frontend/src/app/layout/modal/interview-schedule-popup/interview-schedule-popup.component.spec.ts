import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterviewSchedulePopupComponent } from './interview-schedule-popup.component';

describe('InterviewSchedulePopupComponent', () => {
  let component: InterviewSchedulePopupComponent;
  let fixture: ComponentFixture<InterviewSchedulePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterviewSchedulePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterviewSchedulePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
