import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from "@angular/router";
import { HttpService } from 'src/app/core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-interview-schedule-popup',
  templateUrl: './interview-schedule-popup.component.html',
  styleUrls: ['./interview-schedule-popup.component.less']
})
export class InterviewSchedulePopupComponent implements OnInit {
  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  data: any;
  modalRef: any;
  response: any;
  isInsert: boolean;
  public onClose: Subject<Object>;
  constructor(private httpservice: HttpService, private bsModalRef: BsModalRef,
    private router: Router, private modalService: BsModalService) { }

  ngOnInit() {
    this.onClose = new Subject();
    if (this.data) {
      this.response = JSON.parse(JSON.stringify(this.data));
      this.response.day = moment(this.response.day).format('DD/MM/YYYY')
    } else {
      this.isInsert = true;
      this.response = {}
    }


  }

  confirm(data, insertFlag) {
    if (!insertFlag) {
      if (data.day.length <= 10) data.day = new Date(data.day.split('/').reverse().join('/')).toUTCString();
      this.onClose.next(data);
      this.bsModalRef.hide();
      this.httpservice.securePost(ConstUrls.updateOneInterview, data).subscribe((res) => {
        if (res) {
          $('body').removeClass('modalPopupOne');
          const initialState = {
            title: "Success",
            content: "Interview details updated successfully!",
            link: "Ok",
          };
          this.modalRef = this.modalService.show(
            DefaultModalComponent,
            {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            }
          );
        }
      })
    } else {
      if (data.day && data.day.length <= 10) data.day = new Date(data.day.split('/').reverse().join('/')).toUTCString();
      this.bsModalRef.hide();
      this.httpservice.securePost(ConstUrls.saveInterview, data).subscribe((res) => {
      this.onClose.next(res);
        $('body').removeClass('modalPopupOne');
        const initialState = {
          title: "Success",
          content: `Interview details inserted successfully!`,
          link: "Ok",
        };
        this.modalRef = this.modalService.show(
          DefaultModalComponent,
          {
            initialState,
            class: "success-class",
            backdrop: "static",
            keyboard: false,
          }
        );
      })
    }
  }
  close() {
    this.onClose.next(false);
    this.bsModalRef.hide();
    $('body').removeClass('modalPopupOne');
  }


}
