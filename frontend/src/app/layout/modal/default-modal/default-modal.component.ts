import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
@Component({
  selector: 'app-default-modal',
  templateUrl: './default-modal.component.html',
  styleUrls: ['./default-modal.component.less']
})
export class DefaultModalComponent implements OnInit {
  parameter: number;
  content: any;
  action: any;
  link: any;
  title: any;
  constructor(
    private bsModalRef: BsModalRef,
    private router: Router  ) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  confirm() {
    this.close();
  }
  close() {
    if(this.link == 'Copy Link'){
      this.copyText(this.content);
      // alert('copied' + this.content);
    }
    this.bsModalRef.hide();
  }

  copyText(val: string){
    let selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = val;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
    }

}
