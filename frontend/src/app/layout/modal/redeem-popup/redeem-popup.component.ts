import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
// import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';

@Component({
  selector: 'app-redeem-popup',
  templateUrl: './redeem-popup.component.html',
  styleUrls: ['./redeem-popup.component.less']
})
export class RedeemPopupComponent implements OnInit {
  firstscreen = true;
  congratsScreen = false
public onClose:Subject<Boolean>;
  constructor(private bsModalRef: BsModalRef,
    private router: Router) { }
  ngOnInit() {
    this.onClose= new Subject();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }
  public confirm(){
this.onClose.next(true);
this.bsModalRef.hide();
  }
  public close() {
    this.onClose.next(false);
    this.bsModalRef.hide();
    $('.redeem-section').addClass('d-none');
  }
  closeRedmeen() {
    this.onClose.next(false);
    this.bsModalRef.hide();
    $('.redeem-section').addClass('d-none');
  }
  nextPage(){
    this.firstscreen = false;
    this.congratsScreen = true;
  }

}
