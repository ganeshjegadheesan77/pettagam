import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatElementsComponent } from './chat-elements.component';

describe('ChatElementsComponent', () => {
  let component: ChatElementsComponent;
  let fixture: ComponentFixture<ChatElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
