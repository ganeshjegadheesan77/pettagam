import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { Router } from "@angular/router";
import { NotificationService } from "../../notification.service";
import { LocalStorageService } from 'src/app/core/data-services/local-storage.service';
import { ConfigService } from 'src/config.service';
// import {environment} from 'src/environments/environment'

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.less"],
})
export class HeaderComponent implements OnInit, OnChanges {
  classToggled = false;
  notificationBar = false;
  @Output() private classGenerated = new EventEmitter<boolean>();
  noOfNotication: any;
  notifications: any;
  userName: string;
  profilePic = "../../../assets/img/profile.png";

  constructor(
    private httpservice: HttpService,
    private router: Router,
    private notification: NotificationService,
    private localStorageService:LocalStorageService,
    private configService:ConfigService
  ) {
    if (localStorage.getItem("username") === "gillian") {
      this.profilePic = "../../../assets/img/gillian.png";
    } else if (localStorage.getItem("username") === "elmar") {
      this.profilePic = "../../../assets/img/elmar.png";
    } else {
      this.profilePic = "../../../assets/img/profile.png";
    }
  }

  ngOnInit() {
    if (localStorage.getItem("username") === "gillian") {
      this.profilePic = "../../../assets/img/gillian.png";
    } else if (localStorage.getItem("username") === "elmar") {
      this.profilePic = "../../../assets/img/elmar.png";
    } else {
      this.profilePic = "../../../assets/img/profile.png";
    }
    let activeUser = localStorage.getItem("username");
    this.userName = activeUser
      ? activeUser.charAt(0).toUpperCase() + activeUser.slice(1)
      : ``;
    //console.log("Active User: ", activeUser);
    let getUserNotification = {
      database: this.configService.config.db,
      UserName: activeUser,
    };

    this.httpservice
      .secureJavaPostApi(ConstUrls.getNotificationData, getUserNotification)
      .subscribe((res: any) => {
        if (res) {
          this.notifications = res["data"];
          this.noOfNotication = res["data"].length;
          //console.log("Notifications: ", this.notifications);
          //console.log("Count Notification: ", this.noOfNotication);
        } else {
          this.notifications = "";
          this.noOfNotication = "";
        }
      });
  }

  ngOnChanges() {}
  openLinkedIn() {
    if (localStorage.getItem("username") === "gillian") {
      this.profilePic = "../../../assets/img/profile.png";
      window.open(
        "https://www.linkedin.com/in/gillian-hollenstein-bsc-cfa-61006b14/"
      );
    } else if (localStorage.getItem("username") === "elmar") {
      window.open("https://www.linkedin.com/in/elmar-hollenstein-444817/");
    } else {
      window.open("https://www.linkedin.com/login");
    }
  }

  toggleIcon() {
    this.classToggled = !this.classToggled;
    const randomClass = !this.classGenerated;
    this.classGenerated.emit(randomClass);
  }

  logout() {  
    //console.log('logout');  
    this.localStorageService.clearLocalStorageItem();  
    this.router.navigate(['login']);  
  }
  goToTab(data) {
    this.notification.currentmessage = data;
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }
  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark' };
  showNot(){
    this.notificationBar = true;
  }
  closNot(){
    this.notificationBar = false;
  }
}
