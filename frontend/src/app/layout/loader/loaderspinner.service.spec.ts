import { TestBed } from '@angular/core/testing';

import { LoaderspinnerService } from './loaderspinner.service';

describe('LoaderspinnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoaderspinnerService = TestBed.get(LoaderspinnerService);
    expect(service).toBeTruthy();
  });
});
