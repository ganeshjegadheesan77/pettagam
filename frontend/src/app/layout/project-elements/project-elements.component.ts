import { Component, OnInit } from '@angular/core';
declare var $;

@Component({
  selector: 'app-project-elements',
  templateUrl: './project-elements.component.html',
  styleUrls: ['./project-elements.component.less']
})
export class ProjectElementsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    $(function() {
      $('#toggle-one').bootstrapToggle();
    })

  }

}
