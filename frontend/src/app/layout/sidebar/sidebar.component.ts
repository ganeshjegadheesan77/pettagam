import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { Router } from "@angular/router";
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {

  classToggled = true;

  @Output() private numberGenerated = new EventEmitter<boolean>();
  constructor(private router: Router,) { }

  ngOnInit() {
    this.tablefix();
    this.sidebar();
    this.click();
  }

  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark' };
  tablefix() {
    $('body').addClass('table-full-width');
  }
  toggleField() {
    $('body').toggleClass('table-full-width');
    this.classToggled = !this.classToggled; 
    const randomNumber = !this.numberGenerated;
    this.numberGenerated.emit(randomNumber);  
  }

  sidebar(){
   
    $(".navHover").mouseover(function(){
      $(".sidebar-container").addClass("hover-big");
    });
    $(".navHover").mouseout(function(){
      $(".sidebar-container").removeClass("hover-big");
    });

}

click(){
  $(".clickSubmenu").click(function(){
    $(this).toggleClass("collapsed");
    $("#submenu1").toggleClass("show-section");
    $("#submenu2").removeClass("show-section");
    $(".clickSubmenuOne").addClass("collapsed");
  });
  $(".clickSubmenuOne").click(function(){
    $(this).toggleClass("collapsed");
    $("#submenu1").removeClass("show-section");
    $("#submenu2").toggleClass("show-section");
    $(".clickSubmenu").addClass("collapsed");
    
  });
}

liClick(){
  this.router.routeReuseStrategy.shouldReuseRoute = function(){return false;};
}



}
