import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.less']
})
export class DashboardLayoutComponent implements OnInit {
  public randomNumber: boolean;
  public randomClass : boolean;
  public onNumberGenerated(randomNumber: false) {   
    this.randomNumber = !this.randomNumber;
  }
  public onclassGenerated(randomClass: false) {
    this.randomClass = !this.randomClass;
    
  }

  constructor() { }

  ngOnInit() {
    this.randomNumber = true;
  }
  public scrollbarOptions = { axis: 'yx', theme: 'minimal-dark' };
}
