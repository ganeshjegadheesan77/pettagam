import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTestingAssessmentFormComponent } from './control-assessment-form.component';

describe('ControlTestingAssessmentFormComponent', () => {
  let component: ControlTestingAssessmentFormComponent;
  let fixture: ComponentFixture<ControlTestingAssessmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTestingAssessmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTestingAssessmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
