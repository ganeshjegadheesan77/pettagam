import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { HttpService } from "../../../../core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConfirmPopupComponent } from 'src/app/layout/modal/confirm-popup/confirm-popup.component';
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { TabsetComponent } from "ngx-bootstrap/tabs";
// import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ConfigService } from 'src/config.service';
declare var $;

@Component({
  selector: "app-control-assessment-form",
  templateUrl: "./control-assessment-form.component.html",
  styleUrls: ["./control-assessment-form.component.less"],
})
export class ControlTestingAssessmentFormComponent
  implements OnInit, OnChanges {
  @Output() closedControlTestForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input("controlData") controlData: any;
  @ViewChild("controlAssessmentForm", { static: false })
  controlAssessmentForm: TabsetComponent;
  modalRef: BsModalRef;
  uri;
  fetchedData: any;
  uploadedFilesArr;
  position;
  controlStatuses: any;
  controlType: any;
  imagePreview: string;
  controlFrequency: any;
  newControlTest: any;
  controlobjectId: any;
  controlTestingForms: FormGroup;
  riskMasterDepartment: any;
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  controlGuidanceSteps: any;
  riskProcess: any;
  dataloaded: Boolean = false;
  generatedControlTestId: string;
  multiFile: any = [];
  attachedFiles: any = [];
  masterIssue: any = [];
  issueList: any = [{ issueId: Number, issueName: String }];
  issue: any;
  addedIssue: any = [];
  preventingVal: Boolean = true;
  automaticVal: Boolean = true;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private configService:ConfigService,
    private mScrollbarService: MalihuScrollbarService
  ) { 
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.initForm();
    this.controlAsssementCT();
    this.httpservice
      .secureGet(ConstUrls.getAllIssuesRecords)
      .subscribe(async (response) => {
        this.masterIssue = await response;
        this.issue = this.masterIssue;
      });
    if (typeof this.controlData == "object") {
      this.newControlTest = true;
      this.fetchedData = [];
      this.fetchedData.ctlTestingId = "",
        this.fetchedData.ctGuidance = "",
        this.fetchedData.ctAutomatic = false,
        this.fetchedData.ctPreventing = true,
        this.fetchedData.controlRisk = 0,
        this.fetchedData.tests = false,
        this.fetchedData.steptick = [], this.fetchedData.ctFrequency = "";
      this.fetchedData.controlId = [];
      this.fetchedData.attachment = [];
      this.fetchedData.controlId.push(this.controlData);
      var payload = { controlId: this.controlData.controlId };
      this.httpservice
        .securePost(ConstUrls.getOneControl, payload)
        .subscribe(async (response) => {
          this.controlobjectId = response;
        });
    } else {
      this.newControlTest = false;
      this.getOneControlTesting();
    }

    this.httpservice
      .get(ConstUrls.controlGuidance)
      .subscribe((response: any) => {
        this.controlGuidanceSteps = response;
        this.httpservice
          .get(ConstUrls.masterControlType)
          .subscribe((response: any) => {
            this.controlStatuses = response;
          });
        this.httpservice
          .get(ConstUrls.controlType)
          .subscribe((response: any) => {
            this.controlType = response;
          });
        this.httpservice
          .get(ConstUrls.masterFrequency)
          .subscribe((response: any) => {
            this.controlFrequency = response;
          });
        this.httpservice
          .get(ConstUrls.riskDepartment)
          .subscribe((response: any) => {
            this.riskMasterDepartment = response;
          });

        this.httpservice
          .get(ConstUrls.riskProcess)
          .subscribe((response: any) => {
            this.riskProcess = response;
          });
      });
  }
  addIssue() {
    let table = $('#controlAssementCTTable').DataTable();
    let _this = this;

    table.$('input:checked').each(function () {
      let id = $(this).attr('value');
      var newIssue = [];
      let i = _this.issueList.findIndex(i => i.issueId == id);
      if (i > -1) _this.issueList.splice(i, 1);
      newIssue = _this.issue.filter((x) => {
        return x.issueId == id;
      });
      _this.addedIssue.push(newIssue[0]);

    });
    if (this.newControlTest === false) {
      var issueIds = [];
      var actionIds = [];
      this.addedIssue.forEach((element) => {
        if (element.actionId.length >= 1) {
          element.actionId.forEach((x) => {
            actionIds.push(x);
          });
        }
        issueIds.push(element.issueId);
      });
      this.controlAsssementCT();
      // console.log(actionIds);
      var payload = {
        ctlTestingId: this.controlData,
        issueId: issueIds,
        actionId: actionIds,
      };
      this.httpservice
        .securePost(ConstUrls.updateOneControlTesting, payload)
        .subscribe((response: any) => {
          if (response) {
            // console.log("Issuee added");
          }
        });
    }
  }
  ngOnChanges() {
    this.getData();
    this.controlAsssementCT();
  }
  getData() {
    if (typeof this.controlData == "object") {
      this.attachedFiles = [];
      this.preventingVal = false;
      this.automaticVal = false;
      this.newControlTest = true;
      var payload = { controlId: this.controlData.controlId };
      this.httpservice
        .securePost(ConstUrls.getOneControl, payload)
        .subscribe(async (response) => {
          this.controlobjectId = response;
          this.fetchedData = [];
          this.fetchedData.ctlTestingId = "",
            this.fetchedData.ctGuidance = "",
            this.fetchedData.ctAutomatic = false,
            this.fetchedData.ctPreventing = true,
            this.fetchedData.controlRisk = 0,
            this.fetchedData.tests = false,
            this.fetchedData.barColor = "medium";
          this.fetchedData.steptick = [],
            this.fetchedData.ctFrequency = "Daily",
            this.fetchedData.attachment = [];
          this.fetchedData.controlId = [];
          this.fetchedData.controlId.push(this.controlobjectId);

        });
    } else {
      this.newControlTest = false;
      this.addedIssue = [];
      this.attachedFiles = [];
      this.preventingVal;
      this.getOneControlTesting();
    }
  }
  steptick: any = [];
  onCheckStep(e, l) {
    if (e.srcElement.checked) this.fetchedData.steptick.push(l);
    if (!e.srcElement.checked) {
      let i = this.fetchedData.steptick.indexOf(l);
      if (i > -1) this.fetchedData.steptick.splice(i, 1);
    }
  }
  changedValuePreventing() {
    if (this.preventingVal) this.preventingVal = false;
    else this.preventingVal = true;
  }
  changedValueAutomatic() {
    if (this.automaticVal) this.automaticVal = false;
    else this.automaticVal = true;
  }

  initForm() {
    this.controlTestingForms = this.formBuilder.group({
      ctFrequency: ["", []],
      ctAutomatic: [Boolean, []],
      ctPreventing: [Boolean, []],
      testResults: ["", []],
      controlGuidance: ["", []],
      controlRisk: [Number, []],
      tests: [Boolean, []],
      ctlTestingId: ["", []],
      controlId: [, []],
      attachment: [[], []],
      steptick: [[], []],
      preventingVal: [Boolean, []],
      automaticVal: [Boolean, []],
    });
  }
  getCheckValue(stepId) {
    if (this.fetchedData.steptick == null) this.fetchedData.steptick = [];
    if (this.fetchedData.steptick.includes(stepId)) var value = true;
    else var value = false;
    return value;
  }

  onSave() {

    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

    this.attachedFiles.forEach((x) => {
      x.new = false;
    });

    this.controlTestingForms.patchValue({ steptick: this.fetchedData.steptick });
    // console.log(this.attachedFiles);
    if (this.newControlTest) {
      var arr = [];
      // if(this.preventingVal==false) this.controlTestingForms.value.ctPreventing==null;
      // if(this.automaticVal==false) this.controlTestingForms.value.ctAutomatic==null;
      this.generatedControlTestId =
        "CT-" + String(Math.floor(Math.random() * 100) + 1);
      this.controlTestingForms.patchValue({
        ctlTestingId: this.generatedControlTestId,
        controlId: this.controlobjectId._id,
        preventingVal: this.preventingVal,
        automaticVal: this.automaticVal,
      });
      this.upload();
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveControlTest, this.controlTestingForms.value)
            .subscribe((response: any) => {
              if (response) {
                arr.push(this.generatedControlTestId);
                var payload = {
                  controlId: this.controlobjectId.controlId,
                  ctlTestingId: arr,
                  controlRisk: this.controlTestingForms.value.controlRisk,
                };
                this.modalRef.content.onClose.subscribe((r) => {
                  if (r) {
                    this.httpservice
                      .securePost(ConstUrls.updateOneControlforTest, payload)
                      .subscribe((response: any) => {
                        if (response) {
                          const initialState = {
                            title: "Success",
                            content: "Control Test Created Successfully",
                            link: "Ok",
                          };
                          this.modalRef = this.modalService.show(
                            DefaultModalComponent,
                            {
                              initialState,
                              class: "success-class",
                              backdrop: "static",
                              keyboard: false,
                            }
                          );
                          this.closeModal();
                          this.closedControlTestForm.emit(true);
                        }
                      });
                  }
                })

              }
            });
        }
      })

    } else {
      this.upload();
      // if(this.preventingVal==false) this.controlTestingForms.value.ctPreventing==null;
      // if(this.automaticVal==false) this.controlTestingForms.value.ctAutomatic==null;
      this.controlTestingForms.value.automaticVal = this.automaticVal;
      this.controlTestingForms.value.preventingVal = this.preventingVal;
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(
              ConstUrls.updateOneControlTesting,
              this.controlTestingForms.value
            )
            .subscribe((response: any) => {
              if (response) {
                var payload = {
                  controlId: this.fetchedData.controlId[0].controlId,
                  controlRisk: this.controlTestingForms.value.controlRisk,
                };
                this.httpservice
                  .securePost(ConstUrls.updateOneControlforTest, payload)
                  .subscribe((response: any) => {
                    if (response) {
                      const initialState = {
                        title: "Success",
                        content:
                          "Control Testing : " +
                          this.controlData +
                          " updated Successfully",
                        link: "Ok",
                      };
                      this.modalRef = this.modalService.show(
                        DefaultModalComponent,
                        {
                          initialState,
                          class: "success-class",
                          backdrop: "static",
                          keyboard: false,
                        }
                      );
                      this.closeModal();
                      this.closedControlTestForm.emit(true);
                    }
                  });
              }
            });
        }
      })

    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        let extension = ext.relativePath.split(".")[1];
        ext.extension = extension;
      }
    }
    // console.log("upload files array :  ", this.uploadedFilesArr);
    // console.log("files array: ", this.files);
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          this.attachedFiles.push({
            relativePath: file.name.split(".")[0],
            extension: file.name.split(".")[1],
            fullName: file.name,
            new: true,
          });
          // console.log(droppedFile.relativePath, file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        // console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.attachment) {
        this.fetchedData.attachment.push(data[i]["name"]);
      } else {
        this.fetchedData.attachment = [];
        this.fetchedData.attachment.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.attachment;
    this.controlTestingForms.patchValue({ attachment: arr });

    console.log('filearray', arr.toString());
    let filearr = [];
    arr.forEach(ele => {
      filearr.push('controlTest/' + ele)
    });
    let payload = {
      "collection": "document",
      "module": "controlTest",
      "sub-module": "control-testing-matrix",
      "tab": "control-testing-matrix",
      "router-link": "/rcm/control-testing",

      "file": filearr.toString(),
      "metadata": "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      "username": localStorage.getItem('username'),
      "database": this.configService.config.db
    }

    console.log('check', payload)
    this.httpservice.secureJavaPostReportApi('DocumentManagementService/put', payload).subscribe((files: any) => {
      if (files) {
        console.log('Response', files);
      }
      //console.log('Uploaded');
      this.multiFile = [];
    })

    this.httpservice
      .securePost("multipleuploadfileCT", formData)
      .subscribe((files) => {
        this.multiFile = [];
      });
  }
  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/controlTest/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }

    let payload = {
      "database": this.configService.config.db,
      "file": "controlTest/" + file.fullName
    }

    this.httpservice.secureJavaPostReportApi('DocumentManagementService/get', payload).subscribe((files: any) => {
      //  console.log('Uploaded');
      if (files) {
        console.log('CT Response', files);
      }
      this.multiFile = [];
    })
  }
  public fileOver(event) {
    // console.log(event);
  }

  public fileLeave(event) {
    // console.log(event);
  }

  getOneControlTesting() {
    let payload = { ctlTestingId: this.controlData };
    this.httpservice
      .securePost(ConstUrls.getOneControlTesting, payload)
      .subscribe(async (response) => {
        this.fetchedData = await response;
        //this.fetchedData.barColor =this.fetchedData.controlId[0].controlRisk >= 0 && this.fetchedData.controlId[0].controlRisk <= 33? "medium": this.fetchedData.controlId[0] >= 34 && this.fetchedData.controlId[0].controlRisk <= 66 ? "very-low": "very-high";
        this.controlTestingForms.patchValue(response);
        var id = this.fetchedData.issueId;
        this.issueList = [];
        this.issue.forEach((element) => {
          if (element.issueId && !id.includes(element.issueId))
            this.issueList.push(element);
        });
        this.addedIssue = this.masterIssue.filter((x) => {
          return id.includes(x.issueId);
        });
        // console.log(this.fetchedData);
        if (this.fetchedData.preventingVal)
          this.fetchedData.preventingVal = this.preventingVal;
        else this.fetchedData.preventingVal = true;
        if (this.fetchedData.automaticVal)
          this.fetchedData.automaticVal = this.automaticVal;
        else this.fetchedData.automaticVal = true;
        if (this.fetchedData.attachment) {
          for (let i = 0; i < this.fetchedData.attachment.length; i++) {
            this.attachedFiles.push({
              relativePath: this.fetchedData.attachment[i].split(".")[0],
              extension: this.fetchedData.attachment[i].split(".")[1],
              fullName: this.fetchedData.attachment[i],
              new: false,
            });
          }
        }
      });
  }
  closeModal() {
    $(".scroll-container").scrollTop(0);
    // this.dataloaded=false;
    $(".pop-up-form").addClass("d-none");
    this.controlAssessmentForm.tabs[0].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 }
  }
  indicatorUpdate(data) {
    let element = document.getElementById('crRange');
    element.className =
      data >= 0 && data <= 33
        ? "medium"
        : data >= 34 && data <= 66
          ? "very-low"
          : "very-high";
  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
      // console.log("left curr status:", this.marked);
    } else {
      this.openAttachment = true;
      // console.log("selected curr status:", this.marked);
    }
  }
  removeAttachment(removeitem, index) {
    if (removeitem.new) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
    } else {
      const fileindex = parseInt(index);
      let payload = {
        directory: "uploads/controlTest",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            /*console.log(
              "attached files array",
              this.attachedFiles,
              "index from method",
              fileindex
            );*/
            this.attachedFiles.splice(fileindex, 1);

            this.fetchedData.attachment = [];
            this.attachedFiles.forEach((x) => {
              this.fetchedData.attachment.push(x.fullName);
            });
            // console.log(this.fetchedData.attachment);
            let payload = {
              ctlTestingId: this.controlData,
              fileName: removeitem.fullName,
            };
            this.httpservice
              .securePost(ConstUrls.deleteCTFile, payload)
              .subscribe((response: any) => {
                if (response) {
                  console.log("yessss");
                }
              });
            setTimeout(function () {
              // console.log(this.attachedFiles);
            }, 3000);
            if (response) {
              const initialState = {
                title: "Success",
                content: "Attachment Deleted Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              // this.closedAddDefinationActionForm.emit(true);
              // this.closeModal();
            }
          }
        });
    }

    //  /api/removeAttachment
  }

  deleteIssue(issueId: Number, actionIds: [String]) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this Issue?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        // console.log(r);

        var payload = {
          ctlTestingId: this.controlData,
          issueId: issueId,
          actionIds: actionIds,
        };
        this.httpservice
          .securePost(ConstUrls.deleteIssueFromControlTesting, payload)
          .subscribe((response: any) => {
            if (response) {
              console.log("response-->", response);
              this.addedIssue = this.addedIssue.filter(a => response.issueId.includes(a.issueId));
              this.issueList = this.issue.filter((a) => !(response.issueId.includes(a.issueId)));
              this.controlAsssementCT();
            }
          });
      }
    });
  }

  controlAsssementCT() {
    let table = $('#controlAssementCTTable').DataTable();
    table.destroy();
    setTimeout(() => {
      $('#controlAssementCTTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "destroy": true,
        "language": {
          "searchPlaceholder": "Search"
        },
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }
}
