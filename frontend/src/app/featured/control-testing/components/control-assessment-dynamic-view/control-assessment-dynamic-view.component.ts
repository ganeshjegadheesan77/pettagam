import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { count } from 'rxjs/operators';
@Component({
  selector: 'app-control-assessment-dynamic-view',
  templateUrl: './control-assessment-dynamic-view.component.html',
  styleUrls: ['./control-assessment-dynamic-view.component.less']
})
export class ControlAssessmentDynamicViewComponent implements OnInit {
controlCount: any;
IssueCount:any;
data: any;
count:any;
performed: any;
constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.secureGet(ConstUrls.getControlCount).subscribe(response => {
      // console.log("getCount:",response)
      this.controlCount= Number(response);
    })

    this.httpService.secureGet(ConstUrls.getNumberofIssues).subscribe(response => {
      // console.log("getIssueCount:",response)
      this.IssueCount= Number(response);

    }) 
    this.httpService.secureGet(ConstUrls.getAllControlRecords).subscribe(response => {
      // console.log("getIssueCount:",response)
      this.data= response;
      this.count=0;
      this.performed=0
      this.data.forEach(x=>{
        if(x.ctlTestingId && x.ctlTestingId.length>0)
        {
        this.performed = x.ctlTestingId.length +this.performed;
        }
        else
        this.count++
        })

    }) 

   
  }

}
