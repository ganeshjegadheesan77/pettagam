import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlAssessmentDynamicViewComponent } from './control-assessment-dynamic-view.component';

describe('ControlAssessmentDynamicViewComponent', () => {
  let component: ControlAssessmentDynamicViewComponent;
  let fixture: ComponentFixture<ControlAssessmentDynamicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlAssessmentDynamicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlAssessmentDynamicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
