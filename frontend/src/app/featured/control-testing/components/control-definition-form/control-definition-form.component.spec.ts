import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlDefinitionFormComponent } from './control-definition-form.component';

describe('ControlDefinitionFormComponent', () => {
  let component: ControlDefinitionFormComponent;
  let fixture: ComponentFixture<ControlDefinitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlDefinitionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlDefinitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
