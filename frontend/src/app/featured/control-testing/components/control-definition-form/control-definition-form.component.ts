import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output, ViewChild } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import {ConstUrls} from '../../../../../app/config/const-urls';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup } from "@angular/forms";
import { DefaultModalComponent } from 'src/app/layout/modal/default-modal/default-modal.component';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
declare var $;
@Component({
  selector: 'app-control-definition-form',
  templateUrl: './control-definition-form.component.html',
  styleUrls: ['./control-definition-form.component.less']
})
export class ControlDefinitionFormComponent implements OnInit ,OnChanges {
  @Input('controlData') controlData:string;
  @Output()closedControlDesForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('controlDefinationForm', { static: false }) controlDefinationForm: TabsetComponent;
  modalRef: BsModalRef;
  position;
  fetchedData:any;
  controlStatuses:any;
  controlType:any;
  controlForm: FormGroup;
  riskMasterDepartment: any;
  riskProcess: any;
  riskList: any;
  constructor(private httpservice: HttpService, private formBuilder: FormBuilder,private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.controlDefinitionCTTable();
  }

  ngOnChanges(changes:SimpleChanges){
    // console.log(changes.controlData.currentValue)
    if(changes['controlData']['currentValue']){

      this.controlData = changes['controlData']['currentValue'];
      // console.log(this.controlData);
      if(this.controlData) {this.getOneControl() }
       this.initForm();

      this.httpservice.get(ConstUrls.masterControlStatus).subscribe((response:any) => {
      this.controlStatuses = response;
        // console.log('control Status: ' , this.controlStatuses);
      });

      this.httpservice.get(ConstUrls.masterControlType).subscribe((response:any) => {
      this.controlType = response;

      });

      this.httpservice.get(ConstUrls.riskDepartment).subscribe((response:any) => {
        this.riskMasterDepartment = response;
      });

      this.httpservice.get(ConstUrls.riskProcess).subscribe((response:any) => {
        this.riskProcess = response;
        // console.log('process: ' , this.riskProcess)
      });

      let payload = { controlId: this.controlData['id'] };
      this.httpservice.securePost(ConstUrls.getRisksAssigned , payload ).subscribe((response:any)=>{
        if(response) { this.riskList = response; }
        // console.log('risk List: ' , response);
      })
  }
}

initForm() {
  this.controlForm = this.formBuilder.group({
    assignToTask: ["", []],
    controlAddedDate: ["", []],
    controlApprovedBy: ["", []],
    controlApprovedDate: ["", []],
    controlApprovedId: ["", []],
    controlDepartment: ["", []],
    controlDepartmentProcess: ["", []],
    controlDescription: ["", []],
    controlGuidance: ["", []],
    controlId: ["", []],
    controlKeyFlag: ["", []],
    controlKeyProcess: ["", []],
    controlLastChangeDate: ["", []],
    controlLegalReference: ["", []],
    controlName:["", []],
    controlObjective: ["", []],
    controlOwnerId: ["", []],
    controlOwnerName:["", []],
    controlStatus: ["", []],
    controlTesting: ["", []],
    controlType: ["", []],
    riskId: ["", []],
  });
}

onSave(){
  // updateOneControl
  // console.log(this.controlForm.value);
  this.httpservice.securePost(ConstUrls.updateOneControl, this.controlForm.value).subscribe((response:any)=>{
    if(response){
         
      const initialState = {
        title: 'Success',
        content: 'Control : ' + this.controlData +' updated Successfully',
        link: 'Ok',
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
      this.closeModal();
    this.closedControlDesForm.emit(true);
    }
  })
}


getOneControl() {

  let payload = { controlId: this.controlData };
  this.httpservice.securePost(ConstUrls.getOneControl,payload).subscribe(async response => {
   this.fetchedData = await response;
  //  console.log(this.fetchedData);
   this.initForm();
    this.controlForm.patchValue(this.fetchedData);
  });
}
  closeModal() {
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').addClass('d-none');
   this.controlDefinationForm.tabs[0].active = true;
   this.position = this.position ? undefined : {x: 0, y: 0}
  }

  controlDefinitionCTTable() {
    let table = $('#controlDefinitionCT').DataTable();
    table.destroy();
    setTimeout(() => {
      $('#controlDefinitionCT').DataTable({
        "oLanguage": { "sSearch": "" },
        "destroy": true,
        "language": {
          "searchPlaceholder": "Search"
        },
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }
}
