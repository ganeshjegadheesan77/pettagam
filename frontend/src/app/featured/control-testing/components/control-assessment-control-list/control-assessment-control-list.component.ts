import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { HttpService } from '../../../../core/http-services/http.service';
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { ConstUrls } from 'src/app/config/const-urls';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConfirmPopupComponent } from 'src/app/layout/modal/confirm-popup/confirm-popup.component';
import {DefaultModalComponent} from 'src/app/layout/modal/default-modal/default-modal.component';
declare var $;

@Component({
  selector: 'app-control-assessment-control-list',
  templateUrl: './control-assessment-control-list.component.html',
  styleUrls: ['./control-assessment-control-list.component.less']
})
export class ControlAssessmentControlListComponent implements OnInit {
  public controlAssessmentForm = false;
  public controlTestingAssessmentForm= false;
  private loadComponent = false;  
  controlId:any;
  oneControl:any;
  constructor(private httpService: HttpService, private chRef: ChangeDetectorRef,private modalService: BsModalService, private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) {}
  ControlData:any=[];
  modalRef: BsModalRef;
  modalRefDefault:BsModalRef;
  actionData:any;
  data: any;
  private loadctId = false;
  selectedControlTestingId:any;
  selectedControlId:any;
  controlTestingData:any;
  hide = true;
  dataTable: any;
  controlTestTableHeader:any;
  showHideHeaders:any;
  actionList:any;
  actionSet=[]
  getControlTestFormStatus(event){
    if(event === true){
    let table = $('#control-testing-matrix-table').DataTable();
    table.destroy();
      this.getDataList();
    }
  }
  getDataList(){
    this.ControlData=[]
    this.httpService.securePost(ConstUrls.getAllControlTestRecords).subscribe(response=>{
      var controlTestingData:any= response;
      let actionIds=[]
      controlTestingData.forEach(y => {
        if(y.actionId && y.actionId.length>0){
          actionIds.push(y.actionId)
        }
      });
      var merged = [].concat.apply([], actionIds);
      // console.log(merged);
      this.httpService.secureGet(ConstUrls.getAllActionRecords).subscribe(response => {
        this.actionData = response;
        this.actionList= this.actionData.filter(x=>{return merged.includes(x.actionId)})

      })
      // var getctRisk= function(controlTestingData,id){
      //   var result=[]
      //   result= controlTestingData.filter(x=>{ return x.ctlTestingId===id})
      //   var ctRisk= result[0].controlRisk
      //   return ctRisk
      //     }
      //     var getctColor= function(controlTestingData,id){
      //       var result=[]
      //       result= controlTestingData.filter(x=>{ return x.ctlTestingId===id})
      //       var ctColor= result[0].controlRisk>=0 && result[0].controlRisk<=33  ? "medium" : result[0].controlRisk>=34 && result[0].controlRisk<=66 ?"very-low" :"very-high"
      //       return ctColor
      //         }
          this.httpService.secureGet(ConstUrls.getAllControlRecords).subscribe(response => {
            this.data= response
           this.data.forEach(x=>{
              if(x.riskId && x.riskId.length>0){
                x.assignedToRisk=true;
              }
              else
              x.assignedToRisk=false;
              if(x.ctlTestingId && x.ctlTestingId.length >0){
                x.ctlTestingId.forEach(y=> {
                  this.ControlData.push({controlDepartmentProcess:x.controlDepartmentProcess,
                    controlId: x.controlId,
                    controlName: x.controlName,
                    controlDescription :x.controlDescription,
                    ctId: y,
                    controlOwnerName: x.controlOwnerName,
                    controlDepartment: x.controlDepartment,
                    controlStatus: x.controlStatus,
                    ctRisk: x.controlRisk,
                    barColor:x.controlRisk>=0 && x.controlRisk<=33  ? "medium" : x.controlRisk>=34 && x.controlRisk<=66 ?"very-low" :"very-high",
                    assignedToRisk: x.assignedToRisk 
      
                  })
                })
              }
              else{
                this.ControlData.push({controlDepartmentProcess:x.controlDepartmentProcess,
                  controlId: x.controlId,
                  controlName: x.controlName,
                  controlDescription :x.controlDescription,
                  controlOwnerName: x.controlOwnerName,
                  controlDepartment: x.controlDepartment,
                  controlStatus: x.controlStatus,
                  ctRisk:1,
                  barColor: "very-low",
                  assignedToRisk: x.assignedToRisk 
      
                })
              }
      
            })
          
          this.datatable();
        })
    })
}
  ngOnInit() {
    this.controlTestTableHeader=[
      "Area",
      "Ctl ID",
      "Control Name",
      "Control Description",
      "Ctl Testing",
      "Ctl Risk",
      "Ctl Owner",
      "Owner BU","Ctl Status","Assigned to Risk","Action"
    ];
    this.showHideHeaders = this.controlTestTableHeader;
  $('[data-toggle="tooltip"]').tooltip();
    this.getDataList();
  }

  loadMyChildComponent(id, isNewAssessment = false) {
    this.selectedControlTestingId=id
    this.controlTestingAssessmentForm = true;
    this.controlAssessmentForm = false;
    $(".scroll-container").scrollTop(0);
   $('.pop-up-form').removeClass('d-none');
   $(".tabOne").trigger('click');

  }
  loadMyControlTestingId(id){
    this.httpService.securePost(ConstUrls.getOneControlTesting, {ctlTestingId:id}).subscribe(response=>{
      // console.log(response);
      var res:any=response;
      let actionId=res.actionId;
      this.actionList= this.actionData.filter(x=>{return actionId.includes(x.actionId)})
  
    })
    this.selectedControlTestingId=id
      this.controlTestingAssessmentForm = true;
        
    this.controlAssessmentForm = false;
    $(".scroll-container").scrollTop(0);
   $('.pop-up-form').removeClass('d-none');
   $(".tabOne").trigger('click');

   }
   deleteCT= (ctId, control)=>{
    var data=this.data.filter(x=>{return x.controlId==control})
    var array=data[0].ctlTestingId;
    var index= array.indexOf(ctId);
    if(index>-1){
      array.splice(index,1)
    }
    var payload={controlId:control, ctlTestingId:array}
    this.httpService.securePost(ConstUrls.updateOneControl, payload).subscribe(result=>{
      if(result){
        const initialState = {
          title: 'Success',
          content: `Control Test ${ctId} Deleted successfully!`,
          link: 'Ok',
        };
        this.modalRefDefault = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
        let table = $('#control-testing-matrix-table').DataTable();
        table.destroy();
        this.ngOnInit();
      }
    })
  }
   deleteControlTest(ctId,controlId){
    
    const initialState = {
      title: 'Confirmation',
      content: `Do you want to delete ${ctId} from ${controlId}?`,
      link: 'Confirm',
      action:'Cancel',
     confirmFlag:false
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
    this.modalRef.content.onClose.subscribe(r=>{
      if(r){
        this.deleteCT(ctId,controlId)
      }
    })
  }

  
   loadMyControlId(id){
    this.selectedControlId=id
    this.controlTestingAssessmentForm = false;
    this.controlAssessmentForm = true;
    $(".scroll-container").scrollTop(0);
   $('.pop-up-form').removeClass('d-none');
   $(".tabOne").trigger('click');

   }

  datatable() {
    this.chRef.detectChanges();
    const ctMatrixTable: any = $("#control-testing-matrix-table");
    const matrixCT = this.dataTableService.initializeTable(
      ctMatrixTable,
      `CTcontrollist`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      matrixCT,
      `control-testing-matrix-select-all`
    );
    this.dataTableFuncService.columnDropDown(matrixCT, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(matrixCT);
    $("#control-testing-matrix-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#control-testing-matrix-select-all").prop("checked", false);
          var el = $("#control-testing-matrix-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#control-testing-matrix-table tr td input[type='checkbox']:checked").length ==
          $("#control-testing-matrix-table tr td input[type='checkbox']").length
        ) {
          $("#control-testing-matrix-select-all").prop("checked", true);
        }
      }
    );

    this.dataTable.columns.adjust().draw();
   
  } 
  Setting(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    e.stopPropagation();
  }
}
