import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlAssessmentControlListComponent } from './control-assessment-control-list.component';

describe('ControlAssessmentControlListComponent', () => {
  let component: ControlAssessmentControlListComponent;
  let fixture: ComponentFixture<ControlAssessmentControlListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlAssessmentControlListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlAssessmentControlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
