import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTestingComponent } from './control-testing.component';

describe('ControlTestingComponent', () => {
  let component: ControlTestingComponent;
  let fixture: ComponentFixture<ControlTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
