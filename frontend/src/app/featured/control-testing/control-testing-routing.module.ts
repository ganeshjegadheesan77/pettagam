import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ControlTestingComponent } from './control-testing.component';

const routes: Routes = [{ path: '', component: ControlTestingComponent, data: { title: "EnRiCo: Control Testing" } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ControlTestingRoutingModule { }
