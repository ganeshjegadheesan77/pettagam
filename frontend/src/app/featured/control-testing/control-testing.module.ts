import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms'  
import { ReactiveFormsModule} from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ControlTestingRoutingModule } from './control-testing-routing.module';
import {RiskAndControlManagementModule} from '../risk-and-control-management/risk-and-control-management.module'
import { ControlTestingComponent } from './control-testing.component';
import { ControlAssessmentControlListComponent } from './components/control-assessment-control-list/control-assessment-control-list.component';
import { ControlAssessmentDynamicViewComponent } from './components/control-assessment-dynamic-view/control-assessment-dynamic-view.component';
import { ControlTestingAssessmentFormComponent } from './components/control-assessment-form/control-assessment-form.component';
import {ControlDefinitionFormComponent} from './components/control-definition-form/control-definition-form.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { AngularDraggableModule } from "angular2-draggable";
@NgModule({
  declarations: [ControlTestingComponent, ControlAssessmentControlListComponent, ControlAssessmentDynamicViewComponent, ControlTestingAssessmentFormComponent,ControlDefinitionFormComponent],
  imports: [
    CommonModule,
    ControlTestingRoutingModule,
    TabsModule.forRoot(),FormsModule, ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    RiskAndControlManagementModule,
    NgxFileDropModule,
    AngularDraggableModule

  ],

})
export class ControlTestingModule { }






