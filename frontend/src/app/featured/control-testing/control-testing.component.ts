import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-control-testing',
  templateUrl: './control-testing.component.html',
  styleUrls: ['./control-testing.component.less']
})
export class ControlTestingComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('enrico-logo');
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('enrico-logo');
   }
}
