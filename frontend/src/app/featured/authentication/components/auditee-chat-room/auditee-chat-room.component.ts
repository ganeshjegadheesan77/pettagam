import { Component, OnInit } from "@angular/core";
import { ConstUrls } from 'src/app/config/const-urls';
import { HttpService } from 'src/app/core/http-services/http.service';
// import { ActivatedRoute, Router } from "@angular/router";
// import { HttpService } from "src/app/core/http-services/http.service";
// import { ConstUrls } from "src/app/config/const-urls";
// import { BsModalService, BsModalRef } from "ngx-bootstrap";
// import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";

@Component({
  selector: "app-auditee-chat-room",
  templateUrl: "./auditee-chat-room.component.html",
  styleUrls: ["./auditee-chat-room.component.less"],
})
export class AuditeeChatRoomComponent implements OnInit {
  // urlToken;
  // modalRef: BsModalRef;
  chatPopup = true;
  emailId: string;
  auditId: string;
  username;

  constructor(private httpservice: HttpService) // private modalService: BsModalService , // private router: Router, // private activatedRoute: ActivatedRoute,
  { }

  ngOnInit() {
    this.username = localStorage.getItem('username');
    this.generateToken();
    setInterval(() => {
      this.generateToken()
    }, 1500000);
    // this.activatedRoute.queryParams.subscribe((params) => {
    //   if (params["urlToken"]) {
    //     this.urlToken = params["urlToken"];
    //     // console.log(params);
    //     this.httpservice
    //       .post(ConstUrls.checkAuditeeToken, { Token: this.urlToken })
    //       .subscribe(
    //         (res) => {
    //           // use this for chat
    //           console.log(res);
    //           this.emailId = res["emailId"];
    //           this.auditId = res["auditId"];
    //           localStorage.setItem("username", res["emailId"]);

    const body = document.getElementsByTagName("body")[0];
    body.classList.add("admin-chat");
    $("#nav-profile-tab").addClass("active");
    $("#nav-profile").addClass("show active");
    $(".new-group-control").remove();
    //         },
    //         (err) => {
    //           this.openAlert("Failure", err.error.message);
    //           // localStorage.clear();
    //           // this.router.navigate(['/login']);
    //         }
    //       );
    //   } else {
    //     localStorage.clear();
    //     this.router.navigate(["/login"]);
    //   }
    // });
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("admin-chat");
    $("#nav-profile-tab").removeClass("active");
    $("#nav-profile").removeClass("show active");
  }

  // checkUrlToken() {}

  // openAlert(title, content) {
  //   const initialState = { title: title, content: content, link: "Ok" };
  //   this.modalRef = this.modalService.show(DefaultModalComponent, {
  //     initialState,
  //     class: "success-class",
  //     backdrop: "static",
  //     keyboard: false,
  //   });
  // }

  generateToken() {
    let payload = { username: this.username };
    this.httpservice.post(ConstUrls.generateDmsSessionForAuditee, payload).subscribe((res: any) => {
      if (res) {
        localStorage.setItem('auditeeDmsToken', res.dmsSession);
        console.log('GENERATE TOKEN FOR DMS');
      }
    })
  }
}
