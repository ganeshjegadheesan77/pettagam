import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditeeChatRoomComponent } from './auditee-chat-room.component';

describe('AuditeeChatRoomComponent', () => {
  let component: AuditeeChatRoomComponent;
  let fixture: ComponentFixture<AuditeeChatRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditeeChatRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditeeChatRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
