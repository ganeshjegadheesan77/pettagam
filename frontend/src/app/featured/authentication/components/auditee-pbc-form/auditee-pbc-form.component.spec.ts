import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditeePbcFormComponent } from './auditee-pbc-form.component';

describe('AuditeePbcFormComponent', () => {
  let component: AuditeePbcFormComponent;
  let fixture: ComponentFixture<AuditeePbcFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditeePbcFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditeePbcFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
