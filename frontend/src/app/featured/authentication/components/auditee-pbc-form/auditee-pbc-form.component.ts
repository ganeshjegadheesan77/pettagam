import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as moment from "moment";
import { ActivatedRoute, Router } from "@angular/router";

import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { ConfigService } from 'src/config.service';

// global table service added by Abu - 17-09-2020 start 
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { MemoService } from 'src/app/featured/internal-audit/components/field-work/memo.service';
// global table service added by Abu - 17-09-2020 end 

@Component({
  selector: 'app-auditee-pbc-form',
  templateUrl: './auditee-pbc-form.component.html',
  styleUrls: ['./auditee-pbc-form.component.less']
})
export class AuditeePbcFormComponent implements OnInit {
  @Output() closePbcNewForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @Input("selectAudit") selectAudit: any;

  uri;
  position;
  PBCForm: FormGroup;
  modalRef: BsModalRef;

  selectedAuditId: any;
  newId: number;
  PBCId: string;
  isAttachment: boolean;
  marked: boolean;
  openAttachment: boolean;
  uploadedFilesArr;
  multiFile: any = [];
  // attachedFiles: any;
  attachedFiles: any = [];
  myfileName;
  fetchedData: any = [];
  lastPbc: number;
  auditPBC_id: any;
  pbc_id: number;
  title: string;
  pbcFileList = [];
  content: string;
  pbcdate: any;
  documentList: any =[];
  fieldPanes: any;
  pbcResponse: any;
  router: any;
  pbcget: any;
  documentRequestArray: any;
  mixArr: any;
  preparerArr: any[];
  dropdownFirmList = [];
  reviewerArr: any[];
  emailId: string;
  urlToken;
  auditId: string;
  payload: any;
  username;
  // assign search  added by Abu - 17-09-2020 start 
  editsearchDocument: boolean = false;
  addnewPBC: boolean = false;
  pbcData: any;
  token: any;


  //  assign search  added by Abu - 17-09-2020 end 


  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private configService: ConfigService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    private epService: MemoService,
    private activatedRoute: ActivatedRoute,



  ) {
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {



    this.username = localStorage.getItem('username');
    this.generateToken();
    setInterval(() => {
      this.generateToken()
    }, 1500000);

    const body = document.getElementsByTagName("body")[0];
    body.classList.add("admin-chat");
    $("#nav-profile-tab").addClass("active");
    $("#nav-profile").addClass("show active");
    $(".new-group-control").remove();


    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });



    this.urlTokenMethod();

  }

  AuditeePbcData() {


    console.log("$$", this.auditId)
    this.fetchedData = [];
    console.log("$$Payload => ", JSON.stringify(this.payload));

    this.httpservice.post(ConstUrls.getPbcData, this.payload).subscribe(
      (response: any) => {
        console.log('getdata@@@@@@@@@', response)
        this.pbcData = response.data;
        if (response.data.teamAndTimeline.periodInScope && response.data.teamAndTimeline.periodInScope.length > 0) {
          this.pbcData.teamAndTimeline.periodInScope = [new Date(response.data.teamAndTimeline.periodInScope[0]), new Date(response.data.teamAndTimeline.periodInScope[1])];
        }
        else {
          this.pbcData.teamAndTimeline.periodInScope = null;
          this.pbcData.teamAndTimeline.pbc = null;

        }
        console.log('*****data' , this.pbcData.auditPbc.documentList)
        if (this.pbcData.auditPbc.documentList && this.pbcData.auditPbc.documentList.length > 0) {
          this.pbcData.auditPbc.documentList.forEach(d => {
            if(d.dmsInfo.documentName)
            {
              let splitFileNameArr = d.dmsInfo.documentName.split(".")[1];

              console.log('****splitFileNameArr' , splitFileNameArr)
            
                 d.dmsInfo.extenstion = splitFileNameArr;
            }else{
              d.dmsInfo.extenstion = '';

            }
          // console.log('*****DMS INFO' , d.dmsInfo);
          // let splitFileNameArr = d.dmsInfo.documentName.split(".")[1];
          // console.log('****splitFileNameArr' , splitFileNameArr)
        
          //    d.dmsInfo.extenstion = splitFileNameArr;
            d.dueDate = new Date(d.dueDate);
            d.dateUploadedDisplay = this.convertDateToFormat("dd.mm.yyyy", d.dateUploaded);
          })
        }
        this.datatable();
      },
      (err: any) => {

      });
  }

  onApproveToggleChange(event, i) {
    if (event.target.checked) {
      this.pbcData.auditPbc.documentList[i].isApproved = true;
      this.pbcData.auditPbc.documentList[i].isRejected = false;
      
    }
    else {
      this.pbcData.auditPbc.documentList[i].isApproved = false;
      this.pbcData.auditPbc.documentList[i].isRejected = true;
    }
  }


  convertDateToFormat(format, date): String {
    if (date) {
      var myDate = new Date(date);
      var dd = String(myDate.getDate()).padStart(2, '0');
      var mm = String(myDate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = myDate.getFullYear();
      switch (format) {
        case "dd.mm.yyyy":
          return dd + '.' + mm + '.' + yyyy;
        case "dd/mm/yyyy":
          return dd + '/' + mm + '/' + yyyy;
        case "mm.dd.yyyy":
          return mm + '.' + dd + '.' + yyyy;
        case "mm/dd/yyyy":
          return mm + '/' + dd + '/' + yyyy;
      }
    }
    else {
      return "--/--/----"
    }
  }


  deleteFileAttachment(i) {
    var file = this.pbcData.auditPbc.documentList[i].dmsInfo;
    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete current File?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: file.documentNo,
          fileName: file.documentName,
          // _id: file.item_id,
          deletedBy: localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
        };
        console.log("*****payload:", payload);
        this.httpservice
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res) {
              // let payload ={
              //   username: this.pbcData.auditPbc.documentList[i].uploadedBy,
              //   auditId: this.auditId,
              //   rowIndex: i,
              //   documentUrlToken: this.documentList
              // }
            
              this.payload = {
                

                  "auditId" : this.auditId,
                  "rowIndex" : i
              };
              console.log("Payload for deletePbcAttachment : ", this.payload)
              this.httpservice
                .post(ConstUrls.deletePbcAttachment, this.payload)
                .subscribe((resp: any) => {
                  if (resp) {
                    console.log("eee",resp)
                    this.pbcData.auditPbc.documentList[i].dmsInfo={};
                    let table = $("#pbcDocumentTable").DataTable();
                    table.destroy();
                    this.datatable();

                    this.title = "Success";
                    this.content = "File Deleted successfully...";
                    this.myCommonAlert(this.title, this.content);
                  }
                });

              // this.myCommonAlert("Success", "Document deleted successfully.");
            } else {
              this.myCommonAlert("Eroor", res.message);
            }
          });

      }
    });
  }



  ngOnChanges() {
    this.initForm();
    this.fetchedData = [];
  }



  urlTokenMethod() {
    this.activatedRoute.queryParams.subscribe((params) => {
      console.log("pbc called" + params["urlToken"]);
      if (params["urlToken"]) {
        this.urlToken = params["urlToken"];
        console.log("$$$$$", this.urlToken)
        this.httpservice
          .post(ConstUrls.checkPBCToken, { Token: this.urlToken })
          .subscribe(
            (res) => {
              console.log("Response : " + JSON.stringify(res));
              this.auditId = res["data"]["auditId"];
              this.payload = { auditId: this.auditId };

              console.log('auditId', this.auditId)
              const onlineData = {

                username: localStorage.getItem("username"),
                status: true,
              };
              this.AuditeePbcData();
            }
          );
      } else {
        // localStorage.clear();
        localStorage.removeItem("username");
        localStorage.removeItem("token");
        localStorage.removeItem("dmsToken");
        this.router.navigate(["/login"]);
      }
    });
  }

  getAuditeeDmsToken() {
    let payload = { username: localStorage.getItem("username") };
    this.httpservice
      .post(ConstUrls.generateDmsSessionForAuditee, payload)
      .subscribe((res: any) => {
        if (res) {
          localStorage.setItem("auditeeDmsToken", res.dmsSession);
          console.log("GENERATE TOKEN FOR DMS");
        }
      });
  }




  initForm() {
    this.PBCForm = this.formBuilder.group({
      auditId: ["", []],
      auditableEntity: ["", []],
      documentRequest: ["", []],
      auditScope: ["", []],
      remarks: ["", []],
      dueDate: ["", []],


    });
  }
  generateToken() {
    let payload = { username: this.username };
    this.httpservice.post(ConstUrls.generateDmsSessionForAuditee, payload).subscribe((res: any) => {
      if (res) {
        localStorage.setItem('auditeeDmsToken', res.dmsSession);
        console.log('GENERATE TOKEN FOR DMS');
      }
    })
  }
  PbcFileUploaded(event, index) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      console.log("fil anme", files);
      formData.append("file", files[0], files[0]["name"]);
      formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
      formData.append("userType", 'auditee');
      formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=EngagementPlanning,Feature-Name=closingMeeting,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
      formData.append("routerLink", ""),
        formData.append("subModuleName", "pbc-form"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.auditId),
        formData.append("auditName", "name"),
        formData.append("moduleName", "EngagementPlanning"),
        formData.append("checkedinDate", moment().format("L"));
      console.log('****** formData', formData);
      this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            console.log('filename', res[i].fileName);
            let splitFileNameArr = res[i].fileName.split(".");
            let fileExtention = splitFileNameArr[splitFileNameArr.length - 1];
            this.pbcData.auditPbc.documentList[index].dmsInfo = {
              documentNo: res[i].documentNo,
              documentName: res[i].fileName,
              documentUrlToken: res[i].urlToken,
              extenstion : fileExtention
         
            }
            this.pbcData.auditPbc.documentList[index].dateUploaded = new Date();
            this.pbcData.auditPbc.documentList[index].dateUploadedDisplay = this.convertDateToFormat("mm.dd.yyyy", this.pbcData.auditPbc.documentList[index].dateUploaded);
            this.pbcData.auditPbc.documentList[index].uploadedBy = localStorage.getItem("username");
            
            console.log("getDms", this.pbcData.auditPbc.documentList)
         
          }


          this.payload.documentList = this.pbcData.auditPbc.documentList;
          console.log("Final payload : " + JSON.stringify(this.payload));

          this.httpservice.post(ConstUrls.updatePbcAttachment, this.payload).subscribe((res: any) => {
            if (res) {
              // this.getAllDmsDocs();
              console.log('attched filess array:', this.attachedFiles)
              console.log('******res', res)
              this.title = "Success";
              
              this.content = "File Uploaded successfully...";
              this.myCommonAlert(this.title, this.content);
         

            }
          });
        }





      }
        //  }
        , (error) => {
          this.myCommonAlert('Error', error.message);

        })



    } else {
      this.myCommonAlert('Error', 'Invalid file');
      $("#myFile").val('');
    }
  }
  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    console.log('extension', extension)
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }
  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    let currentUser = localStorage.getItem("username");
    // this.documentRequestArray.push(this.PBCForm.value);
    console.log('Arry', this.pbcData);
    if (this.pbcData.auditPbc.preparer && this.pbcData.auditPbc.preparer.length > 0) {
      if (!this.pbcData.auditPbc.preparer.includes(currentUser) && this.pbcData.auditPbc.preparer.length < 2) {
        this.pbcData.auditPbc.preparer.push(currentUser);
      }
    }
    else {
      this.pbcData.auditPbc.preparer = [currentUser];
    }

    this.pbcData.auditPbc.documentList.forEach(d => {
      // if (d.isApproved === true) {
      //   d.isLocked = true;
      //   d.isApproved = false;
      // }
    });

    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updatePbcDoc, { auditPbc: this.pbcData.auditPbc, auditId: this.auditId })
          .subscribe((res: any) => {
            if (res) {
              console.log('@@@', res)
              this.title = "Success";
              this.content = "PBC Submitted successfully...";
              this.myCommonAlert(this.title, this.content);
              this.closeModal();
            }
          },
            err => {
              this.myCommonAlert("Failure", "Please try again");
            });
      }
    });

  }

  
  myCommonAlert( title,content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",
      
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }
  closeModal() {
    this.closePbcNewForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  // print click added by Abu - 16-09-2020 start
  print() {
    window.print();
  }


  datatable() {
    setTimeout(() => {
      $("#pbcDocumentTable").DataTable({
        processing: true,
        responsive: true,
        // fixedHeader: true,
        autoWidth: false,
        scrollX: false,
        scrollCollapse: true,
        // bPaginate: true,
        columnDefs: [
          { targets: 0, visible: false, width: '1px' }
        ],
        lengthMenu: [
          [5, 10, 20, 50, 100, -1],
          [5, 10, 20, 50, 100, "All"]
        ],
        // bInfo: true,
        searching: false,
      });
    }, 0);
  }

  toggleEditable(event) {
    if (event.target.checked) {
      $('.switch-toggle-block input:checked').parent('.switch-toggle-block').addClass('show-checked');
    } else {
      $('.switch-toggle-block').removeClass('show-checked');
    }
  }

  addnewPBCpopup() {
    this.addnewPBC = true;
  }
  addnewFile() {
    this.editsearchDocument = true;
  }
  getSelectedDocs() {
    this.editsearchDocument = false;
  }
  closeSearch() {
    this.editsearchDocument = false;
  }

  // table and search functionlity added by Abu - 17-09-2020 end 
}