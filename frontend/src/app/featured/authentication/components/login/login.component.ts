import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { LocalStorageService } from "src/app/core/data-services/local-storage.service";
import { Router, ActivatedRoute } from "@angular/router";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.less"],
})
export class LoginComponent implements OnInit {
  modalRef: BsModalRef;
  loginForm: FormGroup;
  submitted = false;

  paused = false;
  unpauseOnArrow = false;
  pauseOnIndicator = false;
  pauseOnHover = true;
  redirectUrl;
  constructor(
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private localStorage: LocalStorageService,
    private router: Router,
    private modalService: BsModalService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['redirectUrl']) {
        this.redirectUrl = params['redirectUrl'];
      }
    });
    
    if (this.localStorage.getLocalStorage('token')) {
      this.router.navigate(['/rcm'])
    }
    this.initForm();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // display form values on success

    //change class as per requirement, error: error-class, sucess:sucess-class
    if (this.loginForm.value.username && this.loginForm.value.password)
     {
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
      //console.log(ConstUrls.login);
      this.httpService.post(ConstUrls.login, this.loginForm.value).subscribe(
        (res: any) => {
          //console.log("Response: ", res);
          if (res["message"] === "Unprocessable Entity") {
            const initialState = {
              title: "Error",
              content: "Check Credentials",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "danger-class",
              backdrop: "static",
              keyboard: false,
            });
          } else if (res["message"] === "Unauthorized") {
            const initialState = {
              title: "Error",
              content: "Check username or password",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "danger-class",
              backdrop: "static",
              keyboard: false,
            });
          } else {
            if (res.token) {
              //DO not remove Local Storage since this is importent for Chatroom and InTray
              localStorage.setItem("username", res.username);
              this.localStorage.setLocalStorage("token", res.token);
              
              let payload = {username:res.username};
              this.httpService.securePost(ConstUrls.getDmsSessionToken , payload).subscribe((res:any)=>{
                if(res){
                  if(res.dmsSessionId){
                    if(localStorage.getItem('dmsToken')){
                      localStorage.removeItem('dmsToken');
                      localStorage.setItem('dmsToken',res.dmsSessionId);
                    }else{
                    localStorage.setItem('dmsToken',res.dmsSessionId);

                      
                    }
                  }
                  console.log('******checkSession', res)

                }
              })
              if(this.redirectUrl){
                this.router.navigateByUrl(this.redirectUrl);
              }
              else{
                this.router.navigateByUrl("/rcm/dashboard");
              }
              
            }
          }

          // this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'sucess-class', backdrop: 'static', keyboard: false});
          // if (res.token) {

          //   localStorage.setItem('username', res.username);
          //   this.localStorage.setLocalStorage('token', res.token);

          //   this.router.navigateByUrl('/')

          // }
        },
        (err) => {
          //console.log("err", err);
        }
      );
    }
  }
}
