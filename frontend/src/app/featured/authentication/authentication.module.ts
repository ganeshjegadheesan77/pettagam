import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationRoutingModule } from './authentication-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarouselModule } from "ngx-bootstrap/carousel";



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    CarouselModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthenticationModule { }
