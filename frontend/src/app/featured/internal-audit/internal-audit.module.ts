import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { FormsModule } from '@angular/forms';
import { InternalAuditRoutingModule } from './internal-audit-routing.module';
import { InternalAuditComponent } from './internal-audit.component';
import { ReportingComponent } from './components/reporting/reporting.component';
import { ReportingContainerComponent } from './components/reporting/reporting-container/reporting-container.component';
import { ActiveAuditsComponent } from './components/reporting/active-audits/active-audits.component';
import { CostReportingComponent } from './components/reporting/cost-reporting/cost-reporting.component';
import { QualityAssuranceComponent } from './components/reporting/quality-assurance/quality-assurance.component';
import { RcmViewComponent } from './components/reporting/rcm-view/rcm-view.component';
import { CostRcmViewComponent } from './components/reporting/cost-rcm-view/cost-rcm-view.component';
import { AuditUniverseComponent } from './components/audit-universe/audit-universe.component';
import { AuditViewComponent } from './components/audit-universe/audit-view/audit-view.component';
import { AuditChartComponent } from './components/audit-universe/audit-chart/audit-chart.component';
import { AuditUniverseTableComponent } from './components/audit-universe/audit-universe-table/audit-universe-table.component';
import { AuditDetailsComponent } from './components/audit-universe/audit-details/audit-details.component';
import { AuditMitigatingFactorsComponent } from './components/audit-universe/audit-mitigating-factors/audit-mitigating-factors.component';
import { AuditOverviewComponent } from './components/audit-universe/audit-overview/audit-overview.component';
import { CostTopPaneComponent } from './components/reporting/cost-top-pane/cost-top-pane.component';
import { EngagementPlanningComponent } from './components/engagement-planning/engagement-planning.component';
import { BudgetingComponent } from './components/engagement-planning/budgeting/budgeting.component';
import { MemorandumComponent } from './components/engagement-planning/memorandum/memorandum.component';
import { ProgramComponent } from './components/engagement-planning/program/program.component';
import { AmaaVeiwComponent } from './components/engagement-planning/amaa-veiw/amaa-veiw.component';
import { ResPlanningFormComponent } from './components/engagement-planning/res-planning-form/res-planning-form.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


import { AnnualAuditPlanningComponent } from './components/annual-audit-planning/annual-audit-planning.component';
import { AnnualAuditChartComponent } from './components/annual-audit-planning/annual-audit-chart/annual-audit-chart.component';
import { CreateAuditFormComponent } from './components/annual-audit-planning/create-audit-form/create-audit-form.component';
import { AnnualAuditPlanningTableComponent } from './components/annual-audit-planning/annual-audit-planning-table/annual-audit-planning-table.component';
import { AnnualAuditPlanningViewComponent } from './components/annual-audit-planning/annual-audit-planning-view/annual-audit-planning-view.component';
import { FieldWorkComponent } from './components/field-work/field-work.component';
import { FieldRcmViewComponent } from './components/field-work/field-rcm-view/field-rcm-view.component';
import { FieldsPanesComponent } from './components/field-work/fields-panes/fields-panes.component';
import { AuditPlanMemoPopupComponent } from './components/field-work/audit-plan-memo-popup/audit-plan-memo-popup.component';
import { OpenMeetPopupComponent } from './components/field-work/open-meet-popup/open-meet-popup.component';
import { AuditAnnouncementPopupComponent } from './components/field-work/audit-announcement-popup/audit-announcement-popup.component';
import { PbcPopupComponent } from './components/field-work/pbc-popup/pbc-popup.component';
import { ClosingMeetingPopupComponent } from './components/field-work/closing-meeting-popup/closing-meeting-popup.component';
import { DraftPopupComponent } from './components/field-work/draft-popup/draft-popup.component';
import { FwRecalculationPopupComponent } from './components/field-work/fw-recalculation-popup/fw-recalculation-popup.component';
import { IssueBudgetSetupPopupComponent } from './components/field-work/issue-budget-setup-popup/issue-budget-setup-popup.component';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AuditPdfComponent } from './components/audit-universe/audit-pdf/audit-pdf.component';
import { AuditReportComponent } from './components/annual-audit-planning/audit-report/audit-report.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { ReactiveFormsModule } from "@angular/forms";
import { CopyAuditFormComponent } from './components/annual-audit-planning/copy-audit-form/copy-audit-form.component';
import { PbcNewPopupComponent } from './components/field-work/pbc-new-popup/pbc-new-popup.component';
import { IssueBudgetSetupNewPopupComponent } from './components/field-work/issue-budget-setup-new-popup/issue-budget-setup-new-popup.component';
import { FwRecalcultionNewPopupComponent } from './components/field-work/fw-recalcultion-new-popup/fw-recalcultion-new-popup.component';
import { AuditPlanningDetailsComponent } from './components/annual-audit-planning/audit-planning-details/audit-planning-details.component';
import { AuditPlanningDetailsEditComponent } from './components/annual-audit-planning/audit-planning-details-edit/audit-planning-details-edit.component';
import { AuditCalndarDetailsComponent } from './components/annual-audit-planning/audit-calndar-details/audit-calndar-details.component';
import { FwCtPopupComponent } from './components/field-work/fw-ct-popup/fw-ct-popup.component';
import { AuditTeamDetailPopupComponent } from './components/field-work/audit-team-detail-popup/audit-team-detail-popup.component';
import {NgxTimeSchedulerModule} from 'ngx-time-scheduler';

import { AngularDraggableModule } from "angular2-draggable";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AuditAraNewComponent } from './components/audit-universe/audit-ara-new/audit-ara-new.component';
import { AuditAraEditComponent } from './components/audit-universe/audit-ara-edit/audit-ara-edit.component';
import { TeamTimelineFormComponent } from './components/field-work/team-timeline-form/team-timeline-form.component';
import { EditAuditFormComponent } from './components/annual-audit-planning/edit-audit-form/edit-audit-form.component';
@NgModule({

  declarations: [
    InternalAuditComponent,
    ReportingComponent,
    ReportingContainerComponent,
    ActiveAuditsComponent,
    CostReportingComponent,
    QualityAssuranceComponent,
    RcmViewComponent,
    CostRcmViewComponent,
    AuditUniverseComponent,
    AuditViewComponent,
    AuditChartComponent,
    AuditUniverseTableComponent,
    AuditDetailsComponent,
    AuditMitigatingFactorsComponent,
    AuditOverviewComponent,
    CostTopPaneComponent,
    EngagementPlanningComponent,
    BudgetingComponent,
    MemorandumComponent,
    ProgramComponent,
    AmaaVeiwComponent,
    AnnualAuditPlanningComponent,
    AnnualAuditChartComponent,
    CreateAuditFormComponent,
    AnnualAuditPlanningTableComponent,
    AnnualAuditPlanningViewComponent,
    FieldWorkComponent,
    FieldsPanesComponent,
    FieldRcmViewComponent,
    ResPlanningFormComponent,
    AuditPlanMemoPopupComponent,
    OpenMeetPopupComponent,
    AuditAnnouncementPopupComponent,
    PbcPopupComponent,
    ClosingMeetingPopupComponent,
    DraftPopupComponent,
    FwRecalculationPopupComponent,
    AuditPdfComponent,
    AuditReportComponent,
    IssueBudgetSetupPopupComponent,
    CopyAuditFormComponent,
    PbcNewPopupComponent,
    IssueBudgetSetupNewPopupComponent,
    FwRecalcultionNewPopupComponent,
    AuditPlanningDetailsComponent,
    AuditPlanningDetailsEditComponent,
    AuditCalndarDetailsComponent,
    FwCtPopupComponent,
    AuditTeamDetailPopupComponent,
    AuditAraNewComponent,
    AuditAraEditComponent,
    TeamTimelineFormComponent,
    EditAuditFormComponent
    ],
    

    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InternalAuditRoutingModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    PdfViewerModule,
    AutocompleteLibModule,
    NgxFileDropModule,
    NgxTimeSchedulerModule,
    AngularDraggableModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class InternalAuditModule { }
