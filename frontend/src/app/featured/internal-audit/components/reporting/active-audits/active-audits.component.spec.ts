import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveAuditsComponent } from './active-audits.component';

describe('ActiveAuditsComponent', () => {
  let component: ActiveAuditsComponent;
  let fixture: ComponentFixture<ActiveAuditsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveAuditsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveAuditsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
