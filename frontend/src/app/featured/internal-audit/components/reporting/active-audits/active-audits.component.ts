import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from '../../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
declare var $;

@Component({
  selector: 'app-active-audits',
  templateUrl: './active-audits.component.html',
  styleUrls: ['./active-audits.component.less']
})
export class ActiveAuditsComponent implements OnInit {
  private loadComponent = false;
  private loadcityId = false;
  auditList:any;
  data:any;
  dataTable: any;
  activeAuditTableHeaders: any;
  showHideHeadersActiveAudit: any;
  isLoading = false;
  locations: any;
  activeList: any;
  sites: any[];

  constructor(private httpService: HttpService, private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
  //  this.datatable();
    this.sites=[];
    this.activeAudits();
    this.outsideClick();
    this.getLocations();
    
    }

    getLocations(){
      this.httpService.secureGet(ConstUrls.getLocationsForAA).subscribe((res:any)=>{
        if(res){
            this.locations = res;
        }
      })
    }

    changeLocation(val){
        let table = $('#activeAuditTable').DataTable();
        table.destroy();
        this.isLoading = true;
        this.activeAuditTableHeaders = [];
        this.showHideHeadersActiveAudit = [];
      if(val !=0){
        this.auditList = this.activeList;
        console.log('this.auditList', this.auditList)
        this.sites=[];
        this.auditList =  this.auditList.filter(function(al) {
          return al.parent == val;
        });
        let payload = {parent:val}
        this.httpService.securePost(ConstUrls.getSitesForAA , payload).subscribe((res:any)=>{
          if(res){
            this.sites=[];
            this.sites=res;
            console.log('this.sites', this.sites)
          }
        })
      }else{
        this.auditList = this.activeList;
      }
      this.setTable();
      
    }

    changeSites(val){
      console.log('val', val)
      let table = $('#activeAuditTable').DataTable();
        table.destroy();
        this.isLoading = true;
        this.activeAuditTableHeaders = [];
        this.showHideHeadersActiveAudit = [];
      if(val !=0){
        this.auditList = this.activeList;
        
        this.auditList =  this.auditList.filter(function(al) {
          return al.uID == val;
        });
      }else{
        this.auditList = this.activeList;
      }
      this.setTable();
    }

    setTable(){
      this.activeAuditTableHeaders = [
        "Auditable Entity",
        "Scope",
        "Audit ID",
        "Audit Lead",
        "Location",
        "Risk Level",
        "Progress",
        "Rating",
        "Completed"
      ];
      this.showHideHeadersActiveAudit = this.activeAuditTableHeaders;
      this.isLoading = false;
      this.datatable();
    }
   
    activeAudits() {
    //   let table = $('#activeAuditTable').DataTable();
    // table.destroy();
    this.isLoading = true;
    this.activeAuditTableHeaders = [];
    this.showHideHeadersActiveAudit = [];

    this.httpService.secureGet(ConstUrls.getActiveAudits).subscribe((res:any)=>{
      this.auditList = res;
      this.activeList = res;
      console.log('auditList', this.auditList)
      this.setTable();
    })
    // this.httpService.securePost(ConstUrls.getAuditList).subscribe(response => {
    //   //console.log("getAllAuditReport::",response)
    //   this.auditList = response;
    //     // this.httpService.secureGet(ConstUrls.getActiveAudits).subscribe(r=>{
    //     //   this.data=r;
    //     //   console.log('r', r);
    //     //   if(r){
    //     //    this.auditList.forEach(element => {
    //     //      if(element.newAuditId){
    //     //       element.auditId= element.newAuditId.auditId;
    //     //       element.location=element.name;
    //     //      // element.barColor="medium",
    //     //       element.riskStatus=this.calculateProgress(element.newAuditId);
    //     //       //element.riskProgress=0;
    //     //       var result=0
    //     //       result = element.newAuditId.riskProgress
    //     //       if(result)
    //     //      {
    //     //         if(result <= 7 && result>=0)
    //     //         {
    //     //         element.riskProgress = "Low";
    //     //         element.risk= 'btn low';
    //     //       }
    //     //       else
    //     //       {
    //     //         if(result >=8 && result <=14){
    //     //           element.riskProgress = "Medium";
    //     //           element.risk = 'btn medium';
  
    //     //         }
    //     //         else
    //     //         {
    //     //           element.riskProgress = "High";
    //     //           element.risk = 'btn high';
                  
    //     //         }
    //     //       }
             
    //     //      }
    //     //      }
             
    //     //      if(element.newAuditId && element.newAuditId.rpId){
    //     //      var data= this.data.filter(x=> {return x.rpId==element.newAuditId.rpId})
    //     //      element.auditLead= data[0].auditLead;
    //     //      var result=0
    //     //       result = element.newAuditId.riskProgress
    //     //       if(result <= 7){
    //     //         element.riskProgress = "Low";
    //     //         element.risk= 'btn low';
    //     //       }else if(result >=8 && result <=14){
    //     //         element.riskProgress = "Medium";
    //     //         element.risk = 'btn medium';

    //     //       }else{
    //     //         element.riskProgress = "High";
    //     //         element.risk = 'btn high';
                
    //     //       }
    //     //      }
    //     //    });

    //     //   }
    //     // })
        
    // })

    
    
    }
    calculateProgress(e){
      let count=0
      if(e.audit_announce_new) count++ //1
      if(e.audit_closing_meeting) count++ //2
      if(e.audit_draft_report_new) count++ //3
      if(e.audit_engagement_plan_new) count++ //4
      if(e.audit_opening_meeting_new || e.audit_opening_meeting) count++  //5
      if(e.audit_pbc && e.audit_pbc.length>0) count++ //6
      if(e.audit_plan_memo_new) count++ //7
      if(e.audit_program) count++ //8
      if(e.audit_program=="true") count++ //9
      if(e.audit_issues && e.audit_issues.length>0) count++ //10
      return count;
    }
    

  loadMyCityId(){
    this.loadcityId = true;
    $('.pop-up-form').removeClass('d-none');
  }
  loadMyChildComponent(){
    this.loadComponent = true;
    $('.pop-up-form').removeClass('d-none');
 }
  datatable() {
    this.chRef.detectChanges();
    const activeAuditsTable: any = $("#activeAuditTable");
    const activeAuditListReporting = this.dataTableService.initializeTable(
      activeAuditsTable,
      `activeAuditReporting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      activeAuditListReporting,
      `audit-audits-select-all`
    );
    this.dataTableFuncService.columnDropDown(activeAuditListReporting, `activeAudits-hideshow`);
    this.dataTableFuncService.expandContainer(activeAuditListReporting);
    $("#activeAuditTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#audit-audits-select-all").prop("checked", false);
          var el = $("#audit-audits-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#activeAuditTable tr td input[type='checkbox']:checked").length ==
          $("#activeAuditTable tr td input[type='checkbox']").length
        ) {
          $("#audit-audits-select-all").prop("checked", true);
        }
      }
    );

}
SettingActiveAudit(e) {
  $(".dropdown-menu-column").toggleClass("show-bar");
  $(".dt-button-collection").hide();
  e.stopPropagation();
}
outsideClick() {
  $("body").on("click", function () {
    $(".dropdown-menu-column").removeClass("show-bar");
  });
}
}


