import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
@Component({
  selector: 'app-rcm-view',
  templateUrl: './rcm-view.component.html',
  styleUrls: ['./rcm-view.component.less']
})
export class RcmViewComponent implements OnInit {

  totalAudit:any;
  activeAudit:any;
  year:any;
  noOfAudits:any
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.secureGet(ConstUrls.getAuditList).subscribe((response:any)=>{
      if(!response) { this.activeAudit = 0; }
      this.totalAudit=response;
      //console.log('Total Controls: ' , this.totalAudit);
    })
    this.year = new Date().getFullYear();
    let payload = {year : this.year};
    
    this.httpService.securePost(ConstUrls.getNoOfAuditsCurrYear , payload).subscribe((res:any)=>{
      if(res[0]){
       
         this.noOfAudits= res[0]['year'];
      }else{
        this.noOfAudits = '0';
      }
    })

  // this.httpService.secureGet(ConstUrls.).subscribe(response => {
  //   console.log('action count::',response)
  //   this.activeAudit = response;
  
  // });
  }

}
