import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostReportingComponent } from './cost-reporting.component';

describe('CostReportingComponent', () => {
  let component: CostReportingComponent;
  let fixture: ComponentFixture<CostReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
