import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reporting-container',
  templateUrl: './reporting-container.component.html',
  styleUrls: ['./reporting-container.component.less']
})
export class ReportingContainerComponent implements OnInit {
  private reportingChartView = true;
  private reportingDashboard = false;
  constructor() { }

  ngOnInit() {
  }
  activeAudit() {
    // this.reportingDashboard = false;
    // this.reportingChartView = true;
  }
  costReporting() {
    // this.reportingDashboard = true;
    // this.reportingChartView = false;
  }
}
