import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostTopPaneComponent } from './cost-top-pane.component';

describe('CostTopPaneComponent', () => {
  let component: CostTopPaneComponent;
  let fixture: ComponentFixture<CostTopPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostTopPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostTopPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
