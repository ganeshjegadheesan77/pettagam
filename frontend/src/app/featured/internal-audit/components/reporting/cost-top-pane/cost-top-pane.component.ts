import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';

@Component({
  selector: 'app-cost-top-pane',
  templateUrl: './cost-top-pane.component.html',
  styleUrls: ['./cost-top-pane.component.less']
})
export class CostTopPaneComponent implements OnInit {

  totalAudit:any;
  activeAudit:any;
  year:any;
  noOfAudits:number;
  auditList: any;
  activeCount: number;
  completedCount: number;
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.getActiveCount();
    this.year = new Date().getFullYear();
    this.currentYearAuditCount();
    this.getCompletedCount();
  }

  currentYearAuditCount(){
    let payload = {year : this.year};
    this.httpService.securePost(ConstUrls.auditCount, payload).subscribe((res: any) => {
        if(res){
          this.noOfAudits = res;
        }else{
          this.noOfAudits = 0;
        }
        
      });
  }

  getActiveCount(){
    this.httpService.secureGet(ConstUrls.getActiveAudits).subscribe((res:any)=>{
      if(res){
        this.auditList = res;
        this.activeCount=this.auditList.length;
      }else{
        this.activeCount= 0;
      }
    })
  }

  getCompletedCount(){
    this.httpService.secureGet(ConstUrls.getCompletedAuditForAA).subscribe((res:any)=>{
      if(res){
        console.log('Completed ', res)
        this.completedCount = res.length;
      }
    })
  }
}
