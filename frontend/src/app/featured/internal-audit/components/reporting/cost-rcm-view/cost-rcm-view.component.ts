import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpService } from "../../../../../core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
declare var $;

@Component({
  selector: "app-cost-rcm-view",
  templateUrl: "./cost-rcm-view.component.html",
  styleUrls: ["./cost-rcm-view.component.less"],
})
export class CostRcmViewComponent implements OnInit {
  loadComponent = false;
  loadcityId = false;
  showHideHeadersActiveAudit: any;
  SettingActiveAudit: any;
  auditList: any;
  riskProgress: any;
  data: any;
  dataTable: any;
  costReportingTableHeaders: any;
  showHideHeadercostReporting: any;
  constructor(
    private httpService: HttpService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) {}

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.getCostReporting();
  }
  getCostReporting() {
    this.httpService
      .securePost(ConstUrls.getAuditList)
      .subscribe((response) => {
        //console.log("getAllAuditReport::",response)
        this.auditList = response;
        this.httpService
          .secureGet(ConstUrls.getAllResourcePlanning)
          .subscribe((r) => {
            this.data = r;
            if (r) {
              this.auditList.forEach((element) => {
                if (element.newAuditId) {
                  element.auditId = element.newAuditId.auditId;
                  // element.barColor="medium",
                  // element.riskProgress=0;
                  element.riskStatus = this.calculateProgress(
                    element.newAuditId
                  );
                }
                if (element.newAuditId && element.newAuditId.rpId) {
                  var data = this.data.filter((x) => {
                    return x.rpId == element.newAuditId.rpId;
                  });
                  element.budget = data[0].budget;
                }
              });
            }
          });
        this.costReportingTableHeaders = [
          "Auditable Entity",
          "Scope",
          "Audit ID",
          "Budgeted Hours",
          "Actual Hours",
          "Under/Over",
          "Progress",
          "Projected Hours left",
          "Completed",
        ];
        this.showHideHeadercostReporting = this.costReportingTableHeaders;
        this.costReportingdataTable();
      });
  }

  loadMyCityId() {
    this.loadcityId = true;
    $(".pop-up-form").removeClass("d-none");
  }
  loadMyChildComponent() {
    this.loadComponent = true;
    $(".pop-up-form").removeClass("d-none");
  }
  calculateProgress(e) {
    let count = 0;
    if (e.audit_announce_new) count++; //1
    if (e.audit_closing_meeting) count++; //2
    if (e.audit_draft_report_new) count++; //3
    if (e.audit_engagement_plan_new) count++; //4
    if (e.audit_opening_meeting_new || e.audit_opening_meeting) count++; //5
    if (e.audit_pbc && e.audit_pbc.length > 0) count++; //6
    if (e.audit_plan_memo_new) count++; //7
    if (e.audit_program) count++; //8
    if (e.audit_program == "true") count++; //9
    if (e.audit_issues && e.audit_issues.length > 0) count++; //10
    return count;
  }

  costReportingdataTable() {
    this.chRef.detectChanges();
    const costReportingListTable: any = $("#costReportingTable");
    const costReportingList = this.dataTableService.initializeTable(
      costReportingListTable,
      `costReporting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      costReportingList,
      `cost-reporting-select-all`
    );
    this.dataTableFuncService.columnDropDown(
      costReportingList,
      `costReporting-hideshow`
    );
    this.dataTableFuncService.expandContainer(costReportingList);
    $("#costReportingTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#cost-reporting-select-all").prop("checked", false);
          var el = $("#cost-reporting-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#costReportingTable tr td input[type='checkbox']:checked")
            .length ==
          $("#costReportingTable tr td input[type='checkbox']").length
        ) {
          $("#cost-reporting-select-all").prop("checked", true);
        }
      }
    );
  }
  SettingCostreporting(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    $(".dt-button-collection").hide();
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $(".dropdown-menu-column").removeClass("show-bar");
    });
  }
}
//     var trIndex = null;
// $("#riskcontrolmatrixTable tr td").mouseenter(function () {
//     trIndex = $(this).parent();
//     $(trIndex).find("td:last-child").html('<div class="edit-delete-btn"><a href="">Edit</a>&nbsp;&nbsp;<a href="">Delete</a></div>');
// });

// // remove button on tr mouseleave

// $("#riskcontrolmatrixTable tr td").mouseleave(function () {
//     $(trIndex).find('td:last-child').html("&nbsp;");
// });
