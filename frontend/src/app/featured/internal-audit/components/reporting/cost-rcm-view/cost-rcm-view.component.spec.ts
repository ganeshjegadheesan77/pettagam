import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostRcmViewComponent } from './cost-rcm-view.component';

describe('CostRcmViewComponent', () => {
  let component: CostRcmViewComponent;
  let fixture: ComponentFixture<CostRcmViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostRcmViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostRcmViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
