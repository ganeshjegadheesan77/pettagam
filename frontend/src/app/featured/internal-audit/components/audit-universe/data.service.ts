import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
@Injectable({providedIn:'root'})
export class DataService{
    private dataDetail = new BehaviorSubject<any>([]);
    private siteDetail = new BehaviorSubject<any>([]);
    private auditDetail = new BehaviorSubject<any>([]);
    private audit = new BehaviorSubject<any>([]);
    private nameDetail = new BehaviorSubject<any>([]);
    private stateNames = new BehaviorSubject<any>([]);

    private chartDataReceiver = new BehaviorSubject<any>([]);
    chartData = this.chartDataReceiver.asObservable();

    name= this.nameDetail.asObservable();
    currentmessage= this.dataDetail.asObservable();
    getSites= this.siteDetail.asObservable();
    auditData= this.auditDetail.asObservable();
    auditable=this.audit.asObservable();
    states = this.stateNames.asObservable();
    
    constructor(){}
    changeMessage(dataObject:any){
        this.dataDetail.next(dataObject)
    }

    getSiteRecord(dataObject:any){
        this.siteDetail.next(dataObject)
    }

    updateChart(dataObject:any){
        this.chartDataReceiver.next(dataObject);
    }
}