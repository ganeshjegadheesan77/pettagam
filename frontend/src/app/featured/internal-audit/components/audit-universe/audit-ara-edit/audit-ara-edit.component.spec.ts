import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditAraEditComponent } from './audit-ara-edit.component';

describe('AuditAraEditComponent', () => {
  let component: AuditAraEditComponent;
  let fixture: ComponentFixture<AuditAraEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditAraEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditAraEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
