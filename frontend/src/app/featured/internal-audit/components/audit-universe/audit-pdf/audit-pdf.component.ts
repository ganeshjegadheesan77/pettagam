import { Component, OnInit } from '@angular/core';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

@Component({
  selector: 'app-audit-pdf',
  templateUrl: './audit-pdf.component.html',
  styleUrls: ['./audit-pdf.component.less']
})
export class AuditPdfComponent implements OnInit {
  position;
  pdfSrc = "../../../../../assets/pdf/confidential-ia-report.pdf";
  constructor(private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
  }
  closeModal(){
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0}
  }
  printAuditpdf(){
    window.print();
  }

}
