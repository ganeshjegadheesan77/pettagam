import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditPdfComponent } from './audit-pdf.component';

describe('AuditPdfComponent', () => {
  let component: AuditPdfComponent;
  let fixture: ComponentFixture<AuditPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
