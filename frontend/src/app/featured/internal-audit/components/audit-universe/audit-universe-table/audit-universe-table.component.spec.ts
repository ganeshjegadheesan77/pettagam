import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditUniverseTableComponent } from './audit-universe-table.component';

describe('AuditUniverseTableComponent', () => {
  let component: AuditUniverseTableComponent;
  let fixture: ComponentFixture<AuditUniverseTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditUniverseTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditUniverseTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
