import { Component, OnInit, ChangeDetectorRef, Input } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { DataService } from "../data.service";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { GalaxyDataService } from "../galaxy-data.service";
import * as _ from "lodash";

declare var $;
@Component({
  selector: "app-audit-universe-table",
  templateUrl: "./audit-universe-table.component.html",
  styleUrls: ["./audit-universe-table.component.less"],
})
export class AuditUniverseTableComponent implements OnInit {
  public loadPdf = false;
  public createAuditComponent = false;

  modalRef: BsModalRef;
  currentName = "";
  currentYear: number;
  title: string;
  btnColor: string;
  // assign for multi table added by Abu - 17-09-2020 start
  multitbl = true;
  currenttbl = false;
  // assign for multi table added by Abu - 17-09-2020 end 

  // assigned marked for switch toggle added by Abu - 17-09-2020 start
  marked = false;
  currentChildIds = [];
  // assigned marked for switch toggle added by Abu - 17-09-2020 end 

  minDate = new Date();
  selectedAuditId;
  @Input() auditMasters;
  multiAudits = [];
  currentAudits = [];
  duplicateList = [];
  constructor(
    private httpservice: HttpService,
    public data: DataService,
    private modalService: BsModalService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    private galaxyDataService: GalaxyDataService
  ) { }
  ngOnInit() {
    // initialise the current datatable added by Abu - 17-09-2020 start
    // initialise the current datatable added by Abu - 17-09-2020 end 

    window.addEventListener("message", this.onUniverseChange.bind(this), false);
    // this.datatable();
    $('[data-toggle="tooltip"]').tooltip();
    this.getAuditMasters();
  }

  getAuditMasters() {
    if (this.auditMasters && this.auditMasters.length > 0) {
      this.currentName = this.auditMasters.find(a => a.id == "0.0").name;
      this.currentChildIds = this.auditMasters.find(a => a.id == "0.0").childrenId;
      this.getTableData(this.currentChildIds);
    }
  }

  getTableData(childIds) {
    this.currentChildIds = childIds;
    this.currentYear = new Date().getFullYear();
    // this.tableData = [];
    this.httpservice
      .securePost(ConstUrls.getTableDate, { levelFiveIds: childIds })
      .subscribe((res: any) => {
        this.multiAudits = res.data.multiAudits;
        this.currentAudits = res.data.currentAudits;
        this.currentAudits.forEach(a => {
          if (a.startDate && a.startDate != null) {
            a.startDate = new Date(a.startDate);
            return;
          }
          a.startDate = null;
        });
        console.log(res);
        this.getDataMulti(res.data, this.currentName);
        // this.datatable();
        this.datatable();

      },
        error => {
          this.multiAudits = [];
          this.currentAudits = [];
          this.getDataMulti({ multiAudits: [], currentAudits: [] }, this.currentName);
          this.datatable();
          // this.myCommonAlert("Failure", error.error['message']);
        }
      );
  }

  onUniverseChange(event) {
    if (event.data && event.data.childrenId && event.data.childrenId.length > 0) {
      this.currentName = event.data.name;
      var childIds = event.data.childrenId;
      this.currentChildIds = childIds;

      this.httpservice
        .securePost(ConstUrls.getTableDate, { levelFiveIds: childIds })
        .subscribe((res: any) => {
          this.multiAudits = res.data.multiAudits;
          this.currentAudits = res.data.currentAudits;
          this.currentAudits.forEach(a => {
            if (a.startDate && a.startDate != null) {
              a.startDate = new Date(a.startDate);
              return;
            }
            a.startDate = null;
          })
          console.log(res);
          this.getDataMulti(res.data, this.currentName);
          // this.datatable();
          if (this.multitbl) {
            if ($.fn.dataTable.isDataTable("#audituniverseTbl")) {
              let table = $("#audituniverseTbl").DataTable();
              table.destroy();
              this.datatable();
            }
            if (!$('#audituniverseTbl').hasClass('dataTable')) {
              this.datatable();
            }
          }
          if (this.currenttbl) {
            if ($.fn.dataTable.isDataTable("#currentaudituniverseTbl")) {
              let table1 = $("#currentaudituniverseTbl").DataTable();
              table1.destroy();
              this.currentdatatable();
            }
            if (!$('#currentaudituniverseTbl').hasClass('dataTable')) {
              this.currentdatatable();
            }
          }


        },
          error => {
            this.multiAudits = [];
            this.currentAudits = [];
            this.getDataMulti({ multiAudits: [], currentAudits: [] }, this.currentName);
            if (this.multitbl) {
              if ($.fn.dataTable.isDataTable("#audituniverseTbl")) {
                let table = $("#audituniverseTbl").DataTable();
                table.destroy();
                this.datatable();
              }
              if (!$('#audituniverseTbl').hasClass('dataTable')) {
                this.datatable();
              }
            }
            if (this.currenttbl) {
              if ($.fn.dataTable.isDataTable("#currentaudituniverseTbl")) {
                let table1 = $("#currentaudituniverseTbl").DataTable();
                table1.destroy();
                this.currentdatatable();
              }
              if (!$('#currentaudituniverseTbl').hasClass('dataTable')) {
                this.currentdatatable();
              }
            }
            // this.myCommonAlert("Failure", error.error['message']);
          }
        );
    }
  }

  createAudit() {
    this.selectedAuditId = null;
    this.createAuditComponent = true;
    // $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  editAudit(audit) {
    this.selectedAuditId = audit.auditId;
    this.createAuditComponent = true;
    // $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  deleteAudit(audit) {
    this.selectedAuditId = audit.auditId;
    const initialState = {
      title: "Confirmation",
      content: `Do you want to delete this Audit?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    this.modalRef.content.onClose.subscribe(r => {
      if (r) {
        this.httpservice.secureDelete(ConstUrls.deleteAudit + "/" + this.selectedAuditId).subscribe(
          res => {
            this.myCommonAlert("Success", res['message']);
            this.onUniverseChange({ data: { childrenId: this.currentChildIds, name: this.currentName } });
          },
          err => {
            this.myCommonAlert("Failure", err.error['message']);
          }
        );
      }
    })
  }

  addToCurrentYear(audit) {
    var curyear = new Date().getFullYear();
    if (audit.year + audit.rotation == curyear) {
      this.myCommonAlert("Not Eligible", "Cannot be added to current year!");
    }
    else if (audit.year + audit.rotation == curyear || audit.year == curyear || this.currentAudits.find(ca => ca.auditableEntity == audit.auditableEntity && ca.category.levelFive.data == audit.category.levelFive.data && ca.category.businessProcess == audit.category.businessProcess)) {
      this.myCommonAlert("Not Eligible", "Already exists in current year !");
    }
    else {
      this.httpservice.securePost(ConstUrls.addAuditToCurrentYear, { auditId: audit.auditId }).subscribe(
        res => {
          this.myCommonAlert("Success", res['message']);
          this.onUniverseChange({ data: { childrenId: this.currentChildIds, name: this.currentName } });
        },
        err => {
          this.myCommonAlert("Failure", err.error['message']);
        }
      );
    }
  }

  duplicateAudit() {
    var payload = {
      auditIds: this.multiAudits.map(a => a.auditId),
      username: localStorage.getItem("username")
    }
    if (this.duplicateList.length != 0) {
      payload.auditIds = this.duplicateList;
    }

    const initialState = {
      title: "Confirmation",
      content: `Do you want to duplicate Audits?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    this.modalRef.content.onClose.subscribe(r => {
      if (r) {
        console.log(payload);
        this.httpservice.securePost(ConstUrls.duplicateAudit, payload).subscribe(
          res => {
            this.myCommonAlert("Success", res['message']);
            $('input[type="checkbox"]').removeAttr('checked');
            this.duplicateList = [];
            this.onUniverseChange({ data: { childrenId: this.currentChildIds, name: this.currentName } });
          },
          err => {
            $('input[type="checkbox"]').removeAttr('checked');
            this.duplicateList = [];
            this.myCommonAlert("Info", err.error['message']);
          }
        )
      }
    });
  }

  blockInput(event) {
    event.preventDefault();
  }

  onStartDateChange(event, i) {
    var oldDate = this.currentAudits[i].startDate;
    if (this.currentAudits[i].startDate != event && event != null) {

      this.httpservice.securePut(ConstUrls.updateStartDate, { startDate: event, auditId: this.currentAudits[i].auditId }).subscribe(
        res => {
          this.currentAudits[i].startDate = new Date(event);
          this.myCommonAlert("Success", "Start Date updated successfully");
        },
        err => {
          this.currentAudits[i].startDate = oldDate;
        }
      );
    }
    console.log("on date change", event);
  }

  onIsActiveChange(event, i) {
    event.preventDefault();
    var payload = {
      auditId: this.currentAudits[i].auditId,
      isActive: event.target.checked
    }
    console.log("on toggle change", event, i);
    if (!event.target.checked) {
      const initialState = {
        title: "Confirmation",
        content: `Do you want to disable this Audit?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });

      this.modalRef.content.onClose.subscribe(r => {
        if (r) {

          this.httpservice.securePut(ConstUrls.updateConfirmToggle, payload).subscribe(
            res => {
              this.myCommonAlert("Success", "Audit disabled Successfully");
              this.currentAudits[i].isActive = event.target.checked;
              $(`input[name="${event.target.name}"]`)[0].checked = event.target.checked;
            },
            err => {
              this.myCommonAlert("Failure", err.error['message']);
            }
          );
        }
        else {
          this.currentAudits[i].isActive = true;
          $(`input[name="${event.target.name}"]`)[0].checked = true;
        }

      })
    }
    else {
      if (!this.currentAudits[i].startDate && this.currentAudits[i].startDate == null) {
        this.myCommonAlert("Failure", "Cannot enable without Start Date!");
        this.currentAudits[i].isActive = false;
        $(`input[name="${event.target.name}"]`)[0].checked = false;
      }
      else {
        this.httpservice.securePut(ConstUrls.updateConfirmToggle, payload).subscribe(
          res => {
            this.myCommonAlert("Success", "Audit enabled Successfully");
            this.currentAudits[i].isActive = event.target.checked;
            $(`input[name="${event.target.name}"]`)[0].checked = event.target.checked;
          },
          err => {
            this.myCommonAlert("Failure", err.error['message']);
          }
        );
      }

    }
  }
  myCommonAlert( title,content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }


  pdfClick() {
    this.loadPdf = true;
    this.createAuditComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  getDataMulti(dataUpdate, name) {
    var curyear = new Date().getFullYear();
    var obj: any = [];
    let siteDetail: any = [];
    let auditEntity: any = [];

    dataUpdate.multiAudits.forEach(a => {
      siteDetail.push(a.auditableEntity + " - " + a.category.levelFive.data);
    });

    var finalMultiArray: any = []; // response of first graph
    var finalAuditArray: any = [];
    var set: any = [];
    let parents = [];
    let stateNames;
    var parentData = this.auditMasters.filter(a => a.type == "Region");
    var groupedParents = _.groupBy(parentData, function (p) {
      return p.name
    });
    stateNames = [];
    for (var key of Object.keys(groupedParents)) {
      parents.push(groupedParents[key].map(g => g.id));
      stateNames.push(key);
    }

    //2nd graph
    for (let j = 0; j < parents.length; j++) {
      var matrix1 = [];
      dataUpdate.currentAudits.forEach(x => {
        // if (x.year + x.rotation == curyear && x.isCompleted) {
        //   if (parents[j].includes(x.category.levelThree.id)) {
        //     set.push(x.auditableEntity + " - " + x.category.levelFive.data);
        //     matrix1.push(1);
        //   } else matrix1.push(null);
        // } 
        if (x.year == curyear && !x.isCompleted) {
          if (parents[j].includes(x.category.levelThree.id)) {
            set.push(x.auditableEntity + " - " + x.category.levelFive.data);
            matrix1.push(1);
          } else matrix1.push(null);
        } else matrix1.push(null);
      })
      finalAuditArray.push(matrix1);
    }

    for (let j = 0, y = curyear + 1; j < 5; j++, y++) {
      var matrix = [];
      var temp = [];
      dataUpdate.multiAudits.forEach(a => {
        if ((y - a.year) % a.rotation == 0) {
          matrix.push(1);
        }
        else {
          matrix.push(null);
          return;
        }
      });
      finalMultiArray.push(matrix);
      console.log("matrix", matrix);
    }

    this.data.updateChart({
      currentmessage: finalMultiArray,
      getSites: siteDetail,
      auditData: finalAuditArray,
      name: name,
      states: stateNames,
      auditable: set
    });
  }

  datatable() {
    this.chRef.detectChanges();
    const auditUniverseTable: any = $("#audituniverseTbl");
    const auditUniverseIA = this.dataTableService.initializeTable(
      auditUniverseTable,
      `activeUniverse`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      auditUniverseIA,
      `audit-universe-select-all`
    );
    var thisref = this;
    this.dataTableFuncService.expandContainer(auditUniverseIA);
    $("#audituniverseTbl tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          thisref.duplicateList.splice(thisref.duplicateList.findIndex(d => d == thisref.multiAudits[Number.parseInt(this.parentElement.id)].auditId), 1);
          $("#audit-universe-select-all").prop("checked", false);
          var el = $("#audit-universe-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        else {
          thisref.duplicateList.push(thisref.multiAudits[Number.parseInt(this.parentElement.id)].auditId);
          if (
            $("#audituniverseTbl tr td input[type='checkbox']:checked").length ==
            $("#audituniverseTbl tr td input[type='checkbox']").length
          ) {
            $("#audit-universe-select-all").prop("checked", true);
          }
        }
        console.log(thisref.duplicateList);
      }
    );
  }

  // current datatable added by Abu - 17-09-2020 start 
  currentdatatable() {
    this.chRef.detectChanges();
    const currentauditUniverseTable: any = $("#currentaudituniverseTbl");
    const currentauditUniverseIA = this.dataTableService.initializeTable(
      currentauditUniverseTable,
      `currentactiveUniverse`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      currentauditUniverseIA,
      `current-audit-universe-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   auditUniverseIA,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(currentauditUniverseIA);
    $("#currentaudituniverseTbl tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#current-audit-universe-select-all").prop("checked", false);
          var el = $("#current-audit-universe-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#currentaudituniverseTbl tr td input[type='checkbox']:checked").length ==
          $("#currentaudituniverseTbl tr td input[type='checkbox']").length
        ) {
          $("#current-audit-universe-select-all").prop("checked", true);
        }
      }
    );
  }

  // current datatable added by Abu - 17-09-2020 end 

  // switch toggle function added by Abu - 17-09-2020 start 

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      this.multitbl = true;
      this.currenttbl = false;
      let table = $("#currentaudituniverseTbl").DataTable();
      table.destroy();
      this.datatable();
    } else {
      this.currenttbl = true;
      this.multitbl = false;
      let table = $("#audituniverseTbl").DataTable();
      table.destroy();
      this.currentdatatable();
    }
  }
  // switch toggle function added by Abu - 17-09-2020 end 

  onRowClick(data) {
    console.log("row click :", data);

    if (data.oldAuditIds && data.oldAuditIds.length > 0) {
      if (data.oldAuditIds.length > 2) {
        data.oldAuditIds = data.oldAuditIds.slice(
          Math.max(data.oldAuditIds.length - 2, 0)
        );
      }

      var payload = { oldAuditIds: data.oldAuditIds };
      this.httpservice
        .securePost(ConstUrls.getGalaxyData, payload)
        .subscribe((response: any) => {
          if (response) {
            this.galaxyDataService.changeMessage({
              name: data.auditableEntity + " - " + data.category.levelFive.data,
              response: response.data,
            });
          }
        });
    } else {
      this.galaxyDataService.changeMessage({ name: "no_data", response: [] });
    }
  }

  onAllAuditCheck(event) {
    this.duplicateList = [];
    if (event.target.checked) {
      this.duplicateList = this.multiAudits.map(a => a['auditId']);
    }
    console.log(this.duplicateList);
    console.log(event.target.checked);
  }

  onCloseCreateAudit(response: boolean) {
    if (response === true) {
      this.onUniverseChange({ data: { childrenId: this.currentChildIds, name: this.currentName } });
    }
    this.createAuditComponent = false;
  }

}