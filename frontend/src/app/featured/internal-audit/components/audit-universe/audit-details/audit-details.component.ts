import {Component,OnInit,SimpleChanges,ViewChild,Input} from "@angular/core";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConstUrls } from "src/app/config/const-urls";
import { HttpService } from "src/app/core/http-services/http.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: "app-audit-details",
  templateUrl: "./audit-details.component.html",
  styleUrls: ["./audit-details.component.less"],
})
export class AuditDetailsComponent implements OnInit {
  @Input("auditForm") auditForm: string;
  // @Output() closedRiskDefForm: EventEmitter<boolean> = new EventEmitter<
  //   boolean
  // >();
  @ViewChild("auditDetails", { static: false }) auditDetails: TabsetComponent;
  name: any;
  id: any;
  level: any;
  financialRisk: any;
  complianceRisk: any;
  overallImpact: any;
  processRisk: any;
  internalControls: any;
  overallLikelyhood: any;
  overallRiskScore: any;
  formData: any = [];
  nextAuditYear: any;
  rotation: any = 0;
  impactBtnColor: any;
  likliBtnColor: any;
  overallRiskScoreBtn: any;
  position;
  constructor(
    private httpService: HttpService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.financialRisk = 0;
    this.complianceRisk = 0;
    this.overallImpact = 0;
    this.processRisk = 0;
    this.internalControls = 0;
    this.overallLikelyhood = 0;
    this.overallRiskScore = 0;
    this.rotation = parseInt(this.auditForm["data"]["rotation"], 10);
    //console.log("rotation:", this.rotation);
    this.nextAuditYear = new Date().getFullYear() + this.rotation;
    //console.log("nextAuditYear:", this.nextAuditYear);
    this.fetchButtonColor();
    this.calculateRiskScore();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.name = this.auditForm["name"];
    this.id = this.auditForm["data"]["currentAudit"];
    this.level = this.auditForm["data"]["currentAudit"];
    if (this.auditForm["data"]["currentAudit"]) {
      //console.log(this.auditForm["riskLevel"]);
      let payload = { id: this.level };
      this.httpService
        .securePost(ConstUrls.getARAForm, payload)
        .subscribe(async (res) => {
          //console.log(res);
          this.formData = res;
          if (await this.formData) {
            this.financialRisk = this.formData.impact_financialRisk;
            this.complianceRisk = this.formData.impact_complianceRisk;
            this.overallImpact = parseInt(this.formData.impact_overall, 10);
            this.processRisk = this.formData.likelihood_processRisk;
            this.internalControls = this.formData.likelihood_internalControls;
            this.overallLikelyhood = parseInt(
              this.formData.likelihood_overall,
              10
            );
            this.overallRiskScore = parseInt(
              this.formData.overallRiskScore,
              10
            );
            this.rotation = parseInt(this.auditForm["data"]["rotation"], 10);
            //console.log("rotation:", this.rotation);
            this.nextAuditYear = new Date().getFullYear() + this.rotation;
            //console.log("nextAuditYear:", this.nextAuditYear);
            this.fetchButtonColor();
          }
          if (this.overallRiskScore) {
            this.fetchButtonColor();
            this.calculateRiskScore();
          }
        });
    }
  }
  fetchButtonColor() {
    if (this.overallImpact >= 0 && this.overallImpact <= 3) {
      this.impactBtnColor = "btn medium";
    } else if (this.overallImpact > 3 && this.overallImpact <= 6) {
      this.impactBtnColor = "btn low";
    } else {
      this.impactBtnColor = "btn high";
    }
    if (this.overallLikelyhood >= 0 && this.overallLikelyhood <= 3) {
      this.likliBtnColor = "btn medium";
    } else if (this.overallLikelyhood > 3 && this.overallLikelyhood <= 6) {
      this.likliBtnColor = "btn low";
    } else {
      this.likliBtnColor = "btn high";
    }
  }
  calculateRiskScore() {
    if (this.overallRiskScore <= 7) {
      this.overallRiskScoreBtn = "btn medium";
    } else if (this.overallRiskScore >= 8 && this.overallRiskScore <= 14) {
      this.overallRiskScoreBtn = "btn low";
    } else {
      this.overallRiskScoreBtn = "btn high";
    }
  }
  financeRiskBlur() {
    //console.log("ON BLur called");
  }
  onSearchChange(searchValue: number): void {
    if (isNaN(searchValue)) {
      //console.log(parseFloat("0.0"), "Its float number");
    }
    //console.log(searchValue);
    if (searchValue % 1 === 0) {
      //console.log(searchValue, "Its Integer number");
      /*console.log(
        "Converted in to float: ",
        parseFloat(searchValue.toString()).toFixed(1)
      ); */
    } else {
      /*console.log(
        parseFloat(searchValue.toString()).toFixed(1),
        "Its float number"
      );*/
    }
    //     let sum = 0.1 + 0.2;
    // alert( sum.toFixed(2) ); // 0.30
  }

  closeModal() {
    this.name = "";
    this.id = "";
    this.level = "";
    this.auditForm = "";
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.auditDetails.tabs[0].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
