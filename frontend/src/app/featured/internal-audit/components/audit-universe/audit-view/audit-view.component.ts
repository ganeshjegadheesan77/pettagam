import { Component, OnInit, OnChanges, NgZone, TemplateRef, Input } from "@angular/core";
import { HeatMap, Tooltip } from "@syncfusion/ej2-heatmap";
import { DataService } from "../data.service";
HeatMap.Inject(Tooltip);

import * as am4core from "@amcharts/amcharts4/core";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
import { GalaxyDataService } from "../galaxy-data.service";
am4core.useTheme(am4themes_animated);
import * as _ from "lodash";

@Component({
  selector: "app-audit-view",
  templateUrl: "./audit-view.component.html",
  styleUrls: ["./audit-view.component.less"],
})
export class AuditViewComponent implements OnInit {
  constructor(
    private data: DataService,
    private zone: NgZone,
    private galaxyDataService: GalaxyDataService,

  ) { }
  tableData = [];
  heatmapdata: any;
  yearData: any;
  state: any;
  dataDet: any;
  sitesVal: any;
  heading: any = "";
  showChart = false;
  @Input() auditMasters;

  chart: am4plugins_forceDirected.ForceDirectedTree;
  products: any = [];
  ngOnInit() {

    // this.ngOnChanges(this.data);
    // window.addEventListener("message", this.ngOnChanges.bind(this), false);

    this.galaxyDataService.currentmessage.subscribe((d) => {
      if (d.name && d.response.length > 0) {
        this.reloadChartData(d.response, d.name);
      } else if (d.name == "no_data") {
        this.showNoDataGalaxy();
      }
    });

    this.data.chartData.subscribe((d)=>{
      if(d && d.name){
        this.reloadHeatMaps(d);
      }
    })

    if (this.auditMasters && this.auditMasters.length > 0) {
      var parentData = this.auditMasters.filter(a => a.type == "Region");
      var groupedParents = _.groupBy(parentData, function (p) {
        return p.name
      });
      this.state = [];
      for (var key of Object.keys(groupedParents)) {
        this.state.push(key);
      }
    }
  }

  reloadHeatMaps(data){
    var year = new Date().getFullYear();
    this.yearData = [
      Number(year) + 1,
      Number(year) + 2,
      Number(year) + 3,
      Number(year) + 4,
      Number(year) + 5,
    ];
      this.state = data.states;  
    
    this.heatmapdata = data.auditable;
    if (this.heatmapdata.length == 0) this.heading = "Nickel";
    else this.heading = "Copper & Cobalt";

    setTimeout(() => {
      var heatmap: HeatMap = new HeatMap({
        titleSettings: {
          text: "Multi Year Plan for " + data.name,
          textStyle: {
            size: "15px",
            fontWeight: "500",
            fontStyle: "Normal",
            fontFamily: "Segoe UI",
          },
        },
        height: "200px",
        width: "100%",
        xAxis: {
          labels: this.yearData,
          opposedPosition: true,
        },
        yAxis: {
          labels: data.getSites,
          isInversed: true,
        },
        cellSettings: {
          showLabel: false,
          border: {
            color: "white",
          },
        },
        paletteSettings: {
          palette: [
            { value: 0, color: "#6c757d", label: "1 audits" },
            { value: 1, color: "#28a745", label: "2 audits" },
            { value: 2, color: "#105620", label: "3 audits" },
          ],
          type: "Fixed",
          emptyPointColor: "white",
        },
        dataSource: data.currentmessage,
      });
      var d = new Date();
      var heatmap1: HeatMap = new HeatMap({
        titleSettings: {
          text: `${d.getFullYear()} Audit Plan for ` + data.name,
          textStyle: {
            size: "15px",
            fontWeight: "500",
            fontStyle: "Normal",
            fontFamily: "Segoe UI",
          },
        },
        height: "200px",
        width: "100%",
        xAxis: {
          labels: this.state,
          //labelRotation:45,
          opposedPosition: true,
        },
        yAxis: {
          labels: data.auditable,
          isInversed: true,
        },
        cellSettings: {
          showLabel: false,
          border: {
            color: "white",
          },
        },
        paletteSettings: {
          palette: [
            // { value: 0, color: '#5895ff', label: '1 Audit' },
            // { value: 1, color: 'rgb(172, 213, 242)', label: '1 audits' },
            // { value: 2, color: 'rgb(178, 213, 242)', label: '2 audits' }
          ],
          type: "Fixed",
          emptyPointColor: "white",
        },
        // legendSettings: {
        //     position: 'Bottom',
        //     width: '40%',
        //     alignment: 'Near',
        //     showLabel: false,
        //     labelDisplayType: 'None',
        //     enableSmartLegend: false
        // },
        dataSource: data.auditData,
      });
      heatmap.appendTo("#heatmap");
      heatmap1.appendTo("#heatmap1");
    }, 0);
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.chart = am4core.create(
        "chartdiv",
        am4plugins_forceDirected.ForceDirectedTree
      );
      this.chart.logo.height = -100;
      // chart.logo.opacity = 0;
      let series = this.chart.series.push(
        new am4plugins_forceDirected.ForceDirectedSeries()
      );
      // ... chart code goes here ...
      series.data = [
        {
          name: "Please Select Audit",
          abbr: "No Data",
          x: am4core.percent(10),
          y: am4core.percent(50),
          fixed: true,
        },
      ];

      // Set up data fields
      series.dataFields.value = "value";
      series.dataFields.name = "name";
      series.dataFields.children = "children";
      series.dataFields.color = "color";
      series.dataFields.id = "abbr";
      // to make non draggable
      series.dataFields.fixed = "fixed";

      // to stop auto child expnd
      series.nodes.template.expandAll = false;
      // Add labels
      series.nodes.template.label.text = "{abbr}";
      series.nodes.template.tooltipText = "[bold]{name}[/]";

      // position
      series.nodes.template.propertyFields.x = "x";
      series.nodes.template.propertyFields.y = "y";

      series.fontSize = 13;
      series.minRadius = 35;
      series.maxRadius = 30;
      series.maxLevels = 2;

      // series.manyBodyStrength = -50;
      series.links.template.distance = 2;
      series.links.template.strength = 0.5;

      series.centerStrength = 0.5;
      // this.chart = chart;
    });
  }

  reloadChartData(d, name) {
    //#dd6838 red
    // #fc0 yellow
    // #98aa3a green
    this.chart.series.removeIndex(0).dispose();

    var series = this.chart.series.push(
      new am4plugins_forceDirected.ForceDirectedSeries()
    );
    var yheight = 0;
    var children = [];
    d.forEach((data) => {
      yheight += 40;
      var child = {};
      child["name"] = data.auditId;
      child["abbr"] = data.auditId;
      child["x"] = am4core.percent(35);
      child["y"] = am4core.percent(yheight);
      child["fixed"] = true;
      child["color"] =
        data.rating == "Good"
          ? "#98aa3a"
          : data.rating == "Some Improvement Required"
            ? "#fc0"
            : "#dd6838";

      if (data.issues && data.issues.length > 0) {
        child["children"] = [];

        data.issues.forEach((issue) => {
          if (issue && issue.issueId) {
            var ichild = {};
            ichild["name"] = issue.issueId;
            ichild["abbr"] = issue.issueId;
            ichild["color"] =
              issue.priority == "Low"
                ? "#98aa3a"
                : issue.priority == "Medium"
                  ? "#fc0"
                  : "#dd6838";
            child["children"].push(ichild);

            if (issue.actions && issue.actions.length > 0) {
              ichild["children"] = [];

              issue.actions.forEach((action) => {
                if (action && action.actionId) {
                  var achild = {};
                  achild["name"] = action.actionId;
                  achild["abbr"] = action.actionId;
                  achild["color"] =
                    action.rating == "Normal" ? "#fc0" : "#dd6838";
                  achild["value"] = 60;
                  ichild["children"].push(achild);
                }
              });
            } else {
              ichild["value"] = 60;
            }
          }
        });
      } else {
        child["value"] = 60;
      }

      children.push(child);
    });

    series.data = [
      {
        name: name,
        abbr: name.substr(0, 3) + "...",
        x: am4core.percent(10),
        y: am4core.percent(50),
        fixed: true,
        children: children,
        // children: [
        //   {
        //     name: "OP65",
        //     abbr: "OP65",
        //     color: "#dd6838",
        //     children: [
        //       {
        //         name: "I-65-55", abbr: "I-65-55", color: "#dd6838",
        //         children:
        //           [{ name: "A-102", abbr: "A-102", value: 60 },
        //           { name: "A-112", abbr: "A-112", color: "#fc0", value: 60 }]
        //       },
        //       {
        //         name: "I-65-56", abbr: "I-65-56", color: "#98aa3a",
        //         children:
        //           [{ name: "A-207", abbr: "A-207", color: "#dd6838", value: 60 }]
        //       },
        //     ]
        //   },
        //   {
        //     name: "OP101", abbr: "OP101", color: "#fc0", value: 60
        //   }

        // ]
      },
    ];
    // Set up data fields
    series.dataFields.value = "value";
    series.dataFields.name = "name";
    series.dataFields.children = "children";
    series.dataFields.color = "color";
    series.dataFields.id = "abbr";
    // to make non draggable
    series.dataFields.fixed = "fixed";

    // to stop auto child expnd
    series.nodes.template.expandAll = false;
    // Add labels
    series.nodes.template.label.text = "{abbr}";
    series.nodes.template.tooltipText = "[bold]{name}[/]";

    // position
    series.nodes.template.propertyFields.x = "x";
    series.nodes.template.propertyFields.y = "y";

    series.fontSize = 13;
    series.minRadius = 20;
    series.maxRadius = 25;
    series.maxLevels = 2;

    // series.manyBodyStrength = -5;
    series.links.template.distance = 2;
    series.links.template.strength = 2;

    // series.centerStrength = 0.5;
  }

  showNoDataGalaxy() {
    this.chart = am4core.create(
      "chartdiv",
      am4plugins_forceDirected.ForceDirectedTree
    );
    this.chart.logo.height = -100;
    // chart.logo.opacity = 0;
    let series = this.chart.series.push(
      new am4plugins_forceDirected.ForceDirectedSeries()
    );
    // ... chart code goes here ...
    series.data = [
      {
        name: "Please Select Audit",
        abbr: "No Data",
        x: am4core.percent(10),
        y: am4core.percent(50),
        fixed: true,
      },
    ];

    // Set up data fields
    series.dataFields.value = "value";
    series.dataFields.name = "name";
    series.dataFields.children = "children";
    series.dataFields.color = "color";
    series.dataFields.id = "abbr";
    // to make non draggable
    series.dataFields.fixed = "fixed";

    // to stop auto child expnd
    series.nodes.template.expandAll = false;
    // Add labels
    series.nodes.template.label.text = "{abbr}";
    series.nodes.template.tooltipText = "[bold]{name}[/]";

    // position
    series.nodes.template.propertyFields.x = "x";
    series.nodes.template.propertyFields.y = "y";

    series.fontSize = 13;
    series.minRadius = 35;
    series.maxRadius = 30;
    series.maxLevels = 2;

    // series.manyBodyStrength = -50;
    series.links.template.distance = 2;
    series.links.template.strength = 0.5;

    series.centerStrength = 0.5;
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
  showModal = false;


}