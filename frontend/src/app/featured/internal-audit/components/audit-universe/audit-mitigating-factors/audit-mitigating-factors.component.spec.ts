import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditMitigatingFactorsComponent } from './audit-mitigating-factors.component';

describe('AuditMitigatingFactorsComponent', () => {
  let component: AuditMitigatingFactorsComponent;
  let fixture: ComponentFixture<AuditMitigatingFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditMitigatingFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditMitigatingFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
