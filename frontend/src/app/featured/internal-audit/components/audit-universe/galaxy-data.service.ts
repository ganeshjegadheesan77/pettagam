import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class GalaxyDataService {
    private dataDetail = new BehaviorSubject<any>([]);
   
    currentmessage = this.dataDetail.asObservable();
  
    constructor() { }

    changeMessage(dataObject: any) {
        this.dataDetail.next(dataObject)
    }
}