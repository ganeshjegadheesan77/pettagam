import { Component, OnInit } from "@angular/core";
import { Title } from '@angular/platform-browser';

declare const drawUniverse: any;

@Component({
  selector: "app-audit-chart",
  templateUrl: "./audit-chart.component.html",
  styleUrls: ["./audit-chart.component.less"]
})
export class AuditChartComponent implements OnInit {
  constructor(private title:Title) { }
  
  ngOnInit() {
    drawUniverse();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
this.title.setTitle("EnIA: Audit Universe");      
    }, 2000);
  }
}
