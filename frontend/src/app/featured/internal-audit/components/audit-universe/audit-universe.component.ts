import { Component, OnInit } from '@angular/core';
import { ConstUrls } from 'src/app/config/const-urls';
import { HttpService } from 'src/app/core/http-services/http.service';

@Component({
  selector: 'app-audit-universe',
  templateUrl: './audit-universe.component.html',
  styleUrls: ['./audit-universe.component.less']
})
export class AuditUniverseComponent implements OnInit {

  constructor(private httpservice: HttpService) { }
  auditMasters = [];
  ngOnInit() {
    this.getAuditMasters();
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('audit-body-content');
  }

  getAuditMasters() {
    this.httpservice.secureGet(ConstUrls.getAuditMasters).subscribe(
      res => {
        this.auditMasters = res['data'];
        // this.currentName = this.auditMasters.find(a => a.id == "0.0").name;
        // this.getTableData(this.auditMasters.find(a => a.id == "0.0").childrenId);

      },
      err => {

      })
  }

  closeModal() {
    $('.pop-up-form').addClass('d-none');
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('audit-body-content');
   }
}