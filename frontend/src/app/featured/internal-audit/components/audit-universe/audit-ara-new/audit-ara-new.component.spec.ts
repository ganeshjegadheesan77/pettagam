import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditAraNewComponent } from './audit-ara-new.component';

describe('AuditAraNewComponent', () => {
  let component: AuditAraNewComponent;
  let fixture: ComponentFixture<AuditAraNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditAraNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditAraNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
