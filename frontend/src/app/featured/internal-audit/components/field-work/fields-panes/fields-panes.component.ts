import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as moment from "moment";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { MemoService } from "../memo.service";
import { ChatroomService } from "../../../../../layout/collaboration-elements/components/chat-system/services/chatroom.service";
import { ActivatedRoute } from "@angular/router";

declare var $;

@Component({
  selector: "app-fields-panes",
  templateUrl: "./fields-panes.component.html",
  styleUrls: ["./fields-panes.component.less"],
})
export class FieldsPanesComponent implements OnInit {
  mainFieldWorkForm: FormGroup;
  modalRef: BsModalRef;
  // assign for teamtimeline form added by Abu - 17-09-2020 start 
  public teamtimelineloadComponent = false;
// assign for teamtimeline form added by Abu - 17-09-2020 end 
  public auditloadComponent = false;
  public openloadComponent = false;
  public auditannouncementloadComponent = false;
  public pbcloadComponent = false;
  public recalculationloadComponent = false;
  public budgetloadComponent = false;
  public draftloadComponent = false;
  public cmloadComponent = false;
  public pbcNewloadComponent = false;
  public fwCtPopup = false;
  public budgetloadNewComponent = false;
  public recalculationNewloadComponent = false;
  public isDisplay = true;
  public loadAuditComponent = false;
  disableFieldWork=true;
  disableReporting=true;
  auditDropdown: any;
  copiedAuditData: any;
  fieldworkArr: any;
  auditList: any;
  auditData: { data: any };
  subprocesses: any;
  risks: any;
  selectedFieldWork: any;
  ObjId: any;
  selectedAuditId: string;
  selectAuditAnnounce: string;
  currSubprocess: any;
  selectedDevice: any;
  selectedDraftReport: string;
  issueArr: any;
  selectedIssue: any;
  PBCArr: any[];
  selectedPBC: any;
  selectedAudit: any;
  selectedNewAuditId: any;
  selecteddata: string;
  auditInfo: any;
  title: string;
  add: any;
  content: string;
  selectAuditId: string;
  commonArr: any[];
  // redirectAuditId;
  isCompletedStatusObj : any ;

  openingMeeting: Date;
  pNsOne: Date;
  pNsTwo: Date;
  per_periodOne: Date;
  per_periodTwo: Date;
  updateMeeting: Date;
  closingMeeting: Date;
  overlayLayout = true;
  disableTimeLine=false;
  sharedFlagObj : any;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private memo: MemoService,
    public chatroomService: ChatroomService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.memo.setAuditId('');
    this.memo.auditId.subscribe((auditId) => {
      if(auditId) {
        this.overlayLayout = false;
        this.selectAuditId = auditId;
        this.getEpFormCompleteStatus(auditId);
      } else {
        this.overlayLayout = true;
        this.selectAuditId = "";
        this.isCompletedStatusObj = {};
      }
    });
    this.memo.sharedFlagObj.subscribe((obj) => {
      this.sharedFlagObj = obj;
      if(this.sharedFlagObj.fieldwork){
        this.disableFieldWork = false;
      } else {
        this.disableFieldWork = true;
      }
    });
    
    // this.httpservice.secureGet(ConstUrls.getCountData).subscribe((res) => {
    //   var data = res;
    //   // this.memo.setValue(data[0].memo)
    //   // this.memo.setValueann(data[0].announced)
    //   // this.memo.setValueom(data[0].om)
    // });
    this.initForm();
    this.subprocesses = [];
    this.risks = [];
    localStorage.removeItem("auditData");
    localStorage.removeItem("auditList");
    localStorage.removeItem("selectedAuditId");
    this.getAudits();
  }
  getEpFormCompleteStatus(auditId) {
      let payload = { auditId };
      this.httpservice
        .secureGet(ConstUrls.getSelectedAuditInfo, payload)
        .subscribe(
          (res: any) => {
            this.auditInfo = res.data;
            this.isCompletedStatusObj = res.data.isCompleted;
          },
          (err: any) => {
            this.myCommonAlert('Error', err.error.message);
          }
        );
  }
  getAudits() {
    // this.httpservice.secureGet(ConstUrls.getAudits).subscribe((res: any) => {
    //   if (res) {
    //     this.auditDropdown = [];
    //     this.auditDropdown = res;

    //     // from email to fieldwork redirection
    //     if (this.redirectAuditId) {
    //       this.mainFieldWorkForm.patchValue({ audit_id: this.redirectAuditId });
    //       this.getAuditInfo(this.redirectAuditId);
    //     }
    //   } else {
    //     this.auditDropdown = "";
    //   }
    // });
  }

  // getAuditDetails(id) {
  //   if (id) {
  //     this.PBCArr = [];
  //     this.fieldworkArr = [];
  //     let payload = { auditId: id };
  //     if (localStorage.getItem("ID")) {
  //       localStorage.removeItem("ID");
  //       localStorage.setItem("ID", id);
  //     } else {
  //       localStorage.setItem("ID", id);
  //     }
  //     this.httpservice.securePost(ConstUrls.getAuditDetail, payload).subscribe((res: any) => {
  //       if (res) {
  //         this.auditInfo = res[0];
  //         this.mainFieldWorkForm.patchValue(res[0]);

  //         this.subprocesses = [];
  //         this.risks = [];
  //         this.PBCArr = [];
  //         this.fieldworkArr = [];
  //         this.commonArr = [];
  //         // this.subprocesses = [];
  //         // this.mainFieldWorkForm.patchValue(this.auditInfo);
  //         this.adjustDates(res[0]);

  //         if (res[0].auditPbc && res[0].auditPbc.length > 0) {
  //           res[0].auditPbc.forEach(element => {
  //             this.PBCArr.push(element);
  //           });
  //         }

  //           if (res[0].auditFieldWork && res[0].auditFieldWork.length > 0) {
  //             res[0].auditFieldWork.forEach((fw) => {
  //               this.fieldworkArr.push(fw);
  //             });
  //           }

  //           if (res[0].fieldWordData && res[0].fieldWordData.length > 0) {
  //             res[0].fieldWordData.forEach((fw) => {
  //               this.commonArr.push(fw);
  //             });
  //           }

  //           let newArray = this.commonArr.filter(
  //             (v, i, a) =>
  //               a.findIndex((t) => t.afw_subprocess === v.afw_subprocess) === i
  //           );
  //           newArray.forEach((element) => {
  //             this.subprocesses.push({
  //               afw_subprocess: element.afw_subprocess,
  //             });
  //           });
  //         }
  //       });
  //   }
  // }

  adjustDates(data) {
    let myArr = [];
    let per_period = [];
    if (data.periodInScope.length != 0) {
      if (!moment(data.periodInScope[0], "DD-MM-YYYY", true).isValid()) {
        myArr[0] = moment(data.periodInScope[0]).format("DD-MM-YYYY");
        myArr[1] = moment(data.periodInScope[1]).format("DD-MM-YYYY");
        this.mainFieldWorkForm.patchValue({ periodInScope: myArr });
      }
    }

    if (data.performancePeriod.length != 0) {
      if (!moment(data.performancePeriod[0], "DD-MM-YYYY", true).isValid()) {
        per_period[0] = moment(data.performancePeriod[0]).format("DD-MM-YYYY");
        per_period[1] = moment(data.performancePeriod[1]).format("DD-MM-YYYY");
        this.mainFieldWorkForm.patchValue({ performancePeriod: per_period });
      }
    }

    if (data.openingMeetingDate) {
      if (!moment(data.openingMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.openingMeetingDate).format("DD-MM-YYYY");

        this.mainFieldWorkForm.patchValue({ openingMeetingDate: op });
      }
    }

    if (data.updateMeetingDate) {
      if (!moment(data.updateMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.updateMeetingDate).format("DD-MM-YYYY");

        this.mainFieldWorkForm.patchValue({ updateMeetingDate: op });
      }
    }

    if (data.closingMeetingDate) {
      if (!moment(data.closingMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.closingMeetingDate).format("DD-MM-YYYY");

        this.mainFieldWorkForm.patchValue({ closingMeetingDate: op });
      }
    }
  }

  // getAuditInfo(id) {
  //   this.selectedNewAuditId = "";
  //   this.selectedNewAuditId = id;
  //   if (this.selectedNewAuditId != "") {
  //     this.mainFieldWorkForm.reset(this.mainFieldWorkForm.value);
  //     localStorage.removeItem("selectedAuditId");
  //     localStorage.setItem(
  //       "selectedAuditId",
  //       JSON.stringify(this.selectedNewAuditId)
  //     );
  //     this.getAuditDetails(this.selectedNewAuditId);

  //     // from email to fieldwork redirection
  //     if (this.redirectAuditId) {
  //       this.toggleDisplay();
  //       this.AuditTeam();
  //     }

  //     //get chatroom updated for the audit
  //     this.httpservice
  //       .securePost(ConstUrls.getRoomByAudit, { audit_id: id })
  //       .subscribe((r) => {
  //         if (r) {
  //           var roomData: any = r;
  //           this.chatroomService.setValue(roomData["_id"]);
  //         }
  //       });
  //   } else {
  //     this.mainFieldWorkForm.reset(this.mainFieldWorkForm.value);
  //     localStorage.removeItem("selectedAuditId");
  //   }
  // }

  getRpForm(event) {
    // console.log(event);
    if (event === true) {
      this.loadAuditComponent = false;
    }
  }

  changeSubprocess(subprocess_name) {
    if (subprocess_name) {
      this.fieldworkArr = [];
      this.risks = [];
      let result = this.commonArr.filter(
        (w) => w.afw_subprocess == subprocess_name
      );
      result.forEach((d) => {
        this.fieldworkArr.push(d);
        this.risks.push(d);
      });
    } else {
      this.fieldworkArr = [];
    }
  }
  changeRisk(riskName) {
    if (riskName) {
      this.fieldworkArr = [];
      let result = this.commonArr.filter((w) => w.afw_risk == riskName);
      result.forEach((d) => {
        this.fieldworkArr.push(d);
      });
    }
  }

  initForm() {
    this.mainFieldWorkForm = this.formBuilder.group({
      audit_id: ["", []],
      auditableEntity: ["", []],
      subprocess: ["", []],
      risk: ["", []],
      year: ["", []],
      template: ["", []],
      periodInScope: ["", []],
      performancePeriod: ["", []],
      updateMeetingDate: ["", []],
      openingMeetingDate: ["", []],
      closingMeetingDate: ["", []],
    });
  }

  toggleDisplay() {
    this.isDisplay = !this.isDisplay;
  }

  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design",
      backdrop: "static",
      keyboard: false,
    });
  }
  onUpdate() {
    if (!this.mainFieldWorkForm.get("audit_id").value) {
      this.title = "Error";
      this.content = "Please select Audit ...";
      this.myCommonAlert(this.title, this.content);
    } else {
      let myArr = [];
      let per_period = [];
      if (this.mainFieldWorkForm.get("periodInScope").value[0]) {
        let firstVal = moment(
          this.mainFieldWorkForm.get("periodInScope").value[0]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.periodInScope[0] === firstVal) {
          myArr[0] = new Date(this.auditInfo.periodInScope[0]);
        }
        let secondVal = moment(
          this.mainFieldWorkForm.get("periodInScope").value[1]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.periodInScope[1] === secondVal) {
          myArr[1] = new Date(this.auditInfo.periodInScope[1]);
        }
        if (myArr.length != 0) {
          this.mainFieldWorkForm.patchValue({ periodInScope: myArr });
        }
      }

      if (this.mainFieldWorkForm.get("performancePeriod").value[0]) {
        let firstVal = moment(
          this.mainFieldWorkForm.get("performancePeriod").value[0]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.periodInScope[0] === firstVal) {
          per_period[0] = new Date(this.auditInfo.periodInScope[0]);
        }
        let secondVal = moment(
          this.mainFieldWorkForm.get("performancePeriod").value[1]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.periodInScope[1] === secondVal) {
          per_period[1] = new Date(this.auditInfo.periodInScope[1]);
        }
        if (per_period.length != 0) {
          this.mainFieldWorkForm.patchValue({ performancePeriod: per_period });
        }
      }

      if (this.mainFieldWorkForm.get("openingMeetingDate").value) {
        let firstVal = moment(
          this.mainFieldWorkForm.get("openingMeetingDate").value[0]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.openingMeetingDate === firstVal) {
          let dt = new Date(this.auditInfo.openingMeetingDate);
          this.mainFieldWorkForm.patchValue({ openingMeetingDate: dt });
        }
      }

      if (this.mainFieldWorkForm.get("updateMeetingDate").value) {
        let firstVal = moment(
          this.mainFieldWorkForm.get("updateMeetingDate").value[0]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.updateMeetingDate === firstVal) {
          let dt = new Date(this.auditInfo.updateMeetingDate);
          this.mainFieldWorkForm.patchValue({ updateMeetingDate: dt });
        }
      }

      if (this.mainFieldWorkForm.get("closingMeetingDate").value) {
        let firstVal = moment(
          this.mainFieldWorkForm.get("closingMeetingDate").value[0]
        ).format("DD-MM-YYYY");
        if (this.auditInfo.closingMeetingDate === firstVal) {
          let dt = new Date(this.auditInfo.closingMeetingDate);
          this.mainFieldWorkForm.patchValue({ closingMeetingDate: dt });
        }
      }

      this.httpservice
        .securePost(
          ConstUrls.updateFieldWorkAudit,
          this.mainFieldWorkForm.value
        )
        .subscribe((res: any) => {
          if (res) {
            this.title = "Success";
            this.content = "Audit Updated Successfully ...";
            this.myCommonAlert(this.title, this.content);
          }
        });
    }
  }

  getMemoFormStatus(event) {
    if (event === true) {
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
      // this.selectAuditId = "";
      this.auditloadComponent = false;
      this.overlayLayout=false;
    }
  }

  getOpenMeetFormStatus(event) {
    if (event === true) {
      // this.selectAuditId = "";
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
      this.openloadComponent = false;
      this.overlayLayout=false;

    }
  }
  getIssueFormStatus(event) {
    if (event === true) {
      this.budgetloadNewComponent = false;
      this.overlayLayout=false;

    }
  }

  getPbcNewFormStatus(event) {
    if (event === true) {
      this.overlayLayout = false;
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
      this.pbcNewloadComponent = false;
      // this.getAuditDetails(this.selectedAuditId);
    }
  }

  getPBCFormStatus(event) {
    if (event === true) {
      this.pbcloadComponent = false;
    }
  }

  getCMFormStatus(event) {
    if (event === true) {
      // this.selectAuditId = "";
      this.issueArr = "";
      this.cmloadComponent = false;
      this.overlayLayout=false;
      this.sharedFlagObj.disableAuditDropDown = false;
    }
  }
  overlayClick(){
    if(!this.selectAuditId ){
      this.myCommonAlert("Error", "Please select audit first.");

    }
  }
  getAnnouncementFormStatus(event) {
    if (event === true) {
      // this.selectAuditId = "";
      this.auditannouncementloadComponent = false;
      this.overlayLayout=false;
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);

    }
  }

  getFWFormStatus(event) {
    if (event === true) {
      this.recalculationloadComponent = false;
      this.selectedFieldWork = "";
    }
  }

  getNFWFormStatus(event) {
    if (event === true) {
      this.recalculationNewloadComponent = false;
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
      // this.getAuditDetails(JSON.parse(localStorage.getItem("selectedAuditId")));
    }
  }

  getDRFormStatus(event) {
    if (event === true) {
      this.selectAuditId = "";
      this.draftloadComponent = false;
      this.getAudits();
      this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
    }
  }

  getTeamAndTimelineFormStatus(event) {
  
      if (event === true) {
        this.sharedFlagObj.disableAuditDropDown = false;
      this.memo.setsharedFlagObj(this.sharedFlagObj);
        // this.selectAuditId = "";
        this.teamtimelineloadComponent = false;
        this.overlayLayout = false;
     
         }
       }
// team and timeline form added by Abu - 17-09-2020 start

teamandtimeline() {
  this.overlayLayout = true;

  this.teamtimelineloadComponent = true;
  this.openloadComponent = false;
  this.auditloadComponent = false;
  this.auditannouncementloadComponent = false;
  this.pbcloadComponent = false;
  this.recalculationloadComponent = false;
  this.budgetloadComponent = false;
  this.draftloadComponent = false;
  this.cmloadComponent = false;
  this.pbcNewloadComponent = false;
  this.budgetloadNewComponent = false;
  this.recalculationNewloadComponent = false;
  this.fwCtPopup = false;
  this.loadAuditComponent = false;
  this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
  $('.pop-up-form').removeClass('d-none');
};

// team and timeline form added by Abu - 17-09-2020 end
  auditplanningmodle() {
    this.overlayLayout = true;
    // if (!localStorage.getItem("selectedAuditId")) {
    //   this.selectAuditId = "";
    //   this.title = "Error";
    //   this.content = "Please select Audit ...";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
    //   this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
    //   this.auditloadComponent = true;
    // }
    this.auditloadComponent = true;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    $(".pop-up-form").removeClass("d-none");
  }

  openmeetingmodle() {
    // if (!localStorage.getItem("selectedAuditId")) {
    //   this.selectAuditId = "";
    //   this.title = "Error";
    //   this.content = "Please select Audit ...";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
    //   this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
    //   this.openloadComponent = true;
    // }
    this.overlayLayout = true;

    this.openloadComponent = true;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    $(".pop-up-form").removeClass("d-none");
  }

  auditannouncementmodle() {
    // if (!localStorage.getItem("selectedAuditId")) {
    //   this.selectAuditId = "";
    //   this.title = "Error";
    //   this.content = "Please select Audit ...";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
    //   this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
    //   this.auditannouncementloadComponent = true;
    // }
    this.overlayLayout = true;

    this.auditannouncementloadComponent = true;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    $(".pop-up-form").removeClass("d-none");
  }

  pbcmodle(data) {
    this.selectedPBC = data;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    
    if (!this.selectAuditId) {
      // this.selectAuditId = "";
      this.title = "Error";
      this.content = "Please select Audit ...";
      this.myCommonAlert(this.title, this.content);
    } else {
      // this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
      this.pbcloadComponent = true;
    }
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }

  recalculationmodle(data) {
    
    this.selectedFieldWork = data;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    
    if (!this.selectAuditId) {
      // this.selectAuditId = "";
      this.title = "Error";
      this.content = "Please select Audit ...";
      this.myCommonAlert(this.title, this.content);
    } else {
      // this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
      this.recalculationloadComponent = true;
    }
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
  budgetsetupmodle(data) {
    this.selectedIssue = data;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = true;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
  draftmodle() {
    //console.log(JSON.parse(localStorage.getItem('auditData')))
    if (!this.selectAuditId) {
      // this.selectAuditId = "";
      this.title = "Error";
      this.content = "Please select Audit ...";
      this.myCommonAlert(this.title, this.content);
    } else {
      // this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
      this.draftloadComponent = true;
    }
    // selectedDraftReport
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }

  AuditTeam() {
    if (!this.selectedNewAuditId) {
      this.title = "Error";
      this.content = "Please select Audit";
      this.myCommonAlert(this.title, this.content);
    } else {
      this.selecteddata = this.selectedNewAuditId;
      this.loadAuditComponent = true;
    }
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    $(".pop-up-form").removeClass("d-none");
  }

  closingmeetingmodle() {
    // if (!localStorage.getItem("selectedAuditId")) {
    //   this.selectAuditId = "";
    //   this.title = "Error";
    //   this.content = "Please select Audit ...";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
    //   this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
      
    // }
    this.overlayLayout = true;

    this.cmloadComponent = true;
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    $(".pop-up-form").removeClass("d-none");
  }

  pbcNewmodle() {
    // if (!this.selectAuditId) {
    //   this.title = "Error";
    //   this.content = "Please select Audit";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
      this.pbcNewloadComponent = true;
      // this.selectedAuditId = this.selectedNewAuditId;
    // }
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    this.overlayLayout = true;
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    $(".pop-up-form").removeClass("d-none");
  }
  fieldworkNew() {
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    // if (!this.selectAuditId) {
    //   // this.selectAuditId = "";
    //   this.title = "Error";
    //   this.content = "Please select Audit ...";
    //   this.myCommonAlert(this.title, this.content);
    // } else {
      // this.selectAuditId = JSON.parse(localStorage.getItem("selectedAuditId"));
      this.recalculationNewloadComponent = true;
    // }
    this.sharedFlagObj.disableAuditDropDown = true
  this.memo.setsharedFlagObj(this.sharedFlagObj);
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
  budgetsetupmodleNew() {
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = true;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = false;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
  fieldWorkPopup() {
    // team and timeline form close added by Abu - 17-09-2020 start
    this.teamtimelineloadComponent = false;
    // team and timeline form close added by Abu - 17-09-2020 end
    this.auditloadComponent = false;
    this.openloadComponent = false;
    this.auditannouncementloadComponent = false;
    this.pbcloadComponent = false;
    this.recalculationloadComponent = false;
    this.budgetloadComponent = false;
    this.draftloadComponent = false;
    this.cmloadComponent = false;
    this.pbcNewloadComponent = false;
    this.budgetloadNewComponent = false;
    this.recalculationNewloadComponent = false;
    this.fwCtPopup = true;
    this.loadAuditComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
}