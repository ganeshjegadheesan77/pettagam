import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsPanesComponent } from './fields-panes.component';

describe('FieldsPanesComponent', () => {
  let component: FieldsPanesComponent;
  let fixture: ComponentFixture<FieldsPanesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldsPanesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldsPanesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
