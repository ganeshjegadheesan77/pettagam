import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { InterviewSchedulePopupComponent } from "src/app/layout/modal/interview-schedule-popup/interview-schedule-popup.component";
import { MemoService } from "../memo.service";
import * as moment from "moment";

import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
declare var $;

@Component({
  selector: "app-open-meet-popup",
  templateUrl: "./open-meet-popup.component.html",
  styleUrls: ["./open-meet-popup.component.less"],
})
export class OpenMeetPopupComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  @Output() closedOpenMeetForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  modalRef2;
  openMeetUpForm: FormGroup;
  modalRef: BsModalRef;
  globalSearchData = "";
  // OMisSet:Boolean;
  copiedAuditData: any;
  aomId: any;
  content: string;
  title: string;
  auditName: string;
  newMeetForm: boolean;
  auditId: any;
  interviewList: any;
  aomInterview: any;
  count: number;
  position;
  fieldPanes: any;
  mixArr: any;
  auditTeam: any[];
  auditeeTeam: any;
  openingMeetingDate: string;
  closingMeetingDate: string;
  periodInScope: string;
  arr: any[];
  dropdownListAll: any[];
  dropdownFirmList: any[];
  attachedFiles: any[] = [];
  companyName: any;
  location: any;
  buName: any;
  bProcessName: any;
  closeAttachment: any;
  marked: boolean;
  openAttachment: boolean;
  editsearchDocument: boolean = false;
  fetchedData: any;
  multiFile: any = [];
  dropdownSettings = {};
  dropdownList = [];
  disableSearch = false;
  selectedItems = [];
  isCompleted: boolean;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private epService: MemoService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) { }
  closeSearch() {
    this.editsearchDocument = false;
  }
  getSelectedDocs() {
    // this.searchDocument = false;
    if (!this.selectedItems.length) {
      this.myCommonAlert("Please select atleast a document.", "Alert");
      return;
    }
    var fileList = this.selectedItems.map((item) => {
      return item["fileName"];
    });
    console.log("Selected files: ", this.selectedItems);
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i]['metaData'] = `Module-Name=EngagementPlanning,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
      // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
      this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
      this.selectedItems[i]['userType'] = 'auditor';
      this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
      this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
      this.selectedItems[i]['roomType'] = '';
      this.selectedItems[i]['routerLink'] = '';
      this.selectedItems[i]['subModuleName'] = 'audit-planning-memorandum';
      this.selectedItems[i]['auditYear'] = new Date().getFullYear();
      this.selectedItems[i]['auditId'] = this.selectAudit;
      this.selectedItems[i]['auditName'] = 'name';
      this.selectedItems[i]['moduleName'] = 'EngagementPlanning';
      this.selectedItems[i]['checkedinDate'] = moment().format("L");

    }
    console.log('Selected Item ARRAY: ', this.selectedItems);
    let payloads = { items: this.selectedItems };
    this.httpservice.securePost(ConstUrls.DmsSaveAsDocument, payloads).subscribe((res: any) => {
      if (res) {
        console.log('res', res);
        for (let i = 0; i < res.length; i++) {
          console.log('filename', res[i].fileName)
          let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
          // res[i].fileName.substr(res[i].fileName.lastIndexOf('.') + 1)
          this.attachedFiles.push({
            documentName: res[i].fileName, documentNo: res[i].documentNo
            , documentUrlToken: res[i].urlToken, extension: extn
          });
          console.log('res[i].fileName', res[i].fileName)

        }
        console.log('attched filess array:', this.attachedFiles)
        this.myCommonAlert('File Uploaded successfully', 'Message');
      }
    }, (error) => {
      this.myCommonAlert(error.error.massage, 'Error');

    })
    // let payload = {
    //   auditId: this.auditId, username: localStorage.getItem("username"),
    //   userType: "auditor",
    //   fileList: fileList,
    // };
    // console.log("Payload: ", payload);

    // this.selectedItems.forEach((f,i) => {
    //   this.workstepForm.workpapers.push(
    //     { workpaperId: f.documentNo, workpaperName: f.fileName }
    //   )
    // })

    this.editsearchDocument = false;
  }
  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185", searchBy: localStorage.getItem('username') };

      this.httpservice
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              // console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            console.log('dropdownListAll', this.dropdownListAll)
            this.dropdownFirmList = this.arr;
            // this.attachedFiles = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    // this.dropdownListAll = [
    //   { item_id: 1, item_text: 'Mumbaisssssssssss' },
    //   { item_id: 2, item_text: 'Bangaluru' },
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' },
    //   { item_id: 5, item_text: 'New Delhi' },
    //   { item_id: 6, item_text: 'Mumbai' },
    //   { item_id: 7, item_text: 'Bangaluru' },
    //   { item_id: 8, item_text: 'Pune' },
    //   { item_id: 9, item_text: 'Navsari' },
    //   { item_id: 10, item_text: 'New Delhi' },
    //   { item_id: 11, item_text: 'Mumbai' },
    //   { item_id: 12, item_text: 'Bangaluru' },
    //   { item_id: 13, item_text: 'Pune' },
    //   { item_id: 14, item_text: 'Navsari' },
    //   { item_id: 15, item_text: 'New Delhi' }
    // ];
  }
  addnewFile() {
    this.editsearchDocument = true;
  }
  removeAttachment(removeitem, index) {
    const fileindex = parseInt(index);
    this.attachedFiles.splice(fileindex, 1);
    console.log('@@@@@@---', this.attachedFiles);
    this.myCommonAlert("File removed successfully", "Success");
  }
  // removeAttachment(removeitem, index) {
  //   this.closeAttachment = false;
  //   if (removeitem.new == true) {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
  //     if (idx != -1) this.multiFile.splice(idx, 1);
  //   } else {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     console.log("this.attachedFiles", this.attachedFiles);
  //     let payload = {
  //       directory: "uploads/fieldWork",
  //       fullName: removeitem.fullName,
  //     };
  //     this.httpservice
  //       .securePost("removeAttachment", payload)
  //       .subscribe((response: any) => {
  //         if (response) {
  //           if (response) {
  //             this.fetchedData.openingMeeting.apm_attachments = [];
  //             this.attachedFiles.forEach((ele) => {
  //               this.fetchedData.openingMeeting.apm_attachments.push(
  //                 ele.fullName
  //               );
  //             });

  //             this.title = "Success";
  //             (this.content = "Attachment Deleted Successfully"),
  //               this.myCommonAlert(this.content, this.title );
  //           }
  //         }
  //       });
  //   }
  // }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.epService.auditId.subscribe((auditId) => {
      this.auditId = auditId;
      this.getOpeningMeeting(this.auditId);
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };
    this.initForm();

    // this.getMeetingData();
    // this.memo.getValueom().subscribe((data) => {
    //   this.count = data;
    // });
    // this.httpservice
    //   .secureGet(ConstUrls.getAllInterview)
    //   .subscribe(async (res) => {
    //     if (res) {
    //       this.interviewList = res;

    //       setTimeout(() => {
    //         this.dataTable();
    //       }, 500);
    //     }
    //   });
  }

  getOpeningMeeting(auditId) {
    if (auditId) {
      this.selectAudit = auditId;
      let payload = { auditId: this.selectAudit };
      this.httpservice.securePost(ConstUrls.getOpeningMeetingData, payload).subscribe((res: any) => {
        if (res) {
          console.log('**** RESPONSE:', res)
          let openmeetUpData = res.data[0];
          this.companyName = openmeetUpData.category.levelOne.data;
          this.location = openmeetUpData.category.levelTwo.data;
          this.buName = openmeetUpData.category.levelThree.data;
          this.bProcessName = openmeetUpData.category.businessProcess;
          this.isCompleted = openmeetUpData.openingMeeting.isCompleted;
          this.openMeetUpForm.patchValue(openmeetUpData.openingMeeting);
          if (openmeetUpData.openingMeeting.attachments.length > 0) {
            this.openAttachment = true;
            for (let i = 0; i < openmeetUpData.openingMeeting.attachments.length; i++) {
              console.log('filename: ', openmeetUpData.openingMeeting.attachments[i].documentName);
              let extn = openmeetUpData.openingMeeting.attachments[i].documentName.substr(openmeetUpData.openingMeeting.attachments[i].documentName.lastIndexOf(".") + 1);
              this.attachedFiles.push({
                documentName: openmeetUpData.openingMeeting.attachments[i].documentName,
                documentNo: openmeetUpData.openingMeeting.attachments[i].documentNo
                , documentUrlToken: openmeetUpData.openingMeeting.attachments[i].documentUrlToken,
                extension: extn
              });
            }

            console.log('saved file array', this.attachedFiles)
            // this.attachedFiles = openmeetUpData.openingMeeting.attachments;
          } else {
            this.attachedFiles = [];
            this.openAttachment = false;

          }
          if (openmeetUpData.teamAndTimeline.openingMeetingDate) {
            this.openingMeetingDate = moment(openmeetUpData.teamAndTimeline.openingMeetingDate).format("DD-MM-YYYY");
          } else {
            this.openingMeetingDate = '';
          }
          if (openmeetUpData.teamAndTimeline.closingMeetingDate) {
            this.closingMeetingDate = moment(openmeetUpData.teamAndTimeline.closingMeetingDate).format("DD-MM-YYYY");
          } else {
            this.closingMeetingDate = '';
          }
          if (openmeetUpData.teamAndTimeline.periodInScope && openmeetUpData.teamAndTimeline.periodInScope.length) {
            this.periodInScope = moment(openmeetUpData.teamAndTimeline.periodInScope[0]).format("DD-MM-YYYY") + '-' + moment(openmeetUpData.teamAndTimeline.periodInScope[1]).format("DD-MM-YYYY");
          } else {
            this.periodInScope = '';
          }
          if (typeof openmeetUpData.teamAndTimeline.auditees != undefined) {
            this.auditeeTeam = openmeetUpData.teamAndTimeline.auditees;
            console.log('auditeeTeam', this.auditeeTeam)
          }
          if (openmeetUpData.teamAndTimeline.auditLead || openmeetUpData.teamAndTimeline.auditors) {
            this.auditTeam = [...openmeetUpData.teamAndTimeline.auditLead, ...openmeetUpData.teamAndTimeline.auditors];
            console.log('auditTeam', this.auditTeam)
          }

        }
      }, (error) => {
        this.myCommonAlert(error.error.message, 'Error')
      })
    }

  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      // attachment left

      this.openAttachment = true;
    }
  }
  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let fileNameArr = fileInput.split(".");
    let extension = '.' + fileNameArr[fileNameArr.length - 1];
    console.log('extension@@@', extension);
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }
  myCommonAlert(content, title, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }
  // getMeetingData() {
  //   let payload = { auditId: this.selectAudit };
  //   this.httpservice
  //     .securePost(ConstUrls.getOpenMeetUpData, payload)
  //     .subscribe((res: any) => {
  //       console.log("payload", payload);
  //       if (res) {
  //         console.log("res", res);
  //         if (res[0].openingMeeting.OMisSet === true) {
  //           this.fieldPanes = res[0];
  //           if (
  //             !moment(
  //               this.fieldPanes.periodInScope[0],
  //               "DD-MM-YYYY",
  //               true
  //             ).isValid() &&
  //             this.fieldPanes.periodInScope.length > 0
  //           ) {
  //             let d1 = moment(this.fieldPanes.periodInScope[0]).format(
  //               "DD-MM-YYYY"
  //             );
  //             let d2 = moment(this.fieldPanes.periodInScope[1]).format(
  //               "DD-MM-YYYY"
  //             );
  //             let str = d1 + "-" + d2;
  //             this.openMeetUpForm.patchValue({ period_in_scope: str });
  //           } else if (this.fieldPanes.periodInScope.length == 0) {
  //             this.openMeetUpForm.patchValue({
  //               period_in_scope: this.fieldPanes.periodInScope,
  //             });
  //           } else {
  //             let d1 = this.fieldPanes.periodInScope[0];
  //             let d2 = this.fieldPanes.periodInScope[1];
  //             let str = d1 + "-" + d2;
  //             this.openMeetUpForm.patchValue({ period_in_scope: str });
  //           }

  //           if (
  //             !moment(
  //               this.fieldPanes.performancePeriod[0],
  //               "DD-MM-YYYY",
  //               true
  //             ).isValid() &&
  //             this.fieldPanes.performancePeriod.length > 0
  //           ) {
  //             let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
  //               "DD-MM-YYYY"
  //             );
  //             let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
  //               "DD-MM-YYYY"
  //             );
  //             let str = d1 + "-" + d2;
  //             this.openMeetUpForm.patchValue({ performance_period: str });
  //           } else if (this.fieldPanes.performancePeriod.length == 0) {
  //             this.openMeetUpForm.patchValue({
  //               performance_period: this.fieldPanes.performancePeriod,
  //             });
  //           } else {
  //             let d1 = this.fieldPanes.performancePeriod[0];
  //             let d2 = this.fieldPanes.performancePeriod[1];
  //             let str = d1 + "-" + d2;
  //             this.openMeetUpForm.patchValue({ performance_period: str });
  //           }

  //           if (
  //             !moment(
  //               this.fieldPanes.openingMeetingDate,
  //               "DD-MM-YYYY",
  //               true
  //             ).isValid() &&
  //             this.fieldPanes.openingMeetingDate
  //           ) {
  //             let d1 = moment(this.fieldPanes.openingMeetingDate).format(
  //               "DD-MM-YYYY"
  //             );
  //             this.openMeetUpForm.patchValue({ opening_meeting: d1 });
  //           } else if (!this.fieldPanes.openingMeetingDate) {
  //             this.openMeetUpForm.patchValue({
  //               opening_meeting: this.fieldPanes.openingMeetingDate,
  //             });
  //           } else {
  //             let d1 = this.fieldPanes.openingMeetingDate;
  //             this.openMeetUpForm.patchValue({ opening_meeting: d1 });
  //           }

  //           if (
  //             !moment(
  //               this.fieldPanes.closingMeetingDate,
  //               "DD-MM-YYYY",
  //               true
  //             ).isValid() &&
  //             this.fieldPanes.closingMeetingDate
  //           ) {
  //             let d1 = moment(this.fieldPanes.closingMeetingDate).format(
  //               "DD-MM-YYYY"
  //             );
  //             this.openMeetUpForm.patchValue({ closing_meeting: d1 });
  //           } else if (!this.fieldPanes.closingMeetingDate) {
  //             this.openMeetUpForm.patchValue({
  //               closing_meeting: this.fieldPanes.closingMeetingDate,
  //             });
  //           } else {
  //             let d1 = this.fieldPanes.closingMeetingDate;
  //             this.openMeetUpForm.patchValue({ closing_meeting: d1 });
  //           }

  //           this.auditName = this.fieldPanes.auditableEntity;

  //           this.aomId = res[0].openingMeeting.aom_id;
  //           this.openMeetUpForm.patchValue(res[0].openingMeeting);
  //           // this.openMeetUpForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));

  //           //Audit team code
  //           this.auditTeam = [];
  //           this.mixArr = res[0].resourePlanningData;
  //           console.log("mixArr", this.mixArr);
  //           let newArr = [];
  //           if (this.mixArr[0]) {
  //             this.mixArr[0].pAssignee.forEach((ele) => {
  //               newArr.push(ele);
  //             });
  //             this.mixArr[0].papAssignee.forEach((ele) => {
  //               newArr.push(ele);
  //             });
  //             let newArray = newArr.filter(
  //               (v, i, a) => a.findIndex((t) => t === v) === i
  //             );
  //             newArray.forEach((element) => {
  //               this.auditTeam.push(element);
  //             });
  //           }
  //         } else {
  //           this.title = "Error";
  //           this.content = "Not Allowed ...";
  //           this.myCommonAlert(this.title, this.content);
  //           this.closeModal();
  //         }
  //       }
  //     });
  // }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    // this.upload();

    if (this.selectAudit) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      let capitalizedcurrentUser = currentUser.charAt(0).toUpperCase() + currentUser.slice(1);
      if (!this.isCompleted) {
        this.openMeetUpForm.patchValue({
          preparedBy: capitalizedcurrentUser
        });
      }
      this.openMeetUpForm.patchValue({
        auditId: this.selectAudit,
        isCompleted: true,
        attachments: this.attachedFiles
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveOpeningMeetingData, this.openMeetUpForm.value)
            .subscribe((res: any) => {
              if (res) {
                this.myCommonAlert(res.message, 'Message', 'success');
                this.epService.refresh(true);
                this.closeModal(false);
              }
            }, (error) => {
              this.myCommonAlert(error.error.message, 'Error');
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.content, this.title);
    }


  }

  // onSave() {
  //   if (this.selectAudit) {
  //     console.log("selectAudit", this.selectAudit);
  //     let currentUser = localStorage.getItem("username");
  //     this.openMeetUpForm.patchValue({
  //       auditId: this.selectAudit,
  //       aom_created_by: currentUser,
  //       aom_id: this.aomId,
  //     });
  //     console.log("check");
  //     this.httpservice
  //       .securePost(ConstUrls.saveOpenMeetUptData, this.openMeetUpForm.value)
  //       .subscribe((res: any) => {
  //         if (res) {
  //           console.log("res", res);
  //           this.title = "Success";
  //           this.content = "OpeningMeeting updated successfully...";
  //           this.myCommonAlert(this.title, this.content);
  //           var payload = { openingMeeting: this.count + 1 };
  //           this.httpservice
  //             .securePost(ConstUrls.updateCountData, payload)
  //             .subscribe((r) => {
  //               if (r) {
  //                 this.count = this.count + 1;
  //                 this.memo.setValueann(this.count);
  //               }
  //             });
  //         }
  //       });
  //   } else {
  //     this.title = "Error";
  //     this.content = "Failed to Save...";
  //     this.myCommonAlert(this.title, this.content);
  //   }

  //   this.closeModal();
  // }

  initForm() {
    this.openMeetUpForm = this.formBuilder.group({
      auditId: ["", []],
      auditScope: ["", []],
      agenda: ["", []],
      quesAndAns: ["", []],
      sentOn: ["", []],
      preparedBy: ["", []],
      sentBy: ["", []],
      isCompleted: ["", []],
      attachments: ["", []]

    });
  }
  onFilechanged(event) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      for (let i = 0; i < this.attachedFiles.length; i++) {
        if (this.attachedFiles[i]['documentName'] == files[0]["name"]) {
          this.myCommonAlert('File already exist...', 'Error');
          return
        }
      }
      console.log("fil anme", files);
      formData.append("file", files[0], files[0]["name"]);
      formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
      formData.append("userType", 'auditor');
      formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=EngagementPlanning,Feature-Name=openingMeeting,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
      formData.append("routerLink", ""),
        formData.append("subModuleName", "opening-meeting"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.selectAudit),
        formData.append("auditName", "name"),
        formData.append("moduleName", "EngagementPlanning"),
        formData.append("checkedinDate", moment().format("L"));
      console.log('****** formData', formData);
      this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            console.log('filename', res[i].fileName)
            let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            this.attachedFiles.push({
              documentName: res[i].fileName, documentNo: res[i].documentNo
              , documentUrlToken: res[i].urlToken, extension: extn
            });
            console.log('res[i].fileName', res[i].fileName)

          }
          console.log('attched filess array:', this.attachedFiles)
          console.log('******res', res)
          this.myCommonAlert('File uploaded successfully', 'Success', 'success');

        }
      }
        , (error) => {
          this.myCommonAlert(error.error.message, 'Error');

        })



    } else {
      this.myCommonAlert('Invalid file', 'Error');
      $("#myFile").val('');
    }
  }

  dataTable() {
    $("#interveiwSchduleTable").DataTable({
      processing: true,
      bPaginate: true,
      autoWidth: true,
      searching: false,
      destroy: true,
      fixedColumns: {
        leftColumns: 0,
        rightColumns: 1,
        heightMatch: "auto",
      },
      columnDefs: [{ targets: 6, orderable: false }],
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"],
      ],
      bInfo: true,
    });
  }

  closeModal(popupFlag) {
    if (popupFlag) {
      const initialState = {
        title: "Confirmation",
        content: `Do you want to close current form ?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef2 = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef2.content.onClose.subscribe((r) => {
        if (r) {
          this.closedOpenMeetForm.emit(true);
          $(".scroll-container").scrollTop(0);
          $(".pop-up-form").addClass("d-none");
          this.position = this.position ? undefined : { x: 0, y: 0 };
        }
      })
    } else {
      this.closedOpenMeetForm.emit(true);
      $(".scroll-container").scrollTop(0);
      $(".pop-up-form").addClass("d-none");
      this.position = this.position ? undefined : { x: 0, y: 0 };
    }

  }
  editTable(a, i) {
    $("body").addClass("modalPopupOne");
    const initialState = {
      title: "Interview Schedule",
      link: "Submit",
      action: "Cancel",
      data: a,
    };
    this.modalRef = this.modalService.show(InterviewSchedulePopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.interviewList[i] = r;
      }
    });
  }

  addStep() {
    $("body").addClass("modalPopupOne");
    const initialState = {
      title: "Interview Schedule",
      link: "Submit",
      action: "Cancel",
    };
    this.modalRef = this.modalService.show(InterviewSchedulePopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.interviewList.push(r);
      }
    });
  }
  // print click added by Abu - 16-09-2020 start
  print() {
    window.print();
  }
  // print click added by Abu - 16-09-2020 end
}