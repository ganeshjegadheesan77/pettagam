import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenMeetPopupComponent } from './open-meet-popup.component';

describe('OpenMeetPopupComponent', () => {
  let component: OpenMeetPopupComponent;
  let fixture: ComponentFixture<OpenMeetPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenMeetPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenMeetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
