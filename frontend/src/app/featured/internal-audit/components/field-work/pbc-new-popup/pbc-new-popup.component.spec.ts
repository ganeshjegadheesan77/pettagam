import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PbcNewPopupComponent } from './pbc-new-popup.component';

describe('PbcNewPopupComponent', () => {
  let component: PbcNewPopupComponent;
  let fixture: ComponentFixture<PbcNewPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PbcNewPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbcNewPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
