import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftPopupComponent } from './draft-popup.component';

describe('DraftPopupComponent', () => {
  let component: DraftPopupComponent;
  let fixture: ComponentFixture<DraftPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
