import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { MemoService } from "../memo.service";
// import { environment } from "src/environments/environment";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConfigService } from 'src/config.service';
declare var $;

@Component({
  selector: "app-draft-popup",
  templateUrl: "./draft-popup.component.html",
  styleUrls: ["./draft-popup.component.less"],
})
export class DraftPopupComponent implements OnInit, OnChanges {
  @Input("selectAudit") selectAudit: string;
  @Output() closedDRForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  draftForm: FormGroup;
  modalRef: BsModalRef;
  position;
  uri;
  fetchedData: any;
  fieldPanes: any;
  uploadedFilesArr: any;
  area: any;
  multiFile: any = [];
  actionRatingMaster: any;
  assignedIssue: any;
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  deletedAttachment: any = [];
  adrId: any;
  auditName: string;
  draftName: any;
  auditDraftName: any;
  newDraftForm: boolean;
  auditId: any;
  division: any;
  updateCount: any;
  count: number;
  content: string;
  title: string;
  departments: any;
  leads: any;
  processes: any;
  closeAttachment: boolean;
  mixArr: any;
  auditTeam: any[];
  // assign search  added by Abu - 18-09-2020 start
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {}; 
  editsearchDocument: boolean = false;
  // assign search  added by Abu - 18-09-2020 end
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private memo: MemoService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService

  ) {
    this.uri = configService.config.baseUrl;
  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.memo.getValueann().subscribe((data) => {
      this.count = data;
    });
    // this.isAttachment=false;
    // this.openAttachment = true;
    this.adrId = "";
    // this.genID();
    this.fetchedData = [];

    this.getMasters();
    this.title = "";
    this.content = "";
    this.initForm();
    this.getDraftFormData();
    // this.draftForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
    this.memo.getValueann().subscribe((data) => {
      this.count = data;
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };
  }
  ngOnChanges(changes: SimpleChanges) {}

  getMasters() {
    this.httpservice
      .secureGet(ConstUrls.getMasterDivision)
      .subscribe((res: any) => {
        if (res) {
          this.division = res;
          //console.log('Divisions:' ,this.division)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterDepartment)
      .subscribe((res: any) => {
        if (res) {
          this.departments = res;
          //console.log('departments:' ,this.departments)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterLeadContact)
      .subscribe((res: any) => {
        if (res) {
          this.leads = res;
          //console.log('leads:' ,this.leads)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterProcess)
      .subscribe((res: any) => {
        if (res) {
          this.processes = res;
          //console.log('processes:' ,this.processes)
        }
      });
  }

  // this.genID();
  // this.initForm();
  // this.getDraftFormData();

  myCommonAlert(content, title) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  getDraftFormData() {
    this.attachedFiles = [];

    let payload = { auditId: this.selectAudit };
    this.httpservice
      .securePost(ConstUrls.getDraftRepoData, payload)
      .subscribe(async (res: any) => {
        this.fetchedData = res[0];

        this.fieldPanes = await res[0];
        console.log("response", this.fieldPanes);
        this.initForm();
        this.draftForm.patchValue(res);
        if (
          !moment(
            this.fieldPanes.periodInScope[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.periodInScope.length > 0
        ) {
          let d1 = moment(this.fieldPanes.periodInScope[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.periodInScope[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.draftForm.patchValue({ period_in_scope: str });
        } else if (this.fieldPanes.periodInScope.length == 0) {
          this.draftForm.patchValue({
            period_in_scope: this.fieldPanes.periodInScope,
          });
        } else {
          let d1 = this.fieldPanes.periodInScope[0];
          let d2 = this.fieldPanes.periodInScope[1];
          let str = d1 + "-" + d2;
          this.draftForm.patchValue({ period_in_scope: str });
        }

        if (
          !moment(
            this.fieldPanes.performancePeriod[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.performancePeriod.length > 0
        ) {
          let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.draftForm.patchValue({ performance_period: str });
        } else if (this.fieldPanes.performancePeriod.length == 0) {
          this.draftForm.patchValue({
            performance_period: this.fieldPanes.performancePeriod,
          });
        } else {
          let d1 = this.fieldPanes.performancePeriod[0];
          let d2 = this.fieldPanes.performancePeriod[1];
          let str = d1 + "-" + d2;
          this.draftForm.patchValue({ performance_period: str });
        }

        if (
          !moment(
            this.fieldPanes.openingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.openingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.openingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.draftForm.patchValue({ opening_meeting: d1 });
        } else if (!this.fieldPanes.openingMeetingDate) {
          this.draftForm.patchValue({
            opening_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.openingMeetingDate;
          this.draftForm.patchValue({ opening_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.closingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.closingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.closingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.draftForm.patchValue({ closing_meeting: d1 });
        } else if (!this.fieldPanes.closingMeetingDate) {
          this.draftForm.patchValue({
            closing_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.closingMeetingDate;
          this.draftForm.patchValue({ closing_meeting: d1 });
        }

        this.auditName = this.fieldPanes.auditableEntity;
        if (this.fetchedData.draftReport.adr_attachments.length != 0) {
          this.openAttachment = true;
          for (
            let i = 0;
            i < this.fetchedData.draftReport.adr_attachments.length;
            i++
          ) {
            let extn = this.fetchedData.draftReport.adr_attachments[i].substr(
              this.fetchedData.draftReport.adr_attachments[i].lastIndexOf(".") +
                1
            );
            let name = this.fetchedData.draftReport.adr_attachments[i].substr(
              0,
              this.fetchedData.draftReport.adr_attachments[i].lastIndexOf(".")
            );

            this.attachedFiles.push({
              relativePath: name,
              extension: extn,
              fullName: name + "." + extn,
              new: false,
              isDuplicate: this.fieldPanes.draftReport.dup_adr_Flag,
            });
          }
        } else {
          this.openAttachment = false;
        }
        // if(this.fieldPanes.draftReport.adr_attachments){

        //   for(let i=0;i<this.fieldPanes.draftReport.adr_attachments.length;i++){
        //     this.attachedFiles.push({'relativePath':this.fieldPanes.draftReport.adr_attachments[i].split('.')[0],
        //   'extension':this.fieldPanes.draftReport.adr_attachments[i].split('.')[1],'fullName':this.fieldPanes.draftReport.adr_attachments[i]

        //   })
        //   }
        // }
        console.log("payload", payload);
        if (res) {
          console.log("res", res);
          if (res[0].draftReport.DRisSet === true) {
            this.adrId = res[0].draftReport.adr_id;
            this.draftForm.patchValue(res[0].draftReport);
            //Audit team code
            this.auditTeam = [];
            this.mixArr = res[0].resourePlanningData;
            console.log("mixArr", this.mixArr);
            let newArr = [];
            if (this.mixArr[0]) {
              this.mixArr[0].rAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              this.mixArr[0].rapAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              let newArray = newArr.filter(
                (v, i, a) => a.findIndex((t) => t === v) === i
              );
              newArray.forEach((element) => {
                this.auditTeam.push(element);
              });
            }
          } else {
            this.title = "Error";
            this.content = "Not Allowed ...";
            this.myCommonAlert(this.title, this.content);
            this.closeModal();
          }
        }
      });
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      this.openAttachment = true;
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  removeAttachment(removeitem, index) {
    this.closeAttachment = false;
    if (removeitem.new == true) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
      if (idx != -1) this.multiFile.splice(idx, 1);
    } else {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      console.log("this.attachedFiles", this.attachedFiles);
      let payload = {
        directory: "uploads/fieldWork",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            this.fieldPanes.draftReport.adr_attachments = [];
            this.attachedFiles.forEach((ele) => {
              this.fieldPanes.draftReport.adr_attachments.push(ele.fullName);
            });
            this.title = "Success";
            this.content = "Attachment Deleted Successfully";
            this.myCommonAlert(this.title, this.content);
          }
        });
    }
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.draftReport.adr_attachments) {
        this.fetchedData.draftReport.adr_attachments.push(data[i]["name"]);
      } else {
        this.fetchedData.draftReport.adr_attachments = [];
        this.fetchedData.draftReport.adr_attachments.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.draftReport.adr_attachments;
    this.draftForm.patchValue({ adr_attachments: arr });

    console.log("filearray", arr.toString());
    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "draft-popup",
      tab: "draft-popup",
      "router-link": "/rcm/internal-audit/engagement-planning",

      file: filearr.toString(),
      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: this.configService.config.db,
    };

    console.log("check", payload);
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        if (files) {
          console.log("Response", files);
        }
        this.multiFile = [];
      });

    if (formData) {
      this.httpservice
        .securePost("uploadFieldWork", formData)
        .subscribe((files) => {
          this.multiFile = [];
        });
    }
  }

  getDocument(file) {
    console.log("file", file);

    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
    let payload = {
      database: this.configService.config.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        if (files) {
          console.log("draft Response", files);
        }
        this.multiFile = [];
      });
  }
  public fileOver(event) {}

  public fileLeave(event) {}

  initForm() {
    this.draftForm = this.formBuilder.group({
      _id: ["", []],
      auditName: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      adr_id: ["", []],
      adr_name: ["", []],
      adr_division: ["", []],
      adr_gen_observation: ["", []],
      adr_audit_objective: ["", []],
      adr_lead_contact: ["", []],
      adr_mgmt_feedback: ["", []],
      adr_department: ["", []],
      adr_process: ["", []],
      adr_rating: ["", []],
      auditId: ["", []],
      adr_corrective_action: ["", []],
      adr_audit_team: ["", []],
      adr_comments: ["", []],
      adr_attachments: ["", []],
      adr_created_by: ["", []],
      adr_updated_by: ["", []],
      DRisSet: [{ value: true }, []],
      dup_adr_Flag: ["", []],
    });
  }

  submitDraft() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();

    if (this.selectAudit) {
      console.log("selectAudit", this.selectAudit);

      let currentUser = localStorage.getItem("username");
      this.draftForm.patchValue({
        auditId: this.selectAudit,
        adr_created_by: currentUser,
        adr_id: this.adrId,
        dup_adr_Flag: false,
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveDraftRepoData, this.draftForm.value)
            .subscribe((res: any) => {
              if (res) {
                console.log("res", res);
                let payloadnew = {
                  parent: res.parentUId,
                  id: res.uID,
                  auditId: res.auditId,
                };
                this.httpservice
                  .securePost(ConstUrls.updateUniverseData, payloadnew)
                  .subscribe((result: any) => {
                    if (result) {
                      this.title = "Success";
                      this.content = "DraftReport updated successfully...";
                      this.myCommonAlert(this.title, this.content);
                      var payload = { draftReport: this.count + 1 };
                      this.httpservice
                        .securePost(ConstUrls.updateCountData, payload)
                        .subscribe((r) => {
                          if (r) {
                            this.count = this.count + 1;
                            this.memo.setValueann(this.count);
                          }
                        });
                    }
                  });
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();

    if (this.selectAudit) {
      console.log("selectAudit", this.selectAudit);

      let currentUser = localStorage.getItem("username");
      this.draftForm.patchValue({
        auditId: this.selectAudit,
        adr_created_by: currentUser,
        adr_id: this.adrId,
        dup_adr_Flag: false,
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveDraftRepoData, this.draftForm.value)
            .subscribe((res: any) => {
              if (res) {
                console.log("res", res);
                this.title = "Success";
                this.content = "DraftReport updated successfully...";
                this.myCommonAlert(this.title, this.content);
                var payload = { draftReport: this.count + 1 };
                this.httpservice
                  .securePost(ConstUrls.updateCountData, payload)
                  .subscribe((result) => {
                    if (result) {
                      this.count = this.count + 1;
                      this.memo.setValueann(this.count);
                    }
                  });
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }

  closeModal() {
    this.closedDRForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  
 // print click added by Abu - 16-09-2020 start
 print(){
  window.print();
}
// print click added by Abu - 16-09-2020 end
// search functionlity added by Abu - 18-09-2020 start 
addnewFile() {
  this.editsearchDocument = true;
}
getSelectedDocs() {
  this.editsearchDocument = false;
}
closeSearch() {
  this.editsearchDocument = false;
}

// search functionlity added by Abu - 18-09-2020 end 
}
