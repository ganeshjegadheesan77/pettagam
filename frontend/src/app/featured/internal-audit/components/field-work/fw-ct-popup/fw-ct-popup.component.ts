import { Component, OnInit } from '@angular/core';
import {DefaultModalComponent} from 'src/app/layout/modal/default-modal/default-modal.component';
import { BsModalService,BsModalRef } from 'ngx-bootstrap';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';

@Component({
  selector: 'app-fw-ct-popup',
  templateUrl: './fw-ct-popup.component.html',
  styleUrls: ['./fw-ct-popup.component.less']
})
export class FwCtPopupComponent implements OnInit {
  position;
  auditId:any;
  modalRef: BsModalRef;
  public newControlTest:boolean;
  // assign search  added by Abu - 18-09-2020 start
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {}; 
  editsearchDocument: boolean = false;
  // assign search  added by Abu - 18-09-2020 end
  constructor(private modalService: BsModalService, private mScrollbarService: MalihuScrollbarService  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    
    
    {
    if(!JSON.parse(localStorage.getItem('selectedAuditId'))){
      const initialState = {
        title: 'Error',
        content: 'Please select Audit',
        link: 'Ok',
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
      // this.closeIssueForm.emit(true)
      this.closeModal();
    }
  }
  this.dropdownSettings = {
    singleSelection: false,
    idField: "documentNo",
    textField: "fileName",
    _id: "_id",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 10,
    allowSearchFilter: true,
  };
  }

  changedValuePreventing(){

  }


  closeModal() {
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').addClass('d-none');
    this.position = this.position ? undefined : {x: 0, y: 0}
  }

  // search functionlity added by Abu - 18-09-2020 start 
  addnewFile() {
    this.editsearchDocument = true;
  }
  getSelectedDocs() {
    this.editsearchDocument = false;
  }
  closeSearch() {
    this.editsearchDocument = false;
  }
  
  // search functionlity added by Abu - 18-09-2020 end 

}
