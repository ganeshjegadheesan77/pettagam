import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCtPopupComponent } from './fw-ct-popup.component';

describe('FwCtPopupComponent', () => {
  let component: FwCtPopupComponent;
  let fixture: ComponentFixture<FwCtPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCtPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCtPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
