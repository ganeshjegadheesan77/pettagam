import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

@Component({
  selector: "app-fw-recalcultion-new-popup",
  templateUrl: "./fw-recalcultion-new-popup.component.html",
  styleUrls: ["./fw-recalcultion-new-popup.component.less"],
})
export class FwRecalcultionNewPopupComponent implements OnInit, OnChanges {
  dropdownList = [];
  selectedItems = [];
  fieldWorkForm: FormGroup;

  dropdownSettings = {};
  @Output() closedNFWForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalRef: BsModalRef;
  position;
  uri;

  marked: boolean;
  openAttachment: boolean;
  uploadedFilesArr;
  multiFile: any = [];
  // attachedFiles: any;
  attachedFiles: any = [];
  fetchedData: any = [];
  subprocesses: any;
  risks: any;
  title: string;
  content: string;
  selectAudit: any;
  fw_id: number;
  auditFw_id: string;
  fieldPanes: any;
  auditTeam: any[];
  mixArr: any;
  testerArr: any;
  reviewerArr: any;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService

  ) {
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    console.log("New Field WOrk called...");
    this.initForm();
    this.fetchedData = [];

    if (!JSON.parse(localStorage.getItem("selectedAuditId"))) {
      this.title = "Error";
      this.content = "Please select Audit..";
      this.myCommonAlert(this.title, this.content);
      this.closeModal();
    } else {
      this.selectAudit = JSON.parse(localStorage.getItem("selectedAuditId"));
      this.getSubprocesses();
      this.generateFWId();
    }
  }

  ngOnChanges() {}

  getSubprocesses() {
    this.httpservice
      .secureGet(ConstUrls.getSubprocesses)
      .subscribe((res: any) => {
        if (res) {
          this.subprocesses = res;
        }
      });
  }

  getRisks(sbpName) {
    if (sbpName) {
      let payload = { subProcess_name: sbpName };
      this.httpservice
        .securePost(ConstUrls.getRiskForSubprocess, payload)
        .subscribe((res: any) => {
          if (res) {
            console.log("res", res[0]);
            this.risks = res[0].risks;
          }
        });
    }
  }

  generateFWId() {
    let payload = { auditId: this.selectAudit };

    this.httpservice
      .securePost(ConstUrls.getFWDesc, payload)
      .subscribe((res: any) => {
        if (res) {
          this.httpservice
            .securePost(ConstUrls.getTeamForPbcAndFW, payload)
            .subscribe((resp: any) => {
              console.log("resp", resp);
              //Audit team code
              this.auditTeam = [];
              this.mixArr = resp[0].resourePlanningData[0];
              console.log("mixArr", this.mixArr);
              let newArr = [];
              this.testerArr = [];
              this.reviewerArr = [];
              if (this.mixArr) {
                this.mixArr.fAssignee.forEach((ele) => {
                  this.testerArr.push(ele);
                });

                this.mixArr.fapAssignee.forEach((ele) => {
                  this.reviewerArr.push(ele);
                });
              }
            });
          console.log("res", res);
          this.fieldPanes = res[0];
          if (
            !moment(
              this.fieldPanes.performancePeriod[0],
              "DD-MM-YYYY",
              true
            ).isValid() &&
            this.fieldPanes.performancePeriod.length > 0
          ) {
            let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
              "DD-MM-YYYY"
            );
            let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
              "DD-MM-YYYY"
            );
            let str = d1 + "-" + d2;
            this.fieldWorkForm.patchValue({ performance_period: str });
          } else if (this.fieldPanes.performancePeriod.length == 0) {
            this.fieldWorkForm.patchValue({
              performance_period: this.fieldPanes.performancePeriod,
            });
          } else {
            let d1 = this.fieldPanes.performancePeriod[0];
            let d2 = this.fieldPanes.performancePeriod[1];
            let str = d1 + "-" + d2;
            this.fieldWorkForm.patchValue({ performance_period: str });
          }
          if (res[0].auditFieldWork.length > 0) {
            let data = res[0].auditFieldWork.reduce(function (prev, current) {
              return prev.fw_id > current.fw_id ? prev : current;
            });
            this.fw_id = parseInt(data.fw_id) + 1;
            this.auditFw_id = data.auditFw_id.split("-")[0] + "-" + this.fw_id;
          } else {
            this.fw_id = 1;

            this.auditFw_id = this.selectAudit.replace("-", "") + "-1";
          }
        }
      });
  }

  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      this.fetchedData.push(data[i]["name"]);
    }
    let arr = [];
    arr = this.fetchedData;
    this.fieldWorkForm.patchValue({ afw_attachments: arr });
    this.httpservice
      .securePost("uploadFieldWork", formData)
      .subscribe((files) => {
        this.multiFile = [];
      });
  }

  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
  }

  removeAttachment(removeitem, index) {
    if (removeitem.new) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
    } else {
      const fileindex = parseInt(index);
      let payload = {
        directory: "uploads/fieldWork",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            this.attachedFiles.splice(fileindex, 1);

            this.fetchedData.attachment = [];
            this.attachedFiles.forEach((x) => {
              this.fetchedData.push(x.fullName);
            });
            let payload = {
              fileName: removeitem.fullName,
            };
            this.httpservice
              .securePost(ConstUrls.deleteAIFile, payload)
              .subscribe((response: any) => {
                if (response) {
                }
              });
            setTimeout(function () {}, 3000);
            if (response) {
              const initialState = {
                title: "Success",
                content: "Attachment Deleted Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
            }
          }
        });
    }
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      this.openAttachment = true;
    }
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    let currentUser = localStorage.getItem("username");

    let afw_signoff_period = moment(
      this.fieldWorkForm.get("afw_signoff_period").value
    ).format("DD-MM-YYYY");
    if (afw_signoff_period == "Invalid date") afw_signoff_period = "-";

    this.fieldWorkForm.patchValue({
      afw_created_by: currentUser,
      afw_signoff_period: afw_signoff_period,
      afw_id: this.fw_id,
      master_audit_id: this.selectAudit,
      auditId: this.selectAudit,
      auditFw_id: this.auditFw_id,
      status: "assigned",
    });
    console.log("Form Contents: ", this.fieldWorkForm.value);
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.saveFW, this.fieldWorkForm.value)
          .subscribe((res: any) => {
            if (res) {
              this.title = "Success";
              this.content = "Fieldwork saved successfully...";
              this.myCommonAlert(this.title, this.content);
              this.closeModal();
            }
          });
      }
    });
  }

  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }

  closeModal() {
    this.closedNFWForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }

  initForm() {
    this.fieldWorkForm = this.formBuilder.group({
      auditName: ["", []],
      auditId: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      afw_name: ["", []],
      afw_id: ["", []],
      auditFw_id: ["", []],
      afw_audit_pgm_id: ["", []],
      afw_subprocess: ["", []],
      afw_risk: ["", []],
      afw_tester: ["", []],
      afw_reviewer: ["", []],
      afw_signoff_period: ["", []],
      afw_audit_pgm_title: ["", []],
      afw_test_procedure: ["", []],
      afw_status: ["", []],
      afw_results: ["", []],
      afw_attachments: ["", []],
      afw_created_by: ["", []],
      afw_updated_by: ["", []],
      status: ["", []],
    });
  }
   // print click added by Abu - 16-09-2020 start
   print(){
    window.print();
  }
 // print click added by Abu - 16-09-2020 end
}
