import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwRecalcultionNewPopupComponent } from './fw-recalcultion-new-popup.component';

describe('FwRecalcultionNewPopupComponent', () => {
  let component: FwRecalcultionNewPopupComponent;
  let fixture: ComponentFixture<FwRecalcultionNewPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwRecalcultionNewPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwRecalcultionNewPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
