import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PbcPopupComponent } from './pbc-popup.component';

describe('PbcPopupComponent', () => {
  let component: PbcPopupComponent;
  let fixture: ComponentFixture<PbcPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PbcPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PbcPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
