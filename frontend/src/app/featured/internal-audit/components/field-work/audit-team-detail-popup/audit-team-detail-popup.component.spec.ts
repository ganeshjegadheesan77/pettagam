import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditTeamDetailPopupComponent } from './audit-team-detail-popup.component';

describe('AuditTeamDetailPopupComponent', () => {
  let component: AuditTeamDetailPopupComponent;
  let fixture: ComponentFixture<AuditTeamDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditTeamDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditTeamDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
