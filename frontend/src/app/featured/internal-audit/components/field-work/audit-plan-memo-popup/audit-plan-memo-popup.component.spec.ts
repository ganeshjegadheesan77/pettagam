import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditPlanMemoPopupComponent } from './audit-plan-memo-popup.component';

describe('AuditPlanMemoPopupComponent', () => {
  let component: AuditPlanMemoPopupComponent;
  let fixture: ComponentFixture<AuditPlanMemoPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditPlanMemoPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditPlanMemoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
