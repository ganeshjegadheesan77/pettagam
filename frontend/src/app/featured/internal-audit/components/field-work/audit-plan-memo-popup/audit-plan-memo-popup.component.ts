import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { InterviewSchedulePopupComponent } from "src/app/layout/modal/interview-schedule-popup/interview-schedule-popup.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { MemoService } from "../memo.service";
// import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConfigService } from 'src/config.service';


declare var $;
@Component({
  selector: "app-audit-plan-memo-popup",
  templateUrl: "./audit-plan-memo-popup.component.html",
  styleUrls: ["./audit-plan-memo-popup.component.less"],
})
export class AuditPlanMemoPopupComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;

  @Output() closedMemoForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalRef2;
  memorandumForm: FormGroup;
  globalSearchData = "";
  arr: any;
  position;
  modalRef: BsModalRef;
  disableDropzone = true;
  copiedAuditData: any;
  apmId: any;
  content: string;
  uploadedFilesArr: any;
  fetchedData: any;
  uri;
  multiFile: any = [];
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  fieldPanes: any;
  deletedAttachment: any = [];
  auditName: any;
  newMemoForm: boolean;
  disableSearch = false;
  auditId: any;
  division: any;
  departments: any;
  leads: any;
  processes: any;
  updateCount: any;
  count: number;
  title: string;
  closeAttachment: boolean;
  mixArr: any;
  auditTeam: any[];

  // assign search  added by Abu - 18-09-2020 start
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  editsearchDocument: boolean = false;
  auditeeTeam = [];
  companyName: any;
  location: any;
  buName: any;
  bProcessName: any;
  isCompleted: boolean;
  // assign search  added by Abu - 18-09-2020 end

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private epService: MemoService,
    private mScrollbarService: MalihuScrollbarService,
    private configService: ConfigService


  ) {
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });

    this.epService.auditId.subscribe((auditId) => {
      if (auditId) {
        this.auditId = auditId;
        this.getPlanningMemorandum(this.auditId);
      }
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };

    this.initForm();

  }
  // ngOnInit() {
  //   this.mScrollbarService.initScrollbar(".scroll-container", {
  //     axis: "y",
  //     theme: "metro",
  //   });
  //   //  console.log('ON INIT call');
  //   this.initForm();
  //   this.getMasters();
  //   this.title = "";
  //   this.content = "";

  //   this.getAuditData();
  //   this.memo.getValue().subscribe((val) => {
  //     this.count = val;
  //   });
  //   this.fetchedData = [];

  //   // this.isAttachment=false;
  //   // this.openAttachment = true;
  //   // var data= JSON.parse(localStorage.getItem('fieldWorkCommonData'));
  //   // this.memorandumForm.patchValue(data);

  //   this.dropdownSettings = {
  //     singleSelection: false,
  //     idField: "documentNo",
  //     textField: "fileName",
  //     _id: "_id",
  //     selectAllText: "Select All",
  //     unSelectAllText: "UnSelect All",
  //     itemsShowLimit: 10,
  //     allowSearchFilter: true,
  //   };
  // }

  onFilechanged(event) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      for (let i = 0; i < this.attachedFiles.length; i++) {
        if (this.attachedFiles[i]['documentName'] == files[0]["name"]) {
          this.myCommonAlert('File already exist...', 'Error');
          return
        }
      }
      console.log("fil anme", files);
      formData.append("file", files[0], files[0]["name"]);
      formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
      formData.append("userType", 'auditor');
      formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=EngagementPlanning,Feature-Name=auditPlanningMemo,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
      formData.append("routerLink", ""),
        formData.append("subModuleName", "audit-planning-memorandum"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.selectAudit),
        formData.append("auditName", "name"),
        formData.append("moduleName", "EngagementPlanning"),
        formData.append("checkedinDate", moment().format("L"));
      console.log('****** formData', formData);
      this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            console.log('filename', res[i].fileName)
            let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            this.attachedFiles.push({
              documentName: res[i].fileName, documentNo: res[i].documentNo
              , documentUrlToken: res[i].urlToken, extension: extn
            });
            console.log('res[i].fileName', res[i].fileName)

          }
          console.log('attched filess array:', this.attachedFiles)
          console.log('******res', res)
          this.myCommonAlert('File uploaded successfully', 'Success');

        }
      }
        , (error) => {
          this.myCommonAlert(error.message, 'Error');

        })



    } else {
      this.myCommonAlert('Invalid file', 'Error');
      $("#myFile").val('');
    }
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let fileNameArr = fileInput.split(".");
    let extension = '.' + fileNameArr[fileNameArr.length - 1];
    console.log('extension@@@', extension);
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }

  getPlanningMemorandum(auditId) {
    if (auditId) {
      this.selectAudit = auditId;
      let payload = {
        auditId: auditId
      }
      this.httpservice.securePost(ConstUrls.getAuditPlanningMemorandum, payload).subscribe((res: any) => {
        if (res) {
          console.log('MEMO DATA: ', res)
          let memoData = res.data[0];
          this.companyName = memoData.category.levelOne.data;
          this.location = memoData.category.levelTwo.data;
          this.buName = memoData.category.levelThree.data;
          this.bProcessName = memoData.category.businessProcess;
          this.isCompleted = memoData.planningMemorandum.isCompleted;
          if (memoData.planningMemorandum.attachments.length > 0) {
            for (let i = 0; i < memoData.planningMemorandum.attachments.length; i++) {
              console.log('filename: ', memoData.planningMemorandum.attachments[i].documentName);
              let extn = memoData.planningMemorandum.attachments[i].documentName.substr(memoData.planningMemorandum.attachments[i].documentName.lastIndexOf(".") + 1);
              this.attachedFiles.push({
                documentName: memoData.planningMemorandum.attachments[i].documentName,
                documentNo: memoData.planningMemorandum.attachments[i].documentNo
                , documentUrlToken: memoData.planningMemorandum.attachments[i].documentUrlToken,
                extension: extn
              });
            }

            console.log('saved file array', this.attachedFiles)
            // this.attachedFiles = memoData.planningMemorandum.attachments;
          } else {
            this.attachedFiles = [];
          }
          this.memorandumForm.patchValue(memoData.planningMemorandum);
          let openingMeetingDate = '';
          let closingMeetingDate = '';
          let periodInScope = '';
          if (memoData.teamAndTimeline.openingMeetingDate) {
            openingMeetingDate = moment(memoData.teamAndTimeline.openingMeetingDate).format("DD-MM-YYYY");
          }
          if (memoData.teamAndTimeline.closingMeetingDate) {
            closingMeetingDate = moment(memoData.teamAndTimeline.closingMeetingDate).format("DD-MM-YYYY");
          }
          if (memoData.teamAndTimeline.periodInScope.length) {
            periodInScope = moment(memoData.teamAndTimeline.periodInScope[0]).format("DD-MM-YYYY") + '-' + moment(memoData.teamAndTimeline.periodInScope[1]).format("DD-MM-YYYY")
          }
          this.memorandumForm.patchValue({
            openingMeetingDate: openingMeetingDate,
            closingMeetingDate: closingMeetingDate,
            periodInScope: periodInScope,

          });


          if (typeof memoData.teamAndTimeline.auditees != undefined) {
            this.auditeeTeam = memoData.teamAndTimeline.auditees;
            console.log('auditeeTeam', this.auditeeTeam)
          }
          if (memoData.teamAndTimeline.auditLead || memoData.teamAndTimeline.auditors) {
            this.auditTeam = [...memoData.teamAndTimeline.auditLead, ...memoData.teamAndTimeline.auditors];
            console.log('auditTeam', this.auditTeam)
          }

        }
      }, (error) => {
        this.myCommonAlert(error.message, 'Error');

      })
    }
  }

  getMasters() {
    this.httpservice
      .secureGet(ConstUrls.getMasterDivision)
      .subscribe((res: any) => {
        if (res) {
          this.division = res;
          //console.log('Divisions:' ,this.division)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterDepartment)
      .subscribe((res: any) => {
        if (res) {
          this.departments = res;
          //console.log('departments:' ,this.departments)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterLeadContact)
      .subscribe((res: any) => {
        if (res) {
          this.leads = res;
          //console.log('leads:' ,this.leads)
        }
      });

    this.httpservice
      .secureGet(ConstUrls.getMasterProcess)
      .subscribe((res: any) => {
        if (res) {
          this.processes = res;
          //console.log('processes:' ,this.processes)
        }
      });
  }

  myCommonAlert(content, title, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }
  getAuditData() {
    this.attachedFiles = [];

    let payload = { auditId: this.selectAudit };
    this.httpservice
      .securePost(ConstUrls.getOpenMemotUpData, payload)
      .subscribe(async (res: any) => {
        // console.log('payload', payload)
        this.fetchedData = res[0];

        this.fieldPanes = await res[0];
        console.log("response", this.fieldPanes);
        this.initForm();
        this.memorandumForm.patchValue(res);

        if (
          !moment(
            this.fieldPanes.periodInScope[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.periodInScope.length > 0
        ) {
          let d1 = moment(this.fieldPanes.periodInScope[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.periodInScope[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.memorandumForm.patchValue({ period_in_scope: str });
        } else if (this.fieldPanes.periodInScope.length == 0) {
          this.memorandumForm.patchValue({
            period_in_scope: this.fieldPanes.periodInScope,
          });
        } else {
          let d1 = this.fieldPanes.periodInScope[0];
          let d2 = this.fieldPanes.periodInScope[1];
          let str = d1 + "-" + d2;
          this.memorandumForm.patchValue({ period_in_scope: str });
        }

        if (
          !moment(
            this.fieldPanes.performancePeriod[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.performancePeriod.length > 0
        ) {
          let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.memorandumForm.patchValue({ performance_period: str });
        } else if (this.fieldPanes.performancePeriod.length == 0) {
          this.memorandumForm.patchValue({
            performance_period: this.fieldPanes.performancePeriod,
          });
        } else {
          let d1 = this.fieldPanes.performancePeriod[0];
          let d2 = this.fieldPanes.performancePeriod[1];
          let str = d1 + "-" + d2;
          this.memorandumForm.patchValue({ performance_period: str });
        }

        if (
          !moment(
            this.fieldPanes.openingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.openingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.openingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.memorandumForm.patchValue({ opening_meeting: d1 });
        } else if (!this.fieldPanes.openingMeetingDate) {
          this.memorandumForm.patchValue({
            opening_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.openingMeetingDate;
          this.memorandumForm.patchValue({ opening_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.closingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.closingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.closingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.memorandumForm.patchValue({ closing_meeting: d1 });
        } else if (!this.fieldPanes.closingMeetingDate) {
          this.memorandumForm.patchValue({
            closing_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.closingMeetingDate;
          this.memorandumForm.patchValue({ closing_meeting: d1 });
        }

        this.auditName = this.fieldPanes.auditableEntity;
        if (this.fetchedData.planningMemorandum.apm_attachments.length != 0) {
          this.openAttachment = true;
          for (
            let i = 0;
            i < this.fetchedData.planningMemorandum.apm_attachments.length;
            i++
          ) {
            let extn = this.fetchedData.planningMemorandum.apm_attachments[
              i
            ].substr(
              this.fetchedData.planningMemorandum.apm_attachments[
                i
              ].lastIndexOf(".") + 1
            );
            let name = this.fetchedData.planningMemorandum.apm_attachments[
              i
            ].substr(
              0,
              this.fetchedData.planningMemorandum.apm_attachments[
                i
              ].lastIndexOf(".")
            );

            this.attachedFiles.push({
              relativePath: name,
              extension: extn,
              fullName: name + "." + extn,
              new: false,
              isDuplicate: this.fieldPanes.planningMemorandum.dup_apm_Flag,
            });
          }
        } else {
          this.openAttachment = false;
        }
        if (res) {
          console.log("res", res);
          if (res[0].planningMemorandum.PMisSet === true) {
            this.apmId = res[0].planningMemorandum.apm_id;
            this.memorandumForm.patchValue(res[0].planningMemorandum);
            //Audit team code
            this.auditTeam = [];
            this.mixArr = res[0].resourePlanningData;
            console.log("mixArr", this.mixArr);
            let newArr = [];
            if (this.mixArr[0]) {
              this.mixArr[0].pAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              this.mixArr[0].papAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              let newArray = newArr.filter(
                (v, i, a) => a.findIndex((t) => t === v) === i
              );
              newArray.forEach((element) => {
                this.auditTeam.push(element);
              });
            }
          } else {
            this.title = "Error";
            this.content = "Not Allowed ...";
            this.myCommonAlert(this.content, this.title);
            this.closeModal(false);
          }
        }
      });
  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      // attachment left

      this.openAttachment = true;
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
          //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  removeAttachment(removeitem, index) {
    const fileindex = parseInt(index);
    this.attachedFiles.splice(fileindex, 1);
    console.log('@@@@@@---', this.attachedFiles);
    this.myCommonAlert("File removed successfully", "Success", 'success');
  }
  // removeAttachment(removeitem, index) {
  //   this.closeAttachment = false;
  //   if (removeitem.new == true) {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
  //     if (idx != -1) this.multiFile.splice(idx, 1);
  //   } else {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     console.log("this.attachedFiles", this.attachedFiles);
  //     let payload = {
  //       directory: "uploads/fieldWork",
  //       fullName: removeitem.fullName,
  //     };
  //     this.httpservice
  //       .securePost("removeAttachment", payload)
  //       .subscribe((response: any) => {
  //         if (response) {
  //           if (response) {
  //             this.fetchedData.planningMemorandum.apm_attachments = [];
  //             this.attachedFiles.forEach((ele) => {
  //               this.fetchedData.planningMemorandum.apm_attachments.push(
  //                 ele.fullName
  //               );
  //             });

  //             this.title = "Success";
  //             (this.content = "Attachment Deleted Successfully"),
  //               this.myCommonAlert(this.content, this.title);
  //           }
  //         }
  //       });
  //   }
  // }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    //console.log('this is data',data)
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.planningMemorandum.apm_attachments) {
        this.fetchedData.planningMemorandum.apm_attachments.push(
          data[i]["name"]
        );
      } else {
        this.fetchedData.planningMemorandum.apm_attachments = [];
        this.fetchedData.planningMemorandum.apm_attachments.push(
          data[i]["name"]
        );
      }
    }
    let arr = [];
    arr = this.fetchedData.planningMemorandum.apm_attachments;
    //console.log('attchment array',this.fetchedData)
    this.memorandumForm.patchValue({ apm_attachments: arr });
    console.log("filearray", arr.toString());
    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "auidt-plan-memo",
      tab: "auidt-plan-memo",
      "router-link": "/rcm/internal-audit/engagement-planning",

      file: filearr.toString(),
      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: this.configService.config.db,
    };

    console.log("check", payload);
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        if (files) {
          console.log("Response", files);
        }
        //console.log('Uploaded');
        this.multiFile = [];
      });
    if (formData) {
      this.httpservice
        .securePost("uploadFieldWork", formData)
        .subscribe((files) => {
          //console.log('Uploaded');
          this.multiFile = [];
        });
    }
  }

  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }

    let payload = {
      database: this.configService.config.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        //  console.log('Uploaded');
        if (files) {
          console.log("audit Response", files);
        }
        this.multiFile = [];
      });
  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }

  closeModal(popupFlag) {
    if (popupFlag) {
      const initialState = {
        title: "Confirmation",
        content: `Do you want to close current form ?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef2 = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef2.content.onClose.subscribe((r) => {
        if (r) {
          this.closedMemoForm.emit(true);
          $(".scroll-container").scrollTop(0);
          $(".pop-up-form").addClass("d-none");
          this.position = this.position ? undefined : { x: 0, y: 0 };
        }
      })
    } else {
      this.closedMemoForm.emit(true);
      $(".scroll-container").scrollTop(0);
      $(".pop-up-form").addClass("d-none");
      this.position = this.position ? undefined : { x: 0, y: 0 };
    }

  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    // this.upload();

    if (this.selectAudit) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      let capitalizedcurrentUser = currentUser.charAt(0).toUpperCase() + currentUser.slice(1)
      if (!this.isCompleted) {
        this.memorandumForm.patchValue({
          preparedBy: capitalizedcurrentUser
        });
      }
      this.memorandumForm.patchValue({
        auditId: this.selectAudit,
        isCompleted: true,
        attachments: this.attachedFiles
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveAuditPlanningMemorandum, this.memorandumForm.value)
            .subscribe((res: any) => {
              if (res) {
                this.myCommonAlert(res.message, 'Message', 'success');
                this.epService.refresh(true);
                this.closeModal(false);
              }
            }, (error) => {
              this.myCommonAlert(error.message, 'Error');
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.content, this.title);
    }


  }

  // onSave(){
  //   if(this.newMemoForm === false){
  //         let currentUser = localStorage.getItem('username');
  //       //console.log('UserName: ' , currentUser);

  //       //console.log('Form Contents: ' ,this.memorandumForm.value)
  //       if(this.objIdToFind){
  //         this.upload();
  //         this.updateCount++;
  //         this.memorandumForm.patchValue({'_id':this.objIdToFind ,'apm_created_by':currentUser,'apm_updateCount':this.updateCount})

  //         this.httpservice.securePost(ConstUrls.updateOpenMemo , this.memorandumForm.value).subscribe((res:any)=>{
  //             if(res){
  //               this.count= this.count+1
  //               var payload ={"memo":this.count}
  //           this.httpservice.securePost(ConstUrls.updateCountData,payload).subscribe(r=>{
  //             if(r){
  //               // this.memo.getValue().subscribe(data=>{
  //               //   this.count= data
  //               // })
  //               this.memo.setValue(this.count)
  //             }
  //           })
  //           const initialState = {
  //             title: 'Success',
  //             content: 'Audit plan memorandum Updated successfully',
  //             link: 'Ok',
  //           };
  //           this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //           }else{
  //             const initialState = {
  //               title: 'Fail',
  //               content: 'failed to update Audit plan memorandum',
  //               link: 'Ok',
  //             };
  //             this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //           }
  //           this.closeModal();
  //         })
  //       }else{
  //         const initialState = {
  //           title: 'Success',
  //           content: 'please select audit to Update',
  //           link: 'Ok',
  //         };
  //         this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
  //         this.closeModal();
  //       }
  //   }else{
  //     let currentUser = localStorage.getItem('username');
  //     this.memorandumForm.removeControl('_id');
  //     this.memorandumForm.patchValue({'apm_created_by':currentUser})
  //     //console.log('FORM DATA:' , this.memorandumForm.value)
  //     this.upload();
  //     this.httpservice.securePost(ConstUrls.saveAuditPlanMemo , this.memorandumForm.value).subscribe((res:any)=>{
  //       if(res){
  //         this.auditId = JSON.parse(localStorage.getItem('auditData'))['newAuditId']['auditId'];

  //         let newPayload = {'auditId' : this.auditId , 'audit_plan_memo' : res[0]._id}
  //           //console.log('New Payload:' , newPayload);
  //           this.httpservice.securePost(ConstUrls.updateAuditPlanMemo , newPayload).subscribe((res:any)=>{
  //             if(res){
  //               this.count= this.count+1;
  //               var payload ={"memo":this.count}
  //           this.httpservice.securePost(ConstUrls.updateCountData,payload).subscribe(r=>{
  //             if(r){

  //               this.memo.setValue(this.count)
  //             }
  //           })
  //               const initialState = {
  //                 title: 'Success',
  //                 content: 'Audit planning memorandum Saved successfully',
  //                 link: 'Ok',
  //                };
  //         this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //             }
  //           })

  //         this.closeModal();
  //       }
  //     })

  //     // alert('NEED TO SAVE')
  //   }

  // }

  initForm() {
    this.memorandumForm = this.formBuilder.group({
      auditId: ["", []],
      auditObjectives: ["", []],
      auditScope: ["", []],
      outOfScope: ["", []],
      sentOn: ["", []],
      preparedBy: ["", []],
      sentBy: ["", []],
      openingMeetingDate: ["", []],
      closingMeetingDate: ["", []],
      periodInScope: ["", []],
      isCompleted: ["", []],
      attachments: ["", []]
    });

    //   attachments: [
    //     {
    //         documentNo: {
    //             type: String,
    //             required: false
    //         },
    //         documentName: {
    //             type: String,
    //             required: false
    //         },
    //         documentUrlToken: {
    //             type: String,
    //             required: false
    //         }
    //     }
    // ],
    // this.memorandumForm = this.formBuilder.group({
    //   _id: ["", []],
    //   auditName: ["", []],
    //   closing_meeting: [{ value: "", disabled: true }, []],
    //   opening_meeting: [{ value: "", disabled: true }, []],
    //   update_meeting: [{ value: "", disabled: true }, []],
    //   year: ["", []],
    //   period_in_scope: [{ value: "", disabled: true }, []],
    //   performance_period: [{ value: "", disabled: true }, []],
    //   master_audit_id: ["", []],
    //   apm_id: ["", []],
    //   apm_name: ["", []],
    //   apm_division: ["", []],
    //   apm_audit_scope: ["", []],
    //   apm_audit_objective: ["", []],
    //   apm_lead_contact: ["", []],
    //   apm_dept: ["", []],
    //   PMisSet: [{ value: true }, []],
    //   auditId: ["", []],
    //   apm_process: ["", []],
    //   apm_audit_team: ["", []],
    //   apm_out_of_scope: ["", []],
    //   apm_attachments: ["", []],
    //   apm_created_by: ["", []],
    //   apm_updated_by: ["", []],
    //   apm_updateCount: [],
    //   duplicateAuditFlag: ["", []],
    //   dup_apm_Flag: ["", []],
    // });
  }

  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185", searchBy: localStorage.getItem('username') };

      this.httpservice
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              // console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            console.log('dropdownListAll', this.dropdownListAll)
            this.dropdownFirmList = this.arr;
            // this.attachedFiles = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    // this.dropdownListAll = [
    //   { item_id: 1, item_text: 'Mumbaisssssssssss' },
    //   { item_id: 2, item_text: 'Bangaluru' },
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' },
    //   { item_id: 5, item_text: 'New Delhi' },
    //   { item_id: 6, item_text: 'Mumbai' },
    //   { item_id: 7, item_text: 'Bangaluru' },
    //   { item_id: 8, item_text: 'Pune' },
    //   { item_id: 9, item_text: 'Navsari' },
    //   { item_id: 10, item_text: 'New Delhi' },
    //   { item_id: 11, item_text: 'Mumbai' },
    //   { item_id: 12, item_text: 'Bangaluru' },
    //   { item_id: 13, item_text: 'Pune' },
    //   { item_id: 14, item_text: 'Navsari' },
    //   { item_id: 15, item_text: 'New Delhi' }
    // ];
  }

  addStepTwo() {
    $("body").addClass("modalPopupOne");
    const initialState = {
      title: "Interview Schedule",
      link: "Submit",
      action: "Cancel",
    };
    this.modalRef = this.modalService.show(InterviewSchedulePopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

  }
  // print click added by Abu - 16-09-2020 start
  print() {
    window.print();
  }
  // print click added by Abu - 16-09-2020 end
  // search functionlity added by Abu - 18-09-2020 start 
  addnewFile() {
    this.editsearchDocument = true;
  }
  getSelectedDocs() {
    if (!this.selectedItems.length) {
      this.myCommonAlert("Please select atleast a document.", "Alert");
      return;
    }
    // this.searchDocument = false;
    var fileList = this.selectedItems.map((item) => {
      return item["fileName"];
    });
    console.log("Selected files: ", this.selectedItems);
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i]['metaData'] = `Module-Name=EngagementPlanning,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
      // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
      this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
      this.selectedItems[i]['userType'] = 'auditor';
      this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
      this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
      this.selectedItems[i]['roomType'] = '';
      this.selectedItems[i]['routerLink'] = '';
      this.selectedItems[i]['subModuleName'] = 'audit-planning-memorandum';
      this.selectedItems[i]['auditYear'] = new Date().getFullYear();
      this.selectedItems[i]['auditId'] = this.selectAudit;
      this.selectedItems[i]['auditName'] = 'name';
      this.selectedItems[i]['moduleName'] = 'EngagementPlanning';
      this.selectedItems[i]['checkedinDate'] = moment().format("L");

    }
    console.log('Selected Item ARRAY: ', this.selectedItems);
    let payloads = { items: this.selectedItems };
    this.httpservice.securePost(ConstUrls.DmsSaveAsDocument, payloads).subscribe((res: any) => {
      if (res) {
        console.log('res', res);
        for (let i = 0; i < res.length; i++) {
          console.log('filename', res[i].fileName)
          let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
          // res[i].fileName.substr(res[i].fileName.lastIndexOf('.') + 1)
          this.attachedFiles.push({
            documentName: res[i].fileName, documentNo: res[i].documentNo
            , documentUrlToken: res[i].urlToken, extension: extn
          });
          console.log('res[i].fileName', res[i].fileName)

        }
        console.log('attched filess array:', this.attachedFiles)
        this.myCommonAlert('File Uploaded successfully', 'Message', 'success');
      }
    }, (error) => {
      this.myCommonAlert(error.massage, 'Error');

    })
    // let payload = {
    //   auditId: this.auditId, username: localStorage.getItem("username"),
    //   userType: "auditor",
    //   fileList: fileList,
    // };
    // console.log("Payload: ", payload);

    // this.selectedItems.forEach((f,i) => {
    //   this.workstepForm.workpapers.push(
    //     { workpaperId: f.documentNo, workpaperName: f.fileName }
    //   )
    // })

    this.editsearchDocument = false;
  }
  closeSearch() {
    this.editsearchDocument = false;
  }

  // search functionlity added by Abu - 18-09-2020 end 

}