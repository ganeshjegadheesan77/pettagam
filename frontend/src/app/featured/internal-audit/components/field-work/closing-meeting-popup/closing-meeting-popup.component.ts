import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { HttpService } from "src/app/core/http-services/http.service";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { MemoService } from "../memo.service";
import * as moment from "moment";

// import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';
declare var $;

@Component({
  selector: "app-closing-meeting-popup",
  templateUrl: "./closing-meeting-popup.component.html",
  styleUrls: ["./closing-meeting-popup.component.less"],
})
export class ClosingMeetingPopupComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  @Output() closedCMForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  closingMeetingForm: FormGroup;
  modalRef: BsModalRef;
  position;
  CMId: any;
  content: string;
  auditName: any;
  auditId: any;
  aaId: any;
  uploadedFilesArr: any;
  fetchedData: any;
  uri;
  multiFile: any = [];
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  fieldPanes: any;
  deletedAttachment: any = [];
  isPdf = false;
  notPdf = true;
  newId: number;
  currAuditId: any;
  acmId: any;
  issueTable: any;
  newClosingMeeting: boolean;
  assignedActions: any = [];
  actionList: any = [];
  selectReport: any;
  pdfSrc: string;
  updateCount: any;
  count: number;
  title: string;
  closeAttachment: boolean;
  mixArr: any;
  globalSearchData = "";
  disableSearch = false;
  auditTeam: any[];
  assignedIssues = [];
  // assign search  added by Abu - 18-09-2020 start
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {}; 
  editsearchDocument: boolean = false;
  openingMeetingDate: string;
  closingMeetingDate: string;
  periodInScope: string;
  performancePeriod: string;
  updateMeetingDate: string;
  arr: any[];
  companyName: any;
  location: any;
  buName: any;
  bProcessName: any;
  // assign search  added by Abu - 18-09-2020 end
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private epService: MemoService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService

  ) {
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.fetchedData = [];
    this.initForm();
    this.epService.auditId.subscribe((auditId) => {
      if(auditId){
        this.auditId = auditId;
        this.getClosingMeetingFormData(this.auditId);
      }
    });

    
    // this.memo.getValueann().subscribe((data) => {
    //   this.count = data;
    // });

    // this.isAttachment = false;
    // this.openAttachment = true;
    // this.acmId = "";
    // this.title = "";
    // this.content = "";
    // this.getClosingMeetingData();
    // this.closingMeetingForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
    // this.memo.getValueann().subscribe((data) => {
    //   this.count = data;
    // });
    // console.log('Closing meeting form OPENED..')
    // this.getClosingMeetingData();

    // this.auditId = JSON.parse(localStorage.getItem("auditData"))["newAuditId"][
    //   "auditId"
    // ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };
  }

  getClosingMeetingFormData(auditId){
    this.selectAudit=auditId;
    let payload = { auditId: this.selectAudit };
    this.httpservice.securePost(ConstUrls.getClosingMeetingFormData, payload).subscribe((res: any) => {
        if(res){
          console.log('**** RESPONSE:', res)
          let closingMeetingData = res.data[0];
          this.closingMeetingForm.patchValue(closingMeetingData.closingMeeting);
          this.companyName = closingMeetingData.category.levelOne.data;
          this.location = closingMeetingData.category.levelTwo.data;
          this.buName = closingMeetingData.category.levelThree.data;
          this.bProcessName = closingMeetingData.category.businessProcess;

          if(closingMeetingData.closingMeeting.attachments.length >0){
            for (let i = 0; i < closingMeetingData.closingMeeting.attachments.length; i++) {
              console.log('filename: ',closingMeetingData.closingMeeting.attachments[i].documentName);
               let extn = closingMeetingData.closingMeeting.attachments[i].documentName.substr(closingMeetingData.closingMeeting.attachments[i].documentName.lastIndexOf(".") + 1);
            this.attachedFiles.push({documentName:closingMeetingData.closingMeeting.attachments[i].documentName ,
                 documentNo:closingMeetingData.closingMeeting.attachments[i].documentNo
                ,documentUrlToken:closingMeetingData.closingMeeting.attachments[i].documentUrlToken, 
                extension:extn});
            }
          }

        
          this.openingMeetingDate= moment(closingMeetingData.teamAndTimeline.openingMeetingDate).format("DD-MM-YYYY");
          this.closingMeetingDate=moment(closingMeetingData.teamAndTimeline.closingMeetingDate).format("DD-MM-YYYY"),
          this.updateMeetingDate=moment(closingMeetingData.teamAndTimeline.updateMeetingDate).format("DD-MM-YYYY"),
          
          this.periodInScope= moment(closingMeetingData.teamAndTimeline.periodInScope[0]).format("DD-MM-YYYY")+'-'+moment(closingMeetingData.teamAndTimeline.periodInScope[1]).format("DD-MM-YYYY");
          this.performancePeriod= moment(closingMeetingData.teamAndTimeline.performancePeriod[0]).format("DD-MM-YYYY")+'-'+moment(closingMeetingData.teamAndTimeline.performancePeriod[1]).format("DD-MM-YYYY");
          
          if(closingMeetingData.teamAndTimeline.auditLead || closingMeetingData.teamAndTimeline.auditors){
            this.auditTeam = [...closingMeetingData.teamAndTimeline.auditLead , ...closingMeetingData.teamAndTimeline.auditors];
            console.log('auditTeam', this.auditTeam)
          }
        
        }
      },(error)=>{
        this.myCommonAlert(error.message, 'Error')
      })
  }

  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185" , searchBy:localStorage.getItem('username') };

      this.httpservice
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              // console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            console.log('dropdownListAll', this.dropdownListAll)
            this.dropdownFirmList = this.arr;
            // this.attachedFiles = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    
    
  }
  myCommonAlert(content, title) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design",
      backdrop: "static",
      keyboard: false,
    });
  }

  onFilechanged(event) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
        const formData: any = new FormData();
        files = event.target.files;
        console.log("fil anme", files);
        formData.append("file", files[0], files[0]["name"]);
        formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
        formData.append("userType", 'auditor');
        formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=EngagementPlanning,Feature-Name=closingMeeting,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
        formData.append("routerLink", ""),
        formData.append("subModuleName", "closing-meeting"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.selectAudit),
        formData.append("auditName", "name"),
        formData.append("moduleName", "EngagementPlanning"),
        formData.append("checkedinDate", moment().format("L"));
        console.log('****** formData', formData);
        this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res:any) => {
          if(res){
            for (let i = 0; i < res.length; i++) {
              console.log('filename', res[i].fileName)
               let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            this.attachedFiles.push({documentName:res[i].fileName , documentNo:res[i].documentNo
               ,documentUrlToken:res[i].urlToken , extension:extn});
            console.log('res[i].fileName', res[i].fileName)
              
            }
            console.log('attched filess array:' ,this.attachedFiles)
            console.log('******res', res)
             this.myCommonAlert('Success', 'File uploaded successfully');

          }
        }
        ,(error)=>{
          this.myCommonAlert('Error', error.message);

        })
   
      

    } else {
      this.myCommonAlert('Error', 'Invalid file');
      $("#myFile").val('');
    }
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    console.log('extension', extension)
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }

  getClosingMeetingData() {
    this.attachedFiles = [];
    let payload = { auditId: this.selectAudit };
    console.log("selectAudit", this.auditId);
    this.httpservice
      .securePost(ConstUrls.getClosingMeetingData, payload)
      .subscribe(async (response: any) => {
        console.log("response", response);
        if (response.length) {
          this.assignedIssues = response[0].issueData;
          this.assignedActions = response[0].actionData;
        }

        this.fieldPanes = await response[0];
        //  this.initForm();

        this.closingMeetingForm.patchValue(response[0]);
        if (
          !moment(
            this.fieldPanes.periodInScope[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.periodInScope.length > 0
        ) {
          let d1 = moment(this.fieldPanes.periodInScope[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.periodInScope[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ period_in_scope: str });
        } else if (this.fieldPanes.periodInScope.length == 0) {
          this.closingMeetingForm.patchValue({
            period_in_scope: this.fieldPanes.periodInScope,
          });
        } else {
          let d1 = this.fieldPanes.periodInScope[0];
          let d2 = this.fieldPanes.periodInScope[1];
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ period_in_scope: str });
        }

        if (
          !moment(
            this.fieldPanes.performancePeriod[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.performancePeriod.length > 0
        ) {
          let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ performance_period: str });
        } else if (this.fieldPanes.performancePeriod.length == 0) {
          this.closingMeetingForm.patchValue({
            performance_period: this.fieldPanes.performancePeriod,
          });
        } else {
          let d1 = this.fieldPanes.performancePeriod[0];
          let d2 = this.fieldPanes.performancePeriod[1];
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ performance_period: str });
        }

        if (
          !moment(
            this.fieldPanes.openingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.openingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.openingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ opening_meeting: d1 });
        } else if (!this.fieldPanes.openingMeetingDate) {
          this.closingMeetingForm.patchValue({
            opening_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.openingMeetingDate;
          this.closingMeetingForm.patchValue({ opening_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.closingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.closingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.closingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ closing_meeting: d1 });
        } else if (!this.fieldPanes.closingMeetingDate) {
          this.closingMeetingForm.patchValue({
            closing_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.closingMeetingDate;
          this.closingMeetingForm.patchValue({ closing_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.updateMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.updateMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.updateMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ update_meeting: d1 });
        } else if (!this.fieldPanes.updateMeetingDate) {
          this.closingMeetingForm.patchValue({
            update_meeting: this.fieldPanes.updateMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.updateMeetingDate;
          this.closingMeetingForm.patchValue({ update_meeting: d1 });
        }
        this.auditName = this.fieldPanes.auditableEntity;

        // console.log('closingMeetingForm', this.closingMeetingForm)
        this.fetchedData = response[0];

        if (this.fetchedData.closingMeeting.acm_attachments.length != 0) {
          this.openAttachment = true;
          for (
            let i = 0;
            i < this.fetchedData.closingMeeting.acm_attachments.length;
            i++
          ) {
            let extn = this.fetchedData.closingMeeting.acm_attachments[
              i
            ].substr(
              this.fetchedData.closingMeeting.acm_attachments[i].lastIndexOf(
                "."
              ) + 1
            );
            let name = this.fetchedData.closingMeeting.acm_attachments[
              i
            ].substr(
              0,
              this.fetchedData.closingMeeting.acm_attachments[i].lastIndexOf(
                "."
              )
            );

            this.attachedFiles.push({
              relativePath: name,
              extension: extn,
              fullName: name + "." + extn,
              new: false,
            });
          }
        } else {
          this.openAttachment = false;
        }

        // if (this.fieldPanes.closingMeeting.acm_attachments) {

        //   for (let i = 0; i < this.fieldPanes.closingMeeting.acm_attachments.length; i++) {
        //     this.attachedFiles.push({
        //       'relativePath': this.fieldPanes.closingMeeting.acm_attachments[i].split('.')[0],
        //       'extension': this.fieldPanes.closingMeeting.acm_attachments[i].split('.')[1], 'fullName': this.fieldPanes.closingMeeting.acm_attachments[i]

        //     })
        //   }
        // }
        if (response) {
          if (response[0].closingMeeting.CMisSet === true) {
            this.acmId = response[0].closingMeeting.acm_id;
            this.closingMeetingForm.patchValue(response[0].closingMeeting);
            //Audit team code
            this.auditTeam = [];
            this.mixArr = response[0].resourePlanningData;
            console.log("mixArr", this.mixArr);
            let newArr = [];
            if (this.mixArr[0]) {
              this.mixArr[0].rAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              this.mixArr[0].rapAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              let newArray = newArr.filter(
                (v, i, a) => a.findIndex((t) => t === v) === i
              );
              newArray.forEach((element) => {
                this.auditTeam.push(element);
              });
            }
          } else {
            this.title = "Error";
            this.content = "Not Allowed ...";
            this.myCommonAlert(this.title, this.content);
            this.closeModal();
          }
        }
      });
  }
  /////////////////////// java api for report ////////////////
  PrintClick() {
    this.selectAudit;
    let payload = {
      collection: "audit_closing_meeting",
      params: [
        {
          name: "acmId",
          value: this.acmId,
        },
        {
          name: "auditId",
          value: this.selectAudit,
        },
      ],
    };
    this.httpservice
      .secureJavaPostReportApi(
        "GenericReportBuilderService/buildReport",
        payload
      )
      .subscribe((res: any) => {
        this.pdfSrc = this.configService.config.baseUrl + "reports/" + res.report;
        this.isPdf = true;
        this.notPdf = false;

        if (res) {
          this.selectReport = res.report;
        }
      });
  }

  back() {
    this.isPdf = false;
    this.notPdf = true;
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      // attachment left

      this.openAttachment = true;
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  removeAttachment(removeitem, index) {
    this.closeAttachment = false;
    if (removeitem.new == true) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
      if (idx != -1) this.multiFile.splice(idx, 1);
    } else {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      console.log("this.attachedFiles", this.attachedFiles);
      let payload = {
        directory: "uploads/fieldWork",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            if (response) {
              this.fetchedData.closingMeeting.acm_attachments = [];
              this.attachedFiles.forEach((ele) => {
                this.fetchedData.closingMeeting.acm_attachments.push(
                  ele.fullName
                );
              });

              this.title = "Success";
              (this.content = "Attachment Deleted Successfully"),
                this.myCommonAlert(this.title, this.content);
            }
          }
        });
    }
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.closingMeeting.acm_attachments) {
        this.fetchedData.closingMeeting.acm_attachments.push(data[i]["name"]);
      } else {
        this.fetchedData.closingMeeting.acm_attachments = [];
        this.fetchedData.closingMeeting.acm_attachments.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.closingMeeting.acm_attachments;
    this.closingMeetingForm.patchValue({ acm_attachments: arr });
    console.log("filearray", arr.toString());

    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "closing-meeting",
      tab: "closing-meeting",
      "router-link": "/rcm/internal-audit/engagement-planning",

      file: filearr.toString(),

      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: this.configService.config.db,
    };

    console.log("check", payload);
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        if (files) {
          console.log("Response", files);
        }
        this.multiFile = [];
      });

    if (formData) {
      this.httpservice
        .securePost("uploadFieldWork", formData)
        .subscribe((files) => {
          this.multiFile = [];
        });
    }
  }

  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
    let payload = {
      database: this.configService.config.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        //  console.log('Uploaded');
        if (files) {
          console.log("closing Response", files);
        }
        this.multiFile = [];
      });
  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }

  initForm() {
    this.closingMeetingForm = this.formBuilder.group({
      auditId:["",[]],
      agenda:["", []],
      quesAndAns: ["", []],
      issueActionReview: ["", []],
      comments:["",[]],
      sentOn:["", []],
      preparedBy: ["", []],
      sentBy: ["", []],
      isCompleted: ["", []],
      attachments:["",[]]
      // auditName: ["", []],
      // closing_meeting: [{ value: "", disabled: true }, []],
      // opening_meeting: [{ value: "", disabled: true }, []],
      // update_meeting: [{ value: "", disabled: true }, []],
      // year: ["", []],
      // period_in_scope: [{ value: "", disabled: true }, []],
      // performance_period: [{ value: "", disabled: true }, []],
      // master_audit_id: ["", []],
      // acm_id: ["", []],
      // acm_name: ["", []],
      // acm_agenda: ["", []],
      // acm_issue_action_review: ["", []],
      // acm_qa: ["", []],
      // auditId: ["", []],
      // acm_audit_team: ["", []],
      // acm_comment: ["", []],
      // acm_attachments: ["", []],
      // acm_created_by: ["", []],
      // acm_updated_by: ["", []],
      // CMisSet: [{ value: true }, []],
      // dup_acm_Flag: ["", []],
    });
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    // this.upload();

    if (this.selectAudit) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      this.closingMeetingForm.patchValue({
        auditId: this.selectAudit,
        isCompleted:true,
        attachments:this.attachedFiles
      
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveClosingMeetingFormData, this.closingMeetingForm.value)
            .subscribe((res: any) => {
              if (res) {
                this.myCommonAlert(res.message, 'Message');
                this.epService.refresh(true);
                this.closeModal();
              }
            },(error)=>{
              this.myCommonAlert(error.message, 'Error');
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    
  }

  // onSave() {
  //   const initialState = {
  //     title: "Confirmation",
  //     content: `Do you want to save current changes?`,
  //     link: "Confirm",
  //     action: "Cancel",
  //     confirmFlag: false,
  //   };
  //   this.modalRef = this.modalService.show(ConfirmPopupComponent, {
  //     initialState,
  //     class: "success-class",
  //     backdrop: "static",
  //     keyboard: false,
  //   });
  //   this.upload();

  //   if (this.selectAudit) {
  //     var arr = [];

  //     let currentUser = localStorage.getItem("username");
  //     this.closingMeetingForm.patchValue({
  //       auditId: this.selectAudit,
  //       acm_created_by: currentUser,
  //       acm_id: this.acmId,
  //       dup_acm_Flag: false,
  //     });

  //     this.modalRef.content.onClose.subscribe((r) => {
  //       if (r) {
  //         this.httpservice
  //           .securePost(
  //             ConstUrls.saveCloseMeetData,
  //             this.closingMeetingForm.value
  //           )
  //           .subscribe((res: any) => {
  //             if (res) {
  //               this.title = "Success";
  //               this.content = "CloseMeeting updated successfully...";
  //               this.myCommonAlert(this.title, this.content);
  //               var payload = { closeMeeting: this.count + 1 };
  //               this.httpservice
  //                 .securePost(ConstUrls.updateCountData, payload)
  //                 .subscribe((result) => {
  //                   if (result) {
  //                     this.count = this.count + 1;
  //                     this.memo.setValueann(this.count);
  //                   }
  //                 });
  //             }
  //           });
  //       }
  //     });
  //   } else {
  //     this.title = "Error";
  //     this.content = "Failed to Save...";
  //     this.myCommonAlert(this.title, this.content);
  //   }

  //   this.closeModal();
  // }

  closeModal() {
    this.closedCMForm.emit(true);

    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  // print click added by Abu - 16-09-2020 start
  print(){
    window.print();
  }
 // print click added by Abu - 16-09-2020 end
 // search functionlity added by Abu - 18-09-2020 start 
 addnewFile() {
  this.editsearchDocument = true;
}
getSelectedDocs() {
  // this.searchDocument = false;
  var fileList = this.selectedItems.map((item) => {
      return item["fileName"];
    });
    console.log("Selected files: ", this.selectedItems);
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i]['metaData']=`Module-Name=EngagementPlanning,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
      // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
      this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
      this.selectedItems[i]['userType'] = 'auditor';
      this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
      this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
      this.selectedItems[i]['roomType']='';
      this.selectedItems[i]['routerLink']='';
      this.selectedItems[i]['subModuleName']='closing-meeting';
      this.selectedItems[i]['auditYear']=new Date().getFullYear();
      this.selectedItems[i]['auditId']=this.selectAudit;
      this.selectedItems[i]['auditName']='name';
      this.selectedItems[i]['moduleName']='EngagementPlanning';
      this.selectedItems[i]['checkedinDate']= moment().format("L");
      
    }
    console.log('Selected Item ARRAY: ',this.selectedItems);
    let payloads ={items:this.selectedItems};
    this.httpservice.securePost(ConstUrls.DmsSaveAsDocument , payloads).subscribe((res:any)=>{
      if(res){
        console.log('res', res);
        for (let i = 0; i < res.length; i++) {
          console.log('filename', res[i].fileName)
           let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
          // res[i].fileName.substr(res[i].fileName.lastIndexOf('.') + 1)
        this.attachedFiles.push({documentName:res[i].fileName , documentNo:res[i].documentNo
           ,documentUrlToken:res[i].urlToken , extension:extn});
        console.log('res[i].fileName', res[i].fileName)
          
        }
        console.log('attched filess array:' ,this.attachedFiles)
        this.myCommonAlert('File Uploaded successfully', 'Message');
      }
    },(error)=>{
      this.myCommonAlert(error.massage, 'Error');
      
    })
    // let payload = {
    //   auditId: this.auditId, username: localStorage.getItem("username"),
    //   userType: "auditor",
    //   fileList: fileList,
    // };
    // console.log("Payload: ", payload);
    
    // this.selectedItems.forEach((f,i) => {
    //   this.workstepForm.workpapers.push(
    //     { workpaperId: f.documentNo, workpaperName: f.fileName }
    //   )
    // })

  this.editsearchDocument = false;
}
closeSearch() {
  this.editsearchDocument = false;
}

// search functionlity added by Abu - 18-09-2020 end 
}