import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosingMeetingPopupComponent } from './closing-meeting-popup.component';

describe('ClosingMeetingPopupComponent', () => {
  let component: ClosingMeetingPopupComponent;
  let fixture: ComponentFixture<ClosingMeetingPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClosingMeetingPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosingMeetingPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
