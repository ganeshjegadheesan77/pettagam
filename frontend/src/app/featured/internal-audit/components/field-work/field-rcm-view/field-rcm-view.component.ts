import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { MemoService } from '../memo.service'
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";

@Component({
  selector: "app-field-rcm-view",
  templateUrl: "./field-rcm-view.component.html",
  styleUrls: ["./field-rcm-view.component.less"],
})
export class FieldRcmViewComponent implements OnInit {
  modalRef: BsModalRef;
  noOfAudits: any;
  memoCount: any = 0;
  omCount: any = 0;
  annCount: any = 0
  getCurrentYearAudits: any;
  auditId: string;
  auditList: [];
  auditInfo: any;
  isAuditSelected: Boolean = false;
  progressBarConfig: any = {};
  progressSettings: any = {
    preparation: {},
    fieldwork: {},
    reporting: {},
    review: {},
    complete: {}
  }
  sharedFlagObj: any = { 
    disableAuditDropDown: false 
  };

  constructor(
    private httpservice: HttpService,
    private memo: MemoService,
    private modalService: BsModalService
  ) {
    this.memo.getValue().subscribe(data => {
      this.memoCount = data;
    });
    this.memo.getValueann().subscribe(data => {
      this.annCount = data;
    })
    this.memo.getValueom().subscribe(data => {
      this.omCount = data;
    })
    this.memo.sharedFlagObj.subscribe(obj => {
      this.sharedFlagObj.disableAuditDropDown = obj.disableAuditDropDown
    });
  }

  ngOnInit() {
    // this.httpservice
    //   .securePost(ConstUrls.auditCount, payload)
    //   .subscribe((res: any) => {
    //     this.noOfAudits = res;
    //     // if (res[0]) {
    //     //   this.noOfAudits = res[0]["year"];
    //     // } else {
    //     //   this.noOfAudits = "0";
    //     // }
    //     // if (res[0]) {
    //     //   this.noOfAudits = res[0]["year"];
    //     // } else {
    //     //   this.noOfAudits = "0";
    //     // }
    //   });

    this.memo.refreshPageObserbable.subscribe((flag: Boolean) => {
      if (flag) {
        this.getAuditInfo(this.auditId);
      }
    });
    this.getProgressBarConfiguration();
    this.getCurentYearAudit();
  }

  getCurentYearAudit() {
    this.httpservice
      .secureGet(ConstUrls.getCurrentYearAuditList)
      .subscribe(
        (res: any) => {
          this.auditList = res.data;
        },
        (err: any) => {
          this.myCommonAlert('Error', err.error.message);
        }
      );
  }
  getProgressBarConfiguration() {
    this.httpservice
      .secureGet(ConstUrls.getProgressBarConfiguration)
      .subscribe(
        (res: any) => {
          this.progressBarConfig = res.data;
        },
        (err: any) => {
          this.myCommonAlert('Error', err.error.message);
        }
      );
  }
  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design",
      backdrop: "static",
      keyboard: false,
    });
  }

  getAuditInfo(auditId) {
    this.auditId = auditId;
    this.memo.setAuditId(auditId);
    if (auditId) {
      let payload = { auditId };
      this.httpservice
        .secureGet(ConstUrls.getSelectedAuditInfo, payload)
        .subscribe(
          (res: any) => {
            this.auditInfo = res.data;
            this.setUpProgressBar(this.auditInfo.isCompleted)
            this.isAuditSelected = true;
          },
          (err: any) => {
            this.isAuditSelected = false;
            this.myCommonAlert('Error', err.error.message);
          }
        );
    } else {
      this.isAuditSelected = false;
      this.auditInfo = {
        auditTeam: [],
        openingMeetingDate: '',
        updateMeetingDate: '',
        closingMeetingDate: '',
        finalMeetingDate: ''
      }
      this.progressSettings = {
        preparation: {},
        fieldwork: {},
        reporting: {},
        review: {},
        complete: {}
      }
    }
  }

  setUpProgressBar(isCompleteObj) {
    // let config = JSON.parse(JSON.stringify());
    let config = this.progressBarConfig;
    let progressSettings = {};
    for (let i = 0; i < config.length; i++) {
      let criteria = config[i].criteria;
      let doneFlag = false, notDoneFlag = false;
      for (let j = 0; j < criteria.length; j++) {
        if (isCompleteObj[criteria[j]]) {
          doneFlag = true;
        } else {
          notDoneFlag = true;
        }

      }
      if (doneFlag == true && notDoneFlag == false) {
        progressSettings[config[i].step] = {
          done: true
        }
      } else if (doneFlag == false && notDoneFlag == true) {
        progressSettings[config[i].step] = {}
      } else if (doneFlag == true && notDoneFlag == true) {
        progressSettings[config[i].step] = {
          active: true
        }
      } else if (doneFlag == false && notDoneFlag == false) {
        progressSettings[config[i].step] = {}
      }
      if('done' in progressSettings[config[i].step]) {
        if(progressSettings[config[i].step] == 'fieldwork') {
          if(this.auditInfo.startDate.length ) {
            this.sharedFlagObj[config[i].step] = true;
          } else {
            this.sharedFlagObj[config[i].step] = false;
          }
        } else {
          this.sharedFlagObj[config[i].step] = true;
        }
        
      } else if('active' in progressSettings[config[i].step]) {
        this.sharedFlagObj[config[i].step] = false
      } else {
        this.sharedFlagObj[config[i].step] = false;
      }
    }
    this.memo.setsharedFlagObj(this.sharedFlagObj);
    this.progressSettings = progressSettings;
  }
}
