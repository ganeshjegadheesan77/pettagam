import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldRcmViewComponent } from './field-rcm-view.component';

describe('FieldRcmViewComponent', () => {
  let component: FieldRcmViewComponent;
  let fixture: ComponentFixture<FieldRcmViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldRcmViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldRcmViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
