import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamTimelineFormComponent } from './team-timeline-form.component';

describe('TeamTimelineFormComponent', () => {
  let component: TeamTimelineFormComponent;
  let fixture: ComponentFixture<TeamTimelineFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamTimelineFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamTimelineFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
