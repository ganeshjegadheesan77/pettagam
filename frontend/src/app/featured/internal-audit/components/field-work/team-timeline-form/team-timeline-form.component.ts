import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpService } from "src/app/core/http-services/http.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConstUrls } from "src/app/config/const-urls";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { FormBuilder, FormGroup } from "@angular/forms";
import { MemoService } from "../memo.service";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import * as moment from "moment";

@Component({
  selector: 'app-team-timeline-form',
  templateUrl: './team-timeline-form.component.html',
  styleUrls: ['./team-timeline-form.component.less']
})
export class TeamTimelineFormComponent implements OnInit {
  @Output() closedTeamAndTimelineForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalRef: BsModalRef;
  autoComplete = false;
  autoCompleteTwo = false;
  autoCompleteThree = false;
  autoCompleteFour = false;
  btnFirst = true;
  btnSecound = false;
  btnThree = true;
  btnFour = false;
  btnFive = true;
  btnSix = false;
  btnSeven = true;
  btnEight = false;
  auditId: String;
  auditTeam: any; //type object
  auditorList = [];
  auditLeadList = [];
  auditeeList = [];
  pbcContactList = [];
  keyword = 'name'; // for auto complete
  teamAndTimeLineForm: FormGroup;;
  minDate: Date;
  modalRef2;
  constructor(
    private httpservice: HttpService,
    private mScrollbarService: MalihuScrollbarService,
    private modalService: BsModalService,
    private epService: MemoService,
    private formBuilder: FormBuilder,
  ) {
    this.minDate = new Date();
  }
  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.epService.auditId.subscribe((auditId) => {
      if (auditId) {
        this.auditId = auditId;
        this.getTeamData(auditId);
      }
    });
  }
  auditTeamValidation(e) {
    let msg = '';
    switch (e.target.name) {
      case 'auditLead':
        msg = this.auditTeam.auditleadList.length ? '' : "AuditLead not found."
        break;
      case 'auditor':
        msg = this.auditTeam.auditorList.length ? '' : "Auditor not found."
        break;
      case 'auditee':
        msg = this.auditTeam.auditeeList.length ? '' : "Auditee not found."
        break;
      case 'pbcContact':
        msg = this.auditTeam.pbcContact.length ? '' : "PBC contact not found."
        break;
    }
    if (msg) {
      this.myCommonAlert('Error', msg);
    }
  }
  getTeamData(auditId) {
    let _this = this;
    this.httpservice
      .securePost(ConstUrls.getTeamAndTimeLine, { auditId: auditId })
      .subscribe(
        (res: any) => {
          let teamAndTimeLine = res.data[0];
          console.log('*****teamAndTimeLine', teamAndTimeLine)
          this.teamAndTimeLineForm.patchValue(teamAndTimeLine.teamAndTimeline);
          this.teamAndTimeLineForm.patchValue({ auditId: _this.auditId });
          this.adjustDates(teamAndTimeLine.teamAndTimeline);
          this.auditLeadList = teamAndTimeLine.teamAndTimeline.auditLead;
          this.auditorList = teamAndTimeLine.teamAndTimeline.auditors;
          this.pbcContactList = teamAndTimeLine.teamAndTimeline.pbc;
          this.auditeeList = teamAndTimeLine.teamAndTimeline.auditees;
          let payload = {
            auditableEntity: teamAndTimeLine.auditableEntity,
            businessProcess: teamAndTimeLine.category.businessProcess,
          }
          _this.httpservice
            .secureGet(ConstUrls.auditTeamData, payload)
            .subscribe(
              (res: any) => {
                this.auditTeam = res.data;
              },
              (err: any) => {
                this.myCommonAlert('Error', err.error.message);
              }
            );
        },
        (err: any) => {
          this.myCommonAlert('Error', err.error.message);
        }
      );
  }
  blockInput(event) {
    event.preventDefault();
  }

  adjustDates(data) {
    let myArr = [];
    let per_period = [];
    if (data.periodInScope.length != 0) {
      if (!moment(data.periodInScope[0], "DD-MM-YYYY", true).isValid()) {
        myArr[0] = moment(data.periodInScope[0]).format("DD-MM-YYYY");
        myArr[1] = moment(data.periodInScope[1]).format("DD-MM-YYYY");
        this.teamAndTimeLineForm.patchValue({ periodInScope: myArr });
      }
    }

    if (data.updateMeetingDate2) {
      // if (!moment(data.performancePeriod[0], "DD-MM-YYYY", true).isValid()) {
      //   per_period[0] = moment(data.performancePeriod[0]).format("DD-MM-YYYY");
      //   per_period[1] = moment(data.performancePeriod[1]).format("DD-MM-YYYY");
      //   this.teamAndTimeLineForm.patchValue({ performancePeriod: per_period });
      // }
      if (!moment(data.updateMeetingDate2, "DD-MM-YYYY", true).isValid()) {
        let um2 = moment(data.updateMeetingDate2).format("DD-MM-YYYY");

        this.teamAndTimeLineForm.patchValue({ updateMeeting2: um2 });
      }
    }

    if (data.openingMeetingDate) {
      if (!moment(data.openingMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.openingMeetingDate).format("DD-MM-YYYY");

        this.teamAndTimeLineForm.patchValue({ openingMeetingDate: op });
      }
    }

    if (data.updateMeetingDate) {
      if (!moment(data.updateMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.updateMeetingDate).format("DD-MM-YYYY");

        this.teamAndTimeLineForm.patchValue({ updateMeetingDate: op });
      }
    }

    if (data.closingMeetingDate) {
      if (!moment(data.closingMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.closingMeetingDate).format("DD-MM-YYYY");

        this.teamAndTimeLineForm.patchValue({ closingMeetingDate: op });
      }
    }

    if (data.finalMeetingDate) {
      if (!moment(data.finalMeetingDate, "DD-MM-YYYY", true).isValid()) {
        let op = moment(data.finalMeetingDate).format("DD-MM-YYYY");
        this.teamAndTimeLineForm.patchValue({ finalMeetingDate: op });
      }
    }
  }
  myCommonAlert(title, content, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }

  closeModal(popupFlag) {
    if (popupFlag) {
      const initialState = {
        title: "Confirmation",
        content: `Do you want to close current form ?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef2 = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef2.content.onClose.subscribe((r) => {
        if (r) {
          $(".scroll-container").scrollTop(0);
          $('.pop-up-form').addClass('d-none');
          this.closedTeamAndTimelineForm.emit(true);
        }
      })
    } else {
      $(".scroll-container").scrollTop(0);
      $('.pop-up-form').addClass('d-none');
      this.closedTeamAndTimelineForm.emit(true);
    }

  }
  removeOnClick(name: String, role: String) {
    switch (role) {
      case 'auditLead':
        this.auditLeadList = this.auditLeadList.filter((_name) => name != _name);
        this.auditTeam.auditleadList.push({ "name": name });
        this.autocompleteClickTwo();
        break;
      case 'auditor':
        this.auditorList = this.auditorList.filter((_name) => name != _name);
        this.auditTeam.auditorList.push({ "name": name });
        this.autocompleteClickFour();
        break;
      case 'auditee':
        this.auditeeList = this.auditeeList.filter((_name) => name != _name)
        this.auditTeam.auditeeList.push({ "name": name });
        this.autocompleteClickEight();
        break;
      case 'pbcContact':
        this.pbcContactList = this.pbcContactList.filter((_name) => name != _name);
        this.auditTeam.pbcContact.push({ "name": name });
        this.autocompleteClickSix();
        break;
    }
  }

  selectEventAuditLeadList(item) {
    this.auditLeadList.push(item.name);
    this.auditTeam.auditleadList = this.auditTeam.auditleadList.filter((obj) => {
      return (item.name != obj.name)
    })
    this.autocompleteClickTwo();
  }

  selectEventAuditor(item) {
    this.auditorList.push(item.name);
    this.auditTeam.auditorList = this.auditTeam.auditorList.filter((obj) => {
      return (item.name != obj.name)
    })
    this.autocompleteClickFour();
  }
  selectEventPbcContact(item) {
    this.pbcContactList.push(item.name);
    this.auditTeam.pbcContact = this.auditTeam.pbcContact.filter((obj) => {
      return (item.name != obj.name)
    })
    this.autocompleteClickSix();
  }

  selectEventAuditeeList(item) {
    this.auditeeList.push(item.name);
    this.auditTeam.auditeeList = this.auditTeam.auditeeList.filter((obj) => {
      return (item.name != obj.name)
    })
    this.autocompleteClickEight();
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
  autocompleteClick() {
    this.autoComplete = true;

    this.btnFirst = false;
    this.btnSecound = true;
  }
  autocompleteClickTwo() {
    this.autoComplete = false;

    this.btnFirst = true;
    this.btnSecound = false;
  }

  autocompleteClickThree() {
    this.autoCompleteTwo = true;

    this.btnThree = false;
    this.btnFour = true;
  }
  autocompleteClickFour() {
    this.autoCompleteTwo = false;

    this.btnThree = true;
    this.btnFour = false;
  }

  autocompleteClickFive() {
    this.autoCompleteThree = true;

    this.btnFive = false;
    this.btnSix = true;
  }
  autocompleteClickSix() {
    this.autoCompleteThree = false;

    this.btnFive = true;
    this.btnSix = false;
  }
  autocompleteClickSeven() {
    this.autoCompleteFour = true;

    this.btnSeven = false;
    this.btnEight = true;
  }
  autocompleteClickEight() {
    this.autoCompleteFour = false;

    this.btnSeven = true;
    this.btnEight = false;
  }

  printTeamTimeline() {
    window.print();
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        // let performance1 = moment(this.teamAndTimeLineForm.controls['performancePeriod'].value[0], "DD-MM-YYYY").format();
        // let performance2 = moment(this.teamAndTimeLineForm.controls['performancePeriod'].value[1], "DD-MM-YYYY").format();
        let updateMeeting2 = moment(this.teamAndTimeLineForm.controls['updateMeeting2'].value, "DD-MM-YYYY").format();

        let periodInScope1 = moment(this.teamAndTimeLineForm.controls['periodInScope'].value[0], "DD-MM-YYYY").format();
        let periodInScope2 = moment(this.teamAndTimeLineForm.controls['periodInScope'].value[1], "DD-MM-YYYY").format()

        let finalMeetingDate = moment(this.teamAndTimeLineForm.controls['finalMeetingDate'].value, "DD-MM-YYYY").format();
        let openingMeetingDate = moment(this.teamAndTimeLineForm.controls['openingMeetingDate'].value, "DD-MM-YYYY").format();
        let closingMeetingDate = moment(this.teamAndTimeLineForm.controls['closingMeetingDate'].value, "DD-MM-YYYY").format();
        let updateMeetingDate = moment(this.teamAndTimeLineForm.controls['updateMeetingDate'].value, "DD-MM-YYYY").format();

        if (updateMeeting2 == 'Invalid date' ||
          periodInScope1 == 'Invalid date' ||
          finalMeetingDate == 'Invalid date' ||
          openingMeetingDate == 'Invalid date' ||
          closingMeetingDate == 'Invalid date' ||
          updateMeetingDate == 'Invalid date') {
          this.myCommonAlert('Error', 'Please select all the dates');
        } else {
          this.teamAndTimeLineForm.patchValue({
            updateMeeting2: updateMeeting2,
            periodInScope: [periodInScope1, periodInScope2],
            finalMeetingDate: finalMeetingDate,
            openingMeetingDate: openingMeetingDate,
            closingMeetingDate: closingMeetingDate,
            updateMeetingDate: updateMeetingDate,
            auditLead: this.auditLeadList,
            auditors: this.auditorList,
            pbc: this.pbcContactList,
            auditees: this.auditeeList,
            isCompleted: true
          });

          this.httpservice.securePost(ConstUrls.saveTeamAndTimeLine, this.teamAndTimeLineForm.value).
            subscribe((res: any) => {
              if (res) {
                this.closeModal(false);
                this.epService.refresh(true);
                this.myCommonAlert('Success', res.message, "success");
              }
            }, (error) => {
              console.log("error while saving form team and timeline", error);
              this.myCommonAlert('Error', error.error.message);

            })

          console.log('FORM VALUES', this.teamAndTimeLineForm.value);
        }
      }
    })



    //    console.log('formatedDate', formatedDate);
    // console.log('FORM DATA', this.teamAndTimeLineForm.value);
  }

  // compareDates(){
  //   let performance1 = moment(this.teamAndTimeLineForm.controls['performancePeriod'].value[0]);
  //   let performance2 = moment(this.teamAndTimeLineForm.controls['performancePeriod'].value[1])

  //   const dateIsAfter = moment('2014-03-24T01:15:00.000Z').isAfter(moment(this.teamAndTimeLineForm.controls['finalMeetingDate'].value));
  // }

  initForm() {
    this.teamAndTimeLineForm = this.formBuilder.group({
      auditId: ["", []],
      openingMeetingDate: ["", []],
      closingMeetingDate: ["", []],
      periodInScope: [[], []],
      updateMeeting2: ["", []],
      finalMeetingDate: ["", []],
      updateMeetingDate: ["", []],
      auditLead: ["", []],
      auditors: ["", []],
      pbc: ["", []],
      auditees: ["", []],
      isCompleted: ["", []],
    });
  }

}