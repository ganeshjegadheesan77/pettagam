import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditAnnouncementPopupComponent } from './audit-announcement-popup.component';

describe('AuditAnnouncementPopupComponent', () => {
  let component: AuditAnnouncementPopupComponent;
  let fixture: ComponentFixture<AuditAnnouncementPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditAnnouncementPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditAnnouncementPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
