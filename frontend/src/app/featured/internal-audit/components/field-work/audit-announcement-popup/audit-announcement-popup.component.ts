import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { InterviewSchedulePopupComponent } from "src/app/layout/modal/interview-schedule-popup/interview-schedule-popup.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import { MemoService } from "../memo.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
// import { environment } from "src/environments/environment";
import { HttpService } from "src/app/core/http-services/http.service";
import { Router } from "@angular/router";
import { ConfigService } from 'src/config.service';
declare var $;

@Component({
  selector: "app-audit-announcement-popup",
  templateUrl: "./audit-announcement-popup.component.html",
  styleUrls: ["./audit-announcement-popup.component.less"],
})
export class AuditAnnouncementPopupComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  @Output() closedAnnouncementForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();

  auditAnnouncementForm: FormGroup;
  position;
  modalRef: BsModalRef;
  copiedAuditData: any;
  aaId: any;
  uploadedFilesArr: any;
  fetchedData: any;
  uri;
  multiFile: any = [];
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  disableSearch = false;
  isAttachment: boolean;
  fieldPanes: any;
  deletedAttachment: any = [];
  auditName: any;
  newAnnouncementForm: boolean;
  auditId: any;
  count: number;
  title: string;
  content: string;
  closeAttachment: boolean;
  pAssignee: string[] = [];
  papAssignee: string[] = [];
  // assign search  added by Abu - 18-09-2020 start
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  editsearchDocument: boolean = false;
  auditTeam: any[];
  openingMeetingDate: string;
  closingMeetingDate: string;
  periodInScope: string;
  arr: any[];
  globalSearchData: any;
  companyName: any;
  location: any;
  buName: any;
  bProcessName: any;
  modalRef2;
  isCompleted: boolean;
  // assign search  added by Abu - 18-09-2020 end
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private epService: MemoService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private router: Router,
    private configService: ConfigService

  ) {
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {


    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.epService.auditId.subscribe((auditId) => {
      if (auditId) {
        this.auditId = auditId;
        this.getAuditAnnouncement(this.auditId);
      }
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      _id: "_id",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 10,
      allowSearchFilter: true,
    };


    // this.memo.getValueann().subscribe((data) => {
    //   this.count = data;
    // });
    // this.fetchedData = [];

    // this.isAttachment=false;
    // this.openAttachment = true;
    //   this.title = "";
    //   this.content = "";
    //   this.getAnnouncementData();
    //   // this.auditAnnouncementForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
    //   this.memo.getValueann().subscribe((data) => {
    //     this.count = data;
    //   });
    //  this.dataTable();


  }

  getAuditAnnouncement(auditId) {
    this.selectAudit = auditId;
    let payload = { auditId: this.selectAudit };
    this.httpservice.securePost(ConstUrls.getAuditAnnouncementdData, payload).subscribe((res: any) => {
      if (res) {
        console.log('**** RESPONSE:', res)
        let announcementData = res.data[0];
        this.auditAnnouncementForm.patchValue(announcementData.auditAnnouncement);
        this.companyName = announcementData.category.levelOne.data;
        this.location = announcementData.category.levelTwo.data;
        this.buName = announcementData.category.levelThree.data;
        this.bProcessName = announcementData.category.businessProcess;
        this.isCompleted = announcementData.auditAnnouncement.isCompleted;
        if (announcementData.auditAnnouncement.attachments.length > 0) {
          for (let i = 0; i < announcementData.auditAnnouncement.attachments.length; i++) {
            console.log('filename: ', announcementData.auditAnnouncement.attachments[i].documentName);
            let extn = announcementData.auditAnnouncement.attachments[i].documentName.substr(announcementData.auditAnnouncement.attachments[i].documentName.lastIndexOf(".") + 1);
            this.attachedFiles.push({
              documentName: announcementData.auditAnnouncement.attachments[i].documentName,
              documentNo: announcementData.auditAnnouncement.attachments[i].documentNo
              , documentUrlToken: announcementData.auditAnnouncement.attachments[i].documentUrlToken,
              extension: extn
            });
          }
        }
        this.openingMeetingDate = announcementData.teamAndTimeline.openingMeetingDate ? moment(announcementData.teamAndTimeline.openingMeetingDate).format("DD-MM-YYYY") : '';
        this.closingMeetingDate = announcementData.teamAndTimeline.closingMeetingDate ? moment(announcementData.teamAndTimeline.closingMeetingDate).format("DD-MM-YYYY") : '';
        this.periodInScope = announcementData.teamAndTimeline.periodInScope && announcementData.teamAndTimeline.periodInScope.length ? moment(announcementData.teamAndTimeline.periodInScope[0]).format("DD-MM-YYYY") + '-' + moment(announcementData.teamAndTimeline.periodInScope[1]).format("DD-MM-YYYY") : '';
        if (announcementData.teamAndTimeline.auditLead || announcementData.teamAndTimeline.auditors) {
          this.auditTeam = [...announcementData.teamAndTimeline.auditLead, ...announcementData.teamAndTimeline.auditors];
          console.log('auditTeam', this.auditTeam)
        }

      }
    }, (error) => {
      this.myCommonAlert(error.error.message, 'Error')
    })
  }

  myCommonAlert(content, title, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }

  // getAnnouncementData() {
  //   this.attachedFiles = [];

  //   let payload = { auditId: this.selectAudit };
  //   this.httpservice
  //     .securePost(ConstUrls.getAnnouncementData, payload)
  //     .subscribe(async (res: any) => {
  //       this.fetchedData = res;

  //       this.fieldPanes = await res;
  //       console.log("res", res);
  //       this.initForm();
  //       this.auditAnnouncementForm.patchValue(res);
  //       if (
  //         !moment(
  //           this.fieldPanes.periodInScope[0],
  //           "DD-MM-YYYY",
  //           true
  //         ).isValid() &&
  //         this.fieldPanes.periodInScope.length > 0
  //       ) {
  //         let d1 = moment(this.fieldPanes.periodInScope[0]).format(
  //           "DD-MM-YYYY"
  //         );
  //         let d2 = moment(this.fieldPanes.periodInScope[1]).format(
  //           "DD-MM-YYYY"
  //         );
  //         let str = d1 + "-" + d2;
  //         this.auditAnnouncementForm.patchValue({ period_in_scope: str });
  //       } else if (this.fieldPanes.periodInScope.length == 0) {
  //         this.auditAnnouncementForm.patchValue({
  //           period_in_scope: this.fieldPanes.periodInScope,
  //         });
  //       } else {
  //         let d1 = this.fieldPanes.periodInScope[0];
  //         let d2 = this.fieldPanes.periodInScope[1];
  //         let str = d1 + "-" + d2;
  //         this.auditAnnouncementForm.patchValue({ period_in_scope: str });
  //       }

  //       if (
  //         !moment(
  //           this.fieldPanes.performancePeriod[0],
  //           "DD-MM-YYYY",
  //           true
  //         ).isValid() &&
  //         this.fieldPanes.performancePeriod.length > 0
  //       ) {
  //         let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
  //           "DD-MM-YYYY"
  //         );
  //         let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
  //           "DD-MM-YYYY"
  //         );
  //         let str = d1 + "-" + d2;
  //         this.auditAnnouncementForm.patchValue({ performance_period: str });
  //       } else if (this.fieldPanes.performancePeriod.length == 0) {
  //         this.auditAnnouncementForm.patchValue({
  //           performance_period: this.fieldPanes.performancePeriod,
  //         });
  //       } else {
  //         let d1 = this.fieldPanes.performancePeriod[0];
  //         let d2 = this.fieldPanes.performancePeriod[1];
  //         let str = d1 + "-" + d2;
  //         this.auditAnnouncementForm.patchValue({ performance_period: str });
  //       }

  //       if (
  //         !moment(
  //           this.fieldPanes.openingMeetingDate,
  //           "DD-MM-YYYY",
  //           true
  //         ).isValid() &&
  //         this.fieldPanes.openingMeetingDate
  //       ) {
  //         let d1 = moment(this.fieldPanes.openingMeetingDate).format(
  //           "DD-MM-YYYY"
  //         );
  //         this.auditAnnouncementForm.patchValue({ opening_meeting: d1 });
  //       } else if (!this.fieldPanes.openingMeetingDate) {
  //         this.auditAnnouncementForm.patchValue({
  //           opening_meeting: this.fieldPanes.openingMeetingDate,
  //         });
  //       } else {
  //         let d1 = this.fieldPanes.openingMeetingDate;
  //         this.auditAnnouncementForm.patchValue({ opening_meeting: d1 });
  //       }

  //       if (this.fetchedData.announcement.aa_attachments.length != 0) {
  //         this.openAttachment = true;
  //         for (
  //           let i = 0;
  //           i < this.fetchedData.announcement.aa_attachments.length;
  //           i++
  //         ) {
  //           let extn = this.fetchedData.announcement.aa_attachments[i].substr(
  //             this.fetchedData.announcement.aa_attachments[i].lastIndexOf(".") +
  //               1
  //           );
  //           let name = this.fetchedData.announcement.aa_attachments[i].substr(
  //             0,
  //             this.fetchedData.announcement.aa_attachments[i].lastIndexOf(".")
  //           );

  //           this.attachedFiles.push({
  //             relativePath: name,
  //             extension: extn,
  //             fullName: name + "." + extn,
  //             new: false,
  //             isDuplicate: this.fieldPanes.announcement.dup_aa_Flag,
  //           });
  //         }
  //       } else {
  //         this.openAttachment = false;
  //       }
  //       if (res) {
  //         if (res.announcement.AAisSet === true) {
  //           this.aaId = res.announcement.aa_id;
  //           this.auditAnnouncementForm.patchValue(res.announcement);
  //           this.auditName = this.fieldPanes.auditableEntity;
  //         } else {
  //           this.title = "Error";
  //           this.content = "Not Allowed ...";
  //           this.myCommonAlert(this.title, this.content);
  //           this.closeModal();
  //         }
  //       }
  //     });
  // }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      // attachment left
      this.openAttachment = true;
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);

          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
          console.log("ATTACHED FILES:", this.attachedFiles);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }
  removeAttachment(removeitem, index) {
    const fileindex = parseInt(index);
    this.attachedFiles.splice(fileindex, 1);
    console.log('@@@@@@---', this.attachedFiles);
    this.myCommonAlert("File removed successfully", "Success", 'success');
  }
  // removeAttachment(removeitem, index) {
  //   this.closeAttachment = false;
  //   if (removeitem.new == true) {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
  //     if (idx != -1) this.multiFile.splice(idx, 1);
  //   } else {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //     console.log("this.attachedFiles", this.attachedFiles);
  //     let payload = {
  //       directory: "uploads/fieldWork",
  //       fullName: removeitem.fullName,
  //     };
  //     this.httpservice
  //       .securePost("removeAttachment", payload)
  //       .subscribe((response: any) => {
  //         if (response) {
  //           if (response) {
  //             this.fetchedData.announcement.aa_attachments = [];

  //             this.attachedFiles.forEach((ele) => {
  //               this.fetchedData.announcement.aa_attachments.push(ele.fullName);
  //             });

  //             this.title = "Success";
  //             (this.content = "Attachment Deleted Successfully"),
  //               this.myCommonAlert(this.content, this.title);
  //           }
  //         }
  //       });
  //   }
  // }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    // console.log('this is data',data);return;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.announcement.aa_attachments) {
        this.fetchedData.announcement.aa_attachments.push(data[i]["name"]);
      } else {
        this.fetchedData.announcement.aa_attachments = [];
        this.fetchedData.announcement.aa_attachments.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.announcement.aa_attachments;
    this.auditAnnouncementForm.patchValue({ aa_attachments: arr });

    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "audit-announcement",
      tab: "audit-announcement",
      "router-link": "/rcm/internal-audit/engagement-planning",
      file: filearr.toString(),
      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: this.configService.config.db,
    };
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        if (files) {
          console.log("Response", files);
        }
        this.multiFile = [];
      });

    if (formData) {
      this.httpservice
        .securePost("uploadFieldWork", formData)
        .subscribe((files) => {
          this.multiFile = [];
        });
    }
  }

  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
    let payload = {
      database: this.configService.config.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        //  console.log('Uploaded');
        if (files) {
          console.log("audit announcement Response", files);
        }
        this.multiFile = [];
      });
  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    // this.upload();

    if (this.selectAudit) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      let capitalizedcurrentUser = currentUser.charAt(0).toUpperCase() + currentUser.slice(1);
      if (!this.isCompleted) {
        this.auditAnnouncementForm.patchValue({
          preparedBy: capitalizedcurrentUser
        });
      }
      this.auditAnnouncementForm.patchValue({
        auditId: this.selectAudit,
        isCompleted: true,
        attachments: this.attachedFiles
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.saveAuditAnnouncementData, this.auditAnnouncementForm.value)
            .subscribe((res: any) => {
              if (res) {
                this.closeModal(false);
                this.epService.refresh(true);
                this.myCommonAlert(res.message, 'Message', 'success');
              }
            }, (error) => {
              this.myCommonAlert(error.error.message, 'Error');
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.content, this.title);
    }


  }

  onFilechanged(event) {
    var files = [];
    console.log("%%%%%%%%%", event.target);
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      for (let i = 0; i < this.attachedFiles.length; i++) {
        if (this.attachedFiles[i]['documentName'] == files[0]["name"]) {
          this.myCommonAlert('File already exist...', 'Error');
          return
        }
      }
      console.log("fil anme", files);
      formData.append("file", files[0], files[0]["name"]);
      formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
      formData.append("userType", 'auditor');
      formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=EngagementPlanning,Feature-Name=auditAnnouncement,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
      formData.append("routerLink", ""),
        formData.append("subModuleName", "audit-announcement"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.selectAudit),
        formData.append("auditName", "name"),
        formData.append("moduleName", "EngagementPlanning"),
        formData.append("checkedinDate", moment().format("L"));
      console.log('****** formData', formData);
      this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res: any) => {
        if (res) {
          for (let i = 0; i < res.length; i++) {
            console.log('filename', res[i].fileName)
            let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            this.attachedFiles.push({
              documentName: res[i].fileName, documentNo: res[i].documentNo
              , documentUrlToken: res[i].urlToken, extension: extn
            });
            console.log('res[i].fileName', res[i].fileName)

          }
          console.log('attched filess array:', this.attachedFiles)
          console.log('******res', res)
          this.myCommonAlert('File uploaded successfully', 'Success', 'success');

        }
      }
        , (error) => {
          this.myCommonAlert(error.error.message, 'Error');

        })



    } else {
      this.myCommonAlert('Invalid file', 'Error');
      $("#myFile").val('');
    }
  }

  getSelectedDocs() {
    // this.searchDocument = false;
    if (!this.selectedItems.length) {
      this.myCommonAlert("Please select atleast a document.", "Alert");
      return;
    }
    var fileList = this.selectedItems.map((item) => {
      return item["fileName"];
    });
    console.log("Selected files: ", this.selectedItems);
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i]['metaData'] = `Module-Name=EngagementPlanning,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
      // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
      this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
      this.selectedItems[i]['userType'] = 'auditor';
      this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
      this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
      this.selectedItems[i]['roomType'] = '';
      this.selectedItems[i]['routerLink'] = '';
      this.selectedItems[i]['subModuleName'] = 'audit-announcement';
      this.selectedItems[i]['auditYear'] = new Date().getFullYear();
      this.selectedItems[i]['auditId'] = this.selectAudit;
      this.selectedItems[i]['auditName'] = 'name';
      this.selectedItems[i]['moduleName'] = 'EngagementPlanning';
      this.selectedItems[i]['checkedinDate'] = moment().format("L");

    }
    console.log('Selected Item ARRAY: ', this.selectedItems);
    let payloads = { items: this.selectedItems };
    this.httpservice.securePost(ConstUrls.DmsSaveAsDocument, payloads).subscribe((res: any) => {
      if (res) {
        console.log('res', res);
        for (let i = 0; i < res.length; i++) {
          console.log('filename', res[i].fileName)
          let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
          // res[i].fileName.substr(res[i].fileName.lastIndexOf('.') + 1)
          this.attachedFiles.push({
            documentName: res[i].fileName, documentNo: res[i].documentNo
            , documentUrlToken: res[i].urlToken, extension: extn
          });
          console.log('res[i].fileName', res[i].fileName)

        }
        console.log('attched filess array:', this.attachedFiles)
        this.myCommonAlert('File Uploaded successfully', 'Message', 'success');
      }
    }, (error) => {
      this.myCommonAlert(error.error.massage, 'Error');

    })
    // let payload = {
    //   auditId: this.auditId, username: localStorage.getItem("username"),
    //   userType: "auditor",
    //   fileList: fileList,
    // };
    // console.log("Payload: ", payload);

    // this.selectedItems.forEach((f,i) => {
    //   this.workstepForm.workpapers.push(
    //     { workpaperId: f.documentNo, workpaperName: f.fileName }
    //   )
    // })

    this.editsearchDocument = false;
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let fileNameArr = fileInput.split(".");
    let extension = '.' + fileNameArr[fileNameArr.length - 1];
    console.log('extension@@@', extension);
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }
  // onSave() {
  //   const initialState = {
  //     title: "Confirmation",
  //     content: `Do you want to save current changes?`,
  //     link: "Confirm",
  //     action: "Cancel",
  //     confirmFlag: false,
  //   };
  //   this.modalRef = this.modalService.show(ConfirmPopupComponent, {
  //     initialState,
  //     class: "success-class",
  //     backdrop: "static",
  //     keyboard: false,
  //   });
  //   this.upload();

  //   if (this.selectAudit) {
  //     var arr = [];

  //     let currentUser = localStorage.getItem("username");
  //     this.auditAnnouncementForm.patchValue({
  //       auditId: this.selectAudit,
  //       aa_created_by: currentUser,
  //       aa_id: this.aaId,
  //       dup_aa_Flag: false,
  //     });
  //     console.log(
  //       "this.auditAnnouncementForm",
  //       this.auditAnnouncementForm.value
  //     );

  //     this.modalRef.content.onClose.subscribe((r) => {
  //       if (r) {
  //         this.httpservice
  //           .securePost(
  //             ConstUrls.saveAnnouncementData,
  //             this.auditAnnouncementForm.value
  //           )
  //           .subscribe((res: any) => {
  //             if (res) {
  //               this.title = "Success";
  //               this.content = "Announcement updated successfully...";
  //               this.myCommonAlert(this.title, this.content);
  //               var payload = { announced: this.count + 1 };
  //               this.httpservice
  //                 .securePost(ConstUrls.updateCountData, payload)
  //                 .subscribe((response) => {
  //                   if (response) {
  //                     this.count = this.count + 1;
  //                     this.memo.setValueann(this.count);
  //                   }
  //                 });
  //             }
  //           });
  //       }
  //     });
  //   } else {
  //     this.title = "Error";
  //     this.content = "Failed to Save...";
  //     this.myCommonAlert(this.title, this.content);
  //   }

  //   this.closeModal();
  // }
  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185", searchBy: localStorage.getItem('username') };

      this.httpservice
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              // console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            console.log('dropdownListAll', this.dropdownListAll)
            this.dropdownFirmList = this.arr;
            // this.attachedFiles = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    // this.dropdownListAll = [
    //   { item_id: 1, item_text: 'Mumbaisssssssssss' },
    //   { item_id: 2, item_text: 'Bangaluru' },
    //   { item_id: 3, item_text: 'Pune' },
    //   { item_id: 4, item_text: 'Navsari' },
    //   { item_id: 5, item_text: 'New Delhi' },
    //   { item_id: 6, item_text: 'Mumbai' },
    //   { item_id: 7, item_text: 'Bangaluru' },
    //   { item_id: 8, item_text: 'Pune' },
    //   { item_id: 9, item_text: 'Navsari' },
    //   { item_id: 10, item_text: 'New Delhi' },
    //   { item_id: 11, item_text: 'Mumbai' },
    //   { item_id: 12, item_text: 'Bangaluru' },
    //   { item_id: 13, item_text: 'Pune' },
    //   { item_id: 14, item_text: 'Navsari' },
    //   { item_id: 15, item_text: 'New Delhi' }
    // ];
  }
  initForm() {
    this.auditAnnouncementForm = this.formBuilder.group({
      auditId: ["", []],
      text: ["", []],
      text2: ["", []],
      agenda: ["", []],
      closing: ["", []],
      sentOn: ["", []],
      preparedBy: ["", []],
      sentBy: ["", []],
      isCompleted: ["", []],
      attachments: ["", []]
      // _id: ["", []],
      // auditId: ["", []],
      // auditName: ["", []],
      // closing_meeting: [{ value: "", disabled: true }, []],
      // opening_meeting: [{ value: "", disabled: true }, []],
      // update_meeting: [{ value: "", disabled: true }, []],
      // year: ["", []],
      // period_in_scope: [{ value: "", disabled: true }, []],
      // performance_period: [{ value: "", disabled: true }, []],
      // master_audit_id: ["", []],
      // aa_id: ["", []],
      // aa_name: ["", []],
      // aa_text: ["", []],
      // aa_agenda: ["", []],
      // aa_text2: ["", []],
      // aa_closing: ["", []],
      // aa_attachments: ["", []],
      // aa_created_by: ["", []],
      // aa_updated_by: ["", []],
      // AAisSet: [{ value: true }, []],
      // dup_aa_Flag: ["", []],
      // approverStatus: [""],
      // assigneeStatus: ["assigned"],
    });
  }


  closeModal(popupFlag) {
    if (popupFlag) {
      const initialState = {
        title: "Confirmation",
        content: `Do you want to close current form ?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef2 = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef2.content.onClose.subscribe((r) => {
        if (r) {
          this.closedAnnouncementForm.emit(true);
          $(".scroll-container").scrollTop(0);
          $(".pop-up-form").addClass("d-none");
          this.position = this.position ? undefined : { x: 0, y: 0 };
        }
      })
    }
    else {
      this.closedAnnouncementForm.emit(true);
      $(".scroll-container").scrollTop(0);
      $(".pop-up-form").addClass("d-none");
      this.position = this.position ? undefined : { x: 0, y: 0 };
    }

  }
  dataTable() {
    $("#interveiwSchduleTableTwo").DataTable({
      processing: true,
      bPaginate: true,
      autoWidth: true,
      searching: false,
      destroy: true,
      fixedColumns: {
        leftColumns: 0,
        rightColumns: 1,
        heightMatch: "auto",
      },
      columnDefs: [{ targets: 6, orderable: false }],
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"],
      ],
      bInfo: true,
    });
  }

  addStepOne() {
    $("body").addClass("modalPopupOne");
    const initialState = {
      title: "Interview Schedule",
      link: "Submit",
      action: "Cancel",
    };
    this.modalRef = this.modalService.show(InterviewSchedulePopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

  }



  // print click added by Abu - 16-09-2020 start
  print() {
    window.print();
  }
  // print click added by Abu - 16-09-2020 end
  // search functionlity added by Abu - 18-09-2020 start 
  addnewFile() {
    this.editsearchDocument = true;
  }

  closeSearch() {
    this.editsearchDocument = false;
  }

  // search functionlity added by Abu - 18-09-2020 end 
}