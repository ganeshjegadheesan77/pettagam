import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class MemoService {
    // private dataDetail = new BehaviorSubject<any>([]);
    // private audit = new BehaviorSubject<any>([]);
    // private nameDetail = new BehaviorSubject<any>([]);
    // name= this.nameDetail.asObservable();
    // currentmessage= this.dataDetail.asObservable();
    constructor() { }
    //--------------------------ashraf 25-sept-2020 start
    private auditIdBS = new BehaviorSubject('');
    auditId = this.auditIdBS.asObservable();

    setAuditId(auditId: string) {
        this.auditIdBS.next(auditId);
    }

    private refreshBs = new BehaviorSubject(false);
    refreshPageObserbable = this.refreshBs.asObservable();

    refresh(flag:boolean) {
        this.refreshBs.next(flag);
    }

    private flagObj = new BehaviorSubject({ disableAuditDropDown: false });
    sharedFlagObj = this.flagObj.asObservable();

    setsharedFlagObj(data) {
        this.flagObj.next(data);
    }

    //-------------------------- ashraf 25-sept-2020 end

    private memoCount = new BehaviorSubject<number>(0);
    private annCount = new BehaviorSubject<number>(0);
    private omCount = new BehaviorSubject<number>(0);
    public setValue(value: number): void {
        this.memoCount.next(value)
    }
    public getValue(): Observable<number> {
        return this.memoCount;
    }
    public setValueann(value: number): void {
        this.annCount.next(value)
    }
    public getValueann(): Observable<number> {
        return this.annCount;
    }
    public setValueom(value: number): void {
        this.omCount.next(value)
    }
    public getValueom(): Observable<number> {
        return this.omCount;
    }

}