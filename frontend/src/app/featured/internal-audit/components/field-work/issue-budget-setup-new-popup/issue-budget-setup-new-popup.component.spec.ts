import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueBudgetSetupNewPopupComponent } from './issue-budget-setup-new-popup.component';

describe('IssueBudgetSetupNewPopupComponent', () => {
  let component: IssueBudgetSetupNewPopupComponent;
  let fixture: ComponentFixture<IssueBudgetSetupNewPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueBudgetSetupNewPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueBudgetSetupNewPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
