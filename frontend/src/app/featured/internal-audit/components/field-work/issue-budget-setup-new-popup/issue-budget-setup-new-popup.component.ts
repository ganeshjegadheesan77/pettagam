import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from "@angular/core";

import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { HttpService } from "src/app/core/http-services/http.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";

@Component({
  selector: "app-issue-budget-setup-new-popup",
  templateUrl: "./issue-budget-setup-new-popup.component.html",
  styleUrls: ["./issue-budget-setup-new-popup.component.less"],
})
export class IssueBudgetSetupNewPopupComponent implements OnInit, OnChanges {
  @Output() closeIssueForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  IssueForm: FormGroup;
  area: any;
  position;
  issueStatusMaster: any;
  issueTypeMaster: any;
  issuePriorityMaster: any;
  modalRef: BsModalRef;
  auditId: any;
  issueId: string;
  newId: number;
  allActions: any;
  issueData: any;
  filteredControls = [];
  deletedControls: any = [];
  assignedControls: any = [];

  filteredActions = [];
  deletedActions: any = [];
  assignedActions: any = [];
  assignedIssue: any;
  fetchedData: any;
  allControls = [];
  selectedAuditId: any;

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        //console.log("controls", response);
        this.allControls = response;
        this.filteredControls = response;
        // this.filterControls();
      }
    });

    this.httpservice
      .get(ConstUrls.getAllActionRecords)
      .subscribe((response: any) => {
        if (response) {
          this.allActions = response;
          this.filteredActions = this.allActions;
        }
      });

    // auditData
    this.initForm();
    this.getMasters();

    this.auditId = JSON.parse(localStorage.getItem("auditData"));
    if (!this.auditId) {
      const initialState = {
        title: "Error",
        content: "Please select Audit",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      this.closeIssueForm.emit(true);
      this.closeModal();
    }
    this.generateIssueId();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.auditId = JSON.parse(localStorage.getItem("auditData"))[
        "newAuditId"
      ]["AuditId"];
      this.initForm();
      this.getMasters();
      this.generateIssueId();
    }
  }

  generateIssueId() {
    this.selectedAuditId = this.auditId.newAuditId.auditId.substr(2);
    let payload = { prefix: "I" };
    this.httpservice
      .securePost(ConstUrls.getLastId, payload)
      .subscribe((res: any) => {
        if (res) {
          this.newId = parseInt(res["lastId"], 10) + 1;
          this.issueId = "I" + this.selectedAuditId + "-" + this.newId;
        }
      });
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedControls.push(controlId);
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== controlId
        );
        this.filterControls();
      }
    });
  }

  filterControls() {
    if (this.allControls) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
    }
  }
  addControl(item) {
    if (item) {
      const assignedControl = this.allControls.find(
        (control) => control.controlId === item
      );
      this.assignedControls.push(assignedControl);
      this.filterControls();
    }
  }

  deleteAction(actionId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this Action?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedActions.push(actionId);
        //console.log("remove action:", actionId);
        this.assignedActions = this.assignedActions.filter(
          (actions) => actions.actionId !== actionId
        );
        this.filterActions();
      }
    });
  }

  filterActions() {
    if (this.allActions) {
      const selectedIds = [];
      this.assignedActions.forEach((element) => {
        selectedIds.push(element.actionId);
      });
      this.filteredActions = this.allActions.filter(
        (actions) => selectedIds.indexOf(actions.actionId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addActions(item) {
    //console.log(this.assignedActions);
    if (item) {
      // this.newCIds.push(item);
      const assignedActions = this.allActions.find(
        (action) => action.actionId === item
      );
      this.assignedActions.push(assignedActions);
      this.filterActions();
    }
  }

  getMasters() {
    this.httpservice
      .get(ConstUrls.issueStatusMaster)
      .subscribe((response: any) => {
        this.issueStatusMaster = response;
        //console.log('Status Master: ' , this.issueStatusMaster);
      });

    this.httpservice
      .get(ConstUrls.issueTypeMaster)
      .subscribe((response: any) => {
        this.issueTypeMaster = response;
        //console.log('Issue Type Master: ' , this.issueTypeMaster);
      });

    this.httpservice
      .get(ConstUrls.issuePriorityMaster)
      .subscribe((response: any) => {
        this.issuePriorityMaster = response;
        //console.log('Issue Priority Master: ' , this.issuePriorityMaster);
      });

    this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
      this.area = response;
    });
  }

  onSave() {
    // this.IssueForm.get('issueId').setValue(this.issueId);
    //console.log('Form Data:' , this.IssueForm.value);
    this.IssueForm.patchValue({ issueId: this.issueId });
    const actionIds = [];
    this.assignedActions.forEach((x) => {
      actionIds.push(x.actionId);
    });
    this.IssueForm.patchValue({ actionId: actionIds });

    this.httpservice
      .securePost(ConstUrls.saveIssue, this.IssueForm.value)
      .subscribe((response: any) => {
        if (response) {
          let newPayload = {
            auditId: "OP" + this.selectedAuditId,
            issueId: response.issueId,
          };
          //console.log('New Payload:' , newPayload);
          this.httpservice
            .securePost(ConstUrls.updateAuditAfterNewIssue, newPayload)
            .subscribe((res: any) => {
              if (res) {
                //console.log('after update Audit: ' , res);
                let lastIdPayload = { prefix: "I", newId: this.newId };
                this.httpservice
                  .securePost(ConstUrls.updateLastId, lastIdPayload)
                  .subscribe((updateLastIdResponse: any) => {
                    if (updateLastIdResponse) {
                      const initialState = {
                        title: "Success",
                        content: "Issue saved Successfully",
                        link: "Ok",
                      };
                      this.modalRef = this.modalService.show(
                        DefaultModalComponent,
                        {
                          initialState,
                          class: "success-class",
                          backdrop: "static",
                          keyboard: false,
                        }
                      );

                      this.closeModal();
                    }
                  });
              }
            });

          // console.log('Response from Backend: ' , response);
        }
      });
  }

  initForm() {
    this.IssueForm = this.formBuilder.group({
      issueName: ["", []],
      action: ["", []],
      area: ["", []],
      department: ["", []],
      issueDescription: ["", []],
      issueId: ["", []],
      keyIsuue: ["false", []],
      managementResponse: ["", []],
      priority: ["", []],
      recommendation: ["", []],
      reviewComments: ["", []],
      approvedBy: ["", []],
      approverId: ["", []],
      dateApproved: ["", []],
      dateIdentified: ["", []],
      identifiedBy: ["", []],
      identifierId: ["", []],
      issueType: ["", []],
      issuePriority: ["", []],
      issueStatus: ["", []],
      actionId: [],
    });
  }

  closeModal() {
    this.closeIssueForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
