import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwRecalculationPopupComponent } from './fw-recalculation-popup.component';

describe('FwRecalculationPopupComponent', () => {
  let component: FwRecalculationPopupComponent;
  let fixture: ComponentFixture<FwRecalculationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwRecalculationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwRecalculationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
