import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueBudgetSetupPopupComponent } from './issue-budget-setup-popup.component';

describe('IssueBudgetSetupPopupComponent', () => {
  let component: IssueBudgetSetupPopupComponent;
  let fixture: ComponentFixture<IssueBudgetSetupPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueBudgetSetupPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueBudgetSetupPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
