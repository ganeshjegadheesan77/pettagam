import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  Input,
  SimpleChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";

declare var $;

@Component({
  selector: "app-issue-budget-setup-popup",
  templateUrl: "./issue-budget-setup-popup.component.html",
  styleUrls: ["./issue-budget-setup-popup.component.less"],
})
export class IssueBudgetSetupPopupComponent implements OnInit {
  fwIssueForm: FormGroup;
  modalRef: BsModalRef;
  position;
  @Input("IFormData") IFormData: any;
  @Output() closedFWForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild("issueBudgetSetup", { static: false })
  issueBudgetSetup: TabsetComponent;
  issueName: any;
  issueId: any;
  issueStatusMaster: any;
  issueTypeMaster: any;
  allActions: any;
  issueData: any;
  filteredControls = [];
  deletedControls: any = [];
  assignedControls: any = [];

  filteredActions = [];
  deletedActions: any = [];
  assignedActions: any = [];
  assignedIssue: any;
  fetchedData: any;
  allControls = [];
  issuePriorityMaster: any;
  area: any;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        //console.log("controls", response);
        this.allControls = response;
        this.filteredControls = response;
        // this.filterControls();
      }
    });

    this.httpservice
      .get(ConstUrls.getAllActionRecords)
      .subscribe((response: any) => {
        if (response) {
          //console.log("action", response);
          this.allActions = response;
          this.filteredActions = this.allActions;
          // this.allActions.forEach(x=>{
          //   this.filteredActions.push({"data":x,"name":x.actionId+"-"+x.actionName})
          // })
          // this.filterControls();
        }
      });

    this.initForm();
    this.getIssueData();
    this.getMasters();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.IFormData) {
      this.initForm();
      this.getIssueData();
      this.getMasters();
    }
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedControls.push(controlId);
        //console.log("remove control:", controlId);
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== controlId
        );
        this.filterControls();
      }
    });
  }

  filterControls() {
    if (this.allControls) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addControl(item) {
    //console.log(this.assignedControls);
    if (item) {
      // this.newCIds.push(item);
      const assignedControl = this.allControls.find(
        (control) => control.controlId === item
      );
      this.assignedControls.push(assignedControl);
      this.filterControls();
    }
  }

  deleteAction(actionId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this action?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedActions.push(actionId);
        //console.log("remove action:", actionId);
        this.assignedActions = this.assignedActions.filter(
          (actions) => actions.actionId !== actionId
        );
        this.filterActions();
      }
    });
  }

  filterActions() {
    if (this.allActions) {
      const selectedIds = [];
      this.assignedActions.forEach((element) => {
        selectedIds.push(element.actionid);
      });
      this.filteredActions = this.allActions.filter(
        (actions) => selectedIds.indexOf(actions.actionId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addActions(item) {
    //console.log(this.assignedActions);
    if (item) {
      // this.newCIds.push(item);
      const assignedActions = this.allActions.find(
        (action) => action.actionId === item
      );
      this.assignedActions.push(assignedActions);
      this.filterActions();
    }
  }

  getIssueData() {
    if (this.IFormData) {
      let payload = { issueId: this.IFormData.issueId };
      this.httpservice
        .securePost(ConstUrls.getOneIssues, payload)
        .subscribe((res: any) => {
          if (res) {
            //console.log('Issue Data: ' , res);
            this.issueName = res.issueName;
            this.issueId = res.issueId;
            this.assignedActions = [];
            this.assignedControls = [];
            if (res.actionId.length > 0) {
              res.actionId.forEach((x) => {
                this.addActions(x);
              });
            }
            this.fwIssueForm.patchValue(res);
            // this.fieldWorkForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
          }
        });
    }
  }

  getMasters() {
    this.httpservice
      .get(ConstUrls.issueStatusMaster)
      .subscribe((response: any) => {
        this.issueStatusMaster = response;
      });

    this.httpservice
      .get(ConstUrls.issueTypeMaster)
      .subscribe((response: any) => {
        this.issueTypeMaster = response;
      });

    this.httpservice
      .get(ConstUrls.issuePriorityMaster)
      .subscribe((response: any) => {
        this.issuePriorityMaster = response;
      });

    this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
      this.area = response;
    });
  }

  initForm() {
    this.fwIssueForm = this.formBuilder.group({
      issueName: ["", []],
      action: ["", []],
      area: ["", []],
      department: ["", []],
      issueDescription: ["", []],
      issueId: ["", []],
      keyIsuue: ["false", []],
      managementResponse: ["", []],
      priority: ["", []],
      recommendation: ["", []],
      reviewComments: ["", []],
      approvedBy: ["", []],
      approverId: ["", []],
      dateApproved: ["", []],
      dateIdentified: ["", []],
      identifiedBy: ["", []],
      identifierId: ["", []],
      issueType: ["", []],
      issuePriority: ["", []],
      issueStatus: ["", []],
      actionId: [],
    });
  }

  onSave() {
    if (this.fwIssueForm.value) {
      const actionIds = [];
      this.assignedActions.forEach((x) => {
        actionIds.push(x.actionId);
      });
      this.fwIssueForm.patchValue({ actionId: actionIds });
      // console.log("Form DAta: " , this.fwIssueForm.value)
      this.httpservice
        .securePost(ConstUrls.updateOneIssue, this.fwIssueForm.value)
        .subscribe((response: any) => {
          if (response) {
            const initialState = {
              title: "Success",
              content:
                "Issue : " + this.IFormData.issueId + " updated Successfully",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
            // this.closedDefinationIssueForm.emit(true);
            this.closeModal();
          }
        });
    }
  }
  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.issueBudgetSetup.tabs[0].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
