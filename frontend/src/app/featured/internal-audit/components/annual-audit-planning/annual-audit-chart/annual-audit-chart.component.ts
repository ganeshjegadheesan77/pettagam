import { Component, OnInit } from '@angular/core';
declare const drawUniverse: any;

@Component({
  selector: 'app-annual-audit-chart',
  templateUrl: './annual-audit-chart.component.html',
  styleUrls: ['./annual-audit-chart.component.less']
})
export class AnnualAuditChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    drawUniverse();
  }
}
