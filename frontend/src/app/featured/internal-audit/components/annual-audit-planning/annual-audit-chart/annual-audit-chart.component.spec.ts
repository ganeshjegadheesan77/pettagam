import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualAuditChartComponent } from './annual-audit-chart.component';

describe('AnnualAuditChartComponent', () => {
  let component: AnnualAuditChartComponent;
  let fixture: ComponentFixture<AnnualAuditChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualAuditChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAuditChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
