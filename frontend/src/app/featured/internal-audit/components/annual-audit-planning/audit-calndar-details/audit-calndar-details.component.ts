import { Component, OnInit } from '@angular/core';
import { Events, Period, Section, Item } from 'ngx-time-scheduler';
import * as moment from 'moment';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
@Component({
  selector: 'app-audit-calndar-details',
  templateUrl: './audit-calndar-details.component.html',
  styleUrls: ['./audit-calndar-details.component.less']
})
export class AuditCalndarDetailsComponent implements OnInit {

  position;
  events: Events = new Events();
  periods: Period[];
  sections: Section[];
  items: Item[];
  constructor(private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.periods = [
      {
        name: '1 week',
        timeFrameHeaders: ['DD'],
        classes: '',
        timeFrameOverall: 1440 * 7,
        timeFramePeriod: 1440,
      }
    ];

    this.sections = [{
      name: 'A',
      id: 1
    }, {
      name: 'B',
      id: 2
    }, {
      name: 'C',
      id: 3
    }];

    this.items = [{
      id: 1,
      sectionID: 1,
      name: 'Item 1',
      start: moment().add(0, 'days').startOf('day'),
      end: moment().add(4, 'days').endOf('day'),
      classes: 'schedule-green'
    }, {
      id: 2,
      sectionID: 2,
      name: 'Item 2',
      start: moment().add(1, 'days').startOf('day'),
      end: moment().add(5, 'days').endOf('day'),
      classes: 'schedule-orange'
    }, {
      id: 3,
      sectionID: 3,
      name: 'Item 3',
      start: moment().add(2, 'days').startOf('day'),
      end: moment().add(7, 'days').endOf('day'),
      classes: 'schedule-blue'
    }];
  }
  closeModal(){
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0};
  }
}
