import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditCalndarDetailsComponent } from './audit-calndar-details.component';

describe('AuditCalndarDetailsComponent', () => {
  let component: AuditCalndarDetailsComponent;
  let fixture: ComponentFixture<AuditCalndarDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditCalndarDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditCalndarDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
