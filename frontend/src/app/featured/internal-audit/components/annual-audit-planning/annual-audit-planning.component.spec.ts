import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualAuditPlanningComponent } from './annual-audit-planning.component';

describe('AnnualAuditPlanningComponent', () => {
  let component: AnnualAuditPlanningComponent;
  let fixture: ComponentFixture<AnnualAuditPlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualAuditPlanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAuditPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
