import { Component, OnInit } from '@angular/core';
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: 'app-edit-audit-form',
  templateUrl: './edit-audit-form.component.html',
  styleUrls: ['./edit-audit-form.component.less']
})
export class EditAuditFormComponent implements OnInit {
  position;
  constructor(private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }

}
