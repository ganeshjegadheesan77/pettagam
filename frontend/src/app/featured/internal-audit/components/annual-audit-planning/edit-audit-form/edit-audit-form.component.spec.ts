import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAuditFormComponent } from './edit-audit-form.component';

describe('EditAuditFormComponent', () => {
  let component: EditAuditFormComponent;
  let fixture: ComponentFixture<EditAuditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAuditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAuditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
