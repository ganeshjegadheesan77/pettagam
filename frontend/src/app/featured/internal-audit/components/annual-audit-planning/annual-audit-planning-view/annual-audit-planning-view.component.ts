import { Component, OnInit } from '@angular/core';
import { HeatMap, Tooltip } from "@syncfusion/ej2-heatmap";
import { DataService } from "../../audit-universe/data.service";
HeatMap.Inject(Tooltip);

@Component({
  selector: 'app-annual-audit-planning-view',
  templateUrl: './annual-audit-planning-view.component.html',
  styleUrls: ['./annual-audit-planning-view.component.less']
})
export class AnnualAuditPlanningViewComponent implements OnInit {
  constructor(private data: DataService) { }
  tableData = [];
  heatmapdata: any;
  yearData: any;
  state: any;
  sitesVal: any;
  heading: any = "";
  ngOnInit() {
    this.ngOnChanges();
    window.addEventListener("message", this.ngOnChanges.bind(this), false);
  }
  ngOnChanges() {
    var year = new Date().getFullYear();
    this.yearData = [
      Number(year),
      Number(year) + 1,
      Number(year) + 2,
      Number(year) + 3,
      Number(year) + 4,
      year,
    ];
    this.state = ["S.America", "N.America", "Africa", "Asia", "Australia"];
    this.heatmapdata = this.data.auditable;
    if (this.heatmapdata.length == 0) this.heading = "Nickel";
    else this.heading = "Copper & Cobalt";

    setTimeout(() => {
      var heatmap1: HeatMap = new HeatMap({
        titleSettings: {
          text: "2020 Audit Plan Plan for " + this.data.name,
          textStyle: {
            size: "15px",
            fontWeight: "500",
            fontStyle: "Normal",
            fontFamily: "Segoe UI",
          },
        },
        height: "200px",
        width: "450px",
        xAxis: {
          labels: this.state,
          //labelRotation:45,s
          opposedPosition: true,
        },
        yAxis: {
          labels: this.data.auditable,
          isInversed: true,
        },
        cellSettings: {
          showLabel: false,
          border: {
            color: "white",
          },
        },
        paletteSettings: {
          palette: [
            // { value: 0, color: '#5895ff', label: '1 Audit' },
            // { value: 1, color: 'rgb(172, 213, 242)', label: '1 audits' },
            // { value: 2, color: 'rgb(178, 213, 242)', label: '2 audits' }
          ],
          type: "Fixed",
          emptyPointColor: "white",
        },
        // legendSettings: {
        //     position: 'Bottom',
        //     width: '40%',
        //     alignment: 'Near',
        //     showLabel: false,
        //     labelDisplayType: 'None',
        //     enableSmartLegend: false
        // },
        dataSource: this.data.auditData,
      });
      heatmap1.appendTo("#heatmap1");
    }, 2000);
  }
}
