import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualAuditPlanningViewComponent } from './annual-audit-planning-view.component';

describe('AnnualAuditPlanningViewComponent', () => {
  let component: AnnualAuditPlanningViewComponent;
  let fixture: ComponentFixture<AnnualAuditPlanningViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualAuditPlanningViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAuditPlanningViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
