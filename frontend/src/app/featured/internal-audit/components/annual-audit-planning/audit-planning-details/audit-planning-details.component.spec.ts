import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditPlanningDetailsComponent } from './audit-planning-details.component';

describe('AuditPlanningDetailsComponent', () => {
  let component: AuditPlanningDetailsComponent;
  let fixture: ComponentFixture<AuditPlanningDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditPlanningDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditPlanningDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
