import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
declare var $;
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: "app-audit-planning-details",
  templateUrl: "./audit-planning-details.component.html",
  styleUrls: ["./audit-planning-details.component.less"],
})
export class AuditPlanningDetailsComponent implements OnInit, OnChanges {
  @Input("objIdToFind") objIdToFind: any;
  @Output() closedAssessForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  position;
  impactBtnColor: string;
  likliBtnColor: string;
  result: number;
  overallRiskScore: number;
  overallRiskScoreBtn: string;
  RAauditName: any;
  riskLevelName: any;
  auditRecord: any = [];
  issueList: any = [];
  rotationYr: any;
  aftrYr: any;
  siteName: any;
  previousYrData: any;
  auditData: any = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) {}
  assessmentForm: FormGroup;
  modalRef: BsModalRef;
  impact1: any;
  impact2: any;
  overallImpact: any;

  liklihood1: any;
  liklihood2: any;
  overallLikelihood: any;

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.impactBtnColor = "btn low";
    this.overallImpact = 0;
    this.likliBtnColor = "btn low";
    this.overallLikelihood = 0;
    this.overallRiskScore = 0;
    this.overallRiskScoreBtn = "btn low";
    this.assessmentForm.patchValue({
      impact_financialRisk: 0,
      impact_complianceRisk: 0,
      likelihood_internalControls: 0,
      likelihood_processRisk: 0,
    });
    if (this.objIdToFind.newAuditId) {
      this.RAauditName =
        this.objIdToFind.auditableEntity + "-" + this.objIdToFind.name;
      this.riskLevelName = this.objIdToFind.riskLevel;
      this.rotationYr = this.objIdToFind.rotation;
      this.siteName = this.objIdToFind.name;
      this.previousYrData = this.objIdToFind.oldAuditId;
      this.aftrYr = new Date().getFullYear() + this.rotationYr;
      let payload = { auditId: this.objIdToFind.newAuditId.auditId };
      if (this.objIdToFind.newAuditId.issueId) {
        this.httpservice
          .securePost(ConstUrls.getIssueList, payload)
          .subscribe(async (res) => {
            if (res) {
              this.issueList = res;
            }
          });
      }
    }
  }
  ngOnChanges() {
    this.initForm();
    this.RAauditName =
      this.objIdToFind.auditableEntity + "-" + this.objIdToFind.name;
    this.riskLevelName = this.objIdToFind.riskLevel;
    this.rotationYr = this.objIdToFind.rotation;
    this.siteName = this.objIdToFind.name;
    this.previousYrData = this.objIdToFind.oldAuditId;
    this.aftrYr = new Date().getFullYear() + this.rotationYr;
    let payload1 = { id: this.objIdToFind.riskLevel };
    this.httpservice
      .securePost(ConstUrls.getARAForm, payload1)
      .subscribe(async (res) => {
        this.auditData = res;
        if (this.auditData) {
          this.overallImpact = this.auditData.impact_overall;
          this.overallLikelihood = await this.auditData.likelihood_overall;
          this.overallRiskScore = await this.auditData.overallRiskScore;
          this.fetchButtonColor();
          this.calculateRiskScore();
          this.assessmentForm.patchValue(this.auditData);
          this.assessmentForm.patchValue({
            impact_financialRisk: this.auditData.impact_financialRisk,
            impact_complianceRisk: this.auditData.impact_complianceRisk,
            impact_overall: this.auditData.impact_overall,
            likelihood_processRisk: this.auditData.likelihood_processRisk,
            likelihood_internalControls: this.auditData
              .likelihood_internalControls,
            likelihood_overall: this.auditData.likelihood_overall,
            overallRiskScore: this.auditData.overallRiskScore,
          });
        }
      });
    if (
      this.objIdToFind &&
      this.objIdToFind.newAuditId &&
      this.objIdToFind.newAuditId.issueId
    ) {
      let payload = { auditId: this.objIdToFind.newAuditId.auditId };
      this.httpservice
        .securePost(ConstUrls.getIssueList, payload)
        .subscribe((res) => {
          if (res) {
            this.issueList = res;
          }
        });
    }
  }

  initForm() {
    this.assessmentForm = this.formBuilder.group({
      auditId: ["", []],
      auditName: [this.RAauditName, []],
      riskLevel: [this.riskLevelName, []],
      aduditLocation: ["", []],
      issueIds: [[]],
      riskAssessmentId: ["", []],
      leadAuditor: ["", []],
      rotation: [this.rotationYr, []],
      nextAuditYear: [this.aftrYr, []],
      impact_financialRisk: ["0", []],
      impact_complianceRisk: ["0", []],
      impact_overall: ["0", []],
      likelihood_processRisk: ["0", []],
      likelihood_internalControls: ["0", []],
      likelihood_overall: ["0", []],
      overallRiskScore: ["0", []],
      riskAssessmentSubmitter: ["", []],
      riskAssessmentSubmitID: ["", []],
      riskAssessmentModifiedBy: ["", []],
      riskAssessmentApprovedBy: ["", []],
      riskAssessmentIdentifiedOn: ["", []],
      riskAssessmentApprovedOn: ["", []],
      riskAssessedOn: ["", []],
      riskModifiedOn: ["", []],
    });
  }
  onSave() {
    let issueIdArray: any;
    if (this.issueList) {
      this.issueList.forEach((element) => {
        issueIdArray = element.issueId;
      });
    }
    if (issueIdArray) {
      this.assessmentForm.patchValue({
        issueIds: issueIdArray,
      });
    }
    if (this.overallImpact) {
      this.assessmentForm.patchValue({
        impact_overall: this.overallImpact,
      });
    }
    if (this.overallLikelihood) {
      this.assessmentForm.patchValue({
        likelihood_overall: this.overallLikelihood,
      });
    }
    if (this.overallRiskScore) {
      this.assessmentForm.patchValue({
        overallRiskScore: this.overallRiskScore,
      });
    }
    if (this.assessmentForm && this.objIdToFind.newAuditId) {
      this.assessmentForm.patchValue({
        auditId: this.objIdToFind.newAuditId.auditId,
        auditName: this.RAauditName,
      });
    }
    if (this.auditData) {
      this.httpservice
        .securePost(ConstUrls.updateARAForm, this.assessmentForm.value)
        .subscribe((response: any) => {
          if (response) {
            const initialState = {
              title: "Success",
              content: "Audit Saved Successfully!",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
            this.closeModal();
          }
        });
    } else {
      this.httpservice
        .securePost(ConstUrls.postARAForm, this.assessmentForm.value)
        .subscribe((response: any) => {
          if (response) {
            const initialState = {
              title: "Success",
              content: "Audit Saved Successfully!",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
            this.closeModal();
          }
        });
    }

    if (this.overallImpact && this.objIdToFind.newAuditId) {
      var payload = {
        audit_id: this.objIdToFind.newAuditId.auditId,
        riskProgress: this.overallRiskScore,
      };
      this.httpservice
        .securePost(ConstUrls.updateAudit, payload)
        .subscribe((response) => {
          if (response) {
            console.log(response);
          }
        });
    }
  }

  impact1Blur() {
    if (!this.assessmentForm.get("impact_financialRisk").value) {
      this.impact1 = 0;
    }
  }

  impact2Blur() {
    if (!this.assessmentForm.get("impact_complianceRisk").value) {
      this.impact2 = 0;
    }
  }

  likli1Blur() {
    if (!this.assessmentForm.get("likelihood_processRisk").value) {
      this.liklihood1 = 0;
    }
  }

  likli2Blur() {
    if (!this.assessmentForm.get("likelihood_internalControls").value) {
      this.liklihood2 = 0;
    }
  }

  onImpact1Change(searchValue: number): void {
    if (isNaN(searchValue)) {
      this.impact1 = parseFloat("0.0");
    }
    if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.impact1 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.impact1 = 0;
      this.assessmentForm.patchValue({ impact_financialRisk: this.impact1 });
    }

    this.calculateOverallImpact();
  }

  onImpact2Change(searchValue: number): void {
    if (isNaN(searchValue)) {
      this.impact2 = parseFloat("0.0");
    }
    if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.impact2 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.impact2 = 0;
      this.assessmentForm.patchValue({ impact_complianceRisk: this.impact2 });
    }
    this.calculateOverallImpact();
  }

  onLikeli1Change(searchValue: number): void {
    if (isNaN(searchValue)) {
      this.liklihood1 = parseFloat("0.0");
    }
    if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.liklihood1 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.liklihood1 = 0;
      this.assessmentForm.patchValue({
        likelihood_processRisk: this.liklihood1,
      });
    }

    this.calculateOverallLikelihood();
  }

  onLikeli2Change(searchValue: number): void {
    if (isNaN(searchValue)) {
      this.liklihood2 = parseFloat("0.0");
    }
    if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.liklihood2 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.liklihood2 = 0;
      this.assessmentForm.patchValue({
        likelihood_internalControls: this.liklihood2,
      });
    }

    this.calculateOverallLikelihood();
  }

  calculateOverallLikelihood() {
    if (isNaN(this.liklihood1)) {
      this.liklihood1 = 0;
    }
    if (isNaN(this.liklihood2)) {
      this.liklihood2 = 0;
    }
    if (this.liklihood1 > this.liklihood2) {
      this.overallLikelihood = 0;
      this.overallLikelihood = this.liklihood1;
    } else if (this.liklihood1 <= this.liklihood2) {
      this.overallLikelihood = 0;

      this.overallLikelihood = this.liklihood2;
    } else {
      this.overallLikelihood = 0;
    }
    this.fetchButtonColor();
    this.calculateRiskScore();
  }

  calculateOverallImpact() {
    if (isNaN(this.impact1)) {
      this.impact1 = 0;
    }
    if (isNaN(this.impact2)) {
      this.impact2 = 0;
    }
    if (this.impact1 > this.impact2) {
      this.overallImpact = 0;
      this.overallImpact = this.impact1;
    } else if (this.impact1 <= this.impact2) {
      this.overallImpact = 0;

      this.overallImpact = this.impact2;
    } else {
      this.overallImpact = 0;
    }
    this.fetchButtonColor();
    this.calculateRiskScore();
  }

  calculateRiskScore() {
    let num1 = parseFloat(this.overallImpact);
    let num2 = parseFloat(this.overallLikelihood);

    this.result = parseFloat((num1 + num2).toFixed(2));
    if (this.result <= 7) {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn low";
    } else if (this.result >= 8 && this.result <= 14) {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn medium";
    } else {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn high";
    }
  }

  fetchButtonColor() {
    if (this.overallImpact >= 0 && this.overallImpact <= 3) {
      this.impactBtnColor = "btn low";
    } else if (this.overallImpact > 3 && this.overallImpact <= 6) {
      this.impactBtnColor = " btn medium";
    } else {
      this.impactBtnColor = " btn high";
    }

    if (this.overallLikelihood >= 0 && this.overallLikelihood <= 3) {
      this.likliBtnColor = "btn low";
    } else if (this.overallLikelihood > 3 && this.overallLikelihood <= 6) {
      this.likliBtnColor = " btn medium";
    } else {
      this.likliBtnColor = " btn high";
    }
  }
  closeModalWothoutSave() {
    this.closedAssessForm.emit(false);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  closeModal() {
    this.impactBtnColor = "btn low";
    this.overallImpact = 0;
    this.likliBtnColor = "btn low";
    this.overallLikelihood = 0;
    this.overallRiskScore = 0;
    this.overallRiskScoreBtn = "btn low";
    this.assessmentForm.patchValue({
      impact_financialRisk: 0,
      impact_complianceRisk: 0,
      likelihood_internalControls: 0,
      likelihood_processRisk: 0,
    });
    this.closedAssessForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
  }
}
