import { Component, OnInit } from "@angular/core";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
@Component({
  selector: "app-audit-planning-details-edit",
  templateUrl: "./audit-planning-details-edit.component.html",
  styleUrls: ["./audit-planning-details-edit.component.less"],
})
export class AuditPlanningDetailsEditComponent implements OnInit {
  level: string;
  position;
  constructor(private mScrollbarService: MalihuScrollbarService) {}
  name:any;
  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.name='';
    this.level='';
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0};
    // this.copyAuditForm.tabs[0].active = true;
  }
}
