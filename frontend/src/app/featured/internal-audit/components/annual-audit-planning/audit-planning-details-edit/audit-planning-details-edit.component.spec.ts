import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditPlanningDetailsEditComponent } from './audit-planning-details-edit.component';

describe('AuditPlanningDetailsEditComponent', () => {
  let component: AuditPlanningDetailsEditComponent;
  let fixture: ComponentFixture<AuditPlanningDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditPlanningDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditPlanningDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
