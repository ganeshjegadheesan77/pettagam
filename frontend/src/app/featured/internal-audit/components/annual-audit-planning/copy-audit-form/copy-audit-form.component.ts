import {
  Component,
  OnInit,
  ViewChild,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from "@angular/core";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { HttpService } from "src/app/core/http-services/http.service";
import { ChatroomService } from "../../../../../layout/collaboration-elements/components/chat-system/services/chatroom.service";
import { ConstUrls } from "src/app/config/const-urls";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: "app-copy-audit-form",
  templateUrl: "./copy-audit-form.component.html",
  styleUrls: ["./copy-audit-form.component.less"],
})
export class CopyAuditFormComponent implements OnInit, OnChanges {
  modalRef: BsModalRef;
  @ViewChild("createAuditForm", { static: false })
  copyAuditForm: TabsetComponent;
  @Input("copyForm") copyForm: string;
  // @Output() closedCopyAuditForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() copyFormStatus: EventEmitter<boolean> = new EventEmitter<boolean>();
  fieldWorkCheckedList: any;
  planningSelected: boolean;
  planningchecklist: { id: number; value: string; isSelected: boolean }[];
  fieldSelected: boolean;
  reportSelected: boolean;
  fieldworkchecklist: any;
  reportCheckList: any;
  checkedList: any[];
  newAuditId: any;
  auditInfo: any;
  planningAnnouncement: any;
  planningMemmorandumt: any;
  planningMemmorandum: any;
  planningOpeningMeeting: any;
  reportCheckedList: any[];
  reportSelect: boolean;
  position;
  constructor(
    private httpservice: HttpService,
    private modalService: BsModalService,
    public chatroomService: ChatroomService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.planningSelected = false;
    this.planningchecklist = [];

    this.fieldworkchecklist = [];
    this.fieldSelected = false;
    this.reportSelect = false;
    this.reportSelected = false;
    this.reportCheckList = [];
    this.getCheckedItemList();
  }

  ngOnChanges() {
    // console.log('INput from tbl: ' , this.copyForm);
    let getlastAudit = { prefix: "OP" };
    this.httpservice
      .securePost(ConstUrls.getLastId, getlastAudit)
      .subscribe((res: any) => {
        // NEW AUDIT GENERATE
        if (res) {
          let newId = parseInt(res["lastId"], 10) + 1;
          if (newId <= 9) {
            this.newAuditId = "00" + newId;
          } else {
            this.newAuditId = newId;
          }
          // console.log(this.newAuditId);
        } else {
          // console.log('API FAIled');
        }
      });

    // console.log('Data Returned from Table: ' , this.copyForm['copiedObjId']);
    if (this.copyForm["copiedObjId"]) {
      let getAuditId = { auditId: this.copyForm["copiedObjId"] };
      this.httpservice
        .securePost(ConstUrls.getOldAuditDetails, getAuditId)
        .subscribe((res: any) => {
          if (res) {
            // console.log('Audit Details:' , res);
            this.auditInfo = res[0];

            if (this.auditInfo.oldAuditId.audit_subprocess) {
              //create Fieldwork / subprocess list dynamic for copy tab

              for (
                let index = 0;
                index < this.auditInfo.oldAuditId.audit_subprocess.length;
                index++
              ) {
                const element = this.auditInfo.oldAuditId.audit_subprocess[
                  index
                ];

                let item = {
                  id: index,
                  value: element.subprocess_name,
                  subprocess_name: element.subprocess_name,
                  risk: element.risk,
                  isSelected: false,
                };
                this.fieldworkchecklist.push(item);
              }
            }

            if (this.auditInfo.oldAuditId.audit_announce) {
              //Fix planning submenus-Audit announce
              let announceData = {
                id: 1,
                value: "Announcement",
                _id: this.auditInfo.oldAuditId.audit_announce._id,
                isSelected: false,
              };
              this.planningchecklist.push(announceData);
            }
            if (this.auditInfo.oldAuditId.audit_plan_memo) {
              //Fix planning submenus - Memorendum
              let memoData = {
                id: 2,
                value: "Memmorandum",
                _id: this.auditInfo.oldAuditId.audit_plan_memo._id,
                isSelected: false,
              };
              this.planningchecklist.push(memoData);
            }

            if (this.auditInfo.oldAuditId.audit_opening_meeting) {
              //Fix planning submenus - Memorendum
              let memoData = {
                id: 3,
                value: "Opening Meeting",
                _id: this.auditInfo.oldAuditId.audit_opening_meeting._id,
                isSelected: false,
              };
              this.planningchecklist.push(memoData);
            }
            if (this.auditInfo.oldAuditId.audit_draft_report) {
              //Fix planning submenus - Draft Report
              let memoData = {
                id: 1,
                value: "Draft report",
                _id: this.auditInfo.oldAuditId.audit_draft_report._id,
                isSelected: false,
              };
              this.reportCheckList.push(memoData);
            }
          } else {
            const initialState = {
              title: "Error",
              content: "Technical problem to copy data",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
          }
        });
    }
  }
  closeModal() {
    this.planningchecklist = [];
    this.fieldworkchecklist = [];
    this.copyFormStatus.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }

  checkUncheckAll() {
    for (let i = 0; i < this.planningchecklist.length; i++) {
      this.planningchecklist[i].isSelected = this.planningSelected;
    }
    this.getCheckedItemList();
  }
  checkUncheckAllField() {
    for (let i = 0; i < this.fieldworkchecklist.length; i++) {
      this.fieldworkchecklist[i].isSelected = this.fieldSelected;
    }
    this.getCheckedItemList();
  }

  checkUncheckAllReport() {
    for (let i = 0; i < this.reportCheckList.length; i++) {
      this.reportCheckList[i].isSelected = this.reportSelected;
    }
    this.getCheckedItemList();
  }

  isAllSelected() {
    this.planningSelected = this.planningchecklist.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }
  isAllSelectedFrom() {
    this.fieldSelected = this.fieldworkchecklist.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }

  isAllSelectedReport() {
    this.reportSelected = this.reportCheckList.every(function (item: any) {
      return item.isSelected == true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList = [];
    this.fieldWorkCheckedList = [];
    this.reportCheckedList = [];
    for (let i = 0; i < this.planningchecklist.length; i++) {
      if (this.planningchecklist[i].isSelected) {
        this.checkedList.push(this.planningchecklist[i]);
      }
    }

    for (let i = 0; i < this.fieldworkchecklist.length; i++) {
      if (this.fieldworkchecklist[i].isSelected) {
        this.fieldWorkCheckedList.push(this.fieldworkchecklist[i]);
      }
    }

    for (let i = 0; i < this.reportCheckList.length; i++) {
      if (this.reportCheckList[i].isSelected) {
        this.reportCheckedList.push(this.reportCheckList[i]);
      }
    }
  }

  allPlanning() {}
  allFieldwork() {}

  onSave() {
    if (
      this.checkedList.length === 0 ||
      this.fieldWorkCheckedList.length === 0 ||
      this.reportCheckList.length === 0
    ) {
      const initialState = {
        title: "Error",
        content: "Please select submenus ",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      for (let index = 0; index < this.checkedList.length; index++) {
        let value = this.checkedList[index].value;
        if (value === "Announcement") {
          this.planningAnnouncement = this.checkedList[index]._id;
        }
        if (value === "Memmorandum") {
          this.planningMemmorandum = this.checkedList[index]._id;
        }

        if (value === "Opening Meeting") {
          this.planningOpeningMeeting = this.checkedList[index]._id;
        }
      }
      let payload = {
        auditId: "OP" + this.newAuditId,
        auditName: this.auditInfo.oldAuditId.auditName,
        audit_announce: this.planningAnnouncement,
        audit_plan_memo: this.planningMemmorandum,
        audit_opening_meeting: this.planningOpeningMeeting,
        audit_subprocess: this.fieldWorkCheckedList,
        audit_draft_report: this.reportCheckList[0]._id,
        year: new Date().getFullYear(),
      };
      this.httpservice
        .securePost(ConstUrls.saveAudit, payload)
        .subscribe((res: any) => {
          if (res) {
            let payload2 = {
              _id: this.copyForm["selectedTblRecord"],
              newAuditId: res._id,
            };
            this.httpservice
              .securePost(ConstUrls.updateUniverse, payload2)
              .subscribe((updateResponse: any) => {
                if (updateResponse) {
                  let lastIdPayload = { prefix: "OP", newId: this.newAuditId };
                  this.httpservice
                    .securePost(ConstUrls.updateLastId, lastIdPayload)
                    .subscribe((updateLastIdResponse: any) => {
                      if (updateLastIdResponse) {
                        let ro = {
                          title:
                            "Audit-OP" +
                            this.newAuditId +
                            ": " +
                            payload2["auditName"],
                          owner: localStorage.getItem("username"),
                          auditId: "OP" + this.newAuditId,
                        };
                        this.chatroomService.createRoom(ro);
                        console.log("New Room Created Successfully");
                        const initialState = {
                          title: "Success",
                          content: "New audit created Successfully...",
                          link: "Ok",
                        };
                        this.modalRef = this.modalService.show(
                          DefaultModalComponent,
                          {
                            initialState,
                            class: "success-class",
                            backdrop: "static",
                            keyboard: false,
                          }
                        );
                        this.closeModal();
                      }
                    });
                }
              });
          } else {
            const initialState = {
              title: "Error",
              content: "Failed to Create new Audit",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
          }
        });
    }
  }
}
