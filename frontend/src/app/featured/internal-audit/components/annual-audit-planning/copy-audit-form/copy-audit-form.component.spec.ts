import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyAuditFormComponent } from './copy-audit-form.component';

describe('CopyAuditFormComponent', () => {
  let component: CopyAuditFormComponent;
  let fixture: ComponentFixture<CopyAuditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyAuditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyAuditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
