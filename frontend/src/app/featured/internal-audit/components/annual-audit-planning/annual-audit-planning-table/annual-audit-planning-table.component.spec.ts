import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualAuditPlanningTableComponent } from './annual-audit-planning-table.component';

describe('AnnualAuditPlanningTableComponent', () => {
  let component: AnnualAuditPlanningTableComponent;
  let fixture: ComponentFixture<AnnualAuditPlanningTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualAuditPlanningTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualAuditPlanningTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
