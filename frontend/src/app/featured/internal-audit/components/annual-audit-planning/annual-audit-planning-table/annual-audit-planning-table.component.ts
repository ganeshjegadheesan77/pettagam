import { Component, OnInit, OnChanges, Input, ChangeDetectorRef } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { DataService } from "../../audit-universe/data.service";
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
declare var $;
@Component({
  selector: "app-annual-audit-planning-table",
  templateUrl: "./annual-audit-planning-table.component.html",
  styleUrls: ["./annual-audit-planning-table.component.less"],
})
export class AnnualAuditPlanningTableComponent implements OnInit, OnChanges {
  @Input("copyAuditFormClosed") copyAuditFormClosed: string;
  pdfView = false;
  modalRef: BsModalRef;
  copyComponent = false;
  loadComponent = false;
  newAuditDetailComponent = false;
  editAuditDetailComponent = false;
  selectedAuditId: any;
  tableData = [];
  objId: any;
  currentNode: any;
  currentEvent: any;

  constructor(
    private httpservice: HttpService,
    private modalService: BsModalService,
    private data: DataService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) {}

  ngOnInit() {
    this.myData();
    window.addEventListener("message", this.receiveMessage.bind(this), false);
    // this.datatable();
    $('[data-toggle="tooltip"]').tooltip();
  }

  loadRiskLevel(data) {
    // console.log("Row Data: ", data.newAuditId.auditId);
    if (data) {
      this.loadComponent = false;
      this.objId = data;
      this.newAuditDetailComponent = true;
      this.editAuditDetailComponent = false;
      this.pdfView = false;
      this.copyComponent = false;
      $(".scroll-container").scrollTop(0);
      $(".pop-up-form").removeClass("d-none");
      $(".tabOne").trigger("click");
    }
  }

  getNewAssessFormStatus(event) {
    if (event === true) {
      this.objId = "";
      this.newAuditDetailComponent = false;
      // this.myData();
      if (this.currentEvent) {
        this.receiveMessage(this.currentEvent);
      } else {
        this.myData();
      }
    }
  }
  loadMyChildComponent(is) {}
  loadEditRiskLevel() {
    this.loadComponent = false;
    this.newAuditDetailComponent = false;
    this.editAuditDetailComponent = true;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  ngOnChanges() {
    if (this.copyAuditFormClosed) {
      this.myData();
      window.addEventListener("message", this.receiveMessage.bind(this), false);
      if ($.fn.dataTable.isDataTable("#audituniverseTable")) {
        let table = $("#audituniverseTable").DataTable();
        table.destroy();
      }
      this.datatable();
      $('[data-toggle="tooltip"]').tooltip();
    }
  }

  myData() {
    this.httpservice
      .secureGet(ConstUrls.getAllTableDataByRiskLevel)
      .subscribe((response: any) => {
        if (response) {
          if ($.fn.dataTable.isDataTable("#audituniverseTable")) {
            let table = $("#audituniverseTable").DataTable();
            table.destroy();
          }
          console.log('Response before filter: ', response);
          response = response.filter((res) => res.currentYearAudit);
          console.log('Response After filter: ', response);

          // console.log("Table Data: ", response);
          // for (let i = 0; i < response.length; i++) {
          //   if (response[i].newAuditId) {
          //     let levelName = response[i].newAuditId.auditId.substring(2);
          //     response[i].riskLevelName = "ARA-" + levelName;
          //     console.log("Found: ", response[i].riskLevelName);
          //   } else {
          //     response[i].riskLevelName = response[i].riskLevel;
          //     console.log("Not Found: ", response[i].riskLevelName);
          //   }
          // }
          this.tableData = response;
          var name = "Mining & Co.";
          this.getDataMulti(response, name);
          if (!$.fn.dataTable.isDataTable("#audituniverseTable")) {
            this.datatable();
          }
        }
      });
  }

  checkCopyFormStatus(event) {
    if (event === true) {
      this.ngOnChanges();
    }
  }

  formClosed(val) {
    // console.log("Value Changed: ", val);
    this.datatable();
  }

  showMsg() {
    const initialState = {
      title: "Error",
      content: "No Allowed",
      link: "Ok",
    };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  pdfClick() {
    this.pdfView = true;
    this.loadComponent = false;
    this.copyComponent = false;
    this.editAuditDetailComponent = false;
    this.newAuditDetailComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }
  createAudit() {
    this.loadComponent = true;
    this.pdfView = false;
    this.copyComponent = false;
    this.editAuditDetailComponent = false;
    this.newAuditDetailComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }
  copyData(_id, copied_id) {
    // console.log("copied Obj Id:", _id);
    let data = { copiedObjId: copied_id, selectedTblRecord: _id };
    this.selectedAuditId = data;
    this.loadComponent = false;
    this.pdfView = false;
    this.copyComponent = true;
    this.newAuditDetailComponent = false;
    this.editAuditDetailComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }
  receiveMessage(event) {
    // let table = $("#audituniverseTable").DataTable();
    // table.destroy();
    if (event.data.id !== this.currentNode) {
      var name = event.data.name;
      this.currentNode = event.data.id;
      this.currentEvent = event;
      if ($.fn.dataTable.isDataTable("#audituniverseTable")) {
        let table = $("#audituniverseTable").DataTable();
        table.destroy();
      }
      let payload = { childrenId: event.data.childrenId };
      this.httpservice
        .securePost(ConstUrls.getAuditUniverseTableData, payload)
        .subscribe((response: any) => {
          if (response) {
            this.getDataMulti(response, name);
            this.tableData = response.filter((res) => res.currentYearAudit);
            // console.log(response);
            //  = response;
          }
          if (!$.fn.dataTable.isDataTable("#audituniverseTable")) {
            this.datatable();
          }
        });
    }
  }
  getDataMulti(dataUpdate, name) {
    var curyear = new Date().getFullYear();
    var obj: any = [];
    let siteDetail: any = [];
    let auditEntity: any = [];
    dataUpdate.forEach((element) => {
      if (element.type == "Site") siteDetail.push(element.name);
      auditEntity.push({
        ae: element.auditableEntity,
        site: element.name,
        par: element.parent,
        year: Number(element.lastAudit) + element.rotation,
      });
      obj.push({
        site: element.name,
        year: Number(element.lastAudit) + element.rotation,
      });
    });
    var finalMultiArray: any = [];
    var finalAuditArray: any = [];
    var set: any = [];
    let parents = [
      ["3.6", "3.7"],
      ["3.8"],
      ["3.1", "3.2"],
      ["3.3"],
      ["3.4", "3.5"],
    ];
    for (let j = 0; j < parents.length; j++) {
      var matrix1 = [];
      auditEntity.forEach((x) => {
        if (x.year == curyear) {
          if (parents[j].includes(x.par)) {
            set.push(x.ae);
            matrix1.push(1);
          } else matrix1.push(null);
        }
      });
      finalAuditArray.push(matrix1);
    }
    for (let j = 0, y = curyear; j < 5; j++, y++) {
      var matrix = [];
      obj.forEach((e) => {
        if (e.year == y) matrix.push(1);
        else matrix.push(null);
      });
      finalMultiArray.push(matrix);
    }
    this.data.currentmessage = finalMultiArray;
    this.data.getSites = siteDetail;
    this.data.auditData = finalAuditArray;
    this.data.auditable = set;
    this.data.name = name;
  }
  datatable() {
    this.chRef.detectChanges();
    const annualAuditPlanningTable: any = $("#annualAuditPlanningTable");
    const annualAuditPlanningIA = this.dataTableService.initializeTable(
      annualAuditPlanningTable,
      `annualAuditPlan`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      annualAuditPlanningIA,
      `annual-audit-plan-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   annualAuditPlanningIA,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(annualAuditPlanningIA);
    $("#annualAuditPlanningTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#annual-audit-plan-select-all").prop("checked", false);
          var el = $("#annual-audit-plan-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#annualAuditPlanningTable tr td input[type='checkbox']:checked").length ==
          $("#annualAuditPlanningTable tr td input[type='checkbox']").length
        ) {
          $("#annual-audit-plan-select-all").prop("checked", true);
        }
      }
    );
  }
}
