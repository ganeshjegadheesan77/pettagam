import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAuditFormComponent } from './create-audit-form.component';

describe('CreateAuditFormComponent', () => {
  let component: CreateAuditFormComponent;
  let fixture: ComponentFixture<CreateAuditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAuditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
