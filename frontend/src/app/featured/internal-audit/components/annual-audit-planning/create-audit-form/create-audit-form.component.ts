import { TabsetComponent } from "ngx-bootstrap/tabs";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ChatroomService } from "../../../../../layout/collaboration-elements/components/chat-system/services/chatroom.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild, Input
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
@Component({
  selector: "app-create-audit-form",
  templateUrl: "./create-audit-form.component.html",
  styleUrls: ["./create-audit-form.component.less"],
})
export class CreateAuditFormComponent implements OnInit {

  @Output() closeCreateAuditForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() auditId: String;

  position;
  marked = false;
  modalRef: BsModalRef;
  @Input() auditMasters;
  levelOneMaster;
  levelTwoMaster;
  levelThreeMaster;
  levelFourMaster;
  levelFiveMaster;
  businessMaster;
  auditableEntityMaster;
  masterLabels = {
    levelOneLabel: "",
    levelTwoLabel: "",
    levelThreeLabel: "",
    levelFourLabel: "",
    levelFiveLabel: "",
  }
  auditModel: CreateAuditModel;
  disableSubmit = false;
  disableEditFields = false;

  constructor(
    private httpservice: HttpService,
    private modalService: BsModalService,
    public chatroomService: ChatroomService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    // this.auditId = "OP-033";
    this.getAuditMasters();
    this.getBusinessMasters();
    this.getAuditableEntityMasters();

    if (!this.auditId) {
      this.auditModel = new CreateAuditModel();
    }
    else {
      this.disableEditFields = true;
      this.getAuditById();
    }
  }

  setMasterLabels() {
    this.masterLabels.levelOneLabel = "Company Name";
    this.masterLabels.levelTwoLabel = this.auditMasters.find(a => a.parent == "0.0").type + " Name";
    this.masterLabels.levelThreeLabel = this.auditMasters.find(a => a.parent == "1.1").type + " Name";
    this.masterLabels.levelFourLabel = this.auditMasters.find(a => a.parent == "2.1").type;
    this.masterLabels.levelFiveLabel = this.auditMasters.find(a => a.parent == "3.1").type + " Name";
  }

  getBusinessMasters() {
    // business master db call
    this.businessMaster = ["Payment Cycle", "Safety Education", "Toxic Waste Disposal", "Purchase to Pay", "Inventory Recording", "IT Security", "Correct Procedures", "Complete", "Partial", "Recycling"];
  }

  getAuditableEntityMasters() {
    this.auditableEntityMaster =
      ["Account Payable", "Safety", "Waste Management",
        "Procurement", "Inventory", "IT",
        "Travel Expense", "Site Audit Dallas", "Site Audit Oslo",
        "Site Audit Montreal", "Site Audit Brisbane"];
  }

  getAuditById() {
    this.httpservice.secureGet(ConstUrls.getAuditById + "/" + this.auditId).subscribe(
      res => {
        this.auditModel = res['data'];
        this.auditModel.auditTitle = this.auditModel.auditableEntity + " " + this.auditModel.category.levelFive.data;
        this.auditModel.createdOn = this.convertDateToFormat("dd.mm.yyyy", res['data'].createdAt);
        if (res['data'].createdAt == res['data'].updatedAt) {
          this.auditModel.updatedOn = "--.--.----"
        }
        else {
          this.auditModel.updatedOn = this.convertDateToFormat("dd.mm.yyyy", res['data'].updatedAt);
        }
        this.levelTwoMaster = this.auditMasters.filter(a => a.parent == this.auditModel.category.levelOne.id);
        this.levelThreeMaster = this.auditMasters.filter(a => a.parent == this.auditModel.category.levelTwo.id);
        this.levelFourMaster = this.auditMasters.filter(a => a.parent == this.auditModel.category.levelThree.id);
        this.levelFiveMaster = this.auditMasters.filter(a => a.parent == this.auditModel.category.levelFour.id);
        this.auditModel.isOnsite = !res['data'].isRemote;
      },
      err => {
        this.myCommonAlert(err.error['message'], "Not found");
      }
    )
  }

  convertDateToFormat(format, date): String {
    var myDate = new Date(date);
    var dd = String(myDate.getDate()).padStart(2, '0');
    var mm = String(myDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = myDate.getFullYear();
    switch (format) {
      case "dd.mm.yyyy":
        return dd + '.' + mm + '.' + yyyy;
      case "dd/mm/yyyy":
        return dd + '/' + mm + '/' + yyyy;
      case "mm.dd.yyyy":
        return mm + '.' + dd + '.' + yyyy;
      case "mm/dd/yyyy":
        return mm + '/' + dd + '/' + yyyy;
    }
  }

  getAuditMasters() {
    this.setMasterLabels();
    console.log(this.auditMasters);
    this.levelOneMaster = this.auditMasters.filter(a => a.type == "");
  }

  onRotationChange(event) {
    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      event.preventDefault();
      return;
    }
    if(event.target.value && event.target.value.length > 0 && Number.parseInt(event.target.value) > 9){
      event.preventDefault();
      return;
    }
    if(event.target.value && event.target.value.length > 0 && Number.parseInt(event.target.value) < 0){
      event.preventDefault();
      return;
    }
    return true;
  }

  onLevelChange(id, level) {
    console.log("level chane", id);
    var levelData = this.auditMasters.find(a => a.id == id);
    switch (level) {
      case "one":
        this.levelTwoMaster = this.auditMasters.filter(a => a.parent == id);
        this.auditModel.category.levelOne.id = levelData.id;
        this.auditModel.category.levelOne.name = levelData.type == "" ? "Company" : levelData.type;
        this.auditModel.category.levelOne.data = levelData.name;
        this.levelThreeMaster = [];
        this.levelFourMaster = [];
        this.levelFiveMaster = [];
        break;
      case "two":
        this.levelThreeMaster = this.auditMasters.filter(a => a.parent == id);
        this.auditModel.category.levelTwo.id = levelData.id;
        this.auditModel.category.levelTwo.name = levelData.type;
        this.auditModel.category.levelTwo.data = levelData.name;
        this.levelFourMaster = [];
        this.levelFiveMaster = [];
        break;
      case "three":
        this.levelFourMaster = this.auditMasters.filter(a => a.parent == id);
        this.auditModel.category.levelThree.id = levelData.id;
        this.auditModel.category.levelThree.name = levelData.type;
        this.auditModel.category.levelThree.data = levelData.name;
        this.levelFiveMaster = [];
        break;
      case "four":
        this.levelFiveMaster = this.auditMasters.filter(a => a.parent == id);
        this.auditModel.category.levelFour.id = levelData.id;
        this.auditModel.category.levelFour.name = levelData.type;
        this.auditModel.category.levelFour.data = levelData.name;
        break;
      case "five":
        this.auditModel.category.levelFive.id = levelData.id;
        this.auditModel.category.levelFive.name = levelData.type;
        this.auditModel.category.levelFive.data = levelData.name;
        break;
    }
  }

  closeModal(res: boolean) {
    this.closeCreateAuditForm.emit(res);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }

  printAuditForm() {
    window.print();
  }

  myCommonAlert( title,content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }

  switchProperty(e) {
    var marked = e.target.checked;
    if (marked == false) {
      this.auditModel.isOnsite = false;
    }
    else {
      this.auditModel.isOnsite = true;
    }
  }

  onSave(f) {
    if (f.form.invalid) {
      this.myCommonAlert("Please fill all fields !", "Error");
    }
    else {
      this.auditModel.isRemote = !this.auditModel.isOnsite;
      // this.disableSubmit = true;
      //create
      if (!this.auditId) {
        this.httpservice.securePost(ConstUrls.createAudit, this.auditModel).subscribe(
          res => {
            this.disableSubmit = false;
            this.closeModal(true);
            this.myCommonAlert("Success", res['message']);
          },
          err => {
            this.disableSubmit = false;
            this.myCommonAlert("Failed", err.error['message']);
          }
        )
      }
      //edit audit
      else {
        this.httpservice.securePut(ConstUrls.editAudit, { auditId: this.auditId, rotation: this.auditModel.rotation, isRemote: this.auditModel.isRemote }).subscribe(
          res => {
            this.disableSubmit = false;
            this.closeCreateAuditForm.emit(true);
            this.myCommonAlert("Success", res['message']);
          },
          err => {
            this.disableSubmit = false;
            this.myCommonAlert("Failed", err.error['message']);
          }
        )
      }
    }
    console.log(f);

    console.log(this.auditModel);
  }
}

export class CreateAuditModel {
  constructor() {
    this.auditId = "OP ---";
    this.auditTitle = "-----";
    this.createdOn = "--.--.----";
    this.updatedOn = "--.--.----";
    this.category = {
      levelOne: {
        name: "",
        id: "",
        data: ""
      },
      levelTwo: {
        name: "",
        id: "",
        data: ""
      },
      levelThree: {
        name: "",
        id: "",
        data: ""
      },
      levelFour: {
        name: "",
        id: "",
        data: ""
      },
      levelFive: {
        name: "",
        id: "",
        data: ""
      },
      businessProcess: ""
    }
    this.auditableEntity = "";
    this.isRemote = false;
    this.isOnsite = false;
    this.rotation = 0;
    this.username = localStorage.getItem('username');
    var d = new Date();
    this.year = d.getFullYear();
  }
  auditId: String;
  auditTitle;
  createdOn;
  updatedOn;
  createdBy;
  category;
  auditableEntity;
  isRemote: Boolean;
  isOnsite: Boolean;
  rotation;
  username;
  year;
}