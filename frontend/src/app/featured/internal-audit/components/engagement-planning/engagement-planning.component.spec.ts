import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagementPlanningComponent } from './engagement-planning.component';

describe('EngagementPlanningComponent', () => {
  let component: EngagementPlanningComponent;
  let fixture: ComponentFixture<EngagementPlanningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagementPlanningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagementPlanningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
