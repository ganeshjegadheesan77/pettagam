import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResPlanningFormComponent } from './res-planning-form.component';

describe('ResPlanningFormComponent', () => {
  let component: ResPlanningFormComponent;
  let fixture: ComponentFixture<ResPlanningFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResPlanningFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResPlanningFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
