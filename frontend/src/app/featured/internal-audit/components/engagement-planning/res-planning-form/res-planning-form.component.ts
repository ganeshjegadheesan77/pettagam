import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ViewChild,
  EventEmitter,
  Output,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as moment from "moment";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ChatroomService } from "../../../../../layout/collaboration-elements/components/chat-system/services/chatroom.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { ConfigService } from 'src/config.service';
// import { environment } from "src/environments/environment";
declare var $;

@Component({
  selector: "app-res-planning-form",
  templateUrl: "./res-planning-form.component.html",
  styleUrls: ["./res-planning-form.component.less"],
})
export class ResPlanningFormComponent implements OnInit, OnChanges {
  @ViewChild("resPlanningForm", { static: false })
  resPlanningForm: TabsetComponent;
  @ViewChild("resPlanningFormworkflow", { static: false })
  resPlanningFormworkflow: TabsetComponent;
  currentDate = new Date();
  data: Date;
  position;
  newWF: any;
  keyword = "UserName";
  budgetPlanning = [];
  budgetField = [];
  budgetRep = [];
  dataEnd: Date;
  dataApprovalEnd: Date;
  dataApproval: Date;
  calendarIconApprovalEnd = true;
  calendarIconApproval = true;
  approvalSec = false;
  approvalSecEnd = false;
  calendarIcon = true;
  calendarDetail = false;
  calendarIconEnd = true;
  calendarDetailEnd = false;
  rpId: string;
  form = new FormGroup({
    dateYMD: new FormControl(new Date()),
    dateFull: new FormControl(new Date()),
    dateMDY: new FormControl(new Date()),
    dateRange: new FormControl([
      new Date(),
      new Date(this.currentDate.setDate(this.currentDate.getDate() + 7)),
    ]),
  });
  @Input("auditData") auditData: any;
  @Output() closedForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalRef: BsModalRef;

  validateFlag: string;
  validateFlag1: string;
  validateFlag2: string;
  validateFlag3: string;
  validateFlag4: string;
  validateFlag5: string;
  rpData: any = [];
  typingName = "";
  result: string[];
  userList = [];
  userIdsEx: any;
  userIdsAp: any;
  roomId: any;
  constructor(
    private httpservice: HttpService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private chatroomService: ChatroomService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
  }

  ngOnChanges() {
    this.budgetPlanning = [];
    this.budgetField = [];
    this.budgetRep = [];
    if (this.auditData.rpId) {
      this.rpId = this.auditData.rpId;
      this.newWF = false;
    } else this.newWF = true;
    this.getData();
    this.httpservice
      .securePost(ConstUrls.getRoomByAudit, {
        audit_id: this.auditData.auditId,
      })
      .subscribe((r) => {
        if (r) {
          var roomData: any = r;
          this.roomId = roomData["_id"];
        }
      });
  }

  getData() {
    if (this.newWF) {
      this.rpData = {};
      this.userIdsEx = [];
      this.userIdsAp = [];
      this.rpData.budget = 0;
      this.rpData.planning = 0;
      this.rpData.fieldWork = 0;
      this.rpData.reporting = 0;
      this.rpData.pAssignee = [];
      this.rpData.papAssignee = [];
      this.rpData.fAssignee = [];
      this.rpData.fapAssignee = [];
      this.rpData.rAssignee = [];
      this.rpData.rapAssignee = [];
      this.rpData.execution = 0;
      this.rpData.approval = 0;
      this.rpData.exDuration = 0;
      this.rpData.apDuration = 1;
      this.rpData.fexecution = 0;
      this.rpData.fapproval = 0;
      this.rpData.fexDuration = 0;
      this.rpData.rexecution = 0;
      this.rpData.rapproval = 0;
      this.rpData.rexDuration = 0;
      this.rpData.fapDuration = 0;
      this.rpData.rapDuration = 0;
      this.newWF = true;
      this.calendarIconApprovalEnd = true;
      this.calendarIconApproval = true;
      this.calendarIcon = true;
      this.calendarIconEnd = true;
      this.rpData.lastChangeOn = new Date().toISOString();
      this.rpData.submittedOn = new Date().toISOString();
    } else {
      let pay = { rpId: this.rpId };
      this.userIdsEx = [];
      this.userIdsAp = [];
      this.httpservice
        .securePost(ConstUrls.getOneRpWorkflow, pay)
        .subscribe((data: any) => {
          if (data) {
            this.calendarIconApprovalEnd = false;
            this.calendarIconApproval = false;
            this.calendarIcon = false;
            this.calendarIconEnd = false;
            this.rpData = data;
            this.budgetPlanning = this.rpData.pAssignee.concat(
              this.rpData.papAssignee
            );
            this.budgetField = this.rpData.fAssignee.concat(
              this.rpData.fapAssignee
            );
            this.budgetRep = this.rpData.rAssignee.concat(
              this.rpData.rapAssignee
            );
            if (!this.rpData.lastChangeOn)
              this.rpData.lastChangeOn = moment(
                this.rpData.lastChangeOn
              ).format("dd-mm-yyyy");
            this.rpData.exStart = new Date(this.rpData.exStart).toUTCString();
            this.rpData.exEnd = new Date(this.rpData.exEnd).toUTCString();
            this.rpData.apStart = new Date(this.rpData.apStart).toUTCString();
            this.rpData.apEnd = new Date(this.rpData.apEnd).toUTCString();

            this.getDeadlines();
            this.rpData.fexStart = new Date(this.rpData.fexStart).toUTCString();
            this.rpData.fexEnd = new Date(this.rpData.fexEnd).toUTCString();
            this.rpData.fapStart = new Date(this.rpData.fapStart).toUTCString();
            this.rpData.fapEnd = new Date(this.rpData.fapEnd).toUTCString();
            this.getDeadlinesFields();

            this.rpData.rexStart = new Date(this.rpData.fexStart).toUTCString();
            this.rpData.rexEnd = new Date(this.rpData.rexEnd).toUTCString();
            this.rpData.rapStart = new Date(this.rpData.rapStart).toUTCString();
            this.rpData.rapEnd = new Date(this.rpData.rapEnd).toUTCString();
            this.getDeadlinesR();
          }
        });
    }
  }
  getDeadlines() {
    this.rpData.beforefive = moment(
      moment(this.rpData.exStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.beforefiveAp = moment(
      moment(this.rpData.apStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.afterfive = moment(
      moment(this.rpData.exStart).add(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.afterfiveAp = moment(
      moment(this.rpData.apStart).add(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.afterfourteen = moment(
      moment(this.rpData.exStart).add(14, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.afterfourteenAp = moment(
      moment(this.rpData.apStart).add(14, "days")
    ).format("DD-MMM-YYYY");
  }
  getDeadlinesFields() {
    this.rpData.fbeforefive = moment(
      moment(this.rpData.fexStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.fbeforefiveAp = moment(
      moment(this.rpData.fapStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.fafterfive = moment(
      moment(this.rpData.fexStart).add(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.fafterfiveAp = moment(
      moment(this.rpData.fapStart).add(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.fafterfourteen = moment(
      moment(this.rpData.fexStart).add(14, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.fafterfourteenAp = moment(
      moment(this.rpData.fapStart).add(14, "days")
    ).format("DD-MMM-YYYY");
  }
  getDeadlinesR() {
    this.rpData.rbeforefive = moment(
      moment(this.rpData.rexStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.rbeforefiveAp = moment(
      moment(this.rpData.rapStart).subtract(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.rafterfive = moment(
      moment(this.rpData.rexStart).add(5, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.rafterfiveAp = moment(
      moment(this.rpData.rapStart).add(5, "days")
    ).format("DD-MMM-YYYY");

    this.rpData.rafterfourteen = moment(
      moment(this.rpData.rexStart).add(14, "days")
    ).format("DD-MMM-YYYY");
    this.rpData.rafterfourteenAp = moment(
      moment(this.rpData.rapStart).add(14, "days")
    ).format("DD-MMM-YYYY");
  }
  openSearch(data) {
    switch (data) {
      case "validateFlag":
        this.validateFlag = "ValidatorToken";
        break;
      case "validateFlag1":
        this.validateFlag1 = "ValidatorToken";
        break;
      case "validateFlag2":
        this.validateFlag2 = "ValidatorToken";
        break;
      case "validateFlag3":
        this.validateFlag3 = "ValidatorToken";
        break;
      case "validateFlag4":
        this.validateFlag4 = "ValidatorToken";
        break;
      case "validateFlag5":
        this.validateFlag5 = "ValidatorToken";
        break;
    }
  }
  closeSearch(data) {
    switch (data) {
      case "validateFlag":
        this.validateFlag = "";
        break;
      case "validateFlag1":
        this.validateFlag1 = "";
        break;
      case "validateFlag2":
        this.validateFlag2 = "";
        break;
      case "validateFlag3":
        this.validateFlag3 = "";
        break;
      case "validateFlag4":
        this.validateFlag4 = "";
        break;
      case "validateFlag5":
        this.validateFlag5 = "";
        break;
    }
  }

  onKey(event: any) {}

  onChangeSearch(val: string, start, end) {
    if (val) {
      val = val.toLowerCase();
    }
    if (start == undefined || end == undefined) {
      const initialState = {
        title: "Alert",
        content: "Please enter start and end date of WorkFlow!",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    }
    let data = {
      database: this.configService.config.db,
      StartDate: moment(start).format("DD-MMM-YYYY"),
      EndDate: moment(end).format("DD-MMM-YYYY"),
    };
    this.httpservice
      .secureJavaPostApi(ConstUrls.getUserList, data)
      .subscribe((res: any) => {
        if (res) {
          this.userList = res.data.filter((s) => {
            let str = s.UserName.toLowerCase().trim();
            return str.includes(val);
          });
        }
      });
  }
  removeUser(user, i, section) {
    var d = 0;
    if (this.userIdsEx.length > 0) {
      var k = this.userIdsEx.findIndex((x) => x.UserName === user);
      if (k > -1) {
        this.userIdsEx.splice(k, 1);
      }
    }
    if (this.userIdsAp.length > 0) {
      var k = this.userIdsAp.findIndex((x) => x.UserName === user);
      if (k > -1) {
        this.userIdsAp.splice(k, 1);
      }
    }
    switch (section) {
      case "planningEx":
        {
          this.rpData.pAssignee.splice(i, 1);
          d = this.budgetPlanning.indexOf(user);
          if (d > -1) {
            this.budgetPlanning.splice(d, 1);
          }
        }
        break;
      case "planningAp":
        {
          this.rpData.papAssignee.splice(i, 1);
          d = this.budgetPlanning.indexOf(user);
          if (d > -1) {
            this.budgetPlanning.splice(d, 1);
          }
        }
        break;
      case "fieldEx":
        {
          this.rpData.fAssignee.splice(i, 1);
          d = this.budgetField.indexOf(user);
          if (d > -1) {
            this.budgetField.splice(d, 1);
          }
        }
        break;
      case "fieldAp":
        {
          this.rpData.fapAssignee.splice(i, 1);
          d = this.budgetField.indexOf(user);
          if (d > -1) {
            this.budgetField.splice(d, 1);
          }
        }
        break;
      case "reEx":
        {
          this.rpData.rAssignee.splice(i, 1);
          d = this.budgetRep.indexOf(user);
          if (d > -1) {
            this.budgetRep.splice(d, 1);
          }
        }
        break;
      case "reAp":
        {
          this.rpData.rapAssignee.splice(i, 1);
          d = this.budgetRep.indexOf(user);
          if (d > -1) {
            this.budgetRep.splice(d, 1);
          }
        }
        break;
    }
  }
  selectEvent(item, arr, budgetType, workType) {
    if (arr) {
      if (arr.includes(item.UserName) || item.Available == "false") {
        const initialState = {
          title: "Alert",
          content: "Sorry! " + item.UserName + " is not available!",
          link: "Ok",
        };

        this.modalRef = this.modalService.show(DefaultModalComponent, {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        });
      } else {
        arr.push(item.UserName);
        if (budgetType == "plan") {
          this.budgetPlanning.push(item.UserName);
        }
        if (budgetType == "field") {
          this.budgetField.push(item.UserName);
        }
        if (budgetType == "rep") {
          this.budgetRep.push(item.UserName);
        }
        if (workType == "Execution") {
          this.userIdsEx.push(item);
        }
        if (workType == "Approval") {
          this.userIdsAp.push(item);
        }
      }
    } else {
      if (item.Available == "false") {
        const initialState = {
          title: "Alert",
          content: "Sorry! " + item.UserName + " is not available!",
          link: "Ok",
        };

        this.modalRef = this.modalService.show(DefaultModalComponent, {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        });
      } else {
        arr = [];
        arr.push(item.UserName);
        if (budgetType == "plan") {
          this.budgetPlanning.push(item.UserName);
        }
        if (budgetType == "field") {
          this.budgetField.push(item.UserName);
        }
        if (budgetType == "rep") {
          this.budgetRep.push(item.UserName);
        }
        if (workType == "Execution") {
          this.userIdsEx.push(item);
        }
        if (workType == "Approval") {
          this.userIdsAp.push(item);
        }
      }
    }
  }
  onFocused(e) {
    // do something
  }
  //save
  onSave() {
    if (
      this.rpData.planning + this.rpData.fieldWork + this.rpData.reporting >
      this.rpData.budget
    ) {
      const initialState = {
        title: "Alert",
        content:
          "Total hours of Planning, FieldWork & Reporting is more than Budget Hrs!",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      return;
    }
    if (
      this.rpData.execution + this.rpData.approval > 100 ||
      this.rpData.fexecution + this.rpData.fapproval > 100 ||
      this.rpData.rexecution + this.rpData.rapproval > 100
    ) {
      const initialState = {
        title: "Alert",
        content: "Total of Execution and Approval % value is more than 100",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      return;
    }
    if (
      this.rpData.exDuration < 0 ||
      this.rpData.apDuration < 0 ||
      this.rpData.fexDuration < 0 ||
      this.rpData.fapDuration < 0 ||
      this.rpData.rexDuration < 0 ||
      this.rpData.rapDuration < 0
    ) {
      const initialState = {
        title: "Alert",
        content: "Start date should be less than End Date!",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      return;
    }
    if (
      this.rpData.exStart != "Invalid Date" ||
      this.rpData.exStart != undefined
    )
      this.rpData.exStart = new Date(this.rpData.exStart).toISOString();
    if (
      this.rpData.apStart != "Invalid Date" ||
      this.rpData.apStart != undefined
    )
      this.rpData.apStart = new Date(this.rpData.apStart).toISOString();
    if (this.rpData.exEnd != "Invalid Date" || this.rpData.exEnd != undefined)
      this.rpData.exEnd = new Date(this.rpData.exEnd).toISOString();
    if (this.rpData.apEnd != "Invalid Date" || this.rpData.apEnd != undefined)
      this.rpData.apEnd = new Date(this.rpData.exEnd).toISOString();

    if (
      this.rpData.fexStart != "Invalid Date" ||
      this.rpData.fexStart != undefined
    )
      this.rpData.fexStart = new Date(this.rpData.fexStart).toISOString();
    if (
      this.rpData.fapStart != "Invalid Date" ||
      this.rpData.fapStart != undefined
    )
      this.rpData.fapStart = new Date(this.rpData.fapStart).toISOString();
    if (this.rpData.fexEnd != "Invalid Date" || this.rpData.fexEnd != undefined)
      this.rpData.fexEnd = new Date(this.rpData.fexEnd).toISOString();
    if (this.rpData.fapEnd != "Invalid Date" || this.rpData.fapEnd != undefined)
      this.rpData.fapEnd = new Date(this.rpData.fexEnd).toISOString();

    if (
      this.rpData.rexStart != undefined ||
      this.rpData.rexStart != "Invalid Date"
    )
      this.rpData.rexStart = new Date(this.rpData.rexStart).toISOString();
    if (
      this.rpData.rapStar != undefined ||
      this.rpData.rapStar != "Invalid Date"
    )
      this.rpData.rapStart = new Date(this.rpData.rapStart).toISOString();
    if (this.rpData.rexEnd != undefined || this.rpData.rexEnd != "Invalid Date")
      this.rpData.rexEnd = new Date(this.rpData.rexEnd).toISOString();
    if (this.rpData.rapEnd != undefined || this.rpData.rapEnd != "Invalid Date")
      this.rpData.rapEnd = new Date(this.rpData.rexEnd).toISOString();
    if (this.rpData.lastChangeOn)
      this.rpData.lastChangeOn = new Date().toISOString();

    if (this.newWF) {
      //save New WF
      this.rpData.submittedOn = new Date().toISOString();
      //console.log(this.rpData)
      this.rpData.auditId = this.auditData.auditId;
      this.rpData.auditTeam = this.budgetField.concat(
        this.budgetPlanning,
        this.budgetRep
      );
      this.rpData.rpId = "RP-" + String(Math.floor(Math.random() * 100) + 1);
      this.httpservice
        .securePost(ConstUrls.saveResourcePlanning, this.rpData)
        .subscribe((response: any) => {
          if (response) {
            var pay = {
              audit_id: this.auditData.auditId,
              rpId: this.rpData.rpId,
            };
            this.httpservice
              .securePost(ConstUrls.updateAudit, pay)
              .subscribe((res: any) => {
                if (res) {
                  //console.log('Added');
                  this.newWF = false;

                  this.closedForm.emit(true);
                  const initialState = {
                    title: "Success",
                    content: "Resource Planning saved Successfully",
                    link: "Ok",
                  };
                  this.modalRef = this.modalService.show(
                    DefaultModalComponent,
                    {
                      initialState,
                      class: "success-class",
                      backdrop: "static",
                      keyboard: false,
                    }
                  );

                  this.closeModal();
                }
              });
          }
        });
    } else {
      //update
      delete this.rpData["_id"];
      this.rpData.auditTeam = this.budgetField.concat(
        this.budgetPlanning,
        this.budgetRep
      );
      //console.log(this.rpData)
      this.httpservice
        .securePost(ConstUrls.updateResourcePlanning, this.rpData)
        .subscribe((response: any) => {
          if (response) {
            //console.log('Added');
            this.newWF = false;

            this.closedForm.emit(true);
            const initialState = {
              title: "Success",
              content:
                "Resource Planning " +
                this.auditData.rpId +
                " saved Successfully",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });

            this.closeModal();
          }
        });
    }

    if (this.userIdsEx.length > 0) {
      this.userIdsEx.forEach((element) => {
        // this.httpservice.securePost('sendmail',{username:element.UserName, content:` Audit Workflow : ${this.auditData.auditId}`,email:element.email}).subscribe((res:any)=>{
        //   console.log('email sent');
        // })
        let assginTaskData = {
          database: this.configService.config.db,
          ActionID: this.rpData.rpId,
          UserID: element.UserID,
          StartDate: moment(this.rpData.exStart).format("DD-MMM-YYYY"),
          EndDate: moment(this.rpData.exEnd).format("DD-MMM-YYYY"),
          notice: "Reminder",
          workType: "Execution",
          routerLink: "/rcm/internal-audit/engagement-planning",
          tab: "resource planning",
          seen: false,
        };
        //console.log('Assign Data: ' , assginTaskData);
        this.httpservice
          .secureJavaPostApi(ConstUrls.assignActionTask, assginTaskData)
          .subscribe((response: any) => {
            if (response) {
              //console.log('Notifictaion sent')
            }
          });
      });
    }
    if (this.userIdsAp.length > 0) {
      this.userIdsAp.forEach((element) => {
        let assginTaskData = {
          database: this.configService.config.db,
          ActionID: this.rpData.rpId,
          UserID: element.UserID,
          StartDate: moment(this.rpData.exStart).format("DD-MMM-YYYY"),
          EndDate: moment(this.rpData.exEnd).format("DD-MMM-YYYY"),
          notice: "Reminder",
          workType: "Approval",
          routerLink: "/rcm/internal-audit/engagement-planning",
          tab: "resource planning",
          seen: false,
        };
        this.httpservice
          .secureJavaPostApi(ConstUrls.assignActionTask, assginTaskData)
          .subscribe((response: any) => {
            if (response) {
              //console.log('Notifictaion sent')
            }
          });
      });
    }
    if (this.roomId != "") this.addUserToChat();
  }
  //to add chat users
  addUserToChat() {
    var userList = [];
    if (this.userIdsEx.length > 0 || this.userIdsAp.length > 0) {
      this.userIdsEx.forEach((x) => {
        if (userList.includes(x.UserName)) return;
        userList.push(x.UserName);
      });
      this.userIdsAp.forEach((y) => {
        if (userList.includes(y.UserName)) return;
        userList.push(y.UserName);
      });
    }
    userList.forEach((name) => {
      if (name != localStorage.getItem("username")) {
        this.chatroomService.addUsersToRoom(name, this.roomId);
      }
    });
  }
  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.resPlanningForm.tabs[0].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  onValueChange(value: Date): void {
    this.rpData.exStart = value;
    this.rpData.exDuration =
      (new Date(this.rpData.exEnd).getTime() -
        new Date(this.rpData.exStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.exDuration = parseInt(this.rpData.exDuration);
    this.getDeadlines();
    this.calendarIcon = false;
    this.calendarDetail = true;
  }
  onValueChangeEnd(value: Date): void {
    this.rpData.exEnd = value;
    this.rpData.exDuration =
      (new Date(this.rpData.exEnd).getTime() -
        new Date(this.rpData.exStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.exDuration = parseInt(this.rpData.exDuration);
    this.getDeadlines();
    this.calendarIconEnd = false;
    this.calendarDetailEnd = true;
  }
  onValueChangeSec(value: Date): void {
    this.rpData.apStart = value;
    this.rpData.apDuration =
      (new Date(this.rpData.apEnd).getTime() -
        new Date(this.rpData.apStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.apDuration = parseInt(this.rpData.apDuration);
    this.getDeadlines();
    this.calendarIconApproval = false;
    this.approvalSec = true;
  }

  onValueChangeSecEnd(value: Date): void {
    this.rpData.apEnd = value;
    this.rpData.apDuration =
      (new Date(this.rpData.apEnd).getTime() -
        new Date(this.rpData.apStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.apDuration = parseInt(this.rpData.apDuration);
    this.getDeadlines();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  onValueChangeField(value: Date): void {
    this.rpData.fexStart = value;
    this.rpData.fexDuration =
      (new Date(this.rpData.fexEnd).getTime() -
        new Date(this.rpData.fexStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.fexDuration = parseInt(this.rpData.fexDuration);
    this.getDeadlinesFields();
    this.calendarIconApproval = false;
    //this.approvalSecEnd = true;
  }
  onValueChangeEndField(value: Date): void {
    this.rpData.fexEnd = value;
    this.rpData.fexDuration =
      (new Date(this.rpData.fexEnd).getTime() -
        new Date(this.rpData.fexStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.fexDuration = parseInt(this.rpData.fexDuration);
    this.getDeadlinesFields();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  onValueChangeSecField(value: Date): void {
    this.rpData.fapStart = value;
    this.rpData.fapDuration =
      (new Date(this.rpData.fapEnd).getTime() -
        new Date(this.rpData.fapStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.fapDuration = parseInt(this.rpData.fapDuration);
    this.getDeadlinesFields();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  onValueChangeSecApField(value: Date): void {
    this.rpData.fapEnd = value;
    this.rpData.fapDuration =
      (new Date(this.rpData.fapEnd).getTime() -
        new Date(this.rpData.fapStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.fapDuration = parseInt(this.rpData.fapDuration);
    this.getDeadlinesFields();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  onValueChangeRexStart(value: Date): void {
    this.rpData.rexStart = value;
    this.rpData.rexDuration =
      (new Date(this.rpData.rexEnd).getTime() -
        new Date(this.rpData.rexStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.rexDuration = parseInt(this.rpData.rexDuration);
    this.getDeadlinesR();
    this.calendarIconApproval = false;
    this.approvalSecEnd = true;
  }
  onValueChangeRexEnd(value: Date): void {
    this.rpData.rexEnd = value;
    this.rpData.rexDuration =
      (new Date(this.rpData.rexEnd).getTime() -
        new Date(this.rpData.rexStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.rexDuration = parseInt(this.rpData.rexDuration);
    this.getDeadlinesR();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  onValueChangeRapStart(value: Date): void {
    this.rpData.rapStart = value;
    this.rpData.rapDuration =
      (new Date(this.rpData.rapEnd).getTime() -
        new Date(this.rpData.rapStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.rapDuration = parseInt(this.rpData.rapDuration);
    this.getDeadlinesR();
    this.calendarIconApproval = false;
    this.approvalSecEnd = true;
  }
  onValueChangeRapEnd(value: Date): void {
    this.rpData.rapEnd = value;
    this.rpData.rapDuration =
      (new Date(this.rpData.rapEnd).getTime() -
        new Date(this.rpData.rapStart).getTime()) /
      (1000 * 3600 * 24);
    this.rpData.rapDuration = parseInt(this.rpData.rapDuration);
    this.getDeadlinesR();
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
}
