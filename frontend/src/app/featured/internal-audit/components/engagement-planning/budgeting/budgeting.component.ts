import { Component, OnInit, ChangeDetectorRef, OnChanges } from "@angular/core";
import { HttpService } from "../../../../../core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
import * as moment from "moment";
declare var $;

@Component({
  selector: "app-budgeting",
  templateUrl: "./budgeting.component.html",
  styleUrls: ["./budgeting.component.less"],
})
export class BudgetingComponent implements OnInit, OnChanges {
  public loadComponent = false;
  auditList: any;
  data: any;
  selecteddata: any;
  dataTable: any;
  budgetingEPTableHeaders: any;
  showHideHeadersBEP: any;
  isLoading = false;
  constructor(
    private httpService: HttpService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) {}
  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.getBudgetData();
    this.outsideClick();
  }
  ngOnChanges() {}
  getRpForm(event) {
    console.log(event);
    if (event === true) {
      let table = $("#budgetTable").DataTable();
      table.destroy();
      this.getBudgetData();
    }
  }

  getBudgetData() {
    let table = $("#budgetTable").DataTable();
    table.destroy();
    this.isLoading = true;
    this.budgetingEPTableHeaders = [];
    this.showHideHeadersBEP = [];
    this.httpService
      .securePost(ConstUrls.getAuditList)
      .subscribe((response) => {
        console.log("getAllAuditReport::", response);
        this.auditList = response;
        this.httpService
          .secureGet(ConstUrls.getAllResourcePlanning)
          .subscribe((r) => {
            this.data = r;
            if (r) {
              this.auditList.forEach((element) => {
                if (element.newAuditId) {
                  element.auditId = element.newAuditId.auditId;
                }
                if (element.newAuditId && element.newAuditId.rpId) {
                  var data = this.data.filter((x) => {
                    return x.rpId == element.newAuditId.rpId;
                  });
                  element.auditTeam = data[0].auditTeam;
                  element.startDate = moment(data[0].submittedOn).format(
                    "DD-MM-YYYY"
                  );
                  element.auditId = element.newAuditId.auditId;
                  element.budget = data[0].budget;
                  element.auditLeader = data[0].auditLead;
                  element.rpId = element.newAuditId.rpId;
                }
              });
            }
          });
        this.budgetingEPTableHeaders = [
          "Auditable Entity",
          "Scope",
          "Audit ID",
          "Audit Lead",
          "Audit Team",
          "Budget in Hr(s)",
          "Start Date",
          "Res Planning",
        ];
        this.showHideHeadersBEP = this.budgetingEPTableHeaders;
        this.isLoading = false;
        this.budgetdatatable();
      });
  }
  actionRes(data) {
    this.selecteddata = data;
    // console.log("MyData: " , data)
    this.loadComponent = true;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  budgetdatatable() {
    this.chRef.detectChanges();
    const budgetingTable: any = $("#budgetTable");
    const budgetingListEP = this.dataTableService.initializeTable(
      budgetingTable,
      `budgetingEP`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      budgetingListEP,
      `budget-select-all-EP`
    );
    this.dataTableFuncService.columnDropDown(
      budgetingListEP,
      `budgetEP-hideshow`
    );
    this.dataTableFuncService.expandContainer(budgetingListEP);
    $("#budgetTable tbody").on("change", 'input[type="checkbox"]', function () {
      if (!this.checked) {
        $("#budget-select-all-EP").prop("checked", false);
        var el = $("#budget-select-all-EP").get(0);
        if (el && el.checked && "indeterminate" in el) {
          el.indeterminate = true;
        }
      }
      if (
        $("#budgetTable tr td input[type='checkbox']:checked").length ==
        $("#budgetTable tr td input[type='checkbox']").length
      ) {
        $("#budget-select-all-EP").prop("checked", true);
      }
    });
  }
  SettingbudgetingList(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    $(".dt-button-collection").hide();
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $(".dropdown-menu-column").removeClass("show-bar");
    });
  }
}
