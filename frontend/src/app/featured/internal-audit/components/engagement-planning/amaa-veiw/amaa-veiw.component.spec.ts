import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmaaVeiwComponent } from './amaa-veiw.component';

describe('AmaaVeiwComponent', () => {
  let component: AmaaVeiwComponent;
  let fixture: ComponentFixture<AmaaVeiwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmaaVeiwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmaaVeiwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
