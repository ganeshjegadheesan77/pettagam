import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { Events, Period, Section, Item, NgxTimeSchedulerService } from 'ngx-time-scheduler';
import * as moment from 'moment';
@Component({
  selector: 'app-amaa-veiw',
  templateUrl: './amaa-veiw.component.html',
  styleUrls: ['./amaa-veiw.component.less']
})
export class AmaaVeiwComponent implements OnInit {
  totalAudit: any;
  activeAudit: any;
  year: any;
  noOfAudits: any;

  events: Events = new Events();
  periods: Period[];
  sections: Section[];
  items: Item[];
  constructor(private service: NgxTimeSchedulerService, private httpService: HttpService) { }
  showPopup: any;
  ngOnInit() {
    this.httpService.securePost(ConstUrls.getAuditList).subscribe((response: any) => {
      if (!response) { this.totalAudit = 0; }
      this.totalAudit = response.length;
      //console.log('Total Controls: ' , this.totalAudit);
    })
    this.year = new Date().getFullYear();
    let payload = { year: this.year };

    this.httpService
      .securePost(ConstUrls.getNoOfAuditsCurrYear, payload)
      .subscribe((res: any) => {
        //console.log("Count:", res);
        this.noOfAudits = res;
        // if (res[0]) {
        //   this.noOfAudits = res[0]["year"];
        // } else {
        //   this.noOfAudits = "0";
        // }
      });

    this.periods = [
      {
        name: '1 week',
        timeFrameHeaders: ['DD'],
        classes: '',
        timeFrameOverall: 1440 * 7,
        timeFramePeriod: 1440,
      }
    ];

    this.sections = [{
      name: 'A',
      id: 1
    }, {
      name: 'B',
      id: 2
    }, {
      name: 'C',
      id: 3
    }];

    this.items = [{
      id: 1,
      sectionID: 1,
      name: 'Item 1',
      start: moment().add(0, 'days').startOf('day'),
      end: moment().add(4, 'days').endOf('day'),
      classes: 'schedule-green'
    }, {
      id: 2,
      sectionID: 2,
      name: 'Item 2',
      start: moment().add(1, 'days').startOf('day'),
      end: moment().add(5, 'days').endOf('day'),
      classes: 'schedule-orange'
    }, {
      id: 3,
      sectionID: 3,
      name: 'Item 3',
      start: moment().add(2, 'days').startOf('day'),
      end: moment().add(7, 'days').endOf('day'),
      classes: 'schedule-blue'
    }];
  }


  auditDetails() {
    this.showPopup = true;
    $(".pop-up-form").removeClass("d-none");
  }
}
