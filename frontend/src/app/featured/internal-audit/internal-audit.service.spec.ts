import { TestBed } from '@angular/core/testing';

import { InternalAuditService } from './internal-audit.service';

describe('InternalAuditService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternalAuditService = TestBed.get(InternalAuditService);
    expect(service).toBeTruthy();
  });
});
