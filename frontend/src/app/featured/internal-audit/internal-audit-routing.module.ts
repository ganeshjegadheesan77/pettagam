import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ReportingComponent } from "./components/reporting/reporting.component";
import { AuditUniverseComponent } from "./components/audit-universe/audit-universe.component";
import { EngagementPlanningComponent } from "./components/engagement-planning/engagement-planning.component";
import { AnnualAuditPlanningComponent } from "./components/annual-audit-planning/annual-audit-planning.component";
import { FieldWorkComponent } from "./components/field-work/field-work.component";
import { DataService } from "./components/audit-universe/data.service";
import { MemoService } from "./components/field-work/memo.service";
const routes: Routes = [
  { path: "", redirectTo: "audit-universe", pathMatch: "full" },
  {
    path: "reporting",
    component: ReportingComponent,
    data: { title: "EnIA: Reporting" },
  },
  {
    path: "audit-universe",
    component: AuditUniverseComponent,
    data: { title: "EnIA: Audit Universe" },
  },
  { path: "engagement-planning-old", component: EngagementPlanningComponent },
  {
    path: "annual-audit-planning",
    component: AnnualAuditPlanningComponent,
    data: { title: "Annual Audit Planning" },
  },
  {
    path: "engagement-planning",
    component: FieldWorkComponent,
    data: { title: "EnIA: Fieldwork" },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [DataService, MemoService],
})
export class InternalAuditRoutingModule {}
