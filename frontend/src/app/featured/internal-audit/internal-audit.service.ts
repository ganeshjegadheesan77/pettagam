import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InternalAuditService {

  private auditRefresh = new Subject<any>();
  

  //-------------------------------------------------------

  refreshAudit(message: string) {  //send message from Issue List to IAM
      this.auditRefresh.next({ text: message });
  }

  refreshDone(): Observable<any> {  //got Message from Issue List
    return this.auditRefresh.asObservable();
  }
}
