import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from "ngx-bootstrap";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";

import { AutocompleteLibModule } from "angular-ng-autocomplete";

import { NewControlFormRoutingModule } from "./new-control-form-routing.module";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { AngularDraggableModule } from "angular2-draggable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxFileDropModule } from "ngx-file-drop";
import { NgxTimeSchedulerModule } from "ngx-time-scheduler";

import { NewControlFormComponent } from './new-control-form.component';
import { NewControlFormDashboardComponent } from './components/new-control-form-dashboard/new-control-form-dashboard.component';
import { NewControlDetailsComponent } from './components/new-control-details/new-control-details.component';
import { NewControlExecutionComponent } from './components/new-control-execution/new-control-execution.component';
import { NewControlTestingComponent } from './components/new-control-testing/new-control-testing.component';
import { NewControlLinkedItemsComponent } from './components/new-control-linked-items/new-control-linked-items.component';
import { NewControlAdminComponent } from './components/new-control-admin/new-control-admin.component'
import { AngularEditorModule } from '@kolkov/angular-editor';
import { NewControlTestingDashboardComponent } from './components/new-control-testing-dashboard/new-control-testing-dashboard.component';
import { NewControlTestingPastTestingComponent } from './components/new-control-testing-past-testing/new-control-testing-past-testing.component';
import { NewControlTestingIssueComponent } from './components/new-control-testing-issue/new-control-testing-issue.component';
import { NewControlTestingAdminComponent } from './components/new-control-testing-admin/new-control-testing-admin.component';
import { NewControlTestingDetailsComponent } from './components/new-control-testing-details/new-control-testing-details.component';
import { NewControlExecutionDashboardComponent } from './components/new-control-execution-dashboard/new-control-execution-dashboard.component';
import { NewControlExecutionPastExecutionComponent } from './components/new-control-execution-past-execution/new-control-execution-past-execution.component';
import { NewControlExecutionIssuesAndActionsComponent } from './components/new-control-execution-issues-and-actions/new-control-execution-issues-and-actions.component';
import { NewControlExecutionAdminComponent } from './components/new-control-execution-admin/new-control-execution-admin.component';
import { NewControlExecutionDetailsComponent } from './components/new-control-execution-details/new-control-execution-details.component';

@NgModule({
  declarations: [
    NewControlFormComponent,
    NewControlFormDashboardComponent,
    NewControlDetailsComponent,
    NewControlExecutionComponent,
    NewControlTestingComponent,
    NewControlLinkedItemsComponent,
    NewControlAdminComponent,
    NewControlTestingDashboardComponent,
    NewControlTestingPastTestingComponent,
    NewControlTestingIssueComponent,
    NewControlTestingAdminComponent,
    NewControlTestingDetailsComponent,
    NewControlExecutionDashboardComponent,
    NewControlExecutionPastExecutionComponent,
    NewControlExecutionIssuesAndActionsComponent,
    NewControlExecutionAdminComponent,
    NewControlExecutionDetailsComponent,
 
    
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    PdfViewerModule,
    NewControlFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxTimeSchedulerModule,
    AngularDraggableModule,
    NgxFileDropModule,
    AngularEditorModule
  ],
  exports: [CommonModule],
  providers: [],
})
export class NewControlFormModule {}
