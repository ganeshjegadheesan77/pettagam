import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionAdminComponent } from './new-control-execution-admin.component';

describe('NewControlExecutionAdminComponent', () => {
  let component: NewControlExecutionAdminComponent;
  let fixture: ComponentFixture<NewControlExecutionAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
