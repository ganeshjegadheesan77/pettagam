import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-linked-items',
  templateUrl: './new-control-linked-items.component.html',
  styleUrls: ['./new-control-linked-items.component.less']
})
export class NewControlLinkedItemsComponent implements OnInit {
  FilterBtnFlag: string;
  
  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.newControldatatable();
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  newControldatatable() {
    this.chRef.detectChanges();
    const newLinkedItemsTable: any = $("#newLinkedItems");
    const newLinkedItems = this.dataTableService.initializeTable(
      newLinkedItemsTable,
      `activeControlTesting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      newLinkedItems,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   newLinkedItems,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(newLinkedItems);
    $("#newLinkedItems tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#newLinkedItems tr td input[type='checkbox']:checked").length ==
          $("#newLinkedItems tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
