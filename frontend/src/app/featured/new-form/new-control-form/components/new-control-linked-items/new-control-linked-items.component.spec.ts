import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlLinkedItemsComponent } from './new-control-linked-items.component';

describe('NewControlLinkedItemsComponent', () => {
  let component: NewControlLinkedItemsComponent;
  let fixture: ComponentFixture<NewControlLinkedItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlLinkedItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlLinkedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
