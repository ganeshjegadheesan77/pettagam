import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionComponent } from './new-control-execution.component';

describe('NewControlExecutionComponent', () => {
  let component: NewControlExecutionComponent;
  let fixture: ComponentFixture<NewControlExecutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
