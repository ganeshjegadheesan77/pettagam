import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-execution',
  templateUrl: './new-control-execution.component.html',
  styleUrls: ['./new-control-execution.component.less']
})
export class NewControlExecutionComponent implements OnInit {
  FilterBtnFlag: string;


  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.newControldatatable();
  }
  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }
  
  newControldatatable() {
    this.chRef.detectChanges();
    const newControlExcutionTable: any = $("#newControlExcution");
    const newControlExcution = this.dataTableService.initializeTable(
      newControlExcutionTable,
      `activeControlExecution`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      newControlExcution,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   newControlExcution,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(newControlExcution);
    $("#newControlExcution tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#newControlExcution tr td input[type='checkbox']:checked").length ==
          $("#newControlExcution tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
