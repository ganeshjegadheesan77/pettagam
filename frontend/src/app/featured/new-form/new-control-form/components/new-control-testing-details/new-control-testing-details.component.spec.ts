import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingDetailsComponent } from './new-control-testing-details.component';

describe('NewControlTestingDetailsComponent', () => {
  let component: NewControlTestingDetailsComponent;
  let fixture: ComponentFixture<NewControlTestingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
