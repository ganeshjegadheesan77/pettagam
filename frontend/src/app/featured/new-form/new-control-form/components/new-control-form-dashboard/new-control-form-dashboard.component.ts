import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-control-form-dashboard',
  templateUrl: './new-control-form-dashboard.component.html',
  styleUrls: ['./new-control-form-dashboard.component.less']
})
export class NewControlFormDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  closeModal(){
    $(".pop-up-form").addClass("d-none");
  }

}
