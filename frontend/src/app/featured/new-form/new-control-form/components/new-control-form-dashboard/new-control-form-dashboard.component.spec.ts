import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlFormDashboardComponent } from './new-control-form-dashboard.component';

describe('NewControlFormDashboardComponent', () => {
  let component: NewControlFormDashboardComponent;
  let fixture: ComponentFixture<NewControlFormDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlFormDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlFormDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
