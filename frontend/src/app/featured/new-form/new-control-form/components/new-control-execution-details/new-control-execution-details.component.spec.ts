import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionDetailsComponent } from './new-control-execution-details.component';

describe('NewControlExecutionDetailsComponent', () => {
  let component: NewControlExecutionDetailsComponent;
  let fixture: ComponentFixture<NewControlExecutionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
