import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-testing',
  templateUrl: './new-control-testing.component.html',
  styleUrls: ['./new-control-testing.component.less']
})
export class NewControlTestingComponent implements OnInit {
  FilterBtnFlag: string;
  
  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.newControldatatable()
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  newControldatatable() {
    this.chRef.detectChanges();
    const newControlTestingTable: any = $("#newControlTesting");
    const newControlTesting = this.dataTableService.initializeTable(
      newControlTestingTable,
      `activeControlTesting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      newControlTesting,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   newControlTesting,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(newControlTesting);
    $("#newControlTesting tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#newControlTesting tr td input[type='checkbox']:checked").length ==
          $("#newControlTesting tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
