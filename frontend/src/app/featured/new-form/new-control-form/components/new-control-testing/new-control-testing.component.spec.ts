import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingComponent } from './new-control-testing.component';

describe('NewControlTestingComponent', () => {
  let component: NewControlTestingComponent;
  let fixture: ComponentFixture<NewControlTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
