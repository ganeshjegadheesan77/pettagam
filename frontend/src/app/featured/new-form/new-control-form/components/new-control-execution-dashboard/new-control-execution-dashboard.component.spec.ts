import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionDashboardComponent } from './new-control-execution-dashboard.component';

describe('NewControlExecutionDashboardComponent', () => {
  let component: NewControlExecutionDashboardComponent;
  let fixture: ComponentFixture<NewControlExecutionDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
