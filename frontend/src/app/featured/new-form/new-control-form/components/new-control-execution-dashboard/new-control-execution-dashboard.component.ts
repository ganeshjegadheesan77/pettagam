import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-control-execution-dashboard',
  templateUrl: './new-control-execution-dashboard.component.html',
  styleUrls: ['./new-control-execution-dashboard.component.less']
})
export class NewControlExecutionDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  closeModal(){
    $(".pop-up-form").addClass("d-none");
  }

}
