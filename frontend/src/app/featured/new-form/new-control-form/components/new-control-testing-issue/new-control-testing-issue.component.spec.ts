import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingIssueComponent } from './new-control-testing-issue.component';

describe('NewControlTestingIssueComponent', () => {
  let component: NewControlTestingIssueComponent;
  let fixture: ComponentFixture<NewControlTestingIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
