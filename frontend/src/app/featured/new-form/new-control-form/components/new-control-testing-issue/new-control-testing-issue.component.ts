import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-testing-issue',
  templateUrl: './new-control-testing-issue.component.html',
  styleUrls: ['./new-control-testing-issue.component.less']
})
export class NewControlTestingIssueComponent implements OnInit {
  FilterBtnFlag: string;
  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.issuetestingdatatable();
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  issuetestingdatatable() {
    this.chRef.detectChanges();
    const issuetestingTable: any = $("#issuetesting");
    const issuetesting = this.dataTableService.initializeTable(
      issuetestingTable,
      `issuetesting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      issuetesting,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   issuetesting,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(issuetesting);
    $("#issuetesting tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issuetesting tr td input[type='checkbox']:checked").length ==
          $("#issuetesting tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
