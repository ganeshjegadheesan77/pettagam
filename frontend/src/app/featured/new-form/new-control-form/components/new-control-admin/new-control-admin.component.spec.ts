import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlAdminComponent } from './new-control-admin.component';

describe('NewControlAdminComponent', () => {
  let component: NewControlAdminComponent;
  let fixture: ComponentFixture<NewControlAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
