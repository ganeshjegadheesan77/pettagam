import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionIssuesAndActionsComponent } from './new-control-execution-issues-and-actions.component';

describe('NewControlExecutionIssuesAndActionsComponent', () => {
  let component: NewControlExecutionIssuesAndActionsComponent;
  let fixture: ComponentFixture<NewControlExecutionIssuesAndActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionIssuesAndActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionIssuesAndActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
