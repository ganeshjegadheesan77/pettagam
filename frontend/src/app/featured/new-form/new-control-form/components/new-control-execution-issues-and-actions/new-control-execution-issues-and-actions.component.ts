import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-execution-issues-and-actions',
  templateUrl: './new-control-execution-issues-and-actions.component.html',
  styleUrls: ['./new-control-execution-issues-and-actions.component.less']
})
export class NewControlExecutionIssuesAndActionsComponent implements OnInit {
  FilterBtnFlag: string;

  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.controlexcutionactiondatatable();
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  controlexcutionactiondatatable() {
    this.chRef.detectChanges();
    const controlexcutionactionTable: any = $("#controlexcutionaction");
    const controlexcutionaction = this.dataTableService.initializeTable(
      controlexcutionactionTable,
      `controlexcutionaction`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      controlexcutionaction,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   controlexcutionaction,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(controlexcutionaction);
    $("#controlexcutionaction tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#controlexcutionaction tr td input[type='checkbox']:checked").length ==
          $("#controlexcutionaction tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
