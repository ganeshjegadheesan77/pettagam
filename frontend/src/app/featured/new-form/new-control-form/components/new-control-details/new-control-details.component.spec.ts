import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlDetailsComponent } from './new-control-details.component';

describe('NewControlDetailsComponent', () => {
  let component: NewControlDetailsComponent;
  let fixture: ComponentFixture<NewControlDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
