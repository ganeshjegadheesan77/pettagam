import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingPastTestingComponent } from './new-control-testing-past-testing.component';

describe('NewControlTestingPastTestingComponent', () => {
  let component: NewControlTestingPastTestingComponent;
  let fixture: ComponentFixture<NewControlTestingPastTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingPastTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingPastTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
