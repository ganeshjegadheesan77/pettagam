import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-testing-past-testing',
  templateUrl: './new-control-testing-past-testing.component.html',
  styleUrls: ['./new-control-testing-past-testing.component.less']
})
export class NewControlTestingPastTestingComponent implements OnInit {
  FilterBtnFlag: string;

  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.pasttestingtatable();
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  pasttestingtatable() {
    this.chRef.detectChanges();
    const pasttestingTable: any = $("#pasttesting");
    const pasttesting = this.dataTableService.initializeTable(
      pasttestingTable,
      `pasttesting`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      pasttesting,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   pasttesting,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(pasttesting);
    $("#pasttesting tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#pasttesting tr td input[type='checkbox']:checked").length ==
          $("#pasttesting tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
