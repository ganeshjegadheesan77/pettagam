import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlExecutionPastExecutionComponent } from './new-control-execution-past-execution.component';

describe('NewControlExecutionPastExecutionComponent', () => {
  let component: NewControlExecutionPastExecutionComponent;
  let fixture: ComponentFixture<NewControlExecutionPastExecutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlExecutionPastExecutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlExecutionPastExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
