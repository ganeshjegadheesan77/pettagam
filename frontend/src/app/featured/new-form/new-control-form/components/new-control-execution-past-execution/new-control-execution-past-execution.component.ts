import { Component, OnInit ,Input,  Output,  ChangeDetectorRef,} from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";

@Component({
  selector: 'app-new-control-execution-past-execution',
  templateUrl: './new-control-execution-past-execution.component.html',
  styleUrls: ['./new-control-execution-past-execution.component.less']
})
export class NewControlExecutionPastExecutionComponent implements OnInit {
  FilterBtnFlag: string;
  constructor(
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
  ) { }

  ngOnInit() {
    this.controlexcutionpastdatatable();
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';
  }
  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  controlexcutionpastdatatable() {
    this.chRef.detectChanges();
    const controlexcutionpastTable: any = $("#controlexcutionpast");
    const controlexcutionpast = this.dataTableService.initializeTable(
      controlexcutionpastTable,
      `controlexcutionpast`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      controlexcutionpast,
      `new-control-testing-select-all`
    );
    // this.dataTableFuncService.columnDropDown(
    //   controlexcutionpast,
    //   `control-hideshow`
    // );
    this.dataTableFuncService.expandContainer(controlexcutionpast);
    $("#controlexcutionpast tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-control-testing-select-all").prop("checked", false);
          var el = $("#new-control-testing-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#controlexcutionpast tr td input[type='checkbox']:checked").length ==
          $("#controlexcutionpast tr td input[type='checkbox']").length
        ) {
          $("#new-control-testing-select-all").prop("checked", true);
        }
      }
    );
  }

}
