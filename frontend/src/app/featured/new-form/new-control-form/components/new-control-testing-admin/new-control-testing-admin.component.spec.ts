import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingAdminComponent } from './new-control-testing-admin.component';

describe('NewControlTestingAdminComponent', () => {
  let component: NewControlTestingAdminComponent;
  let fixture: ComponentFixture<NewControlTestingAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
