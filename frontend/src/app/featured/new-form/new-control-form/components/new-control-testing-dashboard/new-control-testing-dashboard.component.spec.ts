import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewControlTestingDashboardComponent } from './new-control-testing-dashboard.component';

describe('NewControlTestingDashboardComponent', () => {
  let component: NewControlTestingDashboardComponent;
  let fixture: ComponentFixture<NewControlTestingDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewControlTestingDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewControlTestingDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
