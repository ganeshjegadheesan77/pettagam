import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-control-testing-dashboard',
  templateUrl: './new-control-testing-dashboard.component.html',
  styleUrls: ['./new-control-testing-dashboard.component.less']
})
export class NewControlTestingDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  closeModal(){
    $(".pop-up-form").addClass("d-none");
  }

}
