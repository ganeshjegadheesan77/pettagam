import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewControlFormComponent } from './new-control-form.component';

const routes: Routes = [{ path: '', component: NewControlFormComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewControlFormRoutingModule { }
