import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from "ngx-bootstrap";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";

import { AutocompleteLibModule } from "angular-ng-autocomplete";

import { NewRiskFormRoutingModule } from "./new-risk-form-routing.module";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { AngularDraggableModule } from "angular2-draggable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxFileDropModule } from "ngx-file-drop";
import { NgxTimeSchedulerModule } from "ngx-time-scheduler";

import { NewRiskFormComponent } from './new-risk-form.component';
import { NewRiskFormDashboardComponent } from './components/new-risk-form-dashboard/new-risk-form-dashboard.component';
import { RiskDetailsComponent } from './components/risk-details/risk-details.component';
import { RiskAssessmentsComponent } from './components/risk-assessments/risk-assessments.component';
import { LinkedItemsComponent } from './components/linked-items/linked-items.component';
import { RiskAdminComponent } from './components/risk-admin/risk-admin.component';
import { AssessmentComponent } from './components/assessment/assessment.component';
import { PastAssessmentsComponent } from './components/past-assessments/past-assessments.component';
import { MitigatingFactorsComponent } from './components/mitigating-factors/mitigating-factors.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { RiskPastTestingComponent } from './components/risk-past-testing/risk-past-testing.component';
import { RiskIssuesComponent } from './components/risk-issues/risk-issues.component';
import { ControlTestingAdminComponent } from './components/control-testing-admin/control-testing-admin.component';
@NgModule({
  declarations: [
    NewRiskFormComponent,
    NewRiskFormDashboardComponent,
    RiskDetailsComponent,
    RiskAssessmentsComponent,
    LinkedItemsComponent,
    RiskAdminComponent,
    AssessmentComponent,
    PastAssessmentsComponent,
    MitigatingFactorsComponent,
    RiskPastTestingComponent,
    RiskIssuesComponent,
    ControlTestingAdminComponent
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    PdfViewerModule,
    NewRiskFormRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxTimeSchedulerModule,
    AngularDraggableModule,
    NgxFileDropModule,
    NgMultiSelectDropDownModule.forRoot(),
    AngularEditorModule
  ],
  exports: [CommonModule],
  providers: [],
})
export class NewRiskFormModule {}
