import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewRiskFormComponent } from './new-risk-form.component';

const routes: Routes = [{ path: '', component: NewRiskFormComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewRiskFormRoutingModule { }
