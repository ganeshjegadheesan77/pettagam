import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedItemsComponent } from './linked-items.component';

describe('LinkedItemsComponent', () => {
  let component: LinkedItemsComponent;
  let fixture: ComponentFixture<LinkedItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
