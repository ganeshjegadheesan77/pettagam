import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
@Component({
  selector: 'app-linked-items',
  templateUrl: './linked-items.component.html',
  styleUrls: ['./linked-items.component.less']
})
export class LinkedItemsComponent implements OnInit {
  FilterBtnFlag: string;
  constructor(private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    this.controlLIDatatable();
    this.issuesActionsLIDatatable();
    this.processesLIDatatable();
  }
  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';

  }

  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }
  controlLIDatatable() {
    this.chRef.detectChanges();
    const controlLITable: any = $("#control-li-table");
    const controlLI = this.dataTableService.initializeTable(
      controlLITable,
      `controlLIRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      controlLI,
      `control-li-select-all`
    );
    this.dataTableFuncService.expandContainer(controlLI);
    $("#control-li-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#control-li-select-all").prop("checked", false);
          var el = $("#control-li-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#control-li-table tr td input[type='checkbox']:checked").length ==
          $("#control-li-table tr td input[type='checkbox']").length
        ) {
          $("#control-li-select-all").prop("checked", true);
        }
      }
    );
  }

  issuesActionsLIDatatable() {
    this.chRef.detectChanges();
    const issuesActionsLITable: any = $("#issuesActions-li-table");
    const issuesActionsLI = this.dataTableService.initializeTable(
      issuesActionsLITable,
      `issuesActionsLIRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      issuesActionsLI,
      `issuesActions-li-select-all`
    );
    this.dataTableFuncService.expandContainer(issuesActionsLI);
    $("#issuesActions-li-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issuesActions-li-select-all").prop("checked", false);
          var el = $("#issuesActions-li-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issuesActions-li-table tr td input[type='checkbox']:checked").length ==
          $("#issuesActions-li-table tr td input[type='checkbox']").length
        ) {
          $("#issuesActions-li-select-all").prop("checked", true);
        }
      }
    );
  }
  processesLIDatatable() {
    this.chRef.detectChanges();
    const processesLITable: any = $("#processes-li-table");
    const processesLI = this.dataTableService.initializeTable(
      processesLITable,
      `processesLIRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      processesLI,
      `processes-li-select-all`
    );
    this.dataTableFuncService.expandContainer(processesLI);
    $("#processes-li-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#processes-li-select-all").prop("checked", false);
          var el = $("#processes-li-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#processes-li-table tr td input[type='checkbox']:checked").length ==
          $("#processes-li-table tr td input[type='checkbox']").length
        ) {
          $("#processes-li-select-all").prop("checked", true);
        }
      }
    );
  }
}
