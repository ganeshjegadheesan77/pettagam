import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskIssuesComponent } from './risk-issues.component';

describe('RiskIssuesComponent', () => {
  let component: RiskIssuesComponent;
  let fixture: ComponentFixture<RiskIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskIssuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
