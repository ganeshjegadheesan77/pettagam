import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitigatingFactorsComponent } from './mitigating-factors.component';

describe('MitigatingFactorsComponent', () => {
  let component: MitigatingFactorsComponent;
  let fixture: ComponentFixture<MitigatingFactorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitigatingFactorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitigatingFactorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
