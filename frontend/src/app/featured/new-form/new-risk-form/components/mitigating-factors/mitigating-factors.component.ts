import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
@Component({
  selector: 'app-mitigating-factors',
  templateUrl: './mitigating-factors.component.html',
  styleUrls: ['./mitigating-factors.component.less']
})
export class MitigatingFactorsComponent implements OnInit {
FilterBtnFlag: string;
  constructor(private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    this.controlMFDatatable();
    this.issuesActionsMFDatatable();
    this.processesMFDatatable();
  }
  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';

  }

  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }
  controlMFDatatable() {
    this.chRef.detectChanges();
    const controlMFTable: any = $("#control-mf-table");
    const controlMF = this.dataTableService.initializeTable(
      controlMFTable,
      `controlMFRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      controlMF,
      `control-mf-select-all`
    );
    this.dataTableFuncService.expandContainer(controlMF);
    $("#control-mf-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#control-mf-select-all").prop("checked", false);
          var el = $("#control-mf-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#control-mf-table tr td input[type='checkbox']:checked").length ==
          $("#control-mf-table tr td input[type='checkbox']").length
        ) {
          $("#control-mf-select-all").prop("checked", true);
        }
      }
    );
  }

  issuesActionsMFDatatable() {
    this.chRef.detectChanges();
    const issuesActionsMFTable: any = $("#issuesActions-mf-table");
    const issuesActionsMF = this.dataTableService.initializeTable(
      issuesActionsMFTable,
      `issuesActionsMFRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      issuesActionsMF,
      `issuesActions-mf-select-all`
    );
    this.dataTableFuncService.expandContainer(issuesActionsMF);
    $("#issuesActions-mf-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issuesActions-mf-select-all").prop("checked", false);
          var el = $("#issuesActions-mf-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issuesActions-mf-table tr td input[type='checkbox']:checked").length ==
          $("#issuesActions-mf-table tr td input[type='checkbox']").length
        ) {
          $("#issuesActions-mf-select-all").prop("checked", true);
        }
      }
    );
  }
  processesMFDatatable() {
    this.chRef.detectChanges();
    const processesMFTable: any = $("#processes-mf-table");
    const processesMF = this.dataTableService.initializeTable(
      processesMFTable,
      `processesMFRisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      processesMF,
      `processes-mf-select-all`
    );
    this.dataTableFuncService.expandContainer(processesMF);
    $("#processes-mf-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#processes-mf-select-all").prop("checked", false);
          var el = $("#processes-mf-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#processes-mf-table tr td input[type='checkbox']:checked").length ==
          $("#processes-mf-table tr td input[type='checkbox']").length
        ) {
          $("#processes-mf-select-all").prop("checked", true);
        }
      }
    );
  }

}
