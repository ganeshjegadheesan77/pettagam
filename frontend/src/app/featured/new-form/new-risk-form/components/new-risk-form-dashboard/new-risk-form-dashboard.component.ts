import { Component, OnInit } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
declare var $;

@Component({
  selector: 'app-new-risk-form-dashboard',
  templateUrl: './new-risk-form-dashboard.component.html',
  styleUrls: ['./new-risk-form-dashboard.component.less']
})
export class NewRiskFormDashboardComponent implements OnInit {
  position;
  constructor(private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0}
  }

}
