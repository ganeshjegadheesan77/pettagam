import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRiskFormDashboardComponent } from './new-risk-form-dashboard.component';

describe('NewRiskFormDashboardComponent', () => {
  let component: NewRiskFormDashboardComponent;
  let fixture: ComponentFixture<NewRiskFormDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRiskFormDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRiskFormDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
