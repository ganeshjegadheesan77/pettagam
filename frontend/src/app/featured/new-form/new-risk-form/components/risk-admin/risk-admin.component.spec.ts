import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskAdminComponent } from './risk-admin.component';

describe('RiskAdminComponent', () => {
  let component: RiskAdminComponent;
  let fixture: ComponentFixture<RiskAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
