import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
@Component({
  selector: 'app-risk-assessments',
  templateUrl: './risk-assessments.component.html',
  styleUrls: ['./risk-assessments.component.less']
})
export class RiskAssessmentsComponent implements OnInit {
  FilterBtnFlag: string;
  constructor(private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    this.completedRAdatatable();
    this.ongoingRADatatable();
  }
  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';

  }

  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }
  completedRAdatatable() {
    this.chRef.detectChanges();
    const completedRATable: any = $("#completedRA-table");
    const completedRA = this.dataTableService.initializeTable(
      completedRATable,
      `completedRARisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      completedRA,
      `completedRA-select-all`
    );
    this.dataTableFuncService.expandContainer(completedRA);
    $("#completedRA-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#completedRA-select-all").prop("checked", false);
          var el = $("#completedRA-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#completedRA-table tr td input[type='checkbox']:checked").length ==
          $("#completedRA-table tr td input[type='checkbox']").length
        ) {
          $("#completedRA-select-all").prop("checked", true);
        }
      }
    );
  }


  ongoingRADatatable() {
    this.chRef.detectChanges();
    const ongoingRATable: any = $("#ongoingRA-table");
    const ongoingRA = this.dataTableService.initializeTable(
      ongoingRATable,
      `ongoingRARisk`
    );
  
    this.dataTableFuncService.selectAllRows(
      ongoingRA,
      `ongoingRA-select-all`
    );
    this.dataTableFuncService.expandContainer(ongoingRA);
    $("#ongoingRA-table tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#ongoingRA-select-all").prop("checked", false);
          var el = $("#ongoingRA-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#ongoingRA-table tr td input[type='checkbox']:checked").length ==
          $("#ongoingRA-table tr td input[type='checkbox']").length
        ) {
          $("#ongoingRA-select-all").prop("checked", true);
        }
      }
    );
  }

}
