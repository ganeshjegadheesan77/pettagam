import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataTableService } from "../../../../services/dataTable.service";
import { DataTableFuncService } from "../../../../services/dataTableFunc.service";
@Component({
  selector: 'app-past-assessments',
  templateUrl: './past-assessments.component.html',
  styleUrls: ['./past-assessments.component.less']
})
export class PastAssessmentsComponent implements OnInit {
 FilterBtnFlag: string;
  constructor( private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    this.riskPastAssementTable();
  }
  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';

  }

  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }
  riskPastAssementTable() {
    this.chRef.detectChanges();
    const riskPATable: any = $("#riskPastAssessment");
    const riskPA = this.dataTableService.initializeTable(
      riskPATable,
      `riskPastAssessments`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      riskPA,
      `new-risk-PA-select-all`
    );
    this.dataTableFuncService.expandContainer(riskPA);
    $("#riskPastAssessment tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#new-risk-PA-select-all").prop("checked", false);
          var el = $("#new-risk-PA-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#riskPastAssessment tr td input[type='checkbox']:checked").length ==
          $("#riskPastAssessment tr td input[type='checkbox']").length
        ) {
          $("#new-risk-PA-select-all").prop("checked", true);
        }
      }
    );
  }
}
