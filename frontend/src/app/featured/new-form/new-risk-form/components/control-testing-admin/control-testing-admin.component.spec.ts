import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlTestingAdminComponent } from './control-testing-admin.component';

describe('ControlTestingAdminComponent', () => {
  let component: ControlTestingAdminComponent;
  let fixture: ComponentFixture<ControlTestingAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlTestingAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlTestingAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
