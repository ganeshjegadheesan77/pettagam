import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskPastTestingComponent } from './risk-past-testing.component';

describe('RiskPastTestingComponent', () => {
  let component: RiskPastTestingComponent;
  let fixture: ComponentFixture<RiskPastTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskPastTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskPastTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
