import { Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-case-management',
  templateUrl: './case-management.component.html',
  styleUrls: ['./case-management.component.scss']
})
export class CaseManagementComponent implements OnInit, OnDestroy {

  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('enrico-logo');
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('enrico-logo');
   }

  ngAfterViewInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['awfId'] || params['actionId']) {
        this.selectTab(3);
      }
    });
  }

  issueActionMatrixCalled() {
    alert('Issue Action Matrix Called...');
  }

  selectTab(tabId: number) {
    this.staticTabs.tabs[tabId].active = true;
  }
}
