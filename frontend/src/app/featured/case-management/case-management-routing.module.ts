import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CaseManagementComponent } from './case-management.component';

const routes: Routes = [{ path: '', component: CaseManagementComponent, data: { title: "EnRiCo: Case Management" } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaseManagementRoutingModule { }
