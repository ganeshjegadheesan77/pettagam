import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CaseManagementRoutingModule } from './case-management-routing.module';
import { CaseManagementComponent } from './case-management.component';
import { IssueActionMatrixComponent } from './components/issue-action-matrix/issue-action-matrix.component';
import { IssueDefinitionFormComponent } from "./components/issue-definition-form/issue-definition-form.component";
import { IssueMonirotingFormComponent } from './components/issue-monitoring-form/issue-monitoring-form.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CaseManagementDynamicViewComponent } from './components/case-management-dynamic-view/case-management-dynamic-view.component';
import { CorrectiveActionFormComponent } from './components/corrective-action-form/corrective-action-form.component';
import { IssueListComponent } from './components/issue-list/issue-list.component';
import { ActionListComponent } from './components/action-list/action-list.component';
import { WfActionFormComponent } from './components/wf-action-form/wf-action-form.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AddNewIssueComponent } from './components/add-new-issue/add-new-issue.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { AddNewActionComponent } from './components/add-new-action/add-new-action.component';
import { ActionWorkflowMatrixComponent } from './components/action-workflow-matrix/action-workflow-matrix.component';
import { PrintPopupCmComponent } from './components/print-popup-cm/print-popup-cm.component';
import { AngularDraggableModule } from "angular2-draggable";
@NgModule({
  declarations: [
    CaseManagementComponent,
     CaseManagementDynamicViewComponent,
     CorrectiveActionFormComponent,
     IssueActionMatrixComponent,
     IssueDefinitionFormComponent,
     IssueMonirotingFormComponent,
     IssueListComponent,
     ActionListComponent,
     WfActionFormComponent,
     AddNewIssueComponent,
     AddNewActionComponent,
     ActionWorkflowMatrixComponent,
     PrintPopupCmComponent,
 
    ],
  imports: [
    CommonModule,
    CaseManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxFileDropModule,
    PdfViewerModule,
    AngularDraggableModule

  ],
  entryComponents:[
    CorrectiveActionFormComponent,
    IssueDefinitionFormComponent
  ]
})
export class CaseManagementModule {
  
}
