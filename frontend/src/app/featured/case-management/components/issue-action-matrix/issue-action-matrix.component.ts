import { DataService } from 'src/app/featured/internal-audit/components/audit-universe/data.service';
import { Component, OnInit, ChangeDetectorRef, OnChanges } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import {CaseManagementServiceService} from '../../service/case-management-service.service';
import { Subscription } from 'rxjs';

import "datatables.net";

declare var $;

@Component({
  selector: 'app-issue-action-matrix',
  templateUrl: './issue-action-matrix.component.html',
  styleUrls: ['./issue-action-matrix.component.less']
})
export class IssueActionMatrixComponent implements OnInit {
  public loadComponent = false;
  public loadcityId = false;
  subscription: Subscription;

  IAMatrixData: any;
  controlTableHeaders: any;
  riskTableHeaders: any;

  selectedactionkId: any;
  selectedIssueId: any;
  displayControls: any;
  dataTable: any;
  showHideHeaders: any;
  isLoading = false;

  marked = false;

  constructor(
    private httpService: HttpService,
    private chRef: ChangeDetectorRef,
    private dataService: DataService,
    private caseMgmtService:CaseManagementServiceService,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) { 
     // subscribe to issue list component messages
     this.subscription = this.caseMgmtService.fromIssueRefreshedIAMTable().subscribe(message => {
      if (message) {
        //console.log('Message: ', message);
        let table = $('#casemanagementTable').DataTable();
        table.destroy();
        this.toggleToIssues();
      } 
    });

    // subscribe to Action list component messages
    this.subscription = this.caseMgmtService.fromActionRefreshedIAMTable().subscribe(message => {
      if (message) {
        //console.log('Message: ', message);
        let table = $('#casemanagementTable').DataTable();
        table.destroy();
        this.toggleToIssues();
      } 
    });

    
  }
  ngOnInit() {
    //console.log('Init called...');

    $('[data-toggle="tooltip"]').tooltip();

    this.selectedactionkId = '';

       this.toggleToIssues();
  }

 

  switchProperty(e) {

    this.marked = e.target.checked;
    if (this.marked == false) {

      this.toggleToIssues();
      //console.log('switched to Issues')
    } else {
      this.toggleToAction();
      //console.log('switched to Actions')
    }

  }






  toggleToIssues() {
    this.displayControls = true;
    
    let table = $('#casemanagementTable2').DataTable();
    table.destroy();
    this.isLoading = true;
    this.showHideHeaders = [];
    this.controlTableHeaders = [];

    this.IAMatrixData = '';
    this.IAMatrixData = [];
    this.httpService.secureGet(ConstUrls.getAllRecord).subscribe((response: any) => {
      //console.log('response', response)
      // let criticalRecrd = response.filter((ele) => ele.rating === 'Critical')
      // let normalRecrd = response.filter((ele) => ele.rating === 'Normal')

      // const objCount = {
      //   critical: criticalRecrd.length,
      //   normal: normalRecrd.length
      // }

      // this.dataService.changeMessage(objCount);



      this.IAMatrixData = [];
      this.IAMatrixData = response;
      this.IAMatrixData.forEach(x => {
        x.barColor = x.actionprogress >= 0 && x.actionprogress <= 33 ? "very-high" : x.actionprogress >= 34 && x.actionprogress <= 66 ? "very-low" : "medium";

      });


      this.controlTableHeaders = ['Area', 'Issue ID', 'Issue Name', 'Issue Description', 'Action ID', 'Action Name', 'Action Description', 'Action Progess', 'Action'];
      this.showHideHeaders = this.controlTableHeaders;
      this.isLoading = false;
      this.datatable();

    });
  }


  toggleToAction() {
    this.displayControls = false;
    
    let table = $('#casemanagementTable').DataTable();
    table.destroy();
    this.isLoading = true;
    this.showHideHeaders = [];
    this.riskTableHeaders = [];

    this.IAMatrixData = '';
    this.IAMatrixData = [];
    this.httpService.secureGet(ConstUrls.getAllRecord).subscribe((response: any) => {
      this.IAMatrixData = [];
      this.IAMatrixData = response;
      this.IAMatrixData.forEach(x => {
        x.barColor = x.actionprogress >= 0 && x.actionprogress <= 33 ? "very-high" : x.actionprogress >= 34 && x.actionprogress <= 66 ? "very-low" : "medium";

      });
      // tslint:disable-next-line: max-line-length
      this.riskTableHeaders = ['Area', 'Action ID', 'Action Name', 'Action Description', 'Action Progress', 'Issue ID', 'Issue Name', 'Issue Description', 'Action'];
      this.showHideHeaders = this.controlTableHeaders;
      this.isLoading = false;
      this.datatable2();
    });
  }


  getStatusOfChild(event) {
    if (event === true) {
      //console.log('FOrms event: ', event);
      if (this.displayControls === true) {
        let table = $('#casemanagementTable').DataTable();
        table.destroy();
        this.toggleToIssues();
      } else {
        let table = $('#casemanagementTable2').DataTable();
        table.destroy();
        this.toggleToAction();
      }

    }

  }
  getAddressOfAddNewIssueForm(event) {
    if (event === true) {
      //console.log("Event Occured:" , event)

    let table = $('#casemanagementTable').DataTable();
    table.destroy();
    this.toggleToIssues();
    this.caseMgmtService.fromIssueActionMatrixRefreshIssue('Refresh IAM');

      // this.toggleToAction();
    }

    
    // this.date = new Date();
  }

  getStatusOfEditActionForm(event) {
    if (event === true) {
      //console.log("Edit Action Form Closed Event Occured:" , event)

    let table = $('#casemanagementTable').DataTable();
    table.destroy();
    this.toggleToIssues();
    this.caseMgmtService.fromIssueActionMatrixRefreshAction('Refresh IAM');

      // this.toggleToAction();
    }

  }



  loadMyCityId(isId) {
    //console.log(isId);
    this.selectedIssueId = isId;
    this.loadcityId = true;
    this.loadComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger('click');
  }
  loadMyChildComponent(id) {
    //console.log(id);
    this.selectedactionkId = id;
    this.loadComponent = true;
    this.loadcityId = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger('click');
  }


  datatable() {
    this.chRef.detectChanges();
    const controlDataTable: any = $("#casemanagementTable");
    const issueMatrixCM = this.dataTableService.initializeTable(
      controlDataTable,
      `issueMatrixCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      issueMatrixCM,
      `cmcissueMatrix-select-all`
    );
    this.dataTableFuncService.columnDropDown(issueMatrixCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(issueMatrixCM);
    $("#casemanagementTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#cmcissueMatrix-select-all").prop("checked", false);
          var el = $("#cmcissueMatrix-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#casemanagementTable tr td input[type='checkbox']:checked").length ==
          $("#casemanagementTable tr td input[type='checkbox']").length
        ) {
          $("#cmcissueMatrix-select-all").prop("checked", true);
        }
      }
    );

       // this.dataTable.columns.adjust().draw();
  }

  datatable2() {

    this.chRef.detectChanges();
    const actionmatrixTable: any = $("#casemanagementTable2");
    const actionMatrixCM = this.dataTableService.initializeTable(
      actionmatrixTable,
      `actionMatrixCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      actionMatrixCM,
      `cmactionMatrix-select-all`
    );
    this.dataTableFuncService.columnDropDown(actionMatrixCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(actionMatrixCM);
    $("#casemanagementTable2 tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#cmactionMatrix-select-all").prop("checked", false);
          var el = $("#cmactionMatrix-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#casemanagementTable2 tr td input[type='checkbox']:checked").length ==
          $("#casemanagementTable2 tr td input[type='checkbox']").length
        ) {
          $("#cmactionMatrix-select-all").prop("checked", true);
        }
      }
    );

   
    // this.dataTable.columns.adjust().draw();
  }

  Setting(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $(".dropdown-menu-column").removeClass("show-bar");
    });
  }
}

