import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueActionMatrixComponent } from './issue-action-matrix.component';

describe('IssueActionMatrixComponent', () => {
  let component: IssueActionMatrixComponent;
  let fixture: ComponentFixture<IssueActionMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueActionMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueActionMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
