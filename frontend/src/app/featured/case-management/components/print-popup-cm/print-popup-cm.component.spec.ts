import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintPopupCmComponent } from './print-popup-cm.component';

describe('PrintPopupCmComponent', () => {
  let component: PrintPopupCmComponent;
  let fixture: ComponentFixture<PrintPopupCmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintPopupCmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintPopupCmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
