import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/core/http-services/http.service';
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

@Component({
  selector: 'app-print-popup-cm',
  templateUrl: './print-popup-cm.component.html',
  styleUrls: ['./print-popup-cm.component.less']
})
export class PrintPopupCmComponent implements OnInit {
  isPdf = false;
  notPdf = true;
   pdfSrc:string;
   selectReport: any;
   uri;
  
   constructor(private httpservice: HttpService,
    private configService:ConfigService
    
    ) {
    this.uri = configService.config.baseUrl;

     }

  ngOnInit() {

    let data = {
    
      collection: "overdue_actions",
  
    };
    this.httpservice
    .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
    .subscribe((res: any) => {
    
         this.pdfSrc = this.configService.config.baseUrl + 'reports/' + res.report;

      // this.pdfSrc = 'http://115.112.185.58:3000/api/reports/'+ res.report;

      this.isPdf = true;
      this.notPdf = false;
  
      if (res) {
        this.selectReport = res.report;
  
  
        console.log("Report List: ", res);
  
      }
    });
  }
  closeModal(){
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    
  }
  }

