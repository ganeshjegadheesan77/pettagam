import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from '../../../../layout/modal/default-modal/default-modal.component';
import { DataService } from 'src/app/featured/internal-audit/components/audit-universe/data.service';

import "datatables.net";
import { ActivatedRoute } from '@angular/router';


declare var $;

@Component({
  selector: 'app-action-workflow-matrix',
  templateUrl: './action-workflow-matrix.component.html',
  styleUrls: ['./action-workflow-matrix.component.less']
})
export class ActionWorkflowMatrixComponent implements OnInit {
  modalRef: BsModalRef;
  public loadComponent = false;
  private loadcityId = false;
  private addNewActionFormComponent = false;
  IAMatrixData: any;

  dataTable: any;
  actionTableList: any;
  selectedActionId: any;
  selectedactionkId: any;

  loadWFComponent: boolean;
  actionprogress: any;
  redirectId;
  redirectActionId;

  constructor(private httpService: HttpService,
    private dataService: DataService,
    private modalService: BsModalService,
    private chRef: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['awfId']) {
        this.redirectId = params['awfId'];
      }
      if (params['actionId']) {
        this.redirectActionId = params['actionId'];
      }
    });
    $('[data-toggle="tooltip"]').tooltip();
    this.getActionList();
    this.outsideClick();

  }

  // this.httpService.secureGet(ConstUrls.getAllActionRecords).subscribe(response => {
  //   console.log('get all actions::',response)
  //   this.actionTableList = response;
  //   this.actionTableList.forEach(x=>{
  //     x.barColor= x.actionprogress>=0 && x.actionprogress<=33  ? "very-high"  : x.actionprogress>=34 && x.actionprogress<=66 ?"very-low" :"medium";
  //     // x.riskStatus = x.actionprogress>=0 && x.actionprogress<=33  ? "CRITICAL"  : x.actionprogress>=34 && x.riskProgress<=66 ? "NORMAL" : "NORMAL";
  //     x.ratingClr =( x.rating =='Critical')? "very-high"  : (x.rating == 'Normal') ? 'very-low':'Low'
  //   });


  //  this.datatableTwo();
  // })





  getStatusOfAddNewActionForm(event) {
    //console.log(event)
    if (event === true) {
      this.getActionList();
    }

  }
  getAddressOfAddNewActionForm
    (event) {
    //console.log(event)
    if (event === true) {
      this.getActionList();
    }

  }

  getControlFormStatus(event) {
    if (event === true) {
      this.getActionList();
    }
  }




  getActionList() {
    //console.log('getActionList')
    let table = $('#actionwfmartix').DataTable();
    table.destroy();
    this.httpService.secureGet(ConstUrls.getAllActionRecords).subscribe((response: any) => {
      //console.log('response', response)
      let criticalRecrd = response.filter((ele) => ele.rating === 'Critical')
      let normalRecrd = response.filter((ele) => ele.rating === 'Normal')

      const objCount = {
        critical: criticalRecrd.length,
        normal: normalRecrd.length
      }

      this.dataService.changeMessage(objCount);



      this.actionTableList = response;
      this.actionTableList.forEach(x => {
        x.barColor = x.actionprogress >= 0 && x.actionprogress <= 33 ? "very-high" : x.actionprogress >= 34 && x.actionprogress <= 66 ? "very-low" : "medium";
        x.riskStatus = x.actionprogress >= 0 && x.actionprogress <= 33 ? "CRITICAL" : x.actionprogress >= 34 && x.riskProgress <= 66 ? "NORMAL" : "NORMAL";
        x.ratingClr = (x.rating == 'Critical') ? "very-high" : (x.rating == 'Normal') ? 'very-low' : 'Low'
      });



      this.datatableTwo();

      if (this.redirectId) {
        this.actionWF(this.redirectId);
      }
      else if (this.redirectActionId) {
        this.loadMyChildComponent(this.redirectActionId);
      }
    })



  }

  delete(id) {
    // var payload={"actionId":id,"actionWFId":''}
    // this.httpService.securePost(ConstUrls.updateOneAction,payload).subscribe(res=>{if(res){

    //   const initialState = {
    //     title: 'Success',
    //     content: 'Action WF deleted Successfully',
    //     link: 'Ok',
    //   };
    //   this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

    // }})
  }
  loadMyChildComponent(id) {
    //console.log(id);
    this.selectedactionkId = id;
    this.loadComponent = true;
    this.loadcityId = true;
    this.addNewActionFormComponent = false;

    $(".pop-up-form").removeClass("d-none");
  }

  addNewActionForm() {
    this.addNewActionFormComponent = true;
    this.loadcityId = false;
    this.loadWFComponent = false;
    this.loadComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }



  actionWF(actionId) {
    if (typeof (actionId) == 'object') {
      //console.log('actionId',actionId)
      actionId = "AWF-" + actionId.actionId.split('-')[1]
    }

    this.selectedActionId = actionId;
    this.loadWFComponent = true;
    this.loadComponent = false;
    this.addNewActionFormComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }
  datatableTwo() {
    this.chRef.detectChanges();
    const actionworkflowTable: any = $("#actionwfmartix");
    const actionwfListTableCM = this.dataTableService.initializeTable(
      actionworkflowTable,
      `actionWorkFlowCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      actionwfListTableCM,
      `cmactionworkflowList-select-all`
    );
    this.dataTableFuncService.columnDropDown(actionwfListTableCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(actionwfListTableCM);
    $("#actionwfmartix tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#cmactionworkflowList-select-all").prop("checked", false);
          var el = $("#cmactionworkflowList-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#actionwfmartix tr td input[type='checkbox']:checked").length ==
          $("#actionwfmartix tr td input[type='checkbox']").length
        ) {
          $("#cmactionworkflowList-select-all").prop("checked", true);
        }
      }
    );

  }
  Setting(e) {
    $('.dropdown-menu-column').toggleClass("show-bar");
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $('.dropdown-menu-column').removeClass("show-bar");
    });
  }

}
