import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionWorkflowMatrixComponent } from './action-workflow-matrix.component';

describe('ActionWorkflowMatrixComponent', () => {
  let component: ActionWorkflowMatrixComponent;
  let fixture: ComponentFixture<ActionWorkflowMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionWorkflowMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionWorkflowMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
