import { Component, OnInit, Input, OnChanges, ViewChild,EventEmitter,Output } from '@angular/core';
import { HttpService } from 'src/app/core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import * as moment from 'moment';
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {DefaultModalComponent} from '../../../../layout/modal/default-modal/default-modal.component';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
declare var $;
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ConfigService } from 'src/config.service';
// import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-wf-action-form',
  templateUrl: './wf-action-form.component.html',
  styleUrls: ['./wf-action-form.component.less']
})
export class WfActionFormComponent implements OnInit, OnChanges {
  @ViewChild('wfActionForm', { static: false }) wfActionForm: TabsetComponent;
  @Output()closedAddDefinationActionForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  position;
  currentDate = new Date();
  calendarIconApprovalEnd = true;
  calendarIconApproval = true;
  public Editreminder = false;
  approvalSec = false;
  approvalSecEnd = false;
  calendarIcon = true;
  calendarDetail = false;
  calendarIconEnd = true;
  calendarDetailEnd = false;
  actionList:any;
  actionTableList:any;
  newWF:Boolean;
  form = new FormGroup({
    dateYMD: new FormControl(new Date()),
    dateFull: new FormControl(new Date()),
    dateMDY: new FormControl(new Date()),
    dateRange: new FormControl([
      new Date(),
      new Date(this.currentDate.setDate(this.currentDate.getDate() + 7))
    ])
  });
 
// tslint:disable-next-line: no-input-rename
  @Input('actionId') actionId:string;
  modalRef: BsModalRef;

  public validateFlag: string;
  public validateFlag2: string;
  actionWorkflowFormData: any;
  typingName='';
  result: string[];
  keyword = 'UserName';
  userList=[];
  userIdsEx=[];
  userIdsAp=[];
  actionTable:any;

  constructor(private httpservice: HttpService,private modalService: BsModalService, private formBuilder: FormBuilder,
    private mScrollbarService: MalihuScrollbarService, private configService:ConfigService) {
  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
  }

  ngOnChanges(){
    let now = moment().format('LLLL');
    // console.log('date time: ' , now);
    this.getActionWFData();
    this.getActionData();
  }

  getActionWFData(){
    let getAction = {actionWFId: this.actionId};
    this.httpservice.securePost(ConstUrls.getOneActionWorkflow, getAction).subscribe((data:any)=>{
      // console.log('noun',data)
      if(data){
        this.newWF=false;
        this.calendarIconApprovalEnd = false;
        this.calendarIconApproval = false;
        this.calendarIcon = false;
        this.calendarIconEnd = false;
        this.actionWorkflowFormData = data; this.actionWorkflowFormData.exStart= new  Date(this.actionWorkflowFormData.exStart).toUTCString();
      //  console.log('checking dates',this.actionWorkflowFormData)
        this.actionWorkflowFormData.exEnd= new  Date(this.actionWorkflowFormData.exEnd).toUTCString();
        
        this.actionWorkflowFormData.apStart= new  Date(this.actionWorkflowFormData.apStart).toUTCString();
        this.actionWorkflowFormData.apEnd= new  Date(this.actionWorkflowFormData.apEnd).toUTCString();
        this.getDeadlines();
        // console.log(data);
      }
      else
      if(data==null){
        this.actionWorkflowFormData=[]
        this.actionWorkflowFormData.actionWFId= this.actionId;
        this.actionWorkflowFormData.execution=0;this.actionWorkflowFormData.approval=0;
        this.actionWorkflowFormData.exAssignee=[],
        this.actionWorkflowFormData.apAssignee=[],
        this.actionWorkflowFormData.corporate='',
        this.actionWorkflowFormData.companyName='',
        this.actionWorkflowFormData.actionWorkFlowName=''

        this.actionWorkflowFormData.buName='',
        this.actionWorkflowFormData.approvedBy='',
        this.actionWorkflowFormData.approvedId=''
        this.newWF= true;
        this.calendarIconApprovalEnd = true;
        this.calendarIconApproval = true;
        this.calendarIcon = true;
        this.calendarIconEnd = true;
        this.actionWorkflowFormData.lastChangeOn= new Date().toLocaleDateString();
        this.actionWorkflowFormData.addedOn= new Date().toLocaleDateString();
      }
    })
  }
  getDeadlines(){
    this.actionWorkflowFormData.beforefive= moment(moment(this.actionWorkflowFormData.exStart).subtract(5, 'days')).format('DD-MMM-YYYY');
    this.actionWorkflowFormData.beforefiveAp= moment(moment(this.actionWorkflowFormData.apStart).subtract(5, 'days')).format('DD-MMM-YYYY');

    this.actionWorkflowFormData.afterfive= moment(moment(this.actionWorkflowFormData.exStart).add(5, 'days')).format('DD-MMM-YYYY');
    this.actionWorkflowFormData.afterfiveAp= moment(moment(this.actionWorkflowFormData.apStart).add(5, 'days')).format('DD-MMM-YYYY');

    this.actionWorkflowFormData.afterfourteen= moment(moment(this.actionWorkflowFormData.exStart).add(14, 'days')).format('DD-MMM-YYYY');
    this.actionWorkflowFormData.afterfourteenAp= moment(moment(this.actionWorkflowFormData.apStart).add(14, 'days')).format('DD-MMM-YYYY');

  }
  save(){
    if((this.actionWorkflowFormData.execution+this.actionWorkflowFormData.approval)>100){
      const initialState = {
              title: 'Alert',
              content: 'Total of Execution and Approval % value is more than 100',
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
        return;     
    }
    if(this.actionWorkflowFormData.exDuration<0 || this.actionWorkflowFormData.apDuration<0){
      const initialState = {
              title: 'Alert',
              content: 'Start date should be less than End Date!',
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
        return;     
    }
    if(this.actionWorkflowFormData.exStart)this.actionWorkflowFormData.exStart= new Date(this.actionWorkflowFormData.exStart).toISOString();
    if(this.actionWorkflowFormData.apStart)this.actionWorkflowFormData.apStart=new Date(this.actionWorkflowFormData.apStart).toISOString();
    if(this.actionWorkflowFormData.exEnd)this.actionWorkflowFormData.exEnd=new Date(this.actionWorkflowFormData.exEnd).toISOString();
    if(this.actionWorkflowFormData.apEnd)this.actionWorkflowFormData.apEnd=new Date(this.actionWorkflowFormData.exEnd).toISOString();
    this.actionWorkflowFormData.lastChangeOn= new Date().toLocaleDateString();
    this.actionWorkflowFormData.addedOn= new Date().toLocaleDateString();
    if(this.actionWorkflowFormData.approvedOn) this.actionWorkflowFormData.approvedOn= new Date(this.actionWorkflowFormData.approvedOn).toLocaleDateString();
if(this.actionWorkflowFormData._id)
    delete this.actionWorkflowFormData['_id']
    // console.log(this.actionWorkflowFormData);
    var data= this.actionWorkflowFormData
    var payload={
"exAssignee":data.exAssignee,
"apAssignee": data.apAssignee,
"actionIds": data.actionIds,
"actionWFId": data.actionWFId,
"execution": data.execution,
"exStart":data.exStart,
"exEnd": data.exEnd,
"exDuration": data.exDuration,
"approval": data.approval,
"apStart":data.apStart,
"apEnd": data.apEnd,
"apDuration": data.apDuration,
"corporate":data.corporate,
"companyName": data.companyName,
"buName": data.buName,
"addedOn":data.addedOn,
"lastChangeOn": data.lastChangeOn,
"approvedBy": data.approvedBy,
"approvedId":data.approvedId,
"actionWorkFlowName":data.actionWorkFlowName,

    }
    if(this.newWF){
      //save new WF
      const initialState = {
        title: "Confirmation",
        content: `Do you want to save current changes?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {   this.httpservice.securePost(ConstUrls.saveActionWF,payload).subscribe((response:any)=>{
          if(response) { 
            if(this.actionWorkflowFormData.actionIds){
              var l= this.actionWorkflowFormData.actionIds.length;
              this.actionWorkflowFormData.actionIds.forEach(x=>{
                var payload= {"actionId":x,"actionWFId":this.actionWorkflowFormData.actionWFId}
           this.httpservice.securePost(ConstUrls.updateOneAction,payload).subscribe((response:any)=>{
             if(response){
            //  console.log('Added');
             this.newWF= false;
             if(x==this.actionWorkflowFormData.actionIds[l-1]){
               this.closedAddDefinationActionForm.emit(true);
               const initialState = {
                title: 'Success',
                content: 'Action WF saved Successfully',
                link: 'Ok',
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});  
            
              this.closeModal();
            }
             }
           })
              })
            }
            
           }
        })}
      })
   
    }
    else{

      const initialState = {
        title: "Confirmation",
        content: `Do you want to save current changes?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {     this.httpservice.securePost(ConstUrls.saveActionWorkflow, this.actionWorkflowFormData).subscribe((response:any)=>{
          if(response) { 
            if(this.actionWorkflowFormData.actionIds){
              var l= this.actionWorkflowFormData.actionIds.length;
              this.actionWorkflowFormData.actionIds.forEach(x=>{
                var payload= {"actionId":x,"actionWFId":this.actionWorkflowFormData.actionWFId}
           this.httpservice.securePost(ConstUrls.updateOneAction,payload).subscribe((response:any)=>{
             if(response){
            //  console.log('Added');
             this.newWF= false;
             if(x==this.actionWorkflowFormData.actionIds[l-1]){
               this.closedAddDefinationActionForm.emit(true);
               const initialState = {
                title: 'Success',
                content: `Action WorkFlow ${this.actionId} saved Successfully`,
                link: 'Ok',
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
              //this.wfActionForm.emit(true);
              
            this.closeModal();
      }
             }
           })
              })
            }
          
           }
        }) }
      })
 
    }
    this.userIdsEx.forEach(element => {
   let assginTaskData = {"database" : this.configService.config.db,"ActionID" : this.actionId,"UserID" : element.UserID ,"StartDate" : moment(this.actionWorkflowFormData.exStart).format('DD-MMM-YYYY'),"EndDate" : moment(this.actionWorkflowFormData.exEnd).format('DD-MMM-YYYY'),
   "routerLink":"/rcm/case-management","tab":"action","seen":false,"notice":"Reminder","workType":"Execution"
  }
  // this.httpservice.securePost('sendmail',{username:element.UserName, content:` Action Workflow : ${this.actionId}`,email:element.email}).subscribe((res:any)=>{
  //   console.log('email sent');
  // })
  // console.log('Assign Data: ' , assginTaskData);
  this.httpservice.secureJavaPostApi(ConstUrls.assignActionTask , assginTaskData).subscribe((response: any)=>{
    if(response){
      this.userIdsAp.forEach(el => {
        let assginTaskData = {"database" : this.configService.config.db,"ActionID" : this.actionId,"UserID" : el.UserID ,"StartDate" : moment(this.actionWorkflowFormData.apStart).format('DD-MMM-YYYY'),"EndDate" : moment(this.actionWorkflowFormData.apEnd).format('DD-MMM-YYYY'),
        "routerLink":"/rcm/case-management","tab":"action","seen":false,"notice":"Reminder","workType":"Approval"
      }
      //  console.log('Assign Data: ' , assginTaskData);
       this.httpservice.secureJavaPostApi(ConstUrls.assignActionTask , assginTaskData).subscribe((response: any)=>{
         if(response){
          //  console.log('successfully added assignees');
         }
       })
    })
  }
  })
  
});
  }
  removeUser(user,i){
    this.actionWorkflowFormData.exAssignee.splice(i,1);
    var k = this.userIdsEx.findIndex(x=>x.UserName===user);
  if (k>-1)this.userIdsEx.splice(k,1)
  }
  removeUserAp(user,i){
    this.actionWorkflowFormData.apAssignee.splice(i,1);
    var k = this.userIdsAp.findIndex(x=>x.UserName===user);
  if (k>-1)this.userIdsAp.splice(k,1)
  }
  openSearch() {
    this.validateFlag = 'ValidatorToken';
  }
  openSearchAp() {
    this.validateFlag2 = 'ValidatorToken';
  }
  closeSearch() {
    this.validateFlag = '';
  }
  closeSearchAp() {
    this.validateFlag2 = '';
  }
  onKey(event: any) { // without type info

    // console.log('hii',event.target.value)
  //   this.typingName = event.target.value;
  //  console.log('Find Username: ' , this.typingName);
  
   
  //   const strs = ['sagar', 'pratik', 'sunny', 'sonal'];
  //   this.result = strs.filter(s => s.includes(this.typingName));

  //     console.log('User Array : ' , strs);
  //     console.log('User find: ' , this.result);
     
     
  }

  onChangeSearch(val: string) {
    val=val.toLowerCase()
    let data = {
      "database" : this.configService.config.db,
      "StartDate" : moment(this.actionWorkflowFormData.exStart).format('DD-MMM-YYYY'),
      "EndDate" : moment(this.actionWorkflowFormData.exEnd).format('DD-MMM-YYYY')
    }
    // console.log(data)
    this.httpservice.secureJavaPostApi(ConstUrls.getUserList, data).subscribe((res:any)=>{

      if(res){
        // val= res;
      //  this.userList = res.data.filter(s => s.UserName.includes(val));;
      this.userList = res.data.filter(s => {
        let str= s.UserName.toLowerCase().trim();
       return str.includes(val)
      });;
        // console.log('User List: ', this.userList);
      }
    })
  }

  selectEvent(item) {
   
    if(this.actionWorkflowFormData.exAssignee){
      if(this.actionWorkflowFormData.exAssignee.includes(item.UserName) || item.Available=='false') 
 {
           const initialState = {
            title: 'Alert',
             content: 'Sorry! '+item.UserName+ ' is not available!',
             link: 'Ok',
           };
           
          this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
          }
          else{   
        this.actionWorkflowFormData.exAssignee.push(item.UserName);
        this.userIdsEx.push(item);
          }
    } else{
      if(item.Available=='false'){
        const initialState = {
          title: 'Alert',
           content: 'Sorry! '+item.UserName+ ' is not available!',
           link: 'Ok',
         };
         
        this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
       
      }
      else{
        this.actionWorkflowFormData.exAssignee=[];
        this.actionWorkflowFormData.exAssignee.push(item.UserName);
      }
     
    }
  }
  selectEventAp(item){
    if(this.actionWorkflowFormData.apAssignee){
      if(this.actionWorkflowFormData.apAssignee.includes(item.UserName) || item.Available=='false') 
 {
           const initialState = {
            title: 'Alert',
            content: 'Sorry!'+item.UserName+ ' is not available!',
            link: 'Ok',
           };
           
          this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
          }
          else{
            this.userIdsAp.push(item);
            this.actionWorkflowFormData.apAssignee.push(item.UserName)
          }
    }
    else{
      if(item.Available=='false'){
        const initialState = {
          title: 'Alert',
          content: 'Sorry!'+item.UserName+ ' is not available!',
          link: 'Ok',
         };
         
        this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
      }
      else{
        this.actionWorkflowFormData.apAssignee=[];
        this.actionWorkflowFormData.apAssignee.push(item.UserName)  
      }
  
    }
    
  }
  // onChangeSearch(search: string) {
  //   // fetch remote data from here
  //   // And reassign the 'data' which is binded to 'data' property.
  // }

  onFocused(e) {
    // do something
  }
  
 getActionData(){
  let table = $('#actionWFTable').DataTable();
  table.destroy();
 this.httpservice.secureGet(ConstUrls.getAllActionRecords).subscribe(response => {
      // console.log('get all actions::',response)
      this.actionTable = response;
      this.actionTableList = response;
      if(this.actionWorkflowFormData.actionIds) { 
      this.actionList= this.actionTable.filter(x=>{return this.actionWorkflowFormData.actionIds.includes(x.actionId)})
      this.actionTableList= this.actionTable.filter(x=>
        {return (!(this.actionWorkflowFormData.actionIds.includes(x.actionId)))&& (x.actionWFId==undefined || x.actionWFId=='')})
      }

      else{
        var id="A-"+this.actionId.split('-')[1]
        this.actionWorkflowFormData.actionIds=[];
        this.actionWorkflowFormData.actionIds.push(id)
        this.actionList= this.actionTable.filter(x=>{return this.actionWorkflowFormData.actionIds.includes(x.actionId)})
        this.actionTableList= this.actionTable.filter(x=>

          {return (!(this.actionWorkflowFormData.actionIds.includes(x.actionId)))&& (x.actionWFId==undefined || x.actionWFId=='')})
      }
      this.datatable();
 })
}
addAction(){
  let table= $('#actionWFTable').DataTable();
  let arr=[];
  let checkValue= table.$('input:checked').each(function(){
    arr.push($(this).attr('value'))
  })
  if(arr.length>0){
    arr.forEach(x=>{
      this.actionWorkflowFormData.actionIds.push(x);

    })
    this.actionList= this.actionTable.filter(x=>{return this.actionWorkflowFormData.actionIds.includes(x.actionId)})
  this.actionTableList= this.actionTableList.filter(x=>
    {return (!(this.actionWorkflowFormData.actionIds.includes(x.actionId)))&& (x.actionWFId==undefined || x.actionWFId=='')})

  }

}
  datatable() {
   setTimeout(()=>{
    $('#actionWFTable').DataTable({
      "oLanguage": { "sSearch": "" }, 
      "language": {
            "searchPlaceholder": "Search"
      },
      "destroy": true,
        "fixedHeader": true,
          "responsive": true,
          "bPaginate": true,
          "lengthMenu": [[5, 10, 20, 50, 100, -1],
          [5, 10, 20, 50, 100, "All"]],
            "bInfo": true,
            "searching": true,
            "lengthChange": true,
            "columnDefs": [{
              'targets': [0],
              'orderable': false,
              'className': 'dt-body-center'
            }],
        });
   },500)}
    

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').addClass('d-none');
    this.position = this.position ? undefined : {x: 0, y: 0};
    this.wfActionForm.tabs[0].active = true;
  }
  onValueChange(value: Date): void {
    this.actionWorkflowFormData.exStart = value;
    this.actionWorkflowFormData.exDuration=(new Date(this.actionWorkflowFormData.exEnd).getTime()-(new Date(this.actionWorkflowFormData.exStart).getTime()))/(1000*3600*24)
    this.actionWorkflowFormData.exDuration= parseInt(this.actionWorkflowFormData.exDuration) 
    this.getDeadlines();
    this.calendarIcon = false;
    this.calendarDetail = true;
  }
  onValueChangeEnd(value: Date): void {
    this.actionWorkflowFormData.exEnd = value;
    this.actionWorkflowFormData.exDuration=(new Date(this.actionWorkflowFormData.exEnd).getTime()-(new Date(this.actionWorkflowFormData.exStart).getTime()))/(1000*3600*24)
    this.actionWorkflowFormData.exDuration= parseInt(this.actionWorkflowFormData.exDuration);
    this.getDeadlines();
    this.calendarIconEnd = false;
    this.calendarDetailEnd = true;
  }
  onValueChangeSec(value: Date): void {
    this.actionWorkflowFormData.apStart = value;
    this.actionWorkflowFormData.apDuration=(new Date(this.actionWorkflowFormData.apEnd).getTime()-(new Date(this.actionWorkflowFormData.apStart).getTime()))/(1000*3600*24)
    this.actionWorkflowFormData.apDuration= parseInt(this.actionWorkflowFormData.apDuration);  
    this.getDeadlines();  
    this.calendarIconApproval = false;
    this.approvalSec = true;
  }

  onValueChangeSecEnd(value: Date): void {
    this.actionWorkflowFormData.apEnd = value;
    this.actionWorkflowFormData.apDuration=(new Date(this.actionWorkflowFormData.apEnd).getTime()-(new Date(this.actionWorkflowFormData.apStart).getTime()))/(1000*3600*24)
    this.actionWorkflowFormData.apDuration= parseInt(this.actionWorkflowFormData.apDuration);  
    this.getDeadlines(); 
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  reminder(){
    this.Editreminder = !this.Editreminder;
  }
  cancelClick(){
    this.Editreminder = false;
  }

 
  
}