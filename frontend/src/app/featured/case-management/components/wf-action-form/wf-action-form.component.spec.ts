import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfActionFormComponent } from './wf-action-form.component';

describe('WfActionFormComponent', () => {
  let component: WfActionFormComponent;
  let fixture: ComponentFixture<WfActionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfActionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfActionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
