import { Component, OnInit, Input, OnChanges, Output, SimpleChanges, EventEmitter, ViewChild } from '@angular/core';
import { HttpService } from "../../../../core/http-services/http.service";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ConstUrls } from 'src/app/config/const-urls';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from '../../../../layout/modal/default-modal/default-modal.component';
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
declare var $;
@Component({
  selector: 'app-issue-monitoring-form',
  templateUrl: './issue-monitoring-form.component.html',
  styleUrls: ['./issue-monitoring-form.component.less']
})
export class IssueMonirotingFormComponent implements OnInit, OnChanges {
  @Output() closedDefinationIssueForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input("issueData") issueData: string;
  @ViewChild('issueMonitoringForm', { static: false }) issueMonitoringForm: TabsetComponent;
  modalRef: BsModalRef;
  issueActionMatrixData: any;
  IssueForm: any;
  issueStatusMaster: any;
  issueTypeMaster: any;
  area: any;
  position;
  filteredControls = [];
  deletedControls: any = [];
  assignedControls: any = [];
  assignedIssue: any;
  allControls = [];
  filteredActions = [];
  deletedActions: any = [];
  assignedActions: any = [];
  fetchedData: any;
  allActions: any;


  issuePriorityMaster: any;
  public enableInput: boolean = true;
  public closeShow = false;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.initForm();
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });

    this.httpservice.get(ConstUrls.getAllActionRecords).subscribe((response: any) => {
      if (response) {

        this.allActions = response;
        this.filteredActions = this.allActions;
        this.filterActions();
      }
    });

    this.datatable();
    this.actiondatatable();
  }


  deleteAction(actionId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this Action?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedActions.push(actionId);
        this.assignedActions = this.assignedActions.filter(
          (actions) => actions.actionId !== actionId
        );
        this.filterActions();
        let table = $('#actionWFTable').DataTable();
        table.destroy();
        this.datatable();
      }
    });
  }

  filterActions() {
    if (this.allActions) {
      const selectedIds = [];
      this.assignedActions.forEach((element) => {
        selectedIds.push(element.actionId);
      });
      this.filteredActions = this.allActions.filter(
        (actions) => selectedIds.indexOf(actions.actionId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addActions() {
    let table = $('#actionWFTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let aId = $(this).attr('value')
      // _this.newCIds.push(cId);
      const assignedActions = _this.allActions.find(
        (action) => action.actionId === aId
      );
      _this.assignedActions.push(assignedActions);
      _this.filterDeletedActions(aId);
    });
    this.filterActions();
    table.destroy();
    this.datatable();
  }
  editTitle() {
    $('.edit-title-name').addClass('no-hover');
    this.enableInput = false;
    this.closeShow = true;
  }

  closeEditTitle() {
    $('.edit-title-name').removeClass('no-hover');
    this.enableInput = true;
    this.closeShow = false;
  }
  ngOnChanges(changes: SimpleChanges) {

    let payload = { issueId: this.issueData };
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        this.allControls = response;
        this.filteredControls = response;
      }
    });
    this.httpservice.securePost(ConstUrls.getActions, payload).subscribe((response: any) => {
      //console.log('recevied issues::',response)
      if (response) {
        this.assignedActions = response;
        this.filterActions();
        this.actiondatatable();
      }
    })
    // getControlsFromIssue
    this.httpservice.securePost(ConstUrls.getControlsFromIssue, payload).subscribe((response: any) => {
      if (response) {
        this.assignedControls = response;
        this.filterControls();
      }
    })
    this.httpservice.securePost(ConstUrls.getOneIssues, payload).subscribe(async response => {

      this.issueActionMatrixData = await response;
      //console.log(this.issueActionMatrixData);
      this.initForm();
      this.IssueForm.patchValue(response);

      this.IssueForm.patchValue({
        dateIdentified: new Date(
          this.issueActionMatrixData.dateIdentified
        ),
        dateApproved: new Date(
          this.issueActionMatrixData.dateApproved
        )
      }
      )

      this.httpservice.get(ConstUrls.issueStatusMaster).subscribe((response: any) => {

        this.issueStatusMaster = response;
      });

      this.httpservice.get(ConstUrls.issueTypeMaster).subscribe((response: any) => {
        this.issueTypeMaster = response;
      });

      this.httpservice.get(ConstUrls.issuePriorityMaster).subscribe((response: any) => {
        this.issuePriorityMaster = response;
      });

      this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
        this.area = response;
      });
    });
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedControls.push(controlId);
        //console.log("remove control:", controlId);
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== controlId
        );
        this.filterControls();
        let table = $('#actioncontrolWFTable').DataTable();
        table.destroy();
        this.actiondatatable();
      }
    });
  }

  filterControls() {
    if (this.allControls) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addControl() {
    //console.log(this.assignedControls);
    let table = $('#actioncontrolWFTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let cId = $(this).attr('value')
      let assignedControl = _this.allControls.find(
        (control) => control.controlId === cId
      );
      _this.assignedControls.push(assignedControl);
      _this.filterDeletedControls(cId);
    }); 
    this.filterControls();
    table.destroy();
    this.actiondatatable();
  }
  //filter assigned action
  filterDeletedActions(actionId) {
    if(this.deletedActions.indexOf(actionId) > -1) {
      this.deletedActions = this.deletedActions.filter((aId) => {
        return aId != actionId
      });
    }
  }
  filterDeletedControls(controlId) {
    if(this.deletedControls.indexOf(controlId) > -1) {
      this.deletedControls = this.deletedControls.filter((cId) => {
        return cId != controlId
      });
    }
  }


  initForm() {
    this.IssueForm = this.formBuilder.group({
      action: ['', []],
      area: ['', []],
      department: ['', []],
      issueDescription: ['', []],
      issueId: ['', []],
      issueName: ['', []],
      keyIsuue: ['false', []],
      managementResponse: ['', []],
      priority: ['', []],
      recommendation: ['', []],
      reviewComments: ['', []],
      approvedBy: ['', []],
      approverId: ['', []],
      dateApproved: ['', []],
      dateIdentified: ['', []],
      identifiedBy: ['', []],
      identifierId: ['', []],
      issueType: ['', []],
      issuePriority: ['', []],
      issueStatus: ['', []],
      actionId: [],
      controlId: []

    });
  }



  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

    if (this.IssueForm.value) {
      //console.log('Form Data:' , this.IssueForm.value);
      this.IssueForm.patchValue({ 'issueId': this.issueData });
      const actionIds = [];
      this.assignedActions.forEach(x => {
        actionIds.push(x.actionId)
      });
      this.IssueForm.patchValue({ "actionId": actionIds });
      let controlId = [];
      this.assignedControls.forEach(x => {
        controlId.push(x.controlId);
      })
      this.IssueForm.patchValue({ "controlId": controlId });
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          if (this.deletedActions.length) {
            let postData = { actionId: this.deletedActions, issueId: this.IssueForm.value.issueId }
            this.httpservice.securePost(ConstUrls.removeOneIssueFromAction, postData).subscribe((res) => {
            });
          }
          if (this.deletedControls.length) {
            let postData = { controlId: this.deletedControls, issueId: this.IssueForm.value.issueId }
            this.httpservice.securePost(ConstUrls.removeIssueFromControl, postData).subscribe((res) => {
            });
          }
          this.httpservice.securePost(ConstUrls.updateIssueWithAction, this.IssueForm.value).subscribe((response: any) => {
            if (response) {
              const initialState = {
                title: 'Success',
                content: 'Issue : ' + this.issueData + 'updated Successfully',
                link: 'Ok',
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
              this.closedDefinationIssueForm.emit(true);
              this.closeModal();
            }
          })
        }
      })

    }

  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').addClass('d-none');
    this.issueMonitoringForm.tabs[0].active = true;
    this.deletedActions = [];
    this.deletedControls = [];
    this.position = this.position ? undefined : { x: 0, y: 0 }
  }


  datatable() {
    setTimeout(() => {
      $('#actionWFTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }


  actiondatatable() {
    setTimeout(() => {
      $('#actioncontrolWFTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }


}

