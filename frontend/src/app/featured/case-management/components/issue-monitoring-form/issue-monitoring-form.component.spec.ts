import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueMonirotingFormComponent } from './issue-monitoring-form.component';

describe('IssueDefinitionFormComponent', () => {
  let component: IssueMonirotingFormComponent;
  let fixture: ComponentFixture<IssueMonirotingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueMonirotingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueMonirotingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
