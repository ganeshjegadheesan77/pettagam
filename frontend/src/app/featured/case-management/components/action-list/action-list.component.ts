import { Component, OnInit , ChangeDetectorRef } from '@angular/core';
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import * as moment from 'moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {NotificationService} from '../../../../notification.service'
declare var $;
import {CaseManagementServiceService} from '../../service/case-management-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.less']
})
export class ActionListComponent implements OnInit {
  modalRef: BsModalRef;
  public printAction = false;

public loadComponent = false;
private loadcityId = false;
public addNewActionFormComponent=false;
subscription: Subscription;

dataTable: any;
actionTableList:any;
selectedActionId: any;
selectedactionkId:any;
loadWFComponent: boolean;
actionprogress:any;

constructor(private httpService: HttpService,
  private modalService: BsModalService, 
  private chRef: ChangeDetectorRef,
  private notification:NotificationService,
  private caseMgmtService:CaseManagementServiceService,
  private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) {
     // subscribe to issue list component messages
     this.subscription = this.caseMgmtService.fromIssueActionMatrixRefreshedActionTable().subscribe(message => {
      if (message) {
 
        this.ngOnInit();
      } 
    });
   }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
      this.getActionList();

    } 
getStatusOfAddNewActionForm(event){
  //console.log(event)
  if(event === true){
    this.getActionList();
  }

}
getAddressOfAddNewActionForm
(event){
  //console.log(event)
  if(event === true){
   this.caseMgmtService.fromActionRefreshIAM('Refresh IAM');

    this.getActionList();
  }

}

popupAction(){
  this.loadComponent = false;
  this.loadcityId = false;
  this.printAction = true;
  
  $(".scroll-container").scrollTop(0);
  $(".pop-up-form").removeClass("d-none");
}





loadMyChildComponent(id){
  console.log(id);
  this.selectedactionkId = id;
  this.loadComponent = true;
  this.loadcityId = true;
  this.printAction = false;

  this.addNewActionFormComponent=false;

  $(".pop-up-form").removeClass("d-none");
}

addNewActionForm(){
this.addNewActionFormComponent= true;
this.loadcityId = false;
this.printAction = false;
 
 this.loadComponent = false;
$(".pop-up-form").removeClass("d-none");
}

getActionList(){
  // console.log('getActionList');

  let table = $('#actionListCM').DataTable();
  table.destroy();
   this.httpService.secureGet(ConstUrls.getAllActionRecords).subscribe(response => {
      // console.log('get all actions::',response)
      this.actionTableList = response;
      for (let i = 0; i < this.actionTableList.length; i++) {
        
        // let element = ;
        this.actionTableList[i].newModifiedDate = moment(this.actionTableList[i].dateExecuted).format('DD/MM/YYYY');
        var date = new Date(this.actionTableList[i].dateExecuted);
        // console.log('dateExecuted date',moment(this.actionTableList[i].dateExecuted).format('DD/MM/YYYY') )
       
        this.actionTableList[i].lastModifiedDate = moment(this.actionTableList[i].dateApproved).format('DD/MM/YYYY');
        var date = new Date(this.actionTableList[i].dateApproved);
        // console.log('dateApproved date',moment(this.actionTableList[i].dateApproved).format('DD/MM/YYYY') )
        date.toString() 
        
      }
      this.actionTableList.forEach(x=>{
        x.barColor= x.actionprogress>=0 && x.actionprogress<=33  ? "very-high"  : x.actionprogress>=34 && x.actionprogress<=66 ?"very-low" :"medium";
         x.riskStatus = x.actionprogress>=0 && x.actionprogress<=33  ? "CRITICAL"  : x.actionprogress>=34 && x.riskProgress<=66 ? "NORMAL" : "NORMAL";
        x.ratingClr =( x.rating =='Critical')? "very-high"  : (x.rating == 'Normal') ? 'very-low':'Low'
      });

      
     this.datatableTwo();
    })
  


}

datatableTwo() {
    this.chRef.detectChanges();
    const actionTable: any = $("#actionListCM");
    const actionListTableCM = this.dataTableService.initializeTable(
      actionTable,
      `actionlistCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      actionListTableCM,
      `cmactionList-select-all`
    );
    this.dataTableFuncService.columnDropDown(actionListTableCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(actionListTableCM);
    $("#actionListCM tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#cmactionList-select-all").prop("checked", false);
          var el = $("#cmactionList-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#actionListCM tr td input[type='checkbox']:checked").length ==
          $("#actionListCM tr td input[type='checkbox']").length
        ) {
          $("#cmactionList-select-all").prop("checked", true);
        }
      }
    );
  }
  Setting(e){
    $('.dropdown-menu-column').toggleClass("show-bar");
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function() {
      $('.dropdown-menu-column').removeClass("show-bar");
    });
  }
}
