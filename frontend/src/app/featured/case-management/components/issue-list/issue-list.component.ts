import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import * as moment from 'moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from '../../../../layout/modal/default-modal/default-modal.component';
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import { Subscription } from 'rxjs';
import { CaseManagementServiceService } from '../../service/case-management-service.service';
declare var $;

// type NewType = AuthService;

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.less']
})
export class IssueListComponent implements OnInit {
  subscription: Subscription;

  formatdate = 'dd/MM/yyyy';
  today: number = Date.now();
  modalRef: BsModalRef;

  private loadComponent = false;
  public addNewIssueFormComponent = false;
  public loadcityId = false;
  issueTableList: any;
  selectedIssueId: any;
  date: Date;
  isLoading = false;

  constructor(private caseMgmtService: CaseManagementServiceService, private httpService: HttpService, private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService, private modalService: BsModalService, private chRef: ChangeDetectorRef) {
    // subscribe to issue list component messages
    this.subscription = this.caseMgmtService.fromIssueActionMatrixRefreshedIssueable().subscribe(message => {
      if (message) {
        //console.log('Message: ', message);
        let table = $('#issueListCM').DataTable();
        table.destroy();
        this.getIssueList();
      }
    });
  }
  ngOnInit() {
    this.getIssueList();
    // console.log(new Date().toISOString())
  }

  getStatusOfAddNewIssueForm(event) {
    if (event === true) {
      this.getIssueList();
    }
    this.date = new Date();
  }

  getAddressOfAddNewIssueForm(event) {
    if (event === true) {
      this.caseMgmtService.fromIssueRefreshIAM('Refresh IAM');

      this.getIssueList();
    }
    this.date = new Date();
  }

  loadMyCityId(isId) {
    //console.log(isId);
    this.selectedIssueId = isId;
    this.loadcityId = true;
    this.loadComponent = true;
    this.addNewIssueFormComponent = false;
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').removeClass('d-none');
    $(".tabOne").trigger('click');
  }
  getIssueList() {
    this.isLoading = true;
    let table = $('#issueListCM').DataTable();
    table.destroy();
    this.httpService.secureGet(ConstUrls.getAllIssuesRecords).subscribe((response) => {
      //console.log('get all issues::',response)
      this.issueTableList = response;
      for (let i = 0; i < this.issueTableList.length; i++) {

        // let element = ;
        this.issueTableList[i].newModifiedDate = moment(this.issueTableList[i].dateIdentified).format('DD/MM/YYYY');
        var date = new Date(this.issueTableList[i].dateIdentified);
        // console.log('identified date',moment(this.issueTableList[i].dateIdentified).format('DD/MM/YYYY') )

        this.issueTableList[i].lastModifiedDate = moment(this.issueTableList[i].dateApproved).format('DD/MM/YYYY');
        var date = new Date(this.issueTableList[i].dateApproved);
        // console.log('dateApproved date',moment(this.issueTableList[i].dateApproved).format('DD/MM/YYYY') )
        date.toString()

      }
      this.isLoading = false;
      this.datatableTwo();
    });
  }

  addNewIssueForm() {
    this.addNewIssueFormComponent = true;
    this.loadcityId = false;
    $('.pop-up-form').removeClass('d-none');
  }


  datatableTwo() {
    this.chRef.detectChanges();
    const issueTable: any = $("#issueListCM");
    const issueListTableCM = this.dataTableService.initializeTable(
      issueTable,
      `issueListCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      issueListTableCM,
      `issue-listCM-select-all`
    );
    this.dataTableFuncService.columnDropDown(issueListTableCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(issueListTableCM);
    $("#issueListCM tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issue-listCM-select-all").prop("checked", false);
          var el = $("#issue-listCM-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#issueListCM tr td input[type='checkbox']:checked").length ==
          $("#issueListCM tr td input[type='checkbox']").length
        ) {
          $("#issue-listCM-select-all").prop("checked", true);
        }
      }
    );
  }
}







