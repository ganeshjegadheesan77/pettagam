import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueDefinitionFormComponent } from './issue-definition-form.component';

describe('IssueDefinitionFormComponent', () => {
  let component: IssueDefinitionFormComponent;
  let fixture: ComponentFixture<IssueDefinitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueDefinitionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueDefinitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
