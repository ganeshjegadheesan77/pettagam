import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ConstUrls } from '../../../../../../src/app/config/const-urls';
import { DefaultModalComponent } from '../../../../layout/modal/default-modal/default-modal.component';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { FilePreviewComponent } from "../../../../layout/collaboration-elements/components/in-tray/file-preview/file-preview.component";
// import { environment } from 'src/environments/environment';
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { CaseManagementServiceService } from '../../service/case-management-service.service';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { CoreEnvironment } from '@angular/compiler/src/compiler_facade_interface';
import { HttpService } from 'src/app/core/http-services/http.service';
import { ConfigService } from 'src/config.service';
declare var $;
@Component({
  selector: 'app-issue-definition-form',
  templateUrl: './issue-definition-form.component.html',
  styleUrls: ['./issue-definition-form.component.less']
})
export class IssueDefinitionFormComponent implements OnInit, OnChanges {
  @Input("actionData") actionData: string;
  @ViewChild('issueDefinitionForm', { static: false }) issueDefinitionForm: TabsetComponent;
  @Output() closedAddDefinationActionForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() closedEditActionForm: EventEmitter<boolean> = new EventEmitter<boolean>();


  // @Output()closedRiskDefForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  modalRef: BsModalRef;
  uri;
  filteredIssues: any = [];
  assignedIssues: any = [];
  deletedIssues = [];
  allIssues = [];
  issueActionMatrixData: any;
  actionForm: FormGroup;
  actionStatusMaster: any;
  fetchedData: any;
  uploadedFilesArr: any;
  area: any;
  ac_attachments: any
  multiFile: any = [];
  actionRatingMaster: any;
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  deletedAttachment: any = [];
  position;
  public enableInput: boolean = true;
  public closeShow = false;
  closeAttachment: boolean;
  // allActions: any;
  // assignedActions: any;

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private caseMgmtService: CaseManagementServiceService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService
    
  ) { 
    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.initForm();
    this.fetchedData = [];
    this.httpservice.get(ConstUrls.getAllIssuesRecords).subscribe((response: any) => {
      if (response) {
        this.allIssues = response;
        this.datatable();
      }
    });
  }

  filterIssues() {
    if (this.allIssues) {
      const selectedIds = [];
      this.assignedIssues.forEach((element) => {
        selectedIds.push(element.issueId);
      });
      this.filteredIssues = this.allIssues.filter(
        (issue) => selectedIds.indexOf(issue.issueId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.attachedFiles = [];
    let payload = { actionId: this.actionData };
    console.log('hey', payload);
    this.httpservice.securePost(ConstUrls.getOneAction, payload).subscribe(async response => {
      this.fetchedData = response
      this.issueActionMatrixData = await response;
      //  console.log(this.issueActionMatrixData);
      this.initForm();
      this.actionForm.patchValue(response);

      // this.issueActionMatrixData=[];
      if (this.fetchedData.ac_attachments.length != 0) {
        this.openAttachment = true;
        for (let i = 0; i < this.fetchedData.ac_attachments.length; i++) {
          let extn = this.fetchedData.ac_attachments[i].substr(this.fetchedData.ac_attachments[i].lastIndexOf('.') + 1);
          let name = this.fetchedData.ac_attachments[i].substr(0, this.fetchedData.ac_attachments[i].lastIndexOf('.'));

          this.attachedFiles.push({
            'relativePath': name,
            'extension': extn, 'fullName': name + '.' + extn, 'new': false
          })
        }
      } else {
        this.openAttachment = false;

      }

      // for(let i=0;i<this.issueActionMatrixData.ac_attachments.length;i++){
      //   this.attachedFiles.push({'relativePath':this.issueActionMatrixData.ac_attachments[i].split('.')[0],
      // 'extension':this.issueActionMatrixData.ac_attachments[i].split('.')[1],'fullName':this.issueActionMatrixData.ac_attachments[i]
      // })
      // }

      console.log('db return attched files::', this.attachedFiles)
      this.actionForm.patchValue({
        performancePeriod: new Date(
          this.issueActionMatrixData.performancePeriod
        ),
        signOffPeriod: new Date(
          this.issueActionMatrixData.signOffPeriod
        ),
        dateExecuted: new Date(
          this.issueActionMatrixData.dateExecuted
        ),
        dateApproved: new Date(
          this.issueActionMatrixData.dateApproved
        )
      }
      )

      this.httpservice.get(ConstUrls.actionStatusMaster).subscribe((response: any) => {
        this.actionStatusMaster = response;
      });

      this.httpservice.get(ConstUrls.actionRatingMaster).subscribe((response: any) => {
        this.actionRatingMaster = response;
      });

      this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
        this.area = response;
      });

      let payload = { actionId: this.actionData };
      this.httpservice.securePost(ConstUrls.getIssue, payload).subscribe((response: any) => {
        //console.log('recevied issues::',response)
        if (response) {
          this.assignedIssues = response;
          this.filterIssues();
          this.datatable();
        }
        else {

          this.attachedFiles = [];

          // this.getOneAction();

        }
      })

    });



  }


  editTitle() {
    $('.edit-title-name').addClass('no-hover');
    this.enableInput = false;
    this.closeShow = true;
  }

  closeEditTitle() {
    $('.edit-title-name').removeClass('no-hover');
    this.enableInput = true;
    this.closeShow = false;
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $('.pop-up-form').addClass('d-none');
    this.deletedIssues = [];
    this.issueDefinitionForm.tabs[1].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 };

  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;

      //console.log('left curr status:' , this.marked)

    } else {
      // attachment left

      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }


  initForm() {
    this.actionForm = this.formBuilder.group({
      actionId: ['', []],
      actionDescription: ['', []],
      approvedBy: ['', []],
      actionName: ['', []],
      approvedId: ['', []],
      area: ['', []],
      comments: ['', []],
      rating: ['', []],
      actionStatus: ['', []],
      correctiveAction: ['', []],
      dateApproved: ['', []],
      dateExecuted: ['', []],
      executedBy: ['', []],
      executerId: ['', []],
      ac_attachments: ["", []],
      guidance: ['', []],
      issueId: [[]],
      controlKeyFlag: [false, []],

      performancePeriod: ['', []],
      signOffPeriod: ['', []],
    });
  }

  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {

      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        var extn = ext.relativePath.substr(ext.relativePath.lastIndexOf('.') + 1);
        var name = ext.relativePath.substr(0, ext.relativePath.lastIndexOf('.'));
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        //  let extension = ext.relativePath.split('.')[1];
        ext.extension = extn;



        //console.log('Updated File Name:' ,ext.relativePath , 'File Extension:' , ext.extension);

      }
      // if(ext.relativePath){
      //   // console.log('relative: ' , ext.relativePath);
      //    let extension = ext.relativePath.split('.')[1];
      //     ext.extension = extension;
      // }
    }
    //console.log('upload files array :  ' , this.uploadedFilesArr);
    //console.log('files array: ' , this.files);
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file)
          let extn = file.name.substr(file.name.lastIndexOf('.') + 1);
          let name = file.name.substr(0, file.name.lastIndexOf('.'));

          this.attachedFiles.push({
            'relativePath': name.replace(/\W+/g, "-") + '.' + extn,
            'extension': extn, 'fullName': file.name, 'new': true
          })
          //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  addIssue () {
    let table = $('#issueWFTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let issueId = $(this).attr('value')
      // _this.newCIds.push(cId);
      const assignedIssue = _this.allIssues.find(
        (issue) => issue.issueId === issueId
      );
      _this.assignedIssues.push(assignedIssue);
      _this.filterDeletedIssues(issueId);
    });
    this.filterIssues();
    table.destroy();
    this.datatable();
  }
  deleteIssue(issueId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove ${issueId} ?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedIssues.push(issueId);
        this.assignedIssues = this.assignedIssues.filter(
          (issue) => issue.issueId !== issueId
        );
        this.filterIssues();
        let table = $('#issueWFTable').DataTable();
        table.destroy();
        this.datatable();
      }
    });
  }
  removeAttachment(removeitem, index) {
    this.closeAttachment = false;
    if (removeitem.new == true) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);

      let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
      if (idx != -1) this.multiFile.splice(idx, 1);

    } else {
      const fileindex = parseInt(index);
      let payload = {
        directory: "uploads/caseMgmt",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            /*console.log(
              "attached files array",
              this.attachedFiles,
              "index from method",
              fileindex
            );*/
            this.attachedFiles.splice(fileindex, 1);

            this.fetchedData.ac_attachments = [];
            this.attachedFiles.forEach((x) => {
              this.fetchedData.ac_attachments.push(x.fullName);
            });
            // console.log(this.fetchedData.attachment);
            let payload = {
              actionId: this.actionData,
              fileName: removeitem.fullName,
            };
            this.httpservice
              .securePost(ConstUrls.deleteAIFile, payload)
              .subscribe((response: any) => {
                if (response) {
                  console.log("yessss");
                }
              });
            setTimeout(function () {
              // console.log(this.attachedFiles);
            }, 3000);
            if (response) {
              const initialState = {
                title: "Success",
                content: "Attachment Deleted Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              // this.closedAddDefinationActionForm.emit(true);
              // this.closeModal();
            }
          }
        });
    }

    //  /api/removeAttachment
  }




  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    //console.log('this is data',data)
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]['name']);
      if (this.fetchedData.ac_attachments) {
        this.fetchedData.ac_attachments.push(data[i]['name'])
      }
      else {
        this.fetchedData.ac_attachments = [];
        this.fetchedData.ac_attachments.push(data[i]['name'])

      }
    }
    let arr = []
    arr = this.fetchedData.ac_attachments;
    //console.log('attchment array',this.fetchedData)
    this.actionForm.patchValue({ 'ac_attachments': arr });

    let filearr = [];
    arr.forEach(ele => {
      filearr.push('caseMgmt/' + ele)
    });
    let payload = {
      "collection": "document",
      "module": "case-management",
      "sub-module": "Action-list",
      "tab": "Action-list",
      "router-link": "/rcm/case-management",
      "file": filearr.toString(),

      "metadata": "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      "username": localStorage.getItem('username'),
      "database": this.configService.config.db
    }


    this.httpservice.secureJavaPostReportApi('DocumentManagementService/put', payload).subscribe(files => {
      if (files) {
        console.log('Response', files);
      }
      this.multiFile = [];
    })


    this.httpservice.securePost('multipleuploadfilecase', formData).subscribe(files => {
      console.log('Uploaded', this.files);
      this.multiFile = [];
    })

  }
  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: 'Alert',
        content: 'Please save the form to view the attachment loaded.',
        link: 'Ok',
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
    }
    else {
      window.open(this.uri + "uploads/caseMgmt/" + file.fullName, "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400");
    }
    let payload = {
      "database": this.configService.config.db,
      "file": "caseMgmt/" + file.fullName

    }

    this.httpservice.secureJavaPostReportApi('DocumentManagementService/get', payload).subscribe((files: any) => {
      //  console.log('Uploaded');
      if (files) {
        console.log(' Response', files);
      }
      this.multiFile = [];
    })

  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }

  /// condition in Control ///////
  // filterActions() {
  //   if (this.allIssues) {
  //     const selectedIds = [];
  //     this.assignedIssues.forEach((element) => {
  //       selectedIds.push(element.actionId);
  //     });
  //     this.filterActions = this.allIssues.filter(
  //       (actions) => selectedIds.indexOf(actions.actionId) === -1
  //     );
  //     // console.log("Filtered Controls:", this.filterControls);
  //   }
  // }
  filterDeletedIssues(issueId) {
    if(this.deletedIssues.indexOf(issueId) > -1) {
      this.deletedIssues = this.deletedIssues.filter((iId) => {
        return iId != issueId
      });
    }
  }
  onSave() {
    this.attachedFiles.forEach((x) => {
      x.new = false;
    });

    this.upload();
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    //console.log('formdData',this.actionForm.value)

    if (this.actionForm.value) {
      let issueId = [];
      this.assignedIssues.forEach(x => {
        issueId.push(x.issueId);
      })
      this.actionForm.patchValue({ "issueId": issueId });
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          if (this.deletedIssues.length) {
            let postData = { issueId: this.deletedIssues, actionId: this.actionForm.value.actionId }
            this.httpservice.securePost(ConstUrls.removeActionFromIssue, postData).subscribe((res) => {
            });
          }
          this.httpservice.securePost(ConstUrls.updateOneAction, this.actionForm.value).subscribe((response: any) => {

            this.upload();
            if (response) {

              const initialState = {
                title: 'Success',
                content: 'Action : ' + this.actionData + ' updated Successfully',
                link: 'Ok',
              };

              this.modalRef = this.modalService.show(DefaultModalComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
              this.closedEditActionForm.emit(true);
              this.closedAddDefinationActionForm.emit(true);
              this.closeModal();
            }

          })
        }
      })


    }


  }


  datatable() {
    setTimeout(() => {
      $('#issueWFTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }


}


