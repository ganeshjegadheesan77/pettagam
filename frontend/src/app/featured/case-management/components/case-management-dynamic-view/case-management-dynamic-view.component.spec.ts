import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaseManagementDynamicViewComponent } from './case-management-dynamic-view.component';

describe('CaseManagementDynamicViewComponent', () => {
  let component: CaseManagementDynamicViewComponent;
  let fixture: ComponentFixture<CaseManagementDynamicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseManagementDynamicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseManagementDynamicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
