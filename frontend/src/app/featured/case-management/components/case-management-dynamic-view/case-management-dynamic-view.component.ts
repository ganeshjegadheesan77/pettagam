import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import { DataService } from 'src/app/featured/internal-audit/components/audit-universe/data.service';

@Component({
  selector: 'app-case-management-dynamic-view',
  templateUrl: './case-management-dynamic-view.component.html',
  styleUrls: ['./case-management-dynamic-view.component.less']
})
export class CaseManagementDynamicViewComponent implements OnInit {
  actionCount:any;
  issueCount:any;
  doughnutChart: object;
  area=[]; ctx: any;
  dashboardCount;
  canvas: any; credit:number=0;capital:number=0;market:number=0;sec:number=0;HR:number=0;creditIssue:number=0;capitalIssue:number=0;marketIssue=0;securityIssue=0;
  constructor(private httpService: HttpService, private dataService: DataService) { }

  ngOnInit() {

    this.dataService.currentmessage.subscribe(data => {
      //console.log('data', data)
      this.dashboardCount = data;

    })

    this.httpService.secureGet(ConstUrls.getNumberofActions).subscribe(response => {
      //console.log('action count::',response)
      this.actionCount = response;
      this.httpService.secureGet(ConstUrls.getNumberofIssues).subscribe(response => {
        //console.log('action count::',response)
        this.issueCount = response;
      
      });
  

});
this.httpService
.secureGet(ConstUrls.getAllActionRecords)
.subscribe((response) => {
  // console.log("Assessment response:", response);
  var assessment:any= response;
    assessment.forEach(element => {
      if(element.area=='Credit') ++this.credit;
      else
      if(element.area=='Capital') ++this.capital;
      else
      if(element.area=='Market') ++this.market;
      else
      if(element.area=='Security') ++this.sec
    });
  
    setTimeout(()=>{
      
    this.getCharts();
    },100)
});

/////////////////////
this.httpService
.secureGet(ConstUrls.getAllIssuesRecords)
.subscribe((response) => {
  //console.log("iSSUE response:", response);
  var assessment:any= response;
    assessment.forEach(element => {
      if(element.area=='Credit') ++this.creditIssue;
      else
      if(element.area=='Capital') ++this.capitalIssue;
      else
      if(element.area=='Market') ++this.marketIssue;
      else
      if(element.area=='Security') ++this.securityIssue;
      
    });
  
    setTimeout(()=>{
      
    this.getIssueCharts();
    },100)
    
});

  



         
}
  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of doughnut element of variable
    this.canvas = <HTMLCanvasElement>document.getElementById("doughnut");
    this.ctx = this.canvas.getContext("2d");
    // And for a doughnut chart
    this.doughnutChart = new Chart(this.ctx, {
      type: "doughnut",
      data: {
        labels: ["Action", "Action", "Action", "Action"],
        datasets: [
          {
            //label: '',
            data: [
            this.capital,this.credit,this.market,this.sec
            ],
            backgroundColor: ["#664da0", "#dd6838", "#ffcc00", "#98aa3a"],
            borderColor: ["#664da0", "#dd6838", "#ffcc00", "#98aa3a"],
            borderWidth: 1,
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
        },
        // cutoutPercentage: 40,
        responsive: false,
        legend: {
          position: "",
          label: {
            display: false,
          },
          display: false,
        },
      },
    });

  }


  ////////////
  getIssueCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of doughnut element of variable
    this.canvas = <HTMLCanvasElement>document.getElementById("doughnut1");
    this.ctx = this.canvas.getContext("2d");
    // And for a doughnut chart
    this.doughnutChart = new Chart(this.ctx, {
      type: "doughnut",
      data: {
        labels: ["Issue", "Issue", "Issue", "Issue"],
        datasets: [
          {
            //label: '',
            data: [
            this.capitalIssue,this.creditIssue,this.marketIssue,this.securityIssue
            ],
            backgroundColor: ["#664da0", "#dd6838", "#ffcc00","#98aa3a"],
            borderColor: ["#664da0", "#dd6838", "#ffcc00","#98aa3a"],
            borderWidth: 1,
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
        },
        // cutoutPercentage: 40,
        responsive: false,
        legend: {
          position: "",
          label: {
            display: false,
          },
          display: false,
        },
      },
    });

  }
}
