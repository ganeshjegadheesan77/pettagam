import { Component, OnInit, Output, Input, EventEmitter, OnChanges, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/core/http-services/http.service';
// import { Component, OnInit, Output, EventEmitter,  } from '@angular/core';

import { ConstUrls } from 'src/app/config/const-urls';
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from '../../../../layout/modal/default-modal/default-modal.component';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { TabsetComponent } from 'ngx-bootstrap/tabs';
declare var $;
@Component({
  selector: 'app-add-new-issue',
  templateUrl: './add-new-issue.component.html',
  styleUrls: ['./add-new-issue.component.less']
})
export class AddNewIssueComponent implements OnInit, OnChanges {
  @Output() closedAddNewIssueForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input("issueData") issueData: string;
  @ViewChild('addNewIssue', { static: false }) addNewIssue: TabsetComponent;
  modalRef: BsModalRef;
  generatedIssueId: any;
  identifiedDate: any;
  position;
  IssueForm: FormGroup;
  issueActionMatrixData: any;
  issueStatusMaster: any;
  area: any;
  issuePriorityMaster: any;
  issueTypeMaster: any;
  allActions: any = [];
  filteredActions: any = [];
  assignedActions: any = [];
  allControls : any = [];
  assignedControls : any = [];
  filteredControls : any = [];
  constructor(private httpservice: HttpService, private formBuilder: FormBuilder, private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.httpservice.get(ConstUrls.getAllActionRecords).subscribe((response: any) => {
      if (response) {
        this.allActions = response;
        this.filterActions();
        this.addnewissueTable();
      }
    });
    //fetching all controls
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        this.allControls = response;
        this.filterControls();
        this.addcontrolissueTable();

      }
    });
    this.initForm();
    this.dropDownMasters();
  }
  addActions() {
    let table = $('#addnewissueTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let aId = $(this).attr('value')
      const assignedActions = _this.allActions.find(
        (action) => action.actionId === aId
      );
      _this.assignedActions.push(assignedActions);
    });
    this.filterActions();
    this.addnewissueTable();
  }
  addControl() {
    let table = $('#issuecontrolCMTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let cId = $(this).attr('value');
      const assignedControl = _this.allControls.find(
        (control) => control.controlId === cId
      );
      _this.assignedControls.push(assignedControl);
    });
    this.filterControls();
    this.addcontrolissueTable();
  }
  filterActions() {
    if (this.allActions.length) {
      const selectedIds = [];
      this.assignedActions.forEach((element) => {
        selectedIds.push(element.actionId);
      });
      this.filteredActions = this.allActions.filter(
        (actions) => selectedIds.indexOf(actions.actionId) === -1
      );
    }
  }
  filterControls() {
    if (this.allControls.length) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (control) => selectedIds.indexOf(control.controlId) === -1
      );
    }
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control ?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.assignedControls = this.assignedControls.filter(
          (control) => control.controlId !== controlId
        );
        this.filterControls();
        this.addcontrolissueTable();
      }
    });
  }
  deleteAction(actionId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this Action?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.assignedActions = this.assignedActions.filter(
          (actions) => actions.actionId !== actionId
        );
        this.filterActions();
        this.addnewissueTable();
      }
    });
  }
  ngOnChanges() {
    // let payload = { issueId: this.issueData };
    // //console.log('hey',payload);
    // this.httpservice.securePost(ConstUrls.getOneIssues,payload).subscribe(async response => {

    //   this.issueActionMatrixData = await response;
    //  //console.log(this.issueActionMatrixData);
    //  this.initForm();
    //   this.IssueForm.patchValue(response);

    //   this.IssueForm.patchValue({
    //     dateIdentified: new Date(
    //       this.issueActionMatrixData.dateIdentified
    //     ),
    //     dateApproved: new Date(
    //       this.issueActionMatrixData.dateApproved
    //     )}
    //   )

    // this.httpservice.get(ConstUrls.issueStatusMaster).subscribe((response:any) => {

    //   this.issueStatusMaster = response;
    //   });

    // this.httpservice.get(ConstUrls.issueTypeMaster).subscribe((response:any) => {
    //   this.issueTypeMaster = response;
    //   });

    //   this.httpservice.get(ConstUrls.issuePriorityMaster).subscribe((response:any) => {
    //     this.issuePriorityMaster = response;
    //     });
    // })
  }

  dropDownMasters() {
    this.httpservice.get(ConstUrls.issueStatusMaster).subscribe((response: any) => {
      //console.log("jhjbjhb",response)
      this.issueStatusMaster = response;
    });

    this.httpservice.get(ConstUrls.issueTypeMaster).subscribe((response: any) => {
      this.issueTypeMaster = response;
    });

    this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
      this.area = response;
    });

    this.httpservice.get(ConstUrls.issuePriorityMaster).subscribe((response: any) => {
      this.issuePriorityMaster = response;
    });

  }


  closeModal() {
    $(".scroll-container").scrollTop(0);

    $('.pop-up-form').addClass('d-none');
    this.addNewIssue.tabs[0].active = true;
    this.position = this.position ? undefined : { x: 0, y: 0 }
  }

  initForm() {
    this.IssueForm = this.formBuilder.group({

      issueName: ['', []],
      action: ['', []],
      area: ['', []],
      department: ['', []],
      issueDescription: ['', []],
      issueId: ['', []],
      keyIsuue: ['false', []],
      managementResponse: ['', []],
      priority: ['', []],
      recommendation: ['', []],
      reviewComments: ['', []],
      approvedBy: ['', []],
      approverId: ['', []],
      dateApproved: ['', []],
      dateIdentified: ['', []],
      identifiedBy: ['', []],
      identifierId: ['', []],
      issueType: ['', []],
      issuePriority: ['', []],
      issueStatus: ['', []],
      controlId: [[]],
      actionId: [[],[]]
    });
  }
  onSave() {
    if(this.assignedActions.length) {
      let actionIds = [];
      this.assignedActions.forEach(action => {actionIds.push(action.actionId)})
      this.IssueForm.patchValue({actionId : actionIds});
    }
    if(this.assignedControls.length) {
      let controlIds = [];
      this.assignedControls.forEach(control => {controlIds.push(control.controlId)})
      this.IssueForm.patchValue({controlId : controlIds});
    }
    this.generatedIssueId = 'I-' + Math.floor(Math.random() * 100) + 1;
    this.identifiedDate = new Date(this.IssueForm.get('area').value).toLocaleDateString();
    if (this.identifiedDate === 'Invalid Date') { this.identifiedDate = new Date().toLocaleDateString() }

    this.IssueForm.patchValue({ 'issueId': this.generatedIssueId, 'controlAddedDate': this.identifiedDate });

    this.httpservice.securePost(ConstUrls.saveIssue, this.IssueForm.value).subscribe((response: any) => {
      if (response) {

        const initialState = {
          title: 'Success',
          content: 'Issue saved Successfully',
          link: 'Ok',
        };
        this.modalRef = this.modalService.show(DefaultModalComponent, { initialState, class: 'success-class', backdrop: 'static', keyboard: false });
        this.closedAddNewIssueForm.emit(true);
        this.closeModal();
      }
    })


  }


  addnewissueTable() {
    let table = $('#addnewissueTable').DataTable();
    table.destroy();
    setTimeout(() => {
      $('#addnewissueTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  };

  addcontrolissueTable() {
    let table = $('#issuecontrolCMTable').DataTable();
    table.destroy();
    setTimeout(() => {
      $('#issuecontrolCMTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  };

}
