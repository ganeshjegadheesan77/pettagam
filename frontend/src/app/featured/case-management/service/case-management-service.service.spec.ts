import { TestBed } from '@angular/core/testing';

import { CaseManagementServiceService } from './case-management-service.service';

describe('CaseManagementServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaseManagementServiceService = TestBed.get(CaseManagementServiceService);
    expect(service).toBeTruthy();
  });
});
