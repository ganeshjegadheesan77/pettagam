import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CaseManagementServiceService {


  private subject = new Subject<any>();
  private MessageFromIssueList = new Subject<any>();
  private MessageFromIAM = new Subject<any>();
  private MessageFromActionList = new Subject<any>();
  private MessageFromIAMToActionList = new Subject<any>();

  //-------------------------------------------------------

  fromIssueRefreshIAM(message: string) {  //send message from Issue List to IAM
      this.MessageFromIssueList.next({ text: message });
  }

  fromIssueRefreshedIAMTable(): Observable<any> {  //got Message from Issue List
    return this.MessageFromIssueList.asObservable();
  }
//-----------------------------------------------------------------

  
  
  //-------------------------------------------------------
  fromIssueActionMatrixRefreshIssue(message: string) {//send message from IAM to Issue List
    this.MessageFromIAM.next({ text: message });
  }

  fromIssueActionMatrixRefreshedIssueable(): Observable<any> { //got Message from IAM
    return this.MessageFromIAM.asObservable();
  }

//-----------------------------------------------------------------


//-----------------------------------------------------------------

fromActionRefreshIAM(message: string) { //send message from Action List to IAM
    this.MessageFromActionList.next({ text: message });
  }
fromActionRefreshedIAMTable(): Observable<any> { //got Message from Action List
  return this.MessageFromActionList.asObservable();
}
//-----------------------------------------------------------------


//-------------------------------------------------------
fromIssueActionMatrixRefreshAction(message: string) {//send message from IAM to Issue List
  this.MessageFromIAMToActionList.next({ text: message });
}

fromIssueActionMatrixRefreshedActionTable(): Observable<any> { //got Message from IAM
  return this.MessageFromIAMToActionList.asObservable();
}

//-----------------------------------------------------------------

  clearMessages() {
      this.subject.next();
  }

  

 
  
}
