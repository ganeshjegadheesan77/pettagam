import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from "ngx-bootstrap";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";

import { AutocompleteLibModule } from "angular-ng-autocomplete";

import { AdminRoutingModule } from "./admin-routing.module";
import { AdminComponent } from "./admin.component";
import { AdminDynamicViewComponent } from "./components/admin-dynamic-view/admin-dynamic-view.component";
import { AdminDashboardContainerComponent } from "./components/admin-dashboard-container/admin-dashboard-container.component";
import { ProcurementComponent } from "./components/procurement/procurement.component";
import { MineSafetyComponent } from "./components/mine-safety/mine-safety.component";
import { ItCsaComponent } from "./components/it-csa/it-csa.component";
import { FinanceComponent } from "./components/finance/finance.component";
import { BrainstormComponent } from "./components/brainstorm/brainstorm.component";
import { TravelComponent } from "./components/travel/travel.component";
import { MeetingReportComponent } from "./components/meeting-report/meeting-report.component";
import { AuditStructureCalendarComponent } from "./components/audit-structure-calendar/audit-structure-calendar.component";
import { NgxFileDropModule } from "ngx-file-drop";
import { NgxTimeSchedulerModule } from "ngx-time-scheduler";
import { NewIssueFormComponent } from "./components/new-issue-form/new-issue-form.component";
import { NewActionFormComponent } from "./components/new-action-form/new-action-form.component";
import { NewPbcFormComponent } from "./components/new-pbc-form/new-pbc-form.component";
import { NewFieldworkFormComponent } from "./components/new-fieldwork-form/new-fieldwork-form.component";
import { AngularDraggableModule } from "angular2-draggable";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AdminOpeningMeetingComponent } from "./components/admin-opening-meeting/admin-opening-meeting.component";
import { AdminDraftReportComponent } from "./components/admin-draft-report/admin-draft-report.component";
import { AdminClosingMeetingComponent } from "./components/admin-closing-meeting/admin-closing-meeting.component";
import { AdminAnnouncementComponent } from "./components/admin-announcement/admin-announcement.component";
import { AdminPlanningMemorandumComponent } from "./components/admin-planning-memorandum/admin-planning-memorandum.component";
import { EditPbcFormComponent } from "./components/edit-pbc-form/edit-pbc-form.component";
import { EditFieldworkFormComponent } from "./components/edit-fieldwork-form/edit-fieldwork-form.component";
import { EditIssueFormComponent } from "./components/edit-issue-form/edit-issue-form.component";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { AdminDashboardAraFormComponent } from './components/admin-dashboard-ara-form/admin-dashboard-ara-form.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NewWorkStepComponent } from './components/new-work-step/new-work-step.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule} from '@angular/common/http';
import { EditWorkStepComponent } from './components/edit-work-step/edit-work-step.component';
//import { IssueFormNewAglieAuditComponent } from './components/issue-form-new-aglie-audit/issue-form-new-aglie-audit.component';
import { AglieAuditPdfComponent } from './components/aglie-audit-pdf/aglie-audit-pdf.component';
import { EditedIssueFormAglieComponent } from './components/edited-issue-form-aglie/edited-issue-form-aglie.component';
import { PdfAnnotationComponent } from './components/pdf-annotation/pdf-annotation.component';


@NgModule({
  declarations: [
    AdminComponent,
    AdminDynamicViewComponent,
    AdminDashboardContainerComponent,
    ProcurementComponent,
    MineSafetyComponent,
    ItCsaComponent,
    FinanceComponent,
    BrainstormComponent,
    TravelComponent,
    MeetingReportComponent,
    AuditStructureCalendarComponent,
    NewIssueFormComponent,
    NewActionFormComponent,
    NewPbcFormComponent,
    NewFieldworkFormComponent,
    AdminOpeningMeetingComponent,
    AdminDraftReportComponent,
    AdminClosingMeetingComponent,
    AdminAnnouncementComponent,
    AdminPlanningMemorandumComponent,
    EditPbcFormComponent,
    EditFieldworkFormComponent,
    EditIssueFormComponent,
    AdminDashboardAraFormComponent,
    NewWorkStepComponent,
    EditWorkStepComponent,
    //IssueFormNewAglieAuditComponent,
    AglieAuditPdfComponent,
    EditedIssueFormAglieComponent,
    PdfAnnotationComponent,
  
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
    PdfViewerModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxTimeSchedulerModule,
    AngularDraggableModule,
    NgxFileDropModule,
    NgMultiSelectDropDownModule.forRoot(),
    HttpClientModule,
    AngularEditorModule
  ],
  exports: [CommonModule],
  providers: [],
})
export class AdminModule {}
