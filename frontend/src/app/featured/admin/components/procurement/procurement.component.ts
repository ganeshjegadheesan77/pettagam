import { Component, OnInit } from '@angular/core';
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";

@Component({
  selector: 'app-procurement',
  templateUrl: './procurement.component.html',
  styleUrls: ['./procurement.component.less']
})
export class ProcurementComponent implements OnInit {
  riskCount: Number;
  issueCount: Number;
  controlCount: Number;
  actionCount: Number;
  doughnutChart: object;
  pieChart: object;
  pieChartCanvas: any;
  pieChartCtx: any;
  canvas: any;
  ctx: any;
  data: any;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  constructor() { }

  ngOnInit() {
    this.getCharts();
  }
  getCharts() {

    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("pie");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "pie",
        data: {
          labels: ["High", "Low", "Medium"],
          datasets: [
            {},
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);
  }

}
