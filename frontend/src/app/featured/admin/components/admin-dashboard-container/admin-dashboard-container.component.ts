import { Component, OnInit, Input, Output, ViewChild } from "@angular/core";
import { Chart, ChartOptions } from "chart.js";
import { ConstUrls } from "src/app/config/const-urls";
import { HttpService } from "../../../../core/http-services/http.service";
declare var $;
// import { CalendarDataService } from "../../calendar-data.service";
import * as moment from "moment";
import { Subscription } from "rxjs";
import { AdminService } from "../../admin.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ChatroomService } from "../../../../layout/collaboration-elements/components/chat-system/services/chatroom.service";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ActivatedRoute, Router } from "@angular/router";
import { CollabChatServiceService } from "src/app/layout/collaboration-elements/collabServices/collab-chat-service.service";
import { LocalStorageService } from 'src/app/core/data-services/local-storage.service';
import { ConfigService } from 'src/config.service';
import { ConfirmPopupComponent } from 'src/app/layout/modal/confirm-popup/confirm-popup.component';

@Component({
  selector: "app-admin-dashboard-container",
  templateUrl: "./admin-dashboard-container.component.html",
  styleUrls: ["./admin-dashboard-container.component.less"],
})
export class AdminDashboardContainerComponent implements OnInit {
  @ViewChild('auto', { static: false }) auto;
  modalRef: BsModalRef;
  subscription: Subscription;

  doughnutChart: object;
  pieChart: object;
  pieChartCanvas: any;
  pieChartCtx: any;
  canvas: any;
  ctx: any;
  data: any;
  //Sagar change start
  assignedAuditProgram: any = [];
  assignedIssuess: any = [];

  workInProgressAuditProgram: any = [];
  workInProgressIssuess: any = [];

  completedAuditProgram: any = [];
  completedIssuess: any = [];

  //sagar change end
  auditableEntityName: any;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  auditData: any;
  assigned: any;
  wip: any = [];
  complete: any = [];
  lengthA: any = 0;
  lengthWIP: any = 0;
  lengthC: any = 0;
  overdueA: any = 0;
  overdueWIP: any = 0;
  overdueC: any = 0;
  assignedMeeting: any = [];
  assignedPBC: any = [];
  assignedFieldWork: any = [];
  assignedIssues: any = [];
  assignedIssuesData: any = [];
  allIssues: any = [];
  wipMeeting: any = [];
  wipPBC: any = [];
  wipFieldWork: any = [];
  wipIssues: any = [];
  completedMeeting: any = [];
  completedPBC: any = [];
  completedFieldWork: any = [];
  completedIssues: any = [];
  WfFormData: any;
  PBCFormData: any;
  iFormData: any;
  btnColor: string = "btn risk-btn link";
  araId = "";
  noAraIdFlag = "ARA---";
  araData: any;
  singleAuditData: any = [];
  closingMeeting: any;
  public addAdminNewIssue = false;
  public addAdminNewAction = false;
  public addAdminNewPbc = false;
  issueData: any;
  workstepData: any;
  auditId: any;
  tabItem: string = "";
  hideAraId: boolean = false;
  public editIssueForm = false;
  public editPbcForm = false;
  public templateList: any[];
  isuueFormAgile = false;
  public addNewWorkstepForm = false;
  title: string;
  content: string;
  pbcloadComponent: boolean;
  currentYear: number;
  activeAuditId = "BrainStorm";
  activeAudit;
  activeTemplate = {};
  urlAuditId;
  totalProgress: number;
  finalProgress: string;
  oldProgress: number;
  displayTabs = false;
  workstepForm = false;
  StatusPieData = {
    assigned: 0,
    wip: 0,
    completed: 0,
    overdue: 0,
  };
  AllTaskGraphData = {
    Assigned: { meetingReports: 0, pbc: 0, fieldWork: 0, issues: 0 },
    WIP: { meetingReports: 0, pbc: 0, fieldWork: 0, issues: 0 },
    Completed: { meetingReports: 0, pbc: 0, fieldWork: 0, issues: 0 },
  };
  public editWorkStep = false;
  pdfOpen = false;
  issueForm = false;
  ActiveissueId;
  ActiveWorkStepId;
  assignedWsCount = 0;
  wipWsCount = 0;
  wipIssueCount = 0;
  completedWsCount = 0;
  completedIssueCount = 0;

  highIssueCount = 0;
  mediumIssueCount = 0;
  lowIssueCount = 0;

  // issues: [],

  isAuditLead = false;
  activeBoard = { audit: null, ws: [], issues: [] };
  marked: boolean;
  teamView: boolean = false;
  tokenClaims;
  baseUrl;
  teamImage: any;
  displayTeamImages = false;
  constructor(
    private httpservice: HttpService,
    // private calendarDataService: CalendarDataService,
    private adminService: AdminService,
    public chatroomService: ChatroomService,
    private modalService: BsModalService,
    private activatedRoute: ActivatedRoute,
    private collabChatService: CollabChatServiceService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private configService: ConfigService
  ) {
    this.subscription = adminService.getCount().subscribe((message) => {
      if (message) {
        let payload = { user: localStorage.getItem("username") };
        // let payload = { user: "amelia"};
        //console.log("Payload:", payload, this.controlData);
        this.tabItem = localStorage.getItem("tabItem") || "Brainstorm";
        var tabName = JSON.parse(JSON.stringify(this.tabItem));
        this.tabItem = localStorage.getItem(this.tabItem);
        this.httpservice
          .securePost(ConstUrls.getMyAuditDetails, payload)
          .subscribe((response: any) => {
            if (response) {
              console.log("response", response);
              this.auditData = response;
              // this.getOneAuditDetail(this.auditData[0], {target:{innerText:tabName}});
              this.getOneAuditDetail(
                this.auditData.filter(
                  (a) =>
                    a.auditId ==
                    tabName.split("-")[0] + "-" + tabName.split("-")[1]
                )[0],
                { target: { innerText: tabName } }
              );
              this.generateChartData(true);
            }
          });
      }
    });
  }
  keyword = 'name';
  clearDropdownInput() {
    this.auto.clear();
  }
  getTemplateList() {
    this.httpservice.secureGet(ConstUrls.getTemplateList).subscribe(
      (res: any) => {
        this.templateList = res.data;
        console.log("##res--->", res);
      },
      (err: any) => {
        this.myCommonAlert('Something went wrong while fetching template...', 'Error');
      }
    )
  }

  ngOnInit() {
    this.baseUrl = this.configService.config.baseUrl;
    // this.ActiveissueId = "I-01"
    this.dropClick();
    this.getCharts();
    // this.getTeamImages('auditId');
    this.getAuditDetails();
    this.getTemplateList();
    // this.changeView({target:{checked:this.teamView}});
    this.totalProgress = 0;
    this.isAuditLead = false;
    // var username = localStorage.getItem("username");
    // if (username == "elmar") {
    //   this.isAuditLead = true;
    // }

    this.tokenClaims = this.localStorageService.parseToken();
    if (this.tokenClaims.role == 'auditLead') {
      this.isAuditLead = true;
    }
    //token parse = claims
    // claims.role == 'auditLead
    //   this.isAuditLead = true;

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params["auditId"]) {
        this.urlAuditId = params["auditId"];
      }
    });
  }

  //Sagar change start
  changeView(e) {
    this.marked = e.target.checked;
    this.teamView = e.target.checked;
    if (this.activeAuditId && this.activeAuditId != 'BrainStorm') {

      let payload = { 'auditId': this.activeAuditId, 'myView': !this.teamView };

      this.httpservice.securePost(ConstUrls.getViewData, payload).subscribe((res: any) => {
        if (res) {
          console.log('***res', res)
          if (res.data.length) {
            this.activeBoard.ws = res.data[0]['workSteps']; 
            this.activeBoard.issues = res.data[1]['issues'];
            this.assignedWsCount = this.activeBoard.ws.filter(
              (ws) => (!this.teamView && ((this.isAuditLead && ws.reviewer.status == 'assigned' || ws.reviewer.status == 'approval') || (!this.isAuditLead && ws.preparer.status == 'assigned'))) || (this.teamView && ((this.isAuditLead && ws.preparer.status == 'assigned')))
            ).length;
            this.wipWsCount = this.activeBoard.ws.filter(
              (ws) => (!this.teamView && ((this.isAuditLead && ws.reviewer.status == 'wip') || (!this.isAuditLead && ws.preparer.status == 'wip'))) || (this.teamView && this.isAuditLead && ws.preparer.status == 'wip')
            ).length;
            this.completedWsCount = this.activeBoard.ws.filter(
              (ws) => (!this.teamView && ((this.isAuditLead && ws.reviewer.status == 'completed') || (!this.isAuditLead && ws.preparer.status == 'completed'))) || (this.teamView && this.isAuditLead && ws.preparer.status == 'completed')
            ).length;
            // this.assignedWsCount = 0;
          }
        }
      }, (error) => {
        this.activeBoard.ws = [];
        this.assignedWsCount = 0;
        this.wipWsCount = 0;
        this.completedWsCount = 0;
        // this.myCommonAlert(error.error.message, 'Error');
      })

    } else if (this.activeAuditId == 'BrainStorm') {
      this.displayTeamImages = false;
      this.activeAuditId = 'BrainStorm';
      this.onClickOnBrainStrom();
    }

  }
  //Sagar change end


  getTeamImages(auditId) {
    this.httpservice.securePost(ConstUrls.getTeamImages, { 'auditId': auditId })
      .subscribe((response: any) => {
        if (response) {
          console.log("img", response)
          this.teamImage = response.data
          this.displayTeamImages = true;
        }
      },
      error=>{
        console.log('img api error',error);
        this.displayTeamImages = false;
      })
  }

  calculateProgress(auditid) {
    if (auditid) {
      let total =
        parseInt(this.lengthA) +
        parseInt(this.lengthWIP) +
        parseInt(this.lengthC);
      let compeletePercentage = Math.floor((this.lengthC / total) * 100);
      let wipPercentage = Math.floor(compeletePercentage / 2);
      this.totalProgress = compeletePercentage + wipPercentage;

      let payload = { auditId: auditid, progress: this.totalProgress };
      this.httpservice
        .securePost(ConstUrls.updateOverallProgress, payload)
        .subscribe((res: any) => {
          if (res) {
            console.log("Progress Updated");
          }
        });
    }
  }

  getAuditDetails() {
    let payload = { user: localStorage.getItem("username") };
    // let payload = { user: "amelia"};
    //console.log("Payload:", payload, this.controlData);
    this.httpservice.secureGet(ConstUrls.getAuditsDetails, payload).subscribe(
      (response: any) => {
        if (response) {
          this.auditData = response.data;
          this.displayTabs = true;
          // this.getOneAuditDetail(this.auditData[0],
          //   { target: { innerText: this.auditData[0].auditId + "-" + this.auditData[0].auditableEntity } }
          //   );
          this.displayTeamImages = false;
          this.activeAuditId = "BrainStorm";
          this.onClickOnBrainStrom();
          setTimeout(() => {
            this.tabScrolling();
            var thisref = this;
            $("#myTabs a").on("click", function (e) {
              e.preventDefault();
              if (e.target.innerText != "OP-000-IT") {
                thisref.getOneAuditDetail(
                  thisref.auditData.filter((a) => a.auditId == e.target.id)[0],
                  e
                );
              } else {
                thisref.displayTeamImages = false;
                thisref.activeAuditId = "BrainStorm";
                thisref.onClickOnBrainStrom();
              }
              $(this).tab("show");
            });
            if (this.urlAuditId) {
              this.redirectToAudit(this.urlAuditId);
            }
          }, 500);

          // this.generateChartData(true);


        } else {
          this.displayTabs = true;
          // this.generateChartData(false);
          setTimeout(() => {
            this.tabScrolling();
          }, 0);
        }
      },
      (error) => {
        localStorage.removeItem('auditorData');
        localStorage.removeItem('auditLeadData');
        this.displayTabs = true;
        this.generateChartData(false);
        setTimeout(() => {
          this.tabScrolling();
        }, 0);
      }
    );
  }



  // openWorkStep() {
  //   this.editWorkStep = true;
  //   this.pdfOpen = false;
  //   this.issueForm = false;
  // }
  pdfView() {
    this.editWorkStep = false;
    this.pdfOpen = true;
    this.issueForm = false;
  }
  issueEditOpen() {
    this.editWorkStep = false;
    this.pdfOpen = false;
    this.issueForm = true;
  }

  onClickOnBrainStrom() {
    this.adminService.setDates({
      closingMeetingDate: "",
      openingMeetingDate: "",
      updateMeetingDate: "",
      finalReport: "",
    });
    this.totalProgress = 0;
    this.hideAraId = false;
    // this.araId = "test"
  }

  async getOneAuditDetail(data, $event) {
    this.activeAuditId = data.auditId;
    console.log("activeAuditId", this.activeAuditId);
    this.activeAudit = data;
    this.activeBoard.audit = this.activeAuditId;
    this.refreshBoard();
    this.httpservice
      .securePost(ConstUrls.getRoomByAudit, { audit_id: data.auditId })
      .subscribe((r) => {
        if (r) {
          var roomData: any = r;
          this.chatroomService.setValue(roomData["_id"]);
        }
      });
    this.collabChatService.sendAuditId(data.auditId);
    this.hideAraId = true;
    this.activeAuditId = data.auditId;
    console.log("activeAuditId", this.activeAuditId);
    this.activeAudit = data;

    localStorage.setItem("tabItem", $event.target.innerText);
    localStorage.setItem($event.target.innerText, data.auditableEntity);
    localStorage.setItem("ID", data.auditId);
    let reportData = {
      closingMeetingDate: data.closingMeetingDate,
      // "openingMeetingDate": data.openingMeetingDate ,
      updateMeetingDate: data.updateMeetingDate,
      // "finalReport": data.performancePeriod && data.performancePeriod.length === 2 ? data.performancePeriod[1] : undefined
    };
    this.adminService.setDates(reportData);

    // this.calendarDataService.updatedDataSelection(data.calendarDetails.auditId);

    //my view, team view api
    // activeBoard.ws = api[ws];
    // activeBoard = { ws:[], issues : []}



    // this.araId = "ARA-" + data.auditId.split("-")[1];
    // this.noAraIdFlag = "ARA---";
    // this.issueData = data;
    // this.workstepData = data;
    this.auditableEntityName = data.auditableEntity;
    this.auditId = data.auditId;

    this.singleAuditData = {
      auditId: data.auditId,
      auditableEntity: data.auditableEntity,
    };
  }

  deleteIssue(isssue) {
    let issueId = isssue.issueId
    let workstepIds = isssue.workstepIds
    const initialState = {
      title: "Confirmation",
      content: workstepIds.length ? `Issue ${issueId} is attached to following workstep : ${workstepIds.toString()} .\n Do you want to Delete it ?` : `Do you want to Delete issue ${issueId}`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    this.modalRef.content.onClose.subscribe(r => {
      if (r) {
        this.httpservice.secureDelete(ConstUrls.deleteIssue + "/" + issueId).subscribe(
          (res : any) => {
            let attachment = res.data
            for (let i = 0; i < attachment.length; i++) {
              let payload = {
                dmsSessionId: localStorage.getItem("dmsToken"),
                documentNo: attachment[i].documentNo,
                fileName: attachment[i].documentName,
                deletedBy: localStorage.getItem("username"),
                comments: `file deleted by ${localStorage.getItem("username")}`,
              };
              this.httpservice.post(ConstUrls.deleteDocument, payload).subscribe((res: any) => {})
            }
            this.myCommonAlert("Issue deleted from system!!", 'Success', "success");
            this.refreshBoard();
          },
          err => {
            this.myCommonAlert(err.error.message, 'Error');
          }
        );
      }
    })
  }
  // editIssue(issue) {
  //   this.ActiveissueId = issue.issueId;
  // this.newIssuePopup();
  // }

  editWS(workstepData) {
    this.ActiveWorkStepId = workstepData;

    console.log("rto", workstepData);
    this.workstepData = workstepData;
    $(".back-drop-container").addClass("show-drop");
    this.newWorkStepPopup();
  }

  deleteWorkStep(workStepId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete this workstep: ${workStepId}?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    this.modalRef.content.onClose.subscribe(r => {
      if (r) {
        this.httpservice.secureDelete(ConstUrls.deleteWorkStep + "/" + workStepId).subscribe(
          res => {
            this.refreshBoard();
          },
          err => {
            this.myCommonAlert('Error', err.error.message);
          }
        )
      }
    })
  }

  openWorkstep() {
    this.ActiveWorkStepId = undefined;
    this.newWorkStepPopup();
    $(".back-drop-container").addClass("show-drop");
  }
  refreshBoard() {
    this.changeView({ target: { checked: this.teamView } });
    this.getTeamImages(this.activeAuditId);
    // this.activeBoard.issues = [
    //   {issueId : "I-01",issueName : "ashraf", cardStatus : "wip", workstepIds : ["WS-020","WS-021","WS-021"] },
    //   {issueName : "sahil", cardStatus : "wip", workstepIds : ["WS-010","WS-011","WS-021"] },
    //   {issueName : "kaife", cardStatus : "wip", workstepIds : ["WS-030","WS-011","WS-021"] }
    // ]
  }

  getCharts() {
    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("pie");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "pie",
        data: {
          labels: ["High", "Low", "Medium"],
          datasets: [{}],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);
  }

  tabScrolling() {
    $(".nav-tab-scroll").scrollingTabs({
      bootstrapVersion: 4,
      scrollToTabEdge: true,
      cssClassLeftArrow: "tab-arrow tab-arrow-left",
      cssClassRightArrow: "tab-arrow tab-arrow-right",
    });
  }

  getPBCFormStatus(event) {
    if (event === true) {
      this.editPbcForm = false;
    }
  }
  recalculateProgress() {
    this.calculateProgress(this.activeAuditId);
  }
  getISSFormStatus(event) {
    if (event === true) {
      this.editIssueForm = false;
      this.recalculateProgress();
    }
  }
  getWSFormStatus(event) {
    if (event === true) {
      this.addNewWorkstepForm = false;
      // this.recalculateProgress();
    }
  }

  getPbcNewFormStatus(event) {
    if (event === true) {
      this.addAdminNewPbc = false;
      // this.getAuditDetails(this.selectedAuditId);
    }
  }

  getISNFormStatus(event) {
    if (event === true) {
      this.addAdminNewIssue = false;
      this.isuueFormAgile = false;
      this.recalculateProgress();

      // this.getAuditDetails(this.selectedAuditId);
    }
  }

  newIssuePopup() {
    this.isuueFormAgile = true;
    this.addAdminNewIssue = false;
    this.addAdminNewAction = false;
    this.addAdminNewPbc = false;
    this.editIssueForm = false;
    this.editPbcForm = false;
    this.addNewWorkstepForm = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }
  newActionPopup() {
    this.addAdminNewIssue = false;
    this.isuueFormAgile = false;
    this.addAdminNewAction = true;
    this.addAdminNewPbc = false;
    this.editIssueForm = false;
    this.editPbcForm = false;
    this.addNewWorkstepForm = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }
  newPbcPopup() {
    this.addAdminNewIssue = false;
    this.isuueFormAgile = false;
    this.addAdminNewAction = false;
    this.addAdminNewPbc = true;
    this.editIssueForm = false;
    this.editPbcForm = false;
    this.addNewWorkstepForm = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  newWorkStepPopup() {
    // this.ActiveWorkStepId = undefined;
    this.addNewWorkstepForm = true;
    this.addAdminNewIssue = false;
    this.isuueFormAgile = false;
    this.addAdminNewAction = false;
    this.addAdminNewPbc = false;
    this.editIssueForm = false;
    this.editPbcForm = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $("#newEditStepForm").addClass("d-none");
    $("#newWorkStepForm").removeClass("d-none");
  }
  editAdminIssue(item) {
    this.iFormData = { issueId: item };
    this.editIssueForm = true;
    this.editPbcForm = false;
    this.addAdminNewIssue = false;
    this.isuueFormAgile = false;
    this.addAdminNewAction = false;
    this.addAdminNewPbc = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  editAdminPBC(pbc) {
    console.log("pbc:", pbc);
    this.PBCFormData = pbc;
    this.editPbcForm = true;
    this.editIssueForm = false;
    this.addAdminNewIssue = false;
    this.isuueFormAgile = false;
    this.addAdminNewAction = false;
    this.addAdminNewPbc = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  myCommonAlert(content, title, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }

  redirectToAudit(auditId) {
    //  auditId= "OP-1";
    $('#myTabs a[href="#' + auditId + '"]').tab("show");
    console.log("show event called");
    var event = { target: { innertext: $('a[id="' + auditId + '"]').text() } };
    this.getOneAuditDetail(
      this.auditData.filter((a) => a.auditId == auditId)[0],
      event
    );
  }

  generateChartData(hasAudits) {
    var assigned = 0;
    var wip = 0;
    var completed = 0;
    var overdue = 0;
    var assignedIssues = [];
    var completedIssues = [];
    var wipIssues = [];
    var assignedMeeting = [];
    var assignedPBC = [];
    var assignedFieldWork = [];
    var wipMeeting = [];
    var wipPBC = [];
    var wipFieldWork = [];
    var completedMeeting = [];
    var completedPBC = [];
    var completedFieldWork = [];
    var lengthA;
    var lengthWIP;
    var lengthC;
    let currentUser = localStorage.getItem("username");
    if (hasAudits) {
      var issueIds = [].concat.apply(
        [],
        this.auditData
          .filter((a) => a.issueIds.length > 0)
          .map((s) => s.issueIds)
      );
      let payload = { issueId: issueIds };
      this.httpservice
        .securePost(ConstUrls.getMultipleIssues, payload)
        .subscribe((response: any) => {
          if (response) {
            response.forEach((ai) => {
              if (ai.status == "assigned") {
                assignedIssues.push(ai);
              }
              if (ai.status == "completed") {
                completedIssues.push(ai);
              }
              if (ai.status == "wip") {
                wipIssues.push(ai);
              }
            });
          }
          this.auditData.forEach((data) => {
            if (data.openingMeeting) {
              if (
                (data.openingMeeting.status == "assigned" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.openingMeeting.approverStatus == "assigned" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                assignedMeeting.push({
                  id: data.openingMeeting.aom_id,
                  name: "OpeningMeeting",
                  auditId: data.auditId,
                });
                // this.assignedMeeting.push("OpeningMeeting");
              }
              if (
                (data.openingMeeting.assigneeStatus == "completed" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.openingMeeting.approverStatus == "completed" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                completedMeeting.push({
                  id: data.openingMeeting.aom_id,
                  name: "OpeningMeeting",
                  auditId: data.auditId,
                });
              }
              if (
                (data.openingMeeting.assigneeStatus == "wip" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.openingMeeting.approverStatus == "wip" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                wipMeeting.push({
                  id: data.openingMeeting.aom_id,
                  name: "OpeningMeeting",
                  auditId: data.auditId,
                });
                // this.wipMeeting.push("OpeningMeeting");
              }
            }
            if (data.draftReport) {
              if (
                (data.draftReport.assigneeStatus == "assigned" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.draftReport.approverStatus == "assigned" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                assignedMeeting.push({
                  id: data.draftReport.adr_id,
                  name: "draftReport",
                  auditId: data.auditId,
                });
                // this.assignedMeeting.push("draftReport");
              }
              if (
                (data.draftReport.assigneeStatus == "completed" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.draftReport.approverStatus == "completed" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                completedMeeting.push({
                  id: data.draftReport.adr_id,
                  name: "draftReport",
                  auditId: data.auditId,
                });
                // this.completedMeeting.push("draftReport");
              }
              if (
                (data.draftReport.assigneeStatus == "wip" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.draftReport.approverStatus == "wip" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                wipMeeting.push({
                  id: data.draftReport.adr_id,
                  name: "draftReport",
                  auditId: data.auditId,
                });
                // this.wipMeeting.push("draftReport");
              }
            }
            if (data.closingMeeting) {
              if (
                (data.closingMeeting.assigneeStatus == "assigned" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.closingMeeting.approverStatus == "assigned" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                assignedMeeting.push({
                  id: data.closingMeeting.acm_id,
                  name: "closingMeeting",
                  auditId: data.auditId,
                });
                // this.assignedMeeting.push("closingMeeting");
              }
              if (
                (data.closingMeeting.assigneeStatus == "completed" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.closingMeeting.approverStatus == "completed" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                completedMeeting.push({
                  id: data.closingMeeting.acm_id,
                  name: "closingMeeting",
                  auditId: data.auditId,
                });
                // this.completedMeeting.push("closingMeeting");
              }
              if (
                (data.closingMeeting.assigneeStatus == "wip" &&
                  data.calendarDetails.rAssignee.indexOf(currentUser) > -1) ||
                (data.closingMeeting.approverStatus == "wip" &&
                  data.calendarDetails.rapAssignee.indexOf(currentUser) > -1)
              ) {
                wipMeeting.push({
                  id: data.closingMeeting.acm_id,
                  name: "closingMeeting",
                  auditId: data.auditId,
                });
                // this.wipMeeting.push("closingMeeting");
              }
            }
            if (data.announcement) {
              if (
                (data.announcement.assigneeStatus == "assigned" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.announcement.approverStatus == "assigned" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                assignedMeeting.push({
                  id: data.announcement.aa_id,
                  name: "announcement",
                  auditId: data.auditId,
                });
                // this.assignedMeeting.push("announcement");
              }
              if (
                (data.announcement.assigneeStatus == "completed" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.announcement.approverStatus == "completed" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                completedMeeting.push({
                  id: data.announcement.aa_id,
                  name: "announcement",
                  auditId: data.auditId,
                });
                // this.completedMeeting.push("announcement");
              }
              if (
                (data.announcement.assigneeStatus == "wip" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.announcement.approverStatus == "wip" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                wipMeeting.push({
                  id: data.announcement.aa_id,
                  name: "announcement",
                  auditId: data.auditId,
                });
                // this.wipMeeting.push("announcement");
              }
            }
            if (data.planningMemorandum) {
              if (
                (data.planningMemorandum.assigneeStatus == "assigned" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.planningMemorandum.approverStatus == "assigned" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                assignedMeeting.push({
                  id: data.planningMemorandum.apm_id,
                  name: "planningMemorandum",
                  auditId: data.auditId,
                });
                // this.assignedMeeting.push("planningMemorandum");
              }
              if (
                (data.planningMemorandum.assigneeStatus == "completed" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.planningMemorandum.approverStatus == "completed" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                completedMeeting.push({
                  id: data.planningMemorandum.apm_id,
                  name: "planningMemorandum",
                  auditId: data.auditId,
                });
                // this.completedMeeting.push("planningMemorandum");
              }
              if (
                (data.planningMemorandum.assigneeStatus == "wip" &&
                  data.calendarDetails.pAssignee.indexOf(currentUser) > -1) ||
                (data.planningMemorandum.approverStatus == "wip" &&
                  data.calendarDetails.papAssignee.indexOf(currentUser) > -1)
              ) {
                wipMeeting.push({
                  id: data.planningMemorandum.apm_id,
                  name: "planningMemorandum",
                  auditId: data.auditId,
                });
                // this.wipMeeting.push("planningMemorandum");
              }
            }
            if (data.auditPbc) {
              if (data.auditPbc instanceof Array && data.auditPbc.length) {
                data.auditPbc.forEach((pbc) => {
                  if (
                    (pbc.assigneeStatus == "assigned" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (pbc.approverStatus == "assigned" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    assignedPBC.push({
                      id: pbc.auditPBC_id,
                      name: pbc.pbc_name,
                    });
                  }
                  if (
                    (pbc.assigneeStatus == "completed" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (pbc.approverStatus == "completed" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    completedPBC.push({
                      id: pbc.auditPBC_id,
                      name: pbc.pbc_name,
                    });
                  }
                  if (
                    (pbc.assigneeStatus == "wip" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (pbc.approverStatus == "wip" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    wipPBC.push({ id: pbc.auditPBC_id, name: pbc.pbc_name });
                  }
                });
              }
            }
            if (data.auditFieldWork) {
              if (
                data.auditFieldWork instanceof Array &&
                data.auditFieldWork.length
              ) {
                data.auditFieldWork.forEach((fw) => {
                  if (
                    (fw.assigneeStatus == "assigned" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (fw.approverStatus == "assigned" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    assignedFieldWork.push({
                      id: fw.auditFw_id,
                      name: fw.afw_name,
                    });
                  }
                  if (
                    (fw.assigneeStatus == "completed" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (fw.approverStatus == "completed" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    completedFieldWork.push({
                      id: fw.auditFw_id,
                      name: fw.afw_name,
                    });
                  }
                  if (
                    (fw.assigneeStatus == "wip" &&
                      data.calendarDetails.fAssignee.indexOf(currentUser) >
                      -1) ||
                    (fw.approverStatus == "wip" &&
                      data.calendarDetails.fapAssignee.indexOf(currentUser) >
                      -1)
                  ) {
                    wipFieldWork.push({
                      id: fw.auditFw_id,
                      name: fw.afw_name,
                    });
                  }
                });
              }
            }
          });
          lengthA =
            assignedMeeting.length +
            assignedPBC.length +
            assignedFieldWork.length;
          lengthWIP = wipMeeting.length + wipFieldWork.length + wipPBC.length;
          lengthC =
            completedMeeting.length +
            completedFieldWork.length +
            completedPBC.length;
          assigned += lengthA;
          wip += lengthWIP;
          completed += lengthC;
          wip += wipIssues.length;
          completed += completedIssues.length;
          this.StatusPieData = {
            assigned,
            wip,
            completed,
            overdue,
          };

          this.AllTaskGraphData = {
            Assigned: {
              meetingReports: assignedMeeting.length,
              pbc: assignedPBC.length,
              fieldWork: assignedFieldWork.length,
              issues: assignedIssues.length,
            },
            WIP: {
              meetingReports: wipMeeting.length,
              pbc: wipPBC.length,
              fieldWork: wipFieldWork.length,
              issues: wipIssues.length,
            },
            Completed: {
              meetingReports: completedMeeting.length,
              pbc: completedPBC.length,
              fieldWork: completedFieldWork.length,
              issues: completedIssues.length,
            },
          };
          console.log("graph data", this.AllTaskGraphData);
          console.log("pie data", this.StatusPieData);
          console.log(
            "assigned wip completed overdue",
            assigned + " " + wip + " " + completed + " " + overdue
          );
        });
    }
  }
  dropClick() {
    $(".dropdownSearch").on("click", function (event) {
      event.stopPropagation();
    });
  }

  editIssue(issue) {
    this.ActiveissueId = issue.issueId;
    this.newIssuePopup();
  }

  openIssue() {
    this.ActiveissueId = undefined;
    this.newIssuePopup();
  }

  generateReport() {
    var canGenerate = true;
    // var auditorData = JSON.parse(localStorage.getItem("auditorData")).find(
    //   (a) => a.audit == this.activeAuditId
    // );
    // var auditLeadData = JSON.parse(localStorage.getItem("auditLeadData")).find(
    //   (a) => a.audit == this.activeAuditId
    // );
    // if (auditorData && auditorData.ws && auditorData.ws.length > 0) {
    //   auditorData.ws.forEach((w) => {
    //     if (w.cardStatus == "wip" || w.cardStatus == 'assigned') {
    //       canGenerate = false;
    //     }
    //   });
    // }
    // if (auditorData && auditorData.issues && auditorData.issues.length > 0) {
    //   auditorData.issues.forEach((w) => {
    //     if (w.cardStatus == "wip") {
    //       canGenerate = false;
    //     }
    //   });
    // }
    // if (auditLeadData && auditLeadData.ws && auditLeadData.ws.length > 0) {
    //   auditLeadData.ws.forEach((w) => {
    //     if (w.status == "wip" || w.cardStatus == 'assigned') {
    //       canGenerate = false;
    //     }
    //   });
    // }

    // if (canGenerate === true) {
    //   // alert("can");
    //   this.router.navigate(["/rcm/agile-audit-report"], {
    //     queryParams: { auditId: this.auditId },
    //   });
    // } else {
    //   this.myCommonAlert("All Steps must be Completed", "Alert");
    //   // alert('cannot');
    // }
  }

  closeIssueForm() {
    this.isuueFormAgile = false;
    this.refreshBoard();
  }
  closeWSForm(event: boolean) {
    this.addNewWorkstepForm = false;
    if (event) {
      this.refreshBoard();
    }
  }


  selectEvent(item) {
    console.log("@@@ item --->", item);
    let payload = { 'auditId': this.activeAuditId, templateId: item.templateId };
    this.httpservice.securePost(ConstUrls.applyAndOverwriteTemplates, payload).subscribe((res: any) => {
      if (res) {
        this.refreshBoard();
        this.myCommonAlert(res.message, 'Message');
        this.clearDropdownInput();
      }
    }, (error) => {
      this.myCommonAlert(error.error.message,'Error');
    })

    // do something with selected item
  }

  onChangeSearch(search: string) {
  // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    // do something
  }
}
