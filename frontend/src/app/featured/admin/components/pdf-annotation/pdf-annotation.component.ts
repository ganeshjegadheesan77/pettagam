import { Component, ViewChild, OnInit, ElementRef, SimpleChanges, AfterViewInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
declare const WebViewer: any;
import { HttpService } from 'src/app/core/http-services/http.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { SaveDocumentPopupComponent } from "src/app/layout/modal/save-document-popup/save-document-popup.component";
import { ConstUrls } from "src/app/config/const-urls";
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';
import { string } from '@amcharts/amcharts4/core';

@Component({
  selector: 'app-pdf-annotation',
  templateUrl: './pdf-annotation.component.html',
  styleUrls: ['./pdf-annotation.component.less']
})
export class PdfAnnotationComponent implements OnInit, AfterViewInit, OnChanges {
  uri;

  constructor(public httpService: HttpService, private modalService: BsModalService,
    private configService:ConfigService
    
    ) { 
    this.uri = configService.config.baseUrl;

    }
  @Input('path') path;
  @Input('filename') filename;  
  @Input('closeForm') closeForm;
  @Input('documentNo') documentNo;
  @Input('wpNumber') wpNumber;
  @Input('auditId') auditId;
  @Input('docType') docType;
  // @Input('closeOpenFIle') closeOpenFIle : boolean;
  modalRef: any;
  @Output() close = new EventEmitter<closeObj>();
  @Output() closeOpenFileFlag = new EventEmitter<boolean>();
  @Output() closefile = new EventEmitter();
  @ViewChild('viewer', { static: false }) viewer: ElementRef;
  workPaperNumber: string;
  currentUser: string;
  blobObj: any;
  show = true;
  test = '';
  extension = '';
  ngOnInit() {
    // this.path = "http://115.112.185.58:3201/DMSIASERVICE/docdownload.do?dmsSession=d484cad1d50ac5f998c5386bfa2c1f01ca5798e5f505b21e51371ce17fa7fe1a07b1e3d765e3125b&urlToken=ed41b6a2d92b0d16d3253f9f54cc64243fa61e4530fc106d20048175f6e30961a64c7540906f0cbe";
    // this.filename = 'WP-1_OP-018_ckeditor4-export-pdf.pdf';
    // this.documentNo = "920";
    // this.wpNumber = ''//"WP-1";
    // this.auditId = "OP-018";
    // let fileArr = this.filename.split('.');
    // this.extension = fileArr[fileArr.length - 1];
  }
  closeDialog() {
    this.close.emit({close : true});
    this.closefile.emit(this.filename);
    this.closeOpenFileFlag.emit(false);
  }
  ngOnChanges(changes: SimpleChanges) {
    let fileArr = this.filename.split('.');
    this.extension = fileArr[fileArr.length - 1];
    if (!this.wpNumber) {
      this.httpService.secureGet(ConstUrls.getWorkpaperNumber, { prefix: 'WP' }).subscribe(obj => {
        let incrementedNumber = String(Number(obj[0]["lastId"]) + 1)
        this.workPaperNumber = "WP-" + incrementedNumber;
        this.httpService.securePost(ConstUrls.setWorkPaperNumber, { prefix: 'WP', lastId: incrementedNumber }).subscribe(o => { });
      });
    }
  }
  
  commonAlert(title, content, popupType?) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: `error-popup-design nobtnIn ${popupType == 'success' ? 'confirm-popup' : ''}`,
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }
  addWpNumberOnPdf(Annotations, annotManager, docViewer, instance) {
    docViewer.one('documentLoaded', async () => {
      try {
        if (this.workPaperNumber) {
          this.show = true;
          const freeText = new Annotations.FreeTextAnnotation();
          freeText.PageNumber = 1;
          freeText.X = 437;
          freeText.Y = 15;
          freeText.Width = 125;
          freeText.Height = 44;
          freeText.setPadding(new Annotations.Rect(0, 0, 0, 0));
          freeText.setContents(`${this.workPaperNumber} \nCreated By: ${this.currentUser}\nApproved By:      `);
          freeText.FillColor = new Annotations.Color(255, 255, 255);
          freeText.FontSize = '10pt';

          annotManager.addAnnotation(freeText);
          annotManager.redrawAnnotation(freeText);
          const doc = docViewer.getDocument();
          const xfdfString = await annotManager.exportAnnotations();
          const options = { xfdfString, flatten: true };
          this.show = false;
          const data = await doc.getFileData(options);
          const arr = new Uint8Array(data);
          const blob = new Blob([arr], { type: 'application/pdf' });
          instance.loadDocument(blob, {
            extension: 'pdf',
          });
        }
      } catch (e) {
        console.log(e);
      }
    })

  }
  ngAfterViewInit(): void {
    this.currentUser = localStorage.getItem("username");
    WebViewer({
      path: '../../../../../../lib',
      backendType: 'ems',
      fullAPI: true,
      ui: 'legacy'
      // licenseKey:'true'
    }, document.getElementById('viewer')).then(instance => {
      try {
        instance.setFitMode(instance.FitMode.FitWidth);
        const { CoreControls, docViewer, Annotations, annotManager } = instance;
        this.createCustomRuberStamp(docViewer);
        annotManager.setCurrentUser(this.currentUser);
        if (this.wpNumber) {
          this.show = false;
          instance.loadDocument(this.path, { extension: this.extension });
        } else {
          if (this.extension == "pdf") {
            this.show = false;
            instance.loadDocument(this.path, { extension: this.extension });
            this.addWpNumberOnPdf(Annotations, annotManager, docViewer, instance);
          } else {
            //l is undefined for now(licence)
            CoreControls.office2PDFBuffer(this.path, { l: undefined, extension: this.extension }).then(buffer => {
              const arr = new Uint8Array(buffer);
              const blob = new Blob([arr], { type: 'application/pdf' });
              this.show = false;
              instance.loadDocument(blob, { filename: 'myfile.pdf' });
              this.addWpNumberOnPdf(Annotations, annotManager, docViewer, instance);
            })
          }

        }
        // Add header button that will get file data on click
        instance.setHeaderItems(header => {
          header.push({
            type: 'actionButton',
            title: 'Upload Changes to PDF',
            img: '../../../../../assets/img/pdf-ico.svg',
            onClick: async () => {
              const doc = docViewer.getDocument();
              const xfdfString = await annotManager.exportAnnotations();
              const data = await doc.getFileData({
                // saves the document with annotations in it
                xfdfString
              });
              const arr = new Uint8Array(data);
              const blob = new Blob([arr], { type: 'application/pdf' });
              let name;
              if (this.wpNumber) {
                name = this.filename;
                this.uploadChangesToPdf(blob, name);
              } else {
                const initialState = {
                  title: "Save As Workpaper",
                  content: `Save as`,
                  link: "Save",
                  action: "Cancel",
                  confirmFlag: false,
                  workPaperNumber: this.workPaperNumber,
                  auditId: this.auditId,
                  fileName: this.filename.split('.')[0]
                };
                this.modalRef = this.modalService.show(SaveDocumentPopupComponent, {
                  initialState,
                  class: "success-class",
                  backdrop: "static",
                  keyboard: false,
                });
                this.modalRef.content.onClose.subscribe((newName) => {
                  if (newName) {
                    name = `${this.workPaperNumber}_${this.auditId}_${newName}.pdf`
                    this.uploadChangesToPdf(blob, name);
                  }
                });
              }
            }
          });
        });
      } catch (error) {
        console.log("error happens----->", error);
        this.show = false;
      }
    })
  }
uploadChangesToPdf(blob, name) {
    const formData: any = new FormData();
    formData.append("file", blob, name);
    formData.append("dmsSessionId", localStorage.getItem('dmsToken'));
    formData.append("docId", this.documentNo);
    formData.append("doctype", "workPaper");
    formData.append("filename", name);
    formData.append("workPaperNumber", this.wpNumber ? this.wpNumber : this.workPaperNumber);
    formData.append("comments", `${this.currentUser} changes file from ${this.filename} to ${name}`);
    this.httpService.securePost(ConstUrls.replaceDocuments, formData).subscribe((x) => {
        this.close.emit({
          "close" : true, 
          "documentNo" : this.documentNo,
          "docType": this.docType,
          "documentName" : name,
          "workPaperNumber" : this.wpNumber ? this.wpNumber : this.workPaperNumber
        });
        setTimeout(() => {
          let message = this.wpNumber ? 'Document updated Successfully.' : 'Work paper created successfully.'
          this.commonAlert("Success", message, "success");
        }, 1000);

    })
  }
  createCustomRuberStamp(docViewer) {
    const tool = docViewer.getTool('AnnotationCreateRubberStamp');
    tool.setStandardStamps([
      `${this.configService.config.baseUrl}uploads/Minor_Execptions_Noted.png`,
      `${this.configService.config.baseUrl}uploads/No_Exeptions_Noticed.png`,
      `${this.configService.config.baseUrl}uploads/Observed.png`,
      `${this.configService.config.baseUrl}uploads/Queries_Outstanding.png`,
      `${this.configService.config.baseUrl}uploads/Reperformance_Done.png`,
      `${this.configService.config.baseUrl}uploads/Walkthourgh_Done.png`,
      `${this.configService.config.baseUrl}uploads/Walkthourgh_Not_Done.png`,
      `${this.configService.config.baseUrl}uploads/legendChart.png`
    ]);
  }

}

interface closeObj {
  close : boolean;
  documentNo? : string;
  docType? : string;
  documentName? : string;
  workPaperNumber? : string;
}