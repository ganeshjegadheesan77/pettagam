import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineSafetyComponent } from './mine-safety.component';

describe('MineSafetyComponent', () => {
  let component: MineSafetyComponent;
  let fixture: ComponentFixture<MineSafetyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MineSafetyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineSafetyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
