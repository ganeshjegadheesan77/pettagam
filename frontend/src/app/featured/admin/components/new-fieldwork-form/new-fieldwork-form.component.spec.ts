import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFieldworkFormComponent } from './new-fieldwork-form.component';

describe('NewFieldworkFormComponent', () => {
  let component: NewFieldworkFormComponent;
  let fixture: ComponentFixture<NewFieldworkFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFieldworkFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFieldworkFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
