import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  OnChanges,
  ViewChild,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: "app-new-action-form",
  templateUrl: "./new-action-form.component.html",
  styleUrls: ["./new-action-form.component.less"],
})
export class NewActionFormComponent implements OnInit, OnChanges {
  @Output() closedAddActionForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @Input("actionData") actionData: string;
  @ViewChild("AddNewAction", { static: false }) AddNewAction: TabsetComponent;
  actionForm: any;
  // actionData:any;
  modalRef: BsModalRef;
  AddNewActionForm: any;
  uploadedFilesArr: any;
  attachedFiles: any = [];
  uri = environment.baseUrl;
  generatedActionId: any;
  ActionForm: FormGroup;
  identifiedDate: any;
  actionStatusMaster: any;
  issueActionMatrixData: any;
  actionRatingMaster: any;
  fetchedData: any;
  multiFile: any = [];
  position;
  performancePeriod: any;
  area: any;
  openAttachment: boolean;
  marked: boolean;
  isAttachment: boolean;

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.dropDownMasters();
    this.isAttachment = false;
    this.openAttachment = true;
  }
  ngOnChanges() {
    this.attachedFiles = [];
    let payload = { actionId: this.actionData };
    //console.log('hey',payload);
    this.httpservice
      .securePost(ConstUrls.getOneAction, payload)
      .subscribe(async (response) => {
        this.issueActionMatrixData = await response;
        //console.log(this.issueActionMatrixData);
        this.initForm();
        this.ActionForm.patchValue(response);

        this.ActionForm.patchValue({
          performancePeriod: new Date(
            this.issueActionMatrixData.performancePeriod
          ),
          signOffPeriod: new Date(this.issueActionMatrixData.signOffPeriod),
        });
      });
  }
  dropDownMasters() {
    this.httpservice
      .get(ConstUrls.actionStatusMaster)
      .subscribe((response: any) => {
        this.actionStatusMaster = response;
      });

    this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
      this.area = response;
    });

    this.httpservice
      .get(ConstUrls.actionRatingMaster)
      .subscribe((response: any) => {
        this.actionRatingMaster = response;
      });
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);

    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
    this.AddNewAction.tabs[1].active = true;
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;

      //console.log('left curr status:' , this.marked)
    } else {
      // attachment left

      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }

  initForm() {
    this.actionForm = this.formBuilder.group({
      actionId: ["", []],
      actionName: ["", []],
      actionDescription: ["", []],
      approvedBy: ["", []],
      approvedId: ["", []],
      area: ["", []],
      comments: ["", []],
      rating: ["", []],
      riskArea: ["", []],
      actionStatus: ["", []],
      correctiveAction: ["", []],
      dateApproved: ["", []],
      dateExecuted: ["", []],
      executedBy: ["", []],
      executerId: ["", []],
      guidance: ["", []],
      issueId: ["", []],
      performancePeriod: ["", []],
      signOffPeriod: ["", []],
    });
  }

  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.isAttachment = true;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        let extension = ext.relativePath.split(".")[1];
        ext.extension = extension;
      }
    }
    //console.log('upload files array :  ' , this.uploadedFilesArr);
    //console.log('files array: ' , this.files);
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          this.attachedFiles.push({
            relativePath: file.name.split(".")[0],
            extension: file.name.split(".")[1],
            fullName: file.name,
          });
          //console.log(droppedFile.relativePath, file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  removeAttachment(removeitem, index) {
    const fileindex = parseInt(index);
    let payload = {
      directory: "uploads/caseMgmt",
      fullName: removeitem.fullName,
    };
    //console.log('attachment',removeitem,'payload',payload);
    this.httpservice
      .securePost("removeAttachment", payload)
      .subscribe((response: any) => {
        if (response) {
          //console.log('attached files array',this.attachedFiles,'index from method',fileindex);
          const index = this.attachedFiles.indexOf(fileindex);
          {
            this.attachedFiles.splice(fileindex, 1);
          }
          this.fetchedData = this.attachedFiles;

          // array = [2, 9]
          setTimeout(function () {
            console.log(this.attachedFiles);
          }, 3000);
          // alert('file deleted sucessfully..!');
          if (response) {
            const initialState = {
              title: "Success",
              content:
                "Action : " +
                this.actionData +
                " Attachment Deleted Successfully",
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
            // this.closedAddDefinationActionForm.emit(true);
            // this.closeModal();
          }
        }
      });
    //  /api/removeAttachment
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    //console.log('this is data',data)
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.issueActionMatrixData.attachedFiles) {
        this.fetchedData.push(data[i]["name"]);
      } else {
        this.fetchedData = [];
        this.fetchedData.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData;
    //console.log('attchment array',this.fetchedData)
    this.actionForm.patchValue({ attachment: arr });
    this.httpservice
      .securePost("multipleuploadfilecase", formData)
      .subscribe((files) => {
        //console.log('Uploaded');
        this.multiFile = [];
      });
  }
  getDocument(file) {
    // const initialState = {
    //   title: file.fullName,
    //   class: "modal-xl"
    // };
    // // this.modalService.show(FilePreviewComponent, { initialState });
    window.open(
      this.uri + "uploads/caseMgmt/" + file.fullName,
      "_blank",
      "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
    );
  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }
  getOneAction() {
    let payload = { actionId: this.actionData };
    this.httpservice
      .securePost(ConstUrls.getOneAction, payload)
      .subscribe(async (response) => {
        this.fetchedData = response;
        //  this.fetchedData.barColor= this.fetchedData.controlRisk>=0 && this.fetchedData.controlRisk<=33  ? "medium" : this.fetchedData.controlRisk>=34 && this.fetchedData.controlRisk<=66 ?"very-low" :"very-high";
        this.actionForm.patchValue(response);
        if (this.fetchedData.attachment) {
          for (let i = 0; i < this.fetchedData.attachment.length; i++) {
            this.attachedFiles.push({
              relativePath: this.fetchedData.attachment[i].split(".")[0],
              extension: this.fetchedData.attachment[i].split(".")[1],
              fullName: this.fetchedData.attachment[i],
            });
          }
        }
      });
  }

  onSave() {
    //console.log(this.actionForm.value)
    this.generatedActionId = "A-" + Math.floor(Math.random() * 500) + 1;
    this.performancePeriod = new Date(
      this.actionForm.get("area").value
    ).toLocaleDateString();
    if (this.performancePeriod === "Invalid Date") {
      this.performancePeriod = new Date().toLocaleDateString();
    }

    // this.actionForm.patchValue({ 'actionId':this.generatedActionId ,'controlAddedDate':this.performancePeriod });
    this.actionForm.patchValue({ actionId: this.generatedActionId });
    this.httpservice
      .securePost(ConstUrls.saveAction, this.actionForm.value)
      .subscribe((response: any) => {
        if (response) {
          const initialState = {
            title: "Success",
            content: "Action saved Successfully",
            link: "Ok",
          };
          this.modalRef = this.modalService.show(DefaultModalComponent, {
            initialState,
            class: "success-class",
            backdrop: "static",
            keyboard: false,
          });
          this.closedAddActionForm.emit(true);
          this.closeModal();
        }
      });
  }
}
