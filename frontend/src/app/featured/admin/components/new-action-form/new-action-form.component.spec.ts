import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewActionFormComponent } from './new-action-form.component';

describe('NewActionFormComponent', () => {
  let component: NewActionFormComponent;
  let fixture: ComponentFixture<NewActionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewActionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewActionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
