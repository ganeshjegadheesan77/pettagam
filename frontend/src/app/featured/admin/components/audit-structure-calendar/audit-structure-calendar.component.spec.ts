import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditStructureCalendarComponent } from './audit-structure-calendar.component';

describe('AuditStructureCalendarComponent', () => {
  let component: AuditStructureCalendarComponent;
  let fixture: ComponentFixture<AuditStructureCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditStructureCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditStructureCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
