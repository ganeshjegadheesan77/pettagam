import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Item, Period, Section, Events, NgxTimeSchedulerService } from 'ngx-time-scheduler';
import * as moment from 'moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SchedulerModalComponent } from "src/app/layout/modal/scheduler-modal/scheduler-modal.component";
import { CalendarDataService } from '../../calendar-data.service';
@Component({
  selector: 'app-audit-structure-calendar',
  templateUrl: './audit-structure-calendar.component.html',
  styleUrls: ['./audit-structure-calendar.component.less']
})
export class AuditStructureCalendarComponent implements OnInit {
  modalRef: BsModalRef;
  events: Events = new Events();
  periods: Period[];
  sections: Section[] = new Array<Section>();
  items: Item[] = new Array<Item>();
  name = "...";
  calendarData;
  displayCalendar = false;
  constructor(private service: NgxTimeSchedulerService, private modalService: BsModalService, private calendarDataService: CalendarDataService) { }

  ngOnInit() {
    this.calendarDataService.calendarData.subscribe(cal => {
      if (cal.items) {
        this.displayCalendar = false;
        this.items = cal.items;
        this.periods = cal.periods;
        if(cal.sections.length > 3){
          this.sections = cal.sections.slice(0,3); //only 3 users in dashboard
        }
        else{
          this.sections = cal.sections;
        }
        
        this.service.refresh();
        this.displayCalendar = true;
      }
    });
    this.calendarDataService.data.subscribe(d => {
      if (d) {
        this.name = d;
      }
    })
    // this.calendarDataService.data.subscribe(d => {
    //   if (d.auditId) {
    //     this.displayCalendar = false;
    //     this.name = d.auditId;
    //     this.calendarData = d;
    //     this.sections = new Array<Section>();
    //     this.items = new Array<Item>();
    //     this.service.refresh();
    //     var auditTeam = [...new Set(this.calendarData.auditTeam)];
    //     for (let i = 0; i < auditTeam.length; i++) {
    //       this.sections.push({ id: (i + 1), name: auditTeam[i] as string });

    //       // this.items.push({
    //       //   id: i + 1,
    //       //   sectionID: i + 1,
    //       //   name: "",
    //       //   start: "",
    //       //   end: "",
    //       //   classes: ""
    //       // });
    //     };

    //     this.periods = [

    //       {
    //         name: '1 week',
    //         timeFrameHeaders: ['DD'],
    //         classes: '',
    //         timeFrameOverall: 1440 * 7,
    //         timeFramePeriod: 1440,
    //       },

    //     ];

    //     this.items = [
    //       {
    //         id: 1,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.pAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.pAssignee[0])[0].id,
    //         name: 'Planning Execution',
    //         start: moment(this.calendarData.exStart).add(0, 'days').startOf('day'),
    //         end: moment(this.calendarData.exEnd).startOf('day'),
    //         classes: 'schedule-green'
    //       },
    //       {
    //         id: 2,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.papAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.papAssignee[0])[0].id,
    //         name: "Planning Approval",
    //         start: moment(this.calendarData.apStart).startOf('day'),
    //         end: moment(this.calendarData.apEnd).startOf('day'),
    //         classes: 'schedule-orange'
    //       },
    //       {
    //         id: 3,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.fAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.fAssignee[0])[0].id,
    //         name: "Fieldwork Execution",
    //         start: moment(this.calendarData.fexStart).startOf('day'),
    //         end: moment(this.calendarData.fexEnd).startOf('day'),
    //         classes: 'schedule-blue'
    //       },
    //       {
    //         id: 4,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.fapAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.fapAssignee[0])[0].id,
    //         name: "Fieldwork Approval",
    //         start: moment(this.calendarData.fapStart).startOf('day'),
    //         end: moment(this.calendarData.fapEnd).startOf('day'),
    //         classes: 'schedule-green'
    //       },
    //       {
    //         id: 5,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.rAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.rAssignee[0])[0].id,
    //         name: "Reporting Execution",
    //         start: moment(this.calendarData.rexStart).startOf('day'),
    //         end: moment(this.calendarData.rexEnd).startOf('day'),
    //         classes: 'schedule-orange'
    //       },
    //       {
    //         id: 6,
    //         sectionID: this.sections.filter(s => s.name == this.calendarData.rapAssignee[0])[0] == undefined ? 0 : this.sections.filter(s => s.name == this.calendarData.rapAssignee[0])[0].id,
    //         name: "Reporting Approval",
    //         start: moment(this.calendarData.rapStart).startOf('day'),
    //         end: moment(this.calendarData.rapEnd).startOf('day'),
    //         classes: 'schedule-blue'
    //       }
    //     ];

    //     this.items == this.items.filter(x=>x.id != 0);



    //     this.service.refresh();
    //     this.displayCalendar = true;

    //     this.calendarDataService.updateCalendatData({items:this.items,periods:this.periods,sections:this.sections});
    //   }
    // });

    // this.events.SectionClickEvent = (section) => { console.log(section); };
    // this.events.ItemClicked = (item) => { console.log(item); };
    // this.events.ItemDropped = (item) => { console.log(item); };



    // this.sections = [{
    //   name: 'Sharon',
    //   id: 1
    // }, {
    //   name: 'Jacob',
    //   id: 2
    // }, {
    //   name: 'Emily',
    //   id: 3
    // }];

    // this.items = [{
    //   id: 1,
    //   sectionID: 1,
    //   name: 'Item 1',
    //   start: moment().add(0, 'days').startOf('day'),
    //   end: moment().add(4, 'days').endOf('day'),
    //   classes: 'schedule-green'
    // }, {
    //   id: 2,
    //   sectionID: 2,
    //   name: 'Item 2',
    //   start: moment().add(1, 'days').startOf('day'),
    //   end: moment().add(5, 'days').endOf('day'),
    //   classes: 'schedule-orange'
    // }, {
    //   id: 3,
    //   sectionID: 3,
    //   name: 'Item 3',
    //   start: moment().add(2, 'days').startOf('day'),
    //   end: moment().add(7, 'days').endOf('day'),
    //   classes: 'schedule-blue'
    // }];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.service.refresh();
  }

  modalClick() {
    const initialState = {
      title: '',
      content: 'Action : updated Successfully',
      link: 'Ok',
      action: 'app-audit-structure-calendar'
    };
    this.modalRef = this.modalService.show(SchedulerModalComponent, { initialState, class: 'success-class schedule-calendar-modal', backdrop: 'static', keyboard: false });
  }
}
