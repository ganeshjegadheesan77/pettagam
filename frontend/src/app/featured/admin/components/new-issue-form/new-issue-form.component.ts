import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  OnChanges,
  ViewChild, ChangeDetectorRef
} from "@angular/core";
import * as moment from "moment";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { AdminService } from "../../admin.service";
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import { ConfirmRejectWorkstepComponent } from 'src/app/layout/modal/confirm-reject-workstep/confirm-reject-workstep.component';
@Component({
  selector: "app-new-issue-form",
  templateUrl: "./new-issue-form.component.html",
  styleUrls: ["./new-issue-form.component.less"],
})
export class NewIssueFormComponent implements OnInit, OnChanges {
  @Output() closedAddNewIssueForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @Input("issueData") issueData: string;
  @ViewChild("addNewIssue", { static: false }) addNewIssue: TabsetComponent;
  @Input() auditId;
  @Input() issueId;
  @Input() auditableEntity;
  @Output() closePopup = new EventEmitter<any>();
  modalRef: BsModalRef;
  generatedIssueId: any;
  identifiedDate: any;
  closeAttachment:Boolean
  position;
  arr: any[];
  globalSearchData = "";
  disableSearch = false;
  IssueForm: FormGroup;
  issueActionMatrixData: any;
  issueStatusMaster: any;
  area: any;
  issuePriorityMaster: any;
  issueTypeMaster: any;
  isAuditor = true;
  issueForm;
  editsearchDocument: boolean = false;
  title: string;
  content: string;
  isNew = true;
  attachedFiles: any = [];
  searchDocument: boolean = false;
  marked: boolean;
  openAttachment: boolean;
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private adminService: AdminService,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService, private chRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
   
    this.initForm();
    this.dropDownMasters();
    this.isAuditor = true;
    var username = localStorage.getItem('username');
    if (username == 'elmar') {
      this.isAuditor = false;
    }
    // this.auditId = "OP-013";
    console.log(this.auditId, this.auditableEntity, this.issueId);
    // this.issueId = "I-01";
    this.issueForm = new EditIssueModel();
    this.isNew = true;
    if (this.issueId) {
      this.isNew = false;
      this.getIssueFromJson(this.issueId);
      // this.issueForm = new EditIssueModel();
      // this.issueForm.issueId = this.issueId;
    }
    this.issueForm.auditId = this.auditId;
    this.issueForm.auditableEntity = this.auditableEntity;
    this.issueActiondatatable()
    this.issueActionSelecteddatatable();

    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true,
    };
  }
  ngOnChanges() {
    console.log("issueData:", this.issueData);
  }
  //////////////////////////////////// 22-10-20 ////////////////////////////////////////////////////

  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    let timer = title == "Alert" ? 6000 : 3000;
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",

      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, timer);
  }


  getSelectedDocs() {
    // this.searchDocument = false;
    var fileList = this.selectedItems.map((item) => {
        return item["fileName"];
      });
      console.log("Selected files: ", this.selectedItems);
      for (let i = 0; i < this.selectedItems.length; i++) {
        this.selectedItems[i]['metaData']=`Module-Name=AgileAuditSpace,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
        // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
        this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
        this.selectedItems[i]['userType'] = 'auditor';
        this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
        this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
        this.selectedItems[i]['roomType']='';
        this.selectedItems[i]['routerLink']='';
        this.selectedItems[i]['subModuleName']='issue-form';
        this.selectedItems[i]['auditYear']=new Date().getFullYear();
        this.selectedItems[i]['auditId']=this.auditId;
        this.selectedItems[i]['auditName']=this.auditableEntity;
        this.selectedItems[i]['moduleName']='AgileAuditSpace';
        this.selectedItems[i]['checkedinDate']= moment().format("L");
        
      }
      console.log('Selected Item ARRAY: ',this.selectedItems);
      let payloads ={items:this.selectedItems};
      this.httpservice.securePost(ConstUrls.DmsSaveAsDocument , payloads).subscribe((res:any)=>{
        if(res){
          console.log('res', res);
          for (let i = 0; i < res.length; i++) {
            console.log('filename', res[i].fileName)
             let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            // res[i].fileName.substr(res[i].fileName.lastIndexOf('.') + 1)
          this.attachedFiles.push({documentName:res[i].fileName , documentNo:res[i].documentNo
             ,documentUrlToken:res[i].urlToken , extension:extn});
          console.log('res[i].fileName', res[i].fileName)
            
          }
          this.title = "Success";
          this.content = "File Uploaded successfully...";
          this.myCommonAlert(this.title, this.content);
          this.closeSearch()
        }
      },(error)=>{
        this.myCommonAlert(error.massage, 'Error');
        
      })

  
    this.editsearchDocument = false;
  }


  globalSearchKeyPress() {
    console.log("hii");
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 3) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];
      // let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: localStorage.getItem('dmsToken') };
      // let payload = { searchQuery: this.globalSearchData };
      let payload = { searchQuery: this.globalSearchData, auditId: this.auditId, moduleName: 'In-Tray', dmsSessionId: "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185" , searchBy:localStorage.getItem('username') };

      this.httpservice
        .post(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          console.log("records", records);
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              // console.log('d', d)
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName, _id: d._id });
            });
            this.dropdownListAll = this.arr;
            console.log('dropdownListAll', this.dropdownListAll)
            this.dropdownFirmList = this.arr;
            // this.attachedFiles = this.arr;
            // this.dropdownList =this.arr;
          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
            // this.dropdownList=[];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
    
    
  }

  onFilechanged(event) {
    console.log(event.target.files);
    var files = [];
    if (this.fileValidation()) {
        const formData: any = new FormData();
        files = event.target.files;
        console.log("fil anme", files);
        formData.append("file", files[0], files[0]["name"]);
        formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
        formData.append("userType", 'auditor');
        formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=AgileAuditSpace,Feature-Name=issueForm,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
        formData.append("routerLink", ""),
        formData.append("subModuleName", "issue-Form"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.auditId),
        formData.append("auditName", this.auditableEntity),
        formData.append("moduleName", "AgileAuditSpace"),
        formData.append("checkedinDate", moment().format("L"));
        console.log('****** formData', formData);
        this.httpservice.post(ConstUrls.DmsUpload, formData).subscribe((res:any) => {
          if(res){
            for (let i = 0; i < res.length; i++) {
              console.log('filename', res[i].fileName)
               let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            this.attachedFiles.push({documentName:res[i].fileName , documentNo:res[i].documentNo
               ,documentUrlToken:res[i].urlToken , extension:extn});
            console.log('res[i].fileName', res[i].fileName)
              
            }
            this.title = "Success";
            this.content = "File Uploaded successfully...";
            this.myCommonAlert(this.title, this.content);
            this.closeSearch()

          }
        }
        ,(error)=>{
          this.myCommonAlert('Error', error.message);

        })
   
      

    } else {
      this.myCommonAlert('Error', 'Invalid file');
      $("#myFile").val('');
    }
  }
  

  deleteFile(index, fileObj) {

    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete current File?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {

      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: fileObj.documentNo,
          fileName: fileObj.documentName,
          deletedBy: localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
          // _id: fileObj.dmsRepoDetail._id
        };
        this.httpservice
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res.errorCode == 0) {
            
                this.attachedFiles.splice(index, 1)
                this.title = "Success";
                this.content = "File Deleted successfully...";
                this.myCommonAlert(this.title, this.content);
              
      
            } else {
              this.myCommonAlert("Error", res.message);
            }
          })
      }
    });
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    console.log('extension', extension)
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  dropDownMasters() {
    this.httpservice
      .get(ConstUrls.issueStatusMaster)
      .subscribe((response: any) => {
        //console.log("jhjbjhb",response)
        this.issueStatusMaster = response;
      });

    this.httpservice
      .get(ConstUrls.issueTypeMaster)
      .subscribe((response: any) => {
        this.issueTypeMaster = response;
      });

    this.httpservice.get(ConstUrls.masterArea).subscribe((response: any) => {
      this.area = response;
    });

    this.httpservice
      .get(ConstUrls.issuePriorityMaster)
      .subscribe((response: any) => {
        this.issuePriorityMaster = response;
      });
  }


  initForm() {
    this.IssueForm = this.formBuilder.group({
      issueName: ["", []],
      action: ["", []],
      area: ["", []],
      department: ["", []],
      issueDescription: ["", []],
      issueId: ["", []],
      auditId: ["", []],
      keyIsuue: ["false", []],
      managementResponse: ["", []],
      priority: ["", []],
      recommendation: ["", []],
      reviewComments: ["", []],
      approvedBy: ["", []],
      approverId: ["", []],
      dateApproved: ["", []],
      dateIdentified: ["", []],
      identifiedBy: ["", []],
      identifierId: ["", []],
      issueType: ["", []],
      issuePriority: ["", []],
      issueStatus: ["", []],
      status: ["", []],
    });
  }
  onSave() {
    this.generatedIssueId = "I-" + Math.floor(Math.random() * 500) + 1;
    this.identifiedDate = new Date(
      this.IssueForm.get("area").value
    ).toLocaleDateString();
    if (this.identifiedDate === "Invalid Date") {
      this.identifiedDate = new Date().toLocaleDateString();
    }
    console.log("this.issueData[auditId]:", this.issueData["auditId"]);
    this.IssueForm.patchValue({
      issueId: this.generatedIssueId,
      controlAddedDate: this.identifiedDate,
      auditId: this.issueData["auditId"],
      status: "wip",
    });

    this.httpservice
      .securePost(ConstUrls.saveIssue, this.IssueForm.value)
      .subscribe((response: any) => {
        if (response) {
          const initialState = {
            title: "Success",
            content: "Issue saved Successfully",
            link: "Ok",
          };
          this.modalRef = this.modalService.show(DefaultModalComponent, {
            initialState,
            class: "success-class",
            backdrop: "static",
            keyboard: false,
          });
          this.adminService.sendCount(true);
          this.closedAddNewIssueForm.emit(true);
          this.closeModal();
        }
      });
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '180px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',

    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [

        'redo',



        'strikeThrough',
        'subscript',
        'superscript',

        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };
  closeModal() {

    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    $(".animation-save").removeClass('active-btn');
    $(".animation-submit").removeClass('active-btn');
    this.position = this.position ? undefined : { x: 0, y: 0 };
    this.closePopup.emit();
  }

  submitForm() {
    $(".animation-submit").addClass('active-btn');
    console.log("Completed", this.issueForm);
    this.UpdateIssueInJson("completed");
    this.closeModal();
  }

  generateRandomNumber() {
    return Math.floor(Math.random() * 90 + 10);
  }

  UpdateIssueInJson(status) {
    // var jsonName = this.isAuditor ? 'auditorData' : 'auditLeadData';
    // this.issueForm.cardStatus = status;
    var jsonData = JSON.parse(localStorage.getItem('auditorData'));
    var auditIndex = jsonData.findIndex(j => j.audit == this.auditId);

    if (auditIndex != -1) {
      if (jsonData[auditIndex].issues && jsonData[auditIndex].issues.length > 0) {
        if (this.issueId) {
          var issueIndex = jsonData[auditIndex].issues.findIndex(i => i.issueId == this.issueId);
          // if (status == 'completed' && !this.isAuditor && jsonName == 'auditLeadData') {
          //   jsonData[auditIndex].issues.splice(issueIndex, 1);
          //   this.issueForm.cardStatus = 'wip';
          //   // add to auditor json
          //   var auditorJson = JSON.parse(localStorage.getItem('auditorData'));
          //   auditorJson.find(a => a.audit == this.auditId).issues.push(this.issueForm);
          //   localStorage.setItem('auditorData', JSON.stringify(auditorJson));
          // }
          // else {
          if (status == 'wip' && this.issueForm.cardStatus == 'completed') {
            this.issueForm.cardStatus = 'completed';
          }
          else {
            this.issueForm.cardStatus = status;
          }
          if (issueIndex != -1) {
            jsonData[auditIndex].issues[issueIndex] = this.issueForm;
          }
          else {
            jsonData[auditIndex].issues.push(this.issueForm);
          }
          localStorage.setItem('auditorData', JSON.stringify(jsonData));
          // }
        }
        else {
          var auditorJson = JSON.parse(localStorage.getItem('auditorData'));

          // if (issuesArray && issuesArray.length > 0) {
          // var lastIssueId = auditorJson.find(a => a.audit == this.auditId).issues.slice(-1)[0].issueId;
          // var issueIdNumber = Number.parseInt(lastIssueId.split("-").slice(-1)[0], 10);
          // issueIdNumber += 1;
          var newIssueId = "I-0" + this.generateRandomNumber();
          this.issueForm.issueId = newIssueId;
          this.issueForm.cardStatus = status;
          auditorJson.find(a => a.audit == this.auditId).issues.push(this.issueForm);
          localStorage.setItem('auditorData', JSON.stringify(auditorJson));
          // }
        }
      }
      else {
        //save issues to auditor wip only
        // else {
        var newIssueId = "I-0"+ this.generateRandomNumber();
        this.issueForm.cardStatus = status;
        this.issueForm.issueId = newIssueId;

        var auditorJson = JSON.parse(localStorage.getItem('auditorData'));
        auditorJson.find(a => a.audit == this.auditId).issues.push(this.issueForm);
        localStorage.setItem('auditorData', JSON.stringify(auditorJson));
        // jsonData[auditIndex].issues.push(this.issueForm);
        // }
      }
    }
  }

  getIssueFromJson(issueId) {
    var jsonName = 'auditorData';
    this.issueForm.cardStatus = status;
    var issuesArray = JSON.parse(localStorage.getItem(jsonName)).find(a => a.audit == this.auditId).issues;
    if (issuesArray) {
      var issue = issuesArray.find(i => i.issueId == issueId);
      if (issue) {
        this.issueForm = issue;
      }
    }
  }

  saveForm() {
    $(".animation-save").addClass('active-btn');
    console.log("WIP", this.issueForm);
    this.UpdateIssueInJson("wip");
    this.closeModal();
  }

  issueActiondatatable() {

    this.chRef.detectChanges();
    const issueTable: any = $("#issueActionTable");
    const issueListTableCM = this.dataTableService.initializeTable(
      issueTable,
      `issueActionAgile`
    );
    this.dataTableFuncService.selectAllRows(
      issueListTableCM,
      `issueAction-select-all`
    );
    this.dataTableFuncService.expandContainer(issueListTableCM);
    $("#issueActionTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issueAction-select-all").prop("checked", false);
          var el = $("#issueAction-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issueActionTable tr td input[type='checkbox']:checked").length ==
          $("#issueActionTable tr td input[type='checkbox']").length
        ) {
          $("#issueAction-select-all").prop("checked", true);
        }
      }
    );
  
  }
  issueActionSelecteddatatable() {
    this.chRef.detectChanges();
    const issueSelectedTable: any = $("#issueActionSelectedTable");
    const issueListSelectedTableCM = this.dataTableService.initializeTable(
      issueSelectedTable,
      `issueActionSelectedAgile`
    );
    this.dataTableFuncService.selectAllRows(
      issueListSelectedTableCM,
      `issueActionSelected-select-all`
    );
    this.dataTableFuncService.expandContainer(issueListSelectedTableCM);
    $("#issueActionSelectedTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issueActionSelected-select-all").prop("checked", false);
          var el = $("#issueActionSelected-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issueActionSelectedTable tr td input[type='checkbox']:checked").length ==
          $("#issueActionSelectedTable tr td input[type='checkbox']").length
        ) {
          $("#issueActionSelected-select-all").prop("checked", true);
        }
      }
    );
  
  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
      
      //console.log('left curr status:' , this.marked)
      
    } else {
      // attachment left
  
      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }
  rejetWorkStep() {
    const initialState = {
      title: "Confirm Reject Issue",
      content: `WS-102`,
      link: "Yes, Reject the Issue",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmRejectWorkstepComponent, {
      initialState,
      class: "error-popup-design extra-popup issue-reject-popup",
      backdrop: "static",
      keyboard: false,
    });
  }
  addnewFile() {
    this.searchDocument = true;
    this.selectedItems = [];
  }
  closeSearch() {
    this.searchDocument = false;
    this.editsearchDocument = false;
    this.globalSearchData = "";
  }

}

export class NewIssueModel {
  constructor() {
    this.reviewer = "Elmar";
    this.preparer = "Amelia";
    this.auditId = "-"
    this.auditableEntity = "-";
    this.auditSite = "-";
    this.status = "";
    this.priority = "";
    this.issueName = "-";
  }

  auditId;
  issueName;
  auditableEntity;
  auditSite;
  reviewer;
  preparer;
  status;
  priority;
  shortDescription;
  reviewComments;
  longDescription;
  recommendation;
  managementResponse;
  actions;
}

export class EditIssueModel {
  constructor() {
    // this.issueId = "I-01"
    // this.issueName = "This is issue name"
    this.preparer = "Amelia";
    this.reviewer = "Elmar";
    this.cardStatus = "";
    // this.auditId = "OP-013";
    // this.auditableEntity = "Procurement";
    // this.auditSite = "Pasar";
    this.status = "";
    this.priority = "";
    this.actions = [
      { actionName: 'Increase Control Frequency', responsible: 'George White', deadline: '25.08.2020' },
      { actionName: 'More Frequent Model Review', responsible: 'David Bangkok', deadline: '27.08.2020' },
      { actionName: 'Trigger Assessment On Change In Counterparty Parameters', responsible: 'Thomas George', deadline: '29.08.2020' }
    ];
    // this.shortDescription = "Procurement pasar short description it is a short desc";
    // this.reviewComments = "this is review comments";
    // this.longDescription = "Procurement pasar long desc long desclong desclong desclong desclong desclong desclong desc long desclong desclong desclong desclong desclong desclong desclong desclong desc";
    // this.recommendation = "this is recommendation field";
    // this.managementResponse = "this is management response";
  }

  // constructor() {
  //   this.reviewer = "Thomas"
  // }
  issueId;
  issueName;
  auditId;
  auditableEntity;
  auditSite;
  reviewer;
  preparer;
  status;
  priority;
  shortDescription;
  reviewComments;
  longDescription;
  recommendation;
  managementResponse;
  cardStatus;
  actions;
}