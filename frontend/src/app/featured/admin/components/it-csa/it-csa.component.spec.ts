import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItCsaComponent } from './it-csa.component';

describe('ItCsaComponent', () => {
  let component: ItCsaComponent;
  let fixture: ComponentFixture<ItCsaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItCsaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItCsaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
