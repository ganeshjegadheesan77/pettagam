import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFieldworkFormComponent } from './edit-fieldwork-form.component';

describe('EditFieldworkFormComponent', () => {
  let component: EditFieldworkFormComponent;
  let fixture: ComponentFixture<EditFieldworkFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFieldworkFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFieldworkFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
