import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { HttpService } from "src/app/core/http-services/http.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { AdminService } from "../../admin.service";
declare var $;

@Component({
  selector: "app-edit-fieldwork-form",
  templateUrl: "./edit-fieldwork-form.component.html",
  styleUrls: ["./edit-fieldwork-form.component.less"],
})
export class EditFieldworkFormComponent implements OnInit {
  @Input("WfFormData") WfFormData: any;
  @Output() closedFWForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  position;
  fieldWorkForm: FormGroup;
  modalRef: BsModalRef;
  fwName: any;
  multiFile: any = [];
  fetchedData: any;
  uri = environment.baseUrl;
  uploadedFilesArr;
  marked: boolean;
  openAttachment: boolean;
  attachedFiles: any = [];
  auditFw_id: any;
  title: string;
  content: string;
  myData: any;
  fieldPanes: any;
  mixArr: any;
  testerArr: any[];
  reviewerArr: any[];
  fAssignee: string[] = [];
  fapAssignee : string[] = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private adminService: AdminService
  ) {}
  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
      this.initForm();
      this.getFieldWorkData(); 
    // console.log('Data from main Page: ' , this.WfFormData);

    }
    

    getFieldWorkData(){
      if(this.WfFormData){
        console.log('Data', this.WfFormData)
        let payload = { auditFw_id:this.WfFormData};
       
        this.httpservice.securePost(ConstUrls.getFWDetails , payload).subscribe((res:any)=>{
          let load = {auditId:localStorage.getItem('ID')};

          this.httpservice.securePost(ConstUrls.getTeamForPbcAndFW , load).subscribe((resp:any)=>{
            console.log('resp', resp)
             //Audit team code
            this.fAssignee = resp[0].resourePlanningData[0].fAssignee;
            this.fapAssignee = resp[0].resourePlanningData[0].fapAssignee;
           this.mixArr = resp[0].resourePlanningData[0];
           console.log('mixArr', this.mixArr)
           let newArr =[];
           this.testerArr=[];
           this.reviewerArr=[];
            if(this.mixArr){
              this.mixArr.fAssignee.forEach(ele => {
                this.testerArr.push(ele);
              });
              
              this.mixArr.fapAssignee.forEach(ele => {
                this.reviewerArr.push(ele);
              });
            }
          })
          this.fetchedData = res[0];
          console.log('res', res)
          //console.log('Attchment ??', this.fetchedData.afw_attachments)
          if(this.fetchedData.afw_attachments.length !=0){
            this.openAttachment = true;
           for(let i=0;i<this.fetchedData.afw_attachments.length;i++){
             let extn = this.fetchedData.afw_attachments[i].substr(this.fetchedData.afw_attachments[i].lastIndexOf('.') + 1);
             let name = this.fetchedData.afw_attachments[i].substr(0, this.fetchedData.afw_attachments[i].lastIndexOf('.')); 
             
             this.attachedFiles.push({'relativePath':name,
           'extension':extn,'fullName':name+'.'+extn,'new':false
           })
           }
         }else{
           this.openAttachment = false;

         }
          if(res){
            console.log('res', res)
            this.myData = res[0];
            this.fwName=this.myData.afw_name;
            this.auditFw_id = this.myData.auditFw_id;
            this.fieldWorkForm.patchValue(this.myData);
            console.log('res', res)
         this.fieldPanes=res[0].masterData[0];
         if(!moment(this.fieldPanes.performancePeriod[0], 'DD-MM-YYYY',true).isValid() && this.fieldPanes.performancePeriod.length>0){
          let d1=moment(this.fieldPanes.performancePeriod[0]).format("DD-MM-YYYY");
          let d2=moment(this.fieldPanes.performancePeriod[1]).format("DD-MM-YYYY");
          let str = d1+'-'+d2;
           this.fieldWorkForm.patchValue({'performance_period':str});
        }else if(this.fieldPanes.performancePeriod.length==0){
          this.fieldWorkForm.patchValue({'performance_period':this.fieldPanes.performancePeriod});
        }else{
          let d1=this.fieldPanes.performancePeriod[0];
          let d2=this.fieldPanes.performancePeriod[1];
          let str = d1+'-'+d2;
          this.fieldWorkForm.patchValue({'performance_period':str});
        }
            // this.fieldWorkForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
          
          }
        })
      }
      
    }

    public files: NgxFileDropEntry[] = [];
 
    public dropped(files: NgxFileDropEntry[]) {
      this.files = files;
     this.uploadedFilesArr = this.files;
      for(let ext of this.uploadedFilesArr){
        
        if(ext.relativePath){
          // console.log('relative: ' , ext.relativePath);
          var extn = ext.relativePath.substr(ext.relativePath.lastIndexOf('.') + 1);
          var name = ext.relativePath.substr(0, ext.relativePath.lastIndexOf('.')); 
          ext.relativePath= name.replace(/\W+/g, "-")+"."+extn;
            //  let extension = ext.relativePath.split('.')[1];
              ext.extension = extn;
          
          
         
            //console.log('Updated File Name:' ,ext.relativePath , 'File Extension:' , ext.extension);
         
        }
        // if(ext.relativePath){
        //   // console.log('relative: ' , ext.relativePath);
        //    let extension = ext.relativePath.split('.')[1];
        //     ext.extension = extension;
        // }
      }
       //console.log('upload files array :  ' , this.uploadedFilesArr);
      //console.log('files array: ' , this.files);
      for (const droppedFile of files) {
   
        // Is it a file?
        if (droppedFile.fileEntry.isFile) {
          const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
          fileEntry.file((file: File) => {
            this.multiFile.push(file)
            let extn = file.name.substr(file.name.lastIndexOf('.') + 1);
            let name = file.name.substr(0, file.name.lastIndexOf('.')); 
            
            this.attachedFiles.push({'relativePath':name.replace(/\W+/g, "-")+'.'+extn,
        'extension':extn,'fullName':file.name,'new':true
        })
            //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
          });
        } else {
          const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
          //console.log(droppedFile.relativePath, fileEntry);
        }
      }
    }

    switchProperty(e) {
      this.marked = e.target.checked;
      if (this.marked == false) {
        // attachment selected
        this.openAttachment = false;
        //console.log('left curr status:' , this.marked)
      } else {
        this.openAttachment = true;
        //console.log('selected curr status:' , this.marked)
      }
    }

    upload() {
      const formData:any= new FormData();
      const data: Array<File>= this.multiFile;
      for(let i=0;i<data.length;i++){
        formData.append("file", data[i], data[i]['name']);
        if(this.fetchedData.afw_attachments){
          this.fetchedData.afw_attachments.push(data[i]['name'])
        }
        else{
          this.fetchedData.afw_attachments=[];
          this.fetchedData.afw_attachments.push(data[i]['name'])
    
        }
      }
      let arr=[]
      arr=this.fetchedData.afw_attachments;
      this.fieldWorkForm.patchValue({'afw_attachments':arr});
      //console.log('Form Data : ' , this.PBCForm.value);
      console.log('filearray',arr.toString());
      let filearr = []; 
          arr.forEach(ele=> {
        filearr.push('fieldWork/'+ ele)
      });
     let payload = {
       "collection": "document",
       "module": "fieldWork",
       "sub-module": "audit-announcement",
       "tab": "audit-announcement",
       "router-link": "/rcm/internal-audit/engagement-planning",
       
       "file" :filearr.toString() ,
      "metadata": "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
       "username": localStorage.getItem('username'),
       "database" : environment.db
   }
   
   console.log('check',payload)
     this.httpservice.secureJavaPostReportApi('DocumentManagementService/put',payload).subscribe((files:any)=>{
       //console.log('Uploaded');
       if(files){
        console.log('Response',files);
      }
       this.multiFile=[];
     })
  
      this.httpservice.securePost('uploadFieldWork',formData).subscribe(files=>{
        //console.log('Uploaded');
        this.multiFile=[];
      })
    
    }
    // removeAttachment(removeitem, index) {
    //   if (removeitem.new) {
    //     const fileindex = parseInt(index);
    //     this.attachedFiles.splice(fileindex, 1);
    //   } else {
    //     const fileindex = parseInt(index);
    //     let payload = {
    //       directory: "uploads/fieldWork",
    //       fullName: removeitem.fullName,
    //     };
    //     this.httpservice
    //       .securePost("removeAttachment", payload)
    //       .subscribe((response: any) => {
    //         if (response) {
    //           /*console.log(
    //             "attached files array",
    //             this.attachedFiles,
    //             "index from method",
    //             fileindex
    //           );*/
    //           this.attachedFiles.splice(fileindex, 1);
    
    //           this.fetchedData.attachment = [];
    //           this.attachedFiles.forEach((x) => {
    //             this.fetchedData.push(x.fullName);
    //           });
    //           // console.log(this.fetchedData.attachment);
    //           let payload = {
    //             actionId: this.WfFormData,
    //             fileName: removeitem.fullName,
    //           };
    //           this.httpservice
    //             .securePost(ConstUrls.deleteAIFile, payload)
    //             .subscribe((response: any) => {
    //               if (response) {
    //                 // console.log("yessss");
    //               }
    //             });
    //           setTimeout(function () {
    //             // console.log(this.attachedFiles);
    //           }, 3000);
    //           if (response) {
    //             const initialState = {
    //               title: "Success",
    //               content: "Attachment Deleted Successfully",
    //               link: "Ok",
    //             };
    //             this.modalRef = this.modalService.show(DefaultModalComponent, {
    //               initialState,
    //               class: "success-class",
    //               backdrop: "static",
    //               keyboard: false,
    //             });
    //             // this.closedAddDefinationActionForm.emit(true);
    //             // this.closeModal();
    //           }
    //         }
    //       });
    //   }
    
    //   //  /api/removeAttachment
    // }

    getDocument(file){
      if(file.new){
        const initialState = {
          title: 'Alert',
          content: 'Please save the form to view the attachment loaded.',
          link: 'Ok',
        };
        this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
       
      }
      else{
        window.open(this.uri+"uploads/fieldWork/"+file.fullName, "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400");								
    
      }
      let payload = {
        "database" : environment.db,
        "file" : "fieldWork/" + file.fullName
      
      }
    
      this.httpservice.secureJavaPostReportApi('DocumentManagementService/get',payload).subscribe((files:any)=>{
        //  console.log('Uploaded');
        if(files){
          console.log('fw Response',files);
        }
         this.multiFile=[];
       })
    
    }


  initForm() {
    this.fieldWorkForm = this.formBuilder.group({
      auditFw_id: ["", []],
      auditName: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      afw_audit_pgm_id: ["", []],
      afw_tester: ["", []],
      afw_reviewer: ["", []],
      afw_signoff_period: ["", []],
      afw_audit_pgm_title: ["", []],
      afw_test_procedure: ["", []],
      afw_status: ["", []],
      afw_results: ["", []],
      afw_attachments: ["", []],
      afw_created_by: ["", []],
      afw_updated_by: ["", []],
      status: ["", []],
      approverStatus : [],
      assigneeStatus : []
    });
  }
  onSave(){
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    let currentUser = localStorage.getItem('username');

    let afw_signoff_period = moment(this.fieldWorkForm.get('afw_signoff_period').value).format('DD/MM/YYYY')
    if (afw_signoff_period == 'Invalid date') afw_signoff_period = '-';
    //console.log('UserName: ' , currentUser);
    
    //console.log('Form Contents: ' ,this.fieldWorkForm.value)
    console.log("test data-----",
      {
        assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditFieldWork[0].assigneeStatus ,
        approverStatus : this.fapAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditFieldWork[0].approverStatus
      }
    );
    if(this.WfFormData){
      this.fieldWorkForm.patchValue({
        auditFw_id:this.WfFormData,
        'afw_created_by':currentUser,
        'afw_signoff_period':afw_signoff_period,
        status: "wip",
        assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditFieldWork[0].assigneeStatus ,
        approverStatus : this.fapAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditFieldWork[0].approverStatus
      })
      
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {         
          this.httpservice.securePost(ConstUrls.updateFWDetails , this.fieldWorkForm.value).subscribe((res:any)=>{
          if(res){
            this.adminService.sendCount(true);

           this.title='Success';
           this.content='FieldWork updated successfully ...';
           this.myCommonAlert(this.title , this.content);
       }
       this.closeModal();
     }) }
      })

    }
   
  }
 
  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to submit current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    let currentUser = localStorage.getItem("username");

    let afw_signoff_period = moment(this.fieldWorkForm.get('afw_signoff_period').value).format('DD/MM/YYYY')
    if (afw_signoff_period == 'Invalid date') afw_signoff_period = '-';
    //console.log('Form Contents: ' ,this.fieldWorkForm.value)
    console.log("submit data 2", {
      assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditFieldWork[0].assigneeStatus ,
      approverStatus : this.fAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.fapAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditFieldWork[0].approverStatus)
    });
    if (this.WfFormData) {
      this.fieldWorkForm.patchValue({
        auditFw_id: this.WfFormData,
        afw_created_by: currentUser,
        'afw_signoff_period': afw_signoff_period,
        status: "completed",
        assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditFieldWork[0].assigneeStatus ,
        approverStatus : this.fAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.fapAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditFieldWork[0].approverStatus)
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(ConstUrls.updateFWDetails, this.fieldWorkForm.value)
            .subscribe((res: any) => {
              if (res) {
              this.adminService.sendCount(true);

                this.title = "Success";
                this.content = "FieldWork submitted successfully ...";
                this.myCommonAlert(this.title, this.content);
              }
              this.closeModal();
            });
        }
      });
    }
  }
  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }

  closeModal() {
    this.closedFWForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}

//       status: ["", []],
//     });
//   }
// }
