import { Component, OnInit } from "@angular/core";
import { Chart, ChartOptions } from "chart.js";
import { AdminService } from "../../admin.service";
import * as ChartAnnotation from "chartjs-plugin-annotation";

@Component({
  selector: "app-brainstorm",
  templateUrl: "./brainstorm.component.html",
  styleUrls: ["./brainstorm.component.less"],
})
export class BrainstormComponent implements OnInit {
  riskCount: Number;
  issueCount: Number;
  controlCount: Number;
  actionCount: Number;
  doughnutChart: object;
  pieChart: object;
  pieChartCanvas: any;
  pieChartCtx: any;
  canvas: any;
  ctx: any;
  data: any;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  public editWorkStep = false;
  pdfOpen = false;
  issueForm = false;
  constructor(
    private adminService: AdminService,
  ) {}

  ngOnInit() {
    this.getCharts();
    // let reportData = {
    //   "closingMeetingDate": "-",
    //   "openingMeetingDate": "-",
    //   "updateMeetingDate": "-",
    //   "finalReport":  "-"
    // }
    // this.adminService.setDates(reportData);
  }
  getCharts() {
    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("pie");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "pie",
        data: {
          labels: ["High", "Low", "Medium"],
          datasets: [{}],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);
  }
  openWorkStep() {
    this.editWorkStep = true;
    this.pdfOpen = false;
    this.issueForm = false;
    $('#newEditStepForm').removeClass('d-none');
    $('#newWorkStepForm').addClass('d-none');
  }
  pdfView(){
    this.editWorkStep = false;
    this.pdfOpen = true;
    this.issueForm = false;
  }
  issueEditOpen(){
    this.editWorkStep = false;
    this.pdfOpen = false;
    this.issueForm = true;
  }
}
