import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import * as moment from 'moment';

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { MemoService } from "../../../internal-audit/components/field-work/memo.service";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { environment } from "src/environments/environment";
import { HttpService } from "src/app/core/http-services/http.service";
import { AdminService } from "../../admin.service";
declare var $;

@Component({
  selector: "app-admin-announcement",
  templateUrl: "./admin-announcement.component.html",
  styleUrls: ["./admin-announcement.component.less"],
})
export class AdminAnnouncementComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  @Output() closedAnnouncementForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();

  auditAnnouncementForm: FormGroup;
  position;
  modalRef: BsModalRef;
  copiedAuditData: any;
  aaId: any;
  uploadedFilesArr: any;
  fetchedData: any;
  uri = environment.baseUrl;
  multiFile: any = [];
  closeAttachment: boolean;

  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  fieldPanes: any;
  deletedAttachment: any = [];
  auditName: any;
  newAnnouncementForm: boolean;
  auditId: any;
  count: number;
  title: string;
  content: string;
  pAssignee: string[] = [];
  papAssignee : string[] = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private memo: MemoService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private adminService: AdminService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.memo.getValueann().subscribe(data=>{
      this.count=data;
      
    })
    this.fetchedData=[];

    // this.isAttachment=false;
    // this.openAttachment = true;
    
    this.initForm();
    this.title='';
    this.content='';
    this.getAnnouncementData();
    // this.auditAnnouncementForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
    this.memo.getValueann().subscribe(data=>{
      this.count=data;

      
    })
    
  }

  myCommonAlert(content ,title){
    const initialState = {title: title,content: content,link: "Ok",};
    this.modalRef = this.modalService.show(DefaultModalComponent,{initialState,class: "success-class",backdrop: "static",keyboard: false,});
  }

  getAnnouncementData(){
    this.attachedFiles = [];
    
    let payload = {auditId:this.selectAudit.auditId};
    this.httpservice.securePost(ConstUrls.getAnnouncementData , payload).subscribe(async(res:any)=>{
      this.fetchedData = res 
      this.pAssignee = res['resourePlanningData'][0]["pAssignee"];
      this.papAssignee = res['resourePlanningData'][0]["papAssignee"];
      this.fieldPanes = await res;
      console.log('res', res)
      this.initForm();
      this.auditAnnouncementForm.patchValue(res);
      if(!moment(this.fieldPanes.periodInScope[0], 'DD-MM-YYYY',true).isValid() && this.fieldPanes.periodInScope.length>0){
        let d1=moment(this.fieldPanes.periodInScope[0]).format("DD-MM-YYYY");
        let d2=moment(this.fieldPanes.periodInScope[1]).format("DD-MM-YYYY");
        let str = d1+'-'+d2;
         this.auditAnnouncementForm.patchValue({'period_in_scope':str});
      }else if(this.fieldPanes.periodInScope.length==0){
        this.auditAnnouncementForm.patchValue({'period_in_scope':this.fieldPanes.periodInScope});
      }else{
        let d1=this.fieldPanes.periodInScope[0];
        let d2=this.fieldPanes.periodInScope[1];
        let str = d1+'-'+d2;
        this.auditAnnouncementForm.patchValue({'period_in_scope':str});
      }


      if(!moment(this.fieldPanes.performancePeriod[0], 'DD-MM-YYYY',true).isValid() && this.fieldPanes.performancePeriod.length>0){
        let d1=moment(this.fieldPanes.performancePeriod[0]).format("DD-MM-YYYY");
        let d2=moment(this.fieldPanes.performancePeriod[1]).format("DD-MM-YYYY");
        let str = d1+'-'+d2;
         this.auditAnnouncementForm.patchValue({'performance_period':str});
      }else if(this.fieldPanes.performancePeriod.length==0){
        this.auditAnnouncementForm.patchValue({'performance_period':this.fieldPanes.performancePeriod});
      }else{
        let d1=this.fieldPanes.performancePeriod[0];
        let d2=this.fieldPanes.performancePeriod[1];
        let str = d1+'-'+d2;
        this.auditAnnouncementForm.patchValue({'performance_period':str});
      }

      if(!moment(this.fieldPanes.openingMeetingDate, 'DD-MM-YYYY',true).isValid() && this.fieldPanes.openingMeetingDate){
        let d1=moment(this.fieldPanes.openingMeetingDate).format("DD-MM-YYYY");
        this.auditAnnouncementForm.patchValue({'opening_meeting':d1});
      }else if(!this.fieldPanes.openingMeetingDate){
        this.auditAnnouncementForm.patchValue({'opening_meeting':this.fieldPanes.openingMeetingDate});
      }else{
        let d1=this.fieldPanes.openingMeetingDate;
        this.auditAnnouncementForm.patchValue({'opening_meeting':d1});
      }
     
             
      
      if(this.fetchedData.announcement.aa_attachments.length !=0){
        this.openAttachment = true;
        for(let i=0;i<this.fetchedData.announcement.aa_attachments.length;i++){
          let extn = this.fetchedData.announcement.aa_attachments[i].substr(this.fetchedData.announcement.aa_attachments[i].lastIndexOf('.') + 1);
          let name = this.fetchedData.announcement.aa_attachments[i].substr(0, this.fetchedData.announcement.aa_attachments[i].lastIndexOf('.')); 
          
          this.attachedFiles.push({'relativePath':name,'extension':extn,'fullName':name+'.'+extn,'new':false,'isDuplicate': this.fieldPanes.announcement.dup_aa_Flag});
        }
      }else{
        this.openAttachment = false;

      } 
      
      if(res){
        if(res.announcement.AAisSet ===true){
         this.aaId=res.announcement.aa_id;
        this.auditAnnouncementForm.patchValue(res.announcement);
        // this.auditAnnouncementForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
        this.auditName=this.fieldPanes.auditableEntity;

        // this.auditName=JSON.parse(localStorage.getItem('fieldWorkCommonData')).auditName;
        }else{
          this.title ='Error';
          this.content ='Not Allowed ...';
          this.myCommonAlert(this.title,this.content);
          this.closeModal();
        }
      }
    })
  }


  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
    } else {
      // attachment left
      this.openAttachment = true;
    }
  }
  public files: NgxFileDropEntry[] = [];
 
  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
   this.uploadedFilesArr = this.files;
    for(let ext of this.uploadedFilesArr){
      
      if(ext.relativePath){
        // console.log('relative: ' , ext.relativePath);
        var extn = ext.relativePath.substr(ext.relativePath.lastIndexOf('.') + 1);
        var name = ext.relativePath.substr(0, ext.relativePath.lastIndexOf('.')); 
        ext.relativePath= name.replace(/\W+/g, "-")+"."+extn;
          //  let extension = ext.relativePath.split('.')[1];
            ext.extension = extn;
        
        
       
          //console.log('Updated File Name:' ,ext.relativePath , 'File Extension:' , ext.extension);
       
      }
      // if(ext.relativePath){
      //   // console.log('relative: ' , ext.relativePath);
      //    let extension = ext.relativePath.split('.')[1];
      //     ext.extension = extension;
      // }
    }
     //console.log('upload files array :  ' , this.uploadedFilesArr);
    //console.log('files array: ' , this.files);
    for (const droppedFile of files) {
 
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
         

          let extn = file.name.substr(file.name.lastIndexOf('.') + 1);
          let name = file.name.substr(0, file.name.lastIndexOf('.')); 
          
          this.attachedFiles.push({'relativePath':name.replace(/\W+/g, "-")+'.'+extn,
          'extension':extn,'fullName':file.name,'new':true
      })
        console.log('ATTACHED FILES:', this.attachedFiles)
          //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }
  removeAttachment(removeitem,index)
  {
    this.closeAttachment =false;
    if (removeitem.new == true) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
      if (idx != -1) this.multiFile.splice(idx, 1);
      
    } else {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      console.log('this.attachedFiles', this.attachedFiles)
      let payload =  {directory: 'uploads/fieldWork',fullName : removeitem.fullName}
     this.httpservice.securePost('removeAttachment',payload).subscribe((response:any)=>{
        if(response)
        {
          if(response){
            this.fetchedData.announcement.aa_attachments=[];
            
              this.attachedFiles.forEach(ele => {
                this.fetchedData.announcement.aa_attachments.push(ele.fullName);
              });

            this.title="Success";
            this.content="Attachment Deleted Successfully",
            this.myCommonAlert(this.title , this.content);
          }
        }
      })
    } 
  }


  upload() {
    const formData:any= new FormData();
    const data: Array<File>= this.multiFile;
    // console.log('this is data',data);return;
    for(let i=0;i<data.length;i++){
      formData.append("file", data[i], data[i]['name']);
      if(this.fetchedData.announcement.aa_attachments){
        this.fetchedData.announcement.aa_attachments.push(data[i]['name'])
      }
      else{
        this.fetchedData.announcement.aa_attachments=[];
        this.fetchedData.announcement.aa_attachments.push(data[i]['name'])
  
      }
    }
    let arr=[]
    arr=this.fetchedData.announcement.aa_attachments;
    this.auditAnnouncementForm.patchValue({'aa_attachments':arr });
    
    let filearr = []; 
        arr.forEach(ele=> {
      filearr.push('fieldWork/'+ ele)
    });
   let payload = {
     "collection": "document",
     "module": "fieldWork",
     "sub-module": "audit-announcement",
     "tab": "audit-announcement",
     "router-link": "/rcm/internal-audit/engagement-planning",
     "file" :filearr.toString() ,
    "metadata": "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
     "username": localStorage.getItem('username'),
     "database" : environment.db
 }
 this.httpservice.secureJavaPostReportApi('DocumentManagementService/put',payload).subscribe((files:any)=>{
    
     if(files){
      console.log('Response',files);
    }
     this.multiFile=[];
   })

   if(formData){
    this.httpservice.securePost('uploadFieldWork',formData).subscribe(files=>{
      this.multiFile=[];
    })
   }
    
  
  }

  getDocument(file){
    if(file.new){
      const initialState = {
        title: 'Alert',
        content: 'Please save the form to view the attachment loaded.',
        link: 'Ok',
      };
  
      this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
    }
    else{
    window.open(this.uri+"uploads/fieldWork/"+file.fullName, "_blank", "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400");
  }
  let payload = {
    "database" : environment.db,
    "file" : "fieldWork/" + file.fullName
  
  }



  this.httpservice.secureJavaPostReportApi('DocumentManagementService/get',payload).subscribe((files:any)=>{
    //  console.log('Uploaded');
    if(files){
      console.log('audit announcement Response',files);
    }
     this.multiFile=[];
   })


  }
    public fileOver(event){
      //console.log(event);
    }
   
    public fileLeave(event){
      //console.log(event);
    }

    onSave(){

      const initialState = {
        title: "Confirmation",
        content: `Do you want to save current changes?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      this.upload();
  
      if(this.selectAudit){
        var arr=[]
  
        let currentUser = localStorage.getItem('username');
        this.auditAnnouncementForm.patchValue({'auditId':this.selectAudit.auditId ,'aa_created_by':currentUser,'aa_id':this.aaId,'dup_aa_Flag':false,status:"wip"});
        console.log('this.auditAnnouncementForm', this.auditAnnouncementForm.value);
        console.log("check",{
          assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.announcement.assigneeStatus ,
        approverStatus : this.papAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.announcement.approverStatus
        } );
        this.auditAnnouncementForm.patchValue({
          assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.announcement.assigneeStatus ,
        approverStatus : this.papAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.announcement.approverStatus
        });
        this.modalRef.content.onClose.subscribe((r) => {
          if (r) {     
            this.httpservice.securePost(ConstUrls.saveAnnouncementData , this.auditAnnouncementForm.value).subscribe((res:any)=>{
            if(res){
              this.title='Success';
              this.content='Announcement updated successfully...';
              this.myCommonAlert(this.title,this.content);
              this.adminService.sendCount(true);
               setTimeout(() => {
                     
                      var payload ={"announced":this.count+1}
                      this.httpservice.securePost(ConstUrls.updateCountData,payload).subscribe(r=>{
                        if(r){
                                    this.count= this.count+1;
                                    this.memo.setValueann(this.count);
                                  }
                             })
                     }, 1000);
            }
          })}
        })
   
      }else{
        this.title='Error';
        this.content='Failed to Save...';
        this.myCommonAlert(this.title,this.content)
      }
     
      this.closeModal();
      
    }

  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to submit Announcement?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();

    if (this.selectAudit["auditId"]) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      this.auditAnnouncementForm.patchValue({
        auditId: this.selectAudit.auditId,
        aa_created_by: currentUser,
        aa_id: this.aaId,
        status: "completed",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.announcement.assigneeStatus ,
        approverStatus : this.pAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.papAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.announcement.approverStatus)
      });
      console.log("check",{
        auditId: this.selectAudit.auditId,
        aa_created_by: currentUser,
        aa_id: this.aaId,
        status: "completed",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.announcement.assigneeStatus ,
        approverStatus : this.pAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.papAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.announcement.approverStatus)
        
      } );
     
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(
              ConstUrls.saveAnnouncementData,
              this.auditAnnouncementForm.value
            )
            .subscribe((res: any) => {
              if (res) {
                this.title = "Success";
                this.content = "Announcement Submitted successfully...";
                this.myCommonAlert(this.title, this.content);
                this.adminService.sendCount(true);

                  setTimeout(() => {
                
                    var payload = { announced: this.count + 1 };
                    this.httpservice
                      .securePost(ConstUrls.updateCountData, payload)
                      .subscribe((r) => {
                        if (r) {
                          this.count = this.count + 1;
                          this.memo.setValueann(this.count);
                        }
                      });
                  }, 1000);
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }
    this.closeModal();
  }
  initForm() {
    this.auditAnnouncementForm = this.formBuilder.group({
      _id: ["", []],
      auditId: ["", []],
      auditName: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      aa_id: ["", []],
      aa_name: ["", []],
      aa_text: ["", []],
      aa_agenda: ["", []],
      aa_text2: ["", []],
      aa_closing: ["", []],
      aa_attachments: ["", []],
      aa_created_by: ["", []],
      aa_updated_by: ["", []],
      AAisSet: [{ value: true }, []],
      status: ["", []],
      assigneeStatus : [],
      approverStatus : []
    });
  }

  closeModal() {
    this.closedAnnouncementForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
