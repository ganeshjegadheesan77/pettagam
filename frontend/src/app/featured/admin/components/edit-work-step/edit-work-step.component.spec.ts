import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWorkStepComponent } from './edit-work-step.component';

describe('EditWorkStepComponent', () => {
  let component: EditWorkStepComponent;
  let fixture: ComponentFixture<EditWorkStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditWorkStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditWorkStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
