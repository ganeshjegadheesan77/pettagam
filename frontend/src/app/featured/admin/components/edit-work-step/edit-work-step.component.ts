import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { ConstUrls } from "src/app/config/const-urls";
import { HttpService } from "../../../../core/http-services/http.service";
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmRejectWorkstepComponent } from "src/app/layout/modal/confirm-reject-workstep/confirm-reject-workstep.component";
declare var $;
@Component({
  selector: 'app-edit-work-step',
  templateUrl: './edit-work-step.component.html',
  styleUrls: ['./edit-work-step.component.less']
})
export class EditWorkStepComponent implements OnInit {
  modalRef: BsModalRef;
  uploadedFilesArr: any;
  multiFile: any = [];
  attachedFiles: any = [];
  dropdownFirmList = [];
  globalSearchData = "";
  dropdownList = [];
  selectedItems = [];

  position;
  dropdownSettings = {};
  dropdownListAll = [];
  workPaperNumber: "";
  auditId: any;
  disableSearch = false;

  auditName = "";
  arr = [];
  editsearchDocument: boolean = false;

  //input varible for pdf annotation
  //start
  documentNumber : string;
  documentUrl : string;
  doucmentName : string;
  workpaperNum : string;
  documentAuditId : string;
  public renameDoc = false;
  //end
  previewMode = false;
  constructor( public httpService: HttpService,private mScrollbarService: MalihuScrollbarService, private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService, private chRef: ChangeDetectorRef,
    private modalService: BsModalService) { }

  ngOnInit() {
    // this.previewMode = true;
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'documentNo',
      textField: 'fileName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.issueActiondatatable()
    this.issueActionSelecteddatatable();
  }
  onAnnotationComponenetClose(flag : boolean){
    if(flag) {
      this.previewMode = false;
    }
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '180px',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',

      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        
        'redo',
        
        
        
        'strikeThrough',
        'subscript',
        'superscript',
        
        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
    'textColor',
    'backgroundColor',
    'customClasses',
    'link',
    'unlink',
    'insertImage',
    'insertVideo',
    'insertHorizontalRule',
    'removeFormat',
    'toggleEditorMode'
      ]
    ]
};
addnewFile() {
  this.editsearchDocument = true;
}
getSelectedDocs() {
  this.editsearchDocument = false;
}
closeSearch() {
  this.editsearchDocument = false;
}
globalSearchKeyPress() {
  console.log("hii");
  this.dropdownListAll = [];
  if (this.globalSearchData.length <= 2) {
    this.dropdownListAll = [];
    this.dropdownFirmList = [];
    // this.dropdownList=[];

    let payload = { searchQuery: this.globalSearchData ,auditId:this.auditId ,moduleName:'In-Tray' ,dmsSessionId:localStorage.getItem('dmsToken')};
    this.httpService
      .securePost(ConstUrls.DmsFileSearch, payload)
      .subscribe((records: any) => {
        console.log("records", records);
        if (records) {
          this.arr = [];
          records.forEach((d) => {
            this.arr.push({ documentNo: d.documentNo, fileName: d.fileName });
          });
          this.dropdownListAll = this.arr;
          this.dropdownFirmList = this.arr;
          // this.dropdownList =this.arr;
        } else {
          this.dropdownListAll = [];
          this.dropdownFirmList = [];
          // this.dropdownList=[];
        }
      });
  } else {
    this.dropdownListAll = [];
    this.dropdownFirmList = [];
  }

}
closeModal() {
  $(".scroll-container").scrollTop(0);
  $('.pop-up-form').addClass('d-none');
  $(".animation-save").removeClass('active-btn');
  $(".animation-submit").removeClass('active-btn');
  this.editsearchDocument = false;
  this.position = this.position ? undefined : {x: 0, y: 0};
  this.renameDoc = false;
 }
public files: NgxFileDropEntry[] = [];

public dropped(files: NgxFileDropEntry[]) {
  this.files = files;
  this.uploadedFilesArr = this.files;
  for (let ext of this.uploadedFilesArr) {

    if (ext.relativePath) {
      var extn = ext.relativePath.substr(ext.relativePath.lastIndexOf('.') + 1);
      var name = ext.relativePath.substr(0, ext.relativePath.lastIndexOf('.'));
      ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
      ext.extension = extn;
   }
  }
  for (const droppedFile of files) {
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        this.multiFile.push(file)
        let extn = file.name.substr(file.name.lastIndexOf('.') + 1);
        let name = file.name.substr(0, file.name.lastIndexOf('.'));

        this.attachedFiles.push({
          'relativePath': name.replace(/\W+/g, "-") + '.' + extn,
          'extension': extn, 'fullName': file.name, 'new': true
        })
      });
    } else {
      const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
    }
  }
}

issueActiondatatable() {

  this.chRef.detectChanges();
  const issueTable: any = $("#issueActionTable");
  const issueListTableCM = this.dataTableService.initializeTable(
    issueTable,
    `issueActionAgile`
  );
  this.dataTableFuncService.selectAllRows(
    issueListTableCM,
    `issueAction-select-all`
  );
  this.dataTableFuncService.expandContainer(issueListTableCM);
  $("#issueActionTable tbody").on(
    "change",
    'input[type="checkbox"]',
    function () {
      if (!this.checked) {
        $("#issueAction-select-all").prop("checked", false);
        var el = $("#issueAction-select-all").get(0);
        // if (el && el.checked && "indeterminate" in el) {
        //   el.indeterminate = true;
        // }
      }
      if (
        $("#issueActionTable tr td input[type='checkbox']:checked").length ==
        $("#issueActionTable tr td input[type='checkbox']").length
      ) {
        $("#issueAction-select-all").prop("checked", true);
      }
    }
  );

}
issueActionSelecteddatatable() {
  this.chRef.detectChanges();
  const issueSelectedTable: any = $("#issueActionSelectedTable");
  const issueListSelectedTableCM = this.dataTableService.initializeTable(
    issueSelectedTable,
    `issueActionSelectedAgile`
  );
  this.dataTableFuncService.selectAllRows(
    issueListSelectedTableCM,
    `issueActionSelected-select-all`
  );
  this.dataTableFuncService.expandContainer(issueListSelectedTableCM);
  $("#issueActionSelectedTable tbody").on(
    "change",
    'input[type="checkbox"]',
    function () {
      if (!this.checked) {
        $("#issueActionSelected-select-all").prop("checked", false);
        var el = $("#issueActionSelected-select-all").get(0);
        // if (el && el.checked && "indeterminate" in el) {
        //   el.indeterminate = true;
        // }
      }
      if (
        $("#issueActionSelectedTable tr td input[type='checkbox']:checked").length ==
        $("#issueActionSelectedTable tr td input[type='checkbox']").length
      ) {
        $("#issueActionSelected-select-all").prop("checked", true);
      }
    }
  );

}
rejectBtn(){
  const initialState = {
    title: "Confirm Reject WorkStep",
    content: `WS-102`,
    link: "Yes, Reject the workstep",
    action: "Cancel",
    confirmFlag: false,
  };
  this.modalRef = this.modalService.show(ConfirmRejectWorkstepComponent, {
    initialState,
    class: "error-popup-design extra-popup",
    backdrop: "static",
    keyboard: false,
  });
}
saveAnimation(){
  $(".animation-save").addClass('active-btn');
}
submitAnimation(){
  $(".animation-submit").addClass('active-btn');
}

renameDocument() {
  this.renameDoc = true;
}
closeRenameDoc() {
  this.renameDoc = false;
}

}
