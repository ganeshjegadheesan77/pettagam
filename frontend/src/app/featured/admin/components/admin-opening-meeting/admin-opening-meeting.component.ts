import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as moment from "moment";

import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { InterviewSchedulePopupComponent } from "src/app/layout/modal/interview-schedule-popup/interview-schedule-popup.component";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { MemoService } from "../../../internal-audit/components/field-work/memo.service";
import { AdminService } from "../../admin.service";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

declare var $;

@Component({
  selector: "app-admin-opening-meeting",
  templateUrl: "./admin-opening-meeting.component.html",
  styleUrls: ["./admin-opening-meeting.component.less"],
})
export class AdminOpeningMeetingComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  @Output() closedOpenMeetForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  openMeetUpForm: FormGroup;
  modalRef: BsModalRef;
  // OMisSet:Boolean;
  copiedAuditData: any;
  aomId: any;
  content: string;
  title: string;
  auditName: string;
  newMeetForm: boolean;
  auditId: any;
  interviewList: any;
  mixArr: any;
  auditTeam: any[];

  aomInterview: any;
  count: number;
  position;
  fieldPanes: any;
  pAssignee: string[] = [];
  papAssignee : string[] = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private memo: MemoService,
    private adminService: AdminService,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.getMeetingData();

    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.memo.getValueom().subscribe((data) => {
      this.count = data;
    });
    // this.getCopiedData();
    // var data= JSON.parse(localStorage.getItem('fieldWorkCommonData'));
    //  this.openMeetUpForm.patchValue(data);
    this.httpservice
      .secureGet(ConstUrls.getAllInterview)
      .subscribe(async (res) => {
        if (res) {
          this.interviewList = await res;

          setTimeout(() => {
            this.dataTable();
          }, 500);
        }
      });
  }

  myCommonAlert(content, title) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  getMeetingData() {
    let payload = { auditId: this.selectAudit.auditId };
    this.httpservice
      .securePost(ConstUrls.getOpenMeetUpData, payload)
      .subscribe((res: any) => {
        if (res) {
          console.log("res", res);
            this.pAssignee = res[0]['resourePlanningData'][0]["pAssignee"];
            this.papAssignee = res[0]['resourePlanningData'][0]["papAssignee"];
          if (res[0].openingMeeting.OMisSet === true) {
            this.fieldPanes = res[0];
            if (
              !moment(
                this.fieldPanes.periodInScope[0],
                "DD-MM-YYYY",
                true
              ).isValid() &&
              this.fieldPanes.periodInScope.length > 0
            ) {
              let d1 = moment(this.fieldPanes.periodInScope[0]).format(
                "DD-MM-YYYY"
              );
              let d2 = moment(this.fieldPanes.periodInScope[1]).format(
                "DD-MM-YYYY"
              );
              let str = d1 + "-" + d2;
              this.openMeetUpForm.patchValue({ period_in_scope: str });
            } else if (this.fieldPanes.periodInScope.length == 0) {
              this.openMeetUpForm.patchValue({
                period_in_scope: this.fieldPanes.periodInScope,
              });
            } else {
              let d1 = this.fieldPanes.periodInScope[0];
              let d2 = this.fieldPanes.periodInScope[1];
              let str = d1 + "-" + d2;
              this.openMeetUpForm.patchValue({ period_in_scope: str });
            }

            if (
              !moment(
                this.fieldPanes.performancePeriod[0],
                "DD-MM-YYYY",
                true
              ).isValid() &&
              this.fieldPanes.performancePeriod.length > 0
            ) {
              let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
                "DD-MM-YYYY"
              );
              let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
                "DD-MM-YYYY"
              );
              let str = d1 + "-" + d2;
              this.openMeetUpForm.patchValue({ performance_period: str });
            } else if (this.fieldPanes.performancePeriod.length == 0) {
              this.openMeetUpForm.patchValue({
                performance_period: this.fieldPanes.performancePeriod,
              });
            } else {
              let d1 = this.fieldPanes.performancePeriod[0];
              let d2 = this.fieldPanes.performancePeriod[1];
              let str = d1 + "-" + d2;
              this.openMeetUpForm.patchValue({ performance_period: str });
            }

            if (
              !moment(
                this.fieldPanes.openingMeetingDate,
                "DD-MM-YYYY",
                true
              ).isValid() &&
              this.fieldPanes.openingMeetingDate
            ) {
              let d1 = moment(this.fieldPanes.openingMeetingDate).format(
                "DD-MM-YYYY"
              );
              this.openMeetUpForm.patchValue({ opening_meeting: d1 });
            } else if (!this.fieldPanes.openingMeetingDate) {
              this.openMeetUpForm.patchValue({
                opening_meeting: this.fieldPanes.openingMeetingDate,
              });
            } else {
              let d1 = this.fieldPanes.openingMeetingDate;
              this.openMeetUpForm.patchValue({ opening_meeting: d1 });
            }

            if (
              !moment(
                this.fieldPanes.closingMeetingDate,
                "DD-MM-YYYY",
                true
              ).isValid() &&
              this.fieldPanes.closingMeetingDate
            ) {
              let d1 = moment(this.fieldPanes.closingMeetingDate).format(
                "DD-MM-YYYY"
              );
              this.openMeetUpForm.patchValue({ closing_meeting: d1 });
            } else if (!this.fieldPanes.closingMeetingDate) {
              this.openMeetUpForm.patchValue({
                closing_meeting: this.fieldPanes.closingMeetingDate,
              });
            } else {
              let d1 = this.fieldPanes.closingMeetingDate;
              this.openMeetUpForm.patchValue({ closing_meeting: d1 });
            }

            this.auditName = this.fieldPanes.auditableEntity;

            this.aomId = res[0].openingMeeting.aom_id;
            this.openMeetUpForm.patchValue(res[0].openingMeeting);
            // this.openMeetUpForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
            //Audit team code
            this.auditTeam = [];
            this.mixArr = res[0].resourePlanningData;
            console.log("mixArr", this.mixArr);
            let newArr = [];
            if (this.mixArr[0]) {
              this.mixArr[0].pAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              this.mixArr[0].papAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              let newArray = newArr.filter(
                (v, i, a) => a.findIndex((t) => t === v) === i
              );
              newArray.forEach((element) => {
                this.auditTeam.push(element);
              });
            }
          } else {
            this.title = "Error";
            this.content = "Not Allowed ...";
            this.myCommonAlert(this.title, this.content);
            this.closeModal();
          }
        }
      });
  }

  onSave() {
    if (this.selectAudit) {
      console.log("selectAudit", this.selectAudit);
      let currentUser = localStorage.getItem("username");
      console.log("patch value---->", {
        auditId: this.selectAudit.auditId,
        aom_created_by: currentUser,
        aom_id: this.aomId,
        status: "wip",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.openingMeeting.assigneeStatus ,
        approverStatus : this.papAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.openingMeeting.approverStatus
      });
      this.openMeetUpForm.patchValue({
        auditId: this.selectAudit.auditId,
        aom_created_by: currentUser,
        aom_id: this.aomId,
        status: "wip",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.openingMeeting.assigneeStatus ,
        approverStatus : this.papAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.openingMeeting.approverStatus
      });
      console.log("check");
      this.httpservice
        .securePost(ConstUrls.saveOpenMeetUptData, this.openMeetUpForm.value)
        .subscribe((res: any) => {
          if (res) {
            this.title = "Success";
            this.content = "OpeningMeeting updated successfully...";
            this.myCommonAlert(this.title, this.content);
            this.adminService.sendCount(true);
            
            
     
              setTimeout(() => {
            
                var payload = { openingMeeting: this.count + 1 };
                this.httpservice
                  .securePost(ConstUrls.updateCountData, payload)
                  .subscribe((r) => {
                    if (r) {
                      this.count = this.count + 1;
                      this.memo.setValueann(this.count);
                    }
                  });
              }, 1000);
          }
        });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }
  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to submit Opening Meeting?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    
    if (this.selectAudit["auditId"]) {
      console.log("selectAudit", this.selectAudit["auditId"]);
      let currentUser = localStorage.getItem("username");
      console.log("check",{
        auditId: this.selectAudit.auditId,
        aom_created_by: currentUser,
        aom_id: this.aomId,
        status: "completed",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.openingMeeting.assigneeStatus ,
        approverStatus : this.pAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.papAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.openingMeeting.approverStatus)
      } );
      this.openMeetUpForm.patchValue({
        auditId: this.selectAudit.auditId,
        aom_created_by: currentUser,
        aom_id: this.aomId,
        status: "completed",
        assigneeStatus : this.pAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.openingMeeting.assigneeStatus ,
        approverStatus : this.pAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.papAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.openingMeeting.approverStatus)
      });
      
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(
              ConstUrls.saveOpenMeetUptData,
              this.openMeetUpForm.value
            )
            .subscribe((res: any) => {
              if (res) {
                this.title = "Success";
                this.content = "OpeningMeeting Submitted successfully...";
                this.myCommonAlert(this.title, this.content);
                this.adminService.sendCount(true);
                // console.log("res", res);
     
                  setTimeout(() => {
                 
                    var payload = { openingMeeting: this.count + 1 };
                    this.httpservice
                      .securePost(ConstUrls.updateCountData, payload)
                      .subscribe((r) => {
                        if (r) {
                          this.count = this.count + 1;
                          this.memo.setValueann(this.count);
                        }
                      });
                  }, 1000);
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }
  // onSave(){
  //   let currentUser = localStorage.getItem('username');
  //   this.openMeetUpForm.patchValue({'aom_created_by':currentUser,'master_audit_id':'OP005'})
  //   this.httpservice.securePost(ConstUrls.saveAuditOpeningMeeting , this.openMeetUpForm.value).subscribe(
  //     (res:any)=>{
  //      if(res){
  //       const initialState = {
  //         title: 'Success',
  //         content: 'Audit Opening Meeting Saved',
  //         link: 'Ok',
  //       };
  //       this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //       }else{
  //         const initialState = {
  //           title: 'Fail',
  //           content: 'Failed to save opening meeting',
  //           link: 'Ok',
  //         };
  //       this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //       }
  //       this.closeModal();
  //     }
  //   )

  // }

  initForm() {
    this.openMeetUpForm = this.formBuilder.group({
      _id: ["", []],
      auditName: ["", []],
      auditId: ["", []],
      status: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      aom_id: ["", []],
      aom_name: ["", []],
      aom_scope: ["", []],
      aom_agenda: ["", []],
      aom_qa: ["", []],
      aom_audit_team: ["", []],
      aom_interview_schedule: ["", []],
      aom_created_by: ["", []],
      aom_updated_by: ["", []],
      OMisSet: [{ value: true }, []],
      assigneeStatus : [],
      approverStatus : []
    });
  }
  dataTable() {
    $("#interveiwSchduleTable").DataTable({
      processing: true,
      bPaginate: true,
      autoWidth: true,
      searching: false,
      destroy: true,
      fixedColumns: {
        leftColumns: 0,
        rightColumns: 1,
        heightMatch: "auto",
      },
      columnDefs: [{ targets: 6, orderable: false }],
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"],
      ],
      bInfo: true,
    });
  }

  closeModal() {
    this.closedOpenMeetForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  // editTable(a,i){
  //   $('body').addClass('modalPopupOne');
  //   const initialState = {
  //     title: 'Interview Schedule',
  //     link: 'Submit',
  //     action: 'Cancel',
  //     data:a,
  //   };
  //   this.modalRef = this.modalService.show(InterviewSchedulePopupComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
  //   this.modalRef.content.onClose.subscribe(r=>{
  //     if(r){
  //       //console.log(r);
  //      this.interviewList[i]= r;
  //     }
  //   })
  // }
  // duplicateRecord(a,i){
  //   a.aom_id= this.aomId;
  //   delete a._id
  //   a.isId=   "IS" + String(Math.floor(Math.random() * 100) + 1);
  //   this.httpservice.securePost(ConstUrls.saveInterview, a).subscribe(x=>{
  //     if(x){
  //       const initialState = {
  //         title: 'Success',
  //         content: 'Record is duplicated successully!',
  //         link: 'Ok',
  //       };
  //     this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
  //       this.interviewList.push(a);

  //     }
  //   })
  // }
  // deleteRecord(a,i){
  //   if(i>-1){
  //     this.httpservice.securePost(ConstUrls.deleteInterview,{aom_id:this.aomId,aom_interview_schedule:a.isId}).subscribe(x=>{
  //       if(x){
  //         const initialState = {
  //           title: 'Success',
  //           content: 'Record deleted successully!',
  //           link: 'Ok',
  //         };
  //       this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});

  //         this.interviewList.splice(i,1)
  //       }
  //     })

  //   }
  // }
}
