import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOpeningMeetingComponent } from './admin-opening-meeting.component';

describe('AdminOpeningMeetingComponent', () => {
  let component: AdminOpeningMeetingComponent;
  let fixture: ComponentFixture<AdminOpeningMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOpeningMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOpeningMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
