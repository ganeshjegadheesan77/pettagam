import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminClosingMeetingComponent } from './admin-closing-meeting.component';

describe('AdminClosingMeetingComponent', () => {
  let component: AdminClosingMeetingComponent;
  let fixture: ComponentFixture<AdminClosingMeetingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminClosingMeetingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminClosingMeetingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
