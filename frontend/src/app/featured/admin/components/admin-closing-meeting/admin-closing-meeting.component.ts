import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import * as moment from "moment";

import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { HttpService } from "src/app/core/http-services/http.service";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { MemoService } from "../../../internal-audit/components/field-work/memo.service";
import { environment } from "src/environments/environment";
import { AdminService } from "../../admin.service";

declare var $;

@Component({
  selector: "app-admin-closing-meeting",
  templateUrl: "./admin-closing-meeting.component.html",
  styleUrls: ["./admin-closing-meeting.component.less"],
})
export class AdminClosingMeetingComponent implements OnInit {
  @Input("selectAudit") selectAudit: any;
  // @Input("issueArr") issueArr: any;
  @Output() closedCMForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  closingMeetingForm: FormGroup;
  modalRef: BsModalRef;
  position;
  CMId: any;
  content: string;
  auditName: any;
  auditId: any;
  aaId: any;
  uploadedFilesArr: any;
  fetchedData: any;
  uri = environment.baseUrl;
  multiFile: any = [];
  attachedFiles: any = [];
  marked: boolean;
  openAttachment: boolean;
  isAttachment: boolean;
  fieldPanes: any;
  closeAttachment: boolean;
  mixArr: any;
  auditTeam: any[];
  deletedAttachment: any = [];
  isPdf = false;
  notPdf = true;
  newId: number;
  currAuditId: any;
  acmId: any;
  issueTable: any;
  newClosingMeeting: boolean;
  assignedActions: any = [];
  actionList: any = [];
  selectReport: any;
  pdfSrc: string;
  updateCount: any;
  count: number;
  title: string;
  issueArr: any = [];
  assignedIssues = [];
  rAssignee: string[] = [];
  rapAssignee : string[] = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private memo: MemoService,
    private adminService: AdminService,
    private mScrollbarService: MalihuScrollbarService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.fetchedData = [];
    this.initForm();
    this.memo.getValueann().subscribe((data) => {
      this.count = data;
    });

    // this.isAttachment = false;
    // this.openAttachment = true;
    this.acmId = "";
    this.title = "";
    this.content = "";
    this.getClosingMeetingData();
    // this.closingMeetingForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
    this.memo.getValueann().subscribe((data) => {
      this.count = data;
    });
    // console.log('Closing meeting form OPENED..')
    // this.getClosingMeetingData();

    // this.auditId = JSON.parse(localStorage.getItem("auditData"))["newAuditId"][
    //   "auditId"
    // ];
  }

  myCommonAlert(content, title) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }
  getClosingMeetingData() {
    this.attachedFiles = [];
    let payload = { auditId: this.selectAudit.auditId };
    console.log("selectAudit", this.auditId);
    this.httpservice
      .securePost(ConstUrls.getClosingMeetingData, payload)
      .subscribe(async (response: any) => {
        console.log("response", response);
        if(response.length) {
          this.rAssignee = response[0]['resourePlanningData'][0]["rAssignee"];
          this.rapAssignee = response[0]['resourePlanningData'][0]["rapAssignee"];
          this.assignedIssues = response[0].issueData;
          this.assignedActions = response[0].actionData;
        }
        this.fieldPanes = await response[0];
        //  this.initForm();

        this.closingMeetingForm.patchValue(response[0]);
        if (
          !moment(
            this.fieldPanes.periodInScope[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.periodInScope.length > 0
        ) {
          let d1 = moment(this.fieldPanes.periodInScope[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.periodInScope[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ period_in_scope: str });
        } else if (this.fieldPanes.periodInScope.length == 0) {
          this.closingMeetingForm.patchValue({
            period_in_scope: this.fieldPanes.periodInScope,
          });
        } else {
          let d1 = this.fieldPanes.periodInScope[0];
          let d2 = this.fieldPanes.periodInScope[1];
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ period_in_scope: str });
        }

        if (
          !moment(
            this.fieldPanes.performancePeriod[0],
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.performancePeriod.length > 0
        ) {
          let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
            "DD-MM-YYYY"
          );
          let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
            "DD-MM-YYYY"
          );
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ performance_period: str });
        } else if (this.fieldPanes.performancePeriod.length == 0) {
          this.closingMeetingForm.patchValue({
            performance_period: this.fieldPanes.performancePeriod,
          });
        } else {
          let d1 = this.fieldPanes.performancePeriod[0];
          let d2 = this.fieldPanes.performancePeriod[1];
          let str = d1 + "-" + d2;
          this.closingMeetingForm.patchValue({ performance_period: str });
        }

        if (
          !moment(
            this.fieldPanes.openingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.openingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.openingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ opening_meeting: d1 });
        } else if (!this.fieldPanes.openingMeetingDate) {
          this.closingMeetingForm.patchValue({
            opening_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.openingMeetingDate;
          this.closingMeetingForm.patchValue({ opening_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.closingMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.closingMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.closingMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ closing_meeting: d1 });
        } else if (!this.fieldPanes.closingMeetingDate) {
          this.closingMeetingForm.patchValue({
            closing_meeting: this.fieldPanes.openingMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.closingMeetingDate;
          this.closingMeetingForm.patchValue({ closing_meeting: d1 });
        }

        if (
          !moment(
            this.fieldPanes.updateMeetingDate,
            "DD-MM-YYYY",
            true
          ).isValid() &&
          this.fieldPanes.updateMeetingDate
        ) {
          let d1 = moment(this.fieldPanes.updateMeetingDate).format(
            "DD-MM-YYYY"
          );
          this.closingMeetingForm.patchValue({ update_meeting: d1 });
        } else if (!this.fieldPanes.updateMeetingDate) {
          this.closingMeetingForm.patchValue({
            update_meeting: this.fieldPanes.updateMeetingDate,
          });
        } else {
          let d1 = this.fieldPanes.updateMeetingDate;
          this.closingMeetingForm.patchValue({ update_meeting: d1 });
        }
        this.auditName = this.fieldPanes.auditableEntity;

        // console.log('closingMeetingForm', this.closingMeetingForm)
        this.fetchedData = response[0];

        if (this.fetchedData.closingMeeting.acm_attachments.length != 0) {
          this.openAttachment = true;
          for (
            let i = 0;
            i < this.fetchedData.closingMeeting.acm_attachments.length;
            i++
          ) {
            let extn = this.fetchedData.closingMeeting.acm_attachments[
              i
            ].substr(
              this.fetchedData.closingMeeting.acm_attachments[i].lastIndexOf(
                "."
              ) + 1
            );
            let name = this.fetchedData.closingMeeting.acm_attachments[
              i
            ].substr(
              0,
              this.fetchedData.closingMeeting.acm_attachments[i].lastIndexOf(
                "."
              )
            );

            this.attachedFiles.push({
              relativePath: name,
              extension: extn,
              fullName: name + "." + extn,
              new: false,
            });
          }
        } else {
          this.openAttachment = false;
        }

        // if (this.fieldPanes.closingMeeting.acm_attachments) {

        //   for (let i = 0; i < this.fieldPanes.closingMeeting.acm_attachments.length; i++) {
        //     this.attachedFiles.push({
        //       'relativePath': this.fieldPanes.closingMeeting.acm_attachments[i].split('.')[0],
        //       'extension': this.fieldPanes.closingMeeting.acm_attachments[i].split('.')[1], 'fullName': this.fieldPanes.closingMeeting.acm_attachments[i]

        //     })
        //   }
        // }
        if (response) {
          if (response[0].closingMeeting.CMisSet === true) {
            this.acmId = response[0].closingMeeting.acm_id;
            this.closingMeetingForm.patchValue(response[0].closingMeeting);
            //Audit team code
            this.auditTeam = [];
            this.mixArr = response[0].resourePlanningData;
            console.log("mixArr", this.mixArr);
            let newArr = [];
            if (this.mixArr[0]) {
              this.mixArr[0].rAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              this.mixArr[0].rapAssignee.forEach((ele) => {
                newArr.push(ele);
              });
              let newArray = newArr.filter(
                (v, i, a) => a.findIndex((t) => t === v) === i
              );
              newArray.forEach((element) => {
                this.auditTeam.push(element);
              });
            }
          } else {
            this.title = "Error";
            this.content = "Not Allowed ...";
            this.myCommonAlert(this.title, this.content);
            this.closeModal();
          }
        }
      });
  }

  getFieldWorkCommonData() {
    if (JSON.parse(localStorage.getItem("fieldWorkCommonData"))) {
      this.closingMeetingForm.patchValue(
        JSON.parse(localStorage.getItem("fieldWorkCommonData"))
      );
    }
  }

  // getLastId() {
  //   this.CMId = "";
  //   let getlastAudit = { prefix: "CM" };
  //   this.httpservice
  //     .securePost(ConstUrls.getLastId, getlastAudit)
  //     .subscribe((res: any) => {
  //       // NEW AUDIT GENERATE
  //       if (res) {
  //         this.newId = parseInt(res["lastId"], 10) + 1;
  //         this.CMId = "CM" + this.newId;
  //         //console.log("Genearated CM ID: ", this.CMId);
  //       }
  //     });
  // }

  /////////////////////// java api for report ////////////////
  PrintClick() {
    this.selectAudit;
    let payload = {
      collection: "audit_closing_meeting",
      params: [
        {
          name: "acmId",
          value: this.acmId,
        },
        {
          name: "auditId",
          value: this.selectAudit,
        },
      ],
    };
    //console.log('hey',payload)
    this.httpservice
      .secureJavaPostReportApi(
        "GenericReportBuilderService/buildReport",
        payload
      )
      .subscribe((res: any) => {
         this.pdfSrc = environment.baseUrl + 'reports/' + res.report;

        // this.pdfSrc = "http://115.112.185.58:3000/api/reports/" + res.report;
        // window.open(this.pdfSrc,"_blank")
        //console.log('this.pdfSrc', this.pdfSrc)
        this.isPdf = true;
        this.notPdf = false;

        if (res) {
          this.selectReport = res.report;
        }
      });
  }

  back() {
    this.isPdf = false;
    this.notPdf = true;
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;

      //console.log('left curr status:' , this.marked)
    } else {
      // attachment left

      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        //  let extension = ext.relativePath.split('.')[1];
        ext.extension = extn;

        //console.log('Updated File Name:' ,ext.relativePath , 'File Extension:' , ext.extension);
      }
      // if(ext.relativePath){
      //   // console.log('relative: ' , ext.relativePath);
      //    let extension = ext.relativePath.split('.')[1];
      //     ext.extension = extension;
      // }
    }
    //console.log('upload files array :  ' , this.uploadedFilesArr);
    //console.log('files array: ' , this.files);
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
          //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  removeAttachment(removeitem, index) {
    this.closeAttachment = false;
    if (removeitem.new == true) {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1);
      let idx = this.multiFile.findIndex((r) => r.name == removeitem.fullName);
      if (idx != -1) this.multiFile.splice(idx, 1);
    } else {
      const fileindex = parseInt(index);
      this.attachedFiles.splice(fileindex, 1),index;
      console.log("this.attachedFiles", this.attachedFiles);
      let payload = {
        directory: "uploads/fieldWork",
        fullName: removeitem.fullName,
      };
      this.httpservice
        .securePost("removeAttachment", payload)
        .subscribe((response: any) => {
          if (response) {
            if (response) {
              this.fetchedData.closingMeeting.acm_attachments = [];
              this.attachedFiles.forEach((ele) => {
                this.fetchedData.closingMeeting.acm_attachments.push(
                  ele.fullName
                );
              });

              this.title = "Success";
              (this.content = "Attachment Deleted Successfully"),
                this.myCommonAlert(this.title, this.content);
            }
          }
        });
    }
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    // console.log('this is data',data.length)
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.closingMeeting.acm_attachments) {
        this.fetchedData.closingMeeting.acm_attachments.push(data[i]["name"]);
      } else {
        this.fetchedData.closingMeeting.acm_attachments = [];
        this.fetchedData.closingMeeting.acm_attachments.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.closingMeeting.acm_attachments;
    //console.log('attchment array',this.fetchedData)
    this.closingMeetingForm.patchValue({ acm_attachments: arr });
    // let filearr = this.actionForm.get('attachment').value;
    console.log("filearray", arr.toString());

    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "closing-meeting",
      tab: "closing-meeting",
      "router-link": "/rcm/internal-audit/engagement-planning",

      file: filearr.toString(),
      // "file" : "multipleuploadfilecase/this.actionForm.get('attachment').value",

      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: environment.db,
    };

    console.log("check", payload);
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        //console.log('Uploaded');
        if (files) {
          console.log("Response", files);
        }
        this.multiFile = [];
      });

    if (formData) {
      this.httpservice
        .securePost("uploadFieldWork", formData)
        .subscribe((files) => {
          this.multiFile = [];
        });
    }
  }
  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };

      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
    let payload = {
      database: environment.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        //  console.log('Uploaded');
        if (files) {
          console.log("closing Response", files);
        }
        this.multiFile = [];
      });
  }
  public fileOver(event) {
    //console.log(event);
  }

  public fileLeave(event) {
    //console.log(event);
  }

  initForm() {
    this.closingMeetingForm = this.formBuilder.group({
      auditName: ["", []],
      closing_meeting: [{ value: "", disabled: true }, []],
      opening_meeting: [{ value: "", disabled: true }, []],
      update_meeting: [{ value: "", disabled: true }, []],
      year: ["", []],
      period_in_scope: [{ value: "", disabled: true }, []],
      performance_period: [{ value: "", disabled: true }, []],
      master_audit_id: ["", []],
      acm_id: ["", []],
      acm_name: ["", []],
      acm_agenda: ["", []],
      acm_issue_action_review: ["", []],
      acm_qa: ["", []],
      auditId: ["", []],
      acm_audit_team: ["", []],
      acm_comment: ["", []],
      acm_attachments: ["", []],
      acm_created_by: ["", []],
      acm_updated_by: ["", []],
      CMisSet: [{ value: true }, []],
      status: ["", []],
      assigneeStatus : [],
      approverStatus : ['']
    });
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    if (this.selectAudit) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      this.closingMeetingForm.patchValue({
        auditId: this.selectAudit.auditId,
        acm_created_by: currentUser,
        acm_id: this.acmId,
        dup_acm_Flag: false,
        status: "wip",
        assigneeStatus : this.rAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.closingMeeting.assigneeStatus ,
        approverStatus : this.rapAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.closingMeeting.approverStatus
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(
              ConstUrls.saveCloseMeetData,
              this.closingMeetingForm.value
            )
            .subscribe((res: any) => {
              if (res) {
                
                 
                
                this.title = "Success";
                this.content = "CloseMeeting updated successfully...";
                this.myCommonAlert(this.title, this.content);
                this.adminService.sendCount(true);
                setTimeout(() => {
                  var payload = { closeMeeting: this.count + 1 };
                this.httpservice
                  .securePost(ConstUrls.updateCountData, payload)
                  .subscribe((r) => {
                    if (r) {
                      this.count = this.count + 1;
                      this.memo.setValueann(this.count);
                    }
                  });
                }, 1000);
                
                  
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }

  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to submit Closing Meeting?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();

    if (this.selectAudit["auditId"]) {
      var arr = [];

      let currentUser = localStorage.getItem("username");
      this.closingMeetingForm.patchValue({
        auditId: this.selectAudit.auditId,
        acm_created_by: currentUser,
        acm_id: this.acmId,
        status: "completed",
        assigneeStatus : this.rAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.closingMeeting.assigneeStatus ,
        approverStatus : this.rAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.rapAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.closingMeeting.approverStatus)
      });

      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {
          this.httpservice
            .securePost(
              ConstUrls.saveCloseMeetData,
              this.closingMeetingForm.value
            )
            .subscribe((res: any) => {
              if (res) {
                
                this.title = "Success";
                this.content = "Closing Meeting submitted successfully...";
                this.myCommonAlert(this.title, this.content);
                this.adminService.sendCount(true);
                setTimeout(() => {
                  var payload = { closeMeeting: this.count + 1 };
                this.httpservice
                  .securePost(ConstUrls.updateCountData, payload)
                  .subscribe((r) => {
                    if (r) {
                      this.count = this.count + 1;
                      this.memo.setValueann(this.count);
                    }
                  });
                }, 1000);
                
              }
            });
        }
      });
    } else {
      this.title = "Error";
      this.content = "Failed to Save...";
      this.myCommonAlert(this.title, this.content);
    }

    this.closeModal();
  }

  closeModal() {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
