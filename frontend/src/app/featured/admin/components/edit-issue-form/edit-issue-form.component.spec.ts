import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIssueFormComponent } from './edit-issue-form.component';

describe('EditIssueFormComponent', () => {
  let component: EditIssueFormComponent;
  let fixture: ComponentFixture<EditIssueFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIssueFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIssueFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
