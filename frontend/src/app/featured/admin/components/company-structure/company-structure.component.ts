import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { RedeemPopupComponent } from "src/app/layout/modal/redeem-popup/redeem-popup.component";
declare var $;
@Component({
  selector: 'app-company-structure',
  templateUrl: './company-structure.component.html',
  styleUrls: ['./company-structure.component.less']
})
export class CompanyStructureComponent implements OnInit {
  modalRef: BsModalRef;
  pieChartCanvas: any;
  pieChartCtx: any;
  pieActionChartCanvas: any;
  pieActionChartCtx: any;
  pieApprovalChartCanvas: any;
  pieApprovalChartCtx: any;
  pieChart: object;
  pieActionChart: object;
  pieApprovalChart: object;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  reedeemSection = false;
  FilterBtnFlag: string;
  dropdownList = [];
  dropdownFirmList = [];
  dropdownDepartmentList = [];
  selectedItems = [];
  dropdownSettings = {};
  constructor(private modalService: BsModalService) { }
  ngOnInit() {

     this.dropdownList = [
      { item_id: 1, item_text: 'Mumbai' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' }
    ];
    this.dropdownFirmList = [
      { item_id: 6, item_text: 'Marin CHE' },
      { item_id: 7, item_text: 'Marin COD' },
      { item_id: 8, item_text: 'Marin Holding' },
      { item_id: 9, item_text: 'Marin IDN' },
      { item_id: 10, item_text: 'Marin FFU' }
    ];
    this.dropdownDepartmentList = [
      { item_id: 11, item_text: 'Audit IDN' },
      { item_id: 12, item_text: 'Audit IQ' },
      { item_id: 13, item_text: 'Audit CHE' },
      { item_id: 14, item_text: 'Audit IDN' },
      { item_id: 15, item_text: 'Audit FFU' }
    ]
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.getCharts();
    this.tabScrolling();
  }
  tabScrolling() {
    $('.nav-tab-scroll').scrollingTabs({
      bootstrapVersion: 4,
      scrollToTabEdge: true,
      cssClassLeftArrow: 'tab-arrow tab-arrow-left',
      cssClassRightArrow: 'tab-arrow tab-arrow-right'
    });
  }

  FilterBtn() {
    this.FilterBtnFlag = 'ValidatorToken';

  }

  FilterBtnClose() {
    this.FilterBtnFlag = '';
  }

  redeemScreen() {
    this.reedeemSection = true;
    $('.redeem-section').removeClass('d-none');
  }
  backtoscreen() {
    this.reedeemSection = false;
  }
  closeRedmeen() {
    this.reedeemSection = false;
  }
  showChildModal() {

    this.modalRef = this.modalService.show(RedeemPopupComponent, {
      class: "modal-redeem",
      backdrop: "static",
      keyboard: false,
    });
  }

  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of doughnut element of variable


    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("bar");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "bar",
        data: {
          labels: ["Ontime", "Delayed", "Escalated"],
          datasets: [
            {
              //label: ' ',
              data: [5, 5, 5],
              backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: true,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false
              },
              gridLines: {
                display: false
              }
            }],
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
        },
      });
    }, 1000);


    // binding of pieChart element to variable
    this.pieActionChartCanvas = <HTMLCanvasElement>document.getElementById("actionbar");
    this.pieActionChartCtx = this.pieActionChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieActionChart = new Chart(this.pieActionChartCtx, {
        type: "bar",
        data: {
          labels: ["Ontime", "Delayed", "Escalated"],
          datasets: [
            {
              //label: ' ',
              data: [2, 5, 5],
              backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: true,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false
              },
              gridLines: {
                display: false
              }
            }],
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
        },
      });
    }, 1000);




    // binding of pieChart element to variable
    this.pieApprovalChartCanvas = <HTMLCanvasElement>document.getElementById("approvalbar");
    this.pieApprovalChartCtx = this.pieApprovalChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieApprovalChart = new Chart(this.pieApprovalChartCtx, {
        type: "bar",
        data: {
          labels: ["Ontime", "Delayed", "Escalated"],
          datasets: [
            {
              //label: ' ',
              data: [10, 1, 4],
              backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: true,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
          scales: {
            yAxes: [{
              ticks: {
                display: false
              },
              gridLines: {
                display: false
              }
            }],
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
        },
      });
    }, 1000);
  }
}