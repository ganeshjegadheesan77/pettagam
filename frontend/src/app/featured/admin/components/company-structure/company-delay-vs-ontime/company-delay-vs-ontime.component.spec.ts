import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDelayVsOntimeComponent } from './company-delay-vs-ontime.component';

describe('CompanyDelayVsOntimeComponent', () => {
  let component: CompanyDelayVsOntimeComponent;
  let fixture: ComponentFixture<CompanyDelayVsOntimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyDelayVsOntimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDelayVsOntimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
