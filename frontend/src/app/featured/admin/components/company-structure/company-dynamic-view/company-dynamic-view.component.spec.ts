import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDynamicViewComponent } from './company-dynamic-view.component';

describe('CompanyDynamicViewComponent', () => {
  let component: CompanyDynamicViewComponent;
  let fixture: ComponentFixture<CompanyDynamicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyDynamicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDynamicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
