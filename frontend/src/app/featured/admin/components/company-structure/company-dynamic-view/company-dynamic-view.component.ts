import { Component, OnInit   } from '@angular/core';
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { RedeemPopupComponent } from "src/app/layout/modal/redeem-popup/redeem-popup.component";
@Component({
  selector: 'app-company-dynamic-view',
  templateUrl: './company-dynamic-view.component.html',
  styleUrls: ['./company-dynamic-view.component.less']
})
export class CompanyDynamicViewComponent implements OnInit {
  modalRef: BsModalRef;
  pieChartCanvas: any;
  pieChartCtx: any;
  pieActionChartCanvas: any;
  pieActionChartCtx: any;
  pieApprovalChartCanvas: any;
  pieApprovalChartCtx: any;
  pieChart: object;
  pieActionChart:object;
  pieApprovalChart:object;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  reedeemSection = false;
  
  constructor(private modalService: BsModalService,) { }
​
  ngOnInit() {
    this.getCharts();
  }
  redeemScreen(){
    this.reedeemSection = true;
  }
  backtoscreen(){
    this.reedeemSection = false;
  }
  showChildModal(){

  this.modalRef = this.modalService.show(RedeemPopupComponent, {
    class: "modal-redeem",
    backdrop: "static",
    keyboard: false,
  });
}
​
  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);
​
    // binding of doughnut element of variable
​
​
    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("bar");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");
​
    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "bar",
        data: {
          labels: ["Ontime", "Delayed", "Escalated"],
          datasets: [
            {
              //label: ' ',
               data: [5, 5, 5],
              backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: true,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
          scales: {
            yAxes: [{
                ticks: {
                  display: false
                },
                gridLines: {
                  display: false
                }
            }],
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
        }
        },
      });
    }, 1000);


     // binding of pieChart element to variable
     this.pieActionChartCanvas = <HTMLCanvasElement>document.getElementById("actionbar");
     this.pieActionChartCtx = this.pieActionChartCanvas.getContext("2d");
 
     // Add for a pieChart chart
   setTimeout(() => {
       this.pieActionChart = new Chart(this.pieActionChartCtx, {
         type: "bar",
         data: {
           labels: ["Ontime", "Delayed", "Escalated"],
           datasets: [
             {
               //label: ' ',
                data: [2, 5, 5],
               backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
               borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
               borderWidth: 1,
             },
           ],
         },
         options: {
           //cutoutPercentage: 40,
           responsive: true,
           legend: {
             // position: 'bottom',
             label: {
               display: false,
             },
             display: false,
           },
           scales: {
             yAxes: [{
                 ticks: {
                   display: false
                 },
                 gridLines: {
                   display: false
                 }
             }],
             xAxes: [{
               gridLines: {
                 display: false
               }
             }]
         }
         },
       });
      }, 1000);



      
     // binding of pieChart element to variable
     this.pieApprovalChartCanvas = <HTMLCanvasElement>document.getElementById("approvalbar");
     this.pieApprovalChartCtx = this.pieApprovalChartCanvas.getContext("2d");
 
     // Add for a pieChart chart
   setTimeout(() => {
       this.pieApprovalChart = new Chart(this.pieApprovalChartCtx, {
         type: "bar",
         data: {
           labels: ["Ontime", "Delayed", "Escalated"],
           datasets: [
             {
               //label: ' ',
                data: [10, 1, 4],
               backgroundColor: ["#98aa3a", "#ffcc00", "#dd6838"],
               borderColor: ["#98aa3a", "#ffcc00", "#dd6838"],
               borderWidth: 1,
             },
           ],
         },
         options: {
           //cutoutPercentage: 40,
           responsive: true,
           legend: {
             // position: 'bottom',
             label: {
               display: false,
             },
             display: false,
           },
           scales: {
             yAxes: [{
                 ticks: {
                   display: false
                 },
                 gridLines: {
                   display: false
                 }
             }],
             xAxes: [{
               gridLines: {
                 display: false
               }
             }]
         }
         },
       });
      }, 1000);
  }
​
}