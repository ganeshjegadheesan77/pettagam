import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyExecutionComponent } from './company-execution.component';

describe('CompanyExecutionComponent', () => {
  let component: CompanyExecutionComponent;
  let fixture: ComponentFixture<CompanyExecutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyExecutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyExecutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
