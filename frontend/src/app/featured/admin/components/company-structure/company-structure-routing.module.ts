import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompanyStructureComponent } from './company-structure.component';
const routes: Routes = [{ path: '', component: CompanyStructureComponent, data: { title: "Company Structure" } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyStructureRoutingModule { }
