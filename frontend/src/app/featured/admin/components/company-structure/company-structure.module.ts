import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FormsModule } from '@angular/forms';

import { CompanyStructureRoutingModule } from "./company-structure-routing.module";
import { CompanyStructureComponent } from './company-structure.component';
import { CompanyDynamicViewComponent } from './company-dynamic-view/company-dynamic-view.component';
import { CompanyDelayVsOntimeComponent } from './company-delay-vs-ontime/company-delay-vs-ontime.component';
import { CompanyExecutionComponent } from './company-execution/company-execution.component';
import { CompanyApprovalComponent } from './company-approval/company-approval.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [
    CompanyStructureComponent,
    CompanyDynamicViewComponent,
    CompanyDelayVsOntimeComponent,
    CompanyExecutionComponent,
    CompanyApprovalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    AutocompleteLibModule,
  
    CompanyStructureRoutingModule,
    
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot()
  ],
  exports:[CommonModule],
  providers: [
  ]
})
export class CompanyStructureModule {}
