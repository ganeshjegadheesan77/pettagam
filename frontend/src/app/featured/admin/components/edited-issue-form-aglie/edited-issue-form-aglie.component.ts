import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmRejectWorkstepComponent } from 'src/app/layout/modal/confirm-reject-workstep/confirm-reject-workstep.component';
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
@Component({
  selector: 'app-edited-issue-form-aglie',
  templateUrl: './edited-issue-form-aglie.component.html',
  styleUrls: ['./edited-issue-form-aglie.component.less']
})
export class EditedIssueFormAglieComponent implements OnInit {
  position;
  searchDocument: boolean = false;
  marked: boolean;
  openAttachment: boolean;
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService, private chRef: ChangeDetectorRef, private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.issueActiondatatable()
    this.issueActionSelecteddatatable();
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '180px',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',

      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [
        
        'redo',
        
        
        
        'strikeThrough',
        'subscript',
        'superscript',
        
        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
    'textColor',
    'backgroundColor',
    'customClasses',
    'link',
    'unlink',
    'insertImage',
    'insertVideo',
    'insertHorizontalRule',
    'removeFormat',
    'toggleEditorMode'
      ]
    ]
};
submitForm() {
  $(".animation-submit").addClass('active-btn');
  this.closeModal();
}

saveForm() {
  $(".animation-save").addClass('active-btn');

}
  closeModal() {
  
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }

  issueActiondatatable() {

    this.chRef.detectChanges();
    const issueTable: any = $("#issueActionTable");
    const issueListTableCM = this.dataTableService.initializeTable(
      issueTable,
      `issueActionAgile`
    );
    this.dataTableFuncService.selectAllRows(
      issueListTableCM,
      `issueAction-select-all`
    );
    this.dataTableFuncService.expandContainer(issueListTableCM);
    $("#issueActionTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issueAction-select-all").prop("checked", false);
          var el = $("#issueAction-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issueActionTable tr td input[type='checkbox']:checked").length ==
          $("#issueActionTable tr td input[type='checkbox']").length
        ) {
          $("#issueAction-select-all").prop("checked", true);
        }
      }
    );
  
  }
  issueActionSelecteddatatable() {
    this.chRef.detectChanges();
    const issueSelectedTable: any = $("#issueActionSelectedTable");
    const issueListSelectedTableCM = this.dataTableService.initializeTable(
      issueSelectedTable,
      `issueActionSelectedAgile`
    );
    this.dataTableFuncService.selectAllRows(
      issueListSelectedTableCM,
      `issueActionSelected-select-all`
    );
    this.dataTableFuncService.expandContainer(issueListSelectedTableCM);
    $("#issueActionSelectedTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issueActionSelected-select-all").prop("checked", false);
          var el = $("#issueActionSelected-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issueActionSelectedTable tr td input[type='checkbox']:checked").length ==
          $("#issueActionSelectedTable tr td input[type='checkbox']").length
        ) {
          $("#issueActionSelected-select-all").prop("checked", true);
        }
      }
    );
  
  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
      
      //console.log('left curr status:' , this.marked)
      
    } else {
      // attachment left
  
      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }
  rejetWorkStep() {
    const initialState = {
      title: "Confirm Reject WorkStep",
      content: `WS-102`,
      link: "Yes, Reject the workstep",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmRejectWorkstepComponent, {
      initialState,
      class: "error-popup-design extra-popup",
      backdrop: "static",
      keyboard: false,
    });
  }
  addnewFile() {
    this.searchDocument = true;

  }
  closeSearch() {
    this.searchDocument = false;

  }
}
