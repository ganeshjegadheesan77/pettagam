import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditedIssueFormAglieComponent } from './edited-issue-form-aglie.component';

describe('EditedIssueFormAglieComponent', () => {
  let component: EditedIssueFormAglieComponent;
  let fixture: ComponentFixture<EditedIssueFormAglieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditedIssueFormAglieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditedIssueFormAglieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
