import { Component, OnInit } from '@angular/core';
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-aglie-audit-report',
  templateUrl: './aglie-audit-report.component.html',
  styleUrls: ['./aglie-audit-report.component.less']
})
export class AglieAuditReportComponent implements OnInit {
  doughnutChartSec: Chart;
  doughnutChartSecOne: Chart;
  doughnutChartCanvas: any;
  doughnutChartCanvasOne: any;
  doughnutChartCtx: any;
  doughnutChartCtxOne: any;
  auditId;
  auditDetails;
  issues;
  highIssueCount;
  mediumIssueCount;
  lowIssueCount;
  monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  monthNamesSmall = ["Jan", "Feb", "Mar", "Apr", "May", "June",
    "July", "Aug", "Sept", "Oct", "Nov", "Dec"
  ];

  reportNumber = '02';
  todaysDate = new Date();

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.click();
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('admin-chat');
    this.getCharts();

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params["auditId"]) {
        debugger;
        this.auditId = params["auditId"];
        var auditArray = JSON.parse(localStorage.getItem('auditData'));
        this.auditDetails = auditArray.filter(x => x.auditId == this.auditId)[0];
        this.issues = JSON.parse(localStorage.getItem('auditorData')).find(a => a.audit == this.auditId).issues;
        this.highIssueCount = this.issues.filter(i => i.priority == 'High').length;
        this.mediumIssueCount = this.issues.filter(i => i.priority == 'Medium').length;
        this.lowIssueCount = this.issues.filter(i => i.priority == 'Low').length;

        // this.auditDetails.openingMeetingDate = this.convertDate(this.auditDetails.openingMeetingDate, 'meeting_date');
        // this.auditDetails.closingMeetingDate = this.convertDate(this.auditDetails.closingMeetingDate, 'meeting_date');
      }
    });
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('admin-chat');
  }

  click() {

    $("#editBtn").click(function () {
      $(this).hide();
      $(".change").prop('disabled', false);
      $(".Btsave").addClass("active-in");
      $(".editBt").removeClass("active-in");
    });
    $("#expBtn").click(function () {
      $("#editBtn").show();
      $(".change").prop('disabled', true);
      $(".Btsave").removeClass("active-in");
      $(".editBt").addClass("active-in");
    });
  }

  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);






    this.doughnutChartCanvas = <HTMLCanvasElement>document.getElementById("chart");
    this.doughnutChartCtx = this.doughnutChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.doughnutChartSec = new Chart(this.doughnutChartCtx, {
        type: "doughnut",
        data: {
          labels: ["High", "Medium", "Low"],
          datasets: [
            {
              //label: ' ',
              data: [this.highIssueCount, this.mediumIssueCount, this.lowIssueCount],
              backgroundColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);

    this.doughnutChartCanvasOne = <HTMLCanvasElement>document.getElementById("chartOne");
    this.doughnutChartCtxOne = this.doughnutChartCanvasOne.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.doughnutChartSecOne = new Chart(this.doughnutChartCtxOne, {
        type: "doughnut",
        data: {
          labels: ["High", "Medium", "Low"],
          datasets: [
            {
              //label: ' ',
              data: [this.highIssueCount, this.mediumIssueCount, this.lowIssueCount],
              backgroundColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);

  }

  convertDate(date, format) {
    var d = new Date(date);
    var newDate = '';
    switch (format) {
      case 'meeting_date':
        newDate += d.getDate() + ". " + this.monthNames[d.getMonth()] + " " + d.getFullYear();
        break;
      case 'report_date':
        newDate += this.monthNamesSmall[d.getMonth()] + " " + d.getDate() + ", " + d.getFullYear();
        break;
    }
    return newDate;
  }

  exportToPdf(){
    window.print();
  }
}
