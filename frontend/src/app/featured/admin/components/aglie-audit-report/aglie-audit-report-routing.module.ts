import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AglieAuditReportComponent } from './aglie-audit-report.component';
const routes: Routes = [{ path: '', component: AglieAuditReportComponent, data: { title: "IA Report" } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AglieAuditReportRoutingModule { }