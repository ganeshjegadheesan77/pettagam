import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AglieAuditReportComponent } from './aglie-audit-report.component';

describe('AglieAuditReportComponent', () => {
  let component: AglieAuditReportComponent;
  let fixture: ComponentFixture<AglieAuditReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AglieAuditReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AglieAuditReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
