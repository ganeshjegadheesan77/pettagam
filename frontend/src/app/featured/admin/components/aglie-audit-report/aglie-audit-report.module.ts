import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FormsModule } from '@angular/forms';
import { AglieAuditReportRoutingModule } from "./aglie-audit-report-routing.module";
import { AglieAuditReportComponent } from './aglie-audit-report.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [
    AglieAuditReportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AutocompleteLibModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    AglieAuditReportRoutingModule
  ],
  exports:[CommonModule],
  providers: [
  ]
})
export class aglieAuditReportModule {}
