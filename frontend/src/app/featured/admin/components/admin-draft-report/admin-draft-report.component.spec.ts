import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDraftReportComponent } from './admin-draft-report.component';

describe('AdminDraftReportComponent', () => {
  let component: AdminDraftReportComponent;
  let fixture: ComponentFixture<AdminDraftReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDraftReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDraftReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
