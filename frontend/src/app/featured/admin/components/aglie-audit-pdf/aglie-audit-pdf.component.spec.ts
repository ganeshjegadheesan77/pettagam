import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AglieAuditPdfComponent } from './aglie-audit-pdf.component';

describe('AglieAuditPdfComponent', () => {
  let component: AglieAuditPdfComponent;
  let fixture: ComponentFixture<AglieAuditPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AglieAuditPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AglieAuditPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
