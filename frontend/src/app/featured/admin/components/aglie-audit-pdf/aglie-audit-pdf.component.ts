import { Component, OnInit } from '@angular/core';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
@Component({
  selector: 'app-aglie-audit-pdf',
  templateUrl: './aglie-audit-pdf.component.html',
  styleUrls: ['./aglie-audit-pdf.component.less']
})
export class AglieAuditPdfComponent implements OnInit {
  position;
  pdfSrc = "../../../../../assets/pdf/confidential-ia-report.pdf";
  constructor(private mScrollbarService: MalihuScrollbarService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
  }
  closeModal(){
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0}
  }

}
