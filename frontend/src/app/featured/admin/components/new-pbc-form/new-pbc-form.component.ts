import {
  Component,
  OnInit,
  OnChanges,
  SimpleChange,
  SimpleChanges,
  Output,
  EventEmitter,
  Input,
  ChangeDetectorRef
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";

import * as moment from "moment";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { ConfigService } from 'src/config.service';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import { AdminService } from "../../admin.service";
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { CollabChatServiceService } from 'src/app/layout/collaboration-elements/collabServices/collab-chat-service.service';
import { MemoService } from 'src/app/featured/internal-audit/components/field-work/memo.service';
@Component({
  selector: "app-new-pbc-form",
  templateUrl: "./new-pbc-form.component.html",
  styleUrls: ["./new-pbc-form.component.less"],
})
export class NewPbcFormComponent implements OnInit {
  @Output() closePbcNewForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @Input("selectAudit") selectAudit: any;
  @Input("selectAudit") activeAuditId: any;

  
  uri = environment.baseUrl;
  position;
  PBCForm: FormGroup;
  modalRef: BsModalRef;
  auditId: any;
  selectedAuditId: any;
  newId: number;
  PBCId: string;
  fieldPanes: any;
  mixArr: any;
  preparerArr: any[];
  reviewerArr: any[];
  pbcData: any;
  PBCPOPUPForm: FormGroup;

  modalRef3: any;
  // assign search  added by Abu - 17-09-2020 start 
  editsearchDocument: boolean = false;
  addnewPBC: boolean = false;
  router: any;
  myForm: FormGroup;
  minDate:Date;
  FormSaved:any;
  title: string;
  content: string;
 
  isAttachment: boolean;
  marked: boolean;
  openAttachment: boolean;
  uploadedFilesArr;
  multiFile: any = [];
  // attachedFiles: any;
  attachedFiles: any = [];
  fetchedData: any = [];
  lastPbc: number;
  auditPBC_id: any;
  pbc_id: number;

  constructor(
    private _fb: FormBuilder,
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private configService: ConfigService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    private collabChatServiceService: CollabChatServiceService,
    private epService: MemoService


  ) {
    this.FormSaved = false;
    this.PBCPOPUPForm = _fb.group({
      'dueDate': [ null, Validators.required ],
      'docRequested': [ null, Validators.required ],
      'remarks': [ null, Validators.required ]


    })
 
    this.minDate = new Date();

    this.uri = configService.config.baseUrl;

  }

  ngOnInit() {


    console.log('check pbc',this.activeAuditId)

    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.epService.auditId.subscribe((auditId) => {
      this.auditId = this.activeAuditId;

      console.log('@@@@@@@@@@@@@@@@@@@@@@@', auditId);
    });
    this.initForm();
    this.getPbcData();



    // $('#dueDate').bsdatepicker({
    //   startDate: new Date()
    // });
  }

  blockInput(event) {
    event.preventDefault();
  }

  unLockFile(i){
    this.pbcData.auditPbc.documentList[i].isLocked = false;
    this.pbcData.auditPbc.documentList[i].isApproved = false;
    this.pbcData.auditPbc.documentList[i].isRejected = false;


  }

 

  getPbcData() {
    let payload =
    {
      auditId: this.activeAuditId
    }
    this.fetchedData = [];


    this.httpservice.post(ConstUrls.getPbcData, payload).subscribe(
      (response: any) => {
        console.log('getdata@@@@@@@@@', response)
        this.pbcData = response.data;
        if (response.data.teamAndTimeline.periodInScope && response.data.teamAndTimeline.periodInScope.length > 0) {
          this.pbcData.teamAndTimeline.periodInScope = [new Date(response.data.teamAndTimeline.periodInScope[0]), new Date(response.data.teamAndTimeline.periodInScope[1])];
          this.pbcData.teamAndTimeline.pbc = response.data.teamAndTimeline.pbc;
          console.log("$$$",this.pbcData.teamAndTimeline.pbc)
        }
        else {
          this.pbcData.teamAndTimeline.periodInScope = null;
          this.pbcData.teamAndTimeline.pbc = null;

          // this.pbcContactList = teamAndTimeLine.teamAndTimeline.pbc;
        }
        if (this.pbcData.auditPbc.documentList && this.pbcData.auditPbc.documentList.length > 0) {
          this.pbcData.auditPbc.documentList.forEach(d => {
            console.log('*****DMS INFO' , d.dmsInfo);
            if(d.dmsInfo.documentName)
            {
              let splitFileNameArr = d.dmsInfo.documentName.split(".")[1];

              console.log('****splitFileNameArr' , splitFileNameArr)
            
                 d.dmsInfo.extenstion = splitFileNameArr;
            }else{
              d.dmsInfo.extenstion = '';

            }
            
            d.dueDate = new Date(d.dueDate);
            d.dateUploadedDisplay = this.convertDateToFormat("dd.mm.yyyy", d.dateUploaded);
         
        

        
          }

        
          )
        }
  
        this.datatable();
      },
      (err: any) => {

      });
  }

  initForm() {
    this.PBCPOPUPForm = this.formBuilder.group({
      docRequested: ["", Validators.required],
      auditScope: ["", []],
      remarks: ["",  Validators.required],
      dueDate: ['', Validators.required],
    

    });
  }

  notAllowed() {
    this.myCommonAlert("Not Allowed", "Audit Lead cannot upload docs!");
  }

  // checkIt()
  // {
  //   this.myCommonAlert("Not Allowed", "select Auditee First!");

  // }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });

    let currentUser = localStorage.getItem("username");
    // this.documentRequestArray.push(this.PBCForm.value);
    console.log('Arry', this.pbcData);
    if (this.pbcData.auditPbc.preparer && this.pbcData.auditPbc.preparer.length > 0) {
      if (!this.pbcData.auditPbc.preparer.includes(currentUser) && this.pbcData.auditPbc.preparer.length < 2) {
        this.pbcData.auditPbc.preparer.push(currentUser);
      }
    }
    else {
      this.pbcData.auditPbc.preparer = [currentUser];
    }

    this.pbcData.auditPbc.documentList.forEach(d => {
      if (d.isApproved === true) {
        d.isLocked = true;
      }
    });
    this.FormSaved =true;
    this.pbcData.auditPbc.isCompleted = true;
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updatePbcDoc, { auditPbc: this.pbcData.auditPbc, auditId: this.activeAuditId })
          .subscribe((res: any) => {
            if (res) {
              console.log('@@@', res)
              this.title = "Success";
              this.content = "PBC saved successfully...";
              this.myCommonAlert(this.title, this.content);
              this.epService.refresh(true);
              // this.closeModal();
            }
          },
          err=>{
            this.myCommonAlert("Failure", "Please try again");
          });
      }

    });

  }

  onSend() {
    if( this.pbcData.auditPbc.documentList.length <= 0)
    {
      this.myCommonAlert("Not Allowed", "Add The Document Request First!");
    }
      else if( !this.pbcData.teamAndTimeline.pbc || this.pbcData.teamAndTimeline.pbc.length === 0)
      {
        
        this.myCommonAlert("Not Allowed", "Select The Auditee First!");
    
      }
      else if( this.FormSaved===false)
      {
      this.myCommonAlert("Not Allowed", "Save The Form First!");
           
      }
 
    else{

    var payload = {
       audit_id: this.activeAuditId,
      requestor_id: localStorage.getItem("username")
    }

    this.httpservice.post(ConstUrls.pbcSendMail, payload).subscribe(
      (response: any) => {
        const initialState = { title: "Mail sent", content: `http://${location.host}/auditee-pbc-form?urlToken=${response.data}`, link: "Copy Link" };
        this.modalRef = this.modalService.show(DefaultModalComponent, {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        });
      },
      (err: any) => {

      });
    }
  }


  onSavePOPUP() {
    if (this.PBCPOPUPForm.invalid) {
      this.myCommonAlert("Not Allowed", "Fill The Form First!");
      
    } else {
    const initialState = {
        title: "Confirmation",
        content: `Do you want to save current changes?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "error-popup-design confirm-popup",
        backdrop: "static",
        keyboard: false,
      });

  
    let currentUser = localStorage.getItem("username");

    console.log('Arry', this.pbcData.auditPbc.documentList);
    this.PBCPOPUPForm.patchValue({
      UploadedBy: currentUser,

      // apbc_signoff_period: apbc_signoff_period,
      // auditableEntity: this.auditableEntity,
      // master_audit_id: this.selectAudit,
      auditId: this.activeAuditId,

    });
    // let payload = {
    //   auditId: this.auditId,
    //   documentList: this.documentRequestArray
    // }
    console.log("Form Contents: ", this.PBCPOPUPForm.value);
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.pbcData.auditPbc.documentList.push(this.PBCPOPUPForm.value);
        this.pbcData.auditPbc.documentList.forEach(d => {
          
          d.dueDate = new Date(d.dueDate);
          d.dateUploadedDisplay = this.convertDateToFormat("dd.mm.yyyy", d.dateUploaded);
        });
        let table = $("#pbcDocumentTable").DataTable();
        table.destroy();
        this.closeInnerModal();
        this.content = "PBC Request Saved Successfully...";
        this.myCommonAlert(this.content,this.title);
        this.datatable();
      }
      //   this.httpservice
      //     .securePost(ConstUrls.updatePbcPopup, payload)
      //     .subscribe((res: any) => {
      //       if (res) {
      //         console.log('@@@', res)
      //         this.title = "Success";
      //         this.content = "PBC saved successfully...";
      //         this.myCommonAlert(this.title, this.content);

      //         let table = $("#pbcDocumentTable").DataTable();
      //         // this.ngOnInit();
      //         table.destroy();

      //         this.closeInnerModal();
      //         this.datatable();
      //       }
      //     });
      // }
    });
  }
  }

  myCommonAlert( title,content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",
      
      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, 3000);
  }
  closeModal() {
    this.closePbcNewForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };

  }
  closeInnerModal() {
    

    this.addnewPBC = false;
    
  }

  saveModel() {
    this.addnewPBC = false;
  }
  // print click added by Abu - 16-09-2020 start
  print() {
    window.print();
  }


  datatable() {
    this.chRef.detectChanges();
    setTimeout(() => {
      $("#pbcDocumentTable").DataTable({
        processing: true,
        responsive: true,
        // fixedHeader: true,
        autoWidth: false,
        scrollX: false,
        scrollCollapse: true,
        // bPaginate: true,
        columnDefs: [
          { targets: 0, visible: false, width: '1px' }
        ],
        lengthMenu: [
          [5, 10, 20, 50, 100, -1],
          [5, 10, 20, 50, 100, "All"]
        ],
        // bInfo: true,
        searching: false,
      });
    }, 100);
  }

  deleteFileEntry(i) {
    var file = this.pbcData.auditPbc.documentList[i].dmsInfo;
    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete current Entry?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          // documentNo: file.documentNo,
          // fileName: file.documentName,
          // _id: file.item_id,
          deletedBy: localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
        };
        console.log("*****payload:", payload);
        this.httpservice
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res) {
          
              let pay = {
                // userType: type,
                username: this.pbcData.auditPbc.documentList[i].uploadedBy,
                auditId: this.activeAuditId,
                rowIndex: i,
              };
              console.log('saved')
              this.httpservice
                .post(ConstUrls.deletePbcEntry, pay)
                .subscribe((resp: any) => {
                  if (resp) {

                    this.pbcData.auditPbc.documentList.splice(i, 1);
                    let table = $("#pbcDocumentTable").DataTable();
                    table.destroy();
                    this.datatable();
                    this.title = "Success";
                    this.content = "Entry Deleted successfully...";
                    this.myCommonAlert(this.title, this.content);
              
              
                  }
                });

              // this.myCommonAlert("Success", "Document deleted successfully.");
            } else {
              this.myCommonAlert("Eroor", res.message);
            }
          });

      }
    });
  }


  deleteFileAttachment(i) {
    var file = this.pbcData.auditPbc.documentList[i].dmsInfo;
    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete current File?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: file.documentNo,
          fileName: file.documentName,
          // _id: file.item_id,
          deletedBy: localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
        };
        console.log("*****payload:", payload);
        this.httpservice
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res) {
              // let payload ={
              //   username: this.pbcData.auditPbc.documentList[i].uploadedBy,
              //   auditId: this.auditId,
              //   rowIndex: i,
              //   documentUrlToken: this.documentList
              // }
            
              let payload = {
                
                 
                  "auditId" : this.activeAuditId,
                  "rowIndex" : i
              };
              console.log("Payload for deletePbcAttachment : ", payload)
              this.httpservice
                .post(ConstUrls.deletePbcAttachment, payload)
                .subscribe((resp: any) => {
                  if (resp) {
                    console.log("eee",resp)
                    this.pbcData.auditPbc.documentList[i].dmsInfo={};
                    let table = $("#pbcDocumentTable").DataTable();
                    table.destroy();
                    this.datatable();
                    this.title = "Success";
                    this.content = "File Deleted successfully...";
                    this.myCommonAlert(this.title, this.content);
        
                  }
                });

              // this.myCommonAlert("Success", "Document deleted successfully.");
            } else {
              this.myCommonAlert("Eroor", res.message);
            }
          });

      }
    });
  }

  downloadFile(i) {
    let fileUrl = `${this.configService.config.dmsBaseUrl}?dmsSession=${localStorage.getItem("dmsToken")}&urlToken=${this.pbcData.auditPbc.documentList[i].dmsInfo.documentUrlToken}`;
    var filename = this.pbcData.auditPbc.documentList[i].dmsInfo.documentName;
    fetch(fileUrl)
      .then(resp => resp.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        // alert('your file has downloaded!'); // or you know, something with better UX...
      })
      .catch((err) => {
        alert('Cannot download doc!');
        console.log("download error", err);
      }
      );
  }

  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    console.log('extension', extension)
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      console.log('valid')
      return true;
    } else {
      console.log('invalid')

      return false;
    }
  }

  onApproveToggleChange(event, i) {
    if (event.target.checked) {
      this.pbcData.auditPbc.documentList[i].isApproved = true;
      this.pbcData.auditPbc.documentList[i].isRejected = false;
    }
    else {
      this.pbcData.auditPbc.documentList[i].isApproved = false;
      this.pbcData.auditPbc.documentList[i].isRejected = true;
    }
  }

  toggleEditable(event, i) {

    if (event.target.checked) {
      $('.switch-toggle-block input:checked').parent('.switch-toggle-block').addClass('show-checked');
      this.pbcData.auditPbc.documentList[i].isApproved = false;
      this.pbcData.auditPbc.documentList[i].isRejected = true;
    } else {
      this.pbcData.auditPbc.documentList[i].isApproved = false;
      this.pbcData.auditPbc.documentList[i].isRejected = false;
      $('.switch-toggle-block').removeClass('show-checked');
    }
  }

  convertDateToFormat(format, date): String {
    if (date) {
      var myDate = new Date(date);
      var dd = String(myDate.getDate()).padStart(2, '0');
      var mm = String(myDate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = myDate.getFullYear();
      switch (format) {
        case "dd.mm.yyyy":
          return dd + '.' + mm + '.' + yyyy;
        case "dd/mm/yyyy":
          return dd + '/' + mm + '/' + yyyy;
        case "mm.dd.yyyy":
          return mm + '.' + dd + '.' + yyyy;
        case "mm/dd/yyyy":
          return mm + '/' + dd + '/' + yyyy;
      }
    }
    else {
      return "--/--/----"
    }
  }

  addnewPBCpopup() {
    this.addnewPBC = true;
    this.PBCPOPUPForm.reset();
  }
  addnewFile() {
    this.editsearchDocument = true;
  }
  getSelectedDocs() {
    this.editsearchDocument = false;
  }
  closeSearch() {
    this.editsearchDocument = false;
  }

}