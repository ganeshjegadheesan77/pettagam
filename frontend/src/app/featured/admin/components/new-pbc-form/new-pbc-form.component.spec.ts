import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPbcFormComponent } from './new-pbc-form.component';

describe('NewPbcFormComponent', () => {
  let component: NewPbcFormComponent;
  let fixture: ComponentFixture<NewPbcFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPbcFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPbcFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
