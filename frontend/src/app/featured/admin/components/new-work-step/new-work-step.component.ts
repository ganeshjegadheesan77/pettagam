import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from "@angular/core";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
} from "ngx-file-drop";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConstUrls } from "src/app/config/const-urls";
import { HttpService } from "../../../../core/http-services/http.service";
import { DataTableService } from 'src/app/featured/services/dataTable.service';
import { DataTableFuncService } from 'src/app/featured/services/dataTableFunc.service';
import * as moment from "moment";
import { ConfigService } from 'src/config.service';
import { ConfirmRejectWorkstepComponent } from 'src/app/layout/modal/confirm-reject-workstep/confirm-reject-workstep.component';
import { LocalStorageService } from 'src/app/core/data-services/local-storage.service';
import { constants } from 'src/app/config/constants';
import { slice } from '@amcharts/amcharts4/.internal/core/utils/Array';
declare var $;

@Component({
  selector: "app-new-work-step",
  templateUrl: "./new-work-step.component.html",
  styleUrls: ["./new-work-step.component.less"],
})
export class NewWorkStepComponent implements OnInit {
  position;
  @Input() auditId;
  @Input() workstepId;

  @Input() auditableEntity;
  @Output() closePopup = new EventEmitter<any>();

  onfileChangeOut = false;
  onfileChangeIn = false;
  documentType = ''; //value will be either workpaper or refrenceDoc
  workstepForm: WorkStepModel;
  isNew = true;
  content: string;
  boolean = false;
  uploadedFilesArr: any;
  multiFile: any = [];
  attachedFiles: any = [];
  dropdownListAll = [];
  dropdownList = [];
  dropdownFirmList = [];
  selectedItems = [];
  dropdownSettings = {};
  disableSearch = false;
  editsearchDocument: boolean = false;
  docNo: any;

  oldFileName: any;
  chkpayload: any;
  checkInBy: any;
  userType: any;
  globalSearchData = "";
  searchData = "";
  documentNo = "";
  workPaperNumber: "";
  auditName = "";
  arr = [];
  currentUser = "";
  renameFileName: any;
  renameFileExtension: any;
  comments: string;
  title: string;
  // refDocuments: any = [];
  // wpDocuments: any = [];
  audit: any;
  searchDocument: boolean = false;
  auditData: any;
  showIssueError = false;
  // showPreparerList = false;
  isAuditor = true;

  //input varible for pdf annotation
  //start
  documentNumber: string;
  documentUrl: string;
  doucmentName: string;
  workpaperNum: string;
  documentAuditId: string;
  docType: string;
  //end
  public renameDoc = false;
  previewMode = false;
  tokenClaims;
  baseProfileUrl;
  selectedPreparer = {
    username: '',
    profileUrl: '',
    preparedOn: null,
    status: ''
  }
  onSaveOrEditWorkpaper(documentObj) {
    if (this.previewMode) {
      this.myCommonAlert('Error', 'Please close existing open file..');
      return;
    }
    this.documentUrl = `${this.configService.config.dmsBaseUrl}?dmsSession=${localStorage.getItem("dmsToken")}&urlToken=${documentObj.dmsRepoDetail.urlToken}`
    this.documentNumber = documentObj.dmsRepoDetail.documentNo;
    this.doucmentName = documentObj.dmsRepoDetail.fileName;
    this.workpaperNum = documentObj.dmsRepoDetail.workPaperNumber;
    this.previewMode = true
    this.docType = documentObj.dmsRepoDetail.docType;
  }

  modalRef: BsModalRef;
  disableAll = false;
  errorTarget = '';
  preparerList: any;

  constructor(
    private modalService: BsModalService,
    private configService: ConfigService,
    private localStorageService: LocalStorageService,
    public httpService: HttpService, private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.baseProfileUrl = this.configService.config.baseUrl;
    this.tokenClaims = this.localStorageService.parseToken();
    this.workstepForm = new WorkStepModel();
    if (this.tokenClaims.role == 'auditLead') {
      this.isAuditor = false;
      this.loadAuditLeadConfig();
    }
    else {
      this.isAuditor = true;
      this.loadAuditorConfig();
    }

    if (this.workstepId) {
      this.isNew = false;
      this.getWorkStep();
    }
    else {
      this.isNew = true;
      if (!this.isAuditor) {
        this.getPreparerList();
      }
    }

    this.workstepForm.auditId = this.auditId;
    this.dropdownSettings = {
      singleSelection: false,
      idField: "documentNo",
      textField: "fileName",
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true,
      clearSearchFilter: true
    };
    this.issueActiondatatable()
    this.issueActionSelecteddatatable();
  }

  onAnnotationComponenetClose(flag) {
    if (flag.close) {
      if (flag.documentNo && flag.documentName) {
        if (flag.docType == constants.docType.referenceDoc) {
          var i = this.workstepForm.referenceDocuments.findIndex(w => w.dmsRepoDetail.documentNo == flag.documentNo);
          this.workstepForm.referenceDocuments[i].dmsRepoDetail.fileName = flag.documentName;
          this.workstepForm.referenceDocuments[i].documentName = flag.documentName;
          this.workstepForm.referenceDocuments[i].dmsRepoDetail.workPaperNumber = flag.workPaperNumber;

        }
        if (flag.docType == constants.docType.workPaper) {
          var i = this.workstepForm.workingPapers.findIndex(w => w.dmsRepoDetail.documentNo == flag.documentNo);
          this.workstepForm.workingPapers[i].dmsRepoDetail.fileName = flag.documentName;
          this.workstepForm.workingPapers[i].documentName = flag.documentName;
          this.workstepForm.workingPapers[i].dmsRepoDetail.workPaperNumber = flag.workPaperNumber;
        }
      }
      this.previewMode = false;
      this.documentUrl = '',
        this.documentNumber = '',
        this.doucmentName = '',
        this.workpaperNum = '',
        this.docType = ''
    }
  }

  onPreparerChange(event, p, i) {
    this.errorTarget = '';
    if (event.target.checked) {
      $(".plist:not(#" + event.target.id + ")").prop('checked', false);
      $("#" + event.target.id + "").prop('checked', true);
      this.selectedPreparer = {
        username: p.preparer,
        profileUrl: p.path,
        preparedOn: null,
        status: null
      };
    }
    else if ($('.plist').not(':checked').length == $('.plist').length) {
      this.selectedPreparer = {
        username: null,
        profileUrl: null,
        preparedOn: null,
        status: null
      };
    }
  }

  getPreparerList() {
    if (this.auditId) {
      let payload = { auditId: this.auditId };
      this.httpService.securePost(ConstUrls.getPreparerList, payload).subscribe((res: any) => {
        if (res) {
          if (res.data[1].preaprerData.length) {
            this.preparerList = res.data[1].preaprerData;
          } else {
            this.preparerList = [];
          }


        }
      }, (error) => {
        this.myCommonAlert('Error', 'No Preparers Found!');
      })
    }
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "180px",
    minHeight: "0",
    maxHeight: "auto",
    width: "auto",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",

    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: "redText",
        class: "redText",
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ],
    uploadUrl: "v1/image",
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      [
        "redo",

        "strikeThrough",
        "subscript",
        "superscript",

        "justifyFull",
        "indent",
        "outdent",
        "heading",
        "fontName",
      ],
      [
        "fontSize",
        "textColor",
        "backgroundColor",
        "customClasses",
        "link",
        "unlink",
        "insertImage",
        "insertVideo",
        "insertHorizontalRule",
        "removeFormat",
        "toggleEditorMode",
      ],
    ],
  };

  loadAuditLeadConfig() {
    this.workstepForm.reviewer.username = this.tokenClaims.username;
    this.workstepForm.reviewer.profileUrl = this.tokenClaims.profileUrl;
  }

  loadAuditorConfig() {
    this.workstepForm.preparer.username = this.tokenClaims.username;
    this.workstepForm.preparer.profileUrl = this.tokenClaims.profileUrl;
  }

  getWorkStep() {
    this.httpService.secureGet(ConstUrls.getWorkStep + "/" + this.workstepId).subscribe(
      res => {
        this.workstepForm = res['data'];
        if (this.workstepForm.referenceDocuments && this.workstepForm.referenceDocuments.length > 0) {
          this.workstepForm.referenceDocuments.forEach(r => {
            let extn = r.documentName.substr(r.documentName.lastIndexOf(".") + 1);
            r.extension = extn;

            r.dateUploaded = new Date(r.dateUploaded);
            r.dateUploadedDisplay = this.convertDateToFormat("dd.mm.yyyy", r.dateUploaded);
          })
        }

        if (this.workstepForm.workingPapers && this.workstepForm.workingPapers.length > 0) {
          this.workstepForm.workingPapers.forEach(r => {
            let extn = r.documentName.substr(r.documentName.lastIndexOf(".") + 1);
            r.extension = extn;
            r.dateUploaded = new Date(r.dateUploaded);
            r.dateUploadedDisplay = this.convertDateToFormat("dd.mm.yyyy", r.dateUploaded);
          })
        }

        if (!this.isAuditor && !this.workstepForm.preparer.username) {
          this.getPreparerList();
        }
      },
      err => {

      }
    );
  }

  convertDateToFormat(format, date): string {
    var myDate = new Date(date);
    var dd = String(myDate.getDate()).padStart(2, '0');
    var mm = String(myDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = myDate.getFullYear();
    switch (format) {
      case "dd.mm.yyyy":
        return dd + '.' + mm + '.' + yyyy;
      case "dd/mm/yyyy":
        return dd + '/' + mm + '/' + yyyy;
      case "mm.dd.yyyy":
        return mm + '.' + dd + '.' + yyyy;
      case "mm/dd/yyyy":
        return mm + '/' + dd + '/' + yyyy;
    }
  }

  getSelectedDocs($event, docType) {
    // this.searchDocument = false;
    var fileList = this.selectedItems.map((item) => {
      return item["fileName"];
    });
    for (let i = 0; i < this.selectedItems.length; i++) {
      this.selectedItems[i]['metaData'] = `Module-Name=AgileDashboard,Feature-Name=SaveAs,Doc-Type=DATAFILE,Sequence-No=1234}`
      // this.selectedItems[i]['dmsSessionId'] = "fbced90e8dd22335df831ad58e2295a92c04bb96735376eb3394e3dd0ac46a8544c3aab27e8f6185";
      this.selectedItems[i]['dmsSessionId'] = localStorage.getItem('dmsToken');
      this.selectedItems[i]['userType'] = 'auditor';
      this.selectedItems[i]['checkedInBy'] = localStorage.getItem("username");
      this.selectedItems[i]['saveAsBy'] = localStorage.getItem("username");
      this.selectedItems[i]['roomType'] = '';
      this.selectedItems[i]['routerLink'] = '';
      this.selectedItems[i]['subModuleName'] = 'workstep-form';
      this.selectedItems[i]['auditYear'] = new Date().getFullYear();
      this.selectedItems[i]['auditId'] = this.auditId;
      this.selectedItems[i]['auditName'] = this.auditableEntity; ``
      this.selectedItems[i]['moduleName'] = 'agileDashboard';
      this.selectedItems[i]['checkedinDate'] = moment().format("L");
      let wpNumber_ = ''
      if (this.selectedItems[i].fileName.indexOf("WP-") === 0 && this.selectedItems[i].fileName.indexOf("_OP-") > -1) {
        let tempArr = this.selectedItems[i].fileName.split("_");
        wpNumber_ = tempArr[0];
      }
      this.selectedItems[i]["workPaperNumber"] = wpNumber_;
    }
    let payloads = { items: this.selectedItems };
    this.httpService.securePost(ConstUrls.DmsSaveAsDocument, payloads).subscribe((res: any) => {
      if (res) {
        for (let i = 0; i < res.length; i++) {
          let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
          res[i].docType = docType;
          let dateUploaded = new Date(res[i].checkedinDate);
          let dateUploadedDisplay:string= this.convertDateToFormat("dd.mm.yyyy", dateUploaded);
          if (docType == constants.docType.referenceDoc) {
            if (this.workstepForm.referenceDocuments && this.workstepForm.referenceDocuments.length > 0) {
              this.workstepForm.referenceDocuments.push({
                documentName: res[i].fileName, documentNo: res[i].documentNo,
                documentUrlToken: res[i].urlToken,
                dateUploaded: dateUploaded,
                dateUploadedDisplay: dateUploadedDisplay,
                extension: extn,
                dmsRepoDetail: res[i]
              });
            }
            else {
              this.workstepForm.referenceDocuments = [{
                documentName: res[i].fileName,
                documentNo: res[i].documentNo,
                documentUrlToken: res[i].urlToken,
                dateUploaded: dateUploaded,
                dateUploadedDisplay: dateUploadedDisplay, 
                extension: extn,
                dmsRepoDetail: res[i]
              }];
            }
          }
          if (docType == constants.docType.workPaper) {

            if (this.workstepForm.workingPapers && this.workstepForm.workingPapers.length > 0) {
              this.workstepForm.workingPapers.push({
                documentName: res[i].fileName,
                documentNo: res[i].documentNo,
                documentUrlToken: res[i].urlToken,
                dateUploaded: dateUploaded,
                dateUploadedDisplay: dateUploadedDisplay, extension: extn, dmsRepoDetail: res[i]
              });
            }
            else {
              this.workstepForm.workingPapers = [{
                documentName: res[i].fileName,
                documentNo: res[i].documentNo,
                dmsRepoDetail: res[i],
                documentUrlToken: res[i].urlToken,
                dateUploaded: dateUploaded,
                dateUploadedDisplay: dateUploadedDisplay,
                extension: extn
              }];
            }
          }
        }
        this.title = "Success";
        this.content = "File Uploaded successfully...";
        this.myCommonAlert(this.title, this.content);
        this.closeSearch()
      }
    }, (error) => {
      this.myCommonAlert(error.massage, 'Error');

    })
    // this.editsearchDocument = false;
  }



  renameDocument(docData) {
    if (this.documentNumber == docData.documentNo && this.previewMode) {
      this.myCommonAlert("Error", "File is open for annotation. Please close it first.");
      return;
    }
    this.oldFileName = docData.fileName.split(".")[0];
    this.docNo = docData.documentNo;
    this.documentType = docData.docType
    this.renameFileExtension = docData.fileName.substr(
      docData.fileName.lastIndexOf(".")
    );

    this.comments = "";

    this.renameDoc = true;

  }
  //////////////////// File Rename /////////////////
  onRenameButton(newFileName) {
    this.upadateFileName();
    this.renameDoc = false;

  }
  countDotsInFileName(s1, letter) {
    return (s1.match(RegExp(letter, "g")) || []).length;
  }
  upadateFileName() {
    if (
      this.oldFileName.length == 0 ||
      this.countDotsInFileName(this.oldFileName, "\\.") > 0
    ) {
      this.myCommonAlert("Error", "Please enter valid file name.");
    } else {
      let fullFileName = this.oldFileName + this.renameFileExtension;
      let data = {
        dmsSessionId: localStorage.getItem("dmsToken"),
        documentNo: this.docNo,
        fileName: fullFileName,
        renameBy: localStorage.getItem("username"),
        comments: this.comments,
      };
      this.httpService
        .post(ConstUrls.renameFile, data)
        .subscribe((res: any) => {
          if (res) {
            if (this.documentType == constants.docType.referenceDoc) {
              var i = this.workstepForm.referenceDocuments.findIndex(w => w.dmsRepoDetail.documentNo == this.docNo);
              this.workstepForm.referenceDocuments[i].dmsRepoDetail.fileName = fullFileName;
              this.workstepForm.referenceDocuments[i].documentName = fullFileName;
            }
            if (this.documentType == constants.docType.workPaper) {
              var i = this.workstepForm.workingPapers.findIndex(w => w.dmsRepoDetail.documentNo == this.docNo);
              this.workstepForm.workingPapers[i].dmsRepoDetail.fileName = fullFileName;
              this.workstepForm.workingPapers[i].documentName = fullFileName;
            }
            this.title = "Success";
            this.content = "File Rename successfully...";
            this.myCommonAlert(this.title, this.content);
          }
        });
    }
  }



  globalSearchKeyPress() {
    this.dropdownListAll = [];
    if (this.globalSearchData.length >= 2) {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
      // this.dropdownList=[];

      let payload = {
        searchQuery: this.globalSearchData,
        auditId: this.auditId,
        moduleName: "Workstep-form",
        dmsSessionId: localStorage.getItem("dmsToken"),
      };
      this.httpService
        .securePost(ConstUrls.DmsFileSearch, payload)
        .subscribe((records: any) => {
          if (records) {
            this.arr = [];
            records.forEach((d) => {
              this.arr.push({ documentNo: d.documentNo, fileName: d.fileName });
            });
            this.dropdownListAll = this.arr;
            this.dropdownFirmList = this.arr;

          } else {
            this.dropdownListAll = [];
            this.dropdownFirmList = [];
          }
        });
    } else {
      this.dropdownListAll = [];
      this.dropdownFirmList = [];
    }
  }






  addnewFile() {
    this.selectedItems = [];
    this.searchDocument = true;
    this.onfileChangeOut = true;
    this.onfileChangeIn = false;
  }

  addnewFileOne() {
    this.selectedItems = [];
    this.searchDocument = true;
    this.onfileChangeOut = false;
    this.onfileChangeIn = true;
  }
  closeSearch() {
    this.searchDocument = false;
    this.editsearchDocument = false;
    this.globalSearchData = "";
  }
  closeModal(event) {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    $(".animation-save").removeClass('active-btn');
    $(".back-drop-container").removeClass("show-drop")
    this.searchDocument = false;

    this.position = this.position ? undefined : { x: 0, y: 0 };
    this.closePopup.emit(event);
    this.renameDoc = false;
  }

  clickYes() {
    $(".btn-infom.yes").addClass("d-none");
    $(".btn-infom.no").removeClass("d-none");
    // this.workstepForm.isIssue = false;
  }
  clickNo() {
    $(".btn-infom.yes").removeClass("d-none");
    $(".btn-infom.no").addClass("d-none");
    // this.workstepForm.isIssue = true;

  }

  /////////////////////////////////////////////// Prateek code ///////////////////////////////////////////
  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    let timer = title == "Alert" ? 6000 : 3000;
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "error-popup-design nobtnIn confirm-popup",

      backdrop: "static",
      keyboard: false,
    });
    setTimeout(() => {
      this.modalRef.hide();
    }, timer);
  }



  onFilechanged(event, docType) {

    var files = [];
    if (this.fileValidation()) {
      const formData: any = new FormData();
      files = event.target.files;
      formData.append("file", files[0], files[0]["name"]);
      formData.append("dmsSessionId", localStorage.getItem("dmsToken"));
      formData.append("userType", 'auditor');
      formData.append("checkedInBy", localStorage.getItem("username")),
        formData.append("metadata", "Module-Name=AgileDashboard,Feature-Name=Workstep,Doc-Type=DATAFILE,Sequence-No=100019"),
        formData.append("roomType", '');
      formData.append("routerLink", ""),
        formData.append("subModuleName", "workstep-form"),
        formData.append("auditYear", new Date().getFullYear()),
        formData.append("auditId", this.auditId),
        formData.append("auditName", this.auditableEntity),
        formData.append("moduleName", "AgileDashboard"),
        formData.append("checkedinDate", moment().format("L"));
        this.httpService.post(ConstUrls.DmsUpload, formData).subscribe((res: any) => {

        if (res) {
          // let refDoc=[];
          for (let i = 0; i < res.length; i++) {
            res[i].docType = docType;
            let extn = res[i].fileName.substr(res[i].fileName.lastIndexOf(".") + 1);
            let dateUploaded = new Date(res[i].checkedinDate);
            let dateUploadedDisplay:string= this.convertDateToFormat("dd.mm.yyyy", dateUploaded);
            if (docType == constants.docType.referenceDoc) {
              if (this.workstepForm.referenceDocuments && this.workstepForm.referenceDocuments.length > 0) {
                this.workstepForm.referenceDocuments.push({
                  documentName: res[i].fileName, documentNo: res[i].documentNo,
                  documentUrlToken: res[i].urlToken,
                  dateUploaded: dateUploaded,
                  dateUploadedDisplay: dateUploadedDisplay,
                  extension: extn,
                  dmsRepoDetail: res[i]
                });
              }
              else {
                this.workstepForm.referenceDocuments = [{
                  documentName: res[i].fileName,
                  documentNo: res[i].documentNo,
                  documentUrlToken: res[i].urlToken,
                  dateUploaded: dateUploaded,
                  dateUploadedDisplay: dateUploadedDisplay,
                  extension: extn,
                  dmsRepoDetail: res[i]
                }];
              }
            }
            if (docType == constants.docType.workPaper) {
              if (this.workstepForm.workingPapers && this.workstepForm.workingPapers.length > 0) {
                this.workstepForm.workingPapers.push({
                  documentName: res[i].fileName,
                  documentNo: res[i].documentNo,
                  documentUrlToken: res[i].urlToken,
                  dateUploaded: dateUploaded,
                  dateUploadedDisplay: dateUploadedDisplay,
                  extension: extn,
                  dmsRepoDetail: res[i]
                });
              }
              else {
                this.workstepForm.workingPapers = [{
                  documentName: res[i].fileName,
                  documentNo: res[i].documentNo,
                  documentUrlToken: res[i].urlToken,
                  dateUploaded: dateUploaded,
                  dateUploadedDisplay: dateUploadedDisplay,
                  extension: extn,
                  dmsRepoDetail: res[i]
                }];
              }
            }
          }
          this.title = "Success";
          this.content = "File Uploaded successfully...";
          this.myCommonAlert(this.title, this.content);
          this.closeSearch()
        }
      }
        , (error) => {
          this.myCommonAlert('Error', error.message);
        })
    } else {
      this.myCommonAlert('Error', 'Invalid file');
      $("#myFile").val('');
    }
  }

  downloadFile(fileUrl, filename) {
    fetch(fileUrl)
      .then(resp => resp.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
        // alert('your file has downloaded!'); // or you know, something with better UX...
      })
      .catch(() =>
        alert('oh no!')
      );
  }
  deleteFile(index, simple, fileObj) {
    if (this.documentNumber == fileObj.documentNo && this.previewMode) {
      this.myCommonAlert("Error", "File is open for annotation. Please close it first.");
      return;
    }
    const initialState = {
      title: "Confirmation",
      content: `Do you want to Delete current File?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "error-popup-design confirm-popup",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {

      if (r) {
        let payload = {
          dmsSessionId: localStorage.getItem("dmsToken"),
          documentNo: fileObj.documentNo,
          fileName: fileObj.documentName,
          deletedBy: localStorage.getItem("username"),
          comments: `file deleted by ${localStorage.getItem("username")}`,
          _id: fileObj.dmsRepoDetail._id
        };
        this.httpService
          .post(ConstUrls.deleteDocument, payload)
          .subscribe((res: any) => {
            if (res.errorCode == 0) {
              if (simple == 'reference') {
                this.workstepForm.referenceDocuments.splice(index, 1)
                this.title = "Success";
                this.content = "File Deleted successfully...";
                this.myCommonAlert(this.title, this.content);
              }
              if (simple == 'workingpaper') {
                this.workstepForm.workingPapers.splice(index, 1)
                this.title = "Success";
                this.content = "File Deleted successfully...";
                this.myCommonAlert(this.title, this.content);
              }
            } else {
              this.myCommonAlert("Error", res.message);
            }
          })
      }
    });
  }











  fileValidation() {
    let fileInput = $("#myFile").val().toString();
    let extension = '.' + fileInput.split(".")[1];
    var allowedExtensions = /(\.xlsx|\.jpeg|\.jpg|\.pptx|\.docx|\.tif|\.pdf|\.png)$/i;
    if (allowedExtensions.exec(extension)) {
      return true;
    } else {

      return false;
    }
  }

  hasIssue(data) {
    this.workstepForm.hasIssues = data;
    this.showIssueError = false;
  }

  saveForm() {
    $(".animation-save").addClass('active-btn');
  }

  closeRenameDoc() {
    this.docNo = "";
    this.renameFileName = "";
    this.renameFileExtension = "";
    this.comments = "";
    this.renameDoc = false;
    this.checkInBy = '';
  }

  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        ext.extension = extn;
      }
    }
    for (const droppedFile of files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
        });
      } else {
      }
    }
  }

  issueActiondatatable() {
    $("#issueActionTable").DataTable({
      fixedHeader: true,
      autoWidth: false,
      scrollX: false,
      scrollCollapse: true,
      bPaginate: true,
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]
      ],
      columnDefs: [
        {
          targets: 3,
          orderable: false,
        },
      ],
      destroy: true,
      language: {
        searchPlaceholder: "Search",
      },
      bInfo: true,
      searching: true,
      oLanguage: {
        sSearch: ""
      }
    });
  }

  issueActionSelecteddatatable() {
    this.chRef.detectChanges();
    const issueSelectedTable: any = $("#issueActionSelectedTable");
    const issueListSelectedTableCM = this.dataTableService.initializeTable(
      issueSelectedTable,
      `issueActionSelectedAgile`
    );
    this.dataTableFuncService.selectAllRows(
      issueListSelectedTableCM,
      `issueActionSelected-select-all`
    );
    this.dataTableFuncService.expandContainer(issueListSelectedTableCM);
    $("#issueActionSelectedTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#issueActionSelected-select-all").prop("checked", false);
          var el = $("#issueActionSelected-select-all").get(0);
          // if (el && el.checked && "indeterminate" in el) {
          //   el.indeterminate = true;
          // }
        }
        if (
          $("#issueActionSelectedTable tr td input[type='checkbox']:checked").length ==
          $("#issueActionSelectedTable tr td input[type='checkbox']").length
        ) {
          $("#issueActionSelected-select-all").prop("checked", true);
        }
      }
    );

  }

  rejetWorkStep() {
    this.errorTarget = '';
    const initialState = {
      title: "Confirm Reject WorkStep",
      content: `WS-102`,
      link: "Yes, Reject the workstep",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmRejectWorkstepComponent, {
      initialState,
      class: "error-popup-design extra-popup",
      backdrop: "static",
      keyboard: false,
    });

    this.modalRef.content.onClose.subscribe(r => {
      if (r && r.length > 0) {
        this.workstepForm.reviewer.status = 'rejected';
        this.workstepForm.preparer.status = 'assigned';
        this.workstepForm.rejectionReason = r;
        this.workstepForm.reviewer.reviewedOn = new Date();
        //update
        this.httpService.securePut(ConstUrls.updateWorkStep, this.workstepForm).subscribe(
          res => {
            this.closeModal(true);
          },
          err => {
            this.myCommonAlert('Error', err.error.message);
          }
        );
      }
    })
  }
  restrictSpace(s) {
    let arr = s.split(" "); //&#160;
    if(arr.length != 1){
      arr.pop();
    }
    return arr.every((value => value == "&#160;"))
  }
  saveWorkStep(status) {
    this.errorTarget = '';
    // if (this.workstepForm.workingPapers && this.workstepForm.workingPapers.length > 0) {
    //   for (let p = 0; p < this.workstepForm.workingPapers.length; p++) {
    //     if (!this.workstepForm.workingPapers[p].dmsRepoDetail.workPaperNumber) {
    //       this.myCommonAlert("Alert", "Please convert all document to workapaper inside working paper section..")
    //       return;
    //     }
    //   }
    // }

    if (this.isNew) {
      if (!this.workstepForm.workStepName) {
        this.errorTarget = 'workStepName';
        return;
      } else if (this.workstepForm.workStepName.trim().length == 0) {
        this.errorTarget = 'workStepName';
        return;
      } else if (!this.workstepForm.taskDescription) {
        this.errorTarget = 'taskDescription';
        return;
      } else if (this.restrictSpace(this.workstepForm.taskDescription)) {
        this.errorTarget = 'taskDescription';
        return;
      }
    }
    status != 'wip' ? $(".animation-submit").addClass('active-btn') : $(".animation-save").addClass('active-btn');
    if (this.isAuditor) {
      this.workstepForm.preparer.status = status;
      if (status == 'completed') {
        this.workstepForm.reviewer.status = 'approval';
        this.workstepForm.preparer.preparedOn = new Date();
      }
    }
    else {
      this.workstepForm.reviewer.status = status;
      if (this.selectedPreparer && this.selectedPreparer.username && this.selectedPreparer.username.length > 0) {
        this.workstepForm.preparer = this.selectedPreparer;
        this.workstepForm.preparer.status = '';
      }
      if (status == 'sent') {
        this.workstepForm.preparer.status = 'assigned';
      }
      if(status == 'completed'){
        this.workstepForm.reviewer.reviewedOn = new Date();
      }
    }
    if (this.workstepId) {
      //update
      this.httpService.securePut(ConstUrls.updateWorkStep, this.workstepForm).subscribe(
        res => {
          this.closeModal(true);
        },
        err => {
          this.myCommonAlert('Error', err.error.message);
          $(".animation-save").removeClass('active-btn');
          $(".animation-submit").removeClass('active-btn');
        }
      );
    }
    else {
      //save
      this.httpService.securePost(ConstUrls.createWorkStep, this.workstepForm).subscribe(
        res => {
          this.closeModal(true);
        },
        err => {
          this.myCommonAlert('Error', err.error.message);
          $(".animation-save").removeClass('active-btn');
          $(".animation-submit").removeClass('active-btn');
        }
      );
    }

    // this.closeModal();
  }

  submitWorkStep() {
    this.errorTarget = '';
    if (this.workstepForm.workingPapers && this.workstepForm.workingPapers.length > 0) {
      for (let p = 0; p < this.workstepForm.workingPapers.length; p++) {
        if (!this.workstepForm.workingPapers[p].dmsRepoDetail.workPaperNumber) {
          this.myCommonAlert("Alert", "Please convert all document to workapaper inside working paper section..")
          return;
        }
      }
    }

    //is new
    if (this.isNew) {
      // //audit lead
      // if (!this.isAuditor) {

      // }
      // //auditor
      // else {

      // }
      if (!this.isAuditor) {
        if ((!this.workstepForm.preparer.username || this.workstepForm.preparer.username.length <= 0) && !this.selectedPreparer.username) {
          
          this.errorTarget = 'preparerList';
          return;
        }
        else {
          this.saveWorkStep('sent')
        }
      }
      else {

        this.saveWorkStep('completed');
      }

    }
    // not new
    else {
      //audit lead
      if (!this.isAuditor) {
        if ((!this.workstepForm.preparer.username || this.workstepForm.preparer.username.length <= 0) && !this.selectedPreparer.username) {
          this.errorTarget = 'preparerList';
          return;
        }
        this.saveWorkStep('sent');
      }
      else {
        //auditor
        if (!this.workstepForm.workPerformed) {
          this.errorTarget = 'workPerformed'
          return;
        } else if (this.restrictSpace(this.workstepForm.workPerformed)) {
          this.errorTarget = 'workPerformed'
          return;
        }
        this.workstepForm.preparer.status = 'completed';
        this.workstepForm.preparer.preparedOn = new Date();
        this.workstepForm.reviewer.status = 'approval';
        $(".animation-submit").addClass('active-btn');
        this.httpService.securePut(ConstUrls.updateWorkStep, this.workstepForm).subscribe(
          res => {
            this.closeModal(true);
          },
          err => {
            this.myCommonAlert('Error', err.error.message);
            $(".animation-save").removeClass('active-btn');
            $(".animation-submit").removeClass('active-btn');
          }
        );
      }
    }
    // this.closeModal();
  }

  approveWorkStep() {
    this.errorTarget = '';
    if (!this.workstepForm.summary) {
      this.errorTarget = 'summary'
      return;
    } else if (this.restrictSpace(this.workstepForm.summary)) {
      this.errorTarget = 'summary'
      return;
    }
    $(".animation-approve").addClass('active-btn');
    this.workstepForm.reviewer.status = 'completed';
    this.workstepForm.reviewer.reviewedOn = new Date();
    //update
    this.httpService.securePut(ConstUrls.updateWorkStep, this.workstepForm).subscribe(
      res => {
        this.closeModal(true);
      },
      err => {
        this.myCommonAlert('Error', err.error.message);
        $(".animation-approve").removeClass('active-btn');
      }
    );
  }
}

export class WorkStepModel {
  constructor() {
    this.preparer = {
      username: null,
      profileUrl: null,
      preparedOn: null,
      status: ""
    };

    this.reviewer = {
      username: null,
      profileUrl: null,
      reviewedOn: null,
      status: ""
    }
    this.hasIssues = false;
    this.origin = 'create';
  }
  workStepId: string;
  workStepName: string;
  hasIssues: Boolean;
  auditId: string;
  preparer: {
    username: string,
    profileUrl: string,
    preparedOn: Date,
    status: string
  };
  reviewer: {
    username: string,
    profileUrl: string,
    reviewedOn: Date,
    status: string
  };
  taskDescription: string = "";
  workPerformed: string = "";
  referenceDocuments: [
    {
      documentUrlToken: string,
      documentName: string,
      dateUploaded: Date,
      dateUploadedDisplay: string,
      documentNo: string,
      extension: string,
      dmsRepoDetail: any
    }
  ];
  workingPapers: [
    {
      documentUrlToken: string,
      documentName: string,
      dateUploaded: Date,
      dateUploadedDisplay: string,
      documentNo: string,
      extension: string,
      dmsRepoDetail: any
    }
  ]
  rejectionReason: string;
  summary: string;
  origin: string;
  createdBy: string;
  updatedBy: string;
  createdAt: Date;
  updatedAt: Date;
}