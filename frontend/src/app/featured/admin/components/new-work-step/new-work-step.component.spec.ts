import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWorkStepComponent } from './new-work-step.component';

describe('NewWorkStepComponent', () => {
  let component: NewWorkStepComponent;
  let fixture: ComponentFixture<NewWorkStepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewWorkStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWorkStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
