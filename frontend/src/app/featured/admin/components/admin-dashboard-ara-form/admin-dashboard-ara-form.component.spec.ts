import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDashboardAraFormComponent } from './admin-dashboard-ara-form.component';

describe('AdminDashboardAraFormComponent', () => {
  let component: AdminDashboardAraFormComponent;
  let fixture: ComponentFixture<AdminDashboardAraFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDashboardAraFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardAraFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
