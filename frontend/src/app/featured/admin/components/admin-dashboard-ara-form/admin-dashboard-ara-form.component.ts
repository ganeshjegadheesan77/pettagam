import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";

declare var $;
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";

@Component({
  selector: "app-admin-dashboard-ara-form",
  templateUrl: "./admin-dashboard-ara-form.component.html",
  styleUrls: ["./admin-dashboard-ara-form.component.less"],
})
export class AdminDashboardAraFormComponent implements OnInit {
  @Input("editassessData") editassessData: any;
  @Output() closedEditAssessForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  level: string;
  position;
  impactBtnColor: string;
  likliBtnColor: string;
  result: number;
  overallRiskScore: number;
  overallRiskScoreBtn: string;
  RAauditName: any;
  riskLevelName: any;
  auditRecord: any = [];
  issueList: any;
  rotationYr: any;
  aftrYr: any;
  siteName: any;
  previousYrData: any;

  auditData: any = [];
  assessmentForm: FormGroup;
  modalRef: BsModalRef;
  impact1: any;
  impact2: any;
  overallImpact: any;

  liklihood1: any;
  liklihood2: any;
  overallLikelihood: any;
  title: string;
  content: string;

  constructor(
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private formBuilder: FormBuilder,
    private httpservice: HttpService
  ) {}
  name: any;
  ngOnInit() {
    if (this.editassessData) {
      console.log("(********)", this.editassessData);
      this.rotationYr = this.editassessData.rotation;
      let currYear = new Date().getFullYear();
      this.aftrYr = parseInt(this.rotationYr) + currYear;
      this.name = this.editassessData.name.split("-")[0];
      this.previousYrData = this.editassessData.oldAuditIds;
      this.getIssueList();
      this.getOldAuditRecords();
    }
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });

    this.level = "";
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.impactBtnColor = "btn low";
    // this.overallImpact = 0;
    this.likliBtnColor = "btn low";
    // this.overallLikelihood = 0;
    // this.overallRiskScore = 0;
    this.overallRiskScoreBtn = "btn low";
    this.getARAData();
    // this.assessmentForm.patchValue({
    //   impact_financialRisk: 0,
    //   impact_complianceRisk: 0,
    //   likelihood_internalControls: 0,
    //   likelihood_processRisk: 0,
    // });
  }

  getARAData() {
    if (this.editassessData) {
      let payload = { araId: this.editassessData.riskAssessment };

      this.httpservice
        .securePost(ConstUrls.getARADetails, payload)
        .subscribe((res: any) => {
          if (res) {
            this.overallImpact = res.impact_overall;
            this.overallLikelihood = res.likelihood_overall;

            this.calculateRiskScore();
            this.fetchButtonColor();
            this.assessmentForm.patchValue({
              id: this.editassessData.id,
              parent: this.editassessData.parent,
              overallRiskScore: parseInt(res.overallRiskScore),
              auditId: res.auditId,
              araId: this.editassessData.assessId,
              impact_financialRisk: res.impact_financialRisk,
              impact_complianceRisk: res.impact_complianceRisk,
              // impact_overall: ["", []],
              likelihood_processRisk: res.likelihood_processRisk,
              likelihood_internalControls: res.likelihood_internalControls,
              likelihood_overall: res.likelihood_overall,
            });
          }
        });
    }
  }

  getIssueList() {
    if (this.editassessData) {
      let payload = { auditId: this.editassessData.currentAudit };
      this.issueList = [];
      this.httpservice
        .securePost(ConstUrls.getIssueDetailsForARA, payload)
        .subscribe((resp: any) => {
          if (resp) {
            this.issueList = resp[0].IssueData;
            if (this.issueList.length == 0) this.issueList = [];
          }
        });
    }
    //getIssueDetailsForARA
  }

  getOldAuditRecords() {
    if (this.editassessData && this.editassessData.oldAuditIds.length > 0) {
      let payload = { auditId: this.editassessData.currentAudit };

      this.httpservice
        .securePost(ConstUrls.getOldAuditsRecord, payload)
        .subscribe((res: any) => {
          if (res) {
            this.previousYrData = res;
          }
        });
    } else {
      this.previousYrData = [];
    }
  }

  initForm() {
    this.assessmentForm = this.formBuilder.group({
      auditId: ["", []],
      araId: ["", []],
      id: ["", []],
      parent: ["", []],
      auditName: [this.RAauditName, []],
      riskLevel: [this.riskLevelName, []],
      riskAssessmentId: ["", []],
      leadAuditor: ["", []],
      impact_financialRisk: ["", []],
      impact_complianceRisk: ["", []],
      impact_overall: ["", []],
      likelihood_processRisk: ["", []],
      likelihood_internalControls: ["", []],
      likelihood_overall: ["", []],
      overallRiskScore: ["", []],
      riskAssessmentSubmitter: ["", []],
      riskAssessmentSubmitID: ["", []],
      riskAssessmentModifiedBy: ["", []],
      riskAssessmentApprovedBy: ["", []],
      riskAssessmentIdentifiedOn: ["", []],
      riskAssessmentApprovedOn: ["", []],
      riskAssessedOn: ["", []],
      riskModifiedOn: ["", []],
    });
  }

  impact1Blur() {
    if (!this.assessmentForm.get("impact_financialRisk").value) {
      this.impact1 = 0;
    }
  }

  impact2Blur() {
    if (!this.assessmentForm.get("impact_complianceRisk").value) {
      this.impact2 = 0;
    }
  }

  likli1Blur() {
    if (!this.assessmentForm.get("likelihood_processRisk").value) {
      this.liklihood1 = 0;
    }
  }

  likli2Blur() {
    if (!this.assessmentForm.get("likelihood_internalControls").value) {
      this.liklihood2 = 0;
    }
  }

  onImpact1Change(searchValue: number): void {
    //Onchange Impact 1
    if (isNaN(searchValue)) {
      this.impact1 = parseFloat("0.0");
    } else if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.impact1 = parseFloat(searchValue.toString()).toFixed(1);
      } else {
        this.impact1 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.impact1 = 0;
      this.assessmentForm.patchValue({ impact_financialRisk: this.impact1 });
    }

    this.calculateOverallImpact();
  }

  onImpact2Change(searchValue: number): void {
    //Onchange Impact 2
    if (isNaN(searchValue)) {
      this.impact2 = parseFloat("0.0");
    } else if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.impact2 = parseFloat(searchValue.toString()).toFixed(1);
      } else {
        this.impact2 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.impact2 = 0;
      this.assessmentForm.patchValue({ impact_complianceRisk: this.impact2 });
    }

    this.calculateOverallImpact();
  }

  onLikeli1Change(searchValue: number): void {
    //Onchange Likely 1
    if (isNaN(searchValue)) {
      this.liklihood1 = parseFloat("0.0");
    } else if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.liklihood1 = parseFloat(searchValue.toString()).toFixed(1);
      } else {
        this.liklihood1 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.liklihood1 = 0;
      this.assessmentForm.patchValue({
        likelihood_processRisk: this.liklihood1,
      });
    }

    this.calculateOverallLikelihood();
  }

  onLikeli2Change(searchValue: number): void {
    //Onchange Likely 2
    if (isNaN(searchValue)) {
      this.liklihood2 = parseFloat("0.0");
    } else if (searchValue <= 10) {
      if (searchValue % 1 === 0) {
        this.liklihood2 = parseFloat(searchValue.toString()).toFixed(1);
      } else {
        this.liklihood2 = parseFloat(searchValue.toString()).toFixed(1);
      }
    } else {
      this.liklihood2 = 0;
      this.assessmentForm.patchValue({
        likelihood_internalControls: this.liklihood2,
      });
    }

    this.calculateOverallLikelihood();
  }

  calculateOverallLikelihood() {
    if (isNaN(this.liklihood1)) {
      this.liklihood1 = 0;
    }
    if (isNaN(this.liklihood2)) {
      this.liklihood2 = 0;
    }
    if (this.liklihood1 > this.liklihood2) {
      this.overallLikelihood = 0;
      this.overallLikelihood = this.liklihood1;
    } else if (this.liklihood1 <= this.liklihood2) {
      this.overallLikelihood = 0;

      this.overallLikelihood = this.liklihood2;
    } else {
      this.overallLikelihood = 0;
    }
    this.fetchButtonColor();
    this.calculateRiskScore();
  }

  calculateOverallImpact() {
    if (isNaN(this.impact1)) {
      this.impact1 = 0;
    }
    if (isNaN(this.impact2)) {
      this.impact2 = 0;
    }
    if (this.impact1 > this.impact2) {
      this.overallImpact = 0;
      this.overallImpact = this.impact1;
    } else if (this.impact1 <= this.impact2) {
      this.overallImpact = 0;

      this.overallImpact = this.impact2;
    } else {
      this.overallImpact = 0;
    }
    this.fetchButtonColor();
    this.calculateRiskScore();
  }

  calculateRiskScore() {
    let num1 = parseFloat(this.overallImpact);
    let num2 = parseFloat(this.overallLikelihood);

    this.result = parseFloat((num1 + num2).toFixed(2));
    if (this.result <= 7) {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn low";
      // console.log(this.overallRiskScore);
    } else if (this.result >= 8 && this.result <= 14) {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn medium";
      // console.log(this.overallRiskScore);
    } else {
      this.overallRiskScore = this.result;
      this.overallRiskScoreBtn = "btn high";
      // console.log(this.overallRiskScore);
    }
  }

  fetchButtonColor() {
    if (this.overallImpact >= 0 && this.overallImpact <= 3) {
      this.impactBtnColor = "btn low";
    } else if (this.overallImpact > 3 && this.overallImpact <= 6) {
      this.impactBtnColor = " btn medium";
    } else {
      this.impactBtnColor = " btn high";
    }

    if (this.overallLikelihood >= 0 && this.overallLikelihood <= 3) {
      this.likliBtnColor = "btn low";
    } else if (this.overallLikelihood > 3 && this.overallLikelihood <= 6) {
      this.likliBtnColor = " btn medium";
    } else {
      this.likliBtnColor = " btn high";
    }
  }

  myCommonAlert(title, content) {
    const initialState = { title: title, content: content, link: "Ok" };
    this.modalRef = this.modalService.show(DefaultModalComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
  }

  onSave() {
    if (
      this.editassessData &&
      this.editassessData.id != "" &&
      this.editassessData.parent != ""
    ) {
      this.assessmentForm.patchValue({
        id: this.editassessData.id,
        parent: this.editassessData.parent,
        overallRiskScore: this.overallRiskScore,
        auditId: this.editassessData.currentAudit,
        araId: this.editassessData.assessId,
        impact_overall: this.overallImpact,
        likelihood_overall: this.overallLikelihood,
      });

      this.httpservice
        .securePost(ConstUrls.updateARADetails, this.assessmentForm.value)
        .subscribe((res: any) => {
          if (res) {
            this.title = "Success";
            this.content =
              this.editassessData.assessId + "updated successfully";
            this.myCommonAlert(this.title, this.content);
            this.closeModal();
          }
        });
    }
  }
  closeModal() {
    this.closedEditAssessForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
    // this.copyAuditForm.tabs[0].active = true;
  }
}
