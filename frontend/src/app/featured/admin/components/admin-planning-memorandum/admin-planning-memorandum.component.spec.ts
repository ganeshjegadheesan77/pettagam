import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPlanningMemorandumComponent } from './admin-planning-memorandum.component';

describe('AdminPlanningMemorandumComponent', () => {
  let component: AdminPlanningMemorandumComponent;
  let fixture: ComponentFixture<AdminPlanningMemorandumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPlanningMemorandumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPlanningMemorandumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
