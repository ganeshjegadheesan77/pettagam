import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input,
  SimpleChanges,
} from "@angular/core";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import {
  NgxFileDropEntry,
  FileSystemFileEntry,
  FileSystemDirectoryEntry,
} from "ngx-file-drop";
import { environment } from "src/environments/environment";
import { MalihuScrollbarService } from "ngx-malihu-scrollbar";
import * as moment from "moment";
import { HttpService } from "src/app/core/http-services/http.service";
import { AdminService } from "../../admin.service";
// import { cpus } from 'os';

declare var $;

@Component({
  selector: "app-edit-pbc-form",
  templateUrl: "./edit-pbc-form.component.html",
  styleUrls: ["./edit-pbc-form.component.less"],
})
export class EditPbcFormComponent implements OnInit, OnChanges {
  position;
  PBCForm: FormGroup;
  modalRef: BsModalRef;
  multiFile: any = [];
  fetchedData: any;
  mixArr: any;
  preparerArr: any[];
  reviewerArr: any[];
  uri = environment.baseUrl;
  uploadedFilesArr;
  attachedFiles: any = [];
  @Input("PBCFormData") PBCFormData: any;

  @Output() closedPBCForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  PBCName: any;
  PBCId: any;
  marked: boolean;
  openAttachment: boolean;
  selectAudit: string;
  myData: any;
  fAssignee: string[] = [];
  fapAssignee : string[] = [];
  fieldPanes: any;
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private adminService: AdminService
  ) {}

  ngOnInit() {
    this.mScrollbarService.initScrollbar(".scroll-container", {
      axis: "y",
      theme: "metro",
    });
    this.initForm();
    this.fetchedData = [];
    this.openAttachment = false;
    console.log("Data from main Page: ", this.PBCFormData);
    this.getPBC();
    this.getFieldWorkData();
  }
  ngOnChanges() {
    this.initForm();
  }

  getFieldWorkData() {
    if (JSON.parse(localStorage.getItem("fieldWorkCommonData"))) {
      console.log("fordata-->", JSON.parse(localStorage.getItem("fieldWorkCommonData")));

      this.PBCForm.patchValue(
        JSON.parse(localStorage.getItem("fieldWorkCommonData"))
      );
    }
  }

  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // attachment selected
      this.openAttachment = false;
      //console.log('left curr status:' , this.marked)
    } else {
      this.openAttachment = true;
      //console.log('selected curr status:' , this.marked)
    }
  }
  public files: NgxFileDropEntry[] = [];

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    this.uploadedFilesArr = this.files;
    for (let ext of this.uploadedFilesArr) {
      if (ext.relativePath) {
        // console.log('relative: ' , ext.relativePath);
        var extn = ext.relativePath.substr(
          ext.relativePath.lastIndexOf(".") + 1
        );
        var name = ext.relativePath.substr(
          0,
          ext.relativePath.lastIndexOf(".")
        );
        ext.relativePath = name.replace(/\W+/g, "-") + "." + extn;
        //  let extension = ext.relativePath.split('.')[1];
        ext.extension = extn;

        //console.log('Updated File Name:' ,ext.relativePath , 'File Extension:' , ext.extension);
      }
      // if(ext.relativePath){
      //   // console.log('relative: ' , ext.relativePath);
      //    let extension = ext.relativePath.split('.')[1];
      //     ext.extension = extension;
      // }
    }
    //console.log('upload files array :  ' , this.uploadedFilesArr);
    //console.log('files array: ' , this.files);
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.multiFile.push(file);
          let extn = file.name.substr(file.name.lastIndexOf(".") + 1);
          let name = file.name.substr(0, file.name.lastIndexOf("."));

          this.attachedFiles.push({
            relativePath: name.replace(/\W+/g, "-") + "." + extn,
            extension: extn,
            fullName: file.name,
            new: true,
          });
          //console.log("relativepath:" , droppedFile.relativePath,"file:", file);
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        //console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  getPBC() {
    if (this.PBCFormData) {
      console.log("ID:", this.PBCFormData);
      let payload = { auditPbc_id: this.PBCFormData.id };

      this.httpservice
        .securePost(ConstUrls.getPBCDetails, payload)
        .subscribe((res: any) => {
          if (res) {
            let load = {auditId:localStorage.getItem('ID')};
            this.httpservice.securePost(ConstUrls.getTeamForPbcAndFW , load).subscribe((resp:any)=>{
              console.log('resp', resp) 
               //Audit team code
               this.fAssignee = resp[0].resourePlanningData[0].fAssignee;
              this.fapAssignee = resp[0].resourePlanningData[0].fapAssignee;
             this.mixArr = resp[0].resourePlanningData[0];
             console.log('mixArr', this.mixArr)
             let newArr =[];
             this.preparerArr=[];
             this.reviewerArr=[];
              if(this.mixArr){
                this.mixArr.fAssignee.forEach(ele => {
                  this.preparerArr.push(ele);
                });
                
                this.mixArr.fapAssignee.forEach(ele => {
                  this.reviewerArr.push(ele);
                });
              }
            })
            this.myData = res[0];
            console.log("PBC Data: ", res);
            this.PBCName = this.myData.apbc_name;
            this.PBCId = this.myData.auditPbc_id;
            this.PBCForm.patchValue(this.myData);
            this.fieldPanes = this.myData.masterData[0];
            if (
              !moment(
                this.fieldPanes.performancePeriod[0],
                "DD-MM-YYYY",
                true
              ).isValid() &&
              this.fieldPanes.performancePeriod.length > 0
            ) {
              let d1 = moment(this.fieldPanes.performancePeriod[0]).format(
                "DD-MM-YYYY"
              );
              let d2 = moment(this.fieldPanes.performancePeriod[1]).format(
                "DD-MM-YYYY"
              );
              let str = d1 + "-" + d2;
              this.PBCForm.patchValue({ performance_period: str });
            } else if (this.fieldPanes.performancePeriod.length == 0) {
              this.PBCForm.patchValue({
                performance_period: this.fieldPanes.performancePeriod,
              });
            } else {
              let d1 = this.fieldPanes.performancePeriod[0];
              let d2 = this.fieldPanes.performancePeriod[1];
              let str = d1 + "-" + d2;
              this.PBCForm.patchValue({ performance_period: str });
            }
            console.log("PBCForm", this.PBCForm);
            this.fetchedData = res[0];
            //console.log('Attchment ??', this.fetchedData.apbc_attachments)
            if (this.fetchedData.apbc_attachments.length != 0) {
              this.openAttachment = true;
              for (
                let i = 0;
                i < this.fetchedData.apbc_attachments.length;
                i++
              ) {
                let extn = this.fetchedData.apbc_attachments[i].substr(
                  this.fetchedData.apbc_attachments[i].lastIndexOf(".") + 1
                );
                let name = this.fetchedData.apbc_attachments[i].substr(
                  0,
                  this.fetchedData.apbc_attachments[i].lastIndexOf(".")
                );

                this.attachedFiles.push({
                  relativePath: name,
                  extension: extn,
                  fullName: name + "." + extn,
                  new: false,
                });
              }
            } else {
              this.openAttachment = false;
            }
            //console.log('ATTACHEMENT ARRAY: '  , this.attachedFiles);
            // this.fieldWorkForm.patchValue(JSON.parse(localStorage.getItem('fieldWorkCommonData')));
          }
        });
    }
  }

  upload() {
    const formData: any = new FormData();
    const data: Array<File> = this.multiFile;
    for (let i = 0; i < data.length; i++) {
      formData.append("file", data[i], data[i]["name"]);
      if (this.fetchedData.apbc_attachments) {
        this.fetchedData.apbc_attachments.push(data[i]["name"]);
      } else {
        this.fetchedData.apbc_attachments = [];
        this.fetchedData.apbc_attachments.push(data[i]["name"]);
      }
    }
    let arr = [];
    arr = this.fetchedData.apbc_attachments;
    this.PBCForm.patchValue({ apbc_attachments: arr });
    //console.log('Form Data : ' , this.PBCForm.value);
    console.log("filearray", arr.toString());
    let filearr = [];
    arr.forEach((ele) => {
      filearr.push("fieldWork/" + ele);
    });
    let payload = {
      collection: "document",
      module: "fieldWork",
      "sub-module": "audit-announcement",
      tab: "audit-announcement",
      "router-link": "/rcm/internal-audit/engagement-planning",

      file: filearr.toString(),
      metadata:
        "Module-Name=RCM,Feature-Name=RISK_LIB,Doc-Type=DATAFILE,Sequence-No=100019",
      username: localStorage.getItem("username"),
      database: environment.db,
    };

    console.log("check", payload);
    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/put", payload)
      .subscribe((files: any) => {
        //console.log('Uploaded');
        if (files) {
          console.log("Response", files);
        }
        this.multiFile = [];
      });

    this.httpservice
      .securePost("uploadFieldWork", formData)
      .subscribe((files) => {
        //console.log('Uploaded');
        this.multiFile = [];
      });
  }
  // removeAttachment(removeitem, index) {
  //   if (removeitem.new) {
  //     const fileindex = parseInt(index);
  //     this.attachedFiles.splice(fileindex, 1);
  //   } else {
  //     const fileindex = parseInt(index);
  //     let payload = {
  //       directory: "uploads/fieldWork",
  //       fullName: removeitem.fullName,
  //     };
  //     this.httpservice
  //       .securePost("removeAttachment", payload)
  //       .subscribe((response: any) => {
  //         if (response) {
  //           /*console.log(
  //             "attached files array",
  //             this.attachedFiles,
  //             "index from method",
  //             fileindex
  //           );*/
  //           this.attachedFiles.splice(fileindex, 1);

  //           this.fetchedData.attachment = [];
  //           this.attachedFiles.forEach((x) => {
  //             this.fetchedData.push(x.fullName);
  //           });
  //           // console.log(this.fetchedData.attachment);
  //           let payload = {
  //             actionId: this.PBCFormData,
  //             fileName: removeitem.fullName,
  //           };
  //           this.httpservice
  //             .securePost(ConstUrls.deleteAIFile, payload)
  //             .subscribe((response: any) => {
  //               if (response) {
  //                 // console.log("yessss");
  //               }
  //             });
  //           setTimeout(function () {
  //             // console.log(this.attachedFiles);
  //           }, 3000);
  //           if (response) {
  //             const initialState = {
  //               title: "Success",
  //               content: "Attachment Deleted Successfully",
  //               link: "Ok",
  //             };
  //             this.modalRef = this.modalService.show(DefaultModalComponent, {
  //               initialState,
  //               class: "success-class",
  //               backdrop: "static",
  //               keyboard: false,
  //             });
  //             // this.closedAddDefinationActionForm.emit(true);
  //             // this.closeModal();
  //           }
  //         }
  //       });
  //   }

  //   //  /api/removeAttachment
  // }

  getDocument(file) {
    if (file.new) {
      const initialState = {
        title: "Alert",
        content: "Please save the form to view the attachment loaded.",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    } else {
      window.open(
        this.uri + "uploads/fieldWork/" + file.fullName,
        "_blank",
        "toolbar=no,scrollbars=yes,resizable=yes,top=80,left=500,width=800,height=400"
      );
    }
    let payload = {
      database: environment.db,
      file: "fieldWork/" + file.fullName,
    };

    this.httpservice
      .secureJavaPostReportApi("DocumentManagementService/get", payload)
      .subscribe((files: any) => {
        //  console.log('Uploaded');
        if (files) {
          console.log("pbc Response", files);
        }
        this.multiFile = [];
      });
  }

  initForm() {
    this.PBCForm = this.formBuilder.group({
      performance_period: ["", []],
      master_audit_id: ["", []],
      apbc_id: ["", []],
      apbc_name: ["", []],
      auditPbc_id: ["", []],
      apbc_audit_pgm_id: ["", []],
      apbc_preparer: ["", []],
      apbc_reviewer: ["", []],
      apbc_signoff_period: ["", []],
      apbc_audit_pgm_title: ["", []],
      apbc_work_step: ["", []],
      apbc_status: ["", []],
      apbc_document_req: ["", []],
      apbc_attachments: ["", []],
      apbc_created_by: ["", []],
      apbc_updated_by: ["", []],
      status: ["", []],
      approverStatus : [],
      assigneeStatus : []
    });
  }

  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    let currentUser = localStorage.getItem("username");

    //console.log('UserName: ' , currentUser);
    //console.log('FORM DATA: ' , this.PBCForm.value);

    let apbc_signoff_period = moment(
      this.PBCForm.get("apbc_signoff_period").value
    ).format("DD/MM/YYYY");
    if (apbc_signoff_period == "Invalid date") apbc_signoff_period = "-";
    console.log("data" ,{
      apbc_created_by: currentUser,
      auditPbc_id: this.PBCFormData.id,
      apbc_signoff_period: apbc_signoff_period,
      status: "wip",
      assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditPbc[0].assigneeStatus ,
      approverStatus : this.fapAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditPbc[0].approverStatus
    })
    this.PBCForm.patchValue({
      apbc_created_by: currentUser,
      auditPbc_id: this.PBCFormData.id,
      apbc_signoff_period: apbc_signoff_period,
      status: "wip",
      assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditPbc[0].assigneeStatus ,
      approverStatus : this.fapAssignee.indexOf(currentUser) > -1 ? "wip" : this.fieldPanes.auditPbc[0].approverStatus
    });
    //console.log('Form Contents: ' ,this.PBCForm.value)
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updatePBCDetails, this.PBCForm.value)
          .subscribe((res: any) => {
            console.log("res", res);
            if (res) {
              this.adminService.sendCount(true);
              const initialState = {
                title: "Success",
                content: "PBC Updated successfully...",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
            } else {
              const initialState = {
                title: "Fail",
                content: "Failed to save PBC",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
            }
            this.closeModal();
          });
      }
    });
  }
  onSubmit() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to submit current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.upload();
    let currentUser = localStorage.getItem("username");

    //console.log('UserName: ' , currentUser);
    //console.log('FORM DATA: ' , this.PBCForm.value);

    let apbc_signoff_period = moment(
      this.PBCForm.get("apbc_signoff_period").value
    ).format("DD/MM/YYYY");
    if (apbc_signoff_period == "Invalid date") apbc_signoff_period = "-";
    console.log("dsad", {
      apbc_created_by: currentUser,
      auditPbc_id: this.PBCFormData.id,
      apbc_signoff_period: apbc_signoff_period,
      status: "completed",
      assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditPbc[0].assigneeStatus ,
      approverStatus : this.fAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.fapAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditPbc[0].approverStatus)
    });
    this.PBCForm.patchValue({
      apbc_created_by: currentUser,
      auditPbc_id: this.PBCFormData.id,
      apbc_signoff_period: apbc_signoff_period,
      status: "completed",
      assigneeStatus : this.fAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditPbc[0].assigneeStatus ,
      approverStatus : this.fAssignee.indexOf(currentUser) > -1 ? "assigned" : (this.fapAssignee.indexOf(currentUser) > -1 ? "completed" : this.fieldPanes.auditPbc[0].approverStatus)
    });
    //console.log('Form Contents: ' ,this.PBCForm.value)
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updatePBCDetails, this.PBCForm.value)
          .subscribe((res: any) => {
            console.log("res", res);
            if (res) {
              this.adminService.sendCount(true);
              const initialState = {
                title: "Success",
                content: "PBC Submitted successfully...",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
            } else {
              const initialState = {
                title: "Fail",
                content: "Failed to save PBC",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
            }
            this.closeModal();
          });
      }
    });
  }
  // onSubmit() {
  //   const initialState = {
  //     title: "Confirmation",
  //     content: `Do you want to submit PBC Form?`,
  //     link: "Confirm",
  //     action: "Cancel",
  //     confirmFlag: false,
  //   };
  //   this.modalRef = this.modalService.show(ConfirmPopupComponent, {
  //     initialState,
  //     class: "success-class",
  //     backdrop: "static",
  //     keyboard: false,
  //   });
  //   this.upload();
  //   let currentUser = localStorage.getItem("username");

  //   //console.log('UserName: ' , currentUser);
  //   //console.log('FORM DATA: ' , this.PBCForm.value);
  //   let dte = moment(this.PBCForm.get("apbc_signoff_period").value).format(
  //     "MM/DD/YYYY"
  //   );

  //   this.PBCForm.patchValue({
  //     apbc_created_by: currentUser,
  //     auditPbc_id: this.PBCFormData["id"],
  //     apbc_signoff_period: dte,
  //     status: "completed",
  //   });
  //   //console.log('Form Contents: ' ,this.PBCForm.value)
  //   this.modalRef.content.onClose.subscribe((r) => {
  //     if (r) {
  //       this.httpservice
  //         .securePost(ConstUrls.updatePBCDetails, this.PBCForm.value)
  //         .subscribe((res: any) => {
  //           if (res) {
  //             const initialState = {
  //               title: "Success",
  //               content: "PBC Submitted successfully...",
  //               link: "Ok",
  //             };
  //             this.modalRef = this.modalService.show(DefaultModalComponent, {
  //               initialState,
  //               class: "success-class",
  //               backdrop: "static",
  //               keyboard: false,
  //             });
  //           } else {
  //             const initialState = {
  //               title: "Fail",
  //               content: "Failed to save PBC",
  //               link: "Ok",
  //             };
  //             this.modalRef = this.modalService.show(DefaultModalComponent, {
  //               initialState,
  //               class: "success-class",
  //               backdrop: "static",
  //               keyboard: false,
  //             });
  //           }
  //           this.adminService.sendCount(true);
  //           this.closeModal();
  //         });
  //     }
  //   });
  // }
  closeModal() {
    this.closedPBCForm.emit(true);
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
}
