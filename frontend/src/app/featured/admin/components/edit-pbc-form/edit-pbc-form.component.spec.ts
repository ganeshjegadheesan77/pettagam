import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPbcFormComponent } from './edit-pbc-form.component';

describe('EditPbcFormComponent', () => {
  let component: EditPbcFormComponent;
  let fixture: ComponentFixture<EditPbcFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPbcFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPbcFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
