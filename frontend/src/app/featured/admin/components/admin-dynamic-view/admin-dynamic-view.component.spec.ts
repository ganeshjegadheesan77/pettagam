import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDynamicViewComponent } from './admin-dynamic-view.component';

describe('AdminDynamicViewComponent', () => {
  let component: AdminDynamicViewComponent;
  let fixture: ComponentFixture<AdminDynamicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDynamicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDynamicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
