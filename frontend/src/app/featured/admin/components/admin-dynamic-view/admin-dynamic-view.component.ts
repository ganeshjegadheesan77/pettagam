import { Component, OnInit, Input, SimpleChanges } from "@angular/core";
import { Chart, ChartOptions } from "chart.js";
import * as ChartAnnotation from "chartjs-plugin-annotation";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { AdminService } from "../../admin.service";
import { CalendarDataService } from "../../calendar-data.service";

@Component({
  selector: "app-admin-dynamic-view",
  templateUrl: "./admin-dynamic-view.component.html",
  styleUrls: ["./admin-dynamic-view.component.less"],
})
export class AdminDynamicViewComponent implements OnInit {
  riskCount: Number;
  issueCount: Number;
  controlCount: Number;
  actionCount: Number;
  doughnutChart: object;
  BarNewChart: Chart;
  pieChart: Chart;
  doughnutChartSec: Chart;
  pieChartCanvas: any;
  doughnutChartCanvas: any;
  doughnutChartCtx: any;
  pieChartCtx: any;
  canvas: any;
  ctx: any;
  data: any;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  riskData: any;
  assessmentData: any;
  displayCalendar = false;

  datesObj = {
    closingMeetingDate: undefined,

    updateMeetingDate: undefined,
  };

  @Input("pieData") pieData: any;

  // @Input() pieData;
  @Input() graphData;
  @Input() auditId;
  @Input() assignedCount;
  @Input() wipCount;
  @Input() completedCount;
  @Input() highIssueCount;
  @Input() mediumIssueCount;
  @Input() lowIssueCount;
  progressPercent = '0';
  constructor(
    private httpservice: HttpService,
    private adminService: AdminService,
    private calendarDataService: CalendarDataService
  ) { }

  ngOnInit() {
    // this.getCharts();

    this.adminService.getDates().subscribe((dates) => {
      console.log("dates--->", dates);
      // let formattedDate = this.alterDate(dates);
      this.datesObj = dates;
    });
    this.getData();
    // this.getAssessmentData();

    this.calendarDataService.calendarData.subscribe((cal) => {
      this.displayCalendar = false;
      if (cal.items) {
        this.displayCalendar = true;
      }
    });
  }

  alterDate(dates) {
    let dateObj = JSON.parse(JSON.stringify(dates));
    if (dateObj.closingMeetingDate != "-") {
      let date = dateObj.closingMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] + "-" + date[1] + "-" + date[0];
      dateObj.closingMeetingDate = alterDate;
    }
    if (dateObj.openingMeetingDate != "-") {
      let date = dateObj.openingMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] + "-" + date[1] + "-" + date[0];
      dateObj.openingMeetingDate = alterDate;
    }
    if (dateObj.updateMeetingDate != "-") {
      let date = dateObj.updateMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] + "-" + date[1] + "-" + date[0];
      dateObj.updateMeetingDate = alterDate;
    }
    if (dateObj.finalReport != "-") {
      let date = dateObj.finalReport.split("T")[0].split("-");
      let alterDate = date[2] + "-" + date[1] + "-" + date[0];
      dateObj.finalReport = alterDate;
    }
    return dateObj;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes.pieData);
    console.log(changes.graphData);
    // if (
    //   changes.pieData.previousValue &&
    //   changes.graphData.previousValue &&
    //   this.BarNewChart &&
    //   this.pieChart
    // ) {
    // this.BarNewChart.destroy();
    if (this.pieChart) {
      this.pieChart.destroy();
    }
    if (this.doughnutChartSec) {
      this.doughnutChartSec.destroy();
    }

    this.getData();
    if ((this.wipCount + this.assignedCount + this.completedCount) == 0) {
      this.progressPercent = '0';
    }
    else {
      this.progressPercent = ((this.completedCount / (this.wipCount + this.assignedCount + this.completedCount)) * 100).toFixed(2);
    }

    // }
  }

  ngAfterViewInit(): void {
    if (this.graphData) {
      this.getCharts();
    }
  }
  getAssessmentData() {
    this.httpservice
      .secureGet(ConstUrls.getRisks)
      .subscribe((response: any) => {
        if (response) {
          // console.log("intrinsicRisk:: ", response);
          this.riskData = response;

          this.httpservice
            .secureGet(ConstUrls.getAssessmentList)
            .subscribe((response) => {
              // console.log("Assessment response:", response);
              this.assessmentData = response;
              if (response) {
                this.riskData.forEach((risk) => {
                  this.assessmentData.forEach((recrd) => {
                    recrd["iresult"] =
                      recrd && recrd.mydata[0]
                        ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
                        : 0;

                    if (risk._id == recrd._id) {
                      if (recrd["iresult"] == 1) {
                        this.lowRisk++;
                      } else if (recrd["iresult"] == 2) {
                        this.mediumRisk++;
                      } else if (recrd["iresult"] == 3) {
                        this.highRisk++;
                      }
                    }
                  });
                });
                console.log(
                  "Risks:",
                  this.lowRisk,
                  this.mediumRisk,
                  this.highRisk
                );
              }
            });

          // this.toggleToRisk();
        }
      });
  }
  getResultForClr(arr) {
    let r = 0;
    if (arr && arr.length) {
      arr.forEach((ele) => {
        if (r < ele.result) r = ele.result;
      });
      // console.log("r", r);
      // return r || 0;
    }
    return r || 0;
  }
  async getData() {
    // await this.httpservice
    //   .secureGet(ConstUrls.getRiskCount)
    //   .subscribe((response: any) => {
    //     if (response) {
    //       this.riskCount = response;
    //     }
    //   });
    // await this.httpservice
    //   .secureGet(ConstUrls.getNumberofIssues)
    //   .subscribe((response: any) => {
    //     if (response) {
    //       this.issueCount = response;
    //     }
    //   });

    // await this.httpservice
    //   .secureGet(ConstUrls.getControlCount)
    //   .subscribe((response: any) => {
    //     if (response) {
    //       this.controlCount = response;
    //     }
    //   });

    await this.httpservice
      .secureGet(ConstUrls.getNumberofActions)
      .subscribe((response: any) => {
        if (response) {
          this.actionCount = response;
        }

        setTimeout(() => {
          this.getCharts();
        }, 1000);
      });
  }
  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("pie");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "doughnut",
        data: {
          labels: ["Assigned", "WIP", "Completed"],
          datasets: [
            {
              //label: ' ',
              data: [this.assignedCount, this.wipCount, this.completedCount],
              backgroundColor: ["#ffcc00", "#5a4296", "#98aa3a"],
              borderColor: ["#ffcc00", "#5a4296", "#98aa3a"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);

    this.doughnutChartCanvas = <HTMLCanvasElement>(
      document.getElementById("chart")
    );
    this.doughnutChartCtx = this.doughnutChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.doughnutChartSec = new Chart(this.doughnutChartCtx, {
        type: "doughnut",
        data: {
          labels: ["High", "Medium", "Low"],
          datasets: [
            {
              //label: ' ',
              data: [this.highIssueCount, this.mediumIssueCount, this.lowIssueCount],
              backgroundColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderColor: ["#dd6838", "#ffcc00", "#98aa3a"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);
  }




}
