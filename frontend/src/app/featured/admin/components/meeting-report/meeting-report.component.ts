import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../admin.service'

@Component({
  selector: 'app-meeting-report',
  templateUrl: './meeting-report.component.html',
  styleUrls: ['./meeting-report.component.less']
})
export class MeetingReportComponent implements OnInit {
  
  datesObj  = {
    "closingMeetingDate": undefined,
    "openingMeetingDate": undefined,
    "updateMeetingDate": undefined,
    "finalReport":  undefined
  };
  constructor(
    private adminService: AdminService,
  ) { }

  ngOnInit() {
    this.adminService.getDates().subscribe((dates)=> {
      console.log("dates--->",dates)
      // let formattedDate = this.alterDate(dates);
        this.datesObj =  dates;
    })
  }

  alterDate(dates)  {
    let dateObj = JSON.parse(JSON.stringify(dates));
    if(dateObj.closingMeetingDate != "-") {
      let date = dateObj.closingMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] +"-" + date[1] + "-" + date[0];
      dateObj.closingMeetingDate = alterDate;
    }
    if(dateObj.openingMeetingDate != "-") {
      let date = dateObj.openingMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] +"-" + date[1] + "-" + date[0];
      dateObj.openingMeetingDate = alterDate;
    }
    if(dateObj.updateMeetingDate != "-") {
      let date = dateObj.updateMeetingDate.split("T")[0].split("-");
      let alterDate = date[2] +"-" + date[1] + "-" + date[0];;
      dateObj.updateMeetingDate = alterDate;
    }
    if(dateObj.finalReport != "-") {
      let date = dateObj.finalReport.split("T")[0].split("-");
      let alterDate = date[2] +"-" + date[1] + "-" + date[0];;
      dateObj.finalReport = alterDate;
    }
    return dateObj;
  }
}
