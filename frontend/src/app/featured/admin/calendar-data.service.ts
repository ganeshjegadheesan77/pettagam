import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CalendarDataService {

    private auditIdSource = new BehaviorSubject<any>([]);
    private calendarDataSource = new BehaviorSubject<any>([]);
    data = this.auditIdSource.asObservable();
    calendarData = this.calendarDataSource.asObservable();

    constructor() { }

    updatedDataSelection(data) {
        this.auditIdSource.next(data);
    }

    updateCalendatData(data){
        this.calendarDataSource.next(data);
    }
}