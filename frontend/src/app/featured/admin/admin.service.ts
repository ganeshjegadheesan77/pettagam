import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class AdminService {
  private subject = new Subject<any>();
  private subject2 = new Subject<any>();
  sendCount(message: boolean) {
    this.subject.next(message);
  }
  getCount(): Observable<any> {
    return this.subject.asObservable();
  }
  getDates() : Observable<any>  {
    return this.subject2.asObservable();
  }
  setDates(data : Object) {
    this.subject2.next(data);
  }
}
