import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RiskLibraryComponent} from './components/risk-library/risk-library.component'

const routes: Routes = [
  {
    path:'',
    component: RiskLibraryComponent,data:{title:"Risk Library"}
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RiskLibraryRoutingModule { }
