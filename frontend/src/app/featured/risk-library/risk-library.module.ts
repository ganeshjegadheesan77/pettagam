import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RiskLibraryRoutingModule } from './risk-library-routing.module';


// import { EditRiskComponent } from './components/edit-risk/edit-risk/edit-risk.component';
import { RiskEditComponent } from './components/risk-edit/risk-edit.component';
import { OpenLibraryComponent } from './components/open-library/open-library.component';
import { CreateLibraryComponent } from './components/create-library/create-library.component';
import { RiskAttachmentComponent } from './components/risk-attachment/risk-attachment.component';
// import { AttachmentComponent} from '../risk-and-control-management/components/attachment/attachment.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RiskLibraryComponent } from './components/risk-library/risk-library.component';
@NgModule({
  declarations: [RiskEditComponent,CreateLibraryComponent, OpenLibraryComponent , OpenLibraryComponent, CreateLibraryComponent, RiskAttachmentComponent, RiskLibraryComponent],
  imports: [
    CommonModule,
    RiskLibraryRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents:[RiskEditComponent , OpenLibraryComponent,CreateLibraryComponent]
  
  
})
export class RiskLibraryModule { }
