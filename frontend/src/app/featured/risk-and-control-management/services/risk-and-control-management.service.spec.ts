import { TestBed } from '@angular/core/testing';

import { RiskAndControlManagementService } from './risk-and-control-management.service';

describe('RiskAndControlManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RiskAndControlManagementService = TestBed.get(RiskAndControlManagementService);
    expect(service).toBeTruthy();
  });
});
