import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class RiskAndControlManagementService {
  private subject = new Subject<any>();
  private MessageFromCL = new Subject<any>();
  sendCount(message: boolean) {
    this.subject.next(message);
  }
  getCount(): Observable<any> {
    return this.subject.asObservable();
  }
  fromControlListRefreshCWF(message: string) {//send message from IAM to Issue List
    this.MessageFromCL.next({ text: message });
  }

  refreshCWF(): Observable<any> { //got Message from IAM
    return this.MessageFromCL.asObservable();
  }
}
