import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RiskAndControlManagementComponent } from './components/risk-and-control-management.component';

const routes: Routes = [{ path: '', component: RiskAndControlManagementComponent, data: { title: "EnRiCo: RCM" } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RiskAndControlManagementRoutingModule { }
