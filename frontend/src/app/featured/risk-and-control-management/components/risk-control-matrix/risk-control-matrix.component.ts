import { Component, OnInit, Input, ChangeDetectorRef } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { Subscription } from "rxjs";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";

declare var $;
@Component({
  selector: "app-risk-control-matrix",
  templateUrl: "./risk-control-matrix.component.html",
  styleUrls: ["./risk-control-matrix.component.less"],
})
export class RiskControlMatrixComponent implements OnInit {
  // tslint:disable-next-line: no-input-rename
  @Input("riskDefFormStatus") riskDefFormStatus: string;
  subscription: Subscription;
  loadComponent = false;
  loadcityId = false;
  printView = false;
  dataTable: any;
  reportList: any = [];
  assessmentData: any = [];
  controlId: any;
  selectedriskId: any;
  selectedControlId: any;
  CRMatrixData: any;
  RCMatrixData: any;
  controlTableHeaders: any;
  riskTableHeaders: any;
  displayControls = true;
  showHideHeaders: any;
  marked = false;
  addNewRisk: boolean;
  riskData: any;
  controlData: any;
  selectReport: any;
  isLoading = false;

  constructor(
    private httpService: HttpService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    RCMservice: RiskAndControlManagementService
  ) {
    this.subscription = RCMservice.getCount().subscribe((message) => {
      if (message) {
        //console.log("message", message);
        this.listUpdated();
      }
    });
  }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.selectedriskId = "";
    this.toggleToControl();
    this.outsideClick();
    this.getAllRisksRecords();
    this.getAllControlRecords();
  }
  listUpdated() {
    if (this.displayControls) {
      this.toggleToControl();
    } else {
      this.toggleToRisk();
    }
  }
  getAllRisksRecords() {
    this.httpService
      .secureGet(ConstUrls.unassignedRisks)
      .subscribe((response: any) => {
        if (response) {
          this.riskData = response;
          //console.log("riskData:", this.riskData);
        }
      });
  }
  getAllControlRecords() {
    this.httpService
      .secureGet(ConstUrls.unassignedControls)
      .subscribe((response: any) => {
        if (response) {
          // console.log("arrayOfIds:", this.CRMatrixData);
          // let arrayOfIds = [];
          // this.CRMatrixData.forEach((record) => {
          //   if (record.controldata.length) {
          //     arrayOfIds.push(record.controldata.controlId);
          //   }
          // });
          this.controlData = response;
          // console.log("arrayOfIds:", arrayOfIds);
        }
      });
  }
  switchProperty(e) {
    this.marked = e.target.checked;
    if (this.marked == false) {
      // Control Selected
      this.toggleToControl();
    } else {
      // Risk Selected
      this.toggleToRisk();
    }
  }
  duplicate() {}
  share() {}
  addRisk() {
    this.addNewRisk = true;
    $(".pop-up-form").removeClass("d-none");
  }

  PrintClick() {
    this.loadComponent = false;
    this.loadcityId = false;
    this.printView = true;
    let data = {
     
      collection: "risk",
 
    };
    //console.log(data);
    this.httpService
      .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
      .subscribe((res: any) => {
        if (res) {
          this.selectReport = res.report;

        }
      });
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    // this.onChangePrint();
  }

  onChangePrint(val: string) {}

  toggleToControl() {
    let table = $("#riskcontrolmatrixTable2").DataTable();
    table.destroy();
    this.isLoading = true;

    if ($.fn.dataTable.isDataTable("#riskcontrolmatrixTable")) {
      let table1 = $("#riskcontrolmatrixTable").DataTable();
      table1.destroy();
    } else {
      this.controlTableHeaders = [];
    }
    this.marked = false;
    this.getAllControlRecords();
    //console.log("marked:", this.marked);
    this.displayControls = true;
    this.showHideHeaders = [];
    this.RCMatrixData = "";
    this.httpService
      .secureGet(ConstUrls.getCRMRecords)
      .subscribe((response: any) => {
        this.CRMatrixData = [];
        //console.log("Matrix Control Data:", response);
        this.httpService
          .secureGet(ConstUrls.getAssessmentList)
          .subscribe((result) => {
            //console.log("Assessment response:", result);
            this.assessmentData = result;
            if (result) {
              response.forEach((element) => {
                if (element.riskdata.length) {
                  this.assessmentData.forEach(async (recrd) => {
                    recrd["iresult"] =
                      recrd && recrd.mydata[0]
                        ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
                        : 0;
                    element.riskdata.forEach((r) => {
                      if (r._id == recrd._id) {
                        if (recrd["iresult"] == 1) {
                          this.CRMatrixData.push({
                            ...element,
                            buttonClass: "medium",
                            riskdata: r,
                          });
                        } else if (recrd["iresult"] == 2) {
                          this.CRMatrixData.push({
                            ...element,
                            buttonClass: "very-low",
                            riskdata: r,
                          });
                        } else if (recrd["iresult"] == 3) {
                          this.CRMatrixData.push({
                            ...element,
                            buttonClass: "very-high",
                            riskdata: r,
                          });
                        } else {
                          this.CRMatrixData.push({
                            ...element,
                            buttonClass: "link",
                            riskdata: r,
                          });
                        }
                      }
                    });
                  });
                } else {
                  //console.log(element);
                  // this.controlData.push(element);
                }
              });
            }
            // tslint:disable-next-line: max-line-length
            this.controlTableHeaders = [
              "Area",
              "Ctl ID",
              "Control Name",
              "Control Description",
              "Control Risk",
              "Risk ID",
              "Risk Name",
              "Description",
            ];
            //console.log("CRMatrixData:", this.CRMatrixData);
            this.showHideHeaders = this.controlTableHeaders;
            // if (this.CRMatrixData) {
            //   this.getAllControlRecords();
            // }
            this.isLoading = false;
            this.datatable();
          });
      });
  }
  getResultForClr(arr) {
    let r = 0;
    if (arr && arr.length) {
      arr.forEach((ele) => {
        if (r < ele.result) r = ele.result;
      });
      // console.log("r", r);
      // return r || 0;
    }
    return r || 0;
  }
  toggleToRisk() {
    let table = $("#riskcontrolmatrixTable").DataTable();
    table.destroy();
    this.isLoading = true;
    if ($.fn.dataTable.isDataTable("#riskcontrolmatrixTable2")) {
      let table2 = $("#riskcontrolmatrixTable2").DataTable();
      table2.destroy();
    } else {
      this.riskTableHeaders = [];
    }

    this.marked = true;
    //console.log("marked:", this.marked);
    this.displayControls = false;
    this.showHideHeaders = [];

    this.CRMatrixData = "";
    this.RCMatrixData = [];
    this.getAllRisksRecords();

    this.httpService
      .secureGet(ConstUrls.getAllRecords)
      .subscribe((response: any) => {
        this.RCMatrixData = [];
        this.RCMatrixData = response;
        this.RCMatrixData = [];
        //console.log("response in Matrix:", response);
        this.httpService
          .secureGet(ConstUrls.getAssessmentList)
          .subscribe((result) => {
            this.assessmentData = result;
            if (result) {
              // response.forEach((risk) => {

              response.forEach((element) => {
                this.assessmentData.forEach(async (recrd) => {
                  recrd["iresult"] =
                    recrd && recrd.mydata[0]
                      ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
                      : 0;
                  element.controldata.forEach((r) => {
                    if (element._id == recrd._id) {
                      if (recrd["iresult"] == 1) {
                        this.RCMatrixData.push({
                          ...element,
                          buttonClass: "medium",
                          controldata: r,
                        });
                      } else if (recrd["iresult"] == 2) {
                        this.RCMatrixData.push({
                          ...element,
                          buttonClass: "very-low",
                          controldata: r,
                        });
                      } else if (recrd["iresult"] == 3) {
                        this.RCMatrixData.push({
                          ...element,
                          buttonClass: "very-high",
                          controldata: r,
                        });
                      } else {
                        this.RCMatrixData.push({
                          ...element,
                          buttonClass: "link",
                          controldata: r,
                        });
                      }
                    } else {
                      // console.log("Unassigned Controls", element);
                    }
                  });
                });
              });
            }
            // tslint:disable-next-line: max-line-length
            this.riskTableHeaders = [
              "Area",
              "Risk ID",
              "Risk Name",
              "Description",
              "Ctl ID",
              "Control Name",
              "Control Description",
              "Control Risk",
            ];
            this.showHideHeaders = this.riskTableHeaders;
            this.isLoading = false;
            this.datatable2();
          });
      });
  }

  getStatusOfChild(event) {
    if (event === true) {
      if (this.displayControls === true) {
        let table = $("#riskcontrolmatrixTable").DataTable();
        table.destroy();
        this.toggleToControl();
      } else {
        let table = $("#riskcontrolmatrixTable2").DataTable();
        table.destroy();
        this.toggleToRisk();
      }
    }
  }

  loadMyCityId(cId) {
    this.selectedControlId = { id: cId, renderFrom: "RCM" };
    this.loadcityId = true;
    this.printView = false;
    this.loadComponent = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  loadMyChildComponent(id) {
    this.selectedriskId = { id: id, renderFrom: "RCM" };
    this.loadComponent = true;
    this.loadcityId = false;
    this.printView = false;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }
  // PrintClick() {
  //   this.loadComponent = false;
  //   this.loadcityId = false;
  //   this.printView = true;
  //   $(".scroll-container").scrollTop(0);
  //   $(".pop-up-form").removeClass("d-none");
  // }

  delete() {}
  addNewControlForm() {}

  datatable() {
    this.chRef.detectChanges();
    const controlDataTable: any = $("#riskcontrolmatrixTable");
    const controlMatrixRCM = this.dataTableService.initializeTable(
      controlDataTable,
      `controlRCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      controlMatrixRCM,
      `rcmcontrolMatrix-select-all`
    );
    this.dataTableFuncService.columnDropDown(controlMatrixRCM, `rcm-hideshow`);
    this.dataTableFuncService.expandContainer(controlMatrixRCM);
    $("#riskcontrolmatrixTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#rcmcontrolMatrix-select-all").prop("checked", false);
          var el = $("#rcmcontrolMatrix-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#riskcontrolmatrixTable tr td input[type='checkbox']:checked")
            .length ==
          $("#riskcontrolmatrixTable tr td input[type='checkbox']").length
        ) {
          $("#rcmcontrolMatrix-select-all").prop("checked", true);
        }
      }
    );
  }

  datatable2() {
    this.chRef.detectChanges();
    const riskmatrixTable: any = $("#riskcontrolmatrixTable2");
    const riskMatrixRCM = this.dataTableService.initializeTable(
      riskmatrixTable,
      `riskRCM`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      riskMatrixRCM,
      `rcmristMatrix-select-all`
    );
    this.dataTableFuncService.columnDropDown(riskMatrixRCM, `rcm-hideshow`);
    this.dataTableFuncService.expandContainer(riskMatrixRCM);
    $("#riskcontrolmatrixTable2 tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#rcmristMatrix-select-all").prop("checked", false);
          var el = $("#rcmristMatrix-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#riskcontrolmatrixTable2 tr td input[type='checkbox']:checked")
            .length ==
          $("#riskcontrolmatrixTable2 tr td input[type='checkbox']").length
        ) {
          $("#rcmristMatrix-select-all").prop("checked", true);
        }
      }
    );
  }
  Setting(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    $(".dt-button-collection").hide();
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $(".dropdown-menu-column").removeClass("show-bar");
    });
  }
}
