import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  Output,
  ViewChild,
} from "@angular/core";
import { HttpService } from "../../../../core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
declare var $;
@Component({
  selector: "app-control-definition-form",
  templateUrl: "./control-definition-form.component.html",
  styleUrls: ["./control-definition-form.component.less"],
})
export class ControlDefinitionFormComponent implements OnInit, OnChanges {
  @Input("controlData") controlData: string;
  @Output() closedControlDefForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @ViewChild("controlListForm", { static: false })
  submitted = false;
  position;
  controlListForm: TabsetComponent;
  modalRef: BsModalRef;
  disableOwner = false;
  fetchedData: any;
  controlStatuses: any;
  controlType: any;
  controlForm: FormGroup;
  riskMasterDepartment: any;
  riskProcess: any;
  allRisks = [];
  filteredRisks = [];
  assignedRisks: any = [];
  newRIds: any = [];
  systemUsers: any = [];
  deletedRisks: any = [];
  controlAddedDate: any;
  controlApprovedDate: any;
  controlLastChangeDate: any;

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private RCMservice: RiskAndControlManagementService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.httpservice.get(ConstUrls.getAllUsers).subscribe((response: any) => {
      if (response) {
        this.systemUsers = response;
      }
    });

  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes.controlData.currentValue)
    if (changes.controlData.currentValue) {
      this.controlData = changes.controlData.currentValue;
      if (this.controlData) {
        this.getOneControl();
      }
      this.initForm();

      this.httpservice
        .get(ConstUrls.masterControlStatus)
        .subscribe((response: any) => {
          this.controlStatuses = response;
        });

      this.httpservice
        .get(ConstUrls.masterControlType)
        .subscribe((response: any) => {
          this.controlType = response;
        });

      this.httpservice
        .get(ConstUrls.riskDepartment)
        .subscribe((response: any) => {
          this.riskMasterDepartment = response;
        });

      this.httpservice.get(ConstUrls.riskProcess).subscribe((response: any) => {
        this.riskProcess = response;
      });

      let payload = { controlId: this.controlData["id"] };
      //console.log("Payload:", payload, this.controlData);
      this.httpservice
        .securePost(ConstUrls.getRisksAssigned, payload)
        .subscribe((response: any) => {
          if (response) {
            //console.log("response:", response);
            this.assignedRisks = response;
            //console.log(this.assignedRisks);
            this.filterRisks();
          }
        });
      this.httpservice.get(ConstUrls.getRisks).subscribe((response: any) => {
        if (response) {
          this.allRisks = response;
          this.filteredRisks = response;
          this.filterRisks();
        }
      });
      this.riskdatatable();
      $(":checkbox").attr("checked", false);
    }
  }
  addRisk() {
    let table = $('#riskcontrolWFTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let rId = $(this).attr('value')
      _this.newRIds.push(rId);
      let assignedRisk = _this.allRisks.find(
        (risks) => risks.riskId === rId
      );
      _this.assignedRisks.push(assignedRisk);
    });
    this.filterRisks();
    // $(":checkbox").attr("checked", false);
    table.destroy();
    this.riskdatatable();
  }
  deleteRisk(id) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedRisks.push(id);

        this.assignedRisks = this.assignedRisks.filter(
          (risks) => risks.riskId !== id
          // this.assignedControls.indexOf(controlId) === -1
        );
        let table = $('#riskcontrolWFTable').DataTable();
        table.destroy();
        this.riskdatatable();
        this.filterRisks();
        const initialState = {
          title: "Success",
          content: "Risk Removed!",
          link: "Ok",
        };
        this.modalRef = this.modalService.show(DefaultModalComponent, {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        });
      }
    });
  }
  filterRisks() {
    if (this.allRisks) {
      const selectedIds = [];
      this.assignedRisks.forEach((element) => {
        selectedIds.push(element.riskId);
      });
      this.filteredRisks = this.allRisks.filter(
        (risk) => selectedIds.indexOf(risk.riskId) === -1
      );
    }
  }
  getOneControl() {
    let payload = { controlId: this.controlData["id"] };
    this.httpservice
      .securePost(ConstUrls.getOneControl, payload)
      .subscribe(async (response) => {
        this.fetchedData = await response;
        this.initForm();
        this.disableOwner = response["controlOwnerFlag"];
        this.controlForm.patchValue(this.fetchedData);
        this.controlForm.patchValue({
          controlAddedDate: new Date(this.fetchedData.controlAddedDate),
          controlApprovedDate: new Date(this.fetchedData.controlApprovedDate),
          controlLastChangeDate: new Date(
            this.fetchedData.controlLastChangeDate
          ),
        });
      });
  }
  get f() {
    return this.controlForm.controls;
  }
  initForm() {
    this.controlForm = this.formBuilder.group({
      controlName: ["", [Validators.required]],
      controlId: ["", []],
      controlAddedDate: ["", []],
      controlApprovedBy: ["", []],
      controlApprovedDate: ["", []],
      controlApprovedId: ["", []],
      controlDepartment: ["", []],
      controlDepartmentProcess: ["", []],
      controlDescription: ["", []],
      controlGuidance: ["", []],
      controlKeyFlag: [false, []],
      controlKeyProcess: ["", []],
      controlLastChangeDate: ["", []],
      controlLegalReference: ["", []],
      controlObjective: ["", []],
      controlOwnerId: ["", []],
      controlOwnerName: ["", []],
      controlSubmitter: ["", []],
      controlSubmitterId: ["", []],
      controlStatus: ["", []],
      controlTesting: [false, []],
      controlType: ["", []],
      riskId: [[]],
      assignToTask: [false, []],
      controlOwnerFlag: [false, []],
      schedule: [false, []],
    });
  }
  selectUser(user) {
    this.controlForm.value.controlOwnerName = user;
    // this.riskForm.value.riskOwnerId = userId;
  }
  ownerSelect(ev) {
    this.disableOwner = ev.target.checked;
    if (ev.target.checked) {
      this.controlForm.value.controlOwnerName = localStorage.getItem(
        "username"
      );
      this.controlForm.value.controlSubmitter = localStorage.getItem(
        "username"
      );
    }
  }
  onSave() {
    // updateOneControl
    this.submitted = true;
    const ids = [];
    this.assignedRisks.forEach((element) => {
      ids.push(element.riskId);
    });
    this.controlForm.value.riskId = ids;
    this.controlForm.value.controlLastChangeDate = new Date();
    // if (this.controlForm.value) {
    // if (this.controlForm.invalid) {
    //   console.log("control form failed", this.controlForm);
    //   return;
    // }
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updateOneControl, this.controlForm.value)
          .subscribe((response: any) => {
            if (response) {
              this.RCMservice.sendCount(true);
              if (this.deletedRisks) {
                this.httpservice.securePost(
                  ConstUrls.removeOneControlFromRisks,
                  {
                    controlId: this.controlData["id"],
                    riskId: this.deletedRisks,
                  },
                  (res) => {
                    this.RCMservice.sendCount(true);
                  }
                );
              }
              const initialState = {
                title: "Success",
                content:
                  "Control : " +
                  this.controlData["id"] +
                  " updated Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.closeModal();
            }
          });
      }
    });

    // console.log(this.controlForm.value);
  }

  closeModal() {
    this.assignedRisks = this.assignedRisks.filter(
      (risks) => this.newRIds.indexOf(risks.riskId) === -1
    );
    this.filterRisks();
    this.newRIds = [];

    // $(".scroll-container").scrollTop(0);
    this.closedControlDefForm.emit(true);
    $(".pop-up-form").addClass("d-none");
    // this.controlListForm.tabs[0].active = true;
  }
  closeModalWithoutSaving() {
    this.assignedRisks = this.assignedRisks.filter(
      (risks) => this.newRIds.indexOf(risks.riskId) === -1
    );
    this.filterRisks();
    this.newRIds = [];
    this.deletedRisks = [];
    // $(".scroll-container").scrollTop(0);
    this.closedControlDefForm.emit(false);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 }
    // this.controlListForm.tabs[0].active = true;
  }


  riskdatatable() {
    let table = $('#riskcontrolWFTable').DataTable();
    table.destroy();
    setTimeout(() => {
      $('#riskcontrolWFTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "destroy": true,
        "language": {
          "searchPlaceholder": "Search"
        },
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  }


}
