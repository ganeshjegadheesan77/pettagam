import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { HttpService } from "src/app/core/http-services/http.service";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";

declare var $;
@Component({
  selector: "app-risk-list",
  templateUrl: "./risk-list.component.html",
  styleUrls: ["./risk-list.component.less"],
})
export class RiskListComponent implements OnInit {
  modalRef: BsModalRef;
  public loadComponent = false;
  public addNewRiskComponent = false;
  public addNewGenerateComponent = false;
  hide = true;
  riskData: any;
  assessmentData: any;
  riskListData: any;
  selectedRiskId: any;
  dataTable: any;
  riskListRCMTableHeaders: any;
  showHideHeadersRL: any;
  RCMatrixData: any;
  selectReport: any;
  isLoading = false;

  constructor(
    private httpservice: HttpService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    private modalService: BsModalService,
    private RCMservice: RiskAndControlManagementService
  ) {}

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();

    this.getRiskDetails();
    this.outsideClick();
  }

  closeModal() {
    this.ngOnInit();
    // this.closedRiskDefForm.emit(true);
    $(".pop-up-form").addClass("d-none");
  }
  toggleToRisk() {
    // this.httpservice
    // .securePost(ConstUrls.getControlsAssigned, payload)
    // this.CRMatrixData = "";
    this.httpservice
      .secureGet(ConstUrls.getAllRecords)
      .subscribe((response: any) => {
        // this.RCMatrixData = [];
        this.riskData.forEach((riskDataElement) => {
          response.forEach((element) => {
            element.forEach((r) => {
              if (
                r.controldata.controlRisk >= 0 &&
                r.controldata.controlRisk <= 33 &&
                r.riskId === riskDataElement.riskId
              ) {
                this.riskListData.push({
                  ...element,
                  buttonClass: "medium",
                });
              } else if (
                r.controldata.controlRisk >= 34 &&
                r.controldata.controlRisk <= 66 &&
                r.riskId === riskDataElement.riskId
              ) {
                this.RCMatrixData.push({
                  ...element,
                  buttonClass: "very-low",
                });
              } else if (
                r.controldata.controlRisk >= 67 &&
                r.controldata.controlRisk <= 100 &&
                r.riskId === riskDataElement.riskId
              ) {
                this.RCMatrixData.push({
                  ...element,
                  buttonClass: "very-high",
                });
              } else {
                this.RCMatrixData.push({
                  ...element,
                  buttonClass: "link",
                });
              }
            });
          });
        });
      });
  }
  getStatusOfAddNewRiskForm(event) {
    if (event === true) {
      this.addNewRiskComponent = false;
      this.loadComponent = false;
      this.getRiskDetails();
    }
  }
  getResultForClr(arr) {
    let r = 0;
    if (arr && arr.length) {
      arr.forEach((ele) => {
        if (r < ele.result) r = ele.result;
      });
      // console.log("r", r);
      // return r || 0;
    }
    return r || 0;
  }
  getRiskDetails() {
    let table = $("#riskListTable").DataTable();
    table.destroy();
    this.isLoading = true;
    this.riskListRCMTableHeaders = [];
    this.showHideHeadersRL = [];
    this.httpservice
      .secureGet(ConstUrls.getRisks)
      .subscribe((response: any) => {
        if (response) {
          // console.log("intrinsicRisk:: ", response);
          this.riskData = response;

          this.httpservice
            .secureGet(ConstUrls.getAssessmentList)
            .subscribe((response) => {
              // console.log("Assessment response:", response);s
              this.assessmentData = response;
              if (response) {
                this.riskData.forEach((risk) => {
                  this.assessmentData.forEach((recrd) => {
                    recrd["iresult"] =
                      recrd && recrd.mydata[0]
                        ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
                        : 0;

                    if (risk._id == recrd._id) {
                      if (recrd["iresult"] == 1) {
                        risk.buttonClass = "medium";
                      } else if (recrd["iresult"] == 2) {
                        risk.buttonClass = "very-low";
                      } else if (recrd["iresult"] == 3) {
                        risk.buttonClass = "very-high";
                      } else {
                        risk.buttonClass = "link";
                      }
                    }
                  });
                });
              }
            });

          // this.toggleToRisk();
        }
        this.riskListRCMTableHeaders = [
          "Risk Area",
          "Risk ID",
          "Risk Name",
          "Risk Description",
        ];
        this.showHideHeadersRL = this.riskListRCMTableHeaders;
        this.isLoading = false;
        this.riskdatatable();
      });
  }
  getControlFormStatus(event) {
    if (event === true) {
      this.addNewRiskComponent = false;
      this.loadComponent = false;
      this.addNewGenerateComponent = false;
      this.getRiskDetails();
    }
  }

  loadAddNewComponent() {
    this.loadComponent = false;
    this.addNewRiskComponent = true;
    this.addNewGenerateComponent = false;
    $(".pop-up-form").removeClass("d-none");
  }

  GenerateReport() {
    this.loadComponent = false;
    this.addNewRiskComponent = true;
    this.addNewGenerateComponent = true;

    // let data = {
    //   // "database" : "newRcmDB",
    //   collection: "rcm_risk_list",
    //   // "value" : "riskControls"
    // };
    // //console.log(data);
    // this.httpservice
    //   .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
    //   .subscribe((res: any) => {
    //     //console.log("Report List: ", res);
    //     if (res) {
    //       this.selectReport = res.report;
    //       // val= res;
    //       // this.reportList = res.data.filter((s) => s.UserName.includes(val));
    //     }
    //   });

    $(".pop-up-form").removeClass("d-none");
  }
  loadReportComponent() {
    this.loadComponent = false;
    this.addNewRiskComponent = false;
    this.addNewGenerateComponent = true;
    $(".pop-up-form").removeClass("d-none");
  }

  delete(id) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to delete this Record?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        var selectedRisk = this.riskData.filter(x=>x.riskId == id)[0];
        
          if (selectedRisk.riskAssessmentId.length || selectedRisk.controlId.length)
          {
              var msg = "";
              if(selectedRisk.controlId.length > 0){
                msg = "Risk used by controls! Cannot delete. "
              }
              else if(selectedRisk.riskAssessmentId.length > 0){
                msg = "Risk used by Assessments! Cannot delete. "
              }
              else {
                msg = "Risk under use! Cannot delete."
              }
              
            const initialState = {
              title: "Warning!",
              content: msg,
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });

            this.closeModal();
            return false;
          }
        this.httpservice
          .securePost(ConstUrls.deleteRisk, { riskId: id })
          .subscribe((response: any) => {
            if (response) {
              // this.riskData = response;
              this.riskData = this.riskData.filter((rec) => {
                if (!rec.riskAssessmentId.length && !rec.controlId.length) {
                  return rec.riskId !== id;
                }
                return true;
              });
              this.RCMservice.sendCount(true);
              const initialState = {
                title: "Success!",
                content: "Record Deleted Successfully!",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              //console.log("Cannot delete");
              this.closeModal();
              return false;
            }
          });
      }
    });
  }
  share() {}
  print() {}
  duplicate() {}
  loadMyChildComponent(id) {
    this.loadComponent = true;
    this.addNewRiskComponent = false;
    this.addNewGenerateComponent = false;
    this.selectedRiskId = { id: id, renderForm: "Risk" };
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }
  riskdatatable() {
    this.chRef.detectChanges();
    const riskTable: any = $("#riskListTable");
    const riskListRCM = this.dataTableService.initializeTable(
      riskTable,
      `risk`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      riskListRCM,
      `rcmristList-select-all`
    );
    this.dataTableFuncService.columnDropDown(riskListRCM, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(riskListRCM);
    $("#riskListTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#rcmristList-select-all").prop("checked", false);
          var el = $("#rcmristList-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#riskListTable tr td input[type='checkbox']:checked").length ==
          $("#riskListTable tr td input[type='checkbox']").length
        ) {
          $("#rcmristList-select-all").prop("checked", true);
        }
      }
    );
  }
  SettingRiskList(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    $(".dt-button-collection").hide();
    e.stopPropagation();
  }
  outsideClick() {
    $("body").on("click", function () {
      $(".dropdown-menu-column").removeClass("show-bar");
    });
  }
}
