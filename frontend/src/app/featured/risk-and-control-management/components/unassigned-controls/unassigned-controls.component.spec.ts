import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnassignedControlsComponent } from './unassigned-controls.component';

describe('UnassignedControlsComponent', () => {
  let component: UnassignedControlsComponent;
  let fixture: ComponentFixture<UnassignedControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnassignedControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnassignedControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
