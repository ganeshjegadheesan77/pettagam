import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { Subscription } from "rxjs";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";

@Component({
  selector: "app-unassigned-controls",
  templateUrl: "./unassigned-controls.component.html",
  styleUrls: ["./unassigned-controls.component.less"],
})
export class UnassignedControlsComponent implements OnInit {
  totalUnassignedControls: any;
  subscription: Subscription;
  constructor(
    private httpservice: HttpService,
    RCMservice: RiskAndControlManagementService
  ) {
    this.subscription = RCMservice.getCount().subscribe((message) => {
      if (message) {
        this.count();
      }
    });
  }

  ngOnInit() {
    this.count();
  }
  count() {
    this.httpservice
      .secureGet(ConstUrls.unassignedControlCount)
      .subscribe((response: any) => {
        if (!response) {
          this.totalUnassignedControls = 0;
        }
        this.totalUnassignedControls = response;

        //console.log("Unassigned Controls: ", this.totalUnassignedControls);
      });
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
}
