import { Component, OnInit, NgModule, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-risk-and-control-management',
  templateUrl: './risk-and-control-management.component.html',
  styleUrls: ['./risk-and-control-management.component.less']
})

export class RiskAndControlManagementComponent implements OnInit, OnDestroy  {

  constructor() { }

  ngOnInit() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('enrico-logo');
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('enrico-logo');
   }
}
