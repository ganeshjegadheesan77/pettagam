import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewControlComponent } from './add-new-control.component';

describe('AddNewControlComponent', () => {
  let component: AddNewControlComponent;
  let fixture: ComponentFixture<AddNewControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
