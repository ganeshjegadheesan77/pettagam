import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
declare var $;
@Component({
  selector: "app-add-new-control",
  templateUrl: "./add-new-control.component.html",
  styleUrls: ["./add-new-control.component.less"],
})
export class AddNewControlComponent implements OnInit {
  @Output() closedAddControlForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @ViewChild("AddNewControl", { static: false }) AddNewControl: TabsetComponent;
  modalRef: BsModalRef;
  position;
  controlStatuses: any;
  controlType: any;
  riskMasterDepartment: any;
  controlForm: FormGroup;
  riskProcess: any;
  submitted = false;
  generatedControlId: string;
  identifiedDate: string;
  riskSubmittedDate: string;
  disableOwner = true;
  systemUsers: any = [];
  allRisks = [];
  filteredRisks = [];
  newRIds: any = [];
  assignedRisks: any = [];
  controlIds: Array<string> = [];
  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private RCMservice: RiskAndControlManagementService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    //console.log("init called!");
    this.initForm();
    this.httpservice.get(ConstUrls.getAllUsers).subscribe((response: any) => {
      if (response) {
        this.systemUsers = response;
      }
    });
    this.httpservice.get(ConstUrls.getAllControlRecords).subscribe((response: any) => {
      if (response) {
        for (const ctrl of response) {
          this.controlIds.push(ctrl.controlId);
        }
      }
    });
    this.httpservice
      .get(ConstUrls.masterControlStatus)
      .subscribe((response: any) => {
        this.controlStatuses = response;
        // console.log('control Status: ' , this.controlStatuses);
      });

    this.httpservice
      .get(ConstUrls.masterControlType)
      .subscribe((response: any) => {
        this.controlType = response;
      });

    this.httpservice
      .get(ConstUrls.riskDepartment)
      .subscribe((response: any) => {
        this.riskMasterDepartment = response;
      });

    this.httpservice.get(ConstUrls.riskProcess).subscribe((response: any) => {
      this.riskProcess = response;
      // console.log('process: ' , this.riskProcess)
    });
    this.httpservice.get(ConstUrls.getRisks).subscribe((response: any) => {
      if (response) {
        this.allRisks = response;
        this.filteredRisks = response;
        this.filterRisks();
      }
    });
    this.addnewcontrolTable();
  }
  deleteRisk(id) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.assignedRisks = this.assignedRisks.filter(
          (risks) => risks.riskId !== id
          // this.assignedControls.indexOf(controlId) === -1
        );
        this.filterRisks();
        let table = $('#addnewcontrolTable').DataTable();
        table.destroy();
        this.addnewcontrolTable();
      }
    });
  }
  ownerSelect(ev) {
    this.disableOwner = ev.target.checked;
    if (ev.target.checked) {
      this.controlForm.patchValue({
        controlOwnerName: localStorage.getItem("username"),
        controlSubmitter: localStorage.getItem("username"),
      });
    }
  }
  addRisk() {
    //console.log(risk);
    //console.log(this.assignedRisks);
    // if (risk) {
    //   this.newRIds.push(risk);
    //   const assignedRisk = this.allRisks.find((risks) => risks.riskId === risk);
    //   this.assignedRisks.push(assignedRisk);
    //   this.filterRisks();
    // }
    let table = $('#addnewcontrolTable').DataTable();
    let arr = [];
    table.$('input:checked').each(function () {
      arr.push($(this).attr('value'))
    })
    let flag = [];
    this.assignedRisks.forEach((risk) => {
      flag.push(risk.riskId);
    })
    if (arr.length > 0) {
      arr.forEach(x => {
        this.allRisks.forEach((risk) => {
          if (x == risk.riskId && flag.indexOf(x) === -1) {
            this.assignedRisks.push(risk);
          }
        });
      })
      this.filterRisks();
      // $(":checkbox").attr("checked", false);
      table.destroy();
      this.addnewcontrolTable()
    }
  }

  filterRisks() {
    if (this.allRisks.length) {
      const selectedIds = [];
      this.assignedRisks.forEach((element) => {
        selectedIds.push(element.riskId);
      });
      this.filteredRisks = this.allRisks.filter(
        (risk) => selectedIds.indexOf(risk.riskId) === -1
      );
    }
  }
  selectUser(user) {
    this.controlForm.patchValue({
      controlOwnerName: user,
    });
  }
  closeModal() {
    this.assignedRisks = [];
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.AddNewControl.tabs[0].active = true;
    this.closedAddControlForm.emit(true);
  }

  get f() {
    return this.controlForm.controls;
  }

  initForm() {
    this.controlForm = this.formBuilder.group({
      controlName: ["", [Validators.required]],
      controlId: ["", []],
      controlAddedDate: [new Date().toLocaleDateString(), []],
      controlApprovedBy: ["", []],
      controlApprovedDate: ["11/11/1111", []],
      controlApprovedId: ["", []],
      controlDepartment: ["", []],
      controlDepartmentProcess: ["", []],
      controlDescription: ["", []],
      controlGuidance: ["", []],
      controlKeyFlag: [false, []],
      controlKeyProcess: [false, []],
      controlLastChangeDate: ["11/11/1111", []],
      controlLegalReference: ["", []],
      controlObjective: ["", []],
      controlOwnerId: ["", []],
      controlOwnerName: [localStorage.getItem("username"), []],
      controlSubmitter: ["", []],
      controlSubmitterId: ["", []],
      controlStatus: ["", []],
      controlTesting: [false, []],
      controlType: ["", []],
      controlRisk: [0, []],
      riskId: [[]],
      assignToTask: [false, []],
      controlOwnerFlag: true,
      schedule: [false, []],
      issueId : [[]]
    });
  }

  public findInvalidControls() {
    const invalid = [];
    const controls = this.controlForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  createControlId() {
    //create 3 digit num from 100 to 999(both inclusive)
    let ctrlId = "C-" + Math.floor(Math.random() * (999 - 100 + 1) + 100);
    if (this.controlIds.indexOf(ctrlId) === -1) {
      this.generatedControlId = ctrlId;
      return;
    } else {
      this.createControlId();
    }

  }

  onSave() {
    //console.log("invalid controls", this.findInvalidControls());
    this.submitted = true;
    if (this.controlForm.invalid) {
      //console.log("control form failed", this.controlForm);
      return;
    }
    // console.log('Submitted Form Details: ' , this.controlForm.value)
    this.createControlId();
    // this.generatedControlId = "C-" + Math.floor(Math.random() * 100) + 1;
    this.identifiedDate = new Date(
      this.controlForm.get("controlAddedDate").value
    ).toLocaleDateString();
    if (this.identifiedDate === "Invalid Date") {
      this.identifiedDate = new Date().toLocaleDateString();
    }
    const ids = [];
    if (this.assignedRisks.length) {
      this.assignedRisks.forEach((element) => {
        ids.push(element.riskId);
      });
    }

    // this.controlForm.value.riskId = ids;
    if (ids.length) {
      this.controlForm.patchValue({
        riskId: ids,
        assignedToRisk: true,
      });
    } else {
      this.controlForm.patchValue({
        assignedToRisk: false,
      });
    }
    this.controlForm.patchValue({
      controlKeyFlag: false,
      controlTesting: false,
      assignToTask: false,
      controlId: this.generatedControlId,
      controlAddedDate: new Date(),
      controlLastChangeDate: new Date(),
    });
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.saveControls, this.controlForm.value)
          .subscribe((response: any) => {
            if (response) {
              this.RCMservice.sendCount(true);
              const initialState = {
                title: "Success",
                content:
                  "Control : " +
                  this.generatedControlId +
                  " Created Successfully!",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.RCMservice.fromControlListRefreshCWF('Refresh WF');

              this.closeModal();
            }
          });
      }
    });
  }
  closeModalWithoutSaving() {
    this.assignedRisks = [];
    this.filteredRisks = this.allRisks;
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    // this.AddNewControl.tabs[0].active = true;
    this.closedAddControlForm.emit(false);
    this.position = this.position ? undefined : { x: 0, y: 0 }
  }
  // console.log('Control Form Data: ' , this.controlForm.value);


  addnewcontrolTable() {
    setTimeout(() => {
      $('#addnewcontrolTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  };

}
