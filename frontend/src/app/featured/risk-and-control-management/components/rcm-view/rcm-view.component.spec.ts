import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcmViewComponent } from './rcm-view.component';

describe('RcmViewComponent', () => {
  let component: RcmViewComponent;
  let fixture: ComponentFixture<RcmViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcmViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcmViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
