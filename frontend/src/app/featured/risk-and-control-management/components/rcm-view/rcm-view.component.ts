import { Component, OnInit } from "@angular/core";
import { Chart, ChartOptions } from "chart.js";

import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import * as ChartAnnotation from "chartjs-plugin-annotation";
// const ChartAnnotation = require(`chartjs-plugin-annotation`)
@Component({
  selector: "app-rcm-view",
  templateUrl: "./rcm-view.component.html",
  styleUrls: ["./rcm-view.component.less"],
})
export class RcmViewComponent implements OnInit {
  riskCount: Number;
  issueCount: Number;
  controlCount: Number;
  actionCount: Number;
  doughnutChart: object;
  pieChart: object;
  pieChartCanvas: any;
  pieChartCtx: any;
  canvas: any;
  ctx: any;
  data: any;
  highRisk: any = 0;
  lowRisk: any = 0;
  mediumRisk: any = 0;
  riskData: any;
  assessmentData: any;

  constructor(private httpservice: HttpService) {}
  getCharts() {
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of doughnut element of variable
    this.canvas = <HTMLCanvasElement>document.getElementById("doughnut");
    this.ctx = this.canvas.getContext("2d");
    // And for a doughnut chart
    this.doughnutChart = new Chart(this.ctx, {
      type: "doughnut",
      data: {
        labels: ["Risk", "Issues", "Controls", "Actions"],
        datasets: [
          {
            //label: '',
            data: [
              this.riskCount,
              this.issueCount,
              this.controlCount,
              this.actionCount,
            ],
            backgroundColor: ["#664da0", "#dd6838", "#ffcc00", "#98aa3a"],
            borderColor: ["#664da0", "#dd6838", "#ffcc00", "#98aa3a"],
            borderWidth: 1,
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
        },
        // cutoutPercentage: 40,
        responsive: false,
        legend: {
          position: "",
          label: {
            display: false,
          },
          display: false,
        },
      },
    });

    // binding of pieChart element to variable
    this.pieChartCanvas = <HTMLCanvasElement>document.getElementById("pie");
    this.pieChartCtx = this.pieChartCanvas.getContext("2d");

    // Add for a pieChart chart
    setTimeout(() => {
      this.pieChart = new Chart(this.pieChartCtx, {
        type: "pie",
        data: {
          labels: ["High", "Low", "Medium"],
          datasets: [
            {
              //label: ' ',
              data: [this.highRisk, this.lowRisk, this.mediumRisk],
              backgroundColor: ["#dd6836", "#98aa3a", "#ffcc00"],
              borderColor: ["#dd4f36", "#98aa3a", "#ffcc00"],
              borderWidth: 1,
            },
          ],
        },
        options: {
          //cutoutPercentage: 40,
          responsive: false,
          legend: {
            // position: 'bottom',
            label: {
              display: false,
            },
            display: false,
          },
        },
      });
    }, 1000);
  }
  ngOnInit() {
    // chart annotation
    this.getData();
    this.getAssessmentData();
  }
  getAssessmentData() {
    this.httpservice
      .secureGet(ConstUrls.getRisks)
      .subscribe((response: any) => {
        if (response) {
          // console.log("intrinsicRisk:: ", response);
          this.riskData = response;

          this.httpservice
            .secureGet(ConstUrls.getAssessmentList)
            .subscribe((response) => {
              // console.log("Assessment response:", response);
              this.assessmentData = response;
              if (response) {
                this.riskData.forEach((risk) => {
                  this.assessmentData.forEach((recrd) => {
                    recrd["iresult"] =
                      recrd && recrd.mydata[0]
                        ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
                        : 0;

                    if (risk._id == recrd._id) {
                      if (recrd["iresult"] == 1) {
                        this.lowRisk++;
                      } else if (recrd["iresult"] == 2) {
                        this.mediumRisk++;
                      } else if (recrd["iresult"] == 3) {
                        this.highRisk++;
                      }
                    }
                  });
                });
                console.log(
                  "Risks:",
                  this.lowRisk,
                  this.mediumRisk,
                  this.highRisk
                );
              }
            });

          // this.toggleToRisk();
        }
      });
  }
  getResultForClr(arr) {
    let r = 0;
    if (arr && arr.length) {
      arr.forEach((ele) => {
        if (r < ele.result) r = ele.result;
      });
      // console.log("r", r);
      // return r || 0;
    }
    return r || 0;
  }
  async getData() {
    await this.httpservice
      .secureGet(ConstUrls.getRiskCount)
      .subscribe((response: any) => {
        if (response) {
          this.riskCount = response;
        }
      });
    await this.httpservice
      .secureGet(ConstUrls.getNumberofIssues)
      .subscribe((response: any) => {
        if (response) {
          this.issueCount = response;
        }
      });

    await this.httpservice
      .secureGet(ConstUrls.getControlCount)
      .subscribe((response: any) => {
        if (response) {
          this.controlCount = response;
        }
      });

    await this.httpservice
      .secureGet(ConstUrls.getNumberofActions)
      .subscribe((response: any) => {
        if (response) {
          this.actionCount = response;
        }

        setTimeout(() => {
          this.getCharts();
        }, 1000);
      });
  }
}
