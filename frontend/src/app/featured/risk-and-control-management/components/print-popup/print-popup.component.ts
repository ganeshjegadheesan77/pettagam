import { Component, OnInit, Input, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import { HttpService } from 'src/app/core/http-services/http.service';
import { environment } from 'src/environments/environment';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ConfigService } from 'src/config.service';

@Component({
  selector: 'app-print-popup',
  templateUrl: './print-popup.component.html',
  styleUrls: ['./print-popup.component.less']
})

export class PrintPopupComponent implements OnInit,OnChanges {
  @Input('selectReportData') selectReportData:any;
  isPdf = false;
  notPdf = true;
   pdfSrc:string;
  uri;

   position;


  selectReport: any;

    constructor(  
      private httpservice: HttpService, private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService
      
      ) {
    this.uri = configService.config.baseUrl;

       }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    let data = {
   
      collection: "rcm_control_list",
     
    };
    this.httpservice
    .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
    .subscribe((res: any) => {
    
      this.pdfSrc = this.configService.config.baseUrl+ 'reports/' + res.report;
      // this.pdfSrc = 'http://115.112.185.58:3001/api/reports/'+ res.report;
   
      this.isPdf = true;
      this.notPdf = false;
  
      if (res) {
        this.selectReport = res.report;
  
  
        console.log("Report List: ", res);
  
      }
    });
  }
  ngOnChanges(changes: SimpleChanges){



  }
  back(){
    this.isPdf = false;
      this.notPdf = true;
  }
  closeModal(){
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0}
    
  }

}
