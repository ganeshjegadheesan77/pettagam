import { TestBed } from '@angular/core/testing';

import { OmniverseService } from './omniverse.service';

describe('OmniverseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OmniverseService = TestBed.get(OmniverseService);
    expect(service).toBeTruthy();
  });
});
