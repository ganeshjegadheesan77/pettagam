import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  ViewChild,
  ComponentRef,
} from "@angular/core";
import { HttpService } from "../../../../core/http-services/http.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ConstUrls } from "src/app/config/const-urls";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { Router, NavigationEnd } from "@angular/router";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { AngularEditorConfig } from '@kolkov/angular-editor';
// import { error } from 'protractor';
declare var $;

@Component({
  selector: "app-risk-definition-form",
  templateUrl: "./risk-definition-form.component.html",
  styleUrls: ["./risk-definition-form.component.less"],
})
export class RiskDefinitionFormComponent implements OnInit, OnChanges {
  @Input("riskData") riskData: string;
  // tslint:disable-next-line: no-output-native
  @Output() closedRiskDefForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  disableOwner = false;
  modalRef: BsModalRef;
  // ngZone
  masterStatus: any[];
  masterSource: any[];
  riskId: any;
  position;
  riskForm: FormGroup;
  submitted = false;
  riskControlMatrixData: any;
  riskStatusMaster: any;
  riskSourceMaster: any;
  riskMasterDepartment: any;
  riskCategoryMaster: any;
  riskDepartment: any;
  riskProcess: any;
  riskApproch: any;
  riskArea: any;
  selectedStatus: any;
  selectedControlType: any;
  selectedDepartment: any = "Finance";
  selectedProcess: any;
  success = false;
  riskControls = [];
  allControls = [];
  filteredControls = [];
  assignedControls: any = [];
  curVal: any;
  controlTypes: any;
  newCIds: any = [];
  systemUsers: any = [];
  deletedControls: any = [];
  riskLastChangeDate: any;
  riskIdentifiedDate: any;
  riskSubmittedDate: any;
  riskLastApprovedDate: any;

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private router: Router,
    private RCMservice: RiskAndControlManagementService,
    private mScrollbarService: MalihuScrollbarService
  ) {
    // this.ref.destroy();
  }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    //console.log("Init Called");
    this.httpservice.get(ConstUrls.getAllUsers).subscribe((response: any) => {
      if (response) {
        this.systemUsers = response;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log("ngOnChanges called");
    if (changes.riskData.currentValue) {
      this.riskData = changes.riskData.currentValue;
      this.initForm();
      if (this.riskData) {
        this.getOneRisk();
      }

      this.httpservice
        .get(ConstUrls.masterRiskStatus)
        .subscribe((response: any) => {
          if (response) {
            this.masterStatus = response;
          }
        });

      this.httpservice.get(ConstUrls.riskSource).subscribe((response: any) => {
        if (response) {
          this.masterSource = response;
        }
      });

      this.httpservice
        .get(ConstUrls.riskDepartment)
        .subscribe((response: any) => {
          if (response) {
            this.riskMasterDepartment = response;
          }
        });

      this.httpservice.get(ConstUrls.riskProcess).subscribe((response: any) => {
        if (response) {
          this.riskProcess = response;
        }
      });

      this.httpservice
        .get(ConstUrls.riskCategory)
        .subscribe((response: any) => {
          if (response) {
            this.riskCategoryMaster = response;
          }
        });

      this.httpservice.get(ConstUrls.riskArea).subscribe((response: any) => {
        if (response) {
          this.riskArea = response;
        }
      });

      this.httpservice.get(ConstUrls.riskApproch).subscribe((response: any) => {
        if (response) {
          this.riskApproch = response;
        }
      });

      this.httpservice.get(ConstUrls.controlType).subscribe((response: any) => {
        if (response) {
          this.controlTypes = response;
        }
      });

      // let payload = { riskId: this.riskData["id"] };
      // this.httpservice
      //   .securePost(ConstUrls.getControlsAssigned, payload)
      //   .subscribe((response: any) => {
      //     if (response) {
      //       this.assignedControls = response;
      //     }
      //   });
    }

    if (this.riskData) {
      this.selectedControlType = this.riskData["controlType"];
      this.selectedDepartment = this.riskData["controlDepartment"];
      this.selectedProcess = this.riskData["controlProcess"];
    }

    //console.log("this.riskData:", this.riskData);
    if (this.riskData["id"]) {
      let payload = { riskId: this.riskData["id"] };
      this.httpservice
        .securePost(ConstUrls.getControlsAssigned, payload)
        .subscribe((response: any) => {
          if (response) {
            //console.log("assignedControls:", response);
            this.assignedControls = response;
            this.filterControls();
          }
        });
    }

    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        this.allControls = response;
        this.filteredControls = response;
        this.filterControls();
      }
    });

    let bsInlineValue = new Date();
    this.datatable();
    $(":checkbox").attr("checked", false);
  }

  filterControls() {
    if (this.allControls) {
      let selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );

    }
  }
  addControl() {
    let table = $('#riskWFTable').DataTable();
    let _this = this;
    table.$('input:checked').each(function () {
      let cId = $(this).attr('value')
      _this.newCIds.push(cId);
      let assignedControl = _this.allControls.find(
        (control) => control.controlId === cId
      );
      _this.assignedControls.push(assignedControl);
    });
    this.filterControls();
    // $(":checkbox").attr("checked", false);
    table.destroy();
    this.datatable();
  }
  selectUser(user) {
    this.riskForm.value.riskOwner = user;
    // this.riskForm.value.riskOwnerId = userId;
  }
  ownerSelect(ev) {
    this.disableOwner = ev.target.checked;
    if (ev.target.checked) {
      this.riskForm.value.riskOwner = localStorage.getItem("username");
      this.riskForm.value.riskSubmitter = localStorage.getItem("username");
    }
  }
  clickMe() { }
  closeModalWithoutSaving() {
    this.submitted = false;
    this.assignedControls = this.assignedControls.filter(
      (controls) => this.newCIds.indexOf(controls.controlId) === -1
    );
    this.filterControls();
    this.deletedControls = [];
    this.newCIds = [];
    this.closedRiskDefForm.emit(false);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 };
  }
  closeModal() {
    this.submitted = false;
    this.assignedControls = this.assignedControls.filter(
      (controls) => this.newCIds.indexOf(controls.controlId) === -1
    );
    this.filterControls();
    this.newCIds = [];
    this.closedRiskDefForm.emit(true);
    $(".pop-up-form").addClass("d-none");
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedControls.push(controlId);
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== controlId
        );
        this.filterControls();
        let table = $('#riskWFTable').DataTable();
        table.destroy();
        this.datatable();
        const initialState = {
          title: "Success",
          content: "Control Removed!",
          link: "Ok",
        };
        this.modalRef = this.modalService.show(DefaultModalComponent, {
          initialState,
          class: "success-class",
          backdrop: "static",
          keyboard: false,
        });
      }
    });
  }

  callControls() {
    alert("tab called");
  }

  getOneRisk() {
    let payload = { riskId: this.riskData["id"] };
    this.httpservice
      .securePost(ConstUrls.getOneRisk, payload)
      .subscribe(async (response) => {
        //console.log("getOneRisk", response);
        this.riskControlMatrixData = await response;
        let changedDateFormat = new Date(
          this.riskControlMatrixData.riskIdentifiedDate
        );
        this.initForm();
        this.riskForm.patchValue(response);
        this.disableOwner = response["riskOwnerFlag"];
        if (response["controlId"].length) {
          //console.log("One risk details:", response);
          this.riskForm.patchValue({
            addControlFlag: true,
          });
        }
        // this.riskForm.patchValue({'riskIdentifiedDate':new Date(this.riskControlMatrixData.riskIdentifiedDate) , 'riskSubmittedDate':new Date(this.riskControlMatrixData.riskSubmittedDate) , 'riskLastChangeDate':new Date(this.riskControlMatrixData.riskLastChangeDate) ,'riskLastApprovedDate':new Date(this.riskControlMatrixData.riskLastApprovedDate)});
        this.riskForm.patchValue({
          riskIdentifiedDate: this.riskControlMatrixData.riskIdentifiedDate
            ? new Date(this.riskControlMatrixData.riskIdentifiedDate)
            : new Date(),
          riskSubmittedDate: this.riskControlMatrixData.riskSubmittedDate
            ? new Date(this.riskControlMatrixData.riskSubmittedDate)
            : new Date(),
          riskLastChangeDate: this.riskControlMatrixData.riskLastChangeDate
            ? new Date(this.riskControlMatrixData.riskLastChangeDate)
            : new Date(),
          riskLastApprovedDate: this.riskControlMatrixData.riskLastApprovedDate
            ? new Date(this.riskControlMatrixData.riskLastApprovedDate)
            : new Date(),
        });
      });
  }
  get f() {
    return this.riskForm.controls;
  }
  initForm() {
    this.riskForm = this.formBuilder.group({
      riskId: ["", []],
      controlId: [[]],
      riskStatus: ["", []],
      riskName: ["", [Validators.required]],
      riskDescription: ["", []],
      riskSource: ["", []],
      riskDepartment: ["", []],
      riskProcess: ["", []],
      riskCategory: ["", []],
      riskArea: ["", []],
      riskApproach: ["", []],
      riskOwner: ["", []],
      riskOwnerFlag: [[]],
      riskPotentialoutcome: ["", []],
      riskIdentifiedDate: ["", []],
      riskSubmittedDate: ["", []],
      riskLastChangeDate: ["", []],
      riskLastApprovedDate: ["", []],
      riskOwnerId: ["", []],
      riskSubmitter: ["", []],
      riskSubmitterId: ["", []],
      riskApprovedBy: ["", []],
      riskApproverId: ["", []],
      addControlFlag: [false, []],
    });
  }
  //{value: '', disabled: true}
  async onSave() {
    const ids = [];
    await this.assignedControls.forEach((element) => {
      ids.push(element.controlId);
    });
    this.riskForm.value.riskLastChangeDate = new Date();
    this.riskForm.value.controlId = await ids;

    this.submitted = true;
    // stop here if form is invalid
    // if (this.riskForm.invalid) {
    //   console.log("failed");
    //   return;
    // }
    /*console.log(
      "this.riskForm.value.controlId:",
      this.riskForm.value.controlId
    ); */
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    await this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.updateRcmRisk, this.riskForm.value)
          .subscribe((response: any) => {
            if (response) {
              this.RCMservice.sendCount(true);
              if (this.deletedControls) {
                this.httpservice.securePost(
                  ConstUrls.removeOneRiskFromControls,
                  {
                    riskId: this.riskData["id"],
                    controlId: this.deletedControls,
                  },
                  (res) => {
                    this.RCMservice.sendCount(true);
                  }
                );
              }
              const initialState = {
                title: "Success",
                content:
                  "Risk : " + this.riskData["id"] + " updated Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.closeModal();
            }
          }, error => {
            if (error && error.error.message == "jwt expired") {
              this.httpservice.manageJwtExpired();
            }
          });
      }
    });

  }

  datatable() {
    setTimeout(() => {
      $('#riskWFTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '100px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',

    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [

        'redo',



        'strikeThrough',
        'subscript',
        'superscript',

        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };


}