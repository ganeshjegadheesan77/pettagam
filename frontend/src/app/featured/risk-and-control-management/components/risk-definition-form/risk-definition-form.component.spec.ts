import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskDefinitionFormComponent } from './risk-definition-form.component';

describe('RiskDefinitionFormComponent', () => {
  let component: RiskDefinitionFormComponent;
  let fixture: ComponentFixture<RiskDefinitionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskDefinitionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskDefinitionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
