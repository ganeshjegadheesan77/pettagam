import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { Subscription } from "rxjs";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";

@Component({
  selector: "app-risk-count",
  templateUrl: "./risk-count.component.html",
  styleUrls: ["./risk-count.component.less"],
})
export class RiskCountComponent implements OnInit {
  totalRisks: any;
  subscription: Subscription;

  constructor(
    private httpservice: HttpService,
    RCMservice: RiskAndControlManagementService
  ) {
    this.subscription = RCMservice.getCount().subscribe((message) => {
      if (message) {
        this.count();
      }
    });
  }

  ngOnInit() {
    this.count();
  }
  count() {
    this.httpservice
      .secureGet(ConstUrls.riskCount)
      .subscribe((response: any) => {
        if (!response) {
          this.totalRisks = 0;
        }
        this.totalRisks = response;
        //console.log("Risk Count: ", this.totalRisks);
      });
  }
}
