import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskCountComponent } from './risk-count.component';

describe('RiskCountComponent', () => {
  let component: RiskCountComponent;
  let fixture: ComponentFixture<RiskCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
