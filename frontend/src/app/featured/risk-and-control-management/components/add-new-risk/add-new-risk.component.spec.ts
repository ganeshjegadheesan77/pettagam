import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewRiskComponent } from './add-new-risk.component';

describe('AddNewRiskComponent', () => {
  let component: AddNewRiskComponent;
  let fixture: ComponentFixture<AddNewRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
