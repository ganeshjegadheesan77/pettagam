import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { AngularEditorConfig } from '@kolkov/angular-editor';
declare var $;
@Component({
  selector: "app-add-new-risk",
  templateUrl: "./add-new-risk.component.html",
  styleUrls: ["./add-new-risk.component.less"],
})
export class AddNewRiskComponent implements OnInit {
  @Output() closedAddNewRiskForm: EventEmitter<boolean> = new EventEmitter<
    boolean
  >();
  @ViewChild("addNewRiskTab", { static: false }) addNewRiskTab: TabsetComponent;
  modalRef: BsModalRef;
  position;
  masterStatus: any;
  riskForm: FormGroup;
  masterSource: any;
  riskMasterDepartment: any;
  riskProcess: any;
  riskCategoryMaster: any;
  riskArea: any;
  riskApproch: any;
  controlTypes: any;
  submitted: boolean;
  identifiedDate: string;
  generatedRiskId: string;
  generatedControlId: string;
  riskSubmittedDate: string;
  submit = false;
  assignedControls: any = [];
  newCIds: any = [];
  allControls = [];
  filteredControls = [];
  disableOwner = true;
  systemUsers: any = [];
  riskIds: Array<string> = [];

  constructor(
    private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private RCMservice: RiskAndControlManagementService,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.initForm();
    // getAllRecords
    this.httpservice.get(ConstUrls.getAllRecords).subscribe((response: any) => {
      if (response) {
        let temp = response;
        for (const obj of temp) {
          this.riskIds.push(obj.riskId);
        }
      }
    });
    this.httpservice.get(ConstUrls.getAllUsers).subscribe((response: any) => {
      if (response) {
        this.systemUsers = response;
      }
    });
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        console.log(response);
        this.allControls = response;
        this.filteredControls = response;
        // console.log("filteredControls: ", this.filteredControls);
      }
    });
    this.httpservice
      .get(ConstUrls.masterRiskStatus)
      .subscribe((response: any) => {
        if (response) {
          this.masterStatus = response;
        }
      });

    this.httpservice.get(ConstUrls.riskSource).subscribe((response: any) => {
      if (response) {
        this.masterSource = response;
      }
    });

    this.httpservice
      .get(ConstUrls.riskDepartment)
      .subscribe((response: any) => {
        if (response) {
          this.riskMasterDepartment = response;
        }
      });

    this.httpservice.get(ConstUrls.riskProcess).subscribe((response: any) => {
      if (response) {
        this.riskProcess = response;
      }
    });

    this.httpservice.get(ConstUrls.riskCategory).subscribe((response: any) => {
      if (response) {
        this.riskCategoryMaster = response;
      }
    });

    this.httpservice.get(ConstUrls.riskArea).subscribe((response: any) => {
      if (response) {
        this.riskArea = response;
      }
    });

    this.httpservice.get(ConstUrls.riskApproch).subscribe((response: any) => {
      if (response) {
        this.riskApproch = response;
      }
    });

    this.httpservice.get(ConstUrls.controlType).subscribe((response: any) => {
      if (response) {
        this.controlTypes = response;
      }
    });

    this.addnewdatatable();
  }
  deleteControl(id) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== id
        );
        this.filterControls();
        let table = $('#addnewriskTable').DataTable();
        table.destroy();
        this.addnewdatatable()
      }
    });
  }
  filterControls() {
    if (this.allControls.length) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
    }
  }
  addControl() {
    let table = $('#addnewriskTable').DataTable();
    let arr = [];
    table.$('input:checked').each(function () {
      arr.push($(this).attr('value'))
    })
    let flag = [];
    this.assignedControls.forEach((ctrl) => {
      flag.push(ctrl.controlId);
    })
    if (arr.length > 0) {
      arr.forEach(x => {
        this.allControls.forEach((ctrl) => {
          if (x == ctrl.controlId && (flag.indexOf(x) === -1)) {
            this.assignedControls.push(ctrl);
          }
        });
      })
      this.filterControls();
      // $(":checkbox").attr("checked", false);
      table.destroy();
      this.addnewdatatable()
    }
  }
  selectUser(user) {
    this.riskForm.value.riskOwner = user;
    // this.riskForm.value.riskOwnerId = userId;
  }
  ownerSelect(ev) {
    this.disableOwner = ev.target.checked;
    if (ev.target.checked) {
      this.riskForm.value.riskOwner = localStorage.getItem("username");
      this.riskForm.value.riskSubmitter = localStorage.getItem("username");
    }
  }

  createRiskId() {
    //create 3 digit num from 100 to 999(both inclusive)
    let riskId = "R-" + Math.floor(Math.random() * (999 - 100 + 1) + 100);
    if (this.riskIds.indexOf(riskId) === -1) {
      this.generatedRiskId = riskId;
      return;
    } else {
      this.createRiskId();
    }

  }

  onSave() {
    this.submit = true;
    const ids = [];
    this.assignedControls.forEach((element) => {
      ids.push(element.controlId);
      //console.log(ids);
    });

    //console.log("this.riskForm.value.controlId", this.riskForm.value.controlId);
    // stop here if form is invalid
    if (this.riskForm.invalid) {
      //console.log("failed");
      return;
    }
    // this.generatedRiskId = "R-" + Math.floor(Math.random() * 100) + 1;
    this.createRiskId();
    // this.generatedControlId = "C-" + Math.floor(Math.random() * 1000) + 1;
    // this.identifiedDate = new Date(this.riskForm.get('riskIdentifiedDate').value).toLocaleDateString();
    // if(this.identifiedDate === 'Invalid Date'){this.identifiedDate = new Date().toLocaleDateString()}

    // this.riskSubmittedDate = new Date(this.riskForm.get('riskSubmittedDate').value).toLocaleDateString();
    // if(this.riskSubmittedDate === 'Invalid Date'){this.riskSubmittedDate = new Date().toLocaleDateString()}

    // tslint:disable-next-line: max-line-length
    if (ids.length) {
      this.riskForm.patchValue({
        controlId: ids,
        addControlFlag: true,
      });
    }
    this.riskForm.patchValue({
      riskOwnerFlag: true,
      riskId: this.generatedRiskId,
      riskIdentifiedDate: new Date(),
    });
    // this.riskForm.value.controlId = ids;
    //console.log("Risk Form Details: ", this.riskForm.value);
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    ///start
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.httpservice
          .securePost(ConstUrls.saveRiskWithControl, this.riskForm.value)
          .subscribe((response: any) => {
            if (response) {
              this.RCMservice.sendCount(true);

              this.submitted = true;
              const initialState = {
                title: "Success",
                content:
                  "Risk : " + this.generatedRiskId + " Created Successfully!",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.closeModal();
            }
          });
      }
    });
    ////end
  }
  get f() {
    return this.riskForm.controls;
  }
  initForm() {
    this.riskForm = this.formBuilder.group({
      riskId: ["", []],
      controlId: [[]],
      riskName: ["", [Validators.required]],
      riskStatus: ["", []],
      riskDescription: ["", []],
      riskSource: ["", []],
      riskDepartment: ["", []],
      riskProcess: ["", []],
      riskCategory: ["", []],
      riskArea: ["", []],
      riskApproach: ["", []],
      riskOwner: ["", []],
      riskOwnerFlag: [true, []],
      riskPotentialoutcome: ["", []],
      riskIdentifiedDate: ["11/11/1111", []],
      riskSubmittedDate: ["11/11/1111", []],
      riskLastChangeDate: ["11/11/1111", []],
      riskLastApprovedDate: ["11/11/1111", []],
      riskOwnerId: ["", []],
      riskSubmitter: ["", []],
      riskSubmitterId: ["", []],
      riskApprovedBy: ["", []],
      riskApproverId: ["", []],
      addControlFlag: [false, []],
    });
  }

  closeModal() {
    this.submit = false;
    this.assignedControls = [];
    // $(".scroll-container").scrollTop(0);
    this.closedAddNewRiskForm.emit(true);
    $(".pop-up-form").addClass("d-none");
    // this.addNewRiskTab.tabs[0].active = true;
  }
  closeModalWithoutSaving() {
    this.submit = false;
    this.assignedControls = [];
    this.filteredControls = this.allControls;
    this.closedAddNewRiskForm.emit(false);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : { x: 0, y: 0 }
  }

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '100px',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',

    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      [

        'redo',



        'strikeThrough',
        'subscript',
        'superscript',

        'justifyFull',
        'indent',
        'outdent',
        'heading',
        'fontName'
      ],
      [
        'fontSize',
        'textColor',
        'backgroundColor',
        'customClasses',
        'link',
        'unlink',
        'insertImage',
        'insertVideo',
        'insertHorizontalRule',
        'removeFormat',
        'toggleEditorMode'
      ]
    ]
  };

  addnewdatatable() {
    setTimeout(() => {
      $('#addnewriskTable').DataTable({
        "oLanguage": { "sSearch": "" },
        "language": {
          "searchPlaceholder": "Search"
        },
        "destroy": true,
        "fixedHeader": true,
        "responsive": true,
        "bPaginate": true,
        "lengthMenu": [[5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]],
        "bInfo": true,
        "searching": true,
        "lengthChange": true,
        "columnDefs": [{
          'targets': [0],
          'orderable': false,
          'className': 'dt-body-center'
        }],
      });
    }, 500)
  };


}
