import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlCountComponent } from './control-count.component';

describe('ControlCountComponent', () => {
  let component: ControlCountComponent;
  let fixture: ComponentFixture<ControlCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
