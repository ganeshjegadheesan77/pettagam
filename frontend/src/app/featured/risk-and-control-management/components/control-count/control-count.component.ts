import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { Subscription } from "rxjs";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";

@Component({
  selector: "app-control-count",
  templateUrl: "./control-count.component.html",
  styleUrls: ["./control-count.component.less"],
})
export class ControlCountComponent implements OnInit {
  totalControls: any;
  subscription: Subscription;

  constructor(
    private httpservice: HttpService,
    RCMservice: RiskAndControlManagementService
  ) {
    this.subscription = RCMservice.getCount().subscribe((message) => {
      if (message) {
        this.count();
      }
    });
  }

  ngOnInit() {
    this.count();
  }
  count() {
    this.httpservice
      .secureGet(ConstUrls.controlCount)
      .subscribe((response: any) => {
        if (!response) {
          this.totalControls = 0;
        }
        this.totalControls = response;
        //console.log("Total Controls: ", this.totalControls);
      });
  }
}
