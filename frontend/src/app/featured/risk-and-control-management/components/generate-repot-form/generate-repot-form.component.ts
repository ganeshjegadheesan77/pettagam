import { Component, OnInit,OnChanges,Input, SimpleChanges } from '@angular/core';
import { ConstUrls } from "src/app/config/const-urls";
import { FormGroup, FormBuilder } from "@angular/forms";
import { HttpService } from 'src/app/core/http-services/http.service';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { environment } from "src/environments/environment";
import { ConfigService } from 'src/config.service';

declare var $;
@Component({
  selector: 'app-generate-repot-form',
  templateUrl: './generate-repot-form.component.html',
  styleUrls: ['./generate-repot-form.component.less']
})
export class GenerateRepotFormComponent implements OnInit,OnChanges {
  @Input('selectReportData') selectReportData:any;
  riskForm: FormGroup;
  isPdf = false;
  uri;

  position;
  notPdf = true;
  savingGenerateReport: boolean = false;
  generateReport: boolean = true;
  riskArea:any;
  riskProcess:any;
  riskDepartment:any;
  masterRiskStatus:any;
  masterRiskSource:any;
  getAllUsers:any;
  selectReport: any;
  pdfSrc: string;
  constructor(private httpservice: HttpService,private formBuilder: FormBuilder,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService
    ) {

    this.uri = configService.config.baseUrl;

     }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    this.initForm();
    
    this.httpservice.get(ConstUrls.riskArea).subscribe((response:any) => {
      this.riskArea = response;
      });

      this.httpservice.get(ConstUrls.riskProcess).subscribe((response:any) => {
        this.riskProcess = response;
        });
        this.httpservice.get(ConstUrls.riskDepartment).subscribe((response:any) => {
          this.riskDepartment = response;
          });
          this.httpservice.get(ConstUrls.masterRiskStatus).subscribe((response:any) => {
            this.masterRiskStatus = response;
            });
            this.httpservice.get(ConstUrls.masterRiskSource).subscribe((response:any) => {
              this.masterRiskSource = response;
              });
              this.httpservice.get(ConstUrls.getAllUsers).subscribe((response:any) => {
                this.getAllUsers = response;
                });

             
    this.savingGenerateTab()
  }

  ngOnChanges(changes: SimpleChanges){
    if (changes.selectReportData){
      //console.log(changes.selectReportData)
      this.pdfSrc = this.configService.config.baseUrl + 'reports/'+changes.selectReportData.currentValue ;

      // this.pdfSrc = 'http://115.112.185.58:3000/api/reports/'+changes.selectReportData.currentValue;
    }
    }


  onSave() {
    // let data = this.riskForm.value
    // data['collection'] = "risk"
    let param = [] 
    let rs = Object.entries(this.riskForm.value).reduce((o, [k, v]) => {
        if(v) {
                param.push({name: k, value: v})
   }
     return param;
    }, {});


    let arr = Object.values(rs)
    let data = { 
      "collection" : "rcm_risk_list",
      "params" : arr 
      
    }
      //console.log(data);
      this.httpservice
      .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
      .subscribe((res: any) => {
      
        // this.pdfSrc = 'http://115.112.185.58:3000/api/reports/'+ res.report;
      this.pdfSrc =  this.configService.config.baseUrl+ 'reports/'+res.report ;

        // window.open(this.pdfSrc,"_blank")
        // console.log('this.pdfSrc', this.pdfSrc)
        this.isPdf = true;
        this.notPdf = false;

        if (res) {
          this.selectReport = res.report;


          //console.log("Report List: ", res);

        }
      });
  
    }
    back(){
      this.isPdf = false;
        this.notPdf = true;
    }

  
  initForm() {
    this.riskForm = this.formBuilder.group({
   
    
      riskStatus: ["", []],
      riskSource: ["", []],
      riskDepartment: ["", []],
      riskProcess: ["", []],
      riskArea: ["", []],
      riskOwner: ["", []],
      riskSubmittedDate:["", []],
     
    });
  }

  closeModal(){

    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.position = this.position ? undefined : {x: 0, y: 0}
  }
  showSavingReport(){
    this.savingGenerateReport = true;
    this.generateReport = false;
  }
  savebtnReport() {
    this.savingGenerateReport = false;
    this.generateReport = true;
  }
  savingGenerateTab() {

  $("#savingGenerateTable").DataTable({
    processing: true,
    fixedHeader: true,
    autoWidth: false,
    scrollY: "100vh",
    scrollX: false,
    scrollCollapse: true,
    fixedColumns:   {
      leftColumns: 0,
      rightColumns: 0,
      heightMatch: true
   },
    bPaginate: true,
   
    lengthMenu: [
      [5, 10, 20, 50, 100, -1],
      [5, 10, 20, 50, 100, "All"]
    ],
    bInfo: true,
    searching: false,
  });

}
}
