import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateRepotFormComponent } from './generate-repot-form.component';

describe('GenerateRepotFormComponent', () => {
  let component: GenerateRepotFormComponent;
  let fixture: ComponentFixture<GenerateRepotFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateRepotFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateRepotFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
