import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DataTableService } from '../../../services/dataTable.service';
import { HttpService } from "src/app/core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/featured/internal-audit/components/audit-universe/data.service';
import { DataTableFuncService } from '../../../services/dataTableFunc.service';
import * as moment from 'moment';
import { RiskAndControlManagementService } from '../../services/risk-and-control-management.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

declare var $;
@Component({
  selector: 'app-control-work-flow',
  templateUrl: './control-work-flow.component.html',
  styleUrls: ['./control-work-flow.component.less']
})
export class ControlWorkFlowComponent implements OnInit {
  modalRef: BsModalRef;

  public formControlFlowRCM =  false;
  public loadcityId = false;

  // private selectedControlId =  false;
  selectedControlId : any;
  selectedControlkId : any;
  private addNewControlFormComponent=false;
  subscription: Subscription;

  controlList: any;
  dataTable: any;
  controlWFRCMTableHeaders: any;
  showHideHeadersControlWF: any;
  isNewWF: any;
  redirectControlId;
  redirectWorkflowId;
  area: any;
  // controlList: any[]=[];
  // addNewControlFormComponent: any;
  constructor(private chRef: ChangeDetectorRef,
    private httpservice: HttpService,
    private dataService: DataService,
    private RCMservice: RiskAndControlManagementService,



    private dataTableService: DataTableService,
    private modalService: BsModalService, 
    private activatedRoute: ActivatedRoute,
    private dataTableFuncService: DataTableFuncService) {
      this.subscription = this.RCMservice.refreshCWF().subscribe(message => {
        if (message) {
          console.log('Message: ', message);
          let table = $('#controlWorkFlowRCM').DataTable();
          table.destroy();
          this.ngOnInit();
        } 
      });
     }

     ngOnInit() {

      // this.httpservice.get(ConstUrls.getAreaInRisk).subscribe((response: any) => {
      //   if (response) {
      //     this.area = response;
      //     console.log('Master Fre 1: ', this.area);
      //   }
      // });

      this.activatedRoute.queryParams.subscribe(params => {
        if (params['cwfId']) {
          this.redirectWorkflowId = params['cwfId'];
        }
        if (params['controlId']) {
          this.redirectControlId= params['controlId'];
        }
      });

      // this.getControlList()
      let table = $("#controlWorkFlowRCM").DataTable();
      table.destroy();
      this.outsideClick();
     
      this.controlWFRCMTableHeaders = [];
      this.showHideHeadersControlWF = [];
      this.httpservice.get(ConstUrls.getAllcontrolWFlist).subscribe((response: any) => {
        if (response) {
          this.controlList = response;
          // console.log('Control list: ', this.controlList);
          this.controlList.forEach(element => {
            element.controldata.forEach(cd => {
              element.period = moment(cd.exStart).format('DD/MM/YYYY')+'-'+moment(cd.exEnd).format('DD/MM/YYYY');
              element.frequency = cd.frequency;
              element.workflowName = cd.controlWorkFlowName;
              element.assignee = cd.exAssignee;
              element.assigneeCount = cd.exAssignee.length-1;
              element.approver = cd.apAssignee;
              element.approverCount = cd.apAssignee.length-1;

          // var date = new Date(this.issueTableList[i].dateIdentified);
            });
          });
          console.log('Control List FInal:' , this.controlList)
        }
        this.controlWFRCMTableHeaders = [
          // "Area",
          "Ctl ID",
          "Control Name",
          "Control Desc",
          "Work Flow ID",
          "Work Flow Name",
  
          "Frequency",
          "Scope Period",
          "Assignee",
          "Approver",
        ];
        this.showHideHeadersControlWF = this.controlWFRCMTableHeaders;
        this.controlWFRCMdatatable();

        if(this.redirectWorkflowId){
          this.controlWF(this.redirectWorkflowId);
        }
        else if(this.redirectControlId){
          this.loadMyCityId(this.redirectControlId);          
        }
  });
  
  
    }

  getControlFormStatus(event) {
    if (event === true) {
      this.formControlFlowRCM = false;
      this.ngOnInit();
    }
  }

  getControlList() {
    let table = $("#controlWorkFlowRCM").DataTable();
    table.destroy();
    // this.controlListRCMTableHeaders = [];
    // this.showHideHeadersCL = [];
    this.httpservice.get(ConstUrls.getAllcontrolWFlist).subscribe((response: any) => {
      if (response) {

        this.controlList = response;
        
       
       
        //console.log('control list',this.controlList)

        // }
    

      }
      // this.controlListRCMTableHeaders = [
      //   "Control ID",
      //   "Control Name",
      //   "Control Description",
      //   "Control Department",
      // ];
      // this.showHideHeadersCL = this.controlListRCMTableHeaders;
      // this.datatableTwo();
    });
  }
  // controlClick(id){
  //   this.selectedControlId = true;
  //   this.addNewControlFormComponent = false;
  //   this.selectedControlId = { id: id, renderFrom: "CONTROL LIST" };
  //   this.formControlFlowRCM = false;
  //   $(".scroll-container").scrollTop(0);
  //   $(".pop-up-form").removeClass("d-none");
  // }
  loadMyCityId(id) {
    this.loadcityId = true;
    this.formControlFlowRCM = false;
this.addNewControlFormComponent = false;
    this.selectedControlId = { id: id, renderFrom: "CONTROL LIST" };
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  controlWF(controlId){
    if(typeof(controlId)=='object'){
      //console.log('controlId',controlId)
      controlId="CWF-"+ controlId.controlId.split('-')[1]
    }
    this.loadcityId = false;

    this.selectedControlkId = false;
    this.formControlFlowRCM = true;
  this.selectedControlkId = controlId;
  // this.isNewWF = isNewWF;
    // this.addNewControlFormComponent= false;
    $(".pop-up-form").removeClass("d-none");
  }
//  wfClick(){
//   this.loadcityId = false;

//     this.selectedControlId = false;
//     this.formControlFlowRCM = true;
//     $(".scroll-container").scrollTop(0);
//     $(".pop-up-form").removeClass("d-none");
//   }


controlWFRCMdatatable() {
    
  this.chRef.detectChanges();
  const RCMcontrolWorkFlowTable: any = $("#controlWorkFlowRCM");
  const controlWorkFlowRCM = this.dataTableService.initializeTable(RCMcontrolWorkFlowTable, `rcmcontrolworkFlow`)
  this.dataTableFuncService.filterDropDown()    
  this.dataTableFuncService.selectAllRows(controlWorkFlowRCM, `rcmControlWF-select-all`)    
  this.dataTableFuncService.columnDropDown(controlWorkFlowRCM)    
  this.dataTableFuncService.expandContainer(controlWorkFlowRCM)  
  $("#controlWorkFlowRCM tbody").on(
    "change",
    'input[type="checkbox"]',
    function() {
      if (!this.checked) {
        $("#rcmControlWF-select-all").prop("checked", false);
        var el = $("#rcmControlWF-select-all").get(0);
        if (el && el.checked && "indeterminate" in el) {
          el.indeterminate = true;
        }
      }
      if (
        $("#controlWorkFlowRCM tr td input[type='checkbox']:checked").length ==
        $("#controlWorkFlowRCM tr td input[type='checkbox']").length
      ) {
        $("#rcmControlWF-select-all").prop("checked", true);
      }
    }
  );
}
SettingControlWorkFlow(e){
  $('.dropdown-menu-column').toggleClass("show-bar");
  $(".dt-button-collection").hide();
  e.stopPropagation();
}
outsideClick() {
  $("body").on("click", function() {
    $('.dropdown-menu-column').removeClass("show-bar");
  });
}
}
