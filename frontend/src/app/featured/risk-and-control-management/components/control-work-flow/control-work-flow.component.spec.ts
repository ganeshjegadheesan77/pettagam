import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlWorkFlowComponent } from './control-work-flow.component';

describe('ControlWorkFlowComponent', () => {
  let component: ControlWorkFlowComponent;
  let fixture: ComponentFixture<ControlWorkFlowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlWorkFlowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlWorkFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
