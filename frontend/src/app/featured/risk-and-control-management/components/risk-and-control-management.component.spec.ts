import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskAndControlManagementComponent } from './risk-and-control-management.component';

describe('RiskAndControlManagementComponent', () => {
  let component: RiskAndControlManagementComponent;
  let fixture: ComponentFixture<RiskAndControlManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskAndControlManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskAndControlManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
