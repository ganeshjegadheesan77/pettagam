import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRiskLibraryComponent } from './view-risk-library.component';

describe('ViewRiskLibraryComponent', () => {
  let component: ViewRiskLibraryComponent;
  let fixture: ComponentFixture<ViewRiskLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRiskLibraryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRiskLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
