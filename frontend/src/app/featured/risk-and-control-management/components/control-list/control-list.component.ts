import { Component, OnInit, ChangeDetectorRef } from "@angular/core";

import { ConstUrls } from "src/app/config/const-urls";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { DefaultModalComponent } from "../../../../layout/modal/default-modal/default-modal.component";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { RiskAndControlManagementService } from "../../services/risk-and-control-management.service";
import { HttpService } from 'src/app/core/http-services/http.service';

declare var $;
@Component({
  selector: "app-control-list",
  templateUrl: "./control-list.component.html",
  styleUrls: ["./control-list.component.less"],
})
export class ControlListComponent implements OnInit {
  modalRef: BsModalRef;
  public printView = false;
  public loadcityId = false;
  private loadComponent = false;
  public addNewControlFormComponent = false;
  selectedControlId: any;
  controlList: any;
  dataTable: any;
  selectReport:any
  isPdf = false;
  notPdf = true;
  controlListRCMTableHeaders: any;
  showHideHeadersCL: any;
  isLoading = false;
  pdfSrc: string;
  constructor(
    private httpservice: HttpService,
    private modalService: BsModalService,
    private chRef: ChangeDetectorRef,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,
    private RCMservice: RiskAndControlManagementService
  ) {}

  ngOnInit() {
    this.getControlList();

    $('[data-toggle="tooltip"]').tooltip();

  }

  getStatusOfAddNewControlForm(event) {
    if (event == true) {
      this.addNewControlFormComponent = false;
      this.loadComponent = false;
      this.getControlList();
    }
  }

  getControlFormStatus(event) {
    if (event === true) {
      this.getControlList();
    }
  }

  getControlList() {
    let table = $("#controlListTable").DataTable();
    table.destroy();
    this.isLoading = true;
    this.controlListRCMTableHeaders = [];
    this.showHideHeadersCL = [];
    this.httpservice.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        this.controlList = response;
      }
      this.controlListRCMTableHeaders = [
        "Control ID",
        "Control Name",
        "Control Description",
        "Control Department",
      ];
      this.showHideHeadersCL = this.controlListRCMTableHeaders;
      this.isLoading = false;
      this.datatableTwo();
    });
  }
  loadMyCityId(id) {
    this.loadcityId = true;
    this.loadComponent = false;
    this.printView = false;
    this.addNewControlFormComponent = false;
    this.selectedControlId = { id: id, renderFrom: "CONTROL LIST" };
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  addNewControlForm() {
    this.addNewControlFormComponent = true;
    this.loadcityId = false;
    this.loadComponent = true;
    this.printView = false;

 
    $(".pop-up-form").removeClass("d-none");
  }


  // let data = {
  //   // "database" : "newRcmDB",
  //   collection: "rcm_control_list",
  //   // "value" : "riskControls"
  // };
  // this.httpservice
  // .secureJavaPostReportApi("GenericReportBuilderService/buildReport", data)
  // .subscribe((res: any) => {
  
  //   this.pdfSrc = 'http://115.112.185.58:3000/api/reports/'+ res.report;
  //   window.open(this.pdfSrc,"_blank")
  //   console.log('this.pdfSrc', this.pdfSrc)
  //   this.isPdf = true;
  //   this.notPdf = false;

  //   if (res) {
  //     // this.selectReport = res.report;


  //     //console.log("Report List: ", res);

  //   }
  // });

  printClick() {
    this.loadComponent = false;
    this.loadcityId = false;
    this.printView = true;


    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
  }

  
  back(){
    this.isPdf = false;
      this.notPdf = true;
  }
  share() {}
  delete(id) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to delete this Record?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        var selectedCtrl = this.controlList.filter(x=>x.controlId == id)[0];
        // for (let i = 0; i < this.controlList.length; i++) {
          if (
            ((selectedCtrl.ctlTestingId &&
              selectedCtrl.ctlTestingId.length) ||
              (selectedCtrl.riskId && selectedCtrl.riskId.length))
          ) {
            var msg = "Control under use! Cannot delete.";
            if(selectedCtrl.ctlTestingId &&
              selectedCtrl.ctlTestingId.length > 0){
                msg = "Control Testing created! Cannot delete";
              }
              else if(selectedCtrl.riskId && selectedCtrl.riskId.length){
                msg = "Control used by risk! cannot delete";
              }
            const initialState = {
              title: "Warning!",
              content: msg,
              link: "Ok",
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: "success-class",
              backdrop: "static",
              keyboard: false,
            });
            this.closeModal();
            return false;
          }
        // }
        this.httpservice
          .securePost(ConstUrls.deleteControl, { controlId: id })
          .subscribe((response: any) => {
            if (response) {
              this.controlList = this.controlList.filter((rec) => {
                if (
                  rec.ctlTestingId &&
                  !rec.ctlTestingId.length &&
                  !rec.riskId.length
                ) {
                  return rec.controlId !== id;
                }
                return true;
              });
              this.RCMservice.sendCount(true);

              const initialState = {
                title: "Success!",
                content: "Record Deleted Successfully!",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.closeModal();
              return false;
            }
          });
      }
    });
  }
  duplicate() {}
  datatableTwo() {
    this.chRef.detectChanges();
    const controlTable: any = $("#controlListTable");
    const controlListRCM = this.dataTableService.initializeTable(
      controlTable,
      `control`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      controlListRCM,
      `rcmcontrolList-select-all`
    );
    this.dataTableFuncService.columnDropDown(
      controlListRCM,
      `control-hideshow`
    );
    this.dataTableFuncService.expandContainer(controlListRCM);
    $("#controlListTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#rcmcontrolList-select-all").prop("checked", false);
          var el = $("#rcmcontrolList-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#controlListTable tr td input[type='checkbox']:checked").length ==
          $("#controlListTable tr td input[type='checkbox']").length
        ) {
          $("#rcmcontrolList-select-all").prop("checked", true);
        }
      }
    );
  }
  closeModal() {
    this.getStatusOfAddNewControlForm(true);
    // this.closedRiskDefForm.emit(true);
    $(".pop-up-form").addClass("d-none");
  }
  SettingControlList(e) {
    $(".dropdown-menu-column").toggleClass("show-bar");
    $(".dt-button-collection").hide();
    e.stopPropagation();
  }
}
