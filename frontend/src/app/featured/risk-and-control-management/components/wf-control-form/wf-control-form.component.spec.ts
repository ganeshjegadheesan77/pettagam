import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WfControlFormComponent } from './wf-control-form.component';

describe('WfControlFormComponent', () => {
  let component: WfControlFormComponent;
  let fixture: ComponentFixture<WfControlFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WfControlFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WfControlFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
