import { Component, OnInit, Input, OnChanges, ViewChild,EventEmitter,Output } from '@angular/core';
import { HttpService } from 'src/app/core/http-services/http.service';
import { ConstUrls } from 'src/app/config/const-urls';
import * as moment from 'moment';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {DefaultModalComponent} from '../../../../layout/modal/default-modal/default-modal.component';
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { ConfigService } from 'src/config.service';
// import { environment} from 'src/environments/environment';
declare var $;
@Component({
  selector: 'app-wf-control-form',
  templateUrl: './wf-control-form.component.html',
  styleUrls: ['./wf-control-form.component.less']
})
export class WfControlFormComponent implements OnInit,OnChanges {
  @ViewChild('wfControlForm', { static: false }) wfControlForm: TabsetComponent;
  @Output()closedControlDefForm: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  controlnWorkflowFormData: any;
  @Input('controlId') controlId:string;
  calendarIconApprovalEnd = true;
  calendarIconApproval = true;
  position;
  calendarIcon = true;
  calendarDetail = false;
  controlTable:any;
  public Editreminder = false;
  approvalSec = false;
  approvalSecEnd = false;
  calendarDetailEnd = false;
  masterSource:any;
  calendarIconEnd = true;
  currentDate = new Date();
  controlTableList:any;
  controlList:any;

  newWF:Boolean;
  form = new FormGroup({
    dateYMD: new FormControl(new Date()),
    dateFull: new FormControl(new Date()),
    dateMDY: new FormControl(new Date()),
    dateRange: new FormControl([
      new Date(),
      new Date(this.currentDate.setDate(this.currentDate.getDate() + 7))
    ])
  });
  modalRef: BsModalRef;
  public validateFlag: string;
  public validateFlag2: string;
  typingName='';
  result: string[];
  keyword = 'UserName';
  userList=[];
  userIdsEx=[];
  userIdsAp=[];
  allControls = [];
  filteredControls = [];
  assignedControls: any = [];
  deletedControls: any = [];

  controlWFId: any;
  masterFrequency: any;
  selectedFrequency: any;
  RCMservice: any;
  constructor(
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService,private httpservice: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private mScrollbarService: MalihuScrollbarService,
    private configService:ConfigService) { }

  ngOnInit() {
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });
    // let now = moment().format('LLLL');
    // console.log('date time: ' , now);
    // this.getControlWFData();
    // this.getControlData();
  }


  ngOnChanges(){
    let now = moment().format('LLLL');
    //console.log('date time: ' , now);
    this.getControlWFData();
    this.getControlData();
    this.httpservice.get(ConstUrls.getMasterCWFrequency).subscribe((response: any) => {
      if (response) {
        this.masterFrequency = response;
        console.log('Master Fre: ', this.masterFrequency);
      }
    });
    // this.datatable();
  }
  onFrequencyChanged(val){
    this.controlnWorkflowFormData.frequency= val;
  }
  getControlWFData(){
    let getControl = {controlWFId: this.controlId};
    //console.log('controlId',this.controlId);
    this.httpservice.securePost(ConstUrls.getOneWF, getControl).subscribe((data:any)=>{
      //console.log('noun',data)

      if(data){
        this.newWF=false;
        this.calendarIconApprovalEnd = false;
        this.calendarIconApproval = false;
        this.calendarIcon = false;
        this.calendarIconEnd = false;
       this.controlnWorkflowFormData = data; 
       this.selectedFrequency = data.frequency;
       //console.log('yes',this.controlnWorkflowFormData )
        // this.date3 = this.datePipe.transform(this.controlnWorkflowFormData.exStart, 'dd.mm.yy');
        this.controlnWorkflowFormData.exStart= new  Date(this.controlnWorkflowFormData.exStart).toUTCString();
        this.controlnWorkflowFormData.exEnd= new  Date(this.controlnWorkflowFormData.exEnd).toUTCString();
        
        this.controlnWorkflowFormData.apStart= new  Date(this.controlnWorkflowFormData.apStart).toUTCString();
        //console.log("1",this.controlnWorkflowFormData.apStart)
        this.controlnWorkflowFormData.apEnd= new  Date(this.controlnWorkflowFormData.apEnd).toUTCString();
        this.getDeadlines();
        //console.log(data);
      }
      else
      if(data==null){
        this.controlnWorkflowFormData=[]
        this.controlnWorkflowFormData.frequency='',
        this.controlnWorkflowFormData.controlWFId= this.controlId;
        this.controlnWorkflowFormData.execution=0;this.controlnWorkflowFormData.approval=0;
        this.controlnWorkflowFormData.exAssignee=[],
        this.controlnWorkflowFormData.apAssignee=[],
        this.controlnWorkflowFormData.corporate='',
        this.controlnWorkflowFormData.companyName='',
        this.controlnWorkflowFormData.buName='',
        this.controlnWorkflowFormData.approvedBy='',
        this.controlnWorkflowFormData.approvedId=''
        this.controlnWorkflowFormData.controlWorkFlowName=''

        this.newWF= true;
        this.calendarIconApprovalEnd = true;
        this.calendarIconApproval = true;
        this.calendarIcon = true;
        this.calendarIconEnd = true;
        this.controlnWorkflowFormData.lastChangeOn= new Date().toLocaleDateString();
        this.controlnWorkflowFormData.addedOn= new Date().toLocaleDateString();
      }
    })
  }

  getDeadlines(){
    this.controlnWorkflowFormData.beforefive= moment(moment(this.controlnWorkflowFormData.exStart).subtract(5, 'days')).format('DD-MMM-YYYY');
    this.controlnWorkflowFormData.beforefiveAp= moment(moment(this.controlnWorkflowFormData.apStart).subtract(5, 'days')).format('DD-MMM-YYYY');

    this.controlnWorkflowFormData.afterfive= moment(moment(this.controlnWorkflowFormData.exStart).add(5, 'days')).format('DD-MMM-YYYY');
    this.controlnWorkflowFormData.afterfiveAp= moment(moment(this.controlnWorkflowFormData.apStart).add(5, 'days')).format('DD-MMM-YYYY');

    this.controlnWorkflowFormData.afterfourteen= moment(moment(this.controlnWorkflowFormData.exStart).add(14, 'days')).format('DD-MMM-YYYY');
    this.controlnWorkflowFormData.afterfourteenAp= moment(moment(this.controlnWorkflowFormData.apStart).add(14, 'days')).format('DD-MMM-YYYY');

  }

  save(){
    if((this.controlnWorkflowFormData.execution+this.controlnWorkflowFormData.approval)>100){
      const initialState = {
              title: 'Alert',
              content: 'Total of Execution and Approval % value is more than 100',
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
        return;     
    }
    if(this.controlnWorkflowFormData.exDuration<0 || this.controlnWorkflowFormData.apDuration<0){
      const initialState = {
              title: 'Alert',
              content: 'Start date should be less than End Date!',
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
        return;     
    }
    if(this.controlnWorkflowFormData.exStart)this.controlnWorkflowFormData.exStart= new Date(this.controlnWorkflowFormData.exStart).toISOString();
    if(this.controlnWorkflowFormData.apStart)this.controlnWorkflowFormData.apStart=new Date(this.controlnWorkflowFormData.apStart).toISOString();
    if(this.controlnWorkflowFormData.exEnd)this.controlnWorkflowFormData.exEnd=new Date(this.controlnWorkflowFormData.exEnd).toISOString();
    if(this.controlnWorkflowFormData.apEnd)this.controlnWorkflowFormData.apEnd=new Date(this.controlnWorkflowFormData.exEnd).toISOString();
    this.controlnWorkflowFormData.lastChangeOn= new Date().toLocaleDateString();
    this.controlnWorkflowFormData.addedOn= new Date().toLocaleDateString();
    if(this.controlnWorkflowFormData.approvedOn) this.controlnWorkflowFormData.approvedOn= new Date(this.controlnWorkflowFormData.approvedOn).toLocaleDateString();
if(this.controlnWorkflowFormData._id)
    delete this.controlnWorkflowFormData['_id']
    //console.log(this.controlnWorkflowFormData);
    var data= this.controlnWorkflowFormData
    //console.log('Daata: ', data);
    var payload={
"exAssignee":data.exAssignee,
"apAssignee": data.apAssignee,
"controlIds": data.controlIds,
"controlWFId": data.controlWFId,
"execution": data.execution,
"frequency":data.frequency,
"exStart":data.exStart,
"exEnd": data.exEnd,
"exDuration": data.exDuration,
"approval": data.approval,
"apStart":data.apStart,
"apEnd": data.apEnd,
"apDuration": data.apDuration,
"corporate":data.corporate,
"companyName": data.companyName,
"buName": data.buName,
"addedOn":data.addedOn,
"lastChangeOn": data.lastChangeOn,
"approvedBy": data.approvedBy,
"approvedId":data.approvedId,
"controlWorkFlowName":data.controlWorkFlowName,

    }
    if(this.newWF){

      const initialState = {
        title: "Confirmation",
        content: `Do you want to save current changes?`,
        link: "Confirm",
        action: "Cancel",
        confirmFlag: false,
      };
      this.modalRef = this.modalService.show(ConfirmPopupComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
      //save new WF
       this.modalRef.content.onClose.subscribe((r) => {
        if (r) {    this.httpservice.securePost(ConstUrls.saveControlWF,payload).subscribe((response:any)=>{
          if(response) { 
           // console.log('check',response)
            //console.log('checkcontrolid',this.controlnWorkflowFormData.controlIds)
  
            if(this.controlnWorkflowFormData.controlIds){
              var l= this.controlnWorkflowFormData.controlIds.length;
              this.controlnWorkflowFormData.controlIds.forEach(x=>{
                // var payload= {"controlId":this.controlId,"controlWFId":this.AWFid}
                var payload= {"controlId":x,"controlWFId":this.controlnWorkflowFormData.controlWFId}
                //console.log("payload",payload)
           this.httpservice.securePost(ConstUrls.updateOneControls,payload).subscribe((response:any)=>{
             if(response){
             //console.log('Added');
             this.newWF= false;
             if(x==this.controlnWorkflowFormData.controlIds[l-1]){
               const initialState = {
                title: 'Success',
                content: 'Control WF saved Successfully',
                link: 'Ok',
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});  
            
              this.closeModal();
     this.closedControlDefForm.emit(true);
  
            }
             }
           })
              })
            }
            
           }
        })
      }}
      )}
  

      
    else{
      this.httpservice.securePost(ConstUrls.updateOneControlWF, this.controlnWorkflowFormData).subscribe((response:any)=>{
        if(response) { 
          if(this.controlnWorkflowFormData.controlIds){
            //console.log("FOrm Data for update:", this.controlnWorkflowFormData)
            var l= this.controlnWorkflowFormData.controlIds.length;
            this.controlnWorkflowFormData.controlIds.forEach(x=>{
              var payload= {"controlId":x,"controlWFId":this.controlnWorkflowFormData.controlWFId}
              //console.log("payload",payload);
         this.httpservice.securePost(ConstUrls.updateOneControls,payload).subscribe((response:any)=>{
           if(response){
           //console.log('Added');
           this.newWF= false;
           if(x==this.controlnWorkflowFormData.controlIds[l-1]){
            //  this.closedAddDefinationActionForm.emit(true);
             const initialState = {
              title: 'Success',
              content: `Control WorkFlow ${this.controlId} saved Successfully`,
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
            
            
          this.closeModal();
   this.closedControlDefForm.emit(true);
          
    }
           }
         })
            })
          }
        
         }
      })
    }
    this.userIdsEx.forEach(element => {
   let assginTaskData = {"database" : this.configService.config.db,"ActionID" : this.controlId,"UserID" : element.UserID ,"StartDate" : moment(this.controlnWorkflowFormData.exStart).format('DD-MMM-YYYY'),"EndDate" : moment(this.controlnWorkflowFormData.exEnd).format('DD-MMM-YYYY'),
   "routerLink":"/rcm/risk-and-control-management","tab":"control","seen":false,"notice":"Reminder","workType":"Execution"
  }
 
  this.httpservice.secureJavaPostApi(ConstUrls.assignActionTask , assginTaskData).subscribe((response: any)=>{
    if(response){
      this.userIdsAp.forEach(el => {
        let assginTaskData = {"database" : this.configService.config.db,"ActionID" : this.controlId,"UserID" : el.UserID ,"StartDate" : moment(this.controlnWorkflowFormData.apStart).format('DD-MMM-YYYY'),"EndDate" : moment(this.controlnWorkflowFormData.apEnd).format('DD-MMM-YYYY'),
        "routerLink":"/rcm/risk-and-control-management","tab":"control","seen":false,"notice":"Reminder","workType":"Approval"
      }
       //console.log('Assign Data: ' , assginTaskData);
       this.httpservice.secureJavaPostApi(ConstUrls.assignActionTask , assginTaskData).subscribe((response: any)=>{
         if(response){
           //console.log('successfully added assignees');
         }
       })
    })
  }
  })
  
});
  }

  removeUser(user,i){
    this.controlnWorkflowFormData.exAssignee.splice(i,1);
    var k = this.userIdsEx.findIndex(x=>x.UserName===user);
  if (k>-1)this.userIdsEx.splice(k,1)
  }
  removeUserAp(user,i){
    this.controlnWorkflowFormData.apAssignee.splice(i,1);
    var k = this.userIdsAp.findIndex(x=>x.UserName===user);
  if (k>-1)this.userIdsAp.splice(k,1)
  }
  openSearch() {
    this.validateFlag = 'ValidatorToken';
  }
  openSearchAp() {
    this.validateFlag2 = 'ValidatorToken';
  }
  closeSearch() {
    this.validateFlag = '';
  }
  closeSearchAp() {
    this.validateFlag2 = '';
  }
  onKey(event: any) { // without type info

    //console.log('hii',event.target.value)
  //   this.typingName = event.target.value;
  //  console.log('Find Username: ' , this.typingName);
  
   
  //   const strs = ['sagar', 'pratik', 'sunny', 'sonal'];
  //   this.result = strs.filter(s => s.includes(this.typingName));

  //     console.log('User Array : ' , strs);
  //     console.log('User find: ' , this.result);
     
     
  }

  onChangeSearch(val: string) {
    val=val.toLowerCase()
    let data = {
      "database" : this.configService.config.db,
      "StartDate" : moment(this.controlnWorkflowFormData.exStart).format('DD-MMM-YYYY'),
      "EndDate" : moment(this.controlnWorkflowFormData.exEnd).format('DD-MMM-YYYY')
    }
    //console.log(data)
    this.httpservice.secureJavaPostApi(ConstUrls.getUserList, data).subscribe((res:any)=>{

      if(res){
        // val= res;
      //  this.userList = res.data.filter(s => s.UserName.includes(val));;
      this.userList = res.data.filter(s => {
        let str= s.UserName.toLowerCase().trim();
       return str.includes(val)
      });;
        //console.log('User List: ', this.userList);
      }
    })
  }

  selectEvent(item) {
   
    if(this.controlnWorkflowFormData.exAssignee){
      if(this.controlnWorkflowFormData.exAssignee.includes(item.UserName) || item.Available=='false') 
 {
           const initialState = {
            title: 'Alert',
             content: 'Sorry! '+item.UserName+ ' is not available!',
             link: 'Ok',
           };
           
          this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
          }
          else{   
        this.controlnWorkflowFormData.exAssignee.push(item.UserName);
        this.userIdsEx.push(item);
          }
    } else{
      if(item.Available=='false'){
        const initialState = {
          title: 'Alert',
           content: 'Sorry! '+item.UserName+ ' is not available!',
           link: 'Ok',
         };
         
        this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
       
      }
      else{
        this.controlnWorkflowFormData.exAssignee=[];
        this.controlnWorkflowFormData.exAssignee.push(item.UserName);
      }
     
    }
  }
  selectEventAp(item){
    if(this.controlnWorkflowFormData.apAssignee){
      if(this.controlnWorkflowFormData.apAssignee.includes(item.UserName) || item.Available=='false') 
 {
           const initialState = {
            title: 'Alert',
            content: 'Sorry!'+item.UserName+ ' is not available!',
            link: 'Ok',
           };
           
          this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
          }
          else{
            this.userIdsAp.push(item);
            this.controlnWorkflowFormData.apAssignee.push(item.UserName)
          }
    }
    else{
      if(item.Available=='false'){
        const initialState = {
          title: 'Alert',
          content: 'Sorry!'+item.UserName+ ' is not available!',
          link: 'Ok',
         };
         
        this.modalRef = this.modalService.show(DefaultModalComponent, {initialState, class: 'success-class', backdrop: 'static', keyboard: false});
      }
      else{
        this.controlnWorkflowFormData.apAssignee=[];
        this.controlnWorkflowFormData.apAssignee.push(item.UserName)  
      }
  
    }
    
  }
  // onChangeSearch(search: string) {
  //   // fetch remote data from here
  //   // And reassign the 'data' which is binded to 'data' property.
  // }

  onFocused(e) {
    // do something
  }
  
 getControlData(){
  let table = $('#controlWFTable').DataTable();
  table.destroy();
 this.httpservice.secureGet(ConstUrls.getControls).subscribe(response => {
      //console.log('get all controls::',response)
      this.controlTable = response;
      this.controlTableList = response;
      if(this.controlnWorkflowFormData.controlIds) { 
      this.controlList= this.controlTable.filter(x=>{return this.controlnWorkflowFormData.controlIds.includes(x.controlId)})
      this.controlTableList= this.controlTable.filter(x=>
        {return (!(this.controlnWorkflowFormData.controlIds.includes(x.controlId)))&& (x.controlWFId==undefined || x.controlWFId=='')})
      }

      else{
        var id="C-"+this.controlId.split('-')[1]
        this.controlnWorkflowFormData.controlIds=[];
        this.controlnWorkflowFormData.controlIds.push(id)
        this.controlList= this.controlTable.filter(x=>{return this.controlnWorkflowFormData.controlIds.includes(x.controlId)})
        this.controlTableList= this.controlTable.filter(x=>

          {return (!(this.controlnWorkflowFormData.controlIds.includes(x.controlId)))&& (x.controlWFId==undefined || x.controlWFId=='')})
      }
      this.datatable();
 })
}
addControl(){
  let table= $('#controlWFTable').DataTable();
  let arr=[];
  let checkValue= table.$('input:checked').each(function(){
    arr.push($(this).attr('value'))
  })
  if(arr.length>0){
    arr.forEach(x=>{
      this.controlnWorkflowFormData.controlIds.push(x);

    })
    this.controlList= this.controlTable.filter(x=>{return this.controlnWorkflowFormData.controlIds.includes(x.controlId)})
  this.controlTableList= this.controlTableList.filter(x=>
    {return (!(this.controlnWorkflowFormData.controlIds.includes(x.controlId)))&& (x.controlIds==undefined || x.controlIds=='')})

  }

}

filterControls() {
  if (this.allControls) {
    const selectedIds = [];
    this.controlList.forEach((element) => {
      selectedIds.push(element.controlId);
    });
    this.filteredControls = this.allControls.filter(
      (controls) => selectedIds.indexOf(controls.controlId) === -1
    );
    // console.log("Filtered Controls:", this.filterControls);
  }
}

deleteControl(controlId) {
  const initialState = {
    title: "Confirmation",
    content: `Do you want to remove this control?`,
    link: "Confirm",
    action: "Cancel",
    confirmFlag: false,
  };
  this.modalRef = this.modalService.show(ConfirmPopupComponent, {
    initialState,
    class: "success-class",
    backdrop: "static",
    keyboard: false,
  });
  this.modalRef.content.onClose.subscribe((r) => {
    if (r) {
      this.deletedControls.push(controlId);
      //console.log("remove control:", controlId);
      this.controlList = this.controlList.filter(
        (controls) => controls.controlId !== controlId
      );
      this.filterControls();
      const initialState = {
        title: "Success",
        content: "Control Removed!",
        link: "Ok",
      };
      this.modalRef = this.modalService.show(DefaultModalComponent, {
        initialState,
        class: "success-class",
        backdrop: "static",
        keyboard: false,
      });
    }
  });
}


// delete(id) {
//   const initialState = {
//     title: "Confirmation",
//     content: `Do you want to delete this Record?`,
//     link: "Confirm",
//     action: "Cancel",
//     confirmFlag: false,
//   };
//   this.modalRef = this.modalService.show(ConfirmPopupComponent, {
//     initialState,
//     class: "success-class",
//     backdrop: "static",
//     keyboard: false,
//   });
//   this.modalRef.content.onClose.subscribe((r) => {
//     if (r) {
//       for (let i = 0; i < this.controlList.length; i++) {
//         if (
//           this.controlList[i].controlId == id &&
//           ((this.controlList[i].ctlTestingId &&
//             this.controlList[i].ctlTestingId.length) ||
//             (this.controlList[i].riskId && this.controlList[i].riskId.length))
//         ) {
//           const initialState = {
//             title: "Warning!",
//             content: "Control under use W/F! Cannot delete.",
//             link: "Ok",
//           };
//           this.modalRef = this.modalService.show(DefaultModalComponent, {
//             initialState,
//             class: "success-class",
//             backdrop: "static",
//             keyboard: false,
//           });
//           this.closeModal();
//           return false;
//         }
//       }
//       this.httpservice
//         .securePost(ConstUrls.deleteControl, { controlId: id })
//         .subscribe((response: any) => {
//           if (response) {
//             this.controlList = this.controlList.filter((rec) => {
//               if (
//                 rec.ctlTestingId &&
//                 !rec.ctlTestingId.length &&
//                 !rec.riskId.length
//               ) {
//                 return rec.controlId !== id;
//               }
//               return true;
//             });
//             this.RCMservice.sendCount(true);

//             const initialState = {
//               title: "Success!",
//               content: "Record Deleted Successfully!",
//               link: "Ok",
//             };
//             this.modalRef = this.modalService.show(DefaultModalComponent, {
//               initialState,
//               class: "success-class",
//               backdrop: "static",
//               keyboard: false,
//             });
//             this.closeModal();
//             return false;
//           }
//         });
//     }
//   });
// }



datatable() {
  setTimeout(()=>{
   $('#controlWFTable').DataTable({
     "oLanguage": { "sSearch": "" }, 
     "language": {
           "searchPlaceholder": "Search"
     },
     "destroy": true,
       "fixedHeader": true,
         "responsive": true,
         "bPaginate": true,
         "lengthMenu": [[5, 10, 20, 50, 100, -1],
         [5, 10, 20, 50, 100, "All"]],
           "bInfo": true,
           "searching": true,
           "lengthChange": true,
           "columnDefs": [{
             'targets': [0],
             'orderable': false,
             'className': 'dt-body-center'
           }],
       });
  },500)}
   

 closeModal() {
   $(".scroll-container").scrollTop(0);
   $('.pop-up-form').addClass('d-none');
   this.wfControlForm.tabs[0].active = true;
   this.position = this.position ? undefined : {x: 0, y: 0}
 }
  // datatable() {

  //   const controlFormWFTable: any = $("#controlWFTable");
  //   const controlFormWFList = this.dataTableService.initializeTable(
  //     controlFormWFTable,
  //     `controlFormWF`
  //   );
   
  //   this.dataTableFuncService.selectAllRows(
  //     controlFormWFList,
  //     `wf-control-select-all`
  //   );
    
  //   $("#controlWFTable tbody").on(
  //     "change",
  //     'input[type="checkbox"]',
  //     function () {
  //       if (!this.checked) {
  //         $("#wf-control-select-all").prop("checked", false);
  //         var el = $("#wf-control-select-all").get(0);
  //         if (el && el.checked && "indeterminate" in el) {
  //           el.indeterminate = true;
  //         }
  //       }
  //       if (
  //         $("#controlWFTable tr td input[type='checkbox']:checked").length ==
  //         $("#controlWFTable tr td input[type='checkbox']").length
  //       ) {
  //         $("#wf-control-select-all").prop("checked", true);
  //       }
  //     }
  //   );
  
  // }
  // closeModal() {
  //   $(".scroll-container").scrollTop(0);
  //   $('.pop-up-form').addClass('d-none');
  // }
  onValueChange(value: Date): void {
    this.controlnWorkflowFormData.exStart = value;
    this.controlnWorkflowFormData.exDuration=(new Date(this.controlnWorkflowFormData.exEnd).getTime()-(new Date(this.controlnWorkflowFormData.exStart).getTime()))/(1000*3600*24)
    this.controlnWorkflowFormData.exDuration= parseInt(this.controlnWorkflowFormData.exDuration) 
    this.getDeadlines();
    this.calendarIcon = false;
    this.calendarDetail = true;
  }
  onValueChangeEnd(value: Date): void {
    this.controlnWorkflowFormData.exEnd = value;
    this.controlnWorkflowFormData.exDuration=(new Date(this.controlnWorkflowFormData.exEnd).getTime()-(new Date(this.controlnWorkflowFormData.exStart).getTime()))/(1000*3600*24)
    this.controlnWorkflowFormData.exDuration= parseInt(this.controlnWorkflowFormData.exDuration);
    this.getDeadlines();
    this.calendarIconEnd = false;
    this.calendarDetailEnd = true;
  }
  onValueChangeSec(value: Date): void {
    this.controlnWorkflowFormData.apStart = value;
    this.controlnWorkflowFormData.apDuration=(new Date(this.controlnWorkflowFormData.apEnd).getTime()-(new Date(this.controlnWorkflowFormData.apStart).getTime()))/(1000*3600*24)
    this.controlnWorkflowFormData.apDuration= parseInt(this.controlnWorkflowFormData.apDuration);  
    this.getDeadlines();  
    this.calendarIconApproval = false;
    this.approvalSec = true;
  }

  onValueChangeSecEnd(value: Date): void {
    this.controlnWorkflowFormData.apEnd = value;
    this.controlnWorkflowFormData.apDuration=(new Date(this.controlnWorkflowFormData.apEnd).getTime()-(new Date(this.controlnWorkflowFormData.apStart).getTime()))/(1000*3600*24)
    this.controlnWorkflowFormData.apDuration= parseInt(this.controlnWorkflowFormData.apDuration);  
    this.getDeadlines(); 
    this.calendarIconApprovalEnd = false;
    this.approvalSecEnd = true;
  }
  reminder(){
    this.Editreminder = !this.Editreminder;
  }
  cancelClick(){
    this.Editreminder = false;
  }
}
