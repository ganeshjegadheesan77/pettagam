import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TabsModule } from 'ngx-bootstrap';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { RiskAndControlManagementRoutingModule } from "./risk-and-control-management-routing.module";
import { RiskAndControlManagementComponent } from "./components/risk-and-control-management.component";
import { DashboardContainerComponent } from "./components/dashboard-container/dashboard-container.component";
import { RiskListComponent } from "./components/risk-list/risk-list.component";
import { ControlListComponent } from "./components/control-list/control-list.component";
import { RiskControlMatrixComponent } from "./components/risk-control-matrix/risk-control-matrix.component";
import { RiskControlComponent } from "./components/risk-control/risk-control.component";
import { UnassignedControlsComponent } from "./components/unassigned-controls/unassigned-controls.component";
import { ChatComponent } from "./components/chat/chat.component";
import { CasesComponent } from "./components/cases/cases.component";
import { ControlDefinitionFormComponent } from "./components/control-definition-form/control-definition-form.component";
import { RiskCountComponent } from "./components/risk-count/risk-count.component";
import { ControlCountComponent } from "./components/control-count/control-count.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { RcmViewComponent } from "./components/rcm-view/rcm-view.component";
import { InTrayComponent } from "./components/in-tray/in-tray.component";
import { FilePreviewComponent } from "./components/file-preview/file-preview.component";
import { PrinterComponent } from "./components/printer/printer.component";
import { WhiteboardComponent } from "./components/whiteboard/whiteboard.component";
import { AddNewRiskComponent } from "./components/add-new-risk/add-new-risk.component";
import { ViewRiskLibraryComponent } from "./components/view-risk-library/view-risk-library.component";


import { RiskDefinitionFormComponent } from "./components/risk-definition-form/risk-definition-form.component";
import { AddNewControlComponent } from './components/add-new-control/add-new-control.component';
import { PrintPopupComponent } from './components/print-popup/print-popup.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ControlWorkFlowComponent } from './components/control-work-flow/control-work-flow.component';
import { GenerateRepotFormComponent } from './components/generate-repot-form/generate-repot-form.component';
import { WfControlFormComponent } from './components/wf-control-form/wf-control-form.component';
import { AngularDraggableModule } from "angular2-draggable";

import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
@NgModule({
  declarations: [
    RiskAndControlManagementComponent,
    DashboardContainerComponent,
    RiskListComponent,
    ControlListComponent,
    RiskControlMatrixComponent,
    RiskControlComponent,
    UnassignedControlsComponent,
    ChatComponent,
    CasesComponent,
    ControlDefinitionFormComponent,
    RiskCountComponent,
    ControlCountComponent,
    AddNewRiskComponent,
    RcmViewComponent,
    InTrayComponent,
    FilePreviewComponent,
    PrinterComponent,
    WhiteboardComponent,
    ViewRiskLibraryComponent,
    WhiteboardComponent,
    RiskDefinitionFormComponent,
    AddNewControlComponent,
    PrintPopupComponent,
    ControlWorkFlowComponent,
    GenerateRepotFormComponent,
    WfControlFormComponent,
    
  ],
  imports: [
    CommonModule,
    AutocompleteLibModule,
   
    RiskAndControlManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    PdfViewerModule,
    AngularDraggableModule,
    HttpClientModule, 
    AngularEditorModule

  ],
  exports:[CommonModule,
  
    FormsModule,ControlListComponent,AddNewControlComponent],
  entryComponents: [
    ControlDefinitionFormComponent,
    FilePreviewComponent,
    InTrayComponent,
    WhiteboardComponent,
    AddNewRiskComponent,
    ViewRiskLibraryComponent
  ],
  providers: [
  ]
})
export class RiskAndControlManagementModule {}
