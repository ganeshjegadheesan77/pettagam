import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesMapModule } from 'countries-map';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';

// import { DashboardComponent } from './components/dashboard/dashboard.component';


@NgModule({
  declarations: [
    DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CountriesMapModule
    
  ],
  entryComponents: [
   
  ]

})
export class DashboardModule { }
