import { Component, OnInit, OnChanges } from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { CountriesData,ChartSelectEvent } from 'countries-map';
import { Chart, ChartOptions } from 'chart.js'
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import { ConstUrls } from 'src/app/config/const-urls';
declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit, OnChanges {
constructor (private httpService: HttpService){}
  public mapData:CountriesData;
  actionList: any;
  crticalAction: any;
  criticalCount: any;
  actionCount: any;
  location: any;
  scheduledCount: any = 0;
  overdueCount: any = 0;
  auditList: any;
  actionData: any;
  auditData: any;
  auditCount: any;
  pendingCount: any = 0;
  financialCount: any = 0;
  superviosryCount: any = 0;
  rmCount: any = 0;
  distributionCount: any = 0;
  operationsCount: any = 0;
  complianceCount: any = 0;
  itCount: any = 0;
  govCount: any = 0;
  public temp: number = 0;
  public locationId: any = [];
  assessmentDetails: any;
  //
  finalSet: any = [];
  dataSet: any = {};
  bubbleChart: object;
  bubbleChartCanvas: any;
  bubbleChartCtx: any;
  flagLoaded:boolean=false;
  ngOnChanges() {
   this.getData();
  }

  ready(){
    this.flagLoaded=true; //chart is ready
  }
   select(event:ChartSelectEvent) {
    this.mapData={}
    if (event.country !== null) {
      this.locationId = this.locationId.filter(x => { return x !== undefined })
      this.locationId.forEach(element => {
        if (event.country == element)
          this.mapData[element] = { value: '0', extra: {} };
        else
          this.mapData[element] = { value: '5', extra: {} };
      });
      let payload = { location: event.country };
      this.auditList=[];this.actionList=[];
      let table = $('#dashboardauditListTable').DataTable();
     table.destroy();
     
      this.httpService.securePost(ConstUrls.getLocationAudit, payload).subscribe(async response => {
        this.auditList = await response;
        this.financialCount = 0, this.superviosryCount = 0, this.operationsCount = 0, this.itCount = 0, this.distributionCount = 0, this.govCount = 0;
        this.pendingCount = 0; this.auditCount = 0; this.scheduledCount = 0
        this.auditCount = this.auditList.length;
        this.auditList.forEach(x => {
          if (x.department == "Finance") this.financialCount++;
          if (x.department == "HR") this.superviosryCount++;
          if (x.department == "Logistics") this.operationsCount++;
          if (x.department == "IT") this.itCount++;
          if (x.department == "Site") this.distributionCount++;
          if (x.department == 'Governance') this.govCount++;
          this.locationId.push(x.location)
          if (new Date(x.startDate).getTime() >= new Date().getTime()) {
            this.pendingCount++;
          }
        });
        this.scheduledCount = this.auditCount - this.pendingCount;
        this.auditList = this.auditList.filter(x => {
          return (new Date(x.startDate).getTime() <= new Date().getTime())
        })
        this.dashboardauditList();  
      });
      this.httpService.securePost(ConstUrls.getLocationAction, payload).subscribe(async response => {
        
    
        this.actionList = await response;
        $('#dashboardactionListTable').DataTable().destroy();
        this.actionList = this.actionList.filter(x => {
          return (new Date(x.signOffPeriod).getTime() <= new Date().getTime())
        })
        this.dashboardactionList();  
      });
    }
  }
  reloadData(){
    if(this.flagLoaded){
      this.getData();
    }
  }
  getData() {
      this.financialCount = 0, this.superviosryCount = 0, this.operationsCount = 0, this.itCount = 0, this.distributionCount = 0, this.govCount = 0;
      this.pendingCount = 0; this.auditCount = 0; this.scheduledCount = 0;this.overdueCount=0
      this.httpService.secureGet(ConstUrls.getAllActionRecords).subscribe(response => {
        this.actionList = response;
        $('#dashboardactionListTable').DataTable().destroy();
        this.dashboardactionList();
        this.actionCount = this.actionList.length;
        this.criticalCount = 0
        this.actionList.forEach(x => {
          if (x.rating == "Critical") this.criticalCount++
          if (new Date(x.signOffPeriod).getTime() <= new Date().getTime()) {
            this.overdueCount++
          }
          
          this.actionList = this.actionList.filter(x => {
            return (new Date(x.signOffPeriod).getTime() <= new Date().getTime())
          });
          this.locationId.push(x.location)
        });

        

      })
      this.httpService.secureGet(ConstUrls.getAllAuditRecords).subscribe(response => {
        this.auditList = response;
        this.auditCount = this.auditList.length;
        let table4 = $('#dashboardauditListTable').DataTable();
        table4.destroy();
        this.auditList.forEach(x => {
          if (x.department == "Finance") this.financialCount++;
          if (x.department == "HR") this.superviosryCount++;
          if (x.department == "Logistics") this.operationsCount++;
          if (x.department == "IT") this.itCount++;
          if (x.department == "Site") this.distributionCount++;
          if (x.department == 'Governance') this.govCount++;
          this.locationId.push(x.location)
          if (new Date(x.startDate).getTime() >= new Date().getTime()) {
            this.pendingCount++;
          }
        });
        this.scheduledCount = this.auditCount - this.pendingCount;
        this.auditList = this.auditList.filter(x => {
          return (new Date(x.startDate).getTime() <= new Date().getTime())
        })
        this.dashboardauditList();
        this.mapData={}
        this.locationId = ([...new Set(this.locationId)]) //to get the location ids
        this.locationId = this.locationId.filter(x => { return x !== undefined })
        this.locationId.forEach(x=>{
          this.mapData[x]={ value: '5', extra: {} };
        })
      })
 
  }
  makeData(location:string){
    return {
      location:{ value: '5', extra: {} }
    }
  }
  checkDate = function (x) {
    if (new Date(x).getTime() <= new Date().getTime())
      return true;
  }
  ngOnInit() {

   
    let namedChartAnnotation = ChartAnnotation;
    namedChartAnnotation["id"] = "annotation";
    Chart.pluginService.register(namedChartAnnotation);

    // binding of bubble chart to variable
    this.bubbleChartCanvas = <HTMLCanvasElement>document.getElementById("bubble");
    this.bubbleChartCtx = this.bubbleChartCanvas.getContext("2d");
    //getAllAssessData
    this.httpService.securePost(ConstUrls.getAllAssessData).subscribe(response => {
      this.assessmentDetails = response;
      this.assessmentDetails.forEach(x => {
        if (x.intrinsicRisk && x.residualRisk)
          x.intrinsicRisk.forEach((element, index) => {

            var data = x;
            var result = {};
            this.dataSet = getDataSet(data, index, element.riskType, result)


            this.finalSet.push(this.dataSet)
          })
      })

      function getDataSet(data, i, riskType, dataSet) {
        switch (riskType) {
          case 'Financial':
            if (data.intrinsicRisk.length > 0) {
              var a = data.intrinsicRisk[i].likelyhood;
              var b = data.intrinsicRisk[i].impact;
              var c = data.residualRisk[i].likelyhood
              var d = data.residualRisk[i].impact;
              var x = (a + c) / 2
              var y = (b + d) / 2
            }
            break;
          case 'Legal':
            if (data.intrinsicRisk.length > 0) {
              var a = data.intrinsicRisk[i].likelyhood;
              var b = data.intrinsicRisk[i].impact;
              var c = data.residualRisk[i].likelyhood
              var d = data.residualRisk[i].impact;
              var x = (a + c) / 2
              var y = (b + d) / 2
            }

            break;
          case 'Reputational':
            if (data.intrinsicRisk.length > 0) {
              var a = data.intrinsicRisk[i].likelyhood;
              var b = data.intrinsicRisk[i].impact;
              var c = data.residualRisk[i].likelyhood
              var d = data.residualRisk[i].impact;
              var x = (a + c) / 2
              var y = (b + d) / 2
            }
            break;
          default: x = 0, y = 0;
            break;
        }

        Object.assign(dataSet, { x: Number(x), y: Number(y), r: 5 })
        return dataSet;
      }
    })

//98aa3a ffca07 dd6838
    setTimeout(() => {
      this.bubbleChart = new Chart(this.bubbleChartCtx, {

        type: 'bubble',
        data: {
          datasets: [{
            label: 'Risk Assessment',
            data: this.finalSet, // Specify the data values array
            borderColor: '#2196f3', // Add custom color border 
            backgroundColor: '#3f51b5', // Add custom color background (Points and Fill)
          }]
        },
        options: {
          tooltips: {
            callbacks: {
              label: function (t, d) {
                return d.datasets[t.datasetIndex].label +
                  ': (Likelihood:' + t.xLabel + ', Impact:' + t.yLabel + ')';
              }
            }
          },
          scales: {
            xAxes: [{
              display: true,
              ticks: {
                beginAtZero: true,
                max: 1
              },
              scaleLabel: {
                display: true,
                labelString: 'Likelihood'
              }
            }],
            yAxes: [{
              display: true,
              ticks: {
                beginAtZero: true,
                max: 1
              },
              scaleLabel: {
                display: true,
                labelString: 'Impact'
              }
            }]
          },
          legend: {
            // position: 'bottom',
            label: {
              display: false
            },
            display: false
          },
          responsive: true, // Instruct chart js to respond nicely.
          maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
          annotation: {
            drawTime: "beforeDatasetsDraw",
            events: ['dblclick'],
            annotations: [{
              id: 'low-box',
              type: 'box',
              xScaleID: 'x-axis-0',
              yScaleID: 'y-axis-0',
              xMin: 0,
              xMax: 0.4,
              yMin: 0,
              yMax: 0.4,
              backgroundColor: '#98aa3a',
              borderColor: '#98aa3a',
              borderWidth: 0
            }, {
              id: 'mi-box',
              type: 'box',
              xScaleID: 'x-axis-0',
              yScaleID: 'y-axis-0',
              xMin: 0.4,
              xMax: 0.7,
              yMin: 0,
              yMax: 0.7,
              backgroundColor: '#ffca07',
              borderColor: '#ffca07',
              borderWidth: 0
            }, {
              id: 'mi-box-1',
              type: 'box',
              xScaleID: 'x-axis-0',
              yScaleID: 'y-axis-0',
              xMin: 0,
              xMax: 0.7,
              yMin: 0.4,
              yMax: 0.7,
              backgroundColor: '#ffca07',
              borderColor: '#ffca07',
              borderWidth: 0
            }, {
              id: 'hi-box',
              type: 'box',
              xScaleID: 'x-axis-0',
              yScaleID: 'y-axis-0',
              xMin: 0.7,
              xMax: 1,
              yMin: 0,
              yMax: 0.7,
              backgroundColor: '#dd6838',
              borderColor: '#dd6838',
              borderWidth: 0
            }, {
              id: 'hi-box-1',
              type: 'box',
              xScaleID: 'x-axis-0',
              yScaleID: 'y-axis-0',
              xMin: 0,
              xMax: 1,
              yMin: 0.7,
              yMax: 1,
              backgroundColor: '#dd6838',
              borderColor: '#dd6838',
              borderWidth: 0
            }]
          }
        } as ChartOptions,
        plugins: [ChartAnnotation]
      })
    }, 1000);
    this.getData();
  }

  dashboardauditList() {
    setTimeout (() => {
    $("#dashboardauditListTable").DataTable({
      processing: true,
      responsive: true,
      fixedHeader: true,
      autoWidth: false,
      scrollY: '165px',
      scrollX: false,
      scrollCollapse: true,
      bPaginate: true,
      columnDefs: [
        { targets: 0, visible: false, width: '1px' },
      ],
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]
      ],
      bInfo: true,
      searching: false,
    });
  }, 0);
  }


  dashboardactionList() {
    setTimeout (() => {
  $("#dashboardactionListTable").DataTable({
    processing: true,
    responsive: true,
    fixedHeader: true,
    autoWidth: false,
    scrollY: '165px',
    scrollX: false,
    scrollCollapse: true,
    bPaginate: true,
    columnDefs: [
      { targets: 0, visible: false, width: '1px' },
    ],
    lengthMenu: [
      [5, 10, 20, 50, 100, -1],
      [5, 10, 20, 50, 100, "All"]
    ],
    bInfo: true,
    searching: false,
  });
}, 0);
}

}

