import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
declare var $;
@Component({
  selector: 'app-top-ten-risks',
  templateUrl: './top-ten-risks.component.html',
  styleUrls: ['./top-ten-risks.component.less']
})
export class TopTenRisksComponent implements OnInit {
  hide = true;
  dataTable: any;
  constructor(private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.datatable();
  }


  
  datatable() {
       
    function cbDropdown(column) {
      return $('<div>', {
        'class': 'dropdown-menu'
      }).appendTo($('<div class="dropdown filter-dropdown">' + '<button type="button" class="btn btn-primary" data-toggle="dropdown"><span class="filter-icon"></span></button>', {
      }).appendTo(column));
    }
    this.chRef.detectChanges();
    const toptenRiskTable: any = $('#topTenRiskTable');
    var toptenRiskAssement = toptenRiskTable.DataTable({
      processing: true,
      oLanguage: {
        sSearch: ""
      },
      language: {
        searchPlaceholder: "Search"
      },
      dom: "Rlfrtip",
      colReorder: {
        fixedColumnsRight: 1
      },
      rowReorder: {
        selector: ".reorder-row"
      },

      fixedHeader: true,
      autoWidth: false,
      scrollY: "100vh",
      scrollX: true,
      scrollCollapse: true,
      bPaginate: true,
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"]
      ],
      bInfo: true,
      searching: true,
      columnDefs: [
        { targets: 0, visible: false },
        {
          targets: 1,
          orderable: false,
          className: "dt-body-center",
          render: function() {
            return '<input type="checkbox" name="" value="">';
          }
        },
        {
          targets: 10,
          orderable: false
        }
      ],
      select: {
        style: "multi"
      },
      initComplete: function() {
        this.api()
          .columns([2, 4, 5])
          .every(function() {
            var TotChkbxs = 0;
            var TikdChkbxs = 0;
            var column = this;
            var ddmenu = cbDropdown($(column.header())).on(
              "change",
              ":checkbox",
              function() {
                var active;
                var vals = $(":checked", ddmenu)
                  .map(function(index, element) {
                    active = true;
                    return $.fn.dataTable.util.escapeRegex($(element).val());
                  })
                  .toArray()
                  .join("|");

                column
                  .search(
                    vals.length > 0 ? "^(" + vals + ")$" : "",
                    true,
                    false
                  )
                  .draw();
              }
            );

            column
              .data()
              .unique()
              .sort()
              .each(function(d, j) {
                var $label = $("<label>"),
                  $text = $("<span>", {
                    class: "filter-text",
                    text: d
                  }),
                  $cb = $("<input>", {
                    class: "checkbox",
                    type: "checkbox",
                    value: d
                  });
                $cb.appendTo($label);
                $text.appendTo($label);

                ddmenu.append(
                  $("<a>", {
                    class: "dropdown-item"
                  }).append($label)
                );
              });
          });
      }
    });
    $('body').on("click", function () {
      $('.dataTables_scrollHead').removeClass('overflow-table-active');
      $('.filter-dropdown').removeClass('show-dropdown-menu');
      if (this.hide) $('.filter-dropdown .dropdown-menu').removeClass('show');
      this.hide = true;
  });

    $('.filter-dropdown').on("click", function (e) {
      $('.dataTables_scrollHead').addClass('overflow-table-active');
      $('.filter-dropdown').removeClass('show-dropdown-menu');
      $(".filter-dropdown .dropdown-menu").removeClass('show');
      $(this).find(".dropdown-menu").toggleClass('show');
      $(this).toggleClass('show-dropdown-menu');
      e.stopPropagation();
  });
   $('#top-ten-risks-select-all').on('click', function(){
    var rows = toptenRiskAssement.rows({ 'search': 'applied' }).nodes();
    $('input[type="checkbox"]', rows).prop('checked', this.checked);
 });
 $('.expand-container').on('click', function (e) {
  toptenRiskAssement.columns.adjust().draw();
});
 $('#topTenRiskTable tbody').on('change', 'input[type="checkbox"]', function(){
    if(!this.checked){
      $('#top-ten-risks-select-all').prop('checked', false);
       var el = $('#top-ten-risks-select-all').get(0);
       if(el && el.checked && ('indeterminate' in el)){
          el.indeterminate = true;
       }
    }
    if($("#topTenRiskTable tr td input[type='checkbox']:checked").length == $("#topTenRiskTable tr td input[type='checkbox']").length) {
      $('#top-ten-risks-select-all').prop('checked',true);
    }    
 });
  }
  


}
