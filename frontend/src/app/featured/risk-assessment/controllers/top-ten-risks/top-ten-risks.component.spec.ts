import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTenRisksComponent } from './top-ten-risks.component';

describe('TopTenRisksComponent', () => {
  let component: TopTenRisksComponent;
  let fixture: ComponentFixture<TopTenRisksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopTenRisksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTenRisksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
