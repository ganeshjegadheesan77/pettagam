import { DataService } from "./../../../internal-audit/components/audit-universe/data.service";
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpService } from "../../../../core/http-services/http.service";
import { ConstUrls } from "src/app/config/const-urls";
import { DataTableService } from "../../../services/dataTable.service";
import { DataTableFuncService } from "../../../services/dataTableFunc.service";
declare var $;
@Component({
  selector: "app-risk-assessment-risk-list",
  templateUrl: "./risk-assessment-risk-list.component.html",
  styleUrls: ["./risk-assessment-risk-list.component.less"],
})
export class RiskAssessmentRiskListComponent implements OnInit {
  hide = true;
  public loadComponent = false;
  riskId: any;
  oneRiskAssesment: any;
  dataTable: any;
  dashboardCounts;
  newResidualRiskComponent: boolean;
  openRiskEditForm: boolean;
  selectedRiskId: any;
  isLoading = false;

  constructor(
    private httpService: HttpService,
    private chRef: ChangeDetectorRef,
    private dataService: DataService,
    private dataTableService: DataTableService,
    private dataTableFuncService: DataTableFuncService
  ) {}
  RiskListData: any;
  componentData = {};

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
    this.getAllRiskAssessments();
  }

  getAllRiskAssessments() {
    this.isLoading = true;
    this.httpService.secureGet(ConstUrls.getAssessmentList).subscribe(response => {
      console.log("getAllRiskAssessment::", response)
      this.RiskListData = response;

        this.RiskListData.forEach((recrd) => {
          //console.log('recrd', recrd)
          recrd["iresult"] =
            recrd && recrd.mydata[0]
              ? this.getResultForClr(recrd.mydata[0].intrinsicRisk)
              : 0;
          recrd["rresult"] =
            recrd && recrd.mydata[0]
              ? this.getResultForClr(recrd.mydata[0].residualRisk)
              : 0;
          return recrd;
        });

      const dashboardCounts = {
        openAssessments: this.RiskListData.filter(ele => ele.riskAssessmentId.length === 0).length,
        highRisk: this.RiskListData.filter(ele => ele.iresult === 3).length,
        risks: this.RiskListData.length
      }
      this.dataService.changeMessage(dashboardCounts);
      
      console.log('RIsk Assessment List: ', this.RiskListData);
      let assessmentTable = $('#riskListAssementTable').DataTable();
      assessmentTable.destroy();
      this.isLoading = false;
      this.datatable();
    })
  }

  getResultForClr(arr) {
    let r = 0;
    if (arr && arr.length) {
      arr.forEach((ele) => {
        if (r < ele.result) r = ele.result;
      });
      //console.log('r', r)
      // return r || 0;
    }
    return r || 0;
  }

  modalClosed(event) {
    //console.log('event', event)
    this.newResidualRiskComponent = false;
    this.loadComponent = false;
    // if(event) {
    this.getAllRiskAssessments();
    // }
  }

  loadAddNewComponent(riskAssementId, boolValue) {
    //console.log('id Rec: ', riskAssementId);
    this.oneRiskAssesment = riskAssementId;
    this.loadComponent = false;
    //console.log("hey", riskAssementId)

    this.newResidualRiskComponent = true;
    $(".pop-up-form").removeClass("d-none");
  }

  loadMyChildComponent(riskAssementId, isNewAssessment) {
    //console.log('id Rec: ', riskAssementId);
    this.oneRiskAssesment = riskAssementId;
    this.RiskListData["isNewAssessment"] = isNewAssessment;
    this.newResidualRiskComponent = false;
    this.loadComponent = true;
    this.openRiskEditForm = false;

    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  loadRiskEditComponent(riskId) {
    this.openRiskEditForm = true;
    this.newResidualRiskComponent = false;
    this.loadComponent = false;
    this.selectedRiskId = { id: riskId, renderForm: "Risk" };
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").removeClass("d-none");
    $(".tabOne").trigger("click");
  }

  getRiskEditFormStatus(event) {
    if (event === true) {
      this.openRiskEditForm = false;
      this.getAllRiskAssessments();
    }
  }

  datatable() {
    this.chRef.detectChanges();
    const riskAssementListTable: any = $("#riskListAssementTable");
    const riskAssementListRA = this.dataTableService.initializeTable(
      riskAssementListTable,
      `risklistAssement`
    );
    this.dataTableFuncService.filterDropDown();
    this.dataTableFuncService.selectAllRows(
      riskAssementListRA,
      `assement-list-select-all`
    );
    this.dataTableFuncService.columnDropDown(riskAssementListRA, `risk-hideshow`);
    this.dataTableFuncService.expandContainer(riskAssementListRA);
    $("#riskListAssementTable tbody").on(
      "change",
      'input[type="checkbox"]',
      function () {
        if (!this.checked) {
          $("#assement-list-select-all").prop("checked", false);
          var el = $("#assement-list-select-all").get(0);
          if (el && el.checked && "indeterminate" in el) {
            el.indeterminate = true;
          }
        }
        if (
          $("#riskListAssementTable tr td input[type='checkbox']:checked").length ==
          $("#riskListAssementTable tr td input[type='checkbox']").length
        ) {
          $("#assement-list-select-all").prop("checked", true);
        }
      }
    );

    $("body").on("click", function () {
      $(".dataTables_scrollHead").removeClass("overflow-table-active");
      $(".filter-dropdown").removeClass("show-dropdown-menu");
      if (this.hide) $(".filter-dropdown .dropdown-menu").removeClass("show");
      this.hide = true;
    });

  }
}
