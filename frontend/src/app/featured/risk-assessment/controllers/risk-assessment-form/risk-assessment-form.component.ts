import * as core from "@angular/core";
import { HttpService } from "../../../../core/http-services/http.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { FormGroup,  FormBuilder, FormArray } from "@angular/forms";
import { DefaultModalComponent } from "src/app/layout/modal/default-modal/default-modal.component";
import { ConstUrls } from "src/app/config/const-urls";
import { ToastrServiceService } from "src/app/core/toastr-service/toastr-service.service";
import { TabsetComponent } from "ngx-bootstrap/tabs";
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
declare var $;
@core.Component({
  selector: "app-risk-assessment-form",
  templateUrl: "./risk-assessment-form.component.html",
  styleUrls: ["./risk-assessment-form.component.less"],
})
export class RiskAssessmentFormComponent implements core.OnInit, core.OnChanges {
  @core.Input("componentData") componentData;
  @core.Input("riskAssementId") riskAssementId: any;

  @core.Output("eventClosed") eventClosed = new core.EventEmitter();
  @core.ViewChild("RiskAssessmentForm", { static: false })
  RiskAssessmentForm: TabsetComponent;
  searchRiskId: any;
  riskId: any;
  modalRef: BsModalRef;
  RiskListData: any;
  position;
  riskForms: FormGroup;
  ControlData: any;
  allControls = [];
  filteredControls = [];
  assignedControls: any = [];
  deletedControls: any = [];
  assignedIssue: any;

  assessmentData: any = {};
  likelyhoods = [
    { name: "low", value: 1 },
    { name: "medium", value: 2 },
    { name: "high", value: 3 },
  ];

  impacts = [
    { name: "low", value: 1 },
    { name: "medium", value: 2 },
    { name: "high", value: 3 },
  ];
  intrinsikRiskResult = {};
  residalRiskResult = {};

  intriiskAvg = 0;
  risidualAvg = 0;
  currRisiDualAvg: any;
  btnColor: any;
  btnName: any;
  randomRiskAssementId: string;

  residulaRA = [
    {
      riskType: "Legal",
      likelyhood: "",
      impact: "",
    },
    {
      riskType: "Financial",
      likelyhood: "",
      impact: "",
    },
    {
      riskType: "Reputational",
      likelyhood: "",
      impact: "",
    },
  ];

  intrinsikRA = [
    {
      riskType: "Legal",
      likelyhood: "",
      impact: "",
    },
    {
      riskType: "Financial",
      likelyhood: "",
      impact: "",
    },
    {
      riskType: "Reputational",
      likelyhood: "",
      impact: "",
    },
  ];

  isData = false;
  newCIds: any = [];
  submitted: boolean;
  content: string;
  title: string;
  newCopyCount: any;
  updateNewCount: any;

  constructor(
    private httpService: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private cd: core.ChangeDetectorRef,
    private mScrollbarService: MalihuScrollbarService
  ) { }

  ngOnInit() {
    this.initForm();
    this.mScrollbarService.initScrollbar('.scroll-container', { axis: 'y', theme: 'metro' });

  }


  ngOnChanges() {
    //console.log("componentData", this.riskAssementId, this.componentData);
    if (
      this.riskAssementId &&
      this.riskAssementId &&
      !this.componentData.isNewAssessment
    ) {
      this.getAssessmentRisk();
      this.getAssessmentDetails();
    }

    this.httpService.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        //console.log("controls", response);
        this.allControls = response;
        this.filteredControls = response;
        this.filterControls();
      }
    });
  }

  ngAfterContentView() { }

  getAssessmentDetails() {
    const payload = { riskAssementId: this.riskAssementId };
    //console.log("payload riskAssementId", payload);
    this.httpService
      .securePost(ConstUrls.getAssessmentDetails, payload)
      .subscribe((response: any) => {
        //console.log("response getAssessmentDetails###", response);
        if (!response) {
          return;
        }
        this.assessmentData = response;
        this.riskForms.patchValue(response);
        this.setData();
        // this.assessmentData = this.RiskListData;
      });
  }
  deleteControl(controlId) {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to remove this control?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {
        this.deletedControls.push(controlId);
        //console.log("remove control:", controlId);
        this.assignedControls = this.assignedControls.filter(
          (controls) => controls.controlId !== controlId
        );
        this.filterControls();
      }
    });
  }

  duplicateAssessement()
  {
 
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });
    this.newCopyCount = parseInt(this.assessmentData.copyCount) + 1;
    this.riskForms.patchValue({'isCopied':true , 'copyCount' :this.newCopyCount , 'riskId':this.assessmentData.riskId})
    const payload = this.riskForms.value;
    delete payload._id;
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) { this.httpService
        .securePost(ConstUrls.postOneAssessmentForRisk, payload)
        .subscribe((response) => {
          if (response) {
            this.content = 'Assessment duplicate successfully...';
            this.title = 'Success';
          }else{
            this.content = 'Failed duplicate Assessment...';
            this.title = 'Error';
          }
        this.myAlert(this.content , this.title);
         
  
        }); }
    })
    
    this.eventClosed.emit(status);
      

      this.closeModal();
    // console.log('Form Data:' , this.riskForms.value , 'Assessment Data: ', this.assessmentData);

    
  }

  myAlert(content , title){
    const initialState = {title: title,content: content,link: 'Ok'};
    this.modalRef = this.modalService.show(DefaultModalComponent,{initialState,class: 'success-class',backdrop: 'static',keyboard: false,});
  }


  filterControls() {
    if (this.allControls) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
    addControl(item) {
    //console.log(this.assignedControls);
    if (item) {
      this.newCIds.push(item);
      const assignedControl = this.allControls.find(
        (control) => control.controlId === item
      );
      this.assignedControls.push(assignedControl);
      this.filterControls();
    }
  }

  getAssessmentRisk() {
    const payload = { riskAssementId: this.riskAssementId };
    //console.log("getAssessmentRisk######################", payload);

    this.httpService
      .securePost(ConstUrls.getRiskAssessmentDetails, payload)
      .subscribe((response: any) => {
        //console.log("recevied assessment::###", response);
        if (!response) {
          return;
        }

        this.isData = true;
        this.RiskListData = response;
        //console.log("this.RiskListData", this.RiskListData);
        this.riskId = response.riskId;
        this.riskForms.patchValue(response);
        const riskFormData = response.riskId;
        this.riskForms.patchValue(riskFormData);
        this.assessmentData = this.RiskListData;
        //console.log("Assessment DAta: ", this.assessmentData);
        this.getOneControl();
      });
  }

  getOneControl() {
    const payload = {
      riskId: this.RiskListData.riskId,
    };
    //console.log("payload for get controls", payload);
    this.httpService
      .securePost(ConstUrls.getOneControls, payload)
      .subscribe((res) => {
        //console.log("get one control:", res);
        this.ControlData = res;
      });
  }

  setData() {
    const residualRiskControl = this.riskForms.get("residualRisk") as FormArray;
    const intrinsicRiskControl = this.riskForms.get(
      "intrinsicRisk"
    ) as FormArray;

    /*console.log(
      "this.assessmentData.residualRisk in setData ##",
      this.assessmentData.residualRisk
    ); */
    if (this.assessmentData && this.assessmentData.residualRisk) {
      for (const rskCtrl of this.assessmentData.residualRisk) {
        const grp = this.formBuilder.group({
          riskType: [rskCtrl.riskType, []],
          likelyhood: [rskCtrl.likelyhood, []],
          impact: [rskCtrl.impact, []],
          result: [rskCtrl.result, []],
        });
        residualRiskControl.push(grp);
      }
    }

    if (this.assessmentData && this.assessmentData.intrinsicRisk) {
      for (const rskCtrl of this.assessmentData.intrinsicRisk) {
        const grp = this.formBuilder.group({
          riskType: [rskCtrl.riskType, []],
          likelyhood: [rskCtrl.likelyhood, []],
          impact: [rskCtrl.impact, []],
          result: [rskCtrl.result, []],
        });
        intrinsicRiskControl.push(grp);
      }
    }

    this.riskAvg(); // FOr get avg
  }

  initForm() {
    this.riskForms = this.formBuilder.group({
      _id: [""],
      isAuditable: [false, []],
      fieldAudit: [false, []],
      automatedControls: [false, []],
      timePeriod: ["", []],
      riskDescription: ["", []],
      riskSource: ["", []],
      riskProcess: ["", []],
      riskCategory: ["", []],
      riskArea: ["", []],
      periodInScope: ["", []],
      riskPotentialoutcome: ["", []],
      residualRisk: this.formBuilder.array([]),
      intrinsicRisk: this.formBuilder.array([]),
      riskIdentifiedDate: ["", []],
      riskId: ["", []],
      riskSubmittedDate: ["", []],
      riskLastChangeDate: ["", []],
      riskLastApprovedDate: ["", []],
      riskOwnerId: ["", []],
      riskSubmitter: ["", []],
      riskSubmitterId: ["", []],
      riskApprovedBy: ["", []],
      riskOwnerName: ["", []],
      riskApproverId: ["", []],
      riskDepartment: ["", []],
      controlId: ["", []],
      controlName: ["", []],
      controlDescription: ["", []],
      controlType: ["", []],
      controlDepartment: ["", []],
      controlKeyProcess: ["", []],
      riskAssementId: ["", []],
      isCopied:["", []],
      copyCount:["", []],
    });
  }

  get getResidualRisk(): FormArray {
    return this.riskForms.get("residualRisk") as FormArray;
  }

  get getIntrinsicRisk(): FormArray {
    return this.riskForms.get("intrinsicRisk") as FormArray;
  }

  getRiskResult(likelihood = 0, impact = 0, i, type) {

    const rs = Math.max(likelihood, impact);
  
    if (rs === 1) {

      this.setResultValue(type, rs, i);
    }

    if (rs === 2) {
      // console.log('medium risk')
      // this.btnColor = 'btn btn-primary year-button medium btn-with-head';
      // this.btnName = 'MEDIUM';
      this.setResultValue(type, rs, i);
    }
    if (rs === 3) {
      // this.btnColor = 'btn btn-primary year-button high btn-with-head';
      // this.btnName = 'HIGH';
      this.setResultValue(type, rs, i);
    }

    const res = { value: rs, btnColor: this.btnColor, btnName: this.btnName };

    if (type === "intrinsic") {
      this.riskAvg(type);
    }
    if (type === "residual") {
      this.riskAvg(type);
    }
    return res;
  }

  setResultValue(type, value, i) {
    //console.log("type", type);
    if (type === "intrinsic") {
      this.getIntrinsicRisk.controls[i].get("result").patchValue(value);
    }

    if (type === "residual") {
      this.getResidualRisk.controls[i].get("result").patchValue(value);
    }

    this.cd.detectChanges();
  }

  riskAvg(type?) {
    // if (type === 'intrinsic') {
    const result1 = this.getIntrinsicRisk.value.map((el) => el.result);
    this.intriiskAvg = Math.max(...result1);
    // }

    // if (type === 'residual') {
    const result2 = this.getResidualRisk.value.map((el) => el.result);
    this.risidualAvg = Math.max(...result2);
    // }
  }


  async onSave() {

    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

    const ids = [];

    await this.assignedControls.forEach((element) => {
      ids.push(element.controlId);
    });
    this.riskForms.value.controlId = await ids;
    this.submitted = true;

    const payload = this.riskForms.value;
    if (!this.componentData.isNewAssessment) {
      payload.riskId = payload._id;
      payload.riskAssementId = this.riskAssementId;
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) {    this.httpService
          .securePut(ConstUrls.updateOneAssessmentRisk, payload)
          .subscribe((response) => {
            
  
            if (response) {
              const initialState = {
                title: "Success",
                content:
                  "Assessment :" + this.riskAssementId + " updated Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
      this.eventClosed.emit(status);
  
              this.closeModal();
            }
  
            // this.updateRisk()
            // this.toasterService.showSuccess('Risk Assessment updsted Successfully!')
            // alert('Saved Successfully!');
          });}
      })
  
    } else {
      const tempId = "RA-" + Math.floor(Math.random() * 100) + 1;

      payload.riskAssementId = tempId;
      payload.riskId = this.riskAssementId.riskId._id;
      delete payload._id;
      //console.log("payload new assessment", payload);
      this.modalRef.content.onClose.subscribe((r) => {
        if (r) { 
          this.httpService
          .securePost(ConstUrls.postOneAssessmentForRisk, payload)
          .subscribe((response) => {
            //console.log("response", response);
            if (response) {
              const initialState = {
                title: "Success",
                content: "Control saved Successfully",
                link: "Ok",
              };
              this.modalRef = this.modalService.show(DefaultModalComponent, {
                initialState,
                class: "success-class",
                backdrop: "static",
                keyboard: false,
              });
              this.closeModal();
            }
          });
         }
      })

      // this.updateRisk()
      // this.toasterService.showSuccess('Risk Assessment updsted Successfully!')
      // alert('Saved Successfully!');
    }
  }

  updateRisk() {
    const payload = this.riskForms.value;
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {     this.httpService
        .securePut(ConstUrls.updateRcmRisk, payload)
        .subscribe((response) => {
  
          alert("updated Successfully!");
      this.eventClosed.emit(status);
  
          this.closeModal(true);
        });}
    })

  }

  closeModal(status = false) {
    $(".scroll-container").scrollTop(0);
    $(".pop-up-form").addClass("d-none");
    this.RiskAssessmentForm.tabs[0].active = true;
    this.position = this.position ? undefined : {x: 0, y: 0};
  }
}
