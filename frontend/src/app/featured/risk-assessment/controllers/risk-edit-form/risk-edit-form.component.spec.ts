import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskEditFormComponent } from './risk-edit-form.component';

describe('RiskEditFormComponent', () => {
  let component: RiskEditFormComponent;
  let fixture: ComponentFixture<RiskEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
