import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentCasesComponent } from './assessment-cases.component';

describe('AssessmentCasesComponent', () => {
  let component: AssessmentCasesComponent;
  let fixture: ComponentFixture<AssessmentCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
