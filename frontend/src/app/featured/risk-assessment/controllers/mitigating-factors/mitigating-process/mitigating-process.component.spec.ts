import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitigatingProcessComponent } from './mitigating-process.component';

describe('MitigatingProcessComponent', () => {
  let component: MitigatingProcessComponent;
  let fixture: ComponentFixture<MitigatingProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitigatingProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitigatingProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
