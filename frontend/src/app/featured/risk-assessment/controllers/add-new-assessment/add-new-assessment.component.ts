import * as core from '@angular/core';
import { HttpService } from '../../../../core/http-services/http.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { DefaultModalComponent } from 'src/app/layout/modal/default-modal/default-modal.component';
import { ConstUrls } from 'src/app/config/const-urls';
import { ConfirmPopupComponent } from "src/app/layout/modal/confirm-popup/confirm-popup.component";

import { ToastrServiceService } from 'src/app/core/toastr-service/toastr-service.service';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
declare var $;
@core.Component({
  selector: 'app-add-new-assessment',
  templateUrl: './add-new-assessment.component.html',
  styleUrls: ['./add-new-assessment.component.less']
})

export class AddNewAssessmentComponent implements core.OnInit, core.OnChanges {
  @core.Input('componentData') componentData;
  @core.Input('riskAssementId') riskAssementId: any;

  @core.Output('eventClosed') eventClosed = new core.EventEmitter();
  @core.ViewChild('RiskAssessmentForm', { static: false })
  RiskAssessmentForm: TabsetComponent;
  searchRiskId: any;
  riskId: any;
  modalRef: BsModalRef;
  RiskListData: any;
  riskForms: FormGroup;
  ControlData: any;
  allControls = [];
  filteredControls = [];
  assignedControls: any = [];
  assignedIssue: any;

  assessmentData: any = {};
  likelyhoods = [
    { name: 'low', value: 1 },
    { name: 'medium', value: 2 },
    { name: 'high', value: 3 },
  ];

  impacts = [
    { name: 'low', value: 1 },
    { name: 'medium', value: 2 },
    { name: 'high', value: 3 },
  ];
  intrinsikRiskResult = {};
  residalRiskResult = {};

  intriiskAvg = 0;
  risidualAvg = 0;
  currRisiDualAvg: any;
  btnColor: any;
  btnName: any;
  randomRiskAssementId: string;

  residulaRA = [
    {
      riskType: 'Legal',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
    {
      riskType: 'Financial',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
    {
      riskType: 'Reputational',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
  ];

  intrinsikRA = [
    {
      riskType: 'Legal',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
    {
      riskType: 'Financial',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
    {
      riskType: 'Reputational',
      likelyhood: 0,
      impact: 0,
      result: 0
    },
  ];

  isData = false;
  updatedRiskAssessmentId: any;

  constructor(
    private httpService: HttpService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private cd: core.ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.initForm();
    this.RiskListData = this.riskAssementId;
    let newRiskId = this.RiskListData.riskId.split("-");
    this.updatedRiskAssessmentId = 'RA-'+newRiskId[1];
     this.riskForms.patchValue(this.RiskListData);
    this.riskForms.patchValue(this.intrinsikRA);
    this.riskForms.patchValue(this.residulaRA);
    this.setData();

  }


  ngOnChanges() {
    //console.log('componentData', this.riskAssementId, this.componentData);
    if (
      this.riskAssementId &&
      !this.componentData.isNewAssessment
    ) {
      this.getAssessmentRisk();
      this.getAssessmentDetails();
    }

    this.httpService.get(ConstUrls.getControls).subscribe((response: any) => {
      if (response) {
        //console.log('controls', response);
        this.allControls = response;
        this.filteredControls = response;
        // this.filterControls();
      }
    });
  }

  ngAfterContentView() {

  } 
  deleteControl(id){

  }

  getAssessmentDetails() {
    const payload = { riskAssementId: this.riskAssementId };
    //console.log('payload riskAssementId', payload);
    this.assessmentData.residualRisk = this.residulaRA;
    //console.log('this.assessmentData.residualRisk', this.assessmentData.residualRisk);
    this.assessmentData.intrinsicRisk = this.intrinsikRA;
    //console.log('this.assessmentData.intrinsicRisk', this.assessmentData.intrinsicRisk);
    this.setData();

    this.httpService
      .securePost(ConstUrls.getAssessmentDetails, payload)
      .subscribe((response: any) => {
        //console.log('response getAssessmentDetails###', response);
        if (!response) {
          return;
        }
        this.assessmentData = response;
        this.riskForms.patchValue(response);
        // this.riskForms.intrinsicRisk.value = this.intrinsikRA;
        // this.riskForms.get('residualRisk.value = this.residulaRA;
        // this.setData()
        this.setData();
        // this.assessmentData = this.RiskListData;
      });
  }


  filterControls() {
    if (this.allControls) {
      const selectedIds = [];
      this.assignedControls.forEach((element) => {
        selectedIds.push(element.controlId);
      });
      this.filteredControls = this.allControls.filter(
        (controls) => selectedIds.indexOf(controls.controlId) === -1
      );
      // console.log("Filtered Controls:", this.filterControls);
    }
  }
  addControl(item) {
    //console.log(this.assignedControls);
    if (item) {
      // this.newCIds.push(item);
      const assignedControl = this.allControls.find(
        (control) => control.controlId === item
      );
      this.assignedControls.push(assignedControl);
      this.filterControls();
    }
  }

  getAssessmentRisk() {
    const payload = { riskAssementId: this.riskAssementId.riskId };
    //console.log('getAssessmentRisk######################', payload);

    this.httpService
      .securePost(ConstUrls.getRiskAssessmentDetails, payload)
      .subscribe((response: any) => {
        //console.log('recevied assessment::###', response);
        if (!response) {
          return;
        }

        this.isData = true;
        this.RiskListData = response;
        //console.log('this.RiskListData', this.RiskListData);
        this.riskId = response.riskId;
        this.riskForms.patchValue(response);
        const riskFormData = response.riskId;
        this.riskForms.patchValue(riskFormData);
        this.assessmentData = this.RiskListData;
        //console.log('Assessment DAta: ', this.assessmentData);
        this.getOneControl();
      });
  }

  getOneControl() {
    const payload = {
      riskId: this.RiskListData.riskId,
    };
    //console.log('payload for get controls', payload);
    this.httpService
      .securePost(ConstUrls.getOneControls, payload)
      .subscribe((res) => {
        //console.log('get one control:', res);
        this.ControlData = res;
      });
  }

  setData() {
    const residualRiskControl = this.riskForms.get('residualRisk') as FormArray;
    const intrinsicRiskControl = this.riskForms.get(
      'intrinsicRisk'
    ) as FormArray;


    if (this.riskAssementId && this.residulaRA) {
      for (const rskCtrl of this.residulaRA) {
        const grp = this.formBuilder.group({
          riskType: [rskCtrl.riskType, []],
          likelyhood: [rskCtrl.likelyhood, []],
          impact: [rskCtrl.impact, []],
          result: [rskCtrl.result, []],
        });
        residualRiskControl.push(grp);
      }
    }

    if (this.riskAssementId && this.intrinsikRA) {
      for (const rskCtrl of this.intrinsikRA) {
        const grp = this.formBuilder.group({
          riskType: [rskCtrl.riskType, []],
          likelyhood: [rskCtrl.likelyhood, []],
          impact: [rskCtrl.impact, []],
          result: [rskCtrl.result, []],
        });
        intrinsicRiskControl.push(grp);
      }
    }

    this.riskAvg(); // FOr get avg

  }

  initForm() {
    this.riskForms = this.formBuilder.group({
      _id: [''],
      isAuditable: [false, []],
      fieldAudit: [false, []],
      automatedControls: [false, []],
      timePeriod: ['', []],
      riskDescription: ['', []],
      riskSource: ['', []],
      riskProcess: ['', []],
      riskCategory: ['', []],
      riskArea: ['', []],
      periodInScope: ['', []],
      riskPotentialoutcome: ['', []],
      residualRisk: this.formBuilder.array([]),
      intrinsicRisk: this.formBuilder.array([]),
      riskIdentifiedDate: ['', []],
      riskId: ['', []],
      riskSubmittedDate: ['', []],
      riskLastChangeDate: ['', []],
      riskLastApprovedDate: ['', []],
      riskOwnerId: ['', []],
      riskSubmitter: ['', []],
      riskSubmitterId: ['', []],
      riskApprovedBy: ['', []],
      riskOwnerName: ['', []],
      riskApproverId: ['', []],
      riskDepartment: ['', []],
      controlId: ['', []],
      controlName: ['', []],
      controlDescription: ['', []],
      controlType: ['', []],
      controlDepartment: ['', []],
      controlKeyProcess: ['', []],
      riskAssementId: ['', []],
    });
  }

  get getResidualRisk(): FormArray {
    return this.riskForms.get('residualRisk') as FormArray;
  }

  get getIntrinsicRisk(): FormArray {
    return this.riskForms.get('intrinsicRisk') as FormArray;
  }

  getRiskResult(likelihood = 0, impact = 0, i, type) {
    const rs = Math.max(likelihood, impact);
    if (rs === 1) {
      this.setResultValue(type, rs, i);
    }

    if (rs === 2) {
      this.setResultValue(type, rs, i);
    }
    if (rs === 3) {
      this.setResultValue(type, rs, i);
    }

    const res = { value: rs, btnColor: this.btnColor, btnName: this.btnName };

    if (type === 'intrinsic') {
      this.riskAvg(type);
    }
    if (type === 'residual') {
      this.riskAvg(type);
    }
    return res;
  }

  setResultValue(type, value, i) {
    //console.log('type', type);
    if (type === 'intrinsic') {
      this.getIntrinsicRisk.controls[i].get('result').patchValue(value);
    }

    if (type === 'residual') {
      this.getResidualRisk.controls[i].get('result').patchValue(value);
    }

    this.cd.detectChanges();
  }

  riskAvg(type?) {
    const result1 = this.getIntrinsicRisk.value.map((el) => el.result);
    this.intriiskAvg = Math.max(...result1);
    const result2 = this.getResidualRisk.value.map((el) => el.result);
    this.risidualAvg = Math.max(...result2);

  }


  onSave() {
    const initialState = {
      title: "Confirmation",
      content: `Do you want to save current changes?`,
      link: "Confirm",
      action: "Cancel",
      confirmFlag: false,
    };
    this.modalRef = this.modalService.show(ConfirmPopupComponent, {
      initialState,
      class: "success-class",
      backdrop: "static",
      keyboard: false,
    });

    const payload = this.riskForms.value;
    // console.log('vaibhac',payload)
    const tempId = 'RA-' + Math.floor(Math.random() * 100) + 1;
   
    payload.riskAssementId = this.updatedRiskAssessmentId;
    payload.riskId = this.riskAssementId._id;
    delete payload._id;
    //console.log('payload new assessment', payload);
    this.modalRef.content.onClose.subscribe((r) => {
      if (r) {    this.httpService
        .securePost(ConstUrls.postOneAssessmentForRisk, payload)
        .subscribe((response) => {
          //console.log('response', response);
          if (response) {
            this.updateRisk();
            const initialState = {
              title: 'Success',
              content: 'New Assessment Saved Successfully',
              link: 'Ok',
            };
            this.modalRef = this.modalService.show(DefaultModalComponent, {
              initialState,
              class: 'success-class',
              backdrop: 'static',
              keyboard: false,
            });
            this.closeModal();
          }
        });}
    })
 
    // this.toasterService.showSuccess('Risk Assessment updsted Successfully!')
    // alert('Saved Successfully!');

  }

  updateRisk() {
    const payload = this.riskForms.value;
    payload.riskId = this.riskAssementId.riskId;
    //console.log('payload updateRisk', payload);

    this.httpService
      .securePut(ConstUrls.updateOneRiskAssessment, payload)
      .subscribe((response) => {
        //console.log('response', response);
        // alert("updated Successfully!");
        this.closeModal(true);
      });
  }

  closeModal(status = false) {
    $('.scroll-container').scrollTop(0);
    $('.pop-up-form').addClass('d-none');
    this.eventClosed.emit(status);
    // this.RiskAssessmentForm.tabs[0].active = true;
  }
}
