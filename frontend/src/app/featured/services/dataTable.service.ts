import { Injectable } from '@angular/core'

declare var $

@Injectable({
  providedIn: 'root',
})

export class DataTableService {
	cbDropdown(column) {
		return $("<div>", {
			class: "dropdown-menu",
		}).appendTo(
			$(
				'<div class="dropdown filter-dropdown">' +
					'<button type="button" class="btn btn-primary" data-toggle="dropdown"><span class="filter-icon"></span></button>',
				{}
			).appendTo(column)
		);
	}

  initializeTable(tbl, tblType) {
   $(".tab-block .nav-tabs li").on("click", function (e) {
      $.fn.dataTable.tables({ visible: true, api: true })
    .columns.adjust().fixedColumns().relayout();
    //$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().responsive.recalc();
    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw()
    });
    $(".buttons-collection").on("click", function (e) {
       $(".dropdown-menu-column").removeClass("show-bar");
    });
    
		const that = this
		let scrollY: string
		let columns: object
    let columnDefTargets: number
    let searching: boolean
    let exportColumns: object
    let pdfTitle: string
    let responsive: any
    if(tblType === `risk`) {
			scrollY = `50vh`
			columns = [0, 2, 4, 5]
      columnDefTargets = 6
      searching = true
      exportColumns = [0, 2, 3, 4, 5]
      pdfTitle = 'Risk List - RCM'
      responsive = {
        details: {
          renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
        }
      }
		} else if(tblType === `control`) {
			scrollY = `100vh`
			columns = [3, 4, 5]
      columnDefTargets = 6
      exportColumns = [0, 2, 3, 4, 5]
      searching = true
      pdfTitle = 'Control List - RCM'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `controlRCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 8, 9]
      columnDefTargets = 10
      searching = true
      exportColumns = [0, 2, 3, 4, 5, 6, 7, 8, 9]
      pdfTitle = 'Control Matrix - RCM'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `riskRCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 7, 8, 9]
      columnDefTargets = 10
      exportColumns = [2, 3, 4, 5, 6, 7, 8, 9]
      pdfTitle = 'Risk Matrix - RCM'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `rcmcontrolworkFlow`) {
			scrollY = `100vh`
			columns = [3, 4, 6, 7, 8]
      columnDefTargets = 11
      searching = true
      exportColumns = [2, 3, 4, 5, 6, 7, 8, 9, 10]
      pdfTitle = 'Control Work Flow - RCM'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `controlFormWF`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 6
      searching = false
      exportColumns = [0]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `budgetingEP`) {
			scrollY = `100vh`
			columns = [2, 3, 7, 8]
      columnDefTargets = 10
      exportColumns = [2, 3, 4, 5, 6, 7, 8, 9]
      pdfTitle = 'Budgeting - Engagement Planning'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `activeUniverse`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 6
      searching = false
      exportColumns = [2, 3, 4, 5]
      pdfTitle = 'Budgeting - Engagement Planning'
      responsive = false
    } else if(tblType === `currentactiveUniverse`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 6
      searching = false
      exportColumns = [2, 3, 4, 5]
      pdfTitle = 'Budgeting - Engagement Planning'
      responsive = false
    } else if(tblType === `annualAuditPlan`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 7
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      pdfTitle = 'Budgeting - Engagement Planning'
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `activeAuditReporting`) {
			scrollY = `100vh`
			columns = [2, 3, 6]
      columnDefTargets = 11
      exportColumns = [2, 3, 4, 5, 6, 7, 8, 9]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `costReporting`) {
			scrollY = `100vh`
			columns = [2, 3, 5, 6, 7, 9]
      columnDefTargets = 11
      exportColumns = [2, 3, 4, 5, 6, 7, 8, 9]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `activeControlTesting`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `controlLIRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `issuesActionsLIRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `processesLIRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `completedRARisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `ongoingRARisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `controlMFRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `issuesActionsMFRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } else if(tblType === `processesMFRisk`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `activeControlExecution`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6 , 7]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `newLinkedItems`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `pasttesting`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `issuetesting`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }

    else if(tblType === `controlexcutionpast`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }

    else if(tblType === `controlexcutionaction`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }

    else if(tblType === `riskPastAssessments`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 0
      searching = false
      exportColumns = [2, 3, 4, 5, 6]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    
    

    else if(tblType === `risklistAssement`) {
			scrollY = `100vh`
			columns = [2, 4, 5]
      columnDefTargets = 9
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } 
   // else if(tblType === `topTenRiskAssement`) {
		// 	scrollY = `100vh`
		// 	columns = [2, 4, 5]
		// 	columnDefTargets = 10
    // } 
    else if(tblType === `CTcontrollist`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 8, 9, 10]
      columnDefTargets = 12
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `issueListCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 6, 7, 8, 9]
      columnDefTargets = 10
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    }
    else if(tblType === `actionlistCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 7, 8, 9]
      columnDefTargets = 10
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `issueMatrixCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 7, 8, 9]
      columnDefTargets = 10
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `actionMatrixCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5, 7, 8, 9]
      columnDefTargets = 10
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		} else if(tblType === `actionWorkFlowCM`) {
			scrollY = `100vh`
			columns = [2, 4, 5]
      columnDefTargets = 9
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
    } 
    else if(tblType === `PBCdocument`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 7
      searching = false
      exportColumns = [0]
      responsive = {
      details: {
        renderer: $.fn.dataTable.Responsive.renderer.listHiddenNodes()
      }
    }
		}
    // else if(tblType === `issueActionAgile`) {
		// 	scrollY = `100vh`
		// 	columns = [0]
    //   columnDefTargets = 4
    //   searching = false
    //   exportColumns = [0]
    //   pdfTitle = ''
    //   responsive = false
    // } 
    else if(tblType === `issueActionSelectedAgile`) {
			scrollY = `100vh`
			columns = [0]
      columnDefTargets = 4
      searching = true
      exportColumns = [0]
      pdfTitle = ''
      responsive = false
    } 
		const dtParams = {
       processing: true,
       serverSide: false,
      oLanguage: {
        sSearch: ""
      },
      destroy: true,
      language: {
        searchPlaceholder: "Search",
      },
      dom: "<Rl><B>frtip",
      colReorder: {
        fixedColumnsRight: 1,
      },
      rowReorder: {
        selector: ".reorder-row"
      },
      details: false,
      fixedHeader: true,
      autoWidth: true,
      scrollY,
      scrollX: true,
      scrollCollapse: true,
      bPaginate: true,
      lengthMenu: [
        [5, 10, 20, 50, 100, -1],
        [5, 10, 20, 50, 100, "All"],
      ],
      bInfo: true,
      bSort: true,
      deferRender: true,
      searching,
      responsive,
      buttons: [
        {
            extend: 'collection',
            text: '<a class="tooltip btn"><span class="tooltiptext">Export</span><img src="assets/img/export_button.svg" alt="Export" /></a>',
            autoClose: true,
            buttons: [
              {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: exportColumns
 
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: exportColumns
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: exportColumns
                }
            },
            {
              extend: 'pdfHtml5',
              exportOptions: {
                  columns: exportColumns
              },
              orientation: 'landscape',
              pageSize: 'A4',
              title:  'ENIA',
              customize: function (doc) {
                doc.content.splice( 1, 0, {
                  margin: [ 0, 0, 0, 40 ],
                  alignment: 'left',
                  image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGgAAAAmCAYAAAAoTt69AAAABHNCSVQICAgIfAhkiAAABRFJREFUaEPtmkFsG0UUhv+36QmJIyAVCSGhCirgggTE5lBouaDKUQvE5UAhtknqSKgVanNAwAW1PbVC0INdp3UCFRJxaIlijhRRCZwAEgfUqEIcGjggAUckn+J9aHaz9u7srD27ttNtsnv1zOyb981775+3JiRPrD1AsbYuMQ4JoJgfggRQAijmHoi5eUkEJYBi7oGYm5dEUAIoHh7Ij115dBdvnAXoZTBummSeuFTPfxsP64KtaEdQuZa+CeDxCAavFbONJyLM6zplKjN/HMDHvdflawDdBvj7Sj23FDR+MjP3HYH2uX/foNaD1eXCX73eUV4YzQCUB9Eer4+4Kd7N4F+IebF4ZLXea62wv28DQK4tB0TGZGbuaQL9pHDOiUp94pMgp5UX0zkwToY4uGtg891BgtpegNqe5sPuaIoCqLyQ+gpEh8KeeDGegTPT2cb7UebKc7YpIEBOX2FSXLmW+gagA9EczE0wvzaoKOoKiMFXprMrb0QztL9Z6hrE1yr13CvOynbhb00CVhryPMz4YPbridOdsZd372LjgiMSmDg/W8/9LM8r1dKnCXjPbz03GbjqrjUXvxx91jTpdQLyAN0z6OgR693VgBwnTmXmz/kheWHqHJeLteceYpi3HGc7cxj4l9gsBEWFmGfCnAHR+vR447zOu3THDBRQuZbmzou5SQbvt06VSWfcKcPaMPhXgpE/lv3hT5WxOhHkzFPXGC+gqczcVSt6XE+lPuG5B5ZqqQsEettnD5tj/aYsByKBXnCLDscXIPq8ON6YC1WDwqY4LyALzY8AnpRPpBtiUL4OA+itTHW/AeO6lOQ86VAHUHkhdRtED0vrXC9mV17UPfGqcXba5HeC/dCetUaGWTj26qrwm/UMMYL0tiRO0HS2cb88Ogygycz8JQIK7jXkGtQLkJ3e+A/ZDiac6idthVeDduZxIEVWcaro8keQJiSFE3QACZEwwq0ZGY546waNPFZdPvpbp051T3Gbd56qL8UY5qj7ROvtyB4VtKbGGu3L//ABMa/DoA9FfhWqh1v0hS+NMC8Vj6wcdhuu30nwb1eOHjGiVwQFObOYbUTuVypTpvAH+LioaZ261FGBzm6cyB06IHmDAY7wtYuiAlLBuROArMNoGqtSPWsSjL2yMCrVUp8R6Kiq9m05ICv0PWrPMqtvQAy+AWBGdbfpBxAiKrjSYvokMYT8dz2sFBwqmE5tHrpIUKWIYQCyveBt8XhTZvcaFCgSIrZtVFHRTRWr6rfw3V0GyL7b5Mcu3zvCIx+pxAGDn1FFUa8aJGCWaul/CLhPqmqRuvU7GpBwoA3J+J1AD3iSCPjGbD33vCwd9ACpaoF1GcmrLpHd1NgOTXFSd+Dgp2/C4HmFo3yfEXQABbV6gN4NUDtisNvpjmyJSOi/kwAMtgb5+2uqLjWD/26R+ZT7Y5wOIDvNBUSRVeJ4CeCq0/axZDLxODFy7fYN8zqRsU8oNeVHUE2Z7URtZBXnOrXtHB1U6OQTHl0kKAEFfIwL34tz7Ozvc0NHld7Ri2pcANkSWtXNBkyYB5z/HehGUP+QvOmwa0Qqi9iAWj1xAtRFMIhUt6e6XPgvLKBNVSe+J00plF2QPvA1O511htIs7aZS4gRo8yJ6iIGyrOoAnK/UJ05FAeTs0VZk/BIYj3jbVPp/Gun7c4MmjGTYFnsgciNwi+3csa9LAMUcfQIoARRzD8TcvCSCEkAx90DMzfsfv+ERY/1SodMAAAAASUVORK5CYII='
                  } );
                var today = new Date();
                var currentdate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                doc.pageMargins = [40,40,40,40];
                doc.defaultStyle.fontSize = 10;
                doc.styles.tableHeader.fontSize = 10;
                doc.styles.tableHeader.alignment = 'left';
                doc.styles.title.fontSize = 0;
                doc.styles.title.color = '#98aa3a';
                doc.styles.title.bold = true;
                doc.styles.title.alignment = 'left';
                // Remove spaces around page title
                doc.content[0].text = doc.content[0].text.trim();
                // Create a footer
                doc['footer']=(function(page, pages) {
                    return {
                        columns: [
                            'Date' + ' ' + currentdate,
                            {
                                // This is the right column
                                alignment: 'right',
                                text: ['Page ', { text: page.toString() },  ' of ', { text: pages.toString() }]
                            }
                        ],
                        margin: [40, 0]
                    }
                });
                // Styling the table: create style object
                var objLayout = {};
                // Horizontal line thickness
                objLayout['hLineWidth'] = function(i) { return .5; };
                // Vertikal line thickness
                objLayout['vLineWidth'] = function(i) { return .5; };
                // Horizontal line color
                objLayout['hLineColor'] = function(i) { return '#aaa'; };
                // Vertical line color
                objLayout['vLineColor'] = function(i) { return '#aaa'; };
                // Left padding of the cell
                objLayout['paddingLeft'] = function(i) { return 10; };
                // Right padding of the cell
                objLayout['paddingRight'] = function(i) { return 10; };
                // Inject the object in the document
                doc.content[1].layout = objLayout;
            }
          }
            ]
        }
    ],
      columnDefs: [
        { targets: 0, visible: false },
        {
          targets: 1,
          orderable: false,
          className: "dt-body-center",
          render: function () {
            return '<input type="checkbox" name="" value="">';
          },
        },
        {
          targets: columnDefTargets,
          orderable: false,
          responsivePriority: columnDefTargets
        },
      ],
      select: {
        style: "multi",
      },
      initComplete: function () {        
          this.api()
          .columns(columns)
          .every(function () {
            var TotChkbxs = 0;
            var TikdChkbxs = 0;
            var column = this;
            var ddmenu = that.cbDropdown($(column.header())).on(
              "change",
              ":checkbox",
              function () {
                var active;
                var vals = $(":checked", ddmenu)
                  .map(function (index, element) {
                    active = true;
                    return $.fn.dataTable.util.escapeRegex($(element).val());
                  })
                  .toArray()
                  .join("|");

                column
                  .search(
                    vals.length > 0 ? "^(" + vals + ")$" : "",
                    true,
                    false
                  )
                  .draw();
              }
            );
              
            column
              .data()
              .unique()
              .sort()
              .each(function (d, j) {
                var html_val = $.parseHTML(d)
                if (html_val != null) {    
                        
                  var new_d = $(html_val)[0].textContent        
                  var $label = $("<label>"),
                  $text = $("<span>", {
                    class: "filter-text",
                    text: new_d,
                  }),
                  $cb = $("<input>", {
                    class: "checkbox",
                    type: "checkbox",
                    value: new_d,
                  });
                }
               
                
                $('.buttons-collection').on('click', function(){
                  $cb.remove();
                  $text.remove();
                  $(".filter-dropdown").removeClass("show-dropdown-menu");
                })
                $('.filter-dropdown button').on('click', function(){
                  $cb.appendTo($label);
                  $text.appendTo($label);
                  
                })
               
                ddmenu.append(
                  $("<a>", {
                    class: "dropdown-item",
                  }).append($label)
                );
               
              });
          });
       
      }
    }
		if(tblType === `riskRCM`) {
			dtParams[`serverSide`] = false
		}

		return tbl.DataTable(dtParams)
	}  
}
