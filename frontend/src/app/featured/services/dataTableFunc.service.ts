import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})

export class DataTableFuncService {
	filterDropDown() {
		$(".filter-dropdown").unbind().click(function(e) {
      $(".dataTables_scrollHead").addClass("overflow-table-active");
      $(".filter-dropdown").removeClass("show-dropdown-menu");
      $(".filter-dropdown .dropdown-menu").removeClass("show");
      $(this)
        .find(".dropdown-menu")
        .toggleClass("show");
      $(this).toggleClass("show-dropdown-menu");
      $('.dt-button-collection').hide();
      e.stopPropagation();
    });
	}
	columnDropDown(tbl, cls=`dropdown-item-column`) {
		$(`a.${cls}`).on("click", function (e) {
      e.preventDefault();
      $(".dropdown-menu-column").addClass("show-bar");
      if ($(this).find("input").is(":checked")) {
        $(this).find("input").prop("checked", false);
      } else {
        $(this).find("input").prop("checked", true);
      }

      // Get the column API object
      const columnIndex = Number($(this).attr("data-column")) + 2;
      const column = tbl.column(columnIndex);

      // Toggle the visibility
      column.visible(!column.visible());
      e.stopPropagation();
    });
  }
  
  selectAllRows(tbl, id) {
    $(`#${id}`).on("click", function () {
      var rows = tbl.rows({ search: "applied" }).nodes();
      const checkValue = ($(this).is(":checked")) ? true : false
      $('input[type="checkbox"]', rows).prop("checked", checkValue);
    });
  }

	expandContainer(tbl) {
		$(".expand-container").on("click", function (e) {
      tbl.columns.adjust().draw();
    });
  }
  columnFixed() {
    var $table = $('.display');
    var $fixedColumn = $table.clone().insertAfter($table).addClass('fixed-column');
    $fixedColumn.find('th:not(:last-child),td:not(:last-child)').remove();
    function calc() {
    $fixedColumn.find('tr').each(function (i, elem) {
        $(this).height($table.find('tr:eq(' + i + ')').height());
    });
    }
    calc()
    $(document).on('mousemove', calc)
  }
}
