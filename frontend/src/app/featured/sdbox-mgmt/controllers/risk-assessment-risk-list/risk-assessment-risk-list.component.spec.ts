import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskAssessmentRiskListComponent } from './risk-assessment-risk-list.component';

describe('RiskAssessmentRiskListComponent', () => {
  let component: RiskAssessmentRiskListComponent;
  let fixture: ComponentFixture<RiskAssessmentRiskListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskAssessmentRiskListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskAssessmentRiskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
