import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskAssessmentOverviewComponent } from './risk-assessment-overview.component';

describe('RiskAssessmentOverviewComponent', () => {
  let component: RiskAssessmentOverviewComponent;
  let fixture: ComponentFixture<RiskAssessmentOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskAssessmentOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskAssessmentOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
