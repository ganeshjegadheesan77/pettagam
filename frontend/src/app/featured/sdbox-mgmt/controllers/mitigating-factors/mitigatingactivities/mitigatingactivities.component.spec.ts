import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MitigatingactivitiesComponent } from './mitigatingactivities.component';

describe('MitigatingactivitiesComponent', () => {
  let component: MitigatingactivitiesComponent;
  let fixture: ComponentFixture<MitigatingactivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MitigatingactivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MitigatingactivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
