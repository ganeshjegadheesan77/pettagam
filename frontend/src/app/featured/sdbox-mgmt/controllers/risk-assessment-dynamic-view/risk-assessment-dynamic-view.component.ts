import { DataService } from './../../../internal-audit/components/audit-universe/data.service';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ConstUrls } from 'src/app/config/const-urls';
import { HttpService } from '../../../../core/http-services/http.service';
import { Chart } from 'chart.js'
@Component({
  selector: 'app-risk-assessment-dynamic-view',
  templateUrl: './risk-assessment-dynamic-view.component.html',
  styleUrls: ['./risk-assessment-dynamic-view.component.less']
})
export class RiskAssessmentDynamicViewComponent implements OnInit ,OnChanges {
  
  riskCount:any;
  barChart: object;
  riskvsassess:any;
  dataSet:any=[];
  data:any=[];
  barChartCanvas: any;
  barChartCtx: any;
  yearData:any=[];
  dashboardCount;
  constructor(private httpService: HttpService,  private dataService: DataService) {}
  ngOnInit() {
    
    this.dataService.currentmessage.subscribe(data => {
      //console.log('data', data)
      this.dashboardCount = data;

    })
    
    // binding of bar chart to variable
  this.httpService.securePost(ConstUrls.getAllAssessData)
    .subscribe(res =>{
         this.riskvsassess = res;
         var obj={};
         
        var year= new Date().getFullYear();
  this.yearData= [ Number(year)-5,Number(year)-4,Number(year)-3,Number(year)-2,Number(year)-1,year]
         this.riskvsassess.forEach(element => {
          if(element.riskId && element.riskId.riskLastApprovedDate){
            var yearValue= Number(element.riskId.riskLastApprovedDate.split('/')[2]);
            if(this.yearData.includes(yearValue))
           {
             this.dataSet.push(yearValue)
           }
          }
        });
        const uni=this.dataSet.reduce((acum,cur)=>Object.assign(acum,{[cur]:(acum[cur]|0)+1}),{})
        this.yearData.forEach(element => {
          if(uni[element]){
            this.data.push(uni[element])
          }
          else this.data.push(0)
        });
        //this.data=Object.values(uni);
        this.data.push(0)
//console.log(this.data)
this.barChartCanvas = <HTMLCanvasElement>document.getElementById("bar");
this.barChartCtx = this.barChartCanvas.getContext("2d");
    this.barChart = new Chart(this.barChartCtx, {
    type: 'bar',
    data: {
    labels: this.yearData,
    datasets: [{
    label: 'Assessed',
    backgroundColor: '#DD6838',
    borderColor: '#DD6838',
    borderWidth: 0,
    data:[4,0,0,0,0,0],
    fill: false,
    }]
    },
    options: {
      tooltips: {
        callbacks: {
        label: function(t, d) {
          return d.datasets[t.datasetIndex].label +
          ': (Risk Count:' + t.yLabel + ')';
        }
        }
      },
    responsive: true,
    legend: {
   // position: 'bottom',
   label:{
     display: false
   },
    display:false
    },
    title: {
    display: false,
    //text: 'Chart.js Bar Chart'
    }
    }
    })
        })
   
    this.getRiskCount();
    }
    ngOnChanges(changes: SimpleChanges): void {

    }
  
    getRiskCount() {
      this.httpService.secureGet(ConstUrls.getRiskCount)
        .subscribe(res => {
          //console.log('get risk count:', res)
          this.riskCount = res;
        }
        )
    }
  
  }

