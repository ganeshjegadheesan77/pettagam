import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskAssessmentDynamicViewComponent } from './risk-assessment-dynamic-view.component';

describe('RiskAssessmentDynamicViewComponent', () => {
  let component: RiskAssessmentDynamicViewComponent;
  let fixture: ComponentFixture<RiskAssessmentDynamicViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskAssessmentDynamicViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskAssessmentDynamicViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
