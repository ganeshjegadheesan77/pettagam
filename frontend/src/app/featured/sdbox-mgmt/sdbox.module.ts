import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RiskAssessmentRoutingModule } from './risk-assessment-routing.module';
import { RiskAssessmentComponent } from './risk-assessment.component';
import { RiskAssessmentDetailsComponent } from './controllers/risk-assessment-details/risk-assessment-details.component';
import { RiskAssessmentFormComponent } from './controllers/risk-assessment-form/risk-assessment-form.component';
import { RiskAssessmentOverviewComponent } from './controllers/risk-assessment-overview/risk-assessment-overview.component';
import { RiskAssessmentRiskListComponent } from './controllers/risk-assessment-risk-list/risk-assessment-risk-list.component';
import { RiskAssessmentDynamicViewComponent } from './controllers/risk-assessment-dynamic-view/risk-assessment-dynamic-view.component';
import { AssessmentCasesComponent } from './controllers/assessment-cases/assessment-cases.component';
import { MitigatingFactorsComponent } from './controllers/mitigating-factors/mitigating-factors.component';
import { HeatmapComponent } from './controllers/heatmap/heatmap.component';

import { ControlComponent } from './controllers/mitigating-factors/control/control.component';
import { MitigatingactivitiesComponent } from './controllers/mitigating-factors/mitigatingactivities/mitigatingactivities.component';
import { MitigatingProcessComponent } from './controllers/mitigating-factors/mitigating-process/mitigating-process.component';
import { TopTenRisksComponent } from './controllers/top-ten-risks/top-ten-risks.component';
import { AddNewAssessmentComponent } from './controllers/add-new-assessment/add-new-assessment.component';
import { RiskEditFormComponent } from './controllers/risk-edit-form/risk-edit-form.component';
import { AngularDraggableModule } from "angular2-draggable";

@NgModule({
  declarations: [RiskAssessmentComponent,
  RiskAssessmentDetailsComponent,

  RiskAssessmentFormComponent,
  RiskAssessmentOverviewComponent,
  RiskAssessmentRiskListComponent,
  RiskAssessmentDynamicViewComponent,
  AssessmentCasesComponent,
  MitigatingFactorsComponent,
  HeatmapComponent,
  ControlComponent,
  MitigatingactivitiesComponent,
  MitigatingProcessComponent,
  TopTenRisksComponent,
  AddNewAssessmentComponent,
  RiskEditFormComponent],
  imports: [
    CommonModule,
    RiskAssessmentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    BsDatepickerModule.forRoot(),
    AngularDraggableModule
  ],
  entryComponents:[
    RiskAssessmentFormComponent,
  ]
})
export class RiskAssessmentModule { }
