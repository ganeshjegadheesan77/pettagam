import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { ModalModule } from "ngx-bootstrap/modal";
import { AppRoutingModule } from "./app-routing.module";
import { TabsModule } from "ngx-bootstrap";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { FilePickerModule } from "ngx-awesome-uploader";
import { ClickOutsideModule } from "ng-click-outside";
import{NgxTimeSchedulerModule} from 'ngx-time-scheduler';
import { AppComponent } from "./app.component";
import { LoginComponent } from "./featured/authentication/components/login/login.component";
import { MalihuScrollbarModule } from "ngx-malihu-scrollbar";
import { HeaderComponent } from "./layout/header/header.component";
import { SidebarComponent } from "./layout/sidebar/sidebar.component";
import { AuthLayoutComponent } from "./layout/auth-layout/auth-layout.component";
import { DashboardLayoutComponent } from "./layout/dashboard-layout/dashboard-layout.component";
import { AttachmentComponent } from "./featured/risk-and-control-management/components/attachment/attachment.component";

import { ProjectElementsComponent } from "./layout/project-elements/project-elements.component";
import { ChatElementsComponent } from "./layout/chat-elements/chat-elements.component";
import { CollaborationElementsComponent } from "./layout/collaboration-elements/collaboration-elements.component";
import { WhiteboardComponent } from "./layout/collaboration-elements/components/whiteboard/whiteboard.component";
import { WhiteboardNewComponent } from "./layout/collaboration-elements/components/whiteboard-new/whiteboard-new.component";

import { ChatSystemComponent } from "./layout/collaboration-elements/components/chat-system/chat-system.component";
import { ChatroomComponent } from "./layout/collaboration-elements/components/chat-system/chatroom/chatroom.component";
import { RoomsComponent } from "./layout/collaboration-elements/components/chat-system/rooms/rooms.component";
import { InTrayComponent } from "./layout/collaboration-elements/components/in-tray/in-tray.component";
import { FileRoomComponent } from "./layout/collaboration-elements/components/in-tray/file-room/file-room.component";
import { FileUploadComponent } from "./layout/collaboration-elements/components/in-tray/file-upload/file-upload.component";
import { TrayComponent } from "./layout/collaboration-elements/components/in-tray/tray/tray.component";
import { FilePreviewComponent } from "./layout/collaboration-elements/components/in-tray/file-preview/file-preview.component";
import { PdfAnnotationComponent } from "./layout/collaboration-elements/components/in-tray/pdf-annotation/pdf-annotation.component";

import { LoaderComponent } from "./layout/loader/loader.component";
import { LoaderspinnerService } from "./layout/loader/loaderspinner.service";
import { LoaderInterceptorService } from "./loader-interceptor.service";
import { AuthenticationRoutingModule } from "./featured/authentication/authentication-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { DefaultModalComponent } from "./layout/modal/default-modal/default-modal.component";
import { ConfirmPopupComponent } from "./layout/modal/confirm-popup/confirm-popup.component";
import { ChatPopupComponent } from "./layout/collaboration-elements/components/chat-system/chat-popup/chat-popup.component";
import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { ChatPopupSecComponent } from "./layout/collaboration-elements/components/chat-popup-sec/chat-popup-sec.component";
import { ActionInTrayComponent } from "./layout/collaboration-elements/components/chat-system/action-in-tray/action-in-tray.component";
import { AngularDraggableModule } from "angular2-draggable";
import { VideoCallComponent } from "./layout/collaboration-elements/components/video-call/video-call.component";
import { InterviewSchedulePopupComponent } from './layout/modal/interview-schedule-popup/interview-schedule-popup.component';
import { AuditChatPopupSecComponent } from './layout/collaboration-elements/components/audit-chat-popup-sec/audit-chat-popup-sec.component';
import { IntrayDocumentsWindowComponent } from './layout/collaboration-elements/components/intray-documents-window/intray-documents-window.component';
import { SchedulerModalComponent } from './layout/modal/scheduler-modal/scheduler-modal.component';
import { RedeemPopupComponent } from './layout/modal/redeem-popup/redeem-popup.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AuditeeChatRoomComponent } from './featured/authentication/components/auditee-chat-room/auditee-chat-room.component';
import { SaveDocumentPopupComponent } from './layout/modal/save-document-popup/save-document-popup.component';
import { ConfigService } from '../config.service';
import { AuditeePbcFormComponent } from './featured/authentication/components/auditee-pbc-form/auditee-pbc-form.component';
import { NgxFileDropModule } from "ngx-file-drop";
import { ConfirmRejectWorkstepComponent } from './layout/modal/confirm-reject-workstep/confirm-reject-workstep.component';
export const configFactory = (configService: ConfigService) => {
  return () => configService.loadConfig();
};
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    AuthLayoutComponent,
    DashboardLayoutComponent,
    RoomsComponent,
    AttachmentComponent,
    LoginComponent,
    ProjectElementsComponent,
    ChatElementsComponent,
    LoaderComponent,
    CollaborationElementsComponent,
    WhiteboardComponent,
    WhiteboardNewComponent,
    ChatSystemComponent,
    ChatroomComponent,
    InTrayComponent,
    FileRoomComponent,
    FileUploadComponent,
    TrayComponent,
    FilePreviewComponent,
    PdfAnnotationComponent,
    DefaultModalComponent,
    ConfirmPopupComponent,
    ChatPopupComponent,
    ChatPopupSecComponent,
    ActionInTrayComponent,
    VideoCallComponent,
    InterviewSchedulePopupComponent,
    AuditChatPopupSecComponent,
    IntrayDocumentsWindowComponent,
    SchedulerModalComponent,
    RedeemPopupComponent,
    AuditeeChatRoomComponent,
    SaveDocumentPopupComponent,
    AuditeePbcFormComponent,
    ConfirmRejectWorkstepComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    AuthenticationRoutingModule,
    BrowserModule,
    ClickOutsideModule,
    AutocompleteLibModule,
    MalihuScrollbarModule.forRoot(),
    AppRoutingModule,
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    HttpClientModule,
    FilePickerModule,
    ModalModule.forRoot(),
    AngularDraggableModule,
    NgxTimeSchedulerModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxFileDropModule
    
  ],
  entryComponents: [
    WhiteboardComponent,
    ChatroomComponent,
    RoomsComponent,
    FilePreviewComponent,
    PdfAnnotationComponent,
    FileRoomComponent,
    FileUploadComponent,
    DefaultModalComponent,
    ConfirmPopupComponent,
    InterviewSchedulePopupComponent,
    SchedulerModalComponent,
    RedeemPopupComponent,
    SaveDocumentPopupComponent,
    ConfirmRejectWorkstepComponent
 
  ],
  providers: [
    LoaderspinnerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: configFactory,
      deps: [ConfigService],
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}
