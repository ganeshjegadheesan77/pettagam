global.__root = __dirname;
var handlebars = require('handlebars');
var fs = require("fs");
var request = require('request')
var cron = require('node-cron')
var nodemailer = require('nodemailer');
var logger = require("./utils/logger").logger;


//00 00 14 * * * cron.schedule("00 00 14 * * *",
module.exports = cron.schedule("00 00 14 * * *", function () {  //00(sec) 56(minutes) 17(hours) * * * //00 01 13- 1:01 pm
  var webUrl = "115.112.185.58:3200";
  var mailSubject = "Message from";
  let javaPort = "8240"
  if (process.env.HOST == "localhost") {
    webUrl = "localhost:4200";
    mailSubject = "(LOCAL) Message from";
  }
  else if (process.env.HOST == "115.112.185.58") {
    if (process.env.PORT == "3000") {
      javaPort = "8240"
      webUrl = "115.112.185.58:4200";
      mailSubject = "(DEV) Message from";
    }
    else if (process.env.PORT == "3100") {
      javaPort = "8160"
      webUrl = "115.112.185.58:3200";
      mailSubject = "Message from";
    }
  } else if (process.env.HOST == "115.112.185.79") {
    if (process.env.PORT == "3000") {
      javaPort = "8240"
      webUrl = "115.112.185.79:4200";
      mailSubject = "(DEV) Message from";
    }
    else if (process.env.PORT == "3100") {
      javaPort = "8160"
      webUrl = "115.112.185.79:3200";
      mailSubject = "Message from";
    }
    else if (process.env.PORT == "6100") {
      javaPort = "8160"
      webUrl = "115.112.185.79:6200";
      mailSubject = "Message from";
    }
  }

  const data = {
    database: process.env.DB,
    days: "5"
  }
  logger.pushInfo('Email Cron Starts on :' + new Date());
  request.post(`http://${process.env.HOST}:${javaPort}/OrionAPIServices/WorkflowManagementService/getReminders`, { json: data }, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var responses = body.data;
      if (responses.length > 0) {
        try {
          console.error("Cron job data found: " + responses.length);
          responses.forEach(y => {
            y.actionTableList = []; y.resourcePlanninglist = []; y.controlList = []
            y.workflows_assigned.forEach(x => {
              if (x.tab == "action" && x['action-objects'] && x['action-objects'].length > 0) {
                y.subject = 'EnIA'
                y.headers = ["Action Id", "Action", "Execution Date", "Area", "Status", "Rating", "Workflow ID"],
                  x['action-objects'].forEach(element => {
                    element.ActionId = x.ActionId; element.type = x.workType; element.notice = x.notice;
                    element.routerLink = `http://${webUrl}` + x.routerLink;
                    element.color = (element.notice == 'Reminder' || element.notice == 'Information') ? '#98aa3a' : (element.notice == 'Overdue') ? '#fc0' : (element.notice == 'Escalation') ? '#dd6838' : '#98aa3a'

                    element.StartDate = x.StartDate;
                    y.actionTableList.push(element)
                  })
              }
              else {
                if (x.tab == "resource planning" && x['audit-objects'] && x['audit-objects'].length > 0) {
                  if (y.subject == 'EnRiCo') y.subject = y.subject + ' & EnIA'
                  else y.subject = 'EnIA'
                  y.headersAudit = ["Audit Id", "Audit", "Created On", "Execution Date", "Workflow ID"],
                    x['audit-objects'].forEach(element => {
                      element.routerLink = `http://${webUrl}` + x.routerLink;
                      element.StartDate = x.StartDate; element.type = x.workType;
                      element.notice = x.notice;
                      element.color = (element.notice == 'Reminder' || element.notice == 'Information') ? 'green' : (element.notice == 'Overdue') ? 'yellow' : (element.notice == 'Escalation') ? 'red' : 'green'

                      y.resourcePlanninglist.push(element)
                    })
                }
                else {
                  if (x.tab == "control" && x['control-objects'] && x['control-objects'].length > 0) {
                    if (y.subject) y.subject = y.subject + ' & EnRiCo'
                    else y.subject = 'EnRiCo'
                    y.headersControl = ["Control WF Id", "Created On", "Execution Date", "Workflow ID"],
                      x['control-objects'].forEach(element => {
                        element.routerLink = `http://${webUrl}` + x.routerLink;
                        element.StartDate = x.StartDate; element.type = x.workType
                        element.notice = x.notice;
                        element.color = (element.notice == 'Reminder' || element.notice == 'Information') ? 'green' : (element.notice == 'Overdue') ? 'yellow' : (element.notice == 'Escalation') ? 'red' : 'green'

                        y.controlList.push(element)
                      })
                  }
                }
              }
            })
          })

          var readHTMLFile = function (path, callback) {
            fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
              if (err) {
                logger.pushError("email.js -> Error occurred while reading file " + path + " Message: " + JSON.stringify(err));
                throw err;
                // callback(err);
              }
              else {
                callback(null, html);
              }
            });
          };
          responses.forEach(element => {
            if (element.actionTableList.length > 0 || element.resourcePlanninglist.length > 0 || element.controlList.length > 0) {
              readHTMLFile(__dirname + '/helpers/emailTemplate.html', function (err, html) {
                var template = handlebars.compile(html);
                var cur = new Date().toDateString();
                var time = new Date().toLocaleTimeString();
                element.currentdate = cur;
                element.currentTime = time;
                element.username = element.username[0].toUpperCase() + element.username.slice(1);
                element.url = `http://${webUrl}`
                var replacements = element;
                var htmlToSend = template(replacements);
                var transporter = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                    user: 'enrico.mailer',
                    pass: 'Orion@123'
                  }
                });
                const mailOptions = {
                  from: 'enrico.mailer@gmail.com', // sender address
                  to: 'yash.soni@3i-infotech.com',//element.email, //receivers
                  subject: `${mailSubject} ${element.subject}`, // Subject line
                  attachments: [
                    {
                      filename: 'logo-enrico.png',
                      path: __dirname + '/helpers/logo-enrico.png',
                      cid: 'logo',
                    },
                  ],
                  html: htmlToSend
                };
                transporter.sendMail(mailOptions, function (err, info) {
                  if (err)
                    console.error(err)
                  else {
                    console.error("Mail sent to :" + info);
                    console.log(info);
                    logger.pushInfo(info);
                  }
                });
              });
            }
            else {
              console.error('No records to send email :) Cross check using postman !');
            }

          })
        } catch (error) {
          console.error("Error in cron job: ", error);
        }
      }
      else {
        console.error("Email Cron Ends: No Data Found in API")
      }
    }
  })
  // }
});