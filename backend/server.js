const express = require("express");
require("dotenv").config();
global.__root = __dirname;
// const Agenda = require('agenda');
const config = require("./config/config");
const { prefixMiddleware } = require("./middleware");
const connectToDb = require("./config/dbConnect");
const initRouters = require("./routes");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
var multer = require("multer");
var moment = require("moment");
const path = require("path");
const XLSX = require("xlsx");
var cron = require("node-cron");
var request = require("request");
var logger = require("./utils/logger").logger;
const { v4: uuidv4 } = require("uuid");

var nodemailer = require("nodemailer");

var fs = require("fs");
var { xlsxData } = require("./models/xlsxData");
var {
  internalAuditcontroller,
} = require("./controllers/internalAudit/internalAudit.controller");
var rooms = require("./controllers/chatSystem/room.controller");
var users = require("./controllers/chatSystem/users.controller");
var messages = require("./controllers/chatSystem/message.controller");
var auditChatMessages = require("./controllers/chatSystem/collabChatMessages.controller");

var auditChat = require("./controllers/chatSystem/collabChatSystem.controller");
var systemUsers = require("./controllers/users/user.controller");
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,Range"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST, GET, PATCH, DELETE, OPTIONS"
  );
  res.setHeader(
    "Access-Control-Expose-Headers",
    "Accept-Ranges,Content-Encoding,Content-Length,Content-Range"
  );
  next();
});

//create unique id for every request (audit trial)
app.use(function (req, res, next) {
  var unique_request_id = uuidv4();
  req.unique_request_id = unique_request_id;
  // console.log("middleware hit: " + unique_request_id);
  next();
});

//authenticate all urls except login
// app.use(require("./middleware/index").identification("header"));
app.use("/api/uploads", express.static(path.join(__dirname, "uploads")));
app.use(
  "/api/caseMgmt",
  express.static(path.join(__dirname, "uploads/caseMgmt"))
);
app.use(
  "/api/uploadControlTest",
  express.static(path.join(__dirname, "uploads/controlTest"))
);
app.use(
  "/api/uploadInTray",
  express.static(path.join(__dirname, "uploads/inTray"))
);
app.use("/api/reports", express.static(path.join(__dirname, "reports")));
app.use(
  "/api/uploadFieldWork",
  express.static(path.join(__dirname, "uploads/fieldWork"))
);

// const app = express();
/*****************************agenda*Single.js************************************************************** */
const Agenda = require("agenda");
const Agendash = require("agendash");
const dmsController = require("./controllers/dms/dms.controller");
// exports.run = async () => {
(async function run() {
  let configureMongoDBObj = {
    db: {
      address: config.database.url,
      collection: "jobs",
      options: {
        useNewUrlParser: true,
      },
    },
  };
  let agenda = new Agenda(configureMongoDBObj);
  agenda.define("", (job, done) => {
    // console.log("sukanya");
    done();
  });
  agenda.define("Notify Locked Users", function () {
    console.log("Notify locked users");
  });

  agenda.define(
    "send email report",
    { priority: "high", concurrency: 10 },
    (job, done) => {
      const { to } = job.attrs.data;
      emailClient.send(
        {
          to,
          from: "",
          subject: "Email Report",
          body: "...",
        },
        done
      );
      console.log("email send");
    }
  );

  //agendash UI config
  app.use("/dash", Agendash(agenda));
  await agenda.start();

  agenda.now("RCM PROJECT");
  //schedule once
  await agenda.schedule("1 minute", "");
  //repeat
  await agenda.every("1 minute", "");
  await agenda.schedule("1 minutes", "send email report", {
    to: "",
  });
})();

// app.listen(2810, ()=> console.log('listening on the port 2810'));

/***************************************************************************************************************** */
/**
 * Socket server imports and port
 */

var http = require("http").Server(app);
var io = require("socket.io")(http, { origins: "*:*" });

// prefixMiddleware(app);

//email cron job runs from here
require("./email");

connectToDb(config.database.url)
  .then(() => {
    initRouters(app);

    // start server
    app.listen(process.env.PORT, () => {
      logger.pushConsoleInfo(
        `app started at http://${process.env.HOST}:${process.env.PORT}/`
      );
      // check();
    });
  })
  .catch((err) => {
    console.error(err);
    logger.pushError("Error Occurred during startup : ", JSON.stringify(err));
  });

//******************************* SOCKET CONNECTION  ***********************************/
logger.pushInfo("---------------socket----------------");
http.listen(process.env.SOCKETPORT, function () {
  logger.pushConsoleInfo(`Listening on ${process.env.SOCKETPORT}`);
});
io.on("connection", (socket) => {
  logger.pushInfo("[Connection Event] User connected");
  socket.on("join", function (data) {
    let curr_time = moment().format("LT");
    io.emit("new user joined", {
      user: data.user,
      message: "has joined this room.",
      ctime: curr_time,
    });
  });

  socket.on("getCollabChatRoom", (data) => {
    logger.pushInfo("getCollabChatRoom event (socket join): " + JSON.stringify(data));
    if (!socket.joinedRoom || (socket.roomId && socket.roomId != data)) {
      if (socket.roomId) {
        socket.leave(socket.roomId);
      }
      socket.join(data);
      socket.roomId = data;
      socket.joinedRoom = true;
    }
    auditChat.findRoomById(data).then((res) => {
      io.to(data).emit("getCollabChatRoom", res);
    });
  });

  socket.on("userIsOnline", (data) => {
    logger.pushInfo("userOnline fired");
    data.status = true;
    auditChat.toggleOnlineUser(data).then((user) => {
      io.emit("userOnline", user);
    });
  });

  socket.on("setUserOnline", (data) => {
    auditChat.setUserOnline(data).then((room) => {
      io.to(data).emit("getCollabChatRoom", room);
    });
  });

  socket.on("activity logged", function (data) {
    //activity logged
    socket.join("risk");
    let curr_time = moment().format("LT");
    io.emit("new activity done", {
      user: data.owner,
      message: data.operation,
      ctime: curr_time,
    });
  });
  socket.on("message", function (data) {
    let curr_time = moment().format("LT");
    io.emit("new message", {
      user: data.user,
      message: data.message,
      ctime: curr_time,
    });
  });
  socket.on("drawing", function (data) {
    socket.broadcast.emit("drawing", data);
  });

  socket.on("rectangle", function (data) {
    socket.broadcast.emit("rectangle", data);
  });

  socket.on("linedraw", function (data) {
    socket.broadcast.emit("linedraw", data);
  });

  socket.on("circledraw", function (data) {
    socket.broadcast.emit("circledraw", data);
  });

  socket.on("ellipsedraw", function (data) {
    socket.broadcast.emit("ellipsedraw", data);
  });

  socket.on("textdraw", function (data) {
    socket.broadcast.emit("textdraw", data);
  });

  socket.on("copyCanvas", function (data) {
    socket.broadcast.emit("copyCanvas", data);
  });

  socket.on("Clearboard", function (data) {
    socket.broadcast.emit("Clearboard", data);
  });

  /*********************************chat Socket******************************** */

  socket.on("getOnlineUsers", (data) => {
    users.findByStatus(data).then((res) => {
      io.emit("onlineUsers", res);
    });
  });

  socket.on("logInUser", (data) => {
    users.logInUser(data, io);
  });

  socket.on("logOutUser", (data) => {
    users.logOutUser(data, io);
  });
  socket.on("getRoomDetailsById", (data) => {
    rooms.getRoomDetailsById(data, io).then((res) => {
      io.emit("getFileRoomDetails", res);
    });
  });
  socket.on("getRoomsList", (data) => {
    rooms.findUserRooms(data).then((res) => {
      io.emit("getRoomsList", res);
    });
  });
  socket.on("getRoomList", (data) => {
    Room.findRoom(data).then((res) => {
      io.emit("getRoomList", res);
    });
  });
  //new File Add

  socket.on("addFile", (data) => {
    // console.log("addFile :", data);
    rooms.updateFileroom(data, io);
  });
  socket.on("updateFile", (data) => {
    rooms.updateFileOpen(data, io);
  });

  socket.on("createRoom", (data) => {
    rooms.create(data, io);
  });

  socket.on("createFileRoom", (data) => {
    Room.createFileRoom(data, io);
  });
  socket.on("deleteRoom", (data) => {
    rooms.delete(data, io);
  });
  //delete File room
  socket.on("deleteFileroom", (data) => {
    //console.log("deleteFileroom", data);
    rooms.deleteFile(data, io);
  });
  socket.on("deleteUser", (data) => {
    rooms.deleteUser(data, io).then((res) => {
      rooms.findRoomById(data.id).then((res) => {
        users.findByStatus(res[0].usernames).then((res) => {
          io.emit("onlineUsers", res);
        });
      });
    });
  });
  //----------------------------New Audit Chat ------------------------------------
  socket.on("getRoomDetails", (data) => {
    rooms.findRoomById(data).then((res) => {
      // console.log("getRoomDetails data -> " + JSON.stringify(data));
      // console.log("getRoomDetails res -> " + JSON.stringify(res));
      if (res.auditId) {
        io.to(res.auditId).emit("getRoomDetails", res);
      } else {
        socket.emit("getRoomDetails", res);
      }
    });
  });
  socket.on("getAuditChatroomMessage", (data) => {
    auditChatMessages.getMessagesByRoomId(data).then((res) => {
      console.log("data:", JSON.stringify(data));
      io.to(data).emit("getAuditChatroomMessage", res);
    });
  });

  socket.on("putAuditChatroomMessage", (data) => {
    auditChatMessages.putMessagesByRoomId(data).then((res) => {
      console.log("putAuditChatroomMessage() data:", JSON.stringify(data));
      io.to(data).emit("putAuditChatroomMessage", res);
    });
  });

  socket.on("setUserJoinedMessage", (data) => {
    auditChatMessages.setUserJoinedMessage(data).then((res) => {
      io.to(data).emit("getAllAuditChatroomMessages", res);
    });
  });

  socket.on("getAllAuditChatroomMessages", (data) => {
    console.log("getAllAuditChatroomMessages -> " + JSON.stringify(data));
    // console.log(data);
    auditChatMessages
      .getAllAuditChatroomMessages(data)
      .then((res) => {
        io.to(data).emit("getAllAuditChatroomMessages", res);
      })
      .catch((err) => console.log(err));
  });

  socket.on("userList", (data) => {
    users.findAllUsers().then((data) => {
      io.emit("userList", data);
    });
  });
  socket.on("fileUserList", (data) => {
    users.findAllUsers().then((data) => {
      io.emit("fileUserList", data);
    });
  });
  socket.on("addUserToRoom", (data) => {
    rooms.update(data, io);
    users.findAllUsers().then((data) => {
      io.emit("userList", data);
    });
  });
  socket.on("addUsersToFileroom", (data) => {
    Room.addUsersToFileroom(data, io);
    // users.findAllUsers().then(data => {
    //   io.emit("userList", data);
    // });
  });
  socket.on("messageList", (data) => {
    console.log("messageList :" + JSON.stringify(data));
    messages.findMessagesById(data).then((res) => {
      // console.log("data:", data);
      io.emit("messageList", res);
    });
  });

  socket.on("newMessage", (data) => {
    messages.create(data, io);
  });

  //getSystemUsers
  socket.on("getSystemUsers", (data) => {
    users.findAllUsers().then((data) => {
      io.emit("getSystemUsers", data);
    });
  });
  //fetch intray files
  socket.on("getIntrayFiles", (obj) => {
    console.log("getIntrayFiles: " + JSON.stringify(obj));
    auditChat.getIntrayFiles(obj).then((data) => {
      socket.emit("gotIntrayFiles", data);
    });
  });
});

//Field Work
const storageFieldWork = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads/fieldWork");
  },
  filename: (req, file, callBack) => {
    callBack(null, `${file.originalname}`);
  },
});

const uploadFieldWork = multer({ storage: storageFieldWork });

//ControlTest
const storageControl = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads/controlTest");
  },
  filename: (req, file, callBack) => {
    callBack(null, `${file.originalname}`);
  },
});

const uploadControl = multer({ storage: storageControl });
//Case Management
const storageCase = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads/caseMgmt");
  },
  filename: (req, file, callBack) => {
    callBack(null, `${file.originalname}`);
  },
});

const uploadCase = multer({ storage: storageCase });

//In-tray
const storageTray = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads/inTray");
  },
  filename: (req, file, callBack) => {
    callBack(null, `${file.originalname}`);
  },
});

const uploadTray = multer({ storage: storageTray });

//Rest:
//Case Management
const storage = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads");
  },
  filename: (req, file, callBack) => {
    callBack(null, `${file.originalname}`);
  },
});

const upload = multer({ storage: storage });

// let upload = multer({ dest : 'uploads/' });

app.post(
  "/api/singleuploadfile",
  uploadTray.single("file"),
  (req, res, next) => {
    const file = req.file;
    // console.log(file.filename);
    if (!file) {
      const error = new Error("No File");
      error.httpStatusCode = 400;
      return next(error);
    }
    res.send({ sttus: "ok" });
  }
);

app.post(
  "/api/multipleuploadfileCT",
  uploadControl.array("file"),
  (req, res, next) => {
    const files = req.files;
    // console.log(files["filename"]);
    if (!files) {
      const error = new Error("No File");
      error.httpStatusCode = 400;
      return next(error);
    }
    res.send({ id: "100022" });
  }
);

app.post(
  "/api/uploadFieldWork",
  uploadFieldWork.array("file"),
  (req, res, next) => {
    const files = req.files;
    console.log("File ARR: ", files["filename"]);
    if (!files) {
      const error = new Error("No File");
      error.httpStatusCode = 400;
      return next(error);
    }
    res.send({ id: "100022" });
  }
);

app.post(
  "/api/multipleuploadfilecase",
  uploadCase.array("file"),
  (req, res, next) => {
    const files = req.files;
    // console.log(files["filename"]);
    if (!files) {
      const error = new Error("No File");
      error.httpStatusCode = 400;
      return next(error);
    }
    res.send({ id: "100022" });
  }
);
const DIR = "uploads";
// app.get("/api/removeImage/:imagename", function(req, res) {
//   // console.log(req.params.imgname)
//   if (!req.params.imagename) {
//     console.log("No file received");
//     message = "Error! in image delete.";
//     return res.status(500).json("error in delete");
//   } else {
//     console.log("file received");
//     console.log(req.params.imagename);
//     try {
//       // console.log(DIR+'/'+req.params.imagename);
//       fs.unlinkSync(DIR + "/" + req.params.imagename);
//       // console.log('successfully deleted /tmp/hello');
//       return res.status(200).send({ messege: "deleted" });
//     } catch (err) {
//       // handle the error
//       return res.status(400).send(err);
//     }
//   }
// });
app.post("/api/removeAttachment", function (req, res) {
  console.log(req.body.fullName);
  if (!req.body.fullName) {
    console.log("No file received");
    message = "Error! in image delete.";
    return res.status(500).json("error in delete");
  } else {
    // console.log("file received");
    // console.log(req.params.imagename);
    try {
      console.log(req.body.directory + "/" + req.body.fullName);
      fs.unlinkSync(req.body.directory + "/" + req.body.fullName);
      // console.log('successfully deleted /tmp/hello');
      return res.status(200).send({ messege: "deleted" });
    } catch (err) {
      // handle the error
      return res.status(400).send(err);
    }
  }
});

app.post("/api/importcsv", upload.array("file"), (req, res, next) => {
  const filess = req.files;
  if (!filess) {
    const error = new Error("No File");
    error.httpStatusCode = 400;
    return next(error);
  }
  res.send({
    filename: filess[0].filename,
    upload_path: filess[0].path,
    file_size: filess[0].size,
  });
});

//nodemailer
app.post("/api/sendmail", (req, res) => {
  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "enrico.mailer",
      pass: "Orion@123",
    },
  });
  const mailOptions = {
    from: "enrico.mailer@gmail.com", // sender address
    to: req.body.email, //req.body.email, // list of receivers
    subject: "Reminder from Enrico", // Subject line
    html: `Hello ${req.body.username},<br/> <br/> The following task require your attention:<br/>${req.body.content}.<br/>
    <br/> Thank you for attending to them at your earliest convenience.<br/><br/><br/> 
    Regards,<br/> Enrico Team`, // plain text body
  };
  transporter.sendMail(mailOptions, function (err, info) {
    if (err) console.log(err);
    else console.log(info);
  });
});

cron.schedule("0 0 1 Jan 1", () => {
  request.get(
    "http://localhost:3000/api/ia/generateCurrentYearAudits",
    (error, response) => {
      if (error) {
        console.log("error");
      }
      if (response) console.log("Responce BY API:", response.body);
    }
  );
  console.log("running a task every two minutes-----WORD--");
});

// middleware for logs logs
app.use(function (err, req, res, next) {
  logger.pushError(err);
  next();
});

// Uncaught exception causing Process to exit
// process.on('uncaughtException', (error)  => {
//   console.log('Oh my god, something terrible happened: ',  error);
//   process.exit(1); // exit application
// });

// Unhandled rejection but that will not cause process exit
// process.on('unhandledRejection', (error, promise) => {
//   console.log(' Oops! We forgot to handle a promise rejection here: ', promise);
//   console.log(' The error was: ', error );
// });

// Uncaught exception causing Process to exit
process.on('uncaughtException', (error) => {
  console.log('Oh my god, something terrible happened: ', error);
  logger.pushError('Oh my god, something terrible happened: ' + JSON.stringify(error));
  process.exit(1); // exit application
});
// Unhandled rejection but that will not cause process exit
process.on('unhandledRejection', (error, promise) => {
  console.log(' Oops! We forgot to handle a promise rejection here: ', promise);
  logger.pushError(' Oops! We forgot to handle a promise rejection here: ' + JSON.stringify(promise));
  console.log(' The error was: ', error);
  logger.pushError(' The error was: ' + JSON.stringify(error));
});

const globby = require("globby");
// cron.schedule('1 * * * * *', async () => {
//   // async function check() {

//   var pathUrl = "D:/3i/enrico/video_recordings";
//   if (process.platform === 'win32') {
//     pathUrl = 'E:/Desktop/temp';
//   }
//   else {
//     pathUrl = '/home/clouduser/openvidu_recordings'
//   }
//   // dmsController.uploadVideoUrl({ body: { videoUrl: 'https://115.112.185.58:4443/recordings/EnRiCo1-3/EnRiCo1-3.mp4', dmsSessionId: connectionResponse.dmsSession, username: 'cronjob', auditId: 'OP-013' } });

//   globby([`${pathUrl}/**/*.mp4`], {
//     ignore: [`${pathUrl}/**/*.copied.mp4`]
//   }).then(async paths => {
//     console.log(paths);
//     if (paths.length > 0) {
//       let data = { sessionId: "testingsession", userName: 'cronjob', repositoryName: config.dms.dmsRepositoryName }
//       let connectionResponse = await connectDms(data);
//       console.log(connectionResponse.dmsSession);

//       for (const p of paths) {
//         var fArr = p.split("/");
//         var fileName = fArr.pop();
//         var filePath = fArr.join("/");
//         var auditId = '';
//         if (fileName.includes('OP-')) {
//           var aRR = path.parse(fileName).name.split("-");
//           var indexofOP = aRR.indexOf('OP');

//           auditId = aRR[indexofOP] + "-" + aRR[indexofOP + 1];
//         }
//         var res = await dmsController.uploadVideoUrl({ body: { videoUrl: p, dmsSessionId: connectionResponse.dmsSession, username: 'cronjob', auditId: auditId } });
//         // var dmsResult = await uploadVideoToDMS(filePath, connectionResponse.dmsSession, fileName);
//         // await addVideoTodocumentTable('cronjob', auditId, dmsResult);

//         if (res === true) {
//           fs.rename(filePath + "/" + fileName, filePath + "/" + path.parse(fileName).name + ".copied" + path.parse(fileName).ext, function (err) {
//             if (err) {
//               console.log('Error', err)
//             }
//             else {
//               console.log('renamed');
//             }
//           });
//         }
//       }
//     }
//   });

//   // }
// });
