var logger = require('./logger').logger;

function manage(res, err, data, statusCode, message) {
    if (err) {
        if (statusCode) {
            logger.pushError(err);
            return res.status(statusCode).send({ message: message, data: null })
        }
        else {
            logger.pushError(err);
            return res.status(500).send({ message: "Internal Server Error", data: null })
        }
    }
    else {
        if(statusCode){
            return res.status(statusCode).send({ message: message, data: data })
        }
        else{
            return res.status(200).send({ message: message, data: data })
        }
        
    }
}

module.exports = {
    manage
}