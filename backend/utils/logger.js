var log4js = require('log4js');
var path = require('path');
log4js.configure({
  appenders: {
    'error': { type: 'file', filename: path.join(__dirname, "../../../backend.logs/enrico.error-" + process.env.PORT), layout: { type: 'pattern', pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] %c - %m' }, pattern: 'yyyy-MM-dd.log', alwaysIncludePattern: true },
    'warning': { type: 'file', filename: path.join(__dirname, "../../../backend.logs/enrico.error-" + process.env.PORT), layout: { type: 'pattern', pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] %c - %m' }, pattern: 'yyyy-MM-dd.log', alwaysIncludePattern: true },
    'info': { type: 'file', filename: path.join(__dirname, "../../../backend.logs/enrico.debug-" + process.env.PORT), layout: { type: 'pattern', pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] %c - %m' }, pattern: 'yyyy-MM-dd.log', alwaysIncludePattern: true },
    'debug': { type: 'file', filename: path.join(__dirname, "../../../backend.logs/enrico.debug-" + process.env.PORT), layout: { type: 'pattern', pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] %c - %m' }, pattern: 'yyyy-MM-dd.log', alwaysIncludePattern: true },
    'debuglow': { type: 'file', filename: path.join(__dirname, "../../../backend.logs/enrico.debug-" + process.env.PORT), layout: { type: 'pattern', pattern: '[%d{yyyy-MM-dd hh:mm:ss}] [%p] %c - %m' }, pattern: 'yyyy-MM-dd.log', alwaysIncludePattern: true }
  },

  categories: {
    default: { appenders: ['error', 'warning', 'info', 'debug', 'debuglow'], level: 'all' },
    error: { appenders: ['error', 'warning'], level: 'all' },
    warning: { appenders: ['warning'], level: 'all' },
    info: { appenders: ['info'], level: 'all' },
    debug: { appenders: ['debug'], level: 'all' },
    debuglow: { appenders: ['debuglow'], level: 'all' }
  }
});

var logger = log4js.getLogger('debug');
var elogger = log4js.getLogger('error');

const DEBUG_LEVEL_DEV = "DEV";
const DEBUG_LEVEL_PROD = "PROD";

var CURRENT_DEBUG_LEVEL = process.env.CURRENT_DEBUG_LEVEL;

if (!CURRENT_DEBUG_LEVEL) {
  CURRENT_DEBUG_LEVEL = DEBUG_LEVEL_DEV;
}

logger.pushDebug = (msg) => {
  if (CURRENT_DEBUG_LEVEL == DEBUG_LEVEL_DEV) {
    logger.debug(msg);
  }
}

logger.pushInfo = (msg) => {
  // console.log("Current Debug Level = [" + CURRENT_DEBUG_LEVEL + "] whereas DEBUG_LEVEL_DEV = [" + DEBUG_LEVEL_DEV + "]");
  // logger.info("Current Debug Level = " + CURRENT_DEBUG_LEVEL);
  if (CURRENT_DEBUG_LEVEL == DEBUG_LEVEL_DEV) {
    logger.info(msg);
  }
}

logger.pushConsoleInfo = (msg) => {
    console.log(msg);
    logger.info(msg);
}

logger.pushDebugLow = (msg) => {
  if (CURRENT_DEBUG_LEVEL == DEBUG_LEVEL_DEV) {
    logger.debuglow(msg);
  }
}

logger.pushWarning = (msg) => {
    elogger.warning(msg);
}

logger.pushWarning = (msg, obj)  => {
  elogger.warning(msg + " => " + JSON.stringify(obj));
}

logger.pushError = (msg)  => {
  elogger.error(msg);
}


//all errors log to file + console
/*
console.error = (d, e, f, g) => {
  var data = "";
  if (d) {
    data += " " + d;
  }
  if (e) {
    data += " " + e;
  }
  if (f) {
    data += " " + f;
  }
  if (g) {
    data += " " + g;
  }
  elogger.error(data);
  // process.stderr.write(data + "\n");
}
*/

//all logs, log to file + console
// console.log = (d, e, f, g) => {
//   var data = "";
//   if (d) {
//     data += " " + d;
//   }
//   if (e) {
//     data += " " + e;
//   }
//   if (f) {
//     data += " " + f;
//   }
//   if (g) {
//     data += " " + g;
//   }
//   dlogger.info(data);
//   process.stdout.write(data + "\n");
// }

module.exports = { logger };