const mongoose = require('mongoose');
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

const {database : { connectionOptions: options }} = require('../config/config');


let attempt = 1;
const connect = (...args) => {
    if (options.autoReconnect) {
      if (attempt >= options.reconnectTries) {
        return Promise.reject();
      } else {
        console.log(`Attempt № ${attempt}`);
        attempt++;
      }
      return new Promise(resolve => {
        setTimeout(() => {
          mongoose.connect(...args, {useNewUrlParser: true })
            .then(res => {
              resolve(res);
            })
            .catch(err => {
              console.log(err);
              connect(...args)
            });
        }, options.reconnectInterval);
      });
    } else {
        return mongoose.connect(...args);
    }
};

module.exports = connect;
