module.exports = {
    server: {
        schema: 'http',
        host: 'localhost',
        port: 3000
    },
    database: {
        host: 'localhost',
        port: 27017,
        db: `${process.env.DB}`,
        url: `mongodb://localhost:27017/${process.env.DB}`,
        connectionOptions: {
            autoReconnect: true,
            reconnectInterval: 5000,
            reconnectTries: 60
        },
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
    auditDatabase: {
        url: 'mongodb://localhost:27017/RcmAudits',
        connectionOptions: {
            autoReconnect: true,
            reconnectInterval: 5000,
            reconnectTries: 60
        },
        useNewUrlParser: true,
        useUnifiedTopology: true,
    },
    dms:{
        url:'http://115.112.185.58:3201/DMSIASERVICE/service/',
        
        connectionOptions : {
            "sessionId": "testingsession",
            "userName": "JDOL$",
            "repositoryName":"RCM"
         },
         dmsRepositoryName:'RCM',
         errorCodes:{
             '0':'success',
             '-7440':'Your DMS session has expired',
         }
    },
    key: {
        privateKey: 'n4k78c2nyjd3lds845hl4'
    }
};
