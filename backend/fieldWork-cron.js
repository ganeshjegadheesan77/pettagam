const express = require("express");
global.__root = __dirname;
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
// var handlebars = require('handlebars');
var fs = require("fs");
// var request= require('request')
var cron = require('node-cron')
var nodemailer = require('nodemailer');
app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,Range"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST, GET, PATCH, DELETE, OPTIONS"
  );
  res.setHeader(
    "Access-Control-Expose-Headers",
    "Accept-Ranges,Content-Encoding,Content-Length,Content-Range"
  );
  next();
});

cron.schedule('1,2,4,5 * * * *', () => {
    console.log('running every minute 1, 2, 4 and 5');
  });