const universeSchema = require('../../models/enia/universe.schema');
const valueSetMaster = require('../../models/common/valueSetMaster.schema');
const logger = require("../../utils/logger").logger;
const rsHelper = require("../../utils/responseHelper");

const valueMasterController = {};

valueMasterController.getAuditMasters = async (req, res) => {
    try {
        var universe = await universeSchema.find();
        rsHelper.manage(res, null, universe, 200, "Audit Masters found successfully");
    } catch (error) {
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}
/*
PARAMS :
 {
    "valueSetMaster": ["auditable_entity", "master_code_2", "master_code_3"]
}
OR
{
    "valueSetMaster": {
        "code": "auditable_entity"
    }
}
OR
{
    "valueSetMaster": {
        "code": "auditable_entity",
        "parentValueCode" : "test"
    }
}
*/
valueMasterController.getValueSetMaster = async (req, res) => {
    let parentValueCodeFalg = false;
    try {
        let params = req.body.valueSetMaster || undefined;
        if (!params) {
            rsHelper.manage(res, {}, null, 500, "Value set params is missing.");
            return;
        }
        let mongoQuery = createQuery(params);
        let valueSetMasterData = await valueSetMaster.find(mongoQuery).lean();
        if (parentValueCodeFalg) {
            valueSetMasterData = valueSetMasterData[0].valueSet.filter(obj => {
                return (obj.parentValueCode == params.parentValueCode)
            });
        }
        rsHelper.manage(res, null, valueSetMasterData, 200, "ok");
    } catch (error) {
        console.log(error);
        rsHelper.manage(res, error, null, 500, "Something went wrong while fetching masters.");
    }
}
//helper function for create mongo query
let createQuery = function (params) {
    let query = {};
    if (!(Array.isArray(params))) {
        query.code = params.code;
        if (params.parentValueCode) {
            parentValueCodeFalg = true;
        }
    } else {
        let tempArr = [];
        for (const i in params) {
            let tempObj = {};
            tempObj.code = params[i];
            tempArr.push(tempObj);
        }
        query['$or'] = tempArr;
    }
    return query;
}

module.exports = valueMasterController;