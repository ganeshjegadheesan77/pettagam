const auditDetail = require("../../models/Audit/audit.schema");
const CollabChatSystem = require("../../models/chatSystem/collabChatSystem");
const logger = require("../../utils/logger").logger;

const auditController = {};

auditController.getAllAuditReport = (req, res) => {
  try {
    auditDetail.audits.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
//get one action //
auditController.getAllAuditRecords = async (req, res) => {
  try {
    auditDetail.audits.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

auditController.getLocationAudit = async (req, res) => {
  try {
    auditDetail.audits.find({ location: req.body.location }, function (
      err,
      data
    ) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }

      res.send(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
//getOneRP
auditController.getOneRP = async (req, res) => {
  try {
    auditDetail.rps.findOne({ rpId: req.body.rpId }, async function (
      err,
      data
    ) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }

      res.send(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
auditController.getAllRPrecords = async (req, res) => {
  try {
    auditDetail.rps.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
auditController.saveRp = async (req, res) => {
  let rd = await new auditDetail.rps(req.body);
  await rd.save(async (err, auditDetail) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      // logger.pushConsoleInfo(auditDetail.auditTeam, auditDetail.auditId);
      // let hostsArr = await req.body.auditTeam.forEach((host) => {
      //   let obj = {
      //     username: host,
      //     status: true,
      //   };
      // });
      let hostsArr = [];
      await req.body.auditTeam.forEach((host) => {
        let obj = {
          username: host,
          status: false,
        };
        hostsArr.push(obj);
      });
      await CollabChatSystem.bulkWrite(
        hostsArr.map((data) => ({
          updateOne: {
            filter: {
              auditId: req.body.auditId,
              "hosts.username": { $ne: data.username },
            },
            update: { $push: { hosts: data } },
          },
        }))
      );
      // let hostsArr = [];
      // req.body.auditTeam.forEach(async (host) => {
      //   let obj = {
      //     username: host,
      //     status: false,
      //   };
      //   hostsArr.push(obj);
      // });
      // await CollabChatSystem.update(
      //   { auditId: req.body.auditId },
      //   { $addToSet: { hosts: { $each: hostsArr } } },
      //   (err, result) => {
      //     if (err) {
      //       logger.pushConsoleInfo(err);
      //       // res.status(404).end({ message: "Record not found", error: err });
      //     } else {
      //       logger.pushConsoleInfo(result);

      //       // res.send(doc);
      //     }
      //   }
      // );
      res.json(auditDetail);
    }
  });
};
auditController.updateRp = async (req, res) => {
  try {
    auditDetail.rps.updateOne(
      { rpId: req.body.rpId },
      { $set: req.body },
      async function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          // logger.pushConsoleInfo("doc", doc);

          let hostsArr = [];
          await req.body.auditTeam.forEach((host) => {
            let obj = {
              username: host,
              status: false,
            };
            hostsArr.push(obj);
          });
          await CollabChatSystem.bulkWrite(
            hostsArr.map((data) => ({
              updateOne: {
                filter: {
                  auditId: req.body.auditId,
                  "hosts.username": { $ne: data.username },
                },
                update: { $push: { hosts: data } },
              },
            }))
          );

          // let hostsArr = [];
          // await req.body.auditTeam.forEach((host) => {
          //   let obj = {
          //     username: host,
          //     status: false,
          //   };
          //   hostsArr.push(obj);
          // });

          // await CollabChatSystem.update(
          //   { auditId: req.body.auditId },
          //   { $addToSet: { hosts: { $each: hostsArr } } },

          //   (err, result) => {
          //     if (err) {
          //       logger.pushConsoleInfo(err);
          //       // res.status(404).end({ message: "Record not found", error: err });
          //     } else {
          //       logger.pushConsoleInfo(result);

          //       // res.send(doc);
          //     }
          //   }
          // );
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
auditController.updateOneAudit = async (req, res) => {
  try {
    auditDetail.audits.updateOne(
      { auditId: req.body.auditId },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
module.exports = auditController;