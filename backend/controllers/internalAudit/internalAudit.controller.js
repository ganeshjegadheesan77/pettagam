const auditDetail = require("../../models/Audit/audit.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const universeCollection = require("../../models/enia/universe.schema");
const issues = require("../../models/IssueActions/issue.schema");
const mongoose = require("mongoose");
const logger = require("../../utils/logger").logger;
const internalAuditController = {};
const collabChatSystem = require("../../controllers/chatSystem/collabChatSystem.controller");

internalAuditController.generateCurrentYearAudits = async (req, res) => {
  try {
    var lastIdArr = [];
    universeCollection.find().exec(async (err, data) => {
      if (err) return handleError(err);
      var uniResult = data.filter((d) => d.auditInfo.length);
      if (!uniResult.length) {
        logger.pushConsoleInfo("No audit for current rotation year");
        res.send("No audit for current rotation year");
      }
      lastIdArr = await internalAudit.lastIdCollection.find();

      uniResult.forEach((element) => {
        var dupYears = element.auditInfo.filter(
          (el) =>
            parseInt(el.rotation) + parseInt(el.lastAudit) ==
            new Date().getFullYear()
        );
        logger.pushConsoleInfo("dupYears", dupYears);

        if (!dupYears.length) {
          logger.pushConsoleInfo("No audit for current rotation year");
          res.send("No audit for current rotation year");
        }
        dupYears.forEach((dy) => {
          internalAudit.auditMasters.findOne(
            { auditId: dy.currentAudit },
            async (err, response) => {
              if (err) res.status(500).json(err);

              let auditResp = JSON.parse(JSON.stringify(response));
              delete auditResp._id;
              delete auditResp.createdAt;
              delete auditResp.updatedAt;
              auditResp.year = new Date().getFullYear();
              auditResp.auditPbc = [];
              lastIdArr.map((obj) => {
                obj.lastId = parseInt(obj.lastId) + 1;
                funcIncrement(obj);
              });

              function funcIncrement(obj) {
                switch (obj.prefix) {
                  case "OP":
                    auditResp.auditId = "OP-" + obj.lastId;
                    break;
                  case "OM":
                    auditResp.openingMeeting.aom_id = "OM-" + obj.lastId;
                    break;
                  case "AA":
                    auditResp.announcement.aa_id = "AA-" + obj.lastId;
                    break;
                  case "DR":
                    auditResp.draftReport.adr_id = "DR-" + obj.lastId;
                    break;
                  case "CM":
                    auditResp.closingMeeting.acm_id = "CM-" + obj.lastId;
                    break;
                  case "PM":
                    auditResp.planningMemorandum.apm_id = "PM-" + obj.lastId;
                    break;
                  default:
                    break;
                }
              }

              async function updateUniverse(updateData) {
                let universeRecord = await universeCollection.findOne({
                  id: updateData.id,
                  parent: updateData.parent,
                });
                // logger.pushConsoleInfo('universeRecord', universeRecord)
                let universePayload = JSON.parse(
                  JSON.stringify(universeRecord)
                );
                let index = universePayload.auditInfo.findIndex(
                  (r) => r.currentAudit == updateData.oldAuditId
                );
                logger.pushConsoleInfo("index", index);
                universePayload.auditInfo[index]["currentAudit"] =
                  updateData.currentAuditId;
                universePayload.auditInfo[index]["lastAudit"] = updateData.year;
                universePayload.auditInfo[index]["oldAuditIds"].push(
                  updateData.oldAuditId
                );
                universeCollection.updateOne(
                  { id: updateData.id, parent: updateData.parent },
                  { $set: { auditInfo: universePayload.auditInfo } },
                  function (err, doc) {
                    if (err) {
                      logger.pushError(e);
                      res
                        .status(404)
                        .end({ message: "Record not found", error: err });
                    }
                  }
                );
              }

              let rd = new internalAudit.auditMasters(auditResp);
              rd.save(async (err, audit) => {
                if (err) res.json(err);
                const updateData = {
                  id: audit.uID,
                  parent: audit.parentUId,
                  oldAuditId: response.auditId,
                  currentAuditId: audit.auditId,
                  year: audit.year,
                };

                updateUniverse(updateData);
                lastIdArr.forEach(async (element) => {
                  await internalAudit.lastIdCollection.findOneAndUpdate(
                    { prefix: element.prefix },
                    { $set: { lastId: element.lastId } },
                    { new: true }
                  );
                });

                res.status(201).json(audit);
              });
            }
          );
        });
      });
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getCurrentYearAudits = async (req, res) => {
  try {
    var jasonObj = {};
    universeCollection.find().exec(function (err, data) {
      if (err) return handleError(err);
      data.forEach((element) => {
        let currYearAudit =
          parseInt(element.rotation) + parseInt(element.lastAudit);
        if (new Date().getFullYear() === currYearAudit) {
          if (element.newAuditId) {
            internalAudit.auditMasters.findOne(
              { _id: element.newAuditId },
              (err, response) => {
                if (err) res.status(500).json(err);
                if (response != null) {
                  if (response.audit_plan_memo) {
                    internalAudit.audit_plan_memos.findOne(
                      { _id: response.audit_plan_memo },
                      (err, response) => {
                        if (err) res.status(500).json(err);
                        // logger.pushConsoleInfo("response:", response);
                        let memoResp = response.toObject();
                        delete memoResp._id;
                        let rd = new internalAudit.audit_plan_memos(memoResp);
                        rd.save((err, memos) => {
                          if (err) {
                            logger.pushError(e);
                            res.json(err);
                          } else {
                            // logger.pushConsoleInfo("Audit Saved" + memos);
                            jasonObj.audit_plan_memo = memos._id;
                            // return memos;
                          }
                        });
                      }
                    );
                  }

                  //  if(response.audit_opening_meeting){

                  //    internalAudit.audit_opening_meeting.findOne(
                  //      { _id: response.audit_opening_meeting },
                  //      (err, response) => {
                  //        if (err) res.status(500).json(err);
                  //        let memoResp = response.toObject();
                  //        delete memoResp._id;
                  //        let rd = new internalAudit.audit_opening_meeting(memoResp);
                  //        rd.save((err, memos) => {
                  //          if (err) {
                 // logger.pushError(e);
                  //            res.json(err);
                  //          } else {
                  //            // logger.pushConsoleInfo("Audit Saved" + memos);
                  //            jasonObj.audit_opening_meeting = memos._id;
                  //            logger.pushConsoleInfo(jasonObj)
                  //            // return memos;
                  //          }

                  //        });
                  //      }
                  //    );
                  //  }

                  //  if(response.audit_announce){

                  //    internalAudit.audit_announce.findOne(
                  //      { _id: response.audit_announce },
                  //      (err, response) => {
                  //        if (err) res.status(500).json(err);
                  //        let memoResp = response.toObject();
                  //        delete memoResp._id;
                  //        let rd = new internalAudit.audit_announce(memoResp);
                  //        rd.save((err, memos) => {
                  //          if (err) {
                  //logger.pushError(e);
                  //            res.json(err);
                  //          } else {
                  //            // logger.pushConsoleInfo("Audit Saved" + memos);
                  //            jasonObj.audit_announce = memos._id;
                  //            logger.pushConsoleInfo(jasonObj)
                  //            // return memos;
                  //          }

                  //        });
                  //      }
                  //    );
                  //  }

                  //  if(response.audit_draft_report){

                  //    internalAudit.audit_draft_report.findOne(
                  //      { _id: response.audit_draft_report },
                  //      (err, response) => {
                  //        if (err) res.status(500).json(err);
                  //        let memoResp = response.toObject();
                  //        delete memoResp._id;
                  //        let rd = new internalAudit.audit_draft_report(memoResp);
                  //        rd.save((err, memos) => {
                  //          if (err) {
                  //logger.pushError(e);
                  //            res.json(err);
                  //          } else {
                  //            // logger.pushConsoleInfo("Audit Saved" + memos);
                  //            jasonObj.audit_draft_report = memos._id;
                  //            logger.pushConsoleInfo(jasonObj)
                  //            // return memos;
                  //          }

                  //        });
                  //      }
                  //    );
                  //  }

                  //  if(response.audit_closing_meeting){
                  //    internalAudit.audit_closing_meeting.findOne(
                  //      { _id: response.audit_closing_meeting },
                  //      (err, response) => {
                  //        if (err) res.status(500).json(err);
                  //        let memoResp = response.toObject();
                  //        delete memoResp._id;
                  //        let rd = new internalAudit.audit_closing_meeting(memoResp);
                  //        rd.save((err, memos) => {
                  //          if (err) {
                  //logger.pushError(e);
                  //            res.json(err);
                  //          } else {
                  //            jasonObj.audit_closing_meeting = memos._id;
                  //            logger.pushConsoleInfo(jasonObj)
                  //          }

                  //        });
                  //      }
                  //    );
                  //  }
                }
              }
            );

            setTimeout(function () {
              var doc = element.toObject();
              delete doc._id;
              delete doc.year;
              delete doc.isNewAudit;

              if (doc.audit_plan_memo) delete doc.audit_plan_memo;
              doc.audit_plan_memo = jasonObj.audit_plan_memo;
              if (doc.audit_opening_meeting) delete doc.audit_opening_meeting;
              doc.audit_opening_meeting = jasonObj.audit_opening_meeting;
              if (doc.audit_announce) delete doc.audit_announce;
              doc.audit_announce = jasonObj.audit_announce;
              if (doc.audit_draft_report) delete doc.audit_draft_report;
              doc.audit_draft_report = jasonObj.audit_draft_report;
              if (doc.audit_closing_meeting) delete doc.audit_closing_meeting;
              doc.audit_closing_meeting = jasonObj.audit_closing_meeting;

              doc._id = mongoose.Types.ObjectId();
              doc.year = new Date().getFullYear();
              doc.isNewAudit = true;
              let str = doc.auditId;
              doc.auditId = str.split("OP")[1];

              let rd = new internalAudit.auditMasters(doc);
              rd.save((err, memos) => {
                if (err) {
                  logger.pushError(e);
                  res.json(err);
                } else {
                  logger.pushConsoleInfo("Audit Saved" + memos);
                  element.newAuditId = memos._id;
                  let lastAudit = new Date().getFullYear();
                  universeCollection.updateOne(
                    { _id: element._id },
                    {
                      $set: {
                        newAuditId: element.newAuditId,
                        lastAudit: lastAudit,
                        currentYearAudit: true,
                      },
                    },
                    function (err, doc) {
                      if (err) {
                        logger.pushError(e);
                        res
                          .status(404)
                          .end({ message: "Record not found", error: err });
                      } else {
                        logger.pushConsoleInfo("doc", doc);
                        // res.send(doc);
                      }
                    }
                  );
                }
              });
              // res.json(jasonObj);
            }, 2000);
          }
        }
      });
      res.json(jasonObj);
    });

    // internalAudit.auditMasters.find()
    //   .exec(function (err, data) {
    //     if (err) return handleError(err);
    //     // logger.pushConsoleInfo("Data: ", data);
    //     data.forEach(element => {
    //     let currYearAudit = parseInt(element.rotation) + parseInt(element.year);
    //       if(new Date().getFullYear() === currYearAudit){

    //         if(element.audit_plan_memo){
    //             internalAudit.audit_plan_memos.findOne(
    //             { _id: element.audit_plan_memo },
    //             (err, response) => {
    //               if (err) res.status(500).json(err);
    //               // logger.pushConsoleInfo("response:", response);
    //               let memoResp = response.toObject();
    //               delete memoResp._id;
    //               let rd = new internalAudit.audit_plan_memos(memoResp);
    //               rd.save((err, memos) => {
    //                 if (err) {
    //logger.pushError(e);
    //                   res.json(err);
    //                 } else {
    //                   // logger.pushConsoleInfo("Audit Saved" + memos);
    //                   jasonObj.audit_plan_memo = memos._id;
    //                   // return memos;
    //                 }

    //               });
    //             }
    //           );
    //         }

    //         if(element.audit_opening_meeting){

    //           internalAudit.audit_opening_meeting.findOne(
    //             { _id: element.audit_opening_meeting },
    //             (err, response) => {
    //               if (err) res.status(500).json(err);
    //               let memoResp = response.toObject();
    //               delete memoResp._id;
    //               let rd = new internalAudit.audit_opening_meeting(memoResp);
    //               rd.save((err, memos) => {
    //                 if (err) {
    //logger.pushError(e);
    //                   res.json(err);
    //                 } else {
    //                   // logger.pushConsoleInfo("Audit Saved" + memos);
    //                   jasonObj.audit_opening_meeting = memos._id;
    //                   logger.pushConsoleInfo(jasonObj)
    //                   // return memos;
    //                 }

    //               });
    //             }
    //           );
    //         }

    //         if(element.audit_announce){

    //           internalAudit.audit_announce.findOne(
    //             { _id: element.audit_announce },
    //             (err, response) => {
    //               if (err) res.status(500).json(err);
    //               let memoResp = response.toObject();
    //               delete memoResp._id;
    //               let rd = new internalAudit.audit_announce(memoResp);
    //               rd.save((err, memos) => {
    //                 if (err) {
    //logger.pushError(e);
    //                   res.json(err);
    //                 } else {
    //                   // logger.pushConsoleInfo("Audit Saved" + memos);
    //                   jasonObj.audit_announce = memos._id;
    //                   logger.pushConsoleInfo(jasonObj)
    //                   // return memos;
    //                 }

    //               });
    //             }
    //           );
    //         }

    //         if(element.audit_draft_report){

    //           internalAudit.audit_draft_report.findOne(
    //             { _id: element.audit_draft_report },
    //             (err, response) => {
    //               if (err) res.status(500).json(err);
    //               let memoResp = response.toObject();
    //               delete memoResp._id;
    //               let rd = new internalAudit.audit_draft_report(memoResp);
    //               rd.save((err, memos) => {
    //                 if (err) {
   // logger.pushError(e);
    //                   res.json(err);
    //                 } else {
    //                   // logger.pushConsoleInfo("Audit Saved" + memos);
    //                   jasonObj.audit_draft_report = memos._id;
    //                   logger.pushConsoleInfo(jasonObj)
    //                   // return memos;
    //                 }

    //               });
    //             }
    //           );
    //         }

    //         if(element.audit_closing_meeting){
    //           internalAudit.audit_closing_meeting.findOne(
    //             { _id: element.audit_closing_meeting },
    //             (err, response) => {
    //               if (err) res.status(500).json(err);
    //               let memoResp = response.toObject();
    //               delete memoResp._id;
    //               let rd = new internalAudit.audit_closing_meeting(memoResp);
    //               rd.save((err, memos) => {
    //                 if (err) {
    //logger.pushError(e);
    //                   res.json(err);
    //                 } else {
    //                   jasonObj.audit_closing_meeting = memos._id;
    //                   logger.pushConsoleInfo(jasonObj)
    //                 }

    //               });
    //             }
    //           );
    //         }

    //         setTimeout(function(){
    //           var doc = element.toObject();
    //             delete doc._id;
    //             delete doc.year;
    //             delete doc.isNewAudit;

    //             if(doc.audit_plan_memo) delete doc.audit_plan_memo; doc.audit_plan_memo = jasonObj.audit_plan_memo;
    //             if(doc.audit_opening_meeting) delete doc.audit_opening_meeting; doc.audit_opening_meeting= jasonObj.audit_opening_meeting;
    //             if(doc.audit_announce) delete doc.audit_announce;doc.audit_announce= jasonObj.audit_announce;
    //             if(doc.audit_draft_report) delete doc.audit_draft_report;doc.audit_draft_report= jasonObj.audit_draft_report;
    //             if(doc.audit_closing_meeting) delete doc.audit_closing_meeting;doc.audit_closing_meeting= jasonObj.audit_closing_meeting;

    //             doc._id = mongoose.Types.ObjectId();
    //             doc.year = new Date().getFullYear();
    //             doc.isNewAudit = true;
    //             let rd =  new internalAudit.auditMasters(doc);
    //              rd.save((err, memos) => {
    //               if (err) {
   // logger.pushError(e);
    //                 res.json(err);
    //               } else {
    //                 logger.pushConsoleInfo("Audit Saved" + memos);

    //                 res.json(memos);
    //               }

    //             });
    //         // res.json(jasonObj);
    //        },5000);

    //       }
    //     });

    //   });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getLastIdCollection = async (req, res) => {
  try {
    internalAudit.lastIdCollection.find().exec(function (err, data) {
      if (err) return handleError(err);

      res.json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getIssueList = async (req, res) => {
  internalAudit.auditMasters.findOne(
    { auditId: req.body.auditId },
    async (err, data) => {
      if (err) return handleError(err);
      if (data) {
        logger.pushConsoleInfo("data", data);
        let issueData = [];
        for (let i = 0; i < data.audit_issues.length; i++) {
          await issues.issue.findOne(
            { issueId: data.audit_issues[i] },
            (err, response) => {
              if (err) res.status(500).json(err);
              logger.pushConsoleInfo("response:", response);
              issueData.push(response);
            }
          );
        }
        // await logger.pushConsoleInfo("risks response:", risks);
        await res.send(issueData);
      } else {
        res.status(500).json(err);
      }
    }
  );
  // try{
  //   // internalAudit.auditMasters.aggregate(
  //   //   [ { $match : { auditId : req.body.auditId } },
  //   //     { $lookup: { from: 'issues', localField: 'issueId', foreignField: 'audit_issues', as: 'issueData' } },

  //   //   ]
  //   //   )
  //     .exec(function (err, data) {
  //       if (err) return handleError(err);
  //        logger.pushConsoleInfo(data);
  //         res.json(data);
  //     });
  // }catch(e){
  //   // { $lookup: { from: 'issues', localField: 'audit_issues', foreignField: 'issueId', as: 'issueData' } },
  //   //     { $lookup: { from: 'audit_PBC', localField: 'audit_pbc', foreignField: 'apbc_id', as: 'PBCData' } },
  //   //     { $lookup: { from: 'audit_closing_meeting', localField: 'audit_closing_meeting', foreignField: '_id', as: 'closingMeetingData' } }
  //     return res.status(406).send(e);
  // }
};

internalAuditController.getAuditDataForRiskAssess = async (req, res) => {
  try {
    universeCollection
      .aggregate([
        { $match: { _id: req.body._id } },
        {
          $lookup: {
            from: "audit_master",
            localField: "_id",
            foreignField: "newAuditId",
            as: "auditData",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) return handleError(err);
        logger.pushConsoleInfo(data);
        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    // { $lookup: { from: 'issues', localField: 'audit_issues', foreignField: 'issueId', as: 'issueData' } },
    //     { $lookup: { from: 'audit_PBC', localField: 'audit_pbc', foreignField: 'apbc_id', as: 'PBCData' } },
    //     { $lookup: { from: 'audit_closing_meeting', localField: 'audit_closing_meeting', foreignField: '_id', as: 'closingMeetingData' } }
    return res.status(406).send(e);
  }
};

internalAuditController.addNewAuditInUniverse = async (req, res) => {
  let rd = await new universeCollection(req.body);
  await rd.save((err, memos) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("Audit Saved" + memos);

      res.send(memos);
    }
  });

  //   try{
  //     universeCollection.find({type:req.body.type}, { name: 1, id: 1 , type:1})
  //   .exec(function (err, data) {
  //     if (err) return handleError(err);

  //       res.json(data);
  //   });
  //   }
  //   catch(e){
  //     return res.status(406).send(e);
  // }
};

internalAuditController.getCommodity = async (req, res) => {
  try {
    universeCollection
      .find({ type: req.body.type }, { name: 1, id: 1, type: 1 })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getRegion = async (req, res) => {
  try {
    universeCollection
      .find(
        { parent: req.body.parent.toString() },
        { name: 1, id: 1, parent: 1, type: 1 }
      )
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getLocation = async (req, res) => {
  try {
    universeCollection
      .find({ parent: req.body.parent }, { name: 1, id: 1, parent: 1, type: 1 })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getSite = async (req, res) => {
  try {
    universeCollection
      .find({ parent: req.body.parent }, { name: 1, id: 1, parent: 1, type: 1 })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//-------------------------------------------------------------------------------------//
internalAuditController.getMasterDivision = async (req, res) => {
  try {
    internalAudit.master_division.find({}).exec(function (err, data) {
      if (err) return handleError(err);

      res.json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getMasterLeadContact = async (req, res) => {
  try {
    internalAudit.masterLeadContactInfo.find().exec(function (err, data) {
      if (err) return handleError(err);

      res.json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getMasterProcess = async (req, res) => {
  try {
    internalAudit.masterProcessInfo.find().exec(function (err, data) {
      if (err) return handleError(err);

      res.json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getMasterDepartment = async (req, res) => {
  try {
    internalAudit.masterDepartmentInfo.find().exec(function (err, data) {
      if (err) return handleError(err);

      res.json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

// ----------------------------------------------------------------------------------//

internalAuditController.updateAuditDraftReport = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $set: { audit_draft_report: req.body.audit_draft_report } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateAuditAnnounce = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $set: { audit_announce: req.body.audit_announce } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateAuditOpeningMeeting = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $set: { audit_opening_meeting: req.body.audit_opening_meeting } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateAuditPlanMemo = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $set: { audit_plan_memo: req.body.audit_plan_memo } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateSingleCM = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_closing_meeting.updateOne(
      { acm_id: req.body.acm_id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateCMIntoAuditMaster = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $set: { audit_closing_meeting: req.body.audit_closing_meeting } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateSinglePBC = async (req, res) => {
  try {
    internalAudit.audit_PBC.findOneAndUpdate(
      { apbc_id: req.body.apbc_id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getPBC = async (req, res) => {
  try {
    internalAudit.audit_PBC.findOne({ apbc_id: req.body.apbc_id }, function (
      err,
      doc
    ) {
      if (err) {
        logger.pushError(e);
        res.status(404).end({ message: "Record not found", error: err });
      } else {
        logger.pushConsoleInfo("doc", doc);
        res.send(doc);
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updatePBC = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $push: { audit_pbc: req.body.pbcId } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateIssue = async (req, res) => {
  try {
    internalAudit.auditMasters.update(
      { auditId: req.body.auditId },
      { $push: { audit_issues: req.body.issueId } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getNoOfAudits = async (req, res) => {
  try {
    logger.pushConsoleInfo("year: ", req.body.year);

    //currentYearAudit

    universeCollection
      .find({ currentYearAudit: true })
      .countDocuments((err, data) => {
        if (err) {
          logger.pushError(e);
          res.status(404).end("Record not found");
        } else {
          logger.pushConsoleInfo("Count:", data);
          res.end(JSON.stringify(data, null, 2));
        }
      });
    // universeCollection.aggregate([
    //   {
    //     "$lookup": {
    //         "from": "audit_master",
    //         "localField": "newAuditId",
    //         "foreignField": "_id",
    //         "as": "follower"
    //     }

    // },
    // { "$match" : { "follower.year" : JSON.stringify(req.body.year) } },
    // { "$project": { "follower.year": 1 } },
    // {
    //   "$count": "year"
    // }
    // ])
    //  .exec(function (err, data) {
    //       logger.pushConsoleInfo("Response Data: " ,data,err)
    //       if (err) return res.status(406).json(err);

    //         res.json(data);
    //     });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getFieldWorkData = async (req, res) => {
  try {
    internalAudit.audit_field_work.findOne(
      { _id: req.body._id },
      (err, data) => {
        if (err) {
          logger.pushError(e);
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getDraftReport = async (req, res) => {
  try {
    internalAudit.audit_draft_report.findOne(
      { _id: req.body._id },
      (err, data) => {
        if (err) {
          logger.pushError(e);
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.updateDraftReport = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_draft_report.updateOne(
      { _id: req.body._id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getAnnouncement = async (req, res) => {
  try {
    internalAudit.audit_announce.findOne({ _id: req.body._id }, (err, data) => {
      if (err) {
        logger.pushError(e);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateAnnouncement = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_announce.updateOne(
      { _id: req.body._id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateFieldWork = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_field_work.updateOne(
      { _id: req.body._id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getOpeningMeeting = async (req, res) => {
  try {
    internalAudit.audit_opening_meeting.findOne(
      { _id: req.body._id },
      (err, data) => {
        if (err) {
          logger.pushError(e);
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateOpenMeet = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_opening_meeting.updateOne(
      { _id: req.body._id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateOpenMemo = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.audit_plan_memos.updateOne(
      { _id: req.body._id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getAuditMemo = async (req, res) => {
  try {
    internalAudit.audit_plan_memos.findOne(
      { _id: req.body._id },
      (err, data) => {
        if (err) {
          logger.pushError(e);
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.updateAudit = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  try {
    internalAudit.auditMasters.updateOne(
      { auditId: req.body.audit_id },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.findFieldWorks = async (req, res) => {
  try {
    internalAudit.audit_field_work
      .find({
        $or: [
          { subprocess_name: req.body.subprocess_name },
          { risk_name: req.body.risk_name },
        ],
      })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getSpecificNewAudit = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "audit_field_work",
            localField: "subprocess_name",
            foreignField: "audit_subprocess.subprocess_name",
            as: "fieldWorkData",
          },
        },
        {
          $lookup: {
            from: "issues",
            localField: "audit_issues",
            foreignField: "issueId",
            as: "issueData",
          },
        },
        {
          $lookup: {
            from: "audit_PBC",
            localField: "audit_pbc",
            foreignField: "apbc_id",
            as: "PBCData",
          },
        },
        {
          $lookup: {
            from: "audit_closing_meeting",
            localField: "audit_closing_meeting",
            foreignField: "_id",
            as: "closingMeetingData",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) return handleError(err);
        logger.pushConsoleInfo(data);
        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getAuditListFieldWork = async (req, res) => {
  try {
    universeCollection
      .find({ newAuditId: { $ne: null } })
      .populate("newAuditId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateLastId = async (req, res) => {
  logger.pushConsoleInfo(req.body);

  try {
    internalAudit.lastIdCollection.updateOne(
      { prefix: req.body.prefix },
      { $set: { lastId: req.body.newId } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.updateUniverse = async (req, res) => {
  logger.pushConsoleInfo(req.body);

  try {
    universeCollection.updateOne(
      { _id: req.body._id },
      { $set: { newAuditId: req.body.newAuditId } },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getLastGeneratedId = async (req, res) => {
  try {
    internalAudit.lastIdCollection
      .findOne({ prefix: req.body.prefix })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.getTestAuditData = async (req, res) => {
  try {
    logger.pushConsoleInfo(req.body.auditId);
    universeCollection
      .find({ oldAuditId: req.body.auditId })
      .populate("oldAuditId")
      .populate({
        path: "oldAuditId",
        populate: [
          {
            path: "audit_plan_memo",
            model: "audit_plan_memo",
          },
          {
            path: "audit_opening_meeting",
            model: "audit_opening_meeting",
          },
          {
            path: "audit_announce",
            model: "audit_announce",
          },
          {
            path: "audit_draft_report",
            model: "audit_draft_report",
          },
          {
            path: "audit_issues",
            model: "issues",
          },
        ],
      })
      .exec(function (err, data) {
        logger.pushConsoleInfo("Response Data: ", data, err);
        if (err) return res.status(406).json(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

internalAuditController.postAuditData = async (req, res) => {
  // res.send('pass');
  logger.pushConsoleInfo("BODY: ", req.body);
  let payload = await new internalAudit.auditMasters({
    parentUId: req.body.parentUId,
    uID: req.body.uID,
    auditableEntity: req.body.auditableEntity,
    rotation: req.body.rotation,
    year: req.body.year,
    auditId: req.body.auditId,
    auditOwner: req.body.auditOwner,
    auditOwnerId: req.body.auditOwnerId,
    auditSubmitter: req.body.auditSubmitter,
    auditSubmitterId: req.body.auditSubmitterId,
    auditApprovedBy: req.body.auditApprovedBy,
    openingMeeting: {
      aom_id: req.body.aom_id,
      OMisSet: req.body.OMisSet,
      approverStatus: "",
    },
    planningMemorandum: { apm_id: req.body.apm_id, PMisSet: req.body.PMisSet },
    announcement: { aa_id: req.body.aa_id, AAisSet: req.body.AAisSet },
    draftReport: { adr_id: req.body.adr_id, DRisSet: req.body.DRisSet },
    closingMeeting: { acm_id: req.body.acm_id, CMisSet: req.body.DRisSet },
    scope: req.body.scope,
  });
  // let rd = await new internalAudit.auditMasters(payload);
  await payload.save(async (err, data) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("Audit Saved" + data);
      let roomData = {
        title: req.body.auditId + "-" + req.body.auditableEntity,
        auditId: req.body.auditId,
        roomId: req.body.auditId,
      };
      logger.pushConsoleInfo("roomData---?>", roomData);
      await collabChatSystem.autoCreateNewChatroom(roomData);
      let uniData = {
        auditableEntity: req.body.auditableEntity,
        rotation: req.body.rotation,
        lastAudit: req.body.year,
        currentAudit: data.auditId,
      };
      universeCollection.findOneAndUpdate(
        { id: req.body.uID, parent: req.body.parentUId },
        { $push: { auditInfo: uniData } },
        function (err, doc) {
          if (err) {
            logger.pushError(e);
            res.status(404).end({ message: "Record not found", error: err });
          } else {
            logger.pushConsoleInfo("doc", doc);
            res.send(doc);
          }
        }
      );
    }
  });
};
internalAuditController.getAuditData = async (req, res) => {
  try {
    auditDetail.audits
      .findOne({ auditId: req.body.auditId })
      .populate("audit_plan_memo")
      .populate("audit_opening_meeting")
      .populate("audit_announce")
      .populate("audit_field_work")
      .populate("audit_draft_report")
      .populate("audit_PBC")
      .populate("audit_issues")
      .populate("audit_closing_meeting")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.saveAuditPlanMemo = async (req, res) => {
  logger.pushConsoleInfo(req.body);
  let rd = await new internalAudit.audit_plan_memos(req.body);
  await rd.save((err, memos) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + memos);

      res.send(memos);
    }
  });
};

internalAuditController.saveAuditOpeningMeeting = async (req, res) => {
  let rd = await new internalAudit.audit_opening_meeting(req.body);
  await rd.save((err, meeting) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + meeting);

      res.send(meeting);
    }
  });
};

internalAuditController.saveAuditAnnounce = async (req, res) => {
  let rd = await new internalAudit.audit_announce(req.body);
  await rd.save((err, announcements) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + announcements);

      res.send(announcements);
    }
  });
};

internalAuditController.saveAuditFeldWork = async (req, res) => {
  let rd = await new internalAudit.audit_field_work(req.body);
  await rd.save((err, work) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + work);

      res.send(work);
    }
  });
};

internalAuditController.saveAuditDraftReport = async (req, res) => {
  let rd = await new internalAudit.audit_draft_report(req.body);
  await rd.save((err, dReport) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + dReport);

      res.send(dReport);
    }
  });
};

internalAuditController.saveAuditPBC = async (req, res) => {
  let rd = await new internalAudit.audit_PBC(req.body);
  await rd.save((err, pbc) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + pbc);

      res.send(pbc);
    }
  });
};

internalAuditController.saveAuditIssues = async (req, res) => {
  let rd = await new internalAudit.audit_issues(req.body);
  await rd.save((err, aIssues) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + aIssues);

      res.send(aIssues);
    }
  });
};

internalAuditController.saveAuditClosingMeeting = async (req, res) => {
  let rd = await new internalAudit.audit_closing_meeting(req.body);
  await rd.save((err, cmeeting) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + cmeeting);

      res.send(cmeeting);
    }
  });
};
internalAuditController.getInterviews = (req, res) => {
  try {
    internalAudit.aom_interview.find((err, data) => {
      if (err) {
        logger.pushError(e);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.updateInterview = async (req, res) => {
  try {
    internalAudit.aom_interview.updateOne(
      { isId: req.body.isId },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.getCountData = async (req, res) => {
  try {
    internalAudit.countData.find((err, data) => {
      if (err) {
        logger.pushError(e);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.updateCountData = async (req, res) => {
  try {
    internalAudit.countData.updateOne(
      { doc: "count" },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(e);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
internalAuditController.saveInterview = async (req, res) => {
  let rd = await new internalAudit.aom_interview(req.body);
  await rd.save((err, rec) => {
    if (err) {
      logger.pushError(e);
      res.json(err);
    } else {
      res.send(rec);
    }
  });
  internalAudit.audit_opening_meeting
    .updateOne(
      { aom_id: req.body.aom_id },
      { $pull: { aom_interview_schedule: req.body.isId } }
    )
    .then((res) => {
      // resolve(res);
    })
    .catch(
      (err) => {
        logger.pushError(err);
      }
      //reject(err)
    );
};
internalAuditController.deleteInterview = async (req, res) => {
  internalAudit.aom_interview.deleteOne(
    { isId: req.body.aom_interview_schedule },
    function (err, data) {
      if (err) {
        logger.pushError(e);
        res.json(err);
      } else {
        res.send(data);
      }
    }
  );
  // internalAudit.audit_opening_meeting.updateOne({ aom_id: req.aom_id }, { $pull: { aom_interview_schedule: req.interview } })
  // .then(res => {
  //   resolve(res);
  // })
  // .catch(err => reject(err));
};
module.exports = internalAuditController;