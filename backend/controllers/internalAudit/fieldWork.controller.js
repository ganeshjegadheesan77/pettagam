const auditDetail = require("../../models/Audit/audit.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const rpsSchema = require("../../models/Audit/audit.schema");

const universeCollection = require("../../models/enia/universe.schema");
const issues = require("../../models/IssueActions/issue.schema");
const actionModel = require("../../models/IssueActions/actions.schema");

const mongoose = require("mongoose");
const rooms = require("../../controllers/chatSystem/room.controller");
// const collabChatSystem = require("../../controllers/chatSystem/collabChatSystem.controller");
const CollabChatSystem = require("../../models/chatSystem/collabChatSystem");
const logger = require("../../utils/logger").logger;

const { resolve } = require("path");

const fieldWorkController = {};

fieldWorkController.updateUniverseData = async (req, res) => {
  try {
    if (req.body.approverStatus == "completed") {
      let universeRecord = await universeCollection.findOne({
        $and: [{ parent: req.body.parent, id: req.body.id }],
      });
      let universePayload = JSON.parse(JSON.stringify(universeRecord));
      let index = universePayload.auditInfo.findIndex(
        (r) => r.currentAudit == req.body.auditId
      );
      // logger.pushConsoleInfo("index", index);

      universePayload.auditInfo[index]["isCompleted"] = true;

      universeCollection.updateOne(
        { id: req.body.id, parent: req.body.parent },
        { $set: { auditInfo: universePayload.auditInfo } },
        function (err, doc) {
          if (err) {
            logger.pushError(err);
            res.status(404).end({ message: "Record not found", error: err });
          } else {
            res.status(200).send(doc);
          }
        }
      );
    } else {
      res.status(200).send({ message: "approver needs to complete it first" });
    }
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.updateOverallProgress = async (req, res) => {
  try {
    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: { auditProgress: req.body.progress } }
      )
      .exec((err, data) => {
        if (err) {
          logger.pushError(err);
          res.json(err);
          res.status(200).json(data);
        }
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getCompletedAudit = async (req, res) => {
  try {
    internalAudit.auditMasters
      .find(
        { $and: [{ year: new Date().getFullYear(), isCompleted: true }] },
        { type: 1, name: 1, id: 1 }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getLocation = async (req, res) => {
  try {
    universeCollection
      .find({ type: "Location" }, { type: 1, name: 1, id: 1 })
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getSites = async (req, res) => {
  try {
    universeCollection
      .find({ parent: req.body.parent }, { name: 1, id: 1, parent: 1, type: 1 })
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }

        res.json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getIssuesAttached = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "issues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "issueData",
          },
        },
        {
          $project: {
            issueData: 1,
          },
        },
      ])
      .exec(async (err, data) => {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data[0].issueData);
        let actionArr = [];
        data[0].issueData.forEach(async (ele) => {
          ele.issuedata = {
            issueId: ele.issueId,
            issueName: ele.issueName,
            issueDescription: ele.issueDescription,
            issueType: ele.issueType,
            keyIsuue: ele.keyIsuue,
          };
          actionArr.push(ele.issuedata);
          ele.actionId.forEach(async (element) => {
            logger.pushConsoleInfo("ele.issueId", ele.issueId);
            await actionModel.action.findOne(
              {
                actionId: element,
                issueId: { $in: ele.issueId },
              },
              (err, doc) => {
                if (err) {
                  logger.pushError(err);
                  res.status(500).json(err);
                }
                logger.pushConsoleInfo("doc", doc);

                ele.issuedata.actionData = {
                  actionId: doc.actionId,
                  actionName: doc.actionName,
                  actionDescription: doc.actionDescription,
                  area: doc.area,
                  rating: doc.rating,
                };
                actionArr.push(ele.actionData);

                logger.pushConsoleInfo("actionArr", actionArr);
              }
            );
          });
        });
        setTimeout(() => {
          res.send(actionArr);
        }, 300);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getTeamForPbcAndFW = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.fAssignee": 1,
            "resourePlanningData.fapAssignee": 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
fieldWorkController.getActiveAudits = async (req, res) => {
  try {
    // var lastIdArr = [];
    let year = new Date().getFullYear();
    internalAudit.auditMasters
      .find(
        { $and: [{ year: year }, { isCompleted: false }] },
        {
          auditId: 1,
          year: 1,
          auditableEntity: 1,
          uID: 1,
          auditProgress: 1,
          "draftReport.adr_rating": 1,
        }
      )
      .exec(async (err, data) => {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        // var uniResult = data.universeData.filter((d) => d.auditInfo);
        // var dd = [];
        var fullArr = [];

        data.forEach(async (dy) => {
          let myArr = await universeCollection.find(
            { id: dy.uID },
            { name: 1, type: 1, parent: 1, id: 1, auditInfo: 1 }
          );
          myArr.forEach((ele) => {
            let index = ele.auditInfo.findIndex(
              (r) => r.currentAudit == dy.auditId
            );
            let riskLevel = ele.auditInfo[index].auditRiskAssessment;
            fullArr.push({
              riskLevel: riskLevel,
              auditId: dy.auditId,
              year: dy.year,
              auditableEntity: dy.auditableEntity,
              uID: dy.uID,
              name: ele.name,
              type: ele.type,
              parent: ele.parent,
              id: ele.id,
              auditProgress: dy.auditProgress,
              rating: dy.draftReport.adr_rating,
            });
          });
        });
        setTimeout(function () {
          res.status(200).json(fullArr);
          logger.pushConsoleInfo("dd", fullArr);
        }, 300);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

function updateFWStatus(data) {
  logger.pushConsoleInfo("data::", data);
  internalAudit.auditMasters.updateOne(
    { auditId: data.auditId, "auditFieldWork.auditFw_id": data.id },
    {
      $set: {
        "auditFieldWork.$.status": data.status,
        "auditFieldWork.$.assigneeStatus": data.assigneeStatus,
        "auditFieldWork.$.approverStatus": data.approverStatus,
      },
    },
    function (err, doc) {
      if (err) {
        logger.pushError(err);
        // res.status(404).end({ message: "Record not found", error: err });
        logger.pushConsoleInfo(err);
      }
      if (doc) {
        return doc;
      }
    }
  );
}
function updatePBCStatus(data) {
  logger.pushConsoleInfo("data::", data);
  internalAudit.auditMasters.updateOne(
    {
      auditId: data.auditId,
      "auditPbc.auditPBC_id": data.id,
    },
    {
      $set: {
        "auditPbc.$.status": data.status,
        "auditPbc.$.assigneeStatus": data.assigneeStatus,
        "auditPbc.$.approverStatus": data.approverStatus,
      },
    },
    function (err, doc) {
      if (err) {
        logger.pushError(err);
        // res.status(404).end({ message: "Record not found", error: err });
        logger.pushConsoleInfo(err);
      }
      if (doc) {
        return doc;
      }
    }
  );
}
fieldWorkController.updateFWDetails = async (req, res) => {
  try {
    let data = {
      id: req.body.auditFw_id,
      auditId: req.body.master_audit_id,
      status: req.body.status,
      assigneeStatus: req.body.assigneeStatus,
      approverStatus: req.body.approverStatus ? req.body.approverStatus : "",
    };
    logger.pushConsoleInfo("data-->", data);
    delete req.body.assigneeStatus;
    delete req.body.approverStatus;

    internalAudit.audit_field_work.findOneAndUpdate(
      { auditFw_id: req.body.auditFw_id },
      { $set: req.body },
      async function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          var response = await updateFWStatus(data);
          logger.pushConsoleInfo("doc", doc, response);
          await res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getFWDetails = async (req, res) => {
  try {
    internalAudit.audit_field_work
      .aggregate([
        { $match: { auditFw_id: req.body.auditFw_id } },
        {
          $lookup: {
            from: "audit_master",
            localField: "master_audit_id",
            foreignField: "auditId",
            as: "masterData",
          },
        },
        {
          $project: {
            "masterData.performancePeriod": 1,
            "masterData.auditFieldWork.assigneeStatus": 1,
            "masterData.auditFieldWork.approverStatus": 1,
            afw_name: 1,
            afw_id: 1,
            auditFw_id: 1,
            afw_subprocess: 1,
            afw_risk: 1,
            afw_status: 1,
            afw_results: 1,
            master_audit_id: 1,
            afw_tester: 1,
            afw_attachments: 1,
            afw_test_procedure: 1,
            afw_signoff_period: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
    // internalAudit.audit_field_work.findOne(
    //   { auditFw_id: req.body.auditFw_id },
    //   (err, data) => {
    //     if (err) {logger.pushError(err);{logger.pushError(err);
    //       res.status(404).end("Record not found");
    //     } else {
    //       res.end(JSON.stringify(data, null, 2));
    //     }
    //   }
    // );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveFW = async (req, res) => {
  let rd = await new internalAudit.audit_field_work(req.body);
  await rd.save(async (err, fw) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + fw);
      let payload = {
        fw_id: fw.afw_id,
        auditFw_id: req.body.auditFw_id,
        afw_name: fw.afw_name,
        status: req.body.status,
        approverStatus: "",
      };
      await internalAudit.auditMasters.findOneAndUpdate(
        { auditId: req.body.auditId },
        { $push: { auditFieldWork: payload } }
      );
      res.send(fw);
    }
  });
};

fieldWorkController.getFWDesc = async (req, res) => {
  try {
    internalAudit.auditMasters
      .find(
        { auditId: req.body.auditId },
        {
          auditFieldWork: 1,
          periodInScope: 1,
          performancePeriod: 1,
          updateMeetingDate: 1,
          openingMeetingDate: 1,
          closingMeetingDate: 1,
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        // let ddd = JSON.parse(JSON.stringify(data));
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getRisks = async (req, res) => {
  try {
    internalAudit.subProcessRiskInfo.find(
      { subProcess_name: req.body.subProcess_name },
      { risks: 1 },
      function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getSubproceses = async (req, res) => {
  try {
    internalAudit.subProcessRiskInfo.find({}, function (err, doc) {
      if (err) {
        logger.pushError(err);
        res.status(404).end({ message: "Record not found", error: err });
      } else {
        logger.pushConsoleInfo("doc", doc);
        res.send(doc);
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.addToCurrentYear = async (req, res) => {
  var lastIdArr = [];

  internalAudit.auditMasters.findOne(
    { auditId: req.body.auditId },
    async (err, response) => {
      if (err) {
        logger.pushError(err);
        res.status(500).json(err);
      }
      lastIdArr = await internalAudit.lastIdCollection.find();

      let auditResp = JSON.parse(JSON.stringify(response));

      delete auditResp._id;
      delete auditResp.createdAt;
      delete auditResp.updatedAt;
      auditResp.year = new Date().getFullYear();

      auditResp.duplicateAuditFlag = true;
      auditResp.isCompleted = false;
      auditResp.issueIds = [];
      let owner = req.query.owner ? req.query.owner : auditResp.auditOwner;

      lastIdArr.map((obj) => {
        obj.lastId = parseInt(obj.lastId) + 1;
        funcIncrement(obj);
      });

      if (auditResp.auditPbc.length > 0) {
        logger.pushConsoleInfo("** console 1");
        await generatePBC(auditResp);
        logger.pushConsoleInfo("** console 2");
      } else {
        auditResp.auditPbc = [];
      }
      if (auditResp.auditFieldWork.length > 0) {
        logger.pushConsoleInfo("** console 1 FW");
        await generateFieldWork(auditResp);
        logger.pushConsoleInfo("** console 2 FW");
      } else {
        auditResp.auditFieldWork = [];
      }

      async function generatePBC(auditResp) {
        let auditId = auditResp.auditId;

        for (const element of auditResp.auditPbc) {
          let pbcRespOld = await internalAudit.audit_PBC.findOne({
            auditPbc_id: element.auditPBC_id,
          });
          let pbcResp = JSON.parse(JSON.stringify(pbcRespOld));
          delete pbcResp._id;
          delete pbcResp.createdAt;
          delete pbcResp.updatedAt;
          let oldPBCId = pbcResp.auditPbc_id;
          let newID = oldPBCId.split("-")[1];
          pbcResp.auditPbc_id = auditId.replace("-", "") + "-" + newID;
          pbcResp.apbc_id = newID;
          pbcResp.master_audit_id = auditId;
          logger.pushConsoleInfo("pbcResp", pbcResp);
          let rd = new internalAudit.audit_PBC(pbcResp);
          let rd1 = await rd.save();
          logger.pushConsoleInfo("rd1", rd1);
          tempArr.push({
            status: "assigned",
            pbc_id: pbcResp.apbc_id,
            pbc_name: pbcResp.apbc_name,
            auditPBC_id: pbcResp.auditPbc_id,
          });
          logger.pushConsoleInfo("tempArr", tempArr);
        }
      }

      async function generateFieldWork(auditResp) {
        let auditId = auditResp.auditId;

        for (const element of auditResp.auditFieldWork) {
          let fwRespOld = await internalAudit.audit_field_work.findOne({
            auditFw_id: element.auditFw_id,
          });
          let fwResp = JSON.parse(JSON.stringify(fwRespOld));
          delete fwResp._id;
          delete fwResp.createdAt;
          delete fwResp.updatedAt;
          let oldFWId = fwResp.auditFw_id;
          let newID = oldFWId.split("-")[1];
          fwResp.auditFw_id = auditId.replace("-", "") + "-" + newID;
          fwResp.afw_id = newID;
          fwResp.master_audit_id = auditId;
          logger.pushConsoleInfo("pbcResp", fwResp);
          let rd = new internalAudit.audit_field_work(fwResp);
          let rd1 = await rd.save();
          logger.pushConsoleInfo("rd1", rd1);
          tempFWArr.push({
            status: "assigned",
            fw_id: fwResp.afw_id,
            afw_name: fwResp.afw_name,
            auditFw_id: fwResp.auditFw_id,
          });
          logger.pushConsoleInfo("tempArr", tempFWArr);
        }
      }

      if (auditResp.rpId) {
        const filter = { rpId: auditResp.rpId };
        const update = { auditId: auditResp.auditId };
        await rpsSchema.rps.findOneAndUpdate(filter, update);
      } else {
        auditResp.rpId = "";
      }

      function funcIncrement(obj) {
        // this.auditLastId = parseInt(obj.lastId) + 1;
        let str = obj.lastId.toString();
        let pad = "000";
        obj.lastId = pad.substring(0, pad.length - str.length) + str;
        switch (obj.prefix) {
          case "OP":
            auditResp.auditId = "OP-" + obj.lastId;
            break;
          case "OM":
            auditResp.openingMeeting.aom_id = "OM-" + obj.lastId;
            auditResp.openingMeeting.dup_aom_Flag = true;
            break;
          case "AA":
            auditResp.announcement.aa_id = "AA-" + obj.lastId;
            auditResp.announcement.dup_aa_Flag = true;
            break;
          case "DR":
            auditResp.draftReport.adr_id = "DR-" + obj.lastId;
            auditResp.draftReport.dup_adr_Flag = true;
            break;
          case "CM":
            auditResp.closingMeeting.acm_id = "CM-" + obj.lastId;
            auditResp.closingMeeting.dup_acm_Flag = true;
            break;
          case "PM":
            auditResp.planningMemorandum.apm_id = "PM-" + obj.lastId;
            auditResp.planningMemorandum.dup_apm_Flag = true;
            break;
          default:
            break;
        }
      }

      async function updateUniverse(updateData) {
        let universeRecord = await universeCollection.findOne({
          id: updateData.id,
          parent: updateData.parent,
        });
        let siteName = universeRecord.name;
        //  logger.pushConsoleInfo('universeRecord', universeRecord);return;
        let universePayload = JSON.parse(JSON.stringify(universeRecord));
        let index = universePayload.auditInfo.findIndex(
          (r) => r.currentAudit == updateData.oldAuditId
        );
        logger.pushConsoleInfo("index", index);
        universePayload.auditInfo[index]["currentAudit"] =
          updateData.currentAuditId;
        universePayload.auditInfo[index]["lastAudit"] = updateData.year;
        universePayload.auditInfo[index]["isCompleted"] = false;

        universePayload.auditInfo[index]["oldAuditIds"].push(
          updateData.oldAuditId
        );
        universeCollection.updateOne(
          { id: updateData.id, parent: updateData.parent },
          { $set: { auditInfo: universePayload.auditInfo } },
          function (err, doc) {
            if (err) {
              logger.pushError(err);
              res.status(404).end({ message: "Record not found", error: err });
            }
          }
        );
        return siteName;
      }

      let rd = new internalAudit.auditMasters(auditResp);
      rd.save(async (err, audit) => {
        if (err) {
          logger.pushError(err);
          res.json(err);
        }
        const chatroom = new CollabChatSystem({
          title: "Audit " + audit.auditId + ":" + auditResp["auditableEntity"],
          hosts: owner,
          auditId: audit.auditId,
          roomId: audit.auditId,
        });
        // Save room in the database
        chatroom
          .save()
          .then((data) => {
            logger.pushConsoleInfo("autoCreateNewChatroom Saved:", data);
            return data;
          })
          .catch((err) => {
            logger.pushConsoleInfo(err);
            return err;
          });
        // let roomData = {
        //   title:
        //     "Audit " +
        //     audit.auditId +
        //     ":" +
        //     auditResp["auditableEntity"] +
        //     "-" +
        //     siteName,
        //   hosts: owner,
        //   auditId: audit.auditId,
        //   roomId: audit.auditId,
        // };
        // logger.pushConsoleInfo("roomData---?>", roomData);
        // await collabChatSystem.autoCreateNewChatroom(roomData);
        const updateData = {
          id: audit.uID,
          parent: audit.parentUId,
          oldAuditId: response.auditId,
          currentAuditId: audit.auditId,
          year: audit.year,
        };

        await updateUniverse(updateData);
        lastIdArr.forEach(async (element) => {
          await internalAudit.lastIdCollection.findOneAndUpdate(
            { prefix: element.prefix },
            { $set: { lastId: element.lastId } },
            { new: true }
          );
        });

        res.status(201).json(audit);
      });
    }
  );
};

fieldWorkController.updatePBCDetails = async (req, res) => {
  try {
    let data = {
      id: req.body.auditPbc_id,
      auditId: req.body.master_audit_id,
      status: req.body.status,
      assigneeStatus: req.body.assigneeStatus,
      approverStatus: req.body.approverStatus ? req.body.approverStatus : "",
    };

    delete req.body.approverStatus;
    delete req.body.assigneeStatus;

    internalAudit.audit_PBC.findOneAndUpdate(
      { auditPbc_id: req.body.auditPbc_id },
      { $set: req.body },
      async function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          let response = updatePBCStatus(data);
          logger.pushConsoleInfo("doc", doc, response);
          await res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getPBCDetails = async (req, res) => {
  try {
    internalAudit.audit_PBC
      .aggregate([
        { $match: { auditPbc_id: req.body.auditPbc_id } },
        {
          $lookup: {
            from: "audit_master",
            localField: "master_audit_id",
            foreignField: "auditId",
            as: "masterData",
          },
        },
        {
          $project: {
            "masterData.performancePeriod": 1,
            "masterData.auditPbc.assigneeStatus": 1,
            "masterData.auditPbc.approverStatus": 1,
            auditPbc_id: 1,
            apbc_name: 1,
            apbc_signoff_period: 1,
            apbc_work_step: 1,
            apbc_status: 1,
            apbc_document_req: 1,
            apbc_attachments: 1,
            master_audit_id: 1,
            auditPbc_id: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getPBCDesc = async (req, res) => {
  try {
    internalAudit.auditMasters
      .find(
        { auditId: req.body.auditId },
        {
          auditPbc: 1,
          performancePeriod: 1,
          updateMeetingDate: 1,
          openingMeetingDate: 1,
          closingMeetingDate: 1,
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        // let ddd = JSON.parse(JSON.stringify(data));
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
fieldWorkController.savePBC = async (req, res) => {
  let rd = await new internalAudit.audit_PBC(req.body);
  await rd.save(async (err, pbc) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      logger.pushConsoleInfo("risk saved!" + pbc);
      let payload = {
        pbc_id: pbc.apbc_id,
        auditPBC_id: req.body.auditPbc_id,
        pbc_name: pbc.apbc_name,
        status: req.body.status,
        approverStatus: "",
      };
      await internalAudit.auditMasters.findOneAndUpdate(
        { auditId: req.body.auditId },
        { $push: { auditPbc: payload } }
      );
      res.send(pbc);
    }
  });
};

fieldWorkController.auditCount = async (req, res) => {
  internalAudit.auditMasters
    .find({ year: req.body.year })
    .countDocuments((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
};
fieldWorkController.updateLastIdCollection = async (req, res) => {
  internalAudit.lastIdCollection.updateMany(
    {},
    { $set: { lastId: req.body.lastId } },
    { new: true },
    function (err, doc) {
      if (err) {
        logger.pushError(err);
        res.status(404).end({ message: "Record not found", error: err });
      }
      res.status(200).json(doc);
    }
  );
};
fieldWorkController.getAuditDetailForRP = async (req, res) => {
  try {
    internalAudit.auditMasters
      .findOne(
        { auditId: req.body.auditId },
        {
          openingMeeting: 0,
          planningMemorandum: 0,
          announcement: 0,
          draftReport: 0,
          closingMeeting: 0,
          audit_engagement_plan: 0,
          audit_program: 0,
          auditPbc: 0,
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveAnnouncementData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: { announcement: req.body } },
        {
          $push: { "announcement.$.aa_attachments": req.body.aa_attachments },
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("DATA:", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getAnnouncementData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.pAssignee": 1,
            "resourePlanningData.papAssignee": 1,
            announcement: 1,
            auditId: 1,
            aa_id: 1,
            auditableEntity: 1,
            auditName: 1,
            periodInScope: 1,
            performancePeriod: 1,
            updateMeetingDate: 1,
            openingMeetingDate: 1,
            closingMeetingDate: 1,
            duplicateAuditFlag: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        // if(data.length) {
        //   rpsSchema.rps.findOne({
        //     auditId: req.body.auditId
        //   },{
        //     pAssignee : 1,
        //     papAssignee : 1
        //   }, function(err, rsp) {
        //     res.status(200).json(data);
        //     logger.pushConsoleInfo("papAssigneepapAssignee--->", rsp);
        //   })
        // }
        res.status(200).json(data[0]);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
////////////////////////////////////////////////////////

fieldWorkController.getClosingMeetingData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.rAssignee": 1,
            "resourePlanningData.rapAssignee": 1,
            closingMeeting: 1,
            auditId: 1,
            acm_id: 1,
            auditableEntity: 1,
            auditName: 1,
            periodInScope: 1,
            performancePeriod: 1,
            updateMeetingDate: 1,
            openingMeetingDate: 1,
            closingMeetingDate: 1,
          },
        },
        {
          $lookup: {
            from: "issues",
            localField: "auditId",
            foreignField: "auditId",
            as: "issueData",
          },
        },
        {
          $lookup: {
            from: "actions",
            localField: "issueData.actionId",
            foreignField: "actionId",
            as: "actionData",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("getClosingMeetingData--->", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getOpenMeetUpData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.pAssignee": 1,
            "resourePlanningData.papAssignee": 1,
            openingMeeting: 1,
            auditId: 1,
            aom_id: 1,
            auditableEntity: 1,
            auditName: 1,
            periodInScope: 1,
            performancePeriod: 1,
            updateMeetingDate: 1,
            openingMeetingDate: 1,
            closingMeetingDate: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveOpenMeetUptData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: { openingMeeting: req.body } }
      )
      .exec((err, data) => {
        if (err) {
          logger.pushError(err);
          res.json(err);
        }
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getOpenMemotUpData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.pAssignee": 1,
            "resourePlanningData.papAssignee": 1,
            planningMemorandum: 1,
            auditId: 1,
            apm_id: 1,
            auditableEntity: 1,
            auditName: 1,
            periodInScope: 1,
            performancePeriod: 1,
            updateMeetingDate: 1,
            openingMeetingDate: 1,
            closingMeetingDate: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getDraftRepoData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "resourceplannings",
            localField: "rpId",
            foreignField: "rpId",
            as: "resourePlanningData",
          },
        },
        {
          $project: {
            "resourePlanningData.rAssignee": 1,
            "resourePlanningData.rapAssignee": 1,
            draftReport: 1,
            auditId: 1,
            adr_id: 1,
            auditableEntity: 1,
            auditName: 1,
            periodInScope: 1,
            performancePeriod: 1,
            updateMeetingDate: 1,
            openingMeetingDate: 1,
            closingMeetingDate: 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("data", data);
        res.status(200).json(data);
      });
    // internalAudit.auditMasters
    //   .findOne(
    //     { auditId: req.body.auditId },
    //     {
    //       draftReport: 1,
    //       auditId: 1,
    //       adr_id: 1,
    //       auditableEntity: 1,
    //       auditName: 1,
    //       periodInScope: 1,
    //       performancePeriod:1,
    //       updateMeetingDate:1,
    //       openingMeetingDate:1,
    //       closingMeetingDate:1,
    //     }
    //   )
    //   .exec(function (err, data) {
    //     if (err) {logger.pushError(err);return handleError(err);}
    //     res.status(200).json(data);
    //   });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveMemorandumData = async (req, res) => {
  try {
    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: { planningMemorandum: req.body } },
        {
          $push: {
            "planningMemorandum.$.apm_attachments": req.body.apm_attachments,
          },
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("DATA:", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
fieldWorkController.updateAudit = async (req, res) => {
  internalAudit.auditMasters.updateOne(
    { auditId: req.body.audit_id },
    { $set: req.body },
    function (err, doc) {
      if (err) {
        logger.pushError(err);
        res.status(404).end({ message: "Record not found", error: err });
      }
      res.status(200).json(doc);
    }
  );
};
fieldWorkController.getAuditDetail = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $project: {
            openingMeeting: 0,
            planningMemorandum: 0,
            announcement: 0,
            draftReport: 0,
            closingMeeting: 0,
            audit_engagement_plan: 0,
            audit_program: 0,
          },
        },
        {
          $lookup: {
            from: "audit_field_work",
            localField: "auditFieldWork.auditFw_id",
            foreignField: "auditFw_id",
            as: "fieldWordData",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        res.status(200).json(data);
      });
    // internalAudit.auditMasters.findOne({ auditId: req.body.auditId },
    //   {
    //     openingMeeting: 0, planningMemorandum: 0, announcement: 0, draftReport: 0, closingMeeting: 0,
    //     audit_engagement_plan: 0, audit_program: 0, uID: 0, parentUId: 0
    //   }).exec(function (err, data) {
    //     if (err) {logger.pushError(err);return handleError(err);
    //     res.status(200).json(data);
    //   });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveDraftRepoData = async (req, res) => {
  try {
    if (!req.body.approverStatus) {
      req.body.approverStatus = "";
    }
    let updatedObj = { draftReport: req.body };

    if (req.body.approverStatus == "completed") {
      updatedObj.isCompleted = true;
    }

    logger.pushConsoleInfo("aaaaaaaaa", req.body);
    logger.pushConsoleInfo("updatedObj--->", updatedObj);

    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: updatedObj },
        {
          $push: { "draftReport.$.adr_attachments": req.body.adr_attachments },
          new: true,
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        // data.approverStatus = req.body.approverStatus;
        logger.pushConsoleInfo("DATA:", data);

        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.saveCloseMeetData = async (req, res) => {
  if (!req.body.approverStatus) {
    req.body.approverStatus = "";
  }
  try {
    internalAudit.auditMasters
      .findOneAndUpdate(
        { auditId: req.body.auditId },
        { $set: { closingMeeting: req.body } },
        {
          $push: {
            "closingMeeting.$.acm_attachments": req.body.acm_attachments,
          },
        }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        logger.pushConsoleInfo("DATA:", data);
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.getAudits = async (req, res) => {
  try {
    let currYear = new Date().getFullYear();
    internalAudit.auditMasters
      .find(
        { year: currYear },
        { auditId: 1, _id: 1, auditableEntity: 1, draftReport: 1 }
      )
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        res.status(200).json(data);
      });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

fieldWorkController.generateCurrentYearAudits = async (req, res) => {
  try {
    logger.pushConsoleInfo("loginUser--->", req.query);
    var lastIdArr = [];
    var dupYears = [];
    var tempArr = [];
    var tempFWArr = [];

    universeCollection.find().exec(async (err, data) => {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }
      var uniResult = data.filter((d) => d.auditInfo.length);
      if (!uniResult.length) {
        let msg = {
          msg: "No audits to Found...",
          status: 200,
        };

        res.status(200).json(msg);
        return;
      }
      lastIdArr = await internalAudit.lastIdCollection.find();
      // logger.pushConsoleInfo('************************************uniResult', uniResult)
      uniResult.forEach((element) => {
        dupYears.push(
          ...element.auditInfo.filter(
            (el) =>
              parseInt(el.rotation) + parseInt(el.lastAudit) ==
              new Date().getFullYear()
          )
        );
      });

      if (!dupYears.length) {
        let msg = {
          msg: "No audits to duplicate for current year...",
          status: 200,
        };

        res.status(200).json(msg);
        return;
      } else {
        dupYears.forEach((dy) => {
          logger.pushConsoleInfo("dy", dy);
          internalAudit.auditMasters.findOne(
            { auditId: dy.currentAudit },
            async (err, response) => {
              if (err) {
                logger.pushError(err);
                res.status(500).json(err);
              }
              let auditResp = JSON.parse(JSON.stringify(response));

              delete auditResp._id;
              delete auditResp.createdAt;
              delete auditResp.updatedAt;
              let owner = req.query.owner
                ? req.query.owner
                : auditResp.auditOwner;
              auditResp.year = new Date().getFullYear();
              auditResp.duplicateAuditFlag = true;
              auditResp.isCompleted = false;
              auditResp.issueIds = [];
              lastIdArr.map((obj) => {
                obj.lastId = parseInt(obj.lastId) + 1;
                funcIncrement(obj);
              });

              if (auditResp.auditPbc.length > 0) {
                logger.pushConsoleInfo("** console 1");
                await generatePBC(auditResp);
                logger.pushConsoleInfo("** console 2");
              } else {
                auditResp.auditPbc = [];
              }
              if (auditResp.auditFieldWork.length > 0) {
                logger.pushConsoleInfo("** console 1 FW");
                await generateFieldWork(auditResp);
                logger.pushConsoleInfo("** console 2 FW");
              } else {
                auditResp.auditFieldWork = [];
              }

              async function generatePBC(auditResp) {
                let auditId = auditResp.auditId;

                for (const element of auditResp.auditPbc) {
                  let pbcRespOld = await internalAudit.audit_PBC.findOne({
                    auditPbc_id: element.auditPBC_id,
                  });
                  let pbcResp = JSON.parse(JSON.stringify(pbcRespOld));
                  delete pbcResp._id;
                  delete pbcResp.createdAt;
                  delete pbcResp.updatedAt;
                  let oldPBCId = pbcResp.auditPbc_id;
                  let newID = oldPBCId.split("-")[1];
                  pbcResp.auditPbc_id = auditId.replace("-", "") + "-" + newID;
                  pbcResp.apbc_id = newID;
                  pbcResp.master_audit_id = auditId;
                  logger.pushConsoleInfo("pbcResp", pbcResp);
                  let rd = new internalAudit.audit_PBC(pbcResp);
                  let rd1 = await rd.save();
                  logger.pushConsoleInfo("rd1", rd1);
                  tempArr.push({
                    status: "assigned",
                    pbc_id: pbcResp.apbc_id,
                    pbc_name: pbcResp.apbc_name,
                    auditPBC_id: pbcResp.auditPbc_id,
                  });
                  logger.pushConsoleInfo("tempArr", tempArr);
                }
              }

              async function generateFieldWork(auditResp) {
                let auditId = auditResp.auditId;

                for (const element of auditResp.auditFieldWork) {
                  let fwRespOld = await internalAudit.audit_field_work.findOne({
                    auditFw_id: element.auditFw_id,
                  });
                  let fwResp = JSON.parse(JSON.stringify(fwRespOld));
                  delete fwResp._id;
                  delete fwResp.createdAt;
                  delete fwResp.updatedAt;
                  let oldFWId = fwResp.auditFw_id;
                  let newID = oldFWId.split("-")[1];
                  fwResp.auditFw_id = auditId.replace("-", "") + "-" + newID;
                  fwResp.afw_id = newID;
                  fwResp.master_audit_id = auditId;
                  logger.pushConsoleInfo("pbcResp", fwResp);
                  let rd = new internalAudit.audit_field_work(fwResp);
                  let rd1 = await rd.save();
                  logger.pushConsoleInfo("rd1", rd1);
                  tempFWArr.push({
                    status: "assigned",
                    fw_id: fwResp.afw_id,
                    afw_name: fwResp.afw_name,
                    auditFw_id: fwResp.auditFw_id,
                  });
                  logger.pushConsoleInfo("tempArr", tempFWArr);
                }
              }

              if (auditResp.rpId) {
                const filter = { rpId: auditResp.rpId };
                const update = { auditId: auditResp.auditId };
                await rpsSchema.rps.findOneAndUpdate(filter, update);
              } else {
                auditResp.rpId = "";
              }

              auditResp.auditPbc = tempArr;
              auditResp.auditFieldWork = tempFWArr;

              let rd = new internalAudit.auditMasters(auditResp);
              rd.save(async (err, audit) => {
                if (err) {
                  logger.pushError(err);
                  res.json(err);
                }
                try {
                  const chatroom = new CollabChatSystem({
                    title:
                      "Audit " +
                      audit.auditId +
                      ":" +
                      auditResp["auditableEntity"],
                    hosts: [{ username: owner }],
                    auditId: audit.auditId,
                    roomId: audit.auditId,
                  });
                  // Save room in the database
                  chatroom
                    .save()
                    .then((data) => {
                      logger.pushConsoleInfo(
                        "autoCreateNewChatroom Saved:",
                        data
                      );
                      return data;
                    })
                    .catch((err) => {
                      logger.pushConsoleInfo(err);
                      return err;
                    });
                  // let roomData = {
                  //   title:
                  //     "Audit " +
                  //     audit.auditId +
                  //     ":" +
                  //     auditResp["auditableEntity"] +
                  //     "-" +
                  //     siteName,
                  //   hosts: owner,
                  //   auditId: audit.auditId,
                  //   roomId: audit.auditId,
                  // };
                  // logger.pushConsoleInfo("roomData---?>", roomData);
                  // await collabChatSystem.autoCreateNewChatroom(roomData);

                  logger.pushConsoleInfo("audit table-->", audit);
                  const updateData = {
                    id: audit.uID,
                    parent: audit.parentUId,
                    oldAuditId: response.auditId,
                    currentAuditId: audit.auditId,
                    year: audit.year,
                  };
                  await updateUniverse(updateData);
                  lastIdArr.forEach(async (element) => {
                    await internalAudit.lastIdCollection.findOneAndUpdate(
                      { prefix: element.prefix },
                      { $set: { lastId: element.lastId } },
                      { new: true }
                    );
                  });
                } catch (e) {
                  logger.pushError(e);
                }
              });

              function funcIncrement(obj) {
                let str = obj.lastId.toString();
                let pad = "000";
                obj.lastId = pad.substring(0, pad.length - str.length) + str;
                switch (obj.prefix) {
                  case "OP":
                    auditResp.auditId = "OP-" + obj.lastId;
                    break;
                  case "OM":
                    auditResp.openingMeeting.aom_id = "OM-" + obj.lastId;
                    auditResp.openingMeeting.dup_aom_Flag = true;
                    break;
                  case "AA":
                    auditResp.announcement.aa_id = "AA-" + obj.lastId;
                    auditResp.announcement.dup_aa_Flag = true;
                    break;
                  case "DR":
                    auditResp.draftReport.adr_id = "DR-" + obj.lastId;
                    auditResp.draftReport.dup_adr_Flag = true;
                    break;
                  case "CM":
                    auditResp.closingMeeting.acm_id = "CM-" + obj.lastId;
                    auditResp.closingMeeting.dup_acm_Flag = true;
                    break;
                  case "PM":
                    auditResp.planningMemorandum.apm_id = "PM-" + obj.lastId;
                    auditResp.planningMemorandum.dup_apm_Flag = true;
                    break;
                  default:
                    break;
                }
              }

              async function updateUniverse(updateData) {
                let universeRecord = await universeCollection.findOne({
                  id: updateData.id,
                  parent: updateData.parent,
                });
                // logger.pushConsoleInfo('universeRecord', universeRecord)
                let siteName = universeRecord.name;
                let universePayload = JSON.parse(
                  JSON.stringify(universeRecord)
                );
                let index = universePayload.auditInfo.findIndex(
                  (r) => r.currentAudit == updateData.oldAuditId
                );
                // logger.pushConsoleInfo("index", index);
                universePayload.auditInfo[index]["currentAudit"] =
                  updateData.currentAuditId;
                universePayload.auditInfo[index]["lastAudit"] = updateData.year;
                universePayload.auditInfo[index]["isCompleted"] = false;
                universePayload.auditInfo[index]["oldAuditIds"].push(
                  updateData.oldAuditId
                );
                universeCollection.updateOne(
                  { id: updateData.id, parent: updateData.parent },
                  { $set: { auditInfo: universePayload.auditInfo } },
                  function (err, doc) {
                    if (err) {
                      logger.pushError(err);
                      res
                        .status(404)
                        .end({ message: "Record not found", error: err });
                    }
                  }
                );
                return siteName;
              }
            }
          );
        }); //////////////////////////////
        let msg = {
          msg: "Audits duplicated successfully..",
          status: 200,
        };
        res.status(201).json(msg);
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

module.exports = fieldWorkController;