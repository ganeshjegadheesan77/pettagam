const auditDetail = require("../../models/Audit/audit.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const ara = require("../../models/InternalAudit/araForm.schema");
const universeCollection = require("../../models/enia/universe.schema");
const issues = require("../../models/IssueActions/issue.schema");
const logger = require("../../utils/logger").logger;
const ARAController = {};

ARAController.saveARA = async (req, res) => {
  let ar = await new ara.araMaster(req.body);
  await ar.save(async (err, fw) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      let universeRecord = await universeCollection.findOne({
        id: req.body.id,
        parent: req.body.parent,
      });

      let universePayload = JSON.parse(JSON.stringify(universeRecord));
      let index = universePayload.auditInfo.findIndex(
        (r) => r.currentAudit == req.body.auditId
      );
      universePayload.auditInfo[index]["auditRiskAssessment"] = fw.araId;
      universePayload.auditInfo[index]["overallRisk"] = fw.overallRiskScore;
      universeCollection.updateOne(
        { id: req.body.id, parent: req.body.parent },
        { $set: { auditInfo: universePayload.auditInfo } },
        function (err, doc) {
          if (err) {
            logger.pushError(err);
            res.status(404).end({ message: "Record not found", error: err });
          }
        }
      );
      res.status(200).json(fw);
    }
  });
};

ARAController.updateARADetails = async (req, res) => {
  try {
    ara.araMaster.findOneAndUpdate(
      { araId: req.body.araId },
      req.body,
      async function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          let universeRecord = await universeCollection.findOne({
            id: req.body.id,
            parent: req.body.parent,
          });

          let universePayload = JSON.parse(JSON.stringify(universeRecord));
          let index = universePayload.auditInfo.findIndex(
            (r) => r.currentAudit == req.body.auditId
          );
          // universePayload.auditInfo[index]["auditRiskAssessment"] =fw.araId;
          universePayload.auditInfo[index]["overallRisk"] =
            req.body.overallRiskScore;
          universeCollection.updateOne(
            { id: req.body.id, parent: req.body.parent },
            { $set: { auditInfo: universePayload.auditInfo } },
            function (err, doc) {
              if (err) {
                logger.pushError(err);
                res
                  .status(404)
                  .end({ message: "Record not found", error: err });
              }
            }
          );
          res.status(200).json(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

ARAController.getARADetails = async (req, res) => {
  try {
    ara.araMaster.findOne({ araId: req.body.araId }).exec(function (err, data) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }
      res.status(200).json(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

ARAController.getOldAuditDetails = async (req, res) => {
  try {
    universeCollection.findOne(
      { "auditInfo.currentAudit": req.body.auditId },
      async (err, data) => {
        if (err) {
          logger.pushError(err);
          res.status(500).json(err);
        }
        let oldAuditData = [];
        data.auditInfo.forEach(async (ele) => {
          if (ele.currentAudit == req.body.auditId) {
            ele.oldAuditIds.forEach(async (elem) => {
              let auditRecord = await internalAudit.auditMasters.findOne(
                {
                  auditId: elem,
                },
                { year: 1, auditId: 1, auditOwner: 1 }
              );
              oldAuditData.push(auditRecord);
            });
          }
        });
        setTimeout(function () {
          res.status(200).json(oldAuditData);
        }, 200);
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

ARAController.getIssueDetails = async (req, res) => {
  try {
    internalAudit.auditMasters
      .aggregate([
        { $match: { auditId: req.body.auditId } },
        {
          $lookup: {
            from: "issues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "IssueData",
          },
        },
        {
          $project: {
            "IssueData.issueName": 1,
            "IssueData.issueId": 1,
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        res.status(200).json(data);
      });
    // internalAudit.auditMasters.findOne({ auditId: req.body.auditId },
    //   {
    //     openingMeeting: 0, planningMemorandum: 0, announcement: 0, draftReport: 0, closingMeeting: 0,
    //     audit_engagement_plan: 0, audit_program: 0, uID: 0, parentUId: 0
    //   }).exec(function (err, data) {
    //     if (err) return handleError(err);
    //     res.status(200).json(data);
    //   });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
module.exports = ARAController;

// exports.postARAForm = async (req, res) => {
//   logger.pushConsoleInfo("req.body:", req.body);
//   let araData = await new araForm.araForm({
//     auditId: req.body.auditId,
//     auditName: req.body.auditName,
//     riskLevel: req.body.riskLevel,
//     auditLocation: req.body.auditLocation,
//     issueIds: req.body.issueIds,
//     riskAssessmentId: req.body.riskAssessmentId,
//     leadAuditor: req.body.leadAuditor,
//     rotation: req.body.rotation,
//     nextAuditYear: req.body.nextAuditYear,
//     impact_financialRisk: req.body.impact_financialRisk,
//     impact_complianceRisk: req.body.impact_complianceRisk,
//     impact_overall: req.body.impact_overall,
//     likelihood_processRisk: req.body.likelihood_processRisk,
//     likelihood_internalControls: req.body.likelihood_internalControls,
//     likelihood_overall: req.body.likelihood_overall,
//     overallRiskScore: req.body.overallRiskScore,
//     riskAssessmentSubmitter: req.body.riskAssessmentSubmitter,
//     riskAssessmentSubmitID: req.body.riskAssessmentSubmitID,
//     riskAssessmentModifiedBy: req.body.riskAssessmentModifiedBy,
//     riskAssessmentApprovedBy: req.body.riskAssessmentApprovedBy,
//     riskAssessmentIdentifiedOn: req.body.riskAssessmentIdentifiedOn,
//     riskAssessmentApprovedOn: req.body.riskAssessmentApprovedOn,
//     riskAssessedOn: req.body.riskAssessedOn,
//     riskModifiedOn: req.body.riskModifiedOn,
//   });

//   await araData.save((err, data) => {
//     if (err) {
//       res.json(err);
//     } else {
//       internalAudit.auditMasters.updateOne(
//         { auditId: req.body.auditId },
//         { $set: { auditComplete: true } },
//         (err, response) => {
//           universeData.updateOne(
//             { riskLevel: req.body.riskLevel },
//             { $set: { overallRisk: req.body.overallRiskScore } },
//             function (err, result) {
//               if (err) {
//                 logger.pushConsoleInfo("Err :", err);
//               } else {
//                 logger.pushConsoleInfo("setCurrentYearAuditValue Data:", result);
//               }
//             }
//           );
//           if (err) {
//             res.status(500).json(err);
//           } else {
//             logger.pushConsoleInfo(response);
//             res.send(data);
//           }
//         }
//       );
//     }
//   });
// };
// exports.getARAForm = (req, res) => {
//   logger.pushConsoleInfo("auditId", req.body.id);

//   araForm.araForm.findOne({ riskLevel: req.body.id }, (err, data) => {
//     if (data) {
//       // auditComplete
//       logger.pushConsoleInfo("ARA Data:", data);
//       res.send(data);
//     } else {
//       logger.pushConsoleInfo("ARA Data:", err);
//       res.status(500).json(err);
//     }
//   });
// };
// exports.updateARAForm = (req, res) => {
//   logger.pushConsoleInfo("auditId", req.body);

//   araForm.araForm.updateOne(
//     { riskLevel: req.body.riskLevel },
//     { $set: req.body },
//     (err, data) => {
//       if (err) return handleError(err);
//       if (data) {
//         // auditComplete overallRiskScore
//         universeData.updateOne(
//           { riskLevel: req.body.riskLevel },
//           { $set: { overallRisk: req.body.overallRiskScore } },
//           function (err, result) {
//             if (err) {
//               logger.pushConsoleInfo("Err :", err);
//             } else {
//               logger.pushConsoleInfo("setCurrentYearAuditValue Data:", result);
//             }
//           }
//         );
//         res.send(data);
//       } else {
//         res.status(500).json(err);
//       }
//     }
//   );
// };