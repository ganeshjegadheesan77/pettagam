var multer = require("multer");
var express = require('express');
var app = express();
const path = require("path");


app.use("/api/uploads", express.static(path.join(__dirname, "uploads")));

const storageFieldWork = multer.diskStorage({
    destination: (req, file, callBack) => {
      callBack(null, "./uploads/fieldWork");
    },
    filename: (req, file, callBack) => {
      callBack(null, `${file.originalname}`);
    }
  });
  
  const uploadFieldWork = multer({ storage: storageFieldWork });
module.exports = uploadFieldWork