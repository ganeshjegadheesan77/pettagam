const documents = require('../../models/documents/documents.schema');
const dmsSessionTracker = require('../../models/dmsSessionTracker/dmsSessionTracker.js');
const fetch = require('node-fetch');
const config = require("../../config/config");
var fs = require('fs');
var moment = require('moment');
const statuses = require('../../config/statuses');
const dummy = require('./dummy');
var base64js = require('base64-js')
const https = require('https');
const { dms } = require('../../config/config');
const { array } = require('./dmsUpload');
const { Buffer } = require('buffer');
const { message } = require('./dummy');
const { response } = require('express');



const dmsController = {};

dmsController.checkDmsSession = async (req, res) => {
    try {
        //find user in DB
        var responseData = await dmsSessionTracker.dmsSessionTrackerInfo.findOne({ username: req.body.username });
        if (!responseData) {


            console.log('If record not found')
            //Generate Session Id
            let data = { sessionId: "testingsession", userName: req.body.username, repositoryName: config.dms.dmsRepositoryName }
            let connectionResponse = await connectDms(data);
            //Save new record in DB
            let saveData = { username: req.body.username, dmsSessionId: connectionResponse.dmsSession, sessionDate: moment().format('L') };
            let rd = new dmsSessionTracker.dmsSessionTrackerInfo(saveData);
            saveresponseData = await rd.save();
            res.send(saveresponseData)


        } if (responseData.sessionDate != moment().format('L')) {
            console.log('If record found but date changed');
            //Disconnect from DMS
            await DMSDisconnection(responseData.dmsSessionId);
            //generate new session id 
            let data = { sessionId: "testingsession", userName: req.body.username, repositoryName: config.dms.dmsRepositoryName }
            let connectionResponse = await connectDms(data);
            //update the collection
            req.body.sessionDate = moment().format('L');
            req.body.dmsSessionId = connectionResponse.dmsSession;
            var upadateResponseData = await dmsSessionTracker.dmsSessionTrackerInfo.findOneAndUpdate({ username: req.body.username }, { $set: req.body }, { new: true });
            res.send(upadateResponseData)

        } else {
            res.send(responseData);

        }


    } catch (e) {
        console.error(e);
        res.send(e);
    }
}

dmsController.generateDmsSessionForAuditee = async (req, res) => {
    try {
        let data = { sessionId: "testingsession", userName: req.body.username, repositoryName: config.dms.dmsRepositoryName }
        let connectionResponse = await connectDms(data);
        console.log('************connectionResponse', connectionResponse)
        res.status(200).send(connectionResponse);
    } catch (e) {
        console.error(e);
        res.status(500).send(e)

    }
}

dmsController.generateDmsSessionForAuditor = async (req, res) => {
    try {
        let data = { sessionId: "testingsession", userName: req.body.username, repositoryName: config.dms.dmsRepositoryName }
        let connectionResponse = await connectDms(data);
        console.log('************connectionResponse', connectionResponse)
        res.status(200).send(connectionResponse);
    } catch (e) {
        console.error(e);
        res.status(500).send(e)

    }
}

dmsController.upload = async (req, res) => {
    try {
        console.log('SESSSION ID:', req.body.dmsSessionId);
        var uploadDataResult = [];
        if (req.body.dmsSessionId == '') {
            console.error('DMS token not found');
            res.status(406).send({ message: 'DMS Token not found' });
        } else {
            let uploadResult = await uploadData(req.files, req.body);
            console.log('uploadResult', uploadResult)
            res.status(200).send(uploadResult);
            // Promise.all(uploadDataResult)
            //     .then((result) => {
                   

            //     }).catch((e) => {
            //         console.error(e);

            //         res.status(500).send(e);
            //     });

        }



    } catch (e) {
        console.error(e);
        res.send(e);
    }
}

async function uploadData(uploadInfo, formData) {
    try {
        console.error('::uploadData called')
        var fileInfo = [];
        for (const ele of uploadInfo) {
            let encodedData = fs.readFileSync(ele.path).toString("base64");
            console.log('*****formData.dmsSessionId', formData.dmsSessionId)
            var data = {
                "dmsSession": formData.dmsSessionId,
                "nodeId": "/NEW1",
                "metaData": formData.metadata,
                "attachments": [{ "filename": ele.originalname, "data": encodedData, "doctype": "DEFAULT" }]
            }
            // console.log('********data', data)
            let fetchRes = await fetch(config.dms.url + 'uploadDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
            let result = await fetchRes.json();
            console.log('*****upload doc result', result)
            if (result.status == '-7440') {
                console.error('::upload doc session expired')
                var i = 1;
                while (i <= 4) {
                    console.log('******i', i)
                    let data = { sessionId: "testingsession", userName: formData.checkedInBy, repositoryName: config.dms.dmsRepositoryName }
                    let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                    let connectionResult = await connectionResponse.json();
                    if (connectionResult.errorCode == 0 && i <= 3) {
                        console.error('::new dms session established')
                        console.log('**** DMS SESSION GENERATED SUCCESSFULLY');
                        console.log('**** UPLOAD DATA FUNCTION CALLED')
                        formData.dmsSessionId = connectionResult.dmsSession;
                        await uploadData(uploadInfo, formData);
                        break;
                    }
                    if (i == 4) {
                        console.error('::failed to generate new session')

                        return statuses[100];
                        // res.status(403).send(statuses['100']);
                    }
                    i++;
                }



            } else {
                formData.documentNo = await result.documentNo;
                formData.fileName = await result.attachmentresponse[0]['filename'];
                formData.urlToken = await result.urlToken;
                let rd = new documents.documentsInfo(formData);
                let rd1 = await rd.save();
                if (rd1) console.error('document generated in DB')
                console.log('****rd1', rd1);
                fileInfo.push(rd1);
                // response.push(rd1) 
                console.log('Continue execution');
            }
            // console.log('******data', data)

        }

        return Promise.resolve(fileInfo);
        // Promise.all(fileInfo)
        //     .then( (fileInfo) => {
        //         console.log('*******************fileInfo', fileInfo)
        //         return fileInfo;
        //     }).catch((e) => {
        //         console.error(e);
        //         throw e
        //         // res.status(500).send(e);
        //     });
        // res.send({message:'File uploaded successfully'});


    } catch (e) {
        console.error(e);
        throw e;
        // res.status(500).send(statuses['500']);
    }

    // try {

    //     console.log('*****INSIDE FUNCTION')
    // for (const element of uploadInfo) {
    //     let encodedData = fs.readFileSync(element.path).toString("base64");
    //     var data = {
    //         "dmsSession": "d3301fa4e16238b96b132c4011e3d2b40949dcdd201d98e472f09e94e28a972320181fb4043314ed",
    //         "nodeId": "/NEW1",
    //         "metaData": req.body.metadata,
    //         "attachments": [{ "filename": element.originalname, "data": encodedData, "doctype": "DEFAULT" }]
    //     }

    //     let fetchRes = await fetch(config.dms.url + 'uploadDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
    //     console.log('fetchRes', fetchRes)
    //     //delete files from node server 
    //     fs.unlink(element.path, (err) => {
    //         if (err) throw err;
    //         console.log(element.originalname + 'was deleted');
    //     });
    //     let result = await fetchRes.json();
    //     console.error()
    //     console.log('result', result)
    //     result.path = await element.path;
    //     fileInfo.push(result);

    // }
    // console.log('*****EXIT FUNCTION')


    // } catch (e) {
    //     console.error(e);
    //     res.send(e);
    // }

}

// dmsController.upload = async (req, res) => {
//     try {

//         // const files = req.files;
//         var fileInfo = [];
//         console.log('*** BEFORE UPLOAD')
//         await uploadData(req.files);
//         console.log('*** AFTER UPLOAD')
//         Promise.all(fileInfo)
//             .then(async (result) => {
//                 await documentTable(fileInfo);
//                 console.log('All files uploaded sucessfully...!', result)
//             }).catch((e) => {
//                 console.error(e);

//                 res.status(500).send(e);
//             });
//         async function uploadData(uploadInfo) {
//             try {

//                 console.log('*****INSIDE FUNCTION')
//             for (const element of uploadInfo) {
//                 let encodedData = fs.readFileSync(element.path).toString("base64");
//                 var data = {
//                     "dmsSession": req.body.dmsSessionId,
//                     "nodeId": "/NEW1",
//                     "metaData": req.body.metadata,
//                     "attachments": [{ "filename": element.originalname, "data": encodedData, "doctype": "DEFAULT" }]
//                 }

//                 let fetchRes = await fetch(config.dms.url + 'uploadDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
//                 console.log('fetchRes', fetchRes)
//                 //delete files from node server 
//                 fs.unlink(element.path, (err) => {
//                     if (err) throw err;
//                     console.log(element.originalname + 'was deleted');
//                 });
//                 let result = await fetchRes.json();
//                 console.error()
//                 console.log('result', result)
//                 result.path = await element.path;
//                 fileInfo.push(result);

//             }
//             console.log('*****EXIT FUNCTION')


//             } catch (e) {
//                 console.error(e);
//                 res.send(e);
//             }

//         }


//         async function documentTable(info) {
//             try {

//                     let response = [];
//             for (const element of info) {
//                 req.body.documentNo = element.documentNo;
//                 req.body.fileName = element.attachmentresponse[0]['filename'];
//                 req.body.urlToken = element.urlToken;
//                 let rd = new documents.documentsInfo(req.body);
//                 let rd1 = await rd.save();
//                 response.push(rd1)
//                 console.log('rd1', rd1)
//             }
//             Promise.all(response)
//                 .then((result) => {
//                     res.status(200).send(statuses[200]);

//                     console.log('All dms files updated in collection ...!', result)

//                 }).catch((e) => {
//                     console.error(e);

//                     res.status(500).send(e);
//                 });

//             } catch (e) {
//                 console.error(e)
//                 res.send(e)
//             }

//         }
//     } catch (e) {
//         console.error(e);
//         res.status(500).send(e);
//     }
// }


dmsController.retrieveDocument = async (req, res) => {
    try {
        var connectDMS = await DMSConnection();
        var result = await documents.documentsInfo.findOne({ $and: [{ moduleName: req.body.moduleName, fileName: req.body.fileName }] }, { documentNo: 1 });
        console.log('result', result)
        let findDoc = { "dmsSession": connectDMS.dmsSession, "documentNo": result.documentNo };
        let fetchRes = await fetch(config.dms.url + 'retrieveDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) });
        let documentResult = await fetchRes.json();
        console.log('documentResult', documentResult)
        await DMSDisconnection(connectDMS.dmsSession)

        res.status(200).send(documentResult)
    } catch (e) {
        console.error(e);
        res.status(500).send(e)

    }
}

dmsController.checkOut = async (req, res) => {
    try {

        //CheckoutType = 1 -> ReadOnly Checkout
        //CheckoutType = 2 -> Exclusive Checkout
        console.error('::CheckOut Called');
        if(req.body.dmsSessionId =='' || req.body.documentNo =='' || req.body.checkoutType ==''){
            console.error('::Unproccessable Data')
            res.status(406).send({ message: 'Unproccessable data' });

        }else{
            await dmsCheckOut(req.body);
            async function dmsCheckOut(bodyData) {
            try {
                
                let findDoc = { 'dmsSession': bodyData.dmsSessionId, 'documentNo': bodyData.documentNo, 'checkoutType': bodyData.checkoutType, 'comments': bodyData.comments };
                let fetchRes = await fetch(config.dms.url + 'checkOutDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) });
                let checkOutResult = await fetchRes.json();
                if(checkOutResult.errorCode != '0' && checkOutResult.errorCode != '-7440'){
                    console.error(checkOutResult.message)
                    res.send(checkOutResult);
                }else if(checkOutResult.errorCode =='-7440'){
                    console.error('::DMS SESSION EXPIRED')
                    var i = 1;
                            while (i <= 4) {
                                console.log('******i', i)
                                if(bodyData.checkOutBy ==''){ 
                                    console.error('::Checkout By missing')
                                    res.status(406).send({message:'username should not be null'});
                                    break;
                                };
                                
                                let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData.checkOutBy, repositoryName: config.dms.dmsRepositoryName }
                                let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                let connectionResult = await connectionResponse.json();
                                
                                if (connectionResult.errorCode == 0 && i <= 3) {
                                    console.error('::new dms session established')
                                    req.body.dmsSessionId = connectionResult.dmsSession;
                                    await dmsCheckOut(req.body);
                                    break;
                                }
                                if (i == 4) {
                                    console.error('::failed to generate new session')
                                    res.status(403).send({message:'failed to create new session or username missing'});
                                }
                                i++;
                            }
            
            
                }else{
                    
                     let updateResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: bodyData.documentNo }, { checkoutStatus: "2", checkOutBy: bodyData.checkOutBy }, { new: true });
                     if(updateResponse !=null){
                        console.error('::Checkout successfully...')
                        res.send(checkOutResult);
                     }else{
                        console.error('::Failed to update record in collection...')
                        res.send({message:'record not found to update'})
                     }
                     
                }
 
            } catch (e) {
                res.send(e)
            }
        }
        }
        
    } catch (e) {
        console.error(e);
        res.status(400).send(e);

    }
}

dmsController.replaceDocuments = async (req, res) => {
    try {
        const files = req.files;
        if (files.length === 1) {

            let encodedData = fs.readFileSync(files[0].path).toString("base64");

            let data = {
                "dmsSession": req.body.dmsSessionId,
                "documentNo": req.body.docId,
                "attachment": { "doctype": req.body.doctype, "filedata": [{ "filename": req.body.filename, "data": encodedData }] },
                "comments": req.body.comments
            }
            let fetchRes = await fetch(config.dms.url + 'replaceDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
            let replaceResult = await fetchRes.json();
            console.log('replaceResult', replaceResult)
            if (replaceResult.errorCode == 0) {
                let updateData = {
                    workPaperNumber: req.body.workPaperNumber,
                    fileName: req.body.filename
                };
                let updateResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: req.body.docId }, updateData, { new: true });
                console.log('replace name in doc repor mongo', updateResponse)
                fs.unlink(files[0].path, (err) => {
                    if (err) throw err;
                    console.log(files[0].originalname + ' was deleted');
                });
            }
            res.status(200).send(replaceResult);

        } else {
            console.error(e)
            res.status(500).send();
        }
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
}

//NEW API
dmsController.checkIn = async (req, res) => {
    try {
        console.error('::CheckIn Called');

        if(req.body.dmsSessionId =='' || req.body.documentNo ==''|| req.body.checkedInBy ==''){
            console.error('::Unproccessable Data')
            res.status(406).send({ message: 'Unproccessable data' });

        }else{
            await dmsCheckIn(req.body);
            async function dmsCheckIn(bodyData) {
            try {
                
                let findDoc = { 'dmsSession': bodyData.dmsSessionId, 'documentNo': bodyData.documentNo, 'comments': bodyData.comments };
                let fetchRes = await fetch(config.dms.url + 'checkInDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) });
                let checkInResult = await fetchRes.json();
                console.log('checkInResult', checkInResult)
                if(checkInResult.errorCode != '0' && checkInResult.errorCode != '-7440'){
                   
                    console.error(checkInResult.message)
                    res.send(checkInResult);
                }else if(checkInResult.errorCode =='-7440'){
                            console.error('::DMS SESSION EXPIRED')
                           var i = 1;
                            while (i <= 4) {
                                console.log('******i', i)
                                if(bodyData.checkedInBy ==''){ 
                                    console.error('::CheckIn By missing')
                                    res.status(406).send({message:'username should not be null'});
                                    break;
                                };
                                let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData.checkedInBy, repositoryName: config.dms.dmsRepositoryName }
                                let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                let connectionResult = await connectionResponse.json();
                               if (connectionResult.errorCode == 0 && i <= 3) {
                                    console.error('::new dms session established')
                                    req.body.dmsSessionId = connectionResult.dmsSession;
                                    await dmsCheckIn(req.body);
                                    break;
                                }
                                if (i == 4) {
                                    console.error('::failed to generate new session')
                                    res.status(403).send({message:'failed to create new session or username missing'});
                                }
                                i++;
                            }
            
            
                }else{
                    
                     let updateResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: bodyData.documentNo }, { checkoutStatus: 0 }, { new: true });
                     if(updateResponse !=null){
                        console.error('::CheckIn successfully...')
                        res.send(checkInResult);
                        
                     }else{
                        console.error('::Failed to update record in collection...')
                        res.send({message:'record not found to update'})
                     }
                     
                }
 
            } catch (e) {
                res.send(e)
            }
        }
        }
        
    } catch (e) {
        console.error(e);
        res.status(400).send(e);

    }


    
}
//old api
// dmsController.checkIn = async (req, res) => {
//     try {
//         let findDoc = { 'dmsSession': req.body.dmsSessionId, 'documentNo': req.body.documentNo, 'comments': req.body.comments };
//         let fetchRes = await fetch(config.dms.url + 'checkInDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) });
//         let checkInResult = await fetchRes.json();
//         if (checkInResult.errorCode == 0) {
//             let updateData = {
//                 checkoutStatus: 0,
//             };
//             let updateResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: req.body.documentNo }, updateData, { new: true });
//             console.log('updateResponse', updateResponse)
//         }
//         res.status(200).send(checkInResult);
//     } catch (e) {
//         console.error(e);
//         res.status(400).send(e);

//     }
// }


dmsController.checkInOutHistory = async (req, res) => {
    try {

        // var result = await documents.documentsInfo.findOne({ $and: [{ routerLink:req.body.routerLink, subModule:req.body.subModule, module:req.body.module,file:req.body.file }] },{documentNo:1});
        let findDoc = { "dmsSession": req.body.dmsSessionId, "documentNo": req.body.documentNo };
        let fetchRes = await fetch(config.dms.url + 'checkinoutVersionDocumentHistory', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) });
        let documentHistory = await fetchRes.json();
        res.status(200).send(documentHistory)

    } catch (e) {

    }
}

dmsController.globalSearch = async (req, res) => {
    try {
        // console.log('REQ : ', req.body);
        console.error('::GlobalSearch Api Called');

        if(req.body.dmsSessionId =='' || req.body.searchQuery ==''|| req.body.searchBy ==''){
            console.error('::Unproccessable Data')
            res.status(406).send({ message: 'Unproccessable data' });

        }else{
                let docData = [];
                await globalSearchByDms(req.body);
                async function globalSearchByDms(bodyData){
                    try {
                var searchQuery = bodyData.searchQuery;

                req.body = bodyData;     
                        //Content search
                let findDoc = { "dmsSession": req.body.dmsSessionId, "searchQuery": searchQuery };
                let fetchRes = await fetch(config.dms.url + 'contentSearch', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) }).catch(err => console.error('error:', err));;
                let contentSearchResultByDms = await fetchRes.json();
                console.log('contentSearchResultByDms', contentSearchResultByDms)
                if (contentSearchResultByDms.documents) {
                    for (const doc of contentSearchResultByDms.documents) {
                        let dataforContentSearch = await documents.documentsInfo.findOne({ $and: [{ documentNo: doc['documentNo'] }] });
                        // console.log('****************dataforContentSearch', dataforContentSearch)
                        if (dataforContentSearch != null) {
                            docData.push(dataforContentSearch)
                        }
                    }
                    // console.log('*******************docData', docData)
                }

                //Meta-data search
                let findDocs = { "dmsSession": req.body.dmsSessionId, "searchQuery": req.body.moduleName };
                let fetchResp = await fetch(config.dms.url + 'searchDocuments', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDocs) }).catch(err => console.error('error:', err));;
                // console.log('fetchResp', fetchResp)
                let metaDataSearchResultByDms = await fetchResp.json();
                 console.log('metaDataSearchResultByDms', metaDataSearchResultByDms)
                if (metaDataSearchResultByDms.documents) {
                    for (const doc of metaDataSearchResultByDms.documents) {
                        let dataforMetadataSearch = await documents.documentsInfo.findOne({ $and: [{ documentNo: doc['documentNo'] }] });
                        if (dataforMetadataSearch != null) {
                            if (docData.includes(!dataforMetadataSearch._id)) {
                                docData.push(dataforMetadataSearch)
                            }
                        }
                    }

                }

                if(contentSearchResultByDms.errorcode == '-7440' || metaDataSearchResultByDms.errorCode =='-7440'){
                    
                    console.error('::DMS SESSION EXPIRED')
                                var i = 1;
                                    while (i <= 4) {
                                        console.log('******i', i)
                                        if(bodyData.searchBy ==''){ 
                                            console.error('::Search By missing')
                                            res.status(406).send({message:'username should not be null'});
                                            break;
                                        };
                                        let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData.searchBy, repositoryName: config.dms.dmsRepositoryName }
                                        let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                        let connectionResult = await connectionResponse.json();
                                    if (connectionResult.errorCode == 0 && i <= 3) {
                                            console.error('::new dms session established')
                                            req.body.dmsSessionId = connectionResult.dmsSession;
                                            await globalSearchByDms(req.body)
                                            break;
                                        }
                                        if (i == 4) {
                                            console.error('::failed to generate new session')
                                            res.status(403).send({message:'failed to create new session or username missing'});
                                        }
                                        i++;
                                    }
                    
                    
                }else if(!contentSearchResultByDms.documents &&  !metaDataSearchResultByDms.documents){
                    
                    res.send(contentSearchResultByDms)
                }else{
                    let docResp = await documents.documentsInfo.find({ fileName: { $regex: new RegExp(searchQuery, "i") } });

                if (docResp) {
                    docResp.forEach(ele => {
                        docData.push(ele);
                    });

                }
                }

                
                    } catch (e) {
                        
                    }
                }
                
                // console.log('docData', docData)
                res.status(200).send(docData);

    }
    } catch (e) {
        console.error(e)
    }
}









// dmsController.globalSearch=async(req, res)=>{
//     try {
//         console.log('REQ : ' ,req.body);

//         var searchQuery = req.body.searchQuery;
//         let docData=[];
//         docData = await documents.documentsInfo.find({ fileName:{$regex:new RegExp(searchQuery, "i")}});
//         console.log('docData BY mongo', docData)

//         //Content search
//         let findDoc = { "dmsSession": req.body.dmsSessionId, "searchQuery": searchQuery };
//         let fetchRes = await fetch(config.dms.url + 'contentSearch', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) }).catch(err => console.error('error:', err));;
//         let contentSearchResultByDms = await fetchRes.json();
//         if (contentSearchResultByDms.documents) {
//             for (const doc of contentSearchResultByDms.documents) {
//                 let dataforContentSearch = await documents.documentsInfo.findOne({ $and: [{ documentNo: doc['documentNo'], moduleName: doc['metadata'][0]['Module-Name'] }] });
//                 if (dataforContentSearch != null) {
//                     if( docData.includes(!dataforContentSearch._id) ){
//                         docData.push(dataforContentSearch)
//                     }
//                 }
//             }

//         }

//         //Meta-data search
//         let findDocs = { "dmsSession": req.body.dmsSessionId, "searchQuery": req.body.moduleName };
//         let fetchResp = await fetch(config.dms.url + 'searchDocuments', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDocs) }).catch(err => console.error('error:', err));;
//         let metaDataSearchResultByDms = await fetchResp.json();
//         if (metaDataSearchResultByDms.documents) {
//             for (const doc of metaDataSearchResultByDms.documents) {
//                 let dataforMetadataSearch = await documents.documentsInfo.findOne({ $and: [{ documentNo: doc['documentNo'], moduleName: doc['metadata'][0]['Module-Name'] }] });
//                 if (dataforMetadataSearch != null) {
//                     if( docData.includes(!dataforMetadataSearch._id) ){
//                         docData.push(dataforMetadataSearch)
//                     }
//                 }
//             }

//         }
//         console.log('docData', docData)
//         res.status(200).send(docData);


//     } catch (e) {
//         console.error(e)
//     }
// }



//New Api
dmsController.renameFile = async (req, res) => {
    try {
        console.error('::Rename Api Called');

        if(req.body.dmsSessionId =='' || req.body.documentNo ==''|| req.body.fileName ==''||req.body.renameBy ==''){
            console.error('::Unproccessable Data')
            res.status(406).send({ message: 'Unproccessable data' });

        }else{
            await dmsRenameFile(req.body);
            async function dmsRenameFile(bodyData) {
            try {
                
                let findDoc = { 'dmsSession': bodyData.dmsSessionId, 'documentNo': bodyData.documentNo,'fileName': bodyData.fileName, 'comments': bodyData.comments };
                let fetchRes = await fetch(config.dms.url + 'renameFile', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) }).catch(err => console.error('error:', err));;
                let renameResponse = await fetchRes.json();
                console.log('renameResponse', renameResponse)
                if(renameResponse.errorCode != '0' && renameResponse.errorCode != '-7440'){
                    console.error(renameResponse.message)
                    res.send(renameResponse);
                }else if(renameResponse.errorCode =='-7440'){
                            console.error('::DMS SESSION EXPIRED')
                           var i = 1;
                            while (i <= 4) {
                                console.log('******i', i)
                                if(bodyData.renameBy ==''){ 
                                    console.error('::Rename By missing')
                                    res.status(406).send({message:'username should not be null'});
                                    break;
                                };
                                let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData.renameBy, repositoryName: config.dms.dmsRepositoryName }
                                let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                let connectionResult = await connectionResponse.json();
                               if (connectionResult.errorCode == 0 && i <= 3) {
                                    console.error('::new dms session established')
                                    req.body.dmsSessionId = connectionResult.dmsSession;
                                    await dmsRenameFile(req.body);
                                    break;
                                }
                                if (i == 4) {
                                    console.error('::failed to generate new session')
                                    res.status(403).send({message:'failed to create new session or username missing'});
                                }
                                i++;
                            }
            
            
                }else{
                    
                     let updateResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: bodyData.documentNo }, {fileName: bodyData.fileName,}, { new: true });
                     if(updateResponse !=null){
                        console.error('::Rename file successfully...')
                        res.send(renameResponse);
                        
                     }else{
                        console.error('::Failed to update record in collection...')
                        res.send({message:'record not found to update'})
                     }
                     
                }
 
            } catch (e) {
                res.send(e)
            }
        }
        }
        
    } catch (e) {
        console.error(e);
        res.status(400).send(e);

    }


    // try {
    //     let data = {
    //         "dmsSession": req.body.dmsSessionId,
    //         "documentNo": req.body.documentNo,
    //         "fileName": req.body.fileName,
    //         "comments": req.body.comments
    //     }
    //     let fetchRes = await fetch(config.dms.url + 'renameFile', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(err => console.error('error:', err));;
    //     let renameResponse = await fetchRes.json();
    //     console.log('renameResponse', renameResponse)
    //     if (renameResponse.errorCode === 0) {
    //         let updateData = {
    //             fileName: req.body.fileName,
    //         };
    //         let documentRepositoryResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: req.body.documentNo }, updateData, { new: 1 })
    //         console.log('documentRepositoryResponse', documentRepositoryResponse)
    //     }

    //     res.status(200).send(renameResponse)
    // } catch (e) {
    //     console.error(e)
    //     res.status(500).send(e);
    // }
}

//Old api
// dmsController.renameFile = async (req, res) => {
//     try {
//         let data = {
//             "dmsSession": req.body.dmsSessionId,
//             "documentNo": req.body.documentNo,
//             "fileName": req.body.fileName,
//             "comments": req.body.comments
//         }
//         let fetchRes = await fetch(config.dms.url + 'renameFile', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(err => console.error('error:', err));;
//         let renameResponse = await fetchRes.json();
//         console.log('renameResponse', renameResponse)
//         if (renameResponse.errorCode === 0) {
//             let updateData = {
//                 fileName: req.body.fileName,
//             };
//             let documentRepositoryResponse = await documents.documentsInfo.findOneAndUpdate({ documentNo: req.body.documentNo }, updateData, { new: 1 })
//             console.log('documentRepositoryResponse', documentRepositoryResponse)
//         }

//         res.status(200).send(renameResponse)
//     } catch (e) {
//         console.error(e)
//         res.status(500).send(e);
//     }
// }

//NEW API
dmsController.deleteDocument = async (req, res) => {
    try {
        console.error('::Delete Api Called');

        if(req.body.dmsSessionId =='' || req.body.documentNo ==''|| req.body.fileName ==''||req.body.deletedBy ==''){
            console.error('::Unproccessable Data')
            res.status(406).send({ message: 'Unproccessable data' });

        }else{
            await dmsDeleteFile(req.body);
            async function dmsDeleteFile(bodyData) {
            try {
                
                let findDoc = { 'dmsSession': bodyData.dmsSessionId, 'documentNo': bodyData.documentNo,'fileName': bodyData.fileName, 'comments': bodyData.comments };
                let fetchRes = await fetch(config.dms.url + 'deleteDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) }).catch(err => console.error('error:', err));;
                let deleteResponse = await fetchRes.json();
                console.log('deleteResponse', deleteResponse)
                if(deleteResponse.errorCode != '0' && deleteResponse.errorCode != '-7440'){
                    console.error(deleteResponse.message)
                    res.send(deleteResponse);
                }else if(deleteResponse.errorCode =='-7440'){
                            console.error('::DMS SESSION EXPIRED')
                           var i = 1;
                            while (i <= 4) {
                                console.log('******i', i)
                                if(bodyData.deletedBy ==''){ 
                                    console.error('::Delete By missing')
                                    res.status(406).send({message:'username should not be null'});
                                    break;
                                };
                                let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData.deletedBy, repositoryName: config.dms.dmsRepositoryName }
                                let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                let connectionResult = await connectionResponse.json();
                               if (connectionResult.errorCode == 0 && i <= 3) {
                                    console.error('::new dms session established')
                                    req.body.dmsSessionId = connectionResult.dmsSession;
                                    await dmsDeleteFile(req.body);
                                    break;
                                }
                                if (i == 4) {
                                    console.error('::failed to generate new session')
                                    res.status(403).send({message:'failed to create new session or username missing'});
                                }
                                i++;
                            }
            
            
                }else{
                    
                    let documentRepositoryResponse = await documents.documentsInfo.findOneAndDelete({$and:[{documentNo:req.body.documentNo}] });
                     if(documentRepositoryResponse !=null){
                        console.error(':: file Deleted successfully...')
                        res.send(deleteResponse);
                        
                     }else{
                        console.error('::Failed to update record in collection...')
                        res.send({message:'record not found to update'})
                     }
                     
                }
 
            } catch (e) {
                res.send(e)
            }
        }
        }
        
    } catch (e) {
        console.error(e);
        res.status(400).send(e);

    }

}

//old api
// dmsController.deleteDocument = async (req, res) => {
//     try {
//         let data = {
//             "dmsSession": req.body.dmsSessionId,
//             "documentNo": req.body.documentNo,
//             "fileName": req.body.fileName,
//             "comments": req.body.comments
//         }
//         let fetchRes = await fetch(config.dms.url + 'deleteDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(e => res.send(e));;
//         let deleteResponse = await fetchRes.json();
//         console.log('deleteResponse', deleteResponse)
//         if (deleteResponse.errorCode === 0) {
//             let documentRepositoryResponse = await documents.documentsInfo.findOneAndDelete({ $and: [{ documentNo: req.body.documentNo, _id: req.body._id }] });
//             console.log('documentRepositoryResponse', documentRepositoryResponse)
//         }

//         res.status(200).send(deleteResponse);
//     } catch (e) {
//         console.error(e);
//         res.status(500).send(e);
//     }
// }

dmsController.fileSearch = async (req, res) => {
    try {

        let docData = await documents.documentsInfo.find({ fileName: { $regex: new RegExp(req.body.searchQuery, "i") } });
        // let docData = await documents.documentsInfo.find({ fileName: { $regex: "/"+req.body.searchQuery+"/" } });

        console.log('docData', docData)
        res.send(docData)

    } catch (e) {
        console.error(e);
        res.send(e)
    }
}

dmsController.contentSearch = async (req, res) => {
    try {
        // var result = await documents.documentsInfo.findOne({ $and: [{ routerLink:req.body.routerLink, subModule:req.body.subModule, module:req.body.module,file:req.body.file }] },{documentNo:1});
        let findDoc = { "dmsSession": req.body.dmsSessionId, "searchQuery": req.body.searchQuery };
        let fetchRes = await fetch(config.dms.url + 'contentSearch', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(findDoc) }).catch(err => console.error('error:', err));;
        let contentSearchResultByDms = await fetchRes.json();
        console.log('contentSearchResultByDms', contentSearchResultByDms)
        let docArr = [];
        if (contentSearchResultByDms.documents) {

            for (const doc of contentSearchResultByDms.documents) {

                let docData = await documents.documentsInfo.findOne({ $and: [{ documentNo: doc['documentNo'] }] });

                if (docData != null) {
                    docArr.push(docData)
                }

            }
            Promise.all(docArr)
                .then((result) => {
                    res.status(200).send(result);
                }).catch((e) => {
                    console.error(e);
                    res.status(500).send(e);
                });
        } else {
            res.status(200).send(contentSearchResultByDms)

        }




    } catch (e) {
        res.status(500).send(e);
    }
}

dmsController.dmsConnect = async (req, res) => {
    try {
        var dmsResp = await DMSConnection();
        console.log('dmsResp', dmsResp)
        res.status(200).send(dmsResp);
    } catch (e) {
        res.status(400).send(e);
    }

}

dmsController.dmsDisConnect = async (req, res) => {
    try {
        var dmsResp = await DMSConnection();
        console.log('dmsResp', dmsResp)
        var disConnectResp = await DMSDisconnection(dmsResp.dmsSession)
        console.log('disConnectResp', disConnectResp)
        res.status(200).send(disConnectResp);
    } catch (e) {
        res.status(400).send(e);
    }

}


dmsController.saveAsDocument = async (req, res) => {
    try {
        var data = '';
        let response = [];
        // console.log('BODY :', req.body);

        if (req.body.items.length > 0) {
            var saveAsResponseArray =[];
            await dmsSaveAs(req.body.items);
            async function dmsSaveAs(bodyData){
                try {
                    for (const ele in bodyData) {
                        console.log('bodyData', bodyData[ele]);
                            data = {
                                "dmsSession": bodyData[ele].dmsSessionId,
                                "documentNo": bodyData[ele].documentNo,
                                "nodeId": "/NEW1",
                                "metaData": bodyData[ele].metaData,
                                "fileName": bodyData[ele].fileName
                            }
            
                            let fetchRes = await fetch(config.dms.url + 'saveAsDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(err => console.error('error:', err));;
                            let saveAsResponse = await fetchRes.json();
                            console.log('******************saveAsResponse', saveAsResponse)
                            if(saveAsResponse.status != '0' && saveAsResponse.status != '-7440'){
                                console.log('other errors occured.....')

                                console.error(saveAsResponse.message)
                                //   res.send(saveAsResponse);
                            }else if(saveAsResponse.status =='-7440'){
                                console.log('SESSION EXPIRED.....')
                                        console.error('::DMS SESSION EXPIRED')
                                       var i = 1;
                                        while (i <= 4) {
                                            console.log('***ele.saveAsBy', bodyData[ele].saveAsBy)
                                            console.log('******i', i)
                                            if(bodyData[ele].saveAsBy ==''){ 
                                                
                                                console.error('::Delete By missing')
                                                res.status(406).send({message:'username should not be null'});
                                                break;
                                            };
                                            let data = { sessionId: config.dms.connectionOptions.sessionId, userName: bodyData[ele].saveAsBy, repositoryName: config.dms.dmsRepositoryName }
                                            let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
                                            let connectionResult = await connectionResponse.json();
                                            console.log('*****connectionResult', connectionResult)
                                           if (connectionResult.errorCode == 0 && i <= 3) {
                                                console.error('::new dms session established')
                                                console.log('I element of for loop',bodyData[ele]);
                                                bodyData[ele].dmsSessionId = connectionResult.dmsSession;
                                                console.log('**********ele.dmsSessionId', ele.dmsSessionId);
                                                data = {
                                                    "dmsSession": bodyData[ele].dmsSessionId,
                                                    "documentNo": bodyData[ele].documentNo,
                                                    "nodeId": "/NEW1",
                                                    "metaData": bodyData[ele].metaData,
                                                    "fileName": bodyData[ele].fileName
                                                }
                                
                                                let fetchRes = await fetch(config.dms.url + 'saveAsDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(err => console.error('error:', err));;
                                                let saveAsResponse = await fetchRes.json();
                                                bodyData[ele].documentNo = await saveAsResponse.documentNo;
                                                bodyData[ele].urlToken = await saveAsResponse.urlToken;
                                                let rd = new documents.documentsInfo(bodyData[ele]);
                                                let rd1 = await rd.save();
                                                console.log('rd1', rd1)
                                                saveAsResponseArray.push(rd1)
                                                break;
                                            }
                                            if (i == 4) {
                                                console.error('::failed to generate new session')
                                                res.status(403).send({message:'failed to create new session or username missing'});
                                                break;
                                            }
                                            i++;
                                        }
                        
                        
                            }else{
                               
                                bodyData[ele].documentNo = await saveAsResponse.documentNo;
                                bodyData[ele].urlToken = await saveAsResponse.urlToken;
                                let rd = new documents.documentsInfo(bodyData[ele]);
                                let rd1 = await rd.save();
                                console.log('rd1', rd1)
                                saveAsResponseArray.push(rd1);

                            }
                        
                    }  
                } catch (e) {
                    res.send(e);
                }
           
            }
           

            Promise.all(saveAsResponseArray)
                .then((result) => {
                    res.status(200).send(result);

                    console.log('All dms files updated in collection ...!', result)

                }).catch((e) => {
                    console.error(e);

                    res.status(500).send(e);
                });
        } else {
            res.status(200).send({ message: 'No document found for save' })
        }




    } catch (e) {
        console.error(e)
        // res.send(e);
    }
}

//old api
// dmsController.saveAsDocument = async (req, res) => {
//     try {
//         var data = '';
//         let response = [];
//         console.log('BODY :', req.body);

//         if (req.body.items.length > 0) {
//             for (const ele of req.body.items) {
//                 data = {
//                     "dmsSession": ele.dmsSessionId,
//                     "documentNo": ele.documentNo,
//                     "nodeId": "/NEW1",
//                     "metaData": ele.metaData,
//                     "fileName": ele.fileName
//                 }

//                 let fetchRes = await fetch(config.dms.url + 'saveAsDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) }).catch(err => console.error('error:', err));;
//                 let saveAsResponse = await fetchRes.json();
//                 ele.documentNo = await saveAsResponse.documentNo;
//                 ele.urlToken = await saveAsResponse.urlToken;
//                 let rd = new documents.documentsInfo(ele);
//                 let rd1 = await rd.save();
//                 response.push(rd1);
//             }

//             Promise.all(response)
//                 .then((result) => {
//                     res.status(200).send(statuses[200]);

//                     console.log('All dms files updated in collection ...!', result)

//                 }).catch((e) => {
//                     console.error(e);

//                     res.status(500).send(e);
//                 });
//         } else {
//             res.status(200).send({ message: 'No document found for save' })
//         }




//     } catch (e) {
//         console.error(e)
//         res.send(e);
//     }
// }
dmsController.getAll = (req, res) => {
    try {
        documents.documentsInfo.find(function (err, docs) {
            if (err) {
                res.send(err);
            }
            else {
                res.send(docs);
            }
        }).select(' _id metadata file fileName urlToken');
    } catch (error) {
        res.status(500).send(error);
    }
}

dmsController.checkFileExist = async (req, res) => {
    try {
        if (req.body.auditId) {
            let auditDocs = await documents.documentsInfo.findOne({ auditId: req.body.auditId });
            if (auditDocs) {
                let docs = await documents.documentsInfo.findOne({ $and: [{ auditId: req.body.auditId, fileName: req.body.fileName, roomType: req.body.roomType }] });

                if (docs) {
                    res.status(200).send({ message: 'filename already exist', data: docs });
                } else {
                    res.status(200).send({ message: 'filename not found' });
                }
            } else {
                res.status(200).send({ message: 'audit does not exist' });
            }


        } else {
            res.send({ message: 'please select audit or file name' });
        }
    } catch (e) {
        console.error(e);
        res.send(e);
    }
}







const getByteArray = async (base64) => {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
    // let fileData = fs.readFileSync(filePath).toString('hex');
    // let result = []
    // for (var i = 0; i < fileData.length; i+=2)
    //   result.push('0x'+fileData[i]+''+fileData[i+1])
    // return result;
}

const connectDms = async (data) => {
    try {
        let connectiondata = data;

        let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(connectiondata) });
        let result = await connectionResponse.json();

        return result;
    } catch (e) {
        return res.status(500).send(e);
    }

}

const DMSConnection = async () => {
    try {
        let connectionResponse = await fetch(config.dms.url + 'dmsConnect', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(config.dms.connectionOptions) });
        let result = await connectionResponse.json();
        console.log('result', result)
        return result;
    } catch (e) {
        return res.status(500).send(e);
    }

}

const DMSDisconnection = async (sessionId) => {
    try {
        let data = { "dmsSession": sessionId }
        let disConnectResponse = await fetch(config.dms.url + 'dmsCloseConnection', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
        let result = await disConnectResponse.json();
        return result;
    } catch (e) {
        return e;
    }

}

dmsController.uploadVideoUrl = async (req, res) => {
    try {
        process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
        var videoFileName = req.body.videoUrl.split('/').slice(-1)[0];

        // const file = fs.createWriteStream(videoFileName);
        // const request = https.get(req.body.videoUrl, function (response) {
        // response.pipe(file);
        // file.on('finish', async function () {


        var dmsResult = await uploadVideoToDMS(req.body.videoUrl, req.body.dmsSessionId, videoFileName);

        await addVideoTodocumentTable(req.body.username, req.body.auditId, dmsResult, videoFileName);
        // res.send('Done');
        return true;
        // });
        // });
    } catch (error) {
        return false;
        // res.status(500).send(error.message);
    }


    async function uploadVideoToDMS(fileUrl, dmsSessionId, fileName) {
        console.log('*****INSIDE FUNCTION')
        // for (const element of uploadInfo) {
        let encodedData = fs.readFileSync(fileUrl).toString("base64");
        var data = {

            "dmsSession": dmsSessionId,
            "nodeId": "/NEW",
            "metaData": "Module-Name=In-Tray,Feature-Name=Video-Chat,Doc-Type=DATAFILE,Sequence-No=100016",
            "attachments": [{ "filename": fileName, "data": encodedData, "doctype": "DEFAULT" }]
        }

        let fetchRes = await fetch(config.dms.url + 'uploadDocument', { method: 'POST', headers: { 'Content-Type': 'application/json;charset=utf-8' }, body: JSON.stringify(data) });
        console.log('fetchRes', fetchRes)
        //delete files from node server 
        // fs.unlink(fileUrl, (err) => {
        //     if (err) throw err;
        //     console.log(fileUrl + 'was deleted');
        // });
        let result = await fetchRes.json();
        return result;
    }

    async function addVideoTodocumentTable(username, auditId, dmsResult, fileName) {

        let rd = new documents.documentsInfo({
            urlToken: dmsResult.urlToken,
            documentNo: dmsResult.documentNo,
            fileName: fileName,
            checkedinBy: username,
            routerLink: '',
            subModuleName: 'In-Tray',
            auditYear: 'Year',
            auditId: auditId,
            moduleName: 'Video-Chat',
            checkedinDate: new Date(),
            metadata: 'Module-Name=Collaboration,Feature-Name=Video_Chat,Doc-Type=DATAFILE,Sequence-No=100019'
        });
        let rd1 = await rd.save();

        console.log('rd1', rd1)
        // }/
    }
}

module.exports = dmsController
