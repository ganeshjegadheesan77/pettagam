const controlTestingDetail = require("../../models/controlTesting/controlTesting.schema");
const issueDetail = require("../../models/IssueActions/issue.schema");

const controlTestingController = {};

controlTestingController.getOneControlTesting = async (req, res) => {
  try {
    controlTestingDetail.ct
      .findOne({ ctlTestingId: req.body.ctlTestingId })
      .populate("controlId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};
//updateOneControlTesting
controlTestingController.updateOneControlTesting = async (req, res) => {
  try {
    controlTestingDetail.ct.updateOne(
      { ctlTestingId: req.body.ctlTestingId },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          console.log("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlTestingController.getAllControlTestRecords = (req, res) => {
  try {
    controlTestingDetail.ct.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlTestingController.saveControlTest = async (req, res) => {
  console.log("Body: ", req.body);
  let rd = await new controlTestingDetail.ct(req.body);
  await rd.save((err, controlTestingDetail) => {
    if (err) {
      res.json(err);
    } else {
      console.log(controlTestingDetail);
      res.json(controlTestingDetail);
    }
  });
};

controlTestingController.deleteCTFile = async (req, res) => {
  try {
    controlTestingDetail.ct.updateOne(
      { ctlTestingId: req.body.ctlTestingId },
      { $pull: { attachment: req.body.fileName } },
      function (err, doc) {
        if (err) {
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          console.log("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};

async function processArray(a, issueId) {
  var res = [];
  for (const item of a) {
    var b = await issueDetail.issue.find({ actionId: item, issueId: { $ne: issueId } }).select(" issueId ");
    if (b.length == 0) {
      res.push(item);
    }
  }
  return res;
}

controlTestingController.deleteIssueFromControlTesting = async (req, res) => {
  try {
    if (req.body.actionIds && req.body.actionIds.length > 0) {
      var ress = await processArray(req.body.actionIds, req.body.issueId);

      var controlTesting = await controlTestingDetail.ct.findOne({ "ctlTestingId": req.body.ctlTestingId }).select(" ctlTestingId actionId issueId");
      controlTesting.issueId.splice(controlTesting.issueId.indexOf(req.body.issueId), 1);
      if (ress && ress.length > 0) {
        ress.forEach(actionId => {
          controlTesting.actionId.splice(controlTesting.actionId.indexOf(actionId), 1);
        });
      }

      controlTesting.save()
      .then((doc) => {
         res.send(doc);
      }).catch((e) => {
        return res.status(406).send(e);
      });
      // res.send(controlTesting);
    }
  } catch (e) {
    return res.status(406).send(e);
  }
};

module.exports = controlTestingController;
