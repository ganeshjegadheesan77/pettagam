const auditMasterSchema = require('../../../models/enia/auditMaster.schema').auditMasterSchema;
const logger = require("../../../utils/logger").logger;
const rsHelper = require("../../../utils/responseHelper");
const _ = require('lodash');
const issueSchema = require("../../../models/IssueActions/issue.schema").issue;
const actionSchema = require("../../../models/IssueActions/actions.schema").action;

const auditMasterController = {};

auditMasterController.getAudit = async (req, res) => {
    try {
        var audit = await auditMasterSchema.findOne({ auditId: req.params.auditId }, { auditId: 1, category: 1, auditableEntity: 1, year: 1, rotation: 1, isRemote: 1, createdAt: 1, updatedAt: 1, createdBy: 1 });
        if (audit) {
            rsHelper.manage(res, null, audit, 200, "Audit details");
        } else {
            rsHelper.manage(res, null, null, 404, "No Audit Found");
        }
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.createAudit = async (req, res) => {
    try {
        var exists = await checkIfAuditExists(req.body.auditableEntity, req.body.category.levelFive.data, req.body.category.businessProcess);
        if (!exists) {
            var lastAUditId = await getLastAuditId();
            var auditNumber = (lastAUditId + 1);
            var auditId = "OP-0" + auditNumber;

            var am = new auditMasterSchema({
                auditId: auditId,
                auditNumber: auditNumber,
                auditableEntity: req.body.auditableEntity,
                rotation: req.body.rotation,
                year: req.body.year,
                isRemote: req.body.isRemote,
                isActive: false,
                isDuplicated: false,
                isCompleted: false,
                category: req.body.category,
                createdBy: req.body.username
            });

            am.save().then((doc) => {
                rsHelper.manage(res, null, doc, 200, "Audit created successfully");
            }).catch((err) => {
                rsHelper.manage(res, err, null, 500, "Database Exception Occured");
            })
        }
        else {
            rsHelper.manage(res, null, null, 400, "Audit already exists");
        }
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

const getLastAuditId = () => {
    return auditMasterSchema.find({}, { auditNumber: 1, _id: 0 }).sort({ auditNumber: -1 }).limit(1)
        .then((doc) => {
            if (doc && doc.length > 0) {
                return doc[0].auditNumber;
            }
            return 0;
        }).catch((err) => {
            throw err;
        })
}

const checkIfAuditExists = (auditableEntity, site, process) => {
    return auditMasterSchema
        .count({ auditableEntity: auditableEntity, "category.levelFive.data": site, "category.businessProcess": process }, function (err, count) {
            if (err) {
                throw err;
            }
            if (count > 0) {
                return true;
            }
            return false;
        });
}

auditMasterController.editAudit = async (req, res) => {
    try {
        var canEdit = await canAuditBeAltered(req.body.auditId);
        if (canEdit) {
            auditMasterSchema.findOneAndUpdate({ auditId: req.body.auditId }, { rotation: req.body.rotation, isRemote: req.body.isRemote }, { new: true })
                .then((doc) => {
                    if (doc) {
                        rsHelper.manage(res, null, doc, 200, "Audit updated successfully");
                        return;
                    }
                    rsHelper.manage(res, null, null, 500, "Error occured ! Please try again");
                })
                .catch((err) => {
                    rsHelper.manage(res, err, null, 500, "Database Error occured ! Please try again");
                })
        }
        else {
            rsHelper.manage(res, null, null, 400, "Team set ! cannot edit Audit");
        }
        // var universe = await universeSchema.find();
        // rsHelper.manage(res, null, universe, 200, "Audit Masters found successfully");
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.deleteAudit = async (req, res) => {
    try {
        var canDelete = await canAuditBeAltered(req.params.auditId);
        if (canDelete) {
            auditMasterSchema.findOneAndDelete({ auditId: req.params.auditId })
                .then((doc) => {
                    if (doc) {
                        rsHelper.manage(res, null, doc, 200, "Audit deleted successfully");
                        return;
                    }
                    rsHelper.manage(res, null, null, 500, "Error occured ! Please try again");
                })
                .catch((err) => {
                    rsHelper.manage(res, err, null, 500, "Database Error occured ! Please try again");
                })
        }
        else {
            rsHelper.manage(res, null, null, 400, "Team set ! cannot delete Audit");
        }
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

const canAuditBeAltered = (auditId) => {
    return auditMasterSchema.findOne({ auditId: auditId }, { auditId: 1, teamAndTimeline: 1 })
        .then((doc) => {
            if (!doc) {
                throw new Error("audit not found to edit / delete");
            }
            if (doc.teamAndTimeline && doc.teamAndTimeline.isCompleted && doc.teamAndTimeline.isCompleted === true) {
                return false;
            }
            return true;
        })
        .catch((err) => {
            throw err;
        })
}

auditMasterController.updateStartDate = async (req, res) => {
    try {
        var startDate = new Date(req.body.startDate);
        auditMasterSchema.findOneAndUpdate({ auditId: req.body.auditId }, { startDate: startDate }, { new: true })
            .then((doc) => {
                if (doc) {
                    rsHelper.manage(res, null, doc, 200, "Start Date updated successfully");
                    return;
                }
                rsHelper.manage(res, null, null, 500, "Error occured ! Please try again");
            })
            .catch((err) => {
                rsHelper.manage(res, err, null, 500, "Database Error occured ! Please try again");
            })
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.updateConfirmToggle = async (req, res) => {
    try {
        auditMasterSchema.findOneAndUpdate({ auditId: req.body.auditId }, { isActive: req.body.isActive }, { new: true })
            .then((doc) => {
                if (doc) {
                    rsHelper.manage(res, null, doc, 200, "Confirm toggle updated successfully");
                    return;
                }
                rsHelper.manage(res, null, null, 500, "Error occured ! Please try again");
            })
            .catch((err) => {
                rsHelper.manage(res, err, null, 500, "Database Error occured ! Please try again");
            })
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.addToCurrentYear = async (req, res) => {
    try {
        var oldAudit = await auditMasterSchema.findOne({ auditId: req.body.auditId })
            .select(' -teamAndTimeline -openingMeeting -planningMemorandum -auditAnnouncement -auditPbc -closingMeeting -draftReport -issueIds ');

        var currentDate = new Date();
        var lastAUditId = await getLastAuditId();
        var auditNumber = (lastAUditId + 1);
        var auditId = "OP-0" + auditNumber;

        var am = new auditMasterSchema({
            auditId: auditId,
            auditNumber: auditNumber,
            auditableEntity: oldAudit.auditableEntity,
            rotation: oldAudit.rotation,
            year: currentDate.getFullYear(),
            isRemote: oldAudit.isRemote,
            isActive: false,
            isDuplicated: false,
            isCompleted: false,
            category: oldAudit.category,
            createdBy: req.body.username
        });

        am.save().then((doc) => {
            rsHelper.manage(res, null, doc, 200, "Audit added to current year");
        }).catch((err) => {
            rsHelper.manage(res, err, null, 500, "Database Exception Occured");
        })
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.duplicateAudit = async (req, res) => {
    try {
        if (req.body.auditIds && req.body.auditIds.length > 0) {
            var eligibilityList = [];
            for (var auditId of req.body.auditIds) {
                var isEligible = await isEligibleForDuplication(auditId);
                if (isEligible === true) {
                    eligibilityList.push(auditId);

                    await duplicateSingleAudit(auditId, req.body.username);
                }

            }

            if (eligibilityList.length > 0) {
                rsHelper.manage(res, null, null, 200, `${eligibilityList.length} Audits duplicated successfully`)
            }
            else {
                rsHelper.manage(res, null, null, 400, `No Audits duplicated / Not Eligible`);
            }
        }
        else {
            rsHelper.manage(res, null, null, 400, `No Audits duplicated / Not Eligible`);
        }
    } catch (error) {
        logger.pushError(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

const isEligibleForDuplication = async (auditId) => {
    var currentDate = new Date()
    var audit = await auditMasterSchema.findOne({ auditId: auditId }, { auditId: 1, category: 1, auditableEntity: 1, year: 1, isCompleted: 1, rotation: 1 });

    if (audit.year == currentDate.getFullYear() || audit.year + audit.rotation != currentDate.getFullYear()) {
        return false;
    }
    return auditMasterSchema.count({
        auditId: { $ne: auditId },
        auditableEntity: audit.auditableEntity,
        year: currentDate.getFullYear(),
        "category.levelFive.data": audit.category.levelFive.data,
        "category.businessProcess": audit.category.businessProcess
    })
        .then((count) => {
            if (count > 0) {
                return false;
            }
            return true;
        })
        .catch((err) => {
            throw err;
        })
}

const duplicateSingleAudit = async (oldAuditId, username) => {
    try {

        var oldAudit = await auditMasterSchema.findOne({ auditId: oldAuditId })
            .select(' -teamAndTimeline -closingMeeting -draftReport -issueIds ');

        var currentDate = new Date();
        var lastAUditId = await getLastAuditId();
        var auditNumber = (lastAUditId + 1);
        var auditId = "OP-0" + auditNumber;

        var am = new auditMasterSchema({
            auditId: auditId,
            auditNumber: auditNumber,
            auditableEntity: oldAudit.auditableEntity,
            rotation: oldAudit.rotation,
            year: currentDate.getFullYear(),
            isRemote: oldAudit.isRemote,
            isActive: false,
            isDuplicated: true,
            isCompleted: false,
            category: oldAudit.category,
            createdBy: username,
            openingMeeting: {
                auditScope: oldAudit.openingMeeting.auditScope,
                isCompleted: false,
                agenda: oldAudit.openingMeeting.agenda,
                quesAndAns: oldAudit.openingMeeting.quesAndAns,
                attachments: []
            },
            planningMemorandum: {
                attachments: [],
                isCompleted: false,
                auditObjectives: oldAudit.planningMemorandum.auditObjectives,
                auditScope: oldAudit.planningMemorandum.auditScope,
                outOfScope: oldAudit.planningMemorandum.outOfScope,
            },
            auditAnnouncement: {
                attachments: [],
                isCompleted: false,
                text: oldAudit.auditAnnouncement.text,
                text2: oldAudit.auditAnnouncement.text2,
                agenda: oldAudit.auditAnnouncement.agenda,
                closing: oldAudit.auditAnnouncement.closing,
            },
            auditPbc: {
                isCompleted: false,
                auditScope: oldAudit.auditPbc.auditScope,
                documentRequest: oldAudit.auditPbc.documentRequest,
                documentList: []
            }
        });

        oldAudit.auditPbc.documentList.forEach(d => {
            am.auditPbc.documentList.push({
                docRequested: d.docRequested,
                // dueDate
                remarks: "",
                dmsInfo: {},
                isApproved: false,
                isRejected: false,
                isLocked: false
            })
        });

        if (oldAudit.workStepIds && oldAudit.workStepIds.length > 0) {
            var oldWorkSteps = await workStepSchema.find({ workStepId: { $in: oldAudit.workStepIds } }, { workStepId: 1, workStepName: 1, taskDescription: 1, uniqueTemplateId: 1 });
            for (var ows of oldWorkSteps) {
                var lastWorkStepNumber = await getLastWorkStepId();
                var workStepNumber = (lastWorkStepNumber + 1);
                var workStepId = "WS-0" + workStepNumber;
                var ws = new workStepSchema({
                    auditId: auditId,
                    workStepId: workStepId,
                    workStepNumber: workStepNumber,
                    workStepName: ows.workStepName,
                    uniqueTemplateId: ows.uniqueTemplateId,
                    taskDescription: ows.taskDescription,
                    createdBy: username,
                    hasIssues: false,
                    origin: 'duplicate',
                    updatedBy: '',
                    preparer: {
                        username: '',
                        profileUrl: '',
                        status: ''
                    },
                    reviewer: {
                        username: '',
                        profileUrl: '',
                        status: 'assigned'
                    }
                })

                await ws.save();
                am.workStepIds.push(workStepId);
            }
        }

        return am.save().then((doc) => {
            if (doc) {
                return true;
            }
            return false;
            // rsHelper.manage(res, null, doc, 200, "Audit duplicated successfully");
        }).catch((err) => {
            throw err;
            // rsHelper.manage(res, err, null, 500, "Database Exception Occured");
        });
    } catch (error) {
        throw error;
    }
}

const getLastWorkStepId = () => {
    return workStepSchema.find({}, { workStepNumber: 1, _id: 0 }).sort({ workStepNumber: -1 }).limit(1)
        .then((doc) => {
            if (doc && doc.length > 0) {
                return doc[0].workStepNumber;
            }
            return 0;
        }).catch((err) => {
            throw err;
        })
}

const workStepSchema = require('../../../models/enia/workSteps.schema').workStepSchema;

auditMasterController.getTableData = async (req, res) => {
    try {
        var currentDate = new Date();
        auditMasterSchema.find({ "category.levelFive.id": { $in: req.body.levelFiveIds } }, { auditId: 1, category: 1, auditableEntity: 1, year: 1, rotation: 1, isCompleted: 1, startDate: 1, isRemote: 1, isDuplicated: 1, isActive: 1, draftReport: 1 }).lean().exec(function (err, audits) {
            if (audits && audits.length > 0) {
                audits.forEach(a => {
                    a.displayTitle = a.auditableEntity + " - " + a.category.levelFive.data + " - " + a.category.businessProcess;
                });
                var groupedAudits = _.groupBy(audits, function (a) {
                    return a.displayTitle
                });

                var multiAudits = [];
                var currentAudits = [];
                for (var ga of Object.keys(groupedAudits)) {
                    // for (var audit of groupedAudits[ga]) {

                    // }
                    groupedAudits[ga].sort((a, b) => a.year - b.year);

                    //multiYearFilter
                    var forMulti = groupedAudits[ga].filter(a => a.isCompleted == true);
                    if (forMulti && forMulti.length > 0) {
                        var eligible = forMulti.pop();
                        if (forMulti.length > 0) {
                            eligible.oldAuditIds = forMulti.map(a => a.auditId);
                        }
                        else {
                            eligible.oldAuditIds = [];
                            // eligible.lastAuditRating = "NA";
                        }
                        multiAudits.push(eligible);
                    }

                    var forCurrent = groupedAudits[ga].find(a => a.isCompleted == false && a.year == currentDate.getFullYear());
                    if (forCurrent) {
                        currentAudits.push(forCurrent);
                    }
                    // console.log(ga + " -> " + groupedAudits[ga])
                }
                rsHelper.manage(res, null, { multiAudits, currentAudits }, 200, "Audit table data");
                return;
            }
            else {
                if (err) {
                    rsHelper.manage(res, err, null, 500, "Exception Occured");
                    return;
                }
                rsHelper.manage(res, null, null, 404, "No Audits Found");
            }
        })
    } catch (error) {
        rsHelper.manage(res, error, null, 500, "Exception Occured");
    }
}

auditMasterController.getGalaxyData = async (req, res) => {
    var audits = [];

    for (let i = 0; i < req.body.oldAuditIds.length; i++) {
        await auditMasterSchema
            .findOne({ auditId: req.body.oldAuditIds[i] })
            .select(" auditId draftReport.rating issueIds")
            .then(async (audit) => {
                if (audit) {
                    var faudit = {
                        auditId: audit.auditId,
                        rating: audit.draftReport.rating,
                        issues: [],
                    };
                    // audits.push();
                    for (let j = 0; j < audit.issueIds.length; j++) {
                        await issueSchema
                            .findOne({ issueId: audit.issueIds[j] })
                            .select(" auditId issueId issuePriority actionId ")
                            .then(async (issue) => {
                                if (issue) {
                                    var fissue = {
                                        issueId: issue.issueId,
                                        priority: issue.issuePriority,
                                        actions: [],
                                    };

                                    for (let k = 0; k < issue.actionId.length; k++) {
                                        await actionSchema
                                            .findOne({ actionId: issue.actionId[k] })
                                            .select(" actionId rating ")
                                            .then(async (action) => {
                                                if (action) {
                                                    var faction = {
                                                        actionId: action.actionId,
                                                        rating: action.rating,
                                                    };
                                                    fissue.actions.push(faction);
                                                }
                                            });
                                    }
                                }
                                faudit.issues.push(fissue);
                            });
                    }
                    audits.push(faudit);
                }
            });
    }
    await rsHelper.manage(res, null, audits, 200, "galaxy data");
};

module.exports = auditMasterController;