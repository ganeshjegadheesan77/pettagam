const logger = require("../../../utils/logger").logger;
const { workStepsTemplateSchema } = require("../../../models/enia/workStepsTemplate.schema");
const rsHelper = require("../../../utils/responseHelper");
const workStepSchema = require('../../../models/enia/workSteps.schema').workStepSchema;
const workStepTemplateSchema = require('../../../models/enia/workStepsTemplate.schema').workStepsTemplateSchema;
const auditMasterSchema = require('../../../models/enia/auditMaster.schema').auditMasterSchema;
const internalAuditIssuesSchema = require('../../../models/enia/internalAuditIssues.schema').internalAuditIssuesSchema;
const { httpCode } = require("../../../constants");
const userSchema = require("../../../models/users/index");
const { docType } = require("../../../constants");
const constants = require("../../../constants");
const { forEach } = require("lodash");

// const { delete } = require("../../users/user.controller");

const agileAuditSpaceController = {};

// Pratik START //
////////////////////////////////////// Attach Reference Document /////////////////////////////////////
agileAuditSpaceController.updateReferenceDocument = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await workStepSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepId: 1, referenceDocuments: 1 });
      if (!audit) {
        rsHelper.manage(res, "no Attachment data found.", null, 500, "no data found.");
        return
      } else {

        audit.referenceDocuments = req.body.referenceDocuments;
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "ReferenceDocument updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

/////////////////////////////////// Attach Working Papers /////////////////////////////////////


agileAuditSpaceController.updateWorkingPapers = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await workStepSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepId: 1, workingPapers: 1 });
      if (!audit) {
        rsHelper.manage(res, "no Attachment data found.", null, 500, "no data found.");
        return
      } else {

        audit.workingPapers = req.body.workingPapers;
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "WorkingPaper updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};
////////////////////////////// Remove Reference Doc //////////////////////////////////////////

agileAuditSpaceController.deleteReferenceDocument = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await workStepSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepId: 1, referenceDocuments: 1 });
      if (!audit) {
        rsHelper.manage(res, null, null, 500, "no ReferenceDocument data found.");
        return
      } else {
        const index = parseInt(req.body.rowIndex);
        if (index > -1) {
          audit.referenceDocuments.splice(index, 1);
        } else {
        }
        audit.save().then((doc) => {
          console.log("[deleteReferenceDocument] Workstep updated successfully");
          rsHelper.manage(res, null, doc, 200, "ReferenceDocument Deleted successfully");
        }).catch((err) => {
          console.log("[deleteReferenceDocument] Workstep update failed");
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

///////////////////////////// Remove Working Papper //////////////////////////////////////////

agileAuditSpaceController.deleteWorkingPaper = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await workStepSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepId: 1, workingPapers: 1 });
      if (!audit) {
        rsHelper.manage(res, null, null, 500, "no WorkingPaper data found.");
        return
      } else {
        const index = parseInt(req.body.rowIndex);
        if (index > -1) {

          audit.workingPapers.splice(index, 1);
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "WorkingPaper deleted successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};


// Pratik END //



// Sagar START //
agileAuditSpaceController.getPreparerList = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (!auditId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
    auditMasterSchema.aggregate([
      { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
      {
        $lookup: {
          from: "users",
          localField: "teamAndTimeline.auditors",
          foreignField: "username",
          as: "imagePaths",
        },
      },
      {
        $project: {
          "teamAndTimeline.auditors": 1,
          auditId: 1,
          "imagePaths.profileUrl": 1
        },
      },
    ]).exec(async (err, docs) => {
      console.log('docs', docs)
      if (err) {
        rsHelper.manage(res, error, null, 500, "Exception Occured");
      } else {
        if (!docs.length) {
          rsHelper.manage(res, "no preparer list found.", null, 500, "no data found.");
          return
        } else {
          let data = [];
          let preparer = [];
          if (docs[0].teamAndTimeline.auditors.length && docs[0].imagePaths.length) {
            data.push({ "auditId": docs[0].auditId })
            for (const auditor of docs[0].teamAndTimeline.auditors) {
              for (const imagePath of docs[0].imagePaths) {
                preparer.push({ "preparer": auditor, "path": imagePath.profileUrl })
                break;
              }
            }
            data.push({ "preaprerData": preparer })
          } else {
            rsHelper.manage(res, "no preparer list found.", null, 500, "no data found.");
            return
          }

          Promise.all(data)
            .then((data) => {
              rsHelper.manage(res, null, data, 200, "Preparer list found successfully");
            }).catch((error) => {
              rsHelper.manage(res, error, null, 500, "Exception Occured");
            });


        }

      }
    })

  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

// agileAuditSpaceController.getViewData = async (req, res) => {
//   try {
//     let auditId = req.body.auditId || undefined;

//     console.log('**** USERNAME', req.user.username, "****ROLE: ", req.user.role)
//     if (!auditId) {
//       rsHelper.manage(res, "validation error", null, 500, "validation error");
//       return
//     }
//     if (req.body.myView == true) {//myView callled
//       auditMasterSchema.aggregate([
//         { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
//         {
//           $lookup: {
//             from: "workSteps",
//             localField: "workStepIds",
//             foreignField: "workStepId",
//             as: "workStepData",
//           },

//         },
//         {
//           $project: {
//             "workStepData": 1,
//             auditId: 1,
//           },
//         },
//       ]).exec(async (err, docs) => {

//         if (err) {
//           rsHelper.manage(res, err, null, 500, "Exception Occured");
//         } else {
//           if (!docs.length) {
//             rsHelper.manage(res, "no worksteps found.", null, 500, "no worksteps found for audit" + auditId + ".");
//             return
//           } else {
//             let workStepResult = [];

//             if (req.user.role == "auditor") {
//               // console.log('**************enters into AUDITOR')
//               const result = docs[0].workStepData.filter(w => w.createdBy == req.user.username || w.preparer.username == req.user.username);
//               if (result.length) {
//                 for (const r of result) {
//                   workStepResult.push({ "createdBy": r.createdBy, "preparer": r.preparer, 'reviewer': r.reviewer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
//                 }
//               } else {
//                 rsHelper.manage(res, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.", null, 500, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.");
//                 return;
//               }

//             }
//             if (req.user.role == "auditLead") {
//               // console.log('**************enters into AUDIT LEAD')

//               const result = docs[0].workStepData.filter(w => w.createdBy == req.user.username || w.reviewer.username == req.user.username);
//               // console.log('**** audit Lead result', result)
//               if (result.length) {
//                 for (const r of result) {
//                   workStepResult.push({ "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
//                 }
//               } else {
//                 rsHelper.manage(res, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.", null, 500, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.");
//                 return;
//               }

//             }

//             Promise.all(workStepResult)
//               .then((workStepResult) => {
//                 rsHelper.manage(res, null, workStepResult, 200, "My View worksteps for " + req.user.role + " found successfully");
//               }).catch((error) => {
//                 rsHelper.manage(res, error, null, 500, "Exception Occured");
//               });


//           }
//         }
//       });

//     }

//     if (req.body.myView == false && req.user.role == 'auditLead') { // team view for AuditLead
//       auditMasterSchema.aggregate([
//         { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
//         {
//           $lookup: {
//             from: "workSteps",
//             localField: "workStepIds",
//             foreignField: "workStepId",
//             as: "workStepData",
//           },
//         },
//         {
//           $project: {
//             "workStepData": 1,
//             auditId: 1,
//           },
//         },
//       ]).exec(async (err, docs) => {

//         if (err) {
//           rsHelper.manage(res, error, null, 500, "Exception Occured");
//         } else {
//           if (!docs.length) {
//             rsHelper.manage(res, "no team view found.", null, 500, "team view data not found for audit-" + auditId + ".");
//             return
//           } else {
//             let workStepResult = [];
//             if (docs[0].workStepData.length) {
//               for (const r of docs[0].workStepData) {
//                 workStepResult.push({ "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
//               }

//               Promise.all(workStepResult)
//                 .then((workStepResult) => {
//                   rsHelper.manage(res, null, workStepResult, 200, "Team view for audit lead found successfully");
//                 }).catch((error) => {
//                   rsHelper.manage(res, error, null, 500, "Exception Occured");
//                 });

//             } else {
//               rsHelper.manage(res, "no team view for audit lead found.", null, 500, "team view data not found for audit-" + auditId + ".");
//             }

//             // rsHelper.manage(res, null, docs, 200, "Worksteps found successfully");

//           }
//         }
//       });
//     }

//     if (req.body.myView == false && req.user.role == 'auditor') {
//       rsHelper.manage(res, "no team view functionality exists for auditor.", null, 500, "Team view functionality exists for auditor.");
//       return
//     }




//   } catch (error) {
//     console.log("@@ error @@ #", error)
//     rsHelper.manage(res, error, null, 500, "Exception Occured");
//     return
//   }
// }


agileAuditSpaceController.getViewData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;

    console.log('**** USERNAME', req.user.username, "****ROLE: ", req.user.role)
    if (!auditId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
    if (req.body.myView == true) {//myView callled
            let workStepResult = [];
            let myViewResult =[];
            let issueData =[];
      auditMasterSchema.aggregate([
        { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
        //workstep
        {
          $lookup: {
            from: "workSteps",
            localField: "workStepIds",
            foreignField: "workStepId",
            as: "workStepData",
          },

        },
        //issue
        {
          $lookup: {
            from: "internalAuditIssues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "issueData",
          },

        },
        {
          $project: {
            "workStepData": 1,
            "issueData":1,
            auditId: 1,
          },
        },
      ]).exec(async (err, docs) => {
        // console.log('***********docs', docs[0]['workStepData']);

        if (err) {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
        } else {
          if (!docs.length) {
            rsHelper.manage(res, "no worksteps found.", null, 500, "no worksteps found for audit" + auditId + ".");
            return
          } else {


            if (req.user.role == "auditor") {
              // console.log('**************enters into AUDITOR')

              //worksteps
              const result = docs[0].workStepData.filter(w => w.createdBy == req.user.username || w.preparer.username == req.user.username);
              if (result.length) {
                for (const r of result) {
                  workStepResult.push({ "createdBy": r.createdBy, "preparer": r.preparer, 'reviewer': r.reviewer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
                }
                myViewResult.push({"workSteps":workStepResult})
              } else {
                myViewResult.push({"workSteps":[]})

              }

              //Issues
              const IssueResult = docs[0].issueData.filter(w => w.createdBy == req.user.username || w.preparer.username == req.user.username);
              if (IssueResult.length) {
                for (const r of IssueResult) {
                  issueData.push({ workStepIds : r.workStepIds, "createdBy": r.createdBy, "preparer": r.preparer, 'reviewer': r.reviewer,  "issueId": r.issueId, "issueName": r.issueName })
                }
                myViewResult.push({"issues":issueData})
              } else {
                myViewResult.push({"issues":[]})
                // rsHelper.manage(res, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.", null, 500, "no work steps found for " + req.user.role + " of audit-" + auditId + " -my view.");
                // return;
              }

            }
            if (req.user.role == "auditLead") {
              // console.log('**************enters into AUDIT LEAD')
              //worksteps
              const wresult = docs[0].workStepData.filter(w => w.createdBy == req.user.username || w.reviewer.username == req.user.username);
              // console.log('**** audit Lead result', result)
              if (wresult.length) {
                for (const r of wresult) {
                  workStepResult.push({ "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
                }
                myViewResult.push({'workSteps':workStepResult})
              } else {
                myViewResult.push({'workSteps':[]});
              }


              //issues
              const issueResult = docs[0].issueData.filter(w => w.createdBy == req.user.username || w.reviewer.username == req.user.username);
            //  console.log('**** audit Lead result', issueResult)
              if (issueResult.length >0) {
                for (const r of issueResult) {
                  issueData.push({ workStepIds : r.workStepIds, "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer,  "issueId": r.issueId, "issueName": r.issueName })
                }
                myViewResult.push({'issues':issueData})
              } else {
                myViewResult.push({'issues':[]});
              }

            }

            Promise.all(myViewResult)
              .then((myViewResult) => {
                rsHelper.manage(res, null, myViewResult, 200, "My View data for " + req.user.role + " found successfully");
              }).catch((error) => {
                rsHelper.manage(res, error, null, 500, "Exception Occured");
              });


          }
        }
      });

    }

    if (req.body.myView == false && req.user.role == 'auditLead') { // team view for AuditLead
            let workStepResult = [];
            let result =[];
            let issueData =[];
      auditMasterSchema.aggregate([
        { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
        //Join with workstep collection
        {
          $lookup: {
            from: "workSteps",
            localField: "workStepIds",
            foreignField: "workStepId",
            as: "workStepData",
          },
        },
        // Join with issues collection
        {
          $lookup:{
              from: "internalAuditIssues", 
              localField: "issueIds", 
              foreignField: "issueId",
              as: "issueData"
          }
        },
        {
          $project: {
            "workStepData": 1,
            "issueData":1,
            auditId: 1,
          },
        },
      ]).exec(async (err, docs) => {
        console.log('***** docs', docs)

        if (err) {
          rsHelper.manage(res, error, null, 500, "Exception Occured");
        } else {
          if (!docs.length) {
            rsHelper.manage(res, "no team view found.", null, 500, "team view data not found for audit-" + auditId + ".");
            return
          } else {

            if (docs[0].workStepData.length) {
              for (const r of docs[0].workStepData) {
                workStepResult.push({ "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer, "workingPapers": r.workingPapers, "workPaperCount": r.workingPapers.length, "workStepId": r.workStepId, "workStepName": r.workStepName })
              }
              result.push({"workSteps":workStepResult});

            }else{
              result.push({"workSteps":[]});
            }
            if(docs[0].issueData && docs[0].issueData.length){
              for(const r of docs[0].issueData){
                issueData.push({ workStepIds : r.workStepIds, "createdBy": r.createdBy, "reviewer": r.reviewer, "preparer": r.preparer, "issueId": r.issueId, "issueName": r.issueName })
              }
              result.push({"issues":issueData});
            }else{
              result.push({"issues":[]});

            } 
             Promise.all(result)
                .then((result) => {
                  rsHelper.manage(res, null, result, 200, "Team view for audit lead found successfully");
                }).catch((error) => {
                  rsHelper.manage(res, error, null, 500, "Exception Occured");
                });

          }
        }
      });
    }

    if (req.body.myView == false && req.user.role == 'auditor') {
      rsHelper.manage(res, "no team view functionality exists for auditor.", null, 500, "Team view functionality exists for auditor.");
      return
    }
  } catch (error) {
    console.log("@@ error @@ #", error)
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

agileAuditSpaceController.applyAndOverwriteTemplates = async (req, res) => {
  try {
    console.log('BODY', req.body);
    let templateId = req.body.templateId || undefined;
    let auditId = req.body.auditId || undefined;
    if (!templateId || !auditId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
    //basic cheque
    var auditData = await auditMasterSchema.findOne({ auditId: auditId }, { workStepIds: 1 });
    var templateData = await workStepsTemplateSchema.findOne({ templateId: templateId }, { workSteps: 1 });
    if (auditData && templateData && templateData.workSteps.length > 0) {
      //Delete old worksteps
      var workStepsData = await workStepSchema.find({
        $and: [{ auditId: auditId, 'reviewer.status': 'assigned', 'origin': { $ne: 'create' }, 'preparer.status': '' }],
      }, { workStepId: 1 });
      console.log('workStepsData', workStepsData)

      if (workStepsData && workStepsData.length > 0) {
        let del = await workStepSchema.deleteMany({ workStepId: { $in: workStepsData.map(w => w['workStepId']) } });
        console.log('*****deleted workstep: ', del);

        for (const wd of workStepsData) {
          var index = auditData.workStepIds.findIndex(i => i === wd.workStepId);
          console.log('index', index)
          if (index != -1) {
            auditData.workStepIds.splice(index, 1);

          }

        }
      }
      for (const w of templateData.workSteps) {
        var lastWorkStepNumber = await getLastWorkStepId();
        // console.log('******************lastWorkStepNumber', lastWorkStepNumber)
        var workStepNumber = (lastWorkStepNumber + 1);
        var workStepId = "WS-0" + workStepNumber;
        var ws = new workStepSchema({
          auditId: auditId,
          workStepId: workStepId,
          workStepNumber: workStepNumber,
          workStepName: w.workStepName,
          uniqueTemplateId: w.uniqueTemplateId,
          taskDescription: w.taskDescription,
          createdBy: req.user.username,
          hasIssues: false,
          origin: 'template',
          updatedBy: ''
        });
        if (req.user.role == 'auditLead') {
          ws.reviewer = {
            username: req.user.username,
            profileUrl: req.user.profileUrl,
            status: 'assigned'
          }
          ws.preparer = {
            status: '',
            username: ''
          }
        }
        auditData.workStepIds.push(workStepId);
        await ws.save().then((d) => {
          // console.log('**** new workstep data', d)
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "create workstep Exception Occured");
        })

      }
      auditData.save().then((a) => {
        // console.log('***** updated audit', a)
        rsHelper.manage(res, null, a, 200, "Template worksteps applied for audit " + auditId + ".");
      }).catch((err) => {
        rsHelper.manage(res, err, null, 500, "Audit Exception Occured");
      })
    } else {
      rsHelper.manage(res, "Met one or more of these conditions 1) Audit data not found for the given Audit ID " + auditId + ", 2) Template data not found for the given Id " + templateId + " and 3) The template matching the given Id " + templateId + " doesn't contain any worksteps associated with it.", null, 500, "Met one or more of these conditions 1) Audit data not found for the given Audit ID " + auditId + ", 2) Template data not found for the given Id " + templateId + " and 3) The template matching the given Id " + templateId + " doesn't contain any worksteps associated with it.");
      return
    }
  } catch (error) {
    console.log("@@ error @@ #", error)
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

agileAuditSpaceController.getAllIssuesOfAudit = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (!auditId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    } else {
      auditMasterSchema.aggregate([
        { $match: { auditId: req.body.auditId, isCompleted: false, isActive: true } },
        //issue
        {
          $lookup: {
            from: "internalAuditIssues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "issueData",
          },

        },
        {
          $project: {
            "issueData": 1,
            auditId: 1,
          },
        },
      ]).exec(async (err, docs) => {
        if (err) {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
        } else {
          if (!docs.length) {
            rsHelper.manage(res, "no issues found.", null, 500, "no issues found for audit-" + auditId + ".");
            return
          } else {
            rsHelper.manage(res, null, docs, 200, "issue list found for audit-" + auditId + " found successfully.");
          }
        }
      });

    }
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

agileAuditSpaceController.getAllIssuesOfWorkStep = async (req, res) => {
  try {
    let workStepId = req.body.workStepId || undefined;
    if (!workStepId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    } else {
      workStepSchema.aggregate([
        { $match: { workStepId: workStepId } },
        {
          $lookup: {
            from: "internalAuditIssues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "issueList",
          },

        },
        {
          $project: {
            "issueList": 1,
            workStepId: 1,
          },
        },
      ]).exec(async (err, docs) => {
        if (err) {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
        } else {
          if (!docs.length) {
            rsHelper.manage(res, "no issues found.", null, 500, "no issues found for workStep-" + workStepId + ".");
            return
          } else {
            rsHelper.manage(res, null, docs, 200, "issue list found for workStep-" + workStepId + " found successfully.");
          }
        }
      });

    }

  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}



// Sagar END //



// Yash START //
agileAuditSpaceController.createWorkStep = async (req, res) => {
  try {
    var todaysDate = new Date();
    var audit = await auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, teamAndTimeline: 1, workStepIds: 1 });
    if (audit) {
      var exists = await checkIfWorkStepNameExists(req.body.workStepName, req.body.auditId);
      if (!exists) {
        var lastWorkStepNumber = await getLastWorkStepId();
        var workStepNumber = (lastWorkStepNumber + 1);
        var workStepId = "WS-0" + workStepNumber;

        var ws = new workStepSchema({
          auditId: req.body.auditId,
          workStepId: workStepId,
          workStepNumber: workStepNumber,
          workStepName: req.body.workStepName,
          workPerformed: req.body.workPerformed,
          uniqueTemplateId: "0",
          taskDescription: req.body.taskDescription,
          referenceDocuments: req.body.referenceDocuments,
          workingPapers: req.body.workingPapers,
          summary: req.body.summary,
          createdBy: req.user.username,
          hasIssues: false,
          origin: 'create',
          updatedBy: ''
        });

        if (req.user.role == 'auditor') {
          ws.preparer = {
            username: req.user.username,
            profileUrl: req.user.profileUrl,
            preparedOn: todaysDate,
            status: req.body.preparer.status
          }
          if (audit.teamAndTimeline && audit.teamAndTimeline.auditLead && audit.teamAndTimeline.auditLead.length > 0) {
            var auditLead = audit.teamAndTimeline.auditLead[0];
            var user = await userSchema.findOne({ username: auditLead }, { username: 1, profileUrl: 1 });
            ws.reviewer = {
              username: user.username,
              profileUrl: user.profileUrl,
              status: req.body.reviewer.status
            }
          }
        }
        else if (req.user.role == 'auditLead') {
          ws.reviewer = {
            username: req.user.username,
            profileUrl: req.user.profileUrl,
            status: req.body.reviewer.status
          }
          if (req.body.preparer) {
            ws.preparer = req.body.preparer;
          } else {
            ws.preparer = {
              status: ''
            }
          }
        }

        ws.save().then(async (doc) => {
          // var audit = await auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepIds: 1 });
          if (audit.workStepIds && audit.workStepIds.length > 0) {
            audit.workStepIds.push(doc.workStepId);
          }
          else {
            audit.workStepIds = [doc.workStepId];
          }
          audit.save().then((a) => {
            rsHelper.manage(res, null, doc, 200, "WorkStep created successfully");
          }).catch((err) => {
            rsHelper.manage(res, err, null, 500, "Audit Exception Occured");
          })

        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Workstep Exception Occured");
        })
      }
      else {
        rsHelper.manage(res, null, null, 400, "WorkStep already exists");
      }
    }
    else {
      rsHelper.manage(res, null, null, 400, "Audit doesn't exists");
    }
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

const checkIfWorkStepNameExists = (workStepName, auditId) => {
  return workStepSchema.count({ workStepName: { '$regex': workStepName, $options: 'i' }, auditId }, function (err, count) {
    if (err) {
      throw err;
    }
    if (count > 0) {
      return true;
    }
    return false;
  })
}

const getLastWorkStepId = () => {
  return workStepSchema.find({}, { workStepNumber: 1, _id: 0 }).sort({ workStepNumber: -1 }).limit(1)
    .then((doc) => {
      if (doc && doc.length > 0) {
        return doc[0].workStepNumber;
      }
      return 0;
    }).catch((err) => {
      throw err;
    })
}

agileAuditSpaceController.getWorkStep = async (req, res) => {
  // Execution start from here ..
  try {
    var lookup = [{
      $lookup: {
        from: 'DmsDocumentRepository',
        localField: 'referenceDocuments.documentNo',
        foreignField: 'documentNo',
        as: 'referenceDocumentLookup'
      }
    },
    {
      $lookup: {
        from: 'DmsDocumentRepository',
        localField: 'workingPapers.documentNo',
        foreignField: 'documentNo',
        as: 'workingPapersLookup'
      }
    }];
    let workStep = await workStepSchema.aggregate([
      {
        $match: {
          workStepId: req.params.workStepId
        }
      },
      ...lookup
    ]);
    if (workStep) {
      if (workStep.origin == 'template' && workStep.reviewer.status == 'assigned' && await checkIfSameTemplateId(workStep.auditId, workStep.workStepId, workStep.uniqueTemplateId)) {
        workStep.hasDuplicate = true;
      }
      let result = appenDmsRepoData(workStep);

      rsHelper.manage(res, null, result[0], httpCode.success, "WorkStep details");
    } else {
      rsHelper.manage(res, null, null, httpCode.notFound, "No WorkStep Found");
    }
  } catch (error) {
    rsHelper.manage(res, error, null, httpCode.error, "Exception Occured");
  }

  function appenDmsRepoData(data) {
    let workstep = JSON.parse(JSON.stringify(data));
    let r = workstep.map((ws) => {
      for (let i = 0; i < ws.referenceDocuments.length; i++) {
        ws.referenceDocuments[i]["dmsRepoDetail"] = ws.referenceDocumentLookup[i] ? ws.referenceDocumentLookup[i] : {};
        ws.referenceDocuments[i]["dmsRepoDetail"].docType = docType.referenceDoc;
      }
      delete ws.referenceDocumentLookup;
      for (let i = 0; i < ws.workingPapers.length; i++) {
        ws.workingPapers[i]["dmsRepoDetail"] = ws.workingPapersLookup[i] ? ws.workingPapersLookup[i] : {};
        ws.workingPapers[i]["dmsRepoDetail"].docType = docType.workPaper;
      }
      delete ws.workingPapersLookup;
      return ws;
    })
    return r;
  }
}

const checkIfSameTemplateId = (auditId, workStepId, uniqueTemplateId) => {
  return workStepSchema.count({ workStepId: { $ne: workStepId }, uniqueTemplateId: uniqueTemplateId, auditId: auditId, origin: 'template' }, function (err, count) {
    if (err) {
      throw err;
    }
    if (count > 0) {
      return true;
    }
    return false;
  })
}

agileAuditSpaceController.deleteWorkStep = async (req, res) => {
  try {
    var workStep = await workStepSchema.findOne({ workStepId: req.params.workStepId }, { createdBy: 1, workStepId: 1, preparer: 1, reviewer: 1, auditId: 1 });
    if (workStep) {
      var auditId = workStep.auditId;
      if ((req.user.role == 'auditor' && workStep.createdBy == req.user.username && workStep.preparer.username == req.user.username) || (req.user.role == 'auditLead' && workStep.createdBy == req.user.username)) {
        workStep.deleteOne(async function (err, doc) {
          if (err && !doc) {
            rsHelper.manage(res, null, null, 500, "Please try again");
            return;
          }
          var audit = await auditMasterSchema.findOne({ auditId: auditId }, { auditId: 1, workStepIds: 1 });
          if (audit && audit.workStepIds && audit.workStepIds.length > 0) {
            var wsIndex = audit.workStepIds.findIndex(a => a == workStep.workStepId);
            if (wsIndex != -1) {
              audit.workStepIds.splice(wsIndex, 1);
              await audit.save();
            }
          }
          rsHelper.manage(res, null, null, 200, "WorkStep Deleted Successfully !");
          return;
        });
      }
      else {
        rsHelper.manage(res, null, null, 404, "Not Authorised to delete");
        return;
      }
    }
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

agileAuditSpaceController.updateWorkStep = async (req, res) => {
  try {
    var workStep = await workStepSchema.findOne({ workStepId: req.body.workStepId });
    workStep.workPerformed = req.body.workPerformed; //
    workStep.referenceDocuments = req.body.referenceDocuments; //
    workStep.workingPapers = req.body.workingPapers; //
    workStep.summary = req.body.summary; //
    workStep.updatedBy = req.user.username;
    workStep.hasIssues = req.body.hasIssues; //
    workStep.preparer = req.body.preparer;
    workStep.reviewer = req.body.reviewer;
    if (req.user.role == 'auditLead') {
      workStep.rejectionReason = req.body.rejectionReason; //
      workStep.taskDescription = req.body.taskDescription; //
    } else if (req.user.role == "auditor" && workStep.createdBy == req.user.username) {
      workStep.taskDescription = req.body.taskDescription;
    }

    workStep.save().then((doc) => {
      rsHelper.manage(res, null, doc, 200, "WorkStep updated Successfully!");
    }).catch((err) => {
      rsHelper.manage(res, err, null, 500, "Database Exception Occured");
    })

  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

agileAuditSpaceController.getIssue = async (req, res) => {
  try {
    let issueId = req.params.issueId || undefined;
    if (!issueId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    } else {
      var issueResp = await internalAuditIssuesSchema.findOne({ issueId: issueId });
      if (issueResp) {
        rsHelper.manage(res, null, issueResp, httpCode.success, "Issue record found successfully!");
      } else {
        rsHelper.manage(res, null, null, httpCode.notFound, "no issue record found.");
        return
      }
    }
  } catch (error) {
    console.log("@@ error @@ #", error)
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

agileAuditSpaceController.createIssue = async (req, res) => {
  try {
    var todaysDate = new Date();
    var workStep;
    var audit = await auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, teamAndTimeline: 1, issueIds: 1 });
    if (audit) {
      var exists = await checkIfIssueNameExists(req.body.issueName, req.body.auditId);
      if (!exists) {
        if (req.body.fromWorkStep == true) {
          workStep = await workStepSchema.findOne({ workStepId: req.body.workStepId }, { auditId: 1, workStepId: 1, issueIds: 1 });
          if (!workStep) {
            rsHelper.manage(res, null, null, 400, "WorkStep doesn't exists");
            return;
          }
        }

        var lastIssueNumber = await getLastIssueId();
        var issueNumber = (lastIssueNumber + 1);
        var issueId = "I-0" + issueNumber;

        var issue = new iaIssueSchema({
          auditId: req.body.auditId,
          issueId: issueId,
          issueNumber: issueNumber,
          issueName: req.body.issueName,
          issueStatus: req.body.issueStatus,
          issuePriority: req.body.issuePriority,
          shortDescription: req.body.shortDescription,
          reviewComments: req.body.reviewComments,
          longDescription: req.body.longDescription,
          recommendations: req.body.recommendations,
          managementResponse: req.body.managementResponse,
          attachments: req.body.attachments,
          // rejectionComments: req.body.rejectionComments,
          createdBy: req.user.username,
          updatedBy: ''
        });

        if (req.body.fromWorkStep == true && workStep) {
          issue.workStepIds = [workStep.workStepId];
        }

        issue.preparer = {
          username: req.user.username,
          profileUrl: req.user.profileUrl,
          preparedOn: todaysDate,
          status: req.body.preparer.status
        }

        if (req.user.role == 'auditLead') {
          issue.reviewer = {
            username: req.user.username,
            profileUrl: req.user.profileUrl,
            reviewedOn: todaysDate,
            status: req.body.preparer.status
          }
        }
        else {
          var auditLead = audit.teamAndTimeline.auditLead[0];
          var user = await userSchema.findOne({ username: auditLead }, { username: 1, profileUrl: 1 });
          issue.reviewer = {
            username: user.username,
            profileUrl: user.profileUrl,
            status: req.body.reviewer.status
          }
        }


        issue.save().then(async (doc) => {
          // var audit = await auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, workStepIds: 1 });
          if (audit.issueIds && audit.issueIds.length > 0) {
            audit.issueIds.push(doc.issueId);
          }
          else {
            audit.issueIds = [doc.issueId];
          }
          audit.save().then(async (a) => {
            if (req.body.fromWorkStep == true && workStep) {
              if (workStep.issueIds && workStep.issueIds.length > 0) {
                workStep.issueIds.push(issueId)
              }
              else {
                workStep.issueIds = [issueId]
              }
              await workStep.save();
            }
            rsHelper.manage(res, null, doc, 200, "Issue created successfully");
          }).catch((err) => {
            rsHelper.manage(res, err, null, 500, "Audit Exception Occured");
          })
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Issue Exception Occured");
        })
      }
      else {
        rsHelper.manage(res, null, null, 400, "Issue already exists");
      }
    }
    else {
      rsHelper.manage(res, null, null, 400, "Audit doesn't exists");
    }
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

const checkIfIssueNameExists = (issueName, auditId) => {
  return iaIssueSchema.count({ issueName: { '$regex': issueName, $options: 'i' }, auditId }, function (err, count) {
    if (err) {
      throw err;
    }
    if (count > 0) {
      return true;
    }
    return false;
  })
}

const getLastIssueId = () => {
  return iaIssueSchema.find({}, { issueNumber: 1, _id: 0 }).sort({ issueNumber: -1 }).limit(1)
    .then((doc) => {
      if (doc && doc.length > 0) {
        return doc[0].issueNumber;
      }
      return 0;
    }).catch((err) => {
      throw err;
    })
}

agileAuditSpaceController.updateIssue = async (req, res) => {
  try {
    var issue = await iaIssueSchema.findOne({ issueId: req.body.issueId });
    issue.issueStatus = req.body.issueStatus; //
    issue.issuePriority = req.body.issuePriority; //
    issue.shortDescription = req.body.shortDescription; //
    issue.reviewComments = req.body.reviewComments; //
    issue.updatedBy = req.user.username;
    issue.preparer = req.body.preparer;
    issue.reviewer = req.body.reviewer;
    issue.longDescription = req.body.longDescription;
    issue.recommendations = req.body.recommendations;
    issue.managementResponse = req.body.managementResponse;
    issue.attachments = req.body.attachments;
    issue.rejectionComments = req.body.rejectionComments;

    issue.save().then((doc) => {
      rsHelper.manage(res, null, doc, 200, "Issue updated Successfully!");
    }).catch((err) => {
      rsHelper.manage(res, err, null, 500, "Database Exception Occured");
    })
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

// Yash END //



// Gopal START //
agileAuditSpaceController.getAudits = async (req, res) => {
  try {
    let conditions = (username, par) => {
      // let criteria = progressBarCriteriaSchema.find({}, { criteria: 1 })
      // let conditionsJson = {}
      // criteria.forEach((ele) => {
      //     conditionsJson[ele]['isCompleted'] = true
      // });
      let conditionsJson = {
        "teamAndTimeline.isCompleted": true,
        "planningMemorandum.isCompleted": true,
        "auditAnnouncement.isCompleted": true,
        "auditPbc.isCompleted": true,
        "isCompleted": false,
        "isActive": true
      }
      conditionsJson[par] = username;
      return conditionsJson;
    };
    if (req.user.role == 'auditLead') {
      var auditLead = await auditMasterSchema.find(conditions(req.user.username, "teamAndTimeline.auditLead"), { auditId: 1, auditableEntity: 1, year: 1, "category.levelFive.data": 1 });
      rsHelper.manage(res, null, auditLead, 200, "auditLead");
    } else if (req.user.role == 'auditor') {
      var auditor = await auditMasterSchema.find(conditions(req.user.username, "teamAndTimeline.auditors"), { auditId: 1, auditableEntity: 1, year: 1, "category.levelFive.data": 1 });
      rsHelper.manage(res, null, auditor, 200, "auditor");
    }
  } catch (error) {
    logger.pushError(error);
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}

agileAuditSpaceController.getAuditsTeam = async (req, res) => {

  try {
    let usersArray = await auditMasterSchema.findOne({ auditId: req.body.auditId }, { "teamAndTimeline.auditLead": 1, "teamAndTimeline.auditors": 1, "teamAndTimeline.pbc": 1, "teamAndTimeline.auditees": 1 });
    let nameArray = usersArray.teamAndTimeline.auditLead.concat(usersArray.teamAndTimeline.auditors,
      usersArray.teamAndTimeline.pbc, usersArray.teamAndTimeline.auditees)
    userSchema.find({ username: { $in: nameArray } }, { profileUrl: 1, username: 1 }, (error, data) => {
      if (error) {
        rsHelper.manage(res, error, null, 200, "users_error");
      } else {
        rsHelper.manage(res, null, data, 200, "users");
      }
    })


  } catch (error) {
    logger.pushError(error);
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}


// Gopal END //



// Asrhaf START //

/*
    *   it gives list of Template containing name and id
    *   output result : [{}]                         //array of objects
*/
agileAuditSpaceController.getTemplateList = async (req, res) => {
  try {
    let templateData = await workStepTemplateSchema.find({ status: true, "workSteps.0": { "$exists": true } }).select('templateId templateName').lean();
    let result = templateData.map((obj) => {
      obj.name = obj.templateName
      delete obj.templateName
      return obj;
    });
    rsHelper.manage(res, null, result, httpCode.success, "ok");
    return;
  } catch (error) {
    rsHelper.manage(res, error, null, httpCode.error, "Exception Occured");
    return;
  }
}

/**
 * @apiMethod {delete} Delete issue by ID
 * @apiParam {String} {{issueId}} issueId is unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "issue deleted successfully!!!",
 *        "data": [{
 *          documentName : "test.pdf",
 *          documentNo : "453",
 *          dateUploaded : "",
 *          documentUrlToken : "&8djhg8768734dkjasd76872y3lamsadlhflkas&sdhi3737"
 *        }]
 *      }
 *
 * @apiError IssueIdNotFound The id of the issue was not found.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *        "message": "issue not found!!",
 *        "data": null
 *     }
 * 
 * @apiError validationError Issue id is missing in params.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *        "message": "validation error! Issue id is required!!",
 *        "data": null
 *     }
 * 
 * @apiError validationError Issue id is missing in params.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *        "message": "validation error! Issue id is required!!",
 *        "data": null
 *     }
 */
agileAuditSpaceController.deleteIssue = async (req, res) => {
  try {
    // params validation
    let issueId = req.params.issueId;
    if (!issueId) {
      rsHelper.manage(res, null, null, httpCode.badRequest, "validation error! Issue id is required!");
      return;
    }
    //check issue exist or not
    let issueData = await internalAuditIssuesSchema.findOne({ "issueId": issueId}).lean();
    if (!issueData) {
      rsHelper.manage(res, null, null, httpCode.notFound, "issue not found!!");
      return;
    }
    //authorization check for deleting issue
    let userRole = req.user.role;
    if(userRole == "auditor") {
      if(issueData.createdBy != req.user.username || (issueData.prepare && issueData.preparer.status != "wip")){
        rsHelper.manage(res, null, null, httpCode.unauthorized, "user is not authorized to delete issue!!!");
        return;
      }
    } else  if (userRole == "auditLead"){
      if(issueData.prepare && issueData.preparer.status != "wip"){
        rsHelper.manage(res, null, null, httpCode.unauthorized, "user is not authorized to delete issue!!!");
        return;
      }
    }
    let attachments = issueData.attachments ? issueData.attachments:[];
    //delete issue
    let deleteResp = await internalAuditIssuesSchema.deleteOne({ issueId: issueId });
    if (!deleteResp.deletedCount) {
      rsHelper.manage(res, null, null, httpCode.error, "Something went wrong!!!");
      return;
    }
    /*UNLINK WORKSTEPS AND AUIDIT*/
    
    //unlink issue from worksteps
    if (issueData.workStepIds && issueData.workStepIds.length) {
      let ws = issueData.workStepIds;
      for (let i = 0; i < ws.length; i++) {
        workStepSchema.findOneAndUpdate(
          { workStepId: ws[i] },
          { $pull: { "issueIds": issueId } },
          (e) => { }
        );
      }
    }
    //unlink issue from audit
    if (issueData.auditId) {
      auditMasterSchema.findOneAndUpdate(
        { "auditId": issueData.auditId },
        { $pull: { "issueIds": issueId } },
        (e) => { }
      )
    }
    rsHelper.manage(res, null, attachments, httpCode.success, "issue deleted successfully!!!");
  } catch (e) {
    rsHelper.manage(res, e, null, httpCode.error, "error occoured!!!");
  }
}
// Ashraf END //

// Tarkesh start //
/*
    *   it gives list of Audit Issues
    *   output result : [{}]                         //array of objects
*/

agileAuditSpaceController.getAuditIssueList = async (req, res) => {
  try {
    let auditIdParam = req.query.auditId || undefined;
    if (!auditIdParam) {
      rsHelper.manage(res, "validation error", null, httpCode.badRequest, "validation error");
      return
    } else {
      console.log("Checking for records using audit Id ["+ auditIdParam + "]");
    }
    
    auditMasterSchema.aggregate([
      { $match: { auditId: auditIdParam} },
      {
        $lookup: {
          from: "internalAuditIssues",
          localField: "auditId",
          foreignField: "auditIdParam",
          as: "audit",
        },
      },
      {
        $unwind: "$audit"
      },
    
    ]).exec(async (err, docs) => { 
      if (err) {
        console.err(err);
        rsHelper.manage(docs, error, null, httpCode.error, "Exception Occured");
      } else {
        if (docs.length == 0)  {
          rsHelper.manage(res, null, null, httpCode.notFound, "no data found");
          return
        } else{
          rsHelper.manage(res, null, docs, httpCode.success, "ok");
          return;
        }
      }
    });
  }catch (error) {
    logger.pushError(error);
    rsHelper.manage(res, error, null, 500, "Exception Occured");
  }
}


agileAuditSpaceController.getworkstepIssueList = async(req,res)=>{
  try {
    let workStepId = req.query.workStepId || undefined;
    if (!workStepId) {
      rsHelper.manage(res, "validation error", null, httpCode.badRequest, "validation error");
      return
    }else{
      workStepSchema.aggregate([
        { $match: { workStepId: workStepId } },
        {
          $lookup: {
            from: "internalAuditIssues",
            localField: "issueIds",
            foreignField: "issueId",
            as: "issueList",
          },

        },
        {
          $project: {
            "issueList":1,
            workStepId: 1,
          },
        },
      ]).exec(async (err, docs) => {
        if (err) {
          rsHelper.manage(res, err, null, httpCode.error, "Exception Occured");
        } else {
          if (!docs.length) {
            rsHelper.manage(res, null, null, httpCode.notFound, "no data found");
            return
          } else{
            rsHelper.manage(res, null, docs, httpCode.success, "ok");
          }
        }
      });
    }
    
  } catch (error) {
    rsHelper.manage(res, error, null, httpCode.error, "Exception Occured");
    return
  }
}


// Tarkesh END//

module.exports = agileAuditSpaceController;