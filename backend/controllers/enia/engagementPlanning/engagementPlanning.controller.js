const ep = require('../../../models/enia/auditMaster.schema');
const auditeeMaster = require("../../../models/common/auditeeMaster.schema");
const user = require("../../../models/users/index");
const logger = require("../../../utils/logger").logger;
const rsHelper = require("../../../utils/responseHelper");
const { progressBarCriteria } = require("../../../models/enia/progressBarCriteria.schema");
const { workStepSchema } = require('../../../models/enia/workSteps.schema');
const epController = {};

//res->res,if(error) error:null , responseData,errorcode,message
//rsHelper.manage(res, null, universe, 200, "Audit Masters found successfully");

//ashraf api
epController.getSelectedAuditInfo = async (req, res) => {
  try {

    let auditId = req.query.auditId || undefined;
    if (!auditId) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
    let auditData = await ep.auditMasterSchema.findOne({ auditId: auditId }).select('teamAndTimeline.updateMeetingDate teamAndTimeline.updateMeetingDate2 teamAndTimeline.openingMeetingDate teamAndTimeline.closingMeetingDate teamAndTimeline.finalMeetingDate teamAndTimeline.auditors teamAndTimeline.auditLead teamAndTimeline.isCompleted planningMemorandum.isCompleted auditPbc.isCompleted openingMeeting.isCompleted closingMeeting.isCompleted auditAnnouncement.isCompleted startDate -_id').lean();

    if (!Object.keys(auditData).length) {
      rsHelper.manage(res, "audit details not found.", null, 500, "audit details not found.");
      return
    }
    let updateMeetingDate = auditData.teamAndTimeline.updateMeetingDate || "";
    let openingMeetingDate = auditData.teamAndTimeline.openingMeetingDate || "";
    let closingMeetingDate = auditData.teamAndTimeline.closingMeetingDate || "";
    let finalMeetingDate = auditData.teamAndTimeline.finalMeetingDate || "";
    let updateMeetingDate2 = auditData.teamAndTimeline.updateMeetingDate2 || "";
    let auditors = ("auditors" in auditData.teamAndTimeline && auditData.teamAndTimeline.auditors.length) ? auditData.teamAndTimeline.auditors : [];
    let auditLead = ("auditLead" in auditData.teamAndTimeline && auditData.teamAndTimeline.auditLead.length) ? auditData.teamAndTimeline.auditLead : [];
    let auditTeam = [...auditLead, ...auditors];
    let isCompleted = {}
    isCompleted.teamAndTimeline = "isCompleted" in auditData.teamAndTimeline ? auditData.teamAndTimeline.isCompleted : false;
    isCompleted.openingMeeting = "isCompleted" in auditData.openingMeeting ? auditData.openingMeeting.isCompleted : false;
    isCompleted.planningMemorandum = "isCompleted" in auditData.planningMemorandum ? auditData.planningMemorandum.isCompleted : false;
    isCompleted.auditPbc = "isCompleted" in auditData.auditPbc ? auditData.auditPbc.isCompleted : false;
    isCompleted.closingMeeting = "isCompleted" in auditData.closingMeeting ? auditData.closingMeeting.isCompleted : false;
    isCompleted.auditAnnouncement = "isCompleted" in auditData.auditAnnouncement ? auditData.auditAnnouncement.isCompleted : false;
    isCompleted.auditProgram = isCompleted.openingMeeting || isCompleted.closingMeeting;
    // rsHelper.manage(res, null, { auditleadList, auditorList, pbcContact, auditeeList }, 200, "ok");
    let respData = {
      auditTeam, updateMeetingDate, openingMeetingDate, closingMeetingDate, finalMeetingDate, isCompleted, updateMeetingDate2,
      startDate: auditData.startDate
    }
    rsHelper.manage(res, null, respData, 200, "ok");
    return
  } catch (error) {
    console.log("@@ error @@ #", error)
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

epController.getAuditTeam = async (req, res) => {
  try {
    let auditableEntity = req.query.auditableEntity || undefined;
    let businessProcess = req.query.businessProcess || undefined;
    if (!auditableEntity || !businessProcess) {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
    let auditeeList = await auditeeMaster.find({}).select('name image email auditableEntity businessProcess -_id').exec();
    // if (!auditeeList.length) {
    //     rsHelper.manage(res, "no auditee data found.", null, 500, "no auditee data found.");
    //     return
    // }
    let pbcContact = [];
    auditeeList.map((obj) => {
      if ((obj.auditableEntity == auditableEntity) && (obj.businessProcess == businessProcess)) {
        pbcContact.push(obj);
      }
    });
    // if (!pbcContact.length) {
    //     rsHelper.manage(res, "no pbc contact data found.", null, 500, "no pbc contact found.");
    //     return
    // }
    let users = await user.find({
      "$and": [
        {
          "auditableEntity": auditableEntity
        },
        {
          "businessProcess": businessProcess
        }
      ]
    }).lean();


    let auditleadList = [], auditorList = [];
    users.map((obj) => {
      if (obj.role == "auditLead") {
        obj.name = obj.username;
        auditleadList.push(obj)
      }
      if (obj.role == "auditor") {
        obj.name = obj.username;
        auditorList.push(obj)
      }
    });
    // if (!auditleadList.length) {
    //     rsHelper.manage(res, "no audit lead data found.", null, 500, "no audit lead data found.");
    //     return
    // }

    // if (!auditorList.length) {
    //     rsHelper.manage(res, "no auditor data found.", null, 500, "no auditor data found.");
    //     return
    // }

    rsHelper.manage(res, null, { auditleadList, auditorList, pbcContact, auditeeList }, 200, "ok");
    return
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

epController.getProgressBarConfig = async (req, res) => {
  try {
    let data = await progressBarCriteria.find().select("-_id");
    if (!data.length) {
      rsHelper.manage(res, "Progress bar configuration not found.", null, 500, "Progress bar configuration not found.");
      return
    }
    rsHelper.manage(res, null, data, 200, "ok");
    return
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

epController.getCurrentYearAuditList = async (req, res) => {
  try {
    let currentyear = new Date().getFullYear();
    let auditData = await ep.auditMasterSchema.find(
      {
        "year": currentyear,
        "startDate": {
          "$exists": true
        },
        "isActive": true
      }
    ).select('-_id auditableEntity auditId category.levelFive category.businessProcess teamAndTimeline.isCompleted planningMemorandum.isCompleted auditPbc.isCompleted openingMeeting.isCompleted closingMeeting.isCompleted auditAnnouncement.isCompleted').lean();

    let finalData = [];
    auditData.map((auditDetails) => {
      let data = { /*isCompleted: {}*/ };
      data.auditId = auditDetails.auditId;
      data.auditableEntity = auditDetails.auditableEntity;
      data.siteName = auditDetails.category.levelFive.data;
      data.businessProcess = auditDetails.category.businessProcess;
      // data.isCompleted.teamAndTimeline = "isCompleted" in auditDetails.teamAndTimeline ? auditDetails.teamAndTimeline.isCompleted : false;
      // data.isCompleted.openingMeeting = "isCompleted" in auditDetails.openingMeeting ? auditDetails.openingMeeting.isCompleted : false;
      // data.isCompleted.planningMemorandum = "isCompleted" in auditDetails.planningMemorandum ? auditDetails.planningMemorandum.isCompleted : false;
      // data.isCompleted.auditPbc = "isCompleted" in auditDetails.auditPbc ? auditDetails.auditPbc.isCompleted : false;
      // data.isCompleted.closingMeeting = "isCompleted" in auditDetails.closingMeeting ? auditDetails.closingMeeting.isCompleted : false;
      // data.isCompleted.auditAnnouncement = "isCompleted" in auditDetails.auditAnnouncement ? auditDetails.auditAnnouncement.isCompleted : false;
      // data.isCompleted.auditProgram = data.isCompleted.openingMeeting || data.isCompleted.closingMeeting;
      finalData.push(data);
    });
    rsHelper.manage(res, null, finalData, 200, "ok");
    return
  } catch (error) {
    rsHelper.manage(res, error, null, 500, "Exception Occured");
    return
  }
}

//SAGAR API's
//SAMPLE CREATE AUDIT FOR TESTING
epController.createAudit = async (req, res) => {
  try {
    let auditMasterColl = await ep.auditMasterSchema(req.body);
    await auditMasterColl.save(async (err, resp) => {
      if (err) {
        logger.error(error);
        rsHelper.manage(res, error, null, 500, "Exception Occured");
      } else {
        rsHelper.manage(res, null, resp, 200, "Audit Masters found successfully");
      }
    });
  } catch (e) {
    logger.error(e);
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//TEST API 
epController.test = async (req, res) => {
  try {
    let response = await ep.auditMasterSchema.find();
    res.send({ message: 'success', data: response });
  } catch (e) {
    res.send(e);
  }
}

//getTeamAndTimeLine API
epController.getTeamAndTimeLine = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      ep.auditMasterSchema.find(
        { auditId: req.body.auditId },
        {
          auditableEntity: 1,
          auditId: 1,
          isActive: 1,
          isCompleted: 1,
          teamAndTimeline: 1,
          category: 1,
          startDate: 1
        }, function (err, docs) {
          if (err) {
            rsHelper.manage(res, error, null, 500, "Exception Occured");
          } else {
            if (!docs.length) {
              rsHelper.manage(res, "no team and timeline data found.", null, 500, "no data found.");
              return
            } else {
              rsHelper.manage(res, null, docs, 200, "Team and Timeline data found successfully");
            }

          }
        });
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  }
  catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}
//planning and memorandum
epController.getMemorandumData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      ep.auditMasterSchema.find(
        { auditId: req.body.auditId },
        {
          auditableEntity: 1,
          auditId: 1,
          isActive: 1,
          isCompleted: 1,
          teamAndTimeline: 1,
          planningMemorandum: 1,
          category: 1
        }, function (err, docs) {
          if (err) {
            rsHelper.manage(res, error, null, 500, "Exception Occured");
          } else {
            if (!docs.length) {
              rsHelper.manage(res, "no planning and memorandum data found.", null, 500, "no data found.");
              return
            } else {
              rsHelper.manage(res, null, docs, 200, "planning and memorandum data found successfully");
            }

          }
        });
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  }
  catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");

  }
}

//Announcement data
epController.getAnnouncementData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      ep.auditMasterSchema.find(
        { auditId: req.body.auditId },
        {
          auditableEntity: 1,
          auditId: 1,
          isActive: 1,
          isCompleted: 1,
          teamAndTimeline: 1,
          auditAnnouncement: 1,
          category: 1
        }, function (err, docs) {
          if (err) {
            rsHelper.manage(res, error, null, 500, "Exception Occured");
          } else {
            if (!docs.length) {
              rsHelper.manage(res, "no audit Announcement data found.", null, 500, "no data found.");
              return
            } else {
              rsHelper.manage(res, null, docs, 200, "Audit Announcement data found successfully");
            }

          }
        });
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  }
  catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");

  }
}
//Opening meeting data
epController.getOpenMeetUpData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      ep.auditMasterSchema.find(
        { auditId: req.body.auditId },
        {
          auditableEntity: 1,
          auditId: 1,
          isActive: 1,
          isCompleted: 1,
          teamAndTimeline: 1,
          openingMeeting: 1,
          category: 1
        }, function (err, docs) {
          if (err) {
            rsHelper.manage(res, error, null, 500, "Exception Occured");
          } else {
            if (!docs.length) {
              rsHelper.manage(res, "no opening meeting data found.", null, 500, "no data found.");
              return
            } else {
              rsHelper.manage(res, null, docs, 200, "Opening meeting data found successfully");
            }


          }
        });
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  }
  catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");

  }
}
//Closing meeting data
epController.getCloseMeetData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      ep.auditMasterSchema.find(
        { auditId: req.body.auditId },
        {
          auditableEntity: 1,
          auditId: 1,
          isActive: 1,
          isCompleted: 1,
          teamAndTimeline: 1,
          closingMeeting: 1,
          category: 1
        }, function (err, docs) {
          if (err) {
            rsHelper.manage(res, error, null, 500, "Exception Occured");
          } else {
            if (!docs.length) {
              rsHelper.manage(res, "no closing meeting data found.", null, 500, "no data found.");
              return
            } else {
              rsHelper.manage(res, null, docs, 200, "Closing meeting data found successfully");
            }


          }
        });
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//------------------------------------------------------------------------
//save team and timeline
epController.saveTeamAndTimeLine = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;

    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { teamAndTimeline: 1 });

      if (!audit) {
        rsHelper.manage(res, "no team and timeline data found.", null, 500, "no data found.");
        return
      } else {
        audit.teamAndTimeline.isCompleted = req.body.isCompleted;
        //auditors
        if (req.body.auditors) {
          audit.teamAndTimeline.auditors = req.body.auditors;
        }
        //auditLead
        if (req.body.auditLead) {
          // assign auditLead to duplicate WS
          audit.teamAndTimeline.auditLead = req.body.auditLead;
          if (req.body.auditLead.length == 1) {
            var dbuser = await user.findOne({ username: req.body.auditLead[0] }, { username: 1, profileUrl: 1 });
            var profileUrl = dbuser.profileUrl;
            var duplicatedWs = await workStepSchema.find({ auditId: req.body.auditId, origin: 'duplicate', 'reviewer.username': '' });
            if (duplicatedWs && duplicatedWs.length > 0) {
              for (const ws of duplicatedWs) {
                ws.reviewer.username = req.body.auditLead[0];
                ws.reviewer.profileUrl = profileUrl;
                await ws.save();
              }
            }
          }
        }
        //openingMeetingDate
        if (req.body.openingMeetingDate) {
          audit.teamAndTimeline.openingMeetingDate = req.body.openingMeetingDate;
        }
        //periodInScope
        if (req.body.periodInScope) {
          audit.teamAndTimeline.periodInScope = req.body.periodInScope;
        }
        //performancePeriod
        if (req.body.updateMeeting2) {
          audit.teamAndTimeline.updateMeetingDate2 = req.body.updateMeeting2;
        }
        //finalMeetingDate
        if (req.body.finalMeetingDate) {
          audit.teamAndTimeline.finalMeetingDate = req.body.finalMeetingDate;
        }
        //updateMeetingDate
        if (req.body.updateMeetingDate) {
          audit.teamAndTimeline.updateMeetingDate = req.body.updateMeetingDate;
        }
        if (req.body.closingMeetingDate) {
          audit.teamAndTimeline.closingMeetingDate = req.body.closingMeetingDate;
        }
        //pbc
        if (req.body.pbc) {
          audit.teamAndTimeline.pbc = req.body.pbc;
        }
        //auditees
        if (req.body.auditees) {
          audit.teamAndTimeline.auditees = req.body.auditees;
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Team and timeline updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })


      }


    } else {
      rsHelper.manage(res, {}, null, 500, "Audit id is missing.");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//save memorandum
epController.saveMemorandumData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { planningMemorandum: 1 });
      if (!audit) {
        rsHelper.manage(res, "no memorandum data found.", null, 500, "no data found.");
        return
      } else {
        audit.planningMemorandum.auditObjectives = req.body.auditObjectives;
        audit.planningMemorandum.auditScope = req.body.auditScope;
        audit.planningMemorandum.outOfScope = req.body.outOfScope;
        audit.planningMemorandum.sentOn = req.body.sentOn;
        audit.planningMemorandum.preparedBy = req.body.preparedBy;
        audit.planningMemorandum.sentBy = req.body.sentBy;
        audit.planningMemorandum.isCompleted = req.body.isCompleted;

        //attachment
        if (req.body.attachments) {
          audit.planningMemorandum.attachments = req.body.attachments;
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Memorandum updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//save saveOpenMeetUpData
epController.saveOpenMeetUpData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { openingMeeting: 1 });
      if (!audit) {
        rsHelper.manage(res, "no memorandum data found.", null, 500, "no data found.");
        return
      } else {
        audit.openingMeeting.auditScope = req.body.auditScope;
        audit.openingMeeting.isCompleted = req.body.isCompleted;

        audit.openingMeeting.agenda = req.body.agenda;
        audit.openingMeeting.quesAndAns = req.body.quesAndAns;
        audit.openingMeeting.isCompleted = req.body.isCompleted;
        audit.openingMeeting.sentOn = req.body.sentOn;
        audit.openingMeeting.preparedBy = req.body.preparedBy;
        audit.openingMeeting.sentBy = req.body.sentBy;
        //attachment
        if (req.body.attachments) {
          audit.openingMeeting.attachments = req.body.attachments;
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Opening meeting updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//save saveAnnouncementData
epController.saveAnnouncementData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditAnnouncement: 1 });
      if (!audit) {
        rsHelper.manage(res, "no announcement data found.", null, 500, "no data found.");
        return
      } else {
        audit.auditAnnouncement.text = req.body.text;
        audit.auditAnnouncement.text2 = req.body.text2;
        audit.auditAnnouncement.agenda = req.body.agenda;
        audit.auditAnnouncement.closing = req.body.closing;
        audit.auditAnnouncement.isCompleted = req.body.isCompleted;
        audit.auditAnnouncement.preparedBy = req.body.preparedBy;
        //attachment
        if (req.body.attachments) {
          audit.auditAnnouncement.attachments = req.body.attachments;
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit announcement updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}

//saveCloseMeetData
epController.saveCloseMeetData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { closingMeeting: 1 });
      if (!audit) {
        rsHelper.manage(res, "no closing meeting data found.", null, 500, "no data found.");
        return
      } else {
        audit.closingMeeting.agenda = req.body.agenda;
        audit.closingMeeting.quesAndAns = req.body.quesAndAns;
        audit.closingMeeting.issueActionReview = req.body.issueActionReview;
        audit.closingMeeting.comments = req.body.comments;
        audit.closingMeeting.isCompleted = req.body.isCompleted;
        //attachment
        if (req.body.attachments) {
          audit.closingMeeting.attachments = req.body.attachments;
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Closing meeting updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
}




module.exports = epController;