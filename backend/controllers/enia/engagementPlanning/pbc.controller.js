const Jwt = require('jsonwebtoken');
const CollabChatSystem = require("../../../models/chatSystem/collabChatSystem");
const AuditMaster = require("../../../models/enia/auditMaster.schema");
const AuditeeMaster = require("../../../models/common/auditeeMaster.schema");
const User = require("../../../models/users/index");
const config = require("../../../config/config");
const fs = require("fs");
var handlebars = require("handlebars");
const { request } = require("http");
var nodemailer = require("nodemailer");
const path = require("path");
var moment = require("moment");
const ep = require("../../../models/enia/auditMaster.schema");
const dmsController = require('../../../controllers/dms/dms.controller');
const logger = require("../../../utils/logger").logger;
const rsHelper = require("../../../utils/responseHelper");
const pbcController = {};
const privateKey = config.key.privateKey;

//////////////////////// Save pbc Api ///////////////////////////
pbcController.updatePbcDoc = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, auditableEntity: 1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {
        audit.auditPbc = req.body.auditPbc
        // audit.auditPbc.auditScope = req.body.auditScope;
        // audit.auditPbc.documentRequest = req.body.documentRequest;
        // audit.auditPbc.documentList = req.body.documentList;
        // audit.auditPbc.preparer = req.body.preparer;
        // audit.auditPbc.reviewer = req.body.reviewer;
        // audit.auditPbc.dueDate = req.body.reviewer;


        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};


//////////////////////////////// (attachments)save dms info /////////////////////
pbcController.updatePbcAttachment = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, documentList:1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {

        audit.auditPbc.documentList = req.body.documentList;



        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

///////////////////////// save pbc api for popup /////////////////
pbcController.updatePbcPopup = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, documentList: 1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {

        audit.auditPbc.documentList = req.body.documentList;

        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};



/////////////////////// save api for Remark in Table ///////////


pbcController.updatePbcRemark = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, documentList: 1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {

        audit.auditPbc.documentList.remarks = req.body.remarks;

        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

/////////////////////////Get Pbc Info ///////////////////////
pbcController.updatePbcInfo = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, auditableEntity: 1, category: 1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

///////////////////// get the data pbc table ////////////////

pbcController.getPbcData = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditId: 1, auditPbc: 1, auditableEntity: 1, category: 1, "teamAndTimeline.periodInScope": 1,"teamAndTimeline.pbc":1 });
      if (!audit) {
        rsHelper.manage(res, null, null, 400, "no data found.");
        return
      } else {
        // audit.save().then((doc) => {
        rsHelper.manage(res, null, audit, 200, "Audit PBC fetched successfully");
        // }).catch((err) => {
        //   rsHelper.manage(res, err, null, 500, "Exception Occured");
        //   return
        // })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

///////////////delete Pbc Api /////////////////////////////


pbcController.deletePbcEntry = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditPbc: 1 });
      if (!audit) {
        rsHelper.manage(res, "no pbc data found.", null, 500, "no data found.");
        return
      } else {
        const index = parseInt(req.body.rowIndex);
        if (index > -1) {
          audit.auditPbc.documentList.splice(index, 1);
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};



///////////////// pbc Attachment Api ////////////////

pbcController.deletePbcAttachment = async (req, res) => {
  try {
    let auditId = req.body.auditId || undefined;
    if (auditId) {
      var audit = await ep.auditMasterSchema.findOne({ auditId: req.body.auditId }, { auditPbc: 1 });
      if (!audit) {
        rsHelper.manage(res, null, null, 500, "no Pbc data found.");
        return
      } else {
        const index = parseInt(req.body.rowIndex);
        if (index > -1) {
          audit.auditPbc.documentList[index].dmsInfo = {};
        }
        audit.save().then((doc) => {
          rsHelper.manage(res, null, doc, 200, "Audit PBC updated successfully");
        }).catch((err) => {
          rsHelper.manage(res, err, null, 500, "Exception Occured");
          return
        })
      }
    } else {
      rsHelper.manage(res, "validation error", null, 500, "validation error");
      return
    }
  } catch (e) {
    rsHelper.manage(res, e, null, 500, "Exception Occured");
  }
};

pbcController.generateTokenForPBC = async (req, res) => {
  console.log("[GJ] Inside generateTokenForPBC ==> " + req.body.invited_by);
  var dateTimeNow = new Date();

  const tokenData = {
    invited_on: dateTimeNow,
    audit_id: req.body.audit_id,
  };


  AuditMaster.auditMasterSchema.find({ auditId: req.body.audit_id }, { auditPbc: 1 }, function (err, docs) {
    if (err) {
      rsHelper.manage(res, err, null, 404, "No audit found for ID " + req.body.audit_id);
      // res.status(404).send({ message: "No audit found for ID " + req.body.audit_id });
    } else {
      var doc = docs[0];
      var emailAddresses = [];
      var prevDueDate = new Date();
      var currentDueDate = new Date();
      var maxDueDate = new Date();
      if (doc) {
        console.log("DOC is valid");
      }

      if (doc && doc.auditPbc && doc.auditPbc.preparer && doc.auditPbc.preparer.length > 0) {
        for (var i = 0; i < doc.auditPbc.preparer.length; i++) {
          AuditeeMaster.findOne({ name: doc.auditPbc.preparer[i] }, function (err1, auditee) {
            if (auditee) {
              emailAddresses.push(auditee.email);
            }
          });
        }
      }

      if (doc && doc.auditPbc && doc.auditPbc.documentList && doc.auditPbc.documentList.length > 0) {
        for (i = 0; i < doc.auditPbc.documentList.length; i++) {
          currentDueDate = doc.auditPbc.documentList[i].dueDate;
          if (currentDueDate > prevDueDate) {
            maxDueDate = currentDueDate;
          }
          prevDueDate = currentDueDate;
        }
        console.log("[PBC] Setting the Max Due Date : " + maxDueDate);
      } else {
        console.log("DocumentList NOT found for audit " + req.body.audit_id);
      }

      maxDueDate.setHours(23);
      maxDueDate.setMinutes(59);

      var expiryPeriod = Math.round((maxDueDate.getTime() - new Date().getTime()) / 1000);

      console.log("*******************************************************************************");
      console.log("PBC Token Expires In = [" + expiryPeriod + "], Timestamp = [" + maxDueDate + "]");
      console.log("*******************************************************************************");

      const result = {
        token: Jwt.sign(tokenData, privateKey, {
          expiresIn: expiryPeriod,
        }),
      };
      doc.auditPbc.pbcAuditeeURLToken = result.token;

      doc.save().then(async () => {
        let emailParamObj = {};
        for (i = 0; i < emailAddresses.length; i++) {
          console.log("Sending email to " + emailAddresses[i] + " with token " + result.token);
          emailParamObj.username = doc.auditPbc.preparer[i];
          emailParamObj.emailId = emailAddresses[i];
          emailParamObj.token = result.token;
          emailParamObj.requestor = req.body.requestor_id;
          emailParamObj.earliestDate = new Date();
          emailParamObj.expiryDate = maxDueDate;
          await sendMailInviteToPBC(emailParamObj);
        }
        rsHelper.manage(res, null, result.token, 200, "Mail Sent Successfully");
        // res.send({
        //   message: `Mail Sent Successfully to ${emailAddresses}`,
        //   token: result.token,
        // });
      });
    }
  }).select(" _id auditId");

  async function sendMailInviteToPBC(emailParamObj) {
    var url = process.env.FRONTEND_URL;
    var emailId = emailParamObj.emailId;
    readHTMLFile(
      path.join(__dirname, "../../../helpers/pbc.html"),
      function (err, html) {
        var template = handlebars.compile(html);

        var htmlToSend = template({
          username: emailParamObj.username,
          url: `${url}/auditee-pbc-form?urlToken=` + emailParamObj.token,
          requestor: emailParamObj.requestor,
          earliestDate : emailParamObj.earliestDate,
          expiryDate : emailParamObj.expiryDate          
        });
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "enrico.mailer",
            pass: "Orion@123",
          },
        });
        let froms = `EnIA Mailer<enrico.mailer@gmail.com>`;
        const mailOptions = {
          from: froms, // sender address
          to: emailId, //element.email, //receivers
          subject: `Invite to Upload PBC Documents`, // Subject line
          attachments: [
            {
              filename: "logo-enia.png",
              path: path.join(__dirname, "../../../helpers/logo-enia.png"),
              cid: "logo",
            },
          ],
          html: htmlToSend,
        };
        logger.pushConsoleInfo("mailOptions", mailOptions);
        transporter.sendMail(mailOptions, function (err2, info) {
          if (err2) logger.pushConsoleInfo(err);
          else {
            logger.pushConsoleInfo("Mail sent to :" + info);
            logger.pushConsoleInfo(info);
          }
        });
      }
    );
  }

  var readHTMLFile = function (htmlPath, callback) {
    fs.readFile(htmlPath, { encoding: "utf-8" }, function (err, html) {
      if (err) {
        // throw err;
        callback(err);
      } else {
        callback(null, html);
      }
    });
  };
  // res.send({ message: `Mail Sent Successfully to ${req.body.auditee_mail_id}` });
  // res.send({ result: result, tokenData: tokenData });
};

pbcController.checkPBCToken = (req, res) => {
  try {
    var token = req.body.Token;

    const claims = Jwt.decode(token);
    const d = new Date(0);
    d.setUTCSeconds(claims.exp);
    logger.pushConsoleInfo("Expiry Time : " + d);
    logger.pushConsoleInfo("Curr time : " + new Date());
    if (new Date() > d) {
      rsHelper.manage(res, null, null, 200, "[PBC] Invalid Token / Token Expired.");
      return;
    }
    // logger.pushConsoleInfo(moment(moment.duration(claims.exp.diff(claims.iat))).format("ss"))
    AuditMaster.auditMasterSchema.find(
      {
        auditId: claims.audit_id,
        "auditPbc.pbcAuditeeURLToken" : token,
      }, { auditPbc : 1 },
      function (err, docs) {
        if (docs && docs.length > 0) {
          rsHelper.manage(res, null, {
            emailId: claims.pbc_mail_id,
            auditId: claims.audit_id,
          }, 200, "[PBC] Token Verified.");
          return;
        } else {
          rsHelper.manage(res, null, null, 200, "[PBC] Invalid Token / Token Expired.");
          return;
        }
      }
    );
  } catch (error) {
    logger.pushError(error);
    rsHelper.manage(res, error, null, 500, "Internal Server Error.");
    // res.send(error);
  }
};
// Added by Ganesh for Generating Token for PBC invite - 29-Sep-2020 - End

module.exports = pbcController;