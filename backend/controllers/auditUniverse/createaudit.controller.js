const auditDetails = require("../../models/auditUniverse/createaudits.schema");
const logger = require("../../utils/logger").logger;

exports.getAllCreateAudit = async (req, res) => {
  try {
    auditDetails.audit.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};