const universeTableData = require("../../models/auditUniverse/auditUniverseTable.schema");
const logger = require("../../utils/logger").logger;

exports.getuniverseData = (req, res) => {
  var data = new Array();
  universeTableData.find((err, universeDataInfo) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      for (var i = 0; i < universeDataInfo.length; i++) {
        if (universeDataInfo[i].value) {
          data.push({
            id: universeDataInfo[i].id,
            parent: universeDataInfo[i].parent,
            name: universeDataInfo[i].name,
            value: universeDataInfo[i].value,
          });
        } else {
          data.push({
            id: universeDataInfo[i].id,
            parent: universeDataInfo[i].parent,
            name: universeDataInfo[i].name,
          });
        }
      }
      res.send(JSON.stringify(data));
    }
  });
};

exports.postuniverseData = (req, res) => {
  let newuniverseDataInfo = new universeTableData({
    id: req.body.id,
    name: req.body.name,
    parent: req.body.parent,
    value: req.body.value,
  });
  newuniverseDataInfo.save((err, universeDataInfo) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      res.json({ msg: "Info Added successfully!" });
    }
  });
};