const universeData = require("../../models/enia/universe.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const issuesDb = require("../../models/IssueActions/issue.schema");
const actionDb = require("../../models/IssueActions/actions.schema");
const logger = require("../../utils/logger").logger;

exports.getuniverseData = (req, res) => {
  var data = new Array();
  universeData.find((err, universeDataInfo) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      for (var i = 0; i < universeDataInfo.length; i++) {
        if (universeDataInfo[i].value) {
          data.push({
            id: universeDataInfo[i].id,
            parent: universeDataInfo[i].parent,
            name: universeDataInfo[i].name,
            value: universeDataInfo[i].value,
            childrenId: universeDataInfo[i].childrenId,
          });
        }
        if (universeDataInfo[i].childrenId) {
          data.push({
            id: universeDataInfo[i].id,
            parent: universeDataInfo[i].parent,
            name: universeDataInfo[i].name,
            childrenId: universeDataInfo[i].childrenId,
          });
        }
      }
      logger.pushConsoleInfo("Universe data sent successfully!");
      res.send(JSON.stringify(data));
    }
  });
};
exports.setCurrentYearAuditValue = (req, res) => {
  try {
    universeData.updateOne(
      { _id: req.body._id },
      { $set: { currentYearAudit: true } },
      function (err, data) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          logger.pushConsoleInfo("setCurrentYearAuditValue Data:", data);
          res.send(data);
        }
      }
    );
  } catch (err) {
    logger.pushError(err);
    return res.status(406).send(err);
  }
};
exports.postuniverseData = (req, res) => {
  let newuniverseDataInfo = new universeData({
    id: req.body.id,
    name: req.body.name,
    parent: req.body.parent,
    value: req.body.value,
  });
  newuniverseDataInfo.save((err, universeDataInfo) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      res.json({ msg: "Info Added successfully!" });
    }
  });
};
exports.getAllTableDataByRiskLevel = (req, res) => {
  try {
    if (req.query.getScope) {
      universeData
        .aggregate([
          { $match: { auditInfo: { $exists: true } } },
          {
            $lookup: {
              from: "audit_master",
              localField: "auditInfo.currentAudit",
              foreignField: "auditId",
              as: "auditMaster",
            },
          },
        ])
        .exec(function (err, data) {
          if (err) {
            logger.pushError(err);
            return handleError(err);
          }
          res.status(200).json(data);
        });
      // universeData.find(
      //   { auditInfo: { $exists: true } },
      //   { name: 1, auditInfo: 1, type: 1, parent: 1, id: 1 },
      //   (err, data) => {
      //     if (err) return handleError(err);
      //     // logger.pushConsoleInfo("getTableData dada:", data);
      //     res.send(data);
      //   }
      // );
    } else {
      universeData.find(
        { auditInfo: { $exists: true } },
        { name: 1, auditInfo: 1, type: 1, parent: 1, id: 1 },
        (err, data) => {
          if (err) {
            logger.pushError(err);
            return handleError(err);
          }
          // logger.pushConsoleInfo("getTableData dada:", data);
          res.send(data);
        }
      );
    }
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
exports.getTableData = (req, res) => {
  try {
    universeData.find(
      { id: { $in: req.body.id } },
      { name: 1, auditInfo: 1, type: 1, parent: 1, id: 1 },
      (err, data) => {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        if (data) {
          res.send(data);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

exports.getGalaxyData = async (req, res) => {
  var audits = [];

  for (let i = 0; i < req.body.oldAuditIds.length; i++) {
    await internalAudit.auditMasters
      .findOne({ auditId: req.body.oldAuditIds[i] })
      .select(" auditId draftReport.adr_rating issueIds")
      .then(async (audit) => {
        if (audit) {
          var faudit = {
            auditId: audit.auditId,
            rating: audit.draftReport.adr_rating,
            issues: [],
          };
          // audits.push();
          for (let j = 0; j < audit.issueIds.length; j++) {
            await issuesDb.issue
              .findOne({ issueId: audit.issueIds[j] })
              .select(" auditId issueId issuePriority actionId ")
              .then(async (issue) => {
                if (issue) {
                  var fissue = {
                    issueId: issue.issueId,
                    priority: issue.issuePriority,
                    actions: [],
                  };

                  for (let k = 0; k < issue.actionId.length; k++) {
                    await actionDb.action
                      .findOne({ actionId: issue.actionId[k] })
                      .select(" actionId rating ")
                      .then(async (action) => {
                        if (action) {
                          var faction = {
                            actionId: action.actionId,
                            rating: action.rating,
                          };
                          fissue.actions.push(faction);
                        }
                      });
                  }
                }
                faudit.issues.push(fissue);
              });
          }
          audits.push(faudit);
        }
      });
  }
  await res.send(audits);
};

// exports.getTableData = async (req, res) => {
//   try {
//     var auditTableData = [];
//     if (req.body.id.length) {
//       for (let i = 0; i < req.body.id.length; i++) {
//         logger.pushConsoleInfo(req.body.id[i]);
//         await internalAudit.auditMasters.find(
//           { uID: req.body.id[i] },
//           (err, data) => {
//             if (err) logger.pushConsoleInfo(err);
//             if (data.length) {
//               auditTableData.push(data);
//             }
//           }
//         );
//       }
//       await res.send(auditTableData);
//     }
//   } catch (e) {
// logger.pushError(e);
//     return res.status(406).send(e);
//   }
// };