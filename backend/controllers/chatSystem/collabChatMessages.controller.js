const CollabChatMessages = require("../../models/chatSystem/collabChatMessages");
const CollabChatSystem = require("../../models/chatSystem/collabChatSystem");
const Jwt = require("jsonwebtoken");
const config = require("../../config/config");
const fs = require("fs");
var handlebars = require("handlebars");
const { request } = require("http");
var nodemailer = require("nodemailer");
const path = require("path");
var moment = require("moment");
const privateKey = config.key.privateKey;
const logger = require("../../utils/logger").logger;
exports.getAllAuditChatroomMessages = (id) => {
  return new Promise((resolve, reject) => {
    CollabChatMessages.findOne({ roomId: id }, { roomId: 1, messages: 1 })
      .then((msgs) => {
        // logger.pushConsoleInfo(msgs);
        resolve(msgs);
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });
};

//------------------Get Message by roomId -------------------------------
exports.getMessagesByRoomId = (roomId) => {
  if (!roomId) {
    return false;
  }
  // logger.pushConsoleInfo(roomId);
  return new Promise((resolve, reject) => {
    CollabChatMessages.findOne({ roomId: id }, { roomId: 1, messages: 1 })
      .then((msgs) => {
        // logger.pushConsoleInfo(msgs);
        resolve(msgs);
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });
};
//------------------save new Message by roomId -------------------------------

exports.putMessagesByRoomId = (message) => {
  if (!message) {
    return false;
  }
  const { roomId, messages } = message;
  // const newMessage = new CollabChatMessages({
  //   roomId,
  //   messages,
  // });
  // return new Promise((resolve, reject) => {
  //   newMessage.save().then((data) => {
  //     CollabChatMessages.findOne({ roomId })
  //       .then((chatHistory) => {
  //         logger.pushConsoleInfo(chatHistory);
  //         resolve(chatHistory);
  //       })
  //       .catch((err) => {
  //   logger.pushError(err);
  //   reject(err);
  // });
  //   });
  // });
  return new Promise((resolve, reject) => {
    CollabChatMessages.update(
      { roomId },
      { $push: { messages: messages } },
      { upsert: true }
    )
      .then((res) => {
        CollabChatMessages.findOne({ roomId })
          .then((chatHistory) => {
            // logger.pushConsoleInfo(chatHistory);
            resolve(chatHistory);
          })
          .catch((err) => {
            logger.pushError(err);
            reject(err);
          });
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });

  // return new Promise((resolve, reject) => {
  //   newMessage.save().then((data) => {
  // CollabChatMessages.findOne({ roomId })
  //   .then((chatHistory) => {
  //     logger.pushConsoleInfo(chatHistory);
  //     resolve(chatHistory);
  //   })
  //   .catch((err) => {
  //   logger.pushError(err);
  //   reject(err);
  // });
  //   });
  // });

  //   CollabChatMessages.findOne({ roomId }).then((chatHistory) => {
  //     io.emit("chatroomMessage", chatHistory);
  //   });
  // })
  // .catch((err) => {
  //   io.emit(
  //     "chatroomMessage",
  //     err.message || "Some error occurred while creating the message."
  //   );
  // });
};

exports.setUserJoinedMessage = (message) => {
  logger.pushConsoleInfo("userJoined:", message);
  return new Promise((resolve, reject) => {
    const { roomId, messages } = message;
    CollabChatMessages.findOne({
      roomId,
      messages: {
        $elemMatch: {
          username: messages.username,
          newJoined: true,
        },
      },
    })
      .then((res) => {
        logger.pushConsoleInfo("found user:", res);
        if (!res) {
          CollabChatMessages.update(
            { roomId },
            { $push: { messages: messages } },
            { upsert: true }
          )
            .then((response) => {
              CollabChatMessages.findOne({ roomId })
                .then((chatHistory) => {
                  // logger.pushConsoleInfo(chatHistory);
                  resolve(chatHistory);
                })
                .catch((err) => {
                  logger.pushError(err);
                  reject(err);
                });
            })
            .catch((err) => {
              logger.pushError(err);
              reject(err);
            });
        }
      })
      .catch((err) => {
        logger.pushConsoleInfo("no user:", err);
      });
  });
};

exports.generateTokenForAuditee = async (req, res) => {
  var dateTimeNow = new Date();

  const tokenData = {
    invited_by: req.body.invited_by,
    auditee_mail_id: req.body.auditee_mail_id,
    invited_on: dateTimeNow,
    audit_id: req.body.audit_id,
  };

  const result = {
    username: req.body.unique_request_id,
    token: Jwt.sign(tokenData, privateKey, {
      expiresIn: process.env.AUDITEE_TOKEN_TIME,
    }),
  };

  CollabChatSystem.find({ auditId: req.body.audit_id }, function (err, docs) {
    if (err) {
      res.status(404).send({ message: "No Room for this audit id found" });
    } else {
      var doc = docs[0];
      if (doc && doc.participants && doc.participants.length > 0) {
        var p = doc.participants.findIndex(
          (p) => p.username == req.body.auditee_mail_id
        );
        if (p >= 0) {
          doc.participants[p].invite_token = result.token;
          doc.participants[p].invited_on = dateTimeNow;
        } else {
          doc.participants.push({
            username: req.body.auditee_mail_id,
            invite_token: result.token,
            invited_on: dateTimeNow,
          });
          doc.sessionStartedOn = dateTimeNow;
        }
      } else {
        doc.participants = [
          {
            username: req.body.auditee_mail_id,
            invite_token: result.token,
            invited_on: dateTimeNow,
          },
        ];
        doc.sessionStartedOn = dateTimeNow;
      }
      doc.save().then(async () => {
        await sendMailInviteToAuditee(req.body.auditee_mail_id, result.token);
        res.send({
          message: `Mail Sent Successfully to ${req.body.auditee_mail_id}`,
          token: result.token,
        });
      });
    }
  }).select(" _id participants auditId sessionStartedOn");

  async function sendMailInviteToAuditee(emailId, token) {
    var url = "115.112.185.58:3200";
    if (process.env.HOST == "localhost" || process.env.HOST == "0.0.0.0") {
      url = "localhost:4200";
    } else if (process.env.HOST == "115.112.185.58") {
      if (process.env.PORT == "3000") {
        url = "115.112.185.58:4200";
      } else {
        url = "115.112.185.58:3200";
      }
    } else if (process.env.HOST == "115.112.185.79") {
      if (process.env.PORT == "6100") {
        url = "115.112.185.79:6200";
      } else {
        url = "115.112.185.79:3200";
      }
    }
    readHTMLFile(
      path.join(__dirname, "../../helpers/auditeeInviteMailTemplate.html"),
      function (err, html) {
        var template = handlebars.compile(html);

        var htmlToSend = template({
          username: emailId,
          url: `http://${url}/auditee-chatroom?urlToken=` + token,
        });
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "enrico.mailer",
            pass: "Orion@123",
          },
        });
        let froms = `EnIA Mailer<enrico.mailer@gmail.com>`;
        const mailOptions = {
          from: froms, // sender address
          to: emailId, //element.email, //receivers
          subject: `Invite to Join Collab`, // Subject line
          attachments: [
            {
              filename: "logo-enia.png",
              path: path.join(__dirname, "../../helpers/logo-enia.png"),
              cid: "logo",
            },
          ],
          html: htmlToSend,
        };
        logger.pushConsoleInfo("mailOptions", mailOptions);
        transporter.sendMail(mailOptions, function (err, info) {
          if (err) logger.pushConsoleInfo(err);
          else {
            logger.pushConsoleInfo("Mail sent to :" + info);
            logger.pushConsoleInfo(info);
          }
        });
      }
    );
  }

  var readHTMLFile = function (path, callback) {
    fs.readFile(path, { encoding: "utf-8" }, function (err, html) {
      if (err) {
        // throw err;
        callback(err);
      } else {
        callback(null, html);
      }
    });
  };
  // res.send({ message: `Mail Sent Successfully to ${req.body.auditee_mail_id}` });
  // res.send({ result: result, tokenData: tokenData });

  // res.send('hello world');
};

exports.checkAuditeeToken = (req, res) => {
  try {
    var token = req.body.Token;

    const claims = Jwt.decode(token);
    const d = new Date(0);
    d.setUTCSeconds(claims.exp);
    logger.pushConsoleInfo("*************************", d);
    logger.pushConsoleInfo("curr time", new Date());
    if (new Date() > d) {
      res.status(400).send({ message: "Invalid Token / Token Expired" });
    }
    // logger.pushConsoleInfo(moment(moment.duration(claims.exp.diff(claims.iat))).format("ss"))
    CollabChatSystem.find(
      {
        auditId: claims.audit_id,
        participants: { $elemMatch: { invite_token: token } },
      },
      function (err, docs) {
        if (docs && docs.length > 0) {
          res.status(200).send({
            message: "verified",
            emailId: claims.auditee_mail_id,
            auditId: claims.audit_id,
          });
        } else {
          res.status(400).send({ message: "Invalid Token / Token Expired" });
        }
      }
    );
    // res.send(claims);
  } catch (error) {
    logger.pushError(error);
    res.status(401).send({ message: "Invalid Token / Token Expired" });
    // res.send(error);
  }
};

exports.closeAuditeeSession = (req, res) => {
  try {
    CollabChatSystem.updateOne(
      { auditId: req.body.audit_id },
      { $set: { participants: [], sessionEndedOn: new Date() } },
      function (err, response) {
        if (err) {
          logger.pushError(error);
          res.status(500).send(error.message);
        }
        res.status(200).send({ message: "Session ended" });
      }
    );
  } catch (error) {
    logger.pushError(error);
    res.status(500).send(error.message);
  }
};