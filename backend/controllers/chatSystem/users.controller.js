const User = require("../../models/users");

//Login user
exports.logInUser = (data, io) => {
  if (!data) {
    console.log("err in login user");
  }
  // let status = 0;
  User.findOneAndUpdate({ username: data }, { $set: { online: true } })
    .then(r => {
      io.emit("logInUser", { status: 1, msg: r });
    })
    .catch(err => io.emit("logInUser", { status: 0, msg: err }));
};

exports.logOutUser = (data, io) => {
  if (!data) {
    console.log("err in logout user");
  }
  // let status = 0;
  User.findOneAndUpdate({ username: data }, { $set: { online: false } })
    .then(r => {
      io.emit("logInUser", { status: 1, msg: r });
    })
    .catch(err => io.emit("logInUser", { status: 0, msg: err }));
};

// Create and Save a new user
exports.create = (data, io) => {
  // Validate request
  if (!data.title) {
    io.emit("userList", "user content cannot be empty");
  }

  // Create a user
  const user = new User({
    name: data.username || "Jon Doe",
    online: data.online
  });

  // Save user in the database
  user
    .save()
    .then(data => {
      io.emit(data);
    })
    .catch(err => {
      io.emit(
        "userList",
        err.message || "Some error occurred while creating the Room."
      );
    });
};

exports.findAllUsers = () => {
  return new Promise((resolve, reject) => {
    User.find()
      .then(users => {
        resolve(users);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.findByStatus = usernames => {
  return new Promise((resolve, reject) => {
    User.find({ username: { $in: usernames } })
      .then(users => {
        resolve(users);
      })
      .catch(err => {
        reject(err);
      });
  });
};
