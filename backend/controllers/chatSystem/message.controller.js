const Message = require("../../models/chatSystem/message");

// Create and Save a new Room
exports.create = (data, io) => {
  // Validate request
  if (!data) {
    io.emit("messageList", "message content cannot be empty");
  }

  // Create a message
  const message = new Message({
    message: data.message,
    username: data.username,
    createdAt: new Date(),
    groupId: data.groupId
  });

  // Save message in the database
  message
    .save()
    .then(data => {
      Message.find({ groupId: data.groupId }).then(messages => {
        io.emit("messageList", messages);
      });
    })
    .catch(err => {
      io.emit(
        "messageList",
        err.message || "Some error occurred while creating the message."
      );
    });
};

exports.findMessagesById = id => {
  return new Promise((resolve, reject) => {
    Message.find({ groupId: id })
      .then(messages => {
        resolve(messages);
      })
      .catch(err => reject(err));
  });
};
