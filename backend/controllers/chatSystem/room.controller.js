const Room = require("../../models/chatSystem/room");

exports.update = (data, io) => {
  if (!data.username) {
    io.emit("addUsersToRoom", "adding users to room failed");
  }
  Room.findOneAndUpdate(
    { _id: data.groupId },
    { $push: { usernames: data.username } },
    { new: true }
  )
    .then(r => {
      Room.find({ _id: data.groupId })
        .then(room => {
          io.emit("getRoomDetails", room);
        })
        .catch(err => console.log(err));
    })
    .catch(err => {
      io.emit("getRoomDetails", err);
    });
};

// Create and Save a new Room
exports.create = (data, io) => {
  // Validate request
  if (!data.title) {
    io.emit("userList", "room content cannot be empty");
  }

  // Create a room
  const room = new Room({
    title: data.title || "Untitled Room",
    usernames: data.usernames,
    createdAt: new Date(),
    owner: data.owner,
    auditId: data.auditId
  });

  // Save room in the database
  room
    .save()
    .then(res => {
      Room.find({ owner: data.owner }).then(rooms => {
        io.emit("getRoomsList", rooms);
      });
      // io.emit('roomList', data);
    })
    .catch(err => {
      io.emit(
        "getRoomsList",
        err.message || "Some error occurred while creating the Room."
      );
    });
};

//createRoom with out socket
exports.createRoom = (data) => {
  // Validate request
  if (!data.title) {
    return res.status(400);
  }

  // Create a room
  const room = new Room({
    title: data.title || "Untitled Room",
    usernames: data.usernames|| [],
    createdAt: new Date(),
    owner: data.owner,
    auditId: data.auditId
  });

  // Save room in the database
  room
    .save()
    .then(res => {
      res.end(res);
    })
    .catch(err => {
      res.status(502).end(err);
    });
};
// Delete and Save a new Room

exports.delete = (data, io) => {
  return new Promise((resolve, reject) => {
    Room.deleteOne({ _id: data })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
exports.deleteUser = (data, io) => {
  return new Promise((resolve, reject) => {
    Room.update({ _id: data.id }, { $pull: { usernames: data.userName } })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};

exports.findAll = () => {
  return new Promise((resolve, reject) => {
    Room.find()
      .then(rooms => {
        resolve(rooms);
      })
      .catch(err => {
        reject(err);
      });
  });
};

exports.findRoomById = id => {
  return new Promise((resolve, reject) => {
    Room.find({ _id: id })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
exports.findRoomByAudit = async (req, res) => {
  try {
    Room.findOne({ auditId: req.body.audit_id })
      .exec(function (err, data) {
        if (err) return handleError(err);
        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};
exports.findUserRooms = user => {
  return new Promise((resolve, reject) => {
    Room.find({ usernames: { $in: [user] } })
      .then(rooms => {
        Room.find({ owner: user })
          .then(ownerRooms => {
            resolve(rooms.concat(ownerRooms));
          })
          .catch(err => reject(err));
      })
      .catch(err => {
        reject(err);
      });
  });
};

// exports.newFile = (data, io) => {
//   // Validate request
//   if (!data) {
//     io.emit("addFilesToRoom", "adding files to room failed");
//   }
//   console.log("file Data:", data);

//   Room.findOneAndUpdate(
//     { _id: data.rooId },
//     { $push: { fileName: data.fileName } },
//     { new: true }
//   )
//     .then(r => {
//       Room.find({ _id: data.rooId })
//         .then(room => {
//           io.emit("getRoomDetails", room);
//         })
//         .catch(err => console.log(err));
//     })
//     .catch(err => {
//       io.emit("getRoomDetails", err);
//     });
// };
exports.updateFileroom = (data, io) => {
  if (!data.fileName) {
    io.emit("addFilesToRoom", "adding files to room failed");
  }
  console.log('getFileRoomDetails:', data);
  Room.findOneAndUpdate(
    { _id: data.roomId },
    { $push: { fileName: { name: data.fileName, isOpen: false } } },
    { useFindAndModify: false }
  )
    .then(r => {
      Room.find({ _id: data.roomId })
        .then(room => {
          io.emit("getFileRoomDetails", room);
        })
        .catch(err => console.log(err));
    })
    .catch(err => {
      io.emit("getFileRoomDetails", err);
    });
};
exports.updateFileOpen = (data, io) => {
  console.log('@@@ updateFile:', data);
  return new Promise((resolve, reject) => {
    Room.update(
      { _id: data.roomId, 'fileName.name': data.name },
      {
        '$set': {
          'fileName.$.isOpen': data.isOpen,
        }
      }
    ).then(r => {
      resolve(r);
    })
      .catch(err => {
        reject(err);
      });
  });
};

exports.getRoomDetailsById = id => {
  if (!id) {
    console.log(err)
    // io.emit("addFilesToRoom", "adding files to room failed");
  }
  return new Promise((resolve, reject) => {
    Room.find({ _id: id })
      .then(room => {
        console.log('roomDetails:', room);
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
exports.deleteFile = (data, io) => {
  console.log('file came to delete:', data)
  return new Promise((resolve, reject) => {
    Room.updateOne({ _id: data.id }, { $pull: { fileName: { name: data.fileName } } })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
