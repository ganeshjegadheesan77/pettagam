const LastWpNumber = require("../../models/chatSystem/intray");
const CollabChatSystemSchema = require("../../models/chatSystem/collabChatSystem");
const DmsDocumentSchema = require("../../models/documents/documents.schema");
const logger = require("../../utils/logger").logger;
let intrayController = {};

intrayController.getWpNumber = async (req, res) => {
  try {
    let prefix = req.query.prefix;
    let lastWpData = await LastWpNumber.lastIdInfo.find({ prefix: prefix });
    res.send(lastWpData);
  } catch (error) {
    logger.pushError(err);
    return res.status(503).send(error);
  }
};
//this update the work paper number in collabchatsystems collection
intrayController.updateFileNameInRoom = async (req, res) => {
  try {
    let auditId = req.body.auditId;
    let username = req.body.currentUser;
    let oldfileName = req.body.oldfileName;
    let newFileName = req.body.newFileName;
    let userType = req.body.userType;
    let roomType = req.body.roomType;
    let collabDocs = await CollabChatSystemSchema.findOne({ auditId });
    collabDocs = JSON.parse(JSON.stringify(collabDocs));
    if (userType == "auditor") {
      let hostArrIndex = collabDocs.hosts.findIndex(
        (obj) => obj.username == username
      );
      let intrayArrIndex = collabDocs.hosts[
        hostArrIndex
      ].intrayDocuments.findIndex((obj) => {
        return obj.fileName == oldfileName && obj.room == roomType;
      });
      collabDocs.hosts[hostArrIndex]["intrayDocuments"][intrayArrIndex][
        "fileName"
      ] = newFileName;
      CollabChatSystemSchema.updateOne(
        { auditId: auditId },
        { $set: { hosts: collabDocs.hosts } },
        function (err, doc) {
          if (err) {
            logger.pushError(err);
            res
              .status(503)
              .end({ message: "Error while updating doc.", error: err });
            return;
          }
          res.status(200).send(doc);
        }
      );
    } else {
      let participantArrIndex = collabDocs.participants.findIndex(
        (obj) => obj.username == username
      );
      let intrayArrIndex = collabDocs.participants[
        participantArrIndex
      ].intrayDocuments.findIndex((obj) => {
        return obj.fileName == oldfileName && obj.room == roomType;
      });
      collabDocs.participants[participantArrIndex]["intrayDocuments"][
        intrayArrIndex
      ]["fileName"] = newFileName;
      CollabChatSystemSchema.updateOne(
        { auditId: auditId },
        { $set: { participants: collabDocs.participants } },
        function (err, doc) {
          if (err) {
            logger.pushError(err);
            res
              .status(503)
              .end({ message: "Error while updating doc.", error: err });
            return;
          }
          res.status(200).send(doc);
        }
      );
    }
  } catch (error) {
    logger.pushError(err);
    return res.status(503).send(error);
  }
};

intrayController.updateWpNumber = async (req, res) => {
  try {
    let prefix = req.body.prefix;
    let lastId = req.body.lastId;
    let lastWpData = await LastWpNumber.lastIdInfo.updateOne(
      { prefix },
      { $set: { lastId: lastId } }
    );
    res.send(lastWpData);
  } catch (error) {
    logger.pushError(err);
    return res.status(503).send(error);
  }
};

intrayController.deleteFileIntray = async (req, res) => {
  // let data = {
  //     "username": req.body.username,
  //     "auditId": req.body.documentNo,
  //     "fileName": req.body.fileName,
  //     "roomName": req.body.roomName
  // }
  try {
    if (req.body.userType == "auditor") {
      CollabChatSystemSchema.find(
        { auditId: req.body.auditId, "hosts.username": req.body.username },
        // { $pull: {fileName:req.body.fileName}  },
        function (err, docs) {
          if (err) {
            logger.pushError(err);
            res.status(404).end({ message: "Record not found", error: err });
          } else {
            var doc = docs[0];
            logger.pushConsoleInfo("doc", doc);
            // res.send(doc);
            // var h = doc.hosts.indexOf(h => h.username === req.body.username);
            doc.hosts.forEach((h) => {
              if (h.username == req.body.username) {
                var filteredfiles = h.intrayDocuments.filter(
                  (i) =>
                    i.fileName == req.body.fileName &&
                    i.room == req.body.roomName
                )[0];
                if (filteredfiles) {
                  h.intrayDocuments.splice(
                    h.intrayDocuments.indexOf(filteredfiles),
                    1
                  );
                }

                // h.intrayDocuments.splice(h.intrayDocuments.indexOf(req.body.fileName), 1);
              }
            });
            // doc.hosts[h].intrayDocuments.splice(doc.hosts[h].intrayDocuments.indexOf(req.body.fileName), 1);
            doc.save().then(() => {
              res.send({ data: doc, message: "Succesful" });
            });
          }
        }
      ); //.select(' _id hosts ');
    } else {
      CollabChatSystemSchema.findOne(
        {
          auditId: req.body.auditId,
          "participants.username": req.body.username,
        },
        // { $pull: {fileName:req.body.fileName}  },
        function (err, doc) {
          if (err) {
            logger.pushError(err);
            res.status(404).end({ message: "Record not found", error: err });
          } else {
            logger.pushConsoleInfo("doc", doc);

            // res.send(doc);
            doc.participants.forEach((h) => {
              if (h.username == req.body.username) {
                var filteredfiles = h.intrayDocuments.filter(
                  (i) =>
                    i.fileName == req.body.fileName &&
                    i.room == req.body.roomName
                )[0];
                if (filteredfiles) {
                  h.intrayDocuments.splice(
                    h.intrayDocuments.indexOf(filteredfiles),
                    1
                  );
                }

                // h.intrayDocuments.splice(h.intrayDocuments.indexOf(req.body.fileName), 1);
              }
            });
            doc.save().then(() => {
              res.send({ data: doc, message: "Succesful" });
            });
          }
        }
      ).select(" _id participants ");
    }
  } catch (e) {
    return res.status(406).send(e);
  }
};

intrayController.updateIntrayDoc = async (req, res) => {
  //req.body.auditId, req.body.username, req.body.userType, req.body.fileName, req.body.roomName
  CollabChatSystemSchema.find({ auditId: req.body.auditId }, function (
    err,
    docs
  ) {
    if (err) {
      logger.pushError(err);
      res.status(404).end({ message: "Record not found", error: err });
    }
    if (docs && docs.length > 0) {
      var doc = docs[0];

      if (req.body.userType == "auditor") {
        if (doc.hosts && doc.hosts.length > 0) {
          var h = doc.hosts.findIndex((p) => p.username == req.body.username);
          if (h >= 0) {
            if (
              doc.hosts[h].intrayDocuments &&
              doc.hosts[h].intrayDocuments.length > 0
            ) {
              req.body.fileList.forEach((f) => {
                doc.hosts[h].intrayDocuments.push({
                  fileName: f.fileName,
                  room: f.room,
                });
              });

              // logger.pushConsoleInfo('send', { fileName: req.body.fileName, room: req.body.roomName })
              // req.body.fileList.forEach(f => {
              //     doc.hosts[p].intrayDocuments.push(f);
              // })
            } else {
              doc.hosts[h].intrayDocuments = [];
              req.body.fileList.forEach((f) => {
                doc.hosts[h].intrayDocuments.push({
                  fileName: f.fileName,
                  room: f.room,
                });
              });
              // doc.hosts[p].intrayDocuments = [{ fileName: req.body.fileName, room: req.body.roomName }];
              // logger.pushConsoleInfo('send', req.body.filename)

              // req.body.fileList.forEach(f => {
              //     doc.hosts[p].intrayDocuments.push(f);
              // })
            }
          } else {
            logger.pushError(err);
            res.status(404).end({ message: "Record not found", error: err });

            // error throw participant not found
          }
        } else {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });

          // error throw participant not found
        }
      } else {
        if (doc.participants && doc.participants.length > 0) {
          var p = doc.participants.findIndex(
            (p) => p.username == req.body.username
          );
          if (p >= 0) {
            if (
              doc.participants[p].intrayDocuments &&
              doc.participants[p].intrayDocuments.length > 0
            ) {
              // doc.hosts[p].intrayDocuments = [];
              req.body.fileList.forEach((f) => {
                doc.participants[p].intrayDocuments.push({
                  fileName: f.fileName,
                  room: f.room,
                });
              });
              // doc.participants[p].intrayDocuments.push({ fileName: req.body.fileName, room: req.body.roomName });
            } else {
              doc.participants[p].intrayDocuments = [];
              req.body.fileList.forEach((f) => {
                doc.participants[p].intrayDocuments.push({
                  fileName: f.fileName,
                  room: f.room,
                });
              });
            }
          } else {
            // error throw participant not found
            logger.pushError("participant not found");
          }
        } else {
          logger.pushError("participant not found");
          // error throw participant not found
        }
      }
      doc.save().then(async () => {
        res.send({ message: `Successfully ` });
      });
    } else {
      res.send("no data found");
    }
  });
};

// intrayController.updateIntrayDoc = async (req, res) => {

// CollabChatSystemSchema.find({ auditId: req.body.auditId }, function (err, docs) {

//         if (docs && docs.length > 0) {

//             var doc = docs[0];

//             if (req.body.userType == 'auditor') {
//                 if (doc.hosts && doc.hosts.length > 0) {
//                     var p = doc.hosts.findIndex(p => p.username == req.body.username);
//                     if (p >= 0) {
//                         if (doc.hosts[p].intrayDocuments && doc.hosts[p].intrayDocuments.length > 0) {
//                             req.body.fileList.forEach(f => {
//                                 doc.hosts[p].intrayDocuments.push(f);
//                             })

//                         }
//                         else {
//                             doc.hosts[p].intrayDocuments = [];
//                             req.body.fileList.forEach(f => {
//                                 doc.hosts[p].intrayDocuments.push(f);
//                             })
//                         }
//                     }
//                     else {
//                         // error throw participant not found
//                     }
//                 }
//                 else {
//                     // error throw participant not found
//                 }
//             }
//             else {
//                 if (doc.participants && doc.participants.length > 0) {
//                     var p = doc.participants.findIndex(p => p.username == req.body.username);
//                     if (p >= 0) {
//                         if (doc.participants[p].intrayDocuments && doc.participants[p].intrayDocuments.length > 0) {
//                             req.body.fileList.forEach(f => {
//                                 doc.participants[p].intrayDocuments.push(f);
//                             })
//                         }
//                         else {
//                             doc.participants[p].intrayDocuments = [];
//                             req.body.fileList.forEach(f => {
//                                 doc.participants[p].intrayDocuments.push(f);
//                             });
//                         }
//                     }
//                     else {
//                         // error throw participant not found
//                     }
//                 }
//                 else {
//                     // error throw participant not found
//                 }
//             }
//             doc.save().then(async () => {
//                 res.send({ message: `Successfully ` });
//             })
//         }
//         else {
//             res.send('no data found');
//         }

//     });
//     // res.send(resp);
//     // let rd = await new IntraySystem.fileName(req.body);
//     // await rd.save((err, IntraySystem) => {
//     //   if (err) {
//     //     res.json(err);
//     //   } else {
//     //     res.json(IntraySystem);
//     //   }
//     // });
// }

intrayController.getFileDetails = async (req, res) => {
  try {
    var roomFileData = [];
    CollabChatSystemSchema.find({ auditId: req.body.auditId }, function (
      err,
      docs
    ) {
      if (err) {
        logger.pushError(err);
        return res.status(404).end({ message: "Record not found", error: err });
      }
      if (docs && docs.length > 0) {
        var doc = docs[0];
        if (doc.hosts && doc.hosts.length > 0) {
          doc.hosts.forEach((h) => {
            if (h.intrayDocuments && h.intrayDocuments.length > 0) {
              var hostdocs = h.intrayDocuments.filter(
                (d) => d.room == req.body.roomName
              );
              if (hostdocs.length > 0) {
                logger.pushConsoleInfo("*****hostdocs", hostdocs);
                roomFileData = roomFileData.concat(hostdocs);
              }
            }
          });
        }
        if (doc.participants && doc.participants.length > 0) {
          doc.participants.forEach((h) => {
            if (h.intrayDocuments && h.intrayDocuments.length > 0) {
              var participantdocs = roomFileData.concat(
                h.intrayDocuments.filter((d) => d.room == req.body.roomName)
              );
              if (participantdocs.length > 0) {
                logger.pushConsoleInfo("*****participantdocs", participantdocs);
                roomFileData = roomFileData.concat(participantdocs);
              }
            }
          });
        }

        DmsDocumentSchema.documentsInfo.find(
          { auditId: req.body.auditId },
          function (err, docs) {
            if (err) {
              logger.pushError(err);
              res.status(404).end({ message: "Record not found", error: err });
            }
            if (docs && docs.length > 0) {
              var newArr = [];
              // roomFileData.forEach(r => {
              //     newArr = newArr.concat(docs.filter(d => d.fileName == r.fileName));
              // })
              // docs.filter(d => roomFileData.includes(r => r.fileName == d.fileName));
              // b.filter(o => !a.find(o2 => o.id === o2.id))
              newArr = docs.filter((d) =>
                roomFileData.find((r) => d.fileName == r.fileName)
              );
              res.status(200).json([{ FileData: newArr }]);
            } else {
              res.send("no dms docs found");
            }
          }
        );
      } else {
        res.send("no room found");
      }
    }).select("_id hosts participants");
    // var roomName = req.body.roomName;
    logger.pushConsoleInfo("dd", req.body.auditId);
    // CollabChatSystemSchema.aggregate([
    //     { $match: { auditId: req.body.auditId } },

    //     {
    //         $lookup: {
    //             from: "DmsDocumentRepository",
    //             localField: "auditId",
    //             foreignField: "auditId",
    //             as: "FileData",
    //         },
    //     }
    // ])
    //     .exec(function (err, data) {
    //         if (err) return handleError(err);
    //         // data[0].FileData = data[0].FileData.filter(f => f.roomType == roomName);
    //         res.status(200).json(data);
    //     });
  } catch (e) {
    return res.status(406).send(e);
  }
};
module.exports = intrayController;