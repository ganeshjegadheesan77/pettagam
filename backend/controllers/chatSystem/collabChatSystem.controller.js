const CollabChatSystem = require("../../models/chatSystem/collabChatSystem");
const CollabChatSystemSchema = require("../../models/chatSystem/collabChatSystem");
const DmsDocumentSchema = require("../../models/documents/documents.schema");
const logger = require("../../utils/logger").logger;
const OnlineUsers = require("../../models/chatSystem/onlineUsers");
const room = require("../../models/chatSystem/room");
exports.findRoomById = (auditIdParam) => {
  if (!auditIdParam) {
    return false;
  }
  // logger.pushConsoleInfo(auditId);
  return new Promise((resolve, reject) => {
    CollabChatSystem.findOne({ auditId: auditIdParam })
      .then((room) => {
        // logger.pushConsoleInfo(room);
        resolve(room);
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });
};

exports.getIntrayFiles = (obj) => {
  let roomName = obj.roomName;
  logger.pushConsoleInfo("roomName", roomName);
  let auditId = obj.auditId;
  return new Promise((resolve, reject) => {
    try {
      var roomFileData = [];
      CollabChatSystemSchema.find({ auditId: auditId }, function (err, docs) {
        if (err) {
          logger.pushError(err);
        }
        if (docs && docs.length > 0) {
          var doc = docs[0];
          if (doc.hosts && doc.hosts.length > 0) {
            doc.hosts.forEach((h) => {
              if (h.intrayDocuments && h.intrayDocuments.length > 0) {
                logger.pushConsoleInfo(
                  "********intrayDocuments",
                  h.intrayDocuments
                );
                var hostdocs = h.intrayDocuments.filter(
                  (d) => d.room == roomName
                );
                logger.pushConsoleInfo("************hostdocs", hostdocs);
                if (hostdocs.length > 0) {
                  roomFileData = roomFileData.concat(hostdocs);
                }
              }
            });
          }
          if (doc.participants && doc.participants.length > 0) {
            doc.participants.forEach((h) => {
              if (h.intrayDocuments && h.intrayDocuments.length > 0) {
                var participantdocs = roomFileData.concat(
                  h.intrayDocuments.filter((d) => d.room == roomName)
                );
                if (participantdocs.length > 0) {
                  roomFileData = roomFileData.concat(participantdocs);
                }
              }
            });
          }

          DmsDocumentSchema.documentsInfo.find(
            { auditId: auditId, roomType: roomName },
            function (err, docs) {
              if (err) {
                logger.pushError(err);
              }
              if (docs && docs.length > 0) {
                var newArr = [];
                // roomFileData.forEach(r => {
                //     newArr = newArr.concat(docs.filter(d => d.fileName == r.fileName));
                // })
                // docs.filter(d => roomFileData.includes(r => r.fileName == d.fileName));
                // b.filter(o => !a.find(o2 => o.id === o2.id))
                logger.pushConsoleInfo("ROOM FILE DATA: ", roomFileData);
                newArr = docs.filter((d) =>
                  roomFileData.find((r) => d.fileName == r.fileName)
                );
                logger.pushConsoleInfo("$$$$$$newArr", newArr);
                resolve([{ FileData: newArr }]);
                // res.status(200).json();
              } else {
                // resolve('no dms docs found');
                resolve([{ FileData: [] }]);
              }
            }
          );
        } else {
          // resolve('no room found');
          resolve([{ FileData: [] }]);
        }
      }).select("_id hosts participants");
      logger.pushConsoleInfo("dd", auditId);
      // CollabChatSystemSchema.aggregate([
      //     { $match: { auditId: req.body.auditId } },

      //     {
      //         $lookup: {
      //             from: "DmsDocumentRepository",
      //             localField: "auditId",
      //             foreignField: "auditId",
      //             as: "FileData",
      //         },
      //     }
      // ])
      //     .exec(function (err, data) {
      //         if (err) return handleError(err);
      //         // data[0].FileData = data[0].FileData.filter(f => f.roomType == roomName);
      //         res.status(200).json(data);
      //     });
    } catch (e) {
      reject(e);
    }
  });
};

exports.createNewChatroom = (req, res) => {
  const { auditId, title, participants, roomId, hosts } = req.body;
  // Validate request
  if (!req.body.title) {
    return res.status(400);
  }
  // Create a room
  // logger.pushConsoleInfo("data:", req.body);

  const chatroom = new CollabChatSystem({
    title,
    auditId,
    roomId,
    participants,
    hosts,
  });

  // Save room in the database
  chatroom
    .save()
    .then((data) => {
      // logger.pushConsoleInfo(data);
      res.json(data);
    })
    .catch((err) => {
      logger.pushError(err);
      res.status(502).json(err);
    });
};

exports.setUserOnline = (user) => {
  return new Promise((resolve, reject) => {
    CollabChatSystem.update(
      { roomId: user.roomId, "hosts.username": user.username },
      { $set: { "hosts.$.status": user.status } },
      { upsert: true }
    )
      .then((res) => {
        CollabChatSystem.findOne({ roomId: user.roomId })
          .then((room) => {
            // logger.pushConsoleInfo("room user status set: ", room);
            resolve(room);
          })
          .catch((err) => {
            logger.pushError(err);
            reject(err);
          });
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });
};

exports.autoCreateNewChatroom = (data) => {
  const { auditId, title, roomId, hosts } = data;
  // Validate request
  if (!data.title) {
    return res.status(400);
  }
  // Create a room
  // logger.pushConsoleInfo("data:", data);

  const chatroom = new CollabChatSystem({
    title,
    auditId,
    roomId,
    hosts,
  });

  // Save room in the database
  chatroom
    .save()
    .then((data) => {
      return data;
    })
    .catch((err) => {
      logger.pushError(err);
      res.status(502).json(err);
    });
};

exports.toggleOnlineUser = (data) => {
  return new Promise((resolve, reject) => {
    OnlineUsers.update(
      { username: data.username },
      { $set: { status: data.status, roomId: data.roomId } },
      { upsert: true }
    )
      .then((res) => {
        OnlineUsers.findOne({ username: data.username })
          .then((user) => {
            // logger.pushConsoleInfo("online user: ", user);
            resolve(user);
          })
          .catch((err) => {
            logger.pushError(err);
            reject(err);
          });
      })
      .catch((err) => {
        logger.pushError(err);
        reject(err);
      });
  });
};