const Jwt = require('jsonwebtoken');
const cuid = require('cuid');
const User = require('../../models/users');
const config = require('../../config/config');
const { decrypt, encrypt } = require('../../utils/crypto');
const statuses = require('../../config/statuses');

const privateKey = config.key.privateKey;

const UserController = {};

UserController.create = async (req, res) => {
    try {
        const body = req.body;

        body.password = encrypt(req.body.password);
        body.cuid = cuid();
        body.location = {
            type: 'Point',
            coordinates: body.location
        };

        await User.create(body);
        return res.status(200).send(statuses[200]);
    } catch (e) {
        return res.status(406).send(e);
    }
};

UserController.login = async (req, res) => {
    try {
        let _username = req.body.username.toLowerCase();
        const user = await User.findOne({ username: _username });
        if (user == null) {
            return res.send(statuses[422]);
        }
        if (req.body.password !== decrypt(user.password)) {
            return res.send(statuses[401]);
        }

        const tokenData = {
            role:user.role,
            profileUrl:user.profileUrl,
            username: user.username,
            id: user.cuid
        };
        const result = {
            role:user.role,
            profileUrl:user.profileUrl,
            username: user.username,
            token: Jwt.sign(tokenData, privateKey, {expiresIn: '8h'})
        };

        return res.json(result);
    } catch (e) {
        return res.status(406).send(e);
    }
};

UserController.delete = async (req, res) => {
    try {
        await req.user.remove();
        return res.json({ message: `user deleted successfully` });
    } catch (e) {
        return res.status(406).send(e);
    }
};
UserController.getAllUsers = async (req, res) => {
    try {
        User.find((err,userData)=>{
        // await req.user.remove();
        return res.json(userData);
        })
    }catch (e) {
        return res.status(406).send(e);
    }
};

module.exports = UserController;