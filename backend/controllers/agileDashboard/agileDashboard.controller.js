const rpsSchema = require("../../models/Audit/audit.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const universeCollection = require("../../models/enia/universe.schema");
const agileDashboard = {};
const logger = require("../../utils/logger").logger;

//get one action //

agileDashboard.getMyAuditDetails = async (req, res) => {
  try {
    let myAuditRecords = [];
    rpsSchema.rps.find(
      { auditTeam: { $in: [req.body.user] } },
      // ,{ auditId: 1 }
      async function (err, data) {
        if (err) {
          logger.pushError(err);
          return handleError(err);
        }
        if (data.length) {
          // filter data.filter
          data = data.filter(
            (v, i, a) => a.findIndex((t) => t.auditId === v.auditId) === i
          );
          logger.pushConsoleInfo("save apis agile", data.length);
          internalAudit.auditMasters.find(
            { auditId: { $in: data.map((x) => x.auditId) } },
            (err, response) => {
              if (err) {
                logger.pushError(err);
                return handleError(err);
              } else {
                response = response.filter(
                  (v, i, a) => a.findIndex((t) => t.auditId === v.auditId) === i
                );
                response.forEach((r) => {
                  var myRes = JSON.parse(JSON.stringify(r));
                  if (data.some((x) => x.auditId === r.auditId)) {
                    myRes["calendarDetails"] = data.filter(
                      (x) => x.auditId == r.auditId
                    )[0];
                  }
                  myAuditRecords.push(myRes);
                });
                logger.pushConsoleInfo("before send", myAuditRecords.length);
                res.send(myAuditRecords);
              }
            }
          );
        } else {
          return res.status(404).send("No Audits found..");
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
//getOneRP
async function auditDetails(data) {
  let auditDetail = [];
  if (data) {
    for (let i = 0; i < data.length; i++) {
      // logger.pushConsoleInfo("data[i]._id:", data[i][0]._id);
      await universeCollection.find(
        { oldAuditId: data[i][0]._id },
        { auditableEntity: 1, name: 1 },
        (err, res) => {
          if (err) {
            logger.pushError(err);
            return err;
          }
          if (res) {
            auditDetail.push({
              myAuditRecords: data[i],
              auditDetails: res,
            });
            logger.pushConsoleInfo("auditDetail:", JSON.stringify(auditDetail));
          }
        }
      );
    }
    return auditDetail;
  }
}
agileDashboard.getMyARADetails = async (req, res) => {
  await universeCollection.find(
    { "auditInfo.currentAudit": req.body.id },
    { name: 1, auditInfo: 1, type: 1, parent: 1, id: 1 },
    (err, doc) => {
      if (err) {
        logger.pushError(err);
        return err;
      }
      if (doc) {
        res.send(JSON.stringify(doc[0]));
      }
    }
  );
};

// exports.getAllTableDataByRiskLevel = (req, res) => {
//   try {
//     universeData.find(
//       { auditInfo: { $exists: true } },
//       { name: 1, auditInfo: 1, type: 1, parent: 1, id: 1 },
//       (err, data) => {
//         if (err) return handleError(err);
//         // logger.pushConsoleInfo("getTableData dada:", data);
//         res.send(data);
//       }
//     );
//   } catch (e) {
// logger.pushError(e);
//     return res.status(406).send(e);
//   }
// };
// /audits/getMyARADetails
module.exports = agileDashboard;