const riskDetail = require("../../models/RiskManagement/risks.schema");
const controlDetail = require("../../models/RiskManagement/riskcontrol.schema");

const masters = require("../../models/RiskManagement/riskmasters.schema");
const auditTrial = require("../../models/auditTrial/auditTrial.schema");

const controlController = {};

//REMOVE ISSUES FROM ACTION
controlController.removeIssuesFromControl = (req, res) => {
  try {
    console.log({controlId : req.body.controlId, issueId: req.body.issueId});
    req.body.controlId.forEach((id) => {
      controlDetail.control.update(
        { controlId: id },
        {
          $pull: { issueId: req.body.issueId }
        },
        (err, response) => {
          if (err) return res.status(406).send(e);
        }
      );
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlController.getNumberofControl = async (req, res) => {
  try {
    controlDetail.control.count((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

controlController.getOneControl = async (req, res) => {
  try {
    controlDetail.control
      .findOne({ controlId: req.body.controlId })
      .exec(function (err, data) {
        if (err) return handleError(err);
        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlController.deleteControl = (req, res) => {
  controlDetail.control.findOneAndDelete(
    { controlId: req.body.controlId },
    (err, response) => {
      if (err) {
        res.status(404).end("Record Cannot Delete!");
      } else {
        // auditTrial(req.unique_request_id, "riskControls", req.body.controlId, req.body.controlId, "Delete", req.user.username);
        res.end(JSON.stringify(response, null, 2));

        // controlDetail.control.find((err, data) => {
        //   if (err) {
        //     res.status(404).end("Record not found!");
        //   } else {
        //     res.end(JSON.stringify(data, null, 2));
        //   }
        // });
      }
    }
  );
};
controlController.getCRMRecords = async (req, res) => {
  try {
    controlDetail.control
      .aggregate([
        {
          $lookup: {
            from: "risks",
            localField: "controlId",
            foreignField: "controlId",
            as: "riskdata",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          res.send("message:Error to get record", err);
        }
        console.log("Control Matrix data:", data);
        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlController.updateControl = async (req, res) => {
  try {
    let olderRiskIds;
    let updatedDoc;
    await controlDetail.control
      .findOne({ controlId: req.body.controlId })
      .exec(function (err, control) {
        if (err) return handleError(err);
        console.log("Control.data:", control);
        olderRiskIds = control.riskId;
        let idsToDelete = [];
        let idsToAdd = [];
        let recdIds = req.body.riskId;
        if (olderRiskIds) {
          if (recdIds) {
            olderRiskIds.forEach((id) => {
              if (recdIds.indexOf(id) === -1) {
                idsToDelete.push(id);
              }
            });
          }
        }
        if (recdIds) {
          idsToAdd = recdIds.filter(function (el) {
            return idsToDelete.indexOf(el) == -1;
          });
        }
        try {
          controlDetail.control.updateOne(
            { controlId: req.body.controlId },
            { $set: req.body },
            function (err, data) {
              if (err) {
                res
                  .status(404)
                  .end({ message: "Record not found", error: err });
              }
              if (data) {
                controlDetail.control.updateOne(
                  { controlId: req.body.controlId },
                  {
                    $set:
                    {
                      riskId: req.body.riskId,
                      "updated_by._id": req.user.id,
                      "updated_by.name": req.user.username,
                      "updated_on": new Date()
                    }
                  },
                  function (err, doc) {
                    console.log(
                      "update Response Line No 92 control controller:",
                      doc
                    );
                    if (err) {
                      res
                        .status(404)
                        .end({ message: "Record not found", error: err });
                    }
                    if (doc) {
                      updatedDoc = doc;
                      // auditTrial(req.unique_request_id, "riskControls", req.body.controlId, doc, "Update", req.user.username);

                      addControlToRisk(req.body.controlId, idsToAdd, req.unique_request_id, req.user.username, req.user.id);
                      removeControlFromRisk(req.body.controlId, idsToDelete, req.unique_request_id, req.user.username, req.user.id);
                      console.log(doc, doc.riskId);
                      res.send(doc);
                    }
                  }
                );
              }
            }
          );
        } catch (err) {
          return res.status(406).send(err);
        }
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

function addControlToRisk(controlId, riskId, unique_request_id, username, userId) {
  riskId.forEach((id) => {
    riskDetail.risk
      .findOneAndUpdate(
        { riskId: id },
        {
          $addToSet: { controlId: controlId }, $set: {
            "updated_by._id": userId,
            "updated_by.name": username,
            "updated_on": new Date()
          }
        },
        { new: true }
      )
      .then((record) => {
        // auditTrial(unique_request_id, "risks", id, record, "Update", username);
        console.log(record, "added risk saved! controller");
      })
      .catch((err) => {
        console.log("getRoomDetails", err);
      });
  });
}





function removeControlFromRisk(controlId, riskId, unique_request_id, username, userId) {
  riskId.forEach((id) => {
    riskDetail.risk
      .findOneAndUpdate(
        { riskId: id },
        {
          $pull: { controlId: controlId }, $set: {
            "updated_by._id": userId,
            "updated_by.name": username,
            "updated_on": new Date()
          }
        },
        { new: true }
      )
      .then((record) => {
        // auditTrial(unique_request_id, "risks", id, record, "Update", username);
        console.log(record, "removed! control saved! controller");
      })
      .catch((err) => {
        console.log("getRoomDetails", err);
      });
  });
}

controlController.addNewRiskToControl = (req, res) => {
  if (!req.body.riskId) {
    console.log("Risk Id", err);
  }
  req.body.riskId.forEach((id) => addRisk(req.body.id, id, req));
};
function addRisk(controlId, riskId, req) {
  riskDetail.risk
    .findOneAndUpdate(
      { riskId: riskId },
      {
        $push: { controlId: controlId }, $set: {
          "updated_by._id": req.user.id,
          "updated_by.name": req.user.username,
          "updated_on": new Date()
        }
      },
      { new: true }
    )
    .then((record) => {
      // auditTrial(req.unique_request_id, "risks", record.riskId, record, "Update", req.user.username);
      controlDetails.control
        .findOneAndUpdate(
          { controlId: controlId },
          {
            $push: { riskId: riskId }, $set: {
              "updated_by._id": req.user.id,
              "updated_by.name": req.user.username,
              "updated_on": new Date()
            }
          },
          { new: true }
        )
        .then((data) => {
          // auditTrial(req.unique_request_id, "riskControls", controlId, data, "Update", req.user.username);
          let query = JSON.parse(JSON.stringify(data.riskId));
          riskDetail.risk
            .find({ riskId: { $in: query } })
            .then((record) => {
              console.log(record);
            })
            .catch((err) => {
              console.log("Invalid Id!", err);
            });
        });
    });
}
////////////////////////////////////// workflow Api's///////////////////////

controlController.getOneWF = async (req, res) => {
  try {
    controlDetail.controlwf.findOne({ controlWFId: req.body.controlWFId }, function (err, data) {
      console.log('check', data)
      if (err) return handleError(err);

      res.send(data);
    })
  }
  catch (e) {
    return res.status(406).send(e);
  }

}

controlController.updateOneControlls = async (req, res) => {
  req.body.updated_by = {
    _id: req.user.id,
    name: req.user.username
  };
  req.body.updated_on = new Date()
  try {
    controlDetail.control.updateOne(
      { "controlId": req.body.controlId },
      { "$set": req.body },
      function (err, doc) {

        if (err) {
          res.status(404).end({ message: "Record not found", error: err });

        } else {
          // auditTrial(req.unique_request_id, "riskControls", req.body.controlId, req.body, "Update", req.user.username);
          console.log('doc', doc)
          res.send(doc)
        }

      });
  } catch (e) {
    return res.status(406).send(e);
  }


}



controlController.getAllcontrolWFlist = async (req, res) => {
  try {
    controlDetail.control
      .aggregate([
        {
          $lookup: {
            from: "controlwfs",
            localField: "controlId",
            foreignField: "controlIds",
            as: "controldata",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          res.send("message:Error to get record", err);
        }
        // console.log("r&c Matrix data:", data);
        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};




controlController.updateOneControlFromWF = async (req, res) => {
  req.body.updated_by = {
    _id: req.user.id,
    name: req.user.name
  };
  req.body.updated_on = new Date();
  controlDetail.control
    .findOneAndUpdate(
      { controlWFId: req.body.controlWFId },
      { "$set": req.body },
      { new: true }
    )
    .then((record) => {
      // auditTrial(req.unique_request_id, "riskControls", req.body.controlId, req.body, "Update", req.user.username);
      console.log(record, "added risk saved! controller");
    })
    .catch((err) => {
      console.log("getRoomDetails", err);
    });
  // });
}

controlController.saveControlWF = async (req, res) => {
  req.body.created_by = {
    _id: req.user.id,
    name: req.user.username
  };
  req.body.created_on = new Date()
  let rd = await new controlDetail.controlwf(req.body);
  await rd.save((err, controlDetail) => {
    if (err) {
      res.json(err);
    } else {
      // auditTrial(req.unique_request_id, "controlwfs", controlDetail.controlWFId, controlDetail, "Insert", req.user.username);
      console.log(controlDetail);
      res.json(controlDetail);
    }
  });
}


controlController.updateOneControlWF = async (req, res) => {
  req.body.updated_by = {
    _id: req.user.id,
    name: req.user.username
  }
  req.body.updated_on = new Date();
  try {
    controlDetail.controlwf.updateOne(
      { "controlWFId": req.body.controlWFId },
      { "$set": req.body },
      function (err, doc) {

        if (err) {
          res.status(404).end({ message: "Record not found", error: err });

        } else {
          console.log('doc', doc);
          // auditTrial(req.unique_request_id, "controlwfs", req.body.controlWFId, req.body, "Update", req.user.username);
          res.send(doc)
        }

      });
  } catch (e) {
    return res.status(406).send(e);
  }


}


controlController.getMasterCWFrequency = (req, res) => {
  try {
    masters.masterWfFrequency.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};




// controlController.getAreaInRisk = (req, res) => {
//   controlDetail.control.findOne({ controlId: req.body.controlId }, { riskId: 1 }, function (err, data) {
//     if (err) return handleError(err);
//     console.log('data', data)
//     let query = JSON.parse(JSON.stringify(data.riskId)) 
//     riskDetail.risk.find(  { riskId: { $in: query }, controlId: { $in: req.body.controlId } },{riskArea: 1},
//       (err, doc) => {
//         if (err) res.status(500).json(err);
//         res.send(doc);                 
//       });

//   })
// }




////////////////////////////////////////////////////////////////////////////
// post all risk //
controlController.saveControls = async (req, res) => {
  req.body.created_by = {
    _id: req.user.id,
    name: req.user.username
  };
  req.body.created_on = new Date()
  let rd = await new controlDetail.control(req.body);
  await rd.save((err, controlDetails) => {
    if (err) {
      res.json(err);
    } else {
      // auditTrial(req.unique_request_id, "riskControls", controlDetails.controlId, controlDetails, "Insert", req.user.username);
      let riskIds = req.body.riskId;
      let controlId = req.body.controlId
      for (let i = 0; i < riskIds.length; i++) {
        addRisk(controlId, riskIds[i], req);
      }
      res.json(controlDetails);
    }
  });
};

controlController.getMasterControlStatus = (req, res) => {
  try {
    masters.masterControlStatus.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

controlController.getMasterControlType = (req, res) => {
  try {
    masters.masterControlType.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

controlController.getMasterControlKeyProcess = (req, res) => {
  try {
    controlDetail.masterControlKeyProcess.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

controlController.getAllControlRecords = (req, res) => {
  try {
    controlDetail.control.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};




controlController.getAllUnassignControls = (req, res) => {
  try {
    controlDetail.control.find(
      { riskId: { $exists: true, $size: 0 } },
      (err, data) => {
        if (err) {
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};
controlController.getUnassignControlCount = (req, res) => {
  try {
    controlDetail.control
      .find({ riskId: { $exists: true, $size: 0 } })
      .countDocuments((err, data) => {
        if (err) {
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

// find({ department: null });

// controlController.getControlsForControl = (req, res) => {
//   controlDetails.control.findOne({ controlId: req.body.controlId },  function (err, data) {
//     if (err) return handleError(err);
//     console.log('data', data)
//     let query = JSON.parse(JSON.stringify(data.riskId))
//     console.log('query',query)

//     riskDetail.risk.find({
//       'riskId': { $in: [...query] }
//     },  { riskId:1, riskName:1, riskDescription:1, addControlFlag:1, riskCategory:1},function (err, docs) {
//       if (err) res.status(500).json(err)
//       console.log('docs', docs);
//       res.send(docs);
//     });
//   }
//   )
// }
controlController.removeOneRiskFromControls = (req, res) => {
  try {
    req.body.controlId.forEach((id) => {
      controlDetail.control.update(
        { controlId: id },
        {
          $pull: { riskId: req.body.riskId },
          $set: {
            "updated_by._id": req.user.id,
            "updated_by.name": req.user.username,
            "updated_on": new Date()
          }
        },
        (err, response) => {
          if (err) return res.status(406).send(e);

          // auditTrial(req.unique_request_id, "riskControls", id, req.body.riskId, "Update", req.user.username);
          res.send(response);
        }
      );
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

controlController.updateControlforTest = async (req, res) => {
  try {
    req.body.updated_by = {
      _id: req.user.id,
      name: req.user.username
    };
    req.body.updated_on = new Date();

    controlDetail.control.update(
      { controlId: req.body.controlId },
      { $set: req.body },
      (err, response) => {
        if (err) return res.status(406).send(e);

        // auditTrial(req.unique_request_id, "riskControls", req.body.controlId, req.body, "Update", req.user.username);
        res.send(response);
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};
module.exports = controlController;
