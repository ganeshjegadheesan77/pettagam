const riskDetail = require("../../models/RiskManagement/risks.schema");
const riskMasters = require("../../models/RiskManagement/riskmasters.schema");
const controlDetails = require("../../models/RiskManagement/riskcontrol.schema");
const auditTrial = require('../../models/auditTrial/auditTrial.schema');
const riskController = {};

//get one risk //
riskController.getOneRisk = async (req, res) => {
  try {
    riskDetail.risk.findOne({ riskId: req.body.riskId }, function (err, data) {
      if (err) return handleError(err);

      res.send(data);
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getRiskWithAssessment = async (req, res) => {
  try {
    riskDetail.risk
      .findOne({ riskId: req.body.riskId })
      .populate("riskAssessmentId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.send(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getAllRiskAssessment = async (req, res) => {
  try {
    riskDetail.assess
      .find({})
      .populate("riskId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.send(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getAllRiskAssessmentList = async (req, res) => {
  try {
    riskDetail.risk
      .aggregate([
        {
          $lookup: {
            from: "assesses",
            localField: "riskAssessmentId",
            foreignField: "riskAssementId",
            as: "mydata",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          res.send("message:Error to get record", err);
        }

        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getAssessmentDetails = async (req, res) => {
  try {
    riskDetail.assess
      .findOne({ riskAssementId: req.body.riskAssementId })
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.send(data);
      });
    // riskDetail.risk.findOne({ riskAssessmentId: "RA-002" },{ controlId: 0,controlType:0 ,controlDepartment:0,controlProcess:0,control:0,controlRiskProgress:0,addControlFlag:0,riskOwnerFlag:0,scheduleFlag:0}).exec(function (err, data) {
    //   console.log(data)
    //   if (err) return handleError(err);

    //   res.send(data)
    // })
  } catch (e) {
    res.json("hii");
  }
};

riskController.getRiskAssessmentDetails = async (req, res) => {
  try {
    riskDetail.risk
      .findOne(
        { riskAssessmentId: req.body.riskAssementId },
        {
          controlId: 0,
          controlType: 0,
          controlDepartment: 0,
          controlProcess: 0,
          control: 0,
          controlRiskProgress: 0,
          addControlFlag: 0,
          riskOwnerFlag: 0,
          scheduleFlag: 0,
        }
      )
      .exec(function (err, data) {
        if (err) return handleError(err);
        res.send(data);
      });
  } catch (e) {
    res.json("hii");
  }
};

riskController.getOneRiskAssessment = async (req, res) => {
  try {
    riskDetail.assess
      .findOne({ riskAssementId: req.body.riskAssementId })
      .populate("riskId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.send(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};
riskController.postOneAssessmentRisk = async (req, res) => {
  console.log("Inside postRisk API::" + req.body)

  let rd = await new riskDetail.assess(req.body);
  await rd.save((err, riskDetails) => {
    if (err) {
      res.json(err);
    } else {
      res.send(riskDetails);
    }
  });
};

function addControl(riskId, controlId, unique_request_id, username, userId) {
  console.log(controlId);
  controlDetails.control
    .findOneAndUpdate(
      { controlId: controlId },
      {
        $addToSet: { riskId: riskId }, $set: {
          "updated_by._id": userId,
          "updated_by.name": username,
          "updated_on": new Date()
        }
      },
      { new: true }
    )
    .then((record) => {
      // auditTrial(unique_request_id, "riskControls", controlId, record, "Update", username, userId);

      riskDetail.risk
        .findOneAndUpdate(
          { riskId: riskId },
          { $addToSet: { controlId: controlId } },
          { new: true }
        )
        .then((data) => {
          // auditTrial(req.unique_request_id, "risks", riskId, data, "Update", req.user.username);
          let query = JSON.parse(JSON.stringify(data.controlId));
          controlDetails.control
            .find({ controlId: { $in: query } })
            .then((record) => {
              console.log(record);
            })
            .catch((err) => {
              console.log("Invalid Id!", err);
            });
        });
    });
}

riskController.addNewControlToRisk = (req, res) => {
  if (!req.body.controlId) {
    console.log("getRoomDetails", err);
  }
  console.log("req.body.controlId:", req.body.controlId);
  req.body.controlId.forEach((id) => addControl(req.body.id, id, req.unique_request_id, req.user.username, req.user.id));
};

//num of allrecords for both tables //
riskController.getAllRecords = async (req, res) => {
  try {
    riskDetail.risk
      .aggregate([
        {
          $lookup: {
            from: "riskcontrols",
            localField: "riskId",
            foreignField: "riskId",
            as: "controldata",
          },
        },
      ])
      .exec(function (err, data) {
        if (err) {
          res.send("message:Error to get record", err);
        }
        // console.log("r&c Matrix data:", data);
        res.json(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

// update one risk //
// riskController.updateOneRisk = async (req, res) => {

//   try {
//     riskDetail.risk.findOneAndUpdate(
//       { "_id": req.body._id },
//       {
//         "$set": req.body
//       },
//       function (err, doc) {

//         if (err) {
//           console.log('err###', err)

//           res.status(404).end({ message: "Record not found", error: err });

//         } else {
//           console.log('doc###', doc)
//           res.send(doc)
//         }

//       });
//   } catch (e) {
//     return res.status(406).send(e);
//   }

// }
function addRiskToControl(riskId, controlId, unique_request_id, username, userId) {
  controlId.forEach((id) => {
    controlDetails.control
      .findOneAndUpdate(
        { controlId: id },
        {
          $addToSet: { riskId: riskId }, $set: {
            "updated_by._id": userId,
            "updated_by.name": username,
            "updated_on": new Date()
          }
        },
        { new: true }
      )
      .then((record) => {
        // auditTrial(unique_request_id, "riskControls", id, record, "Update", username);
        console.log(record, "added control saved!");
      })
      .catch((err) => {
        console.log("getRoomDetails", err);
      });
  });
}

function removeRiskFromControl(riskId, controlId, unique_request_id, username, userId) {
  controlId.forEach((id) => {
    controlDetails.control
      .findOneAndUpdate(
        { controlId: id },
        {
          $pull: { riskId: riskId }, $set: {
            "updated_by._id": userId,
            "updated_by.name": username,
            "updated_on": new Date()
          }
        },
        { new: true }
      )
      .then((record) => {
        // auditTrial(unique_request_id, "riskControls", id, record, "Update", username);
        console.log(record, "removed! control saved!");
      })
      .catch((err) => {
        console.log("getRoomDetails", err);
      });
  });
}

riskController.updateOneRisk = async (req, res) => {
  try {
    console.log(req.body.riskId);
    let olderControlIds;
    let updatedDoc;
    await riskDetail.risk.findOne({ riskId: req.body.riskId }, function (
      err,
      risk
    ) {
      if (err) res.status(404).end({ message: "Record not found", error: err });
      if (risk) {
        // console.log(risk);
        olderControlIds = risk.controlId;
        let idsToDelete = [];
        let idsToAdd = [];
        let recdIds = req.body.controlId;
        if (olderControlIds.length) {
          olderControlIds.forEach((id) => {
            if (recdIds) {
              if (recdIds.indexOf(id) === -1) {
                idsToDelete.push(id);
              }
            }
          });
        }
        if (idsToAdd) {
          idsToAdd = recdIds.filter(function (el) {
            return idsToDelete.indexOf(el) == -1;
          });
        }
        console.log(
          "idsToDelete",
          idsToDelete,
          olderControlIds,
          recdIds,
          idsToAdd
        );
        try {
          riskDetail.risk.updateOne(
            { riskId: req.body.riskId },
            { $set: req.body },
            function (err, data) {
              if (err) {
                res
                  .status(404)
                  .end({ message: "Record not found", error: err });
              }
              if (data) {
                riskDetail.risk.updateOne(
                  { riskId: req.body.riskId },
                  {
                    $set:
                    {
                      controlId: req.body.controlId,
                      "updated_by._id": req.user.id,
                      "updated_by.name": req.user.username,
                      "updated_on": new Date()
                    }
                  },
                  function (err, doc) {
                    console.log("update Response Line No263:", doc);
                    if (err) {
                      res
                        .status(404)
                        .end({ message: "Record not found", error: err });
                    }
                    if (doc) {
                      // auditTrial(req.unique_request_id, "risks", req.body.riskId, req.body, "Update", req.user.username);
                      updatedDoc = doc;

                      addRiskToControl(req.body.riskId, idsToAdd, req.unique_request_id, req.user.username, req.user.id);

                      removeRiskFromControl(req.body.riskId, idsToDelete, req.unique_request_id, req.user.username, req.user.id);

                      console.log(doc, doc.controlId);
                      res.send(doc);
                    }
                  }
                );
              }
            }
          );
        } catch (err) {
          return res.status(406).send(err);
        }
        // res.send(risk);
      }
    });
  } catch (err) {
    return res.status(406).send(err);
  }
};

riskController.updateOneRiskAssessment = async (req, res) => {
  try {
    riskDetail.risk.updateOne(
      {
        riskId: req.body.riskId,
      },
      {
        $push: {
          riskAssessmentId: req.body.riskAssementId,
        },
      },

      function (err, doc) {
        if (err) {
          res.status(404).end({
            message: "Record not found",
            error: err,
          });
        } else {
          console.log("doc####", doc);
          addRiskToControl(req.body.riskId, req.body.controlId, req.unique_request_id, req.user.username, req.user.id);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};
// update one risk //
// riskController.updateOneRisk = async (req, res) => {
//   try {
//     console.log(req.body.riskId);
//     let olderControlIds;
//     let updatedDoc;
//     await riskDetail.risk.findOne({ riskId: req.body.riskId }, function (
//       err,
//       risk
//     ) {
//       if (err) res.status(404).end({ message: "Record not found", error: err });
//       if (risk) {
//         // console.log(risk);
//         olderControlIds = risk.controlId;
//         let idsToDelete = [];
//         let idsToAdd = [];
//         let recdIds = req.body.controlId;
//         olderControlIds.forEach((id) => {
//           if (recdIds.indexOf(id) === -1) {
//             idsToDelete.push(id);
//           }
//         });
//         idsToAdd = recdIds.filter(function (el) {
//           return idsToDelete.indexOf(el) == -1;
//         });
//         console.log(
//           "idsToDelete",
//           idsToDelete,
//           olderControlIds,
//           recdIds,
//           idsToAdd
//         );
//         riskDetail.risk.updateOne(
//           { riskId: req.body.riskId },
//           { $set: { controlId: req.body.controlId } },
//           function (err, doc) {
//             console.log("update Response Line No263:", doc);
//             if (err) {
//               res.status(404).end({ message: "Record not found", error: err });
//             }
//             if (doc) {
//               updatedDoc = doc;
//               addRiskToControl(req.body.riskId, idsToAdd);
//               removeRiskFromControl(req.body.riskId, idsToDelete);
//               console.log(doc, doc.controlId);
//               res.send(doc);
//             }
//           }
//         );
//         // res.send(risk);
//       }
//     });
//     // if (req.body.riskId) {
//     // console.log("Update One Risk", req.body.controlId);
//     // await riskDetail.risk.update(
//     //   { riskId: req.body.riskId },
//     //   { $set: { controlId: req.body.controlId } },
//     //   function (err, doc) {
//     //     console.log("update Response Line No263:", doc);
//     //     if (err) {
//     //       res.status(404).end({ message: "Record not found", error: err });
//     //     }
//     //     if (doc) {
//     //       addRiskToControl(req.body.riskId, req.body.controlId);
//     //       console.log(doc);
//     //       res.send(doc);
//     //     }
//     //   }
//     // );
//     // }
//   } catch (e) {
//     return res.status(406).send(e);
//   }
// };

// update one risk assessment//
riskController.updateOneAssessmentRisk = async (req, res) => {
  console.log("req.body=================>>>", req.body);
  try {
    riskDetail.assess.findOneAndUpdate(
      {
        riskAssementId: req.body.riskAssementId,
      },
      {
        $set: {
          isAuditable: req.body.isAuditable,
          fieldAudit: req.body.fieldAudit,
          automatedControls: req.body.automatedControls,
          timePeriod: req.body.timePeriod,
          residualRisk: req.body.residualRisk,
          intrinsicRisk: req.body.intrinsicRisk,
          riskDepartment: req.body.riskDepartment,
        },
      },
      {
        new: true,
        useFindAndModify: false,
      },
      function (err, doc) {
        if (err) {
          console.log("err###", err);

          res.status(404).end({
            message: "Record not found",
            error: err,
          });
        } else {
          console.log("doc#########----------------->", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};

//num of risk //
riskController.getNumberofRisk = (req, res) => {
  try {
    riskDetail.risk.count((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

// post all risk //
riskController.postRisk = async (req, res) => {
  console.log("req.body:", req.body);
  let riskData = await new riskDetail.risk({
    riskId: req.body.riskId,
    controlId: req.body.controlId,
    riskName: req.body.riskName,
    riskStatus: req.body.riskStatus,
    riskDescription: req.body.riskDescription,
    riskSource: req.body.riskSource,
    riskDepartment: req.body.riskDepartment,
    riskProcess: req.body.riskProcess,
    riskCategory: req.body.riskCategory,
    riskArea: req.body.riskArea,
    riskApproach: req.body.riskApproach,
    riskOwner: req.body.riskOwner,
    riskOwnerFlag: req.body.riskOwnerFlag,
    riskPotentialoutcome: req.body.riskPotentialoutcome,
    controlRiskProgress: req.body.controlRiskProgress,
    riskSubmittedDate: req.body.riskSource,
    riskLastChangeDate: req.body.riskLastChangeDate,
    riskLastApprovedDate: req.body.riskLastApprovedDate,
    riskOwnerId: req.body.riskOwnerId,
    riskOwnerName: req.body.riskOwnerName,
    riskSubmitter: req.body.riskSubmitter,
    riskSubmitterId: req.body.riskSubmitterId,
    riskApprovedBy: req.body.riskApprovedBy,
    riskApproverId: req.body.riskApproverId,
    riskIdentifiedDate: req.body.riskIdentifiedDate,
    created_by: {
      _id: req.user.id,
      name: req.user.username
    },
    created_on: new Date()
  });

  // let controlData = await new controlDetails.control({
  //   riskId: req.body.riskId,
  //   controlId: req.body.controlId,
  //   controlName: req.body.controlName,
  //   controlType: "-",
  //   controlDescription: req.body.controlDescription,
  // });
  await riskData.save((err, riskDetails) => {
    if (err) {
      res.json(err);
    } else {
      // auditTrial(req.unique_request_id, "risks", riskDetails.riskId, riskDetails, "Insert", req.user.username);

      if (req.body.controlId) {
        console.log("req.body.controlId:", req.body.controlId);
        req.body.controlId.forEach((id) => {
          addControl(req.body.riskId, id, req.unique_request_id, req.user.username, req.user.id);

        });
        // addControl(req.body.riskId, req.body.controlId);

        // controlData.save((err, controlDetails) => {
        //   if (err) {
        //     res.json(err);
        //   } else {
        res.send(riskDetails);
        //   }
        // });
      } else {
        res.send(riskDetails);
      }
    }
  });
};

riskController.saveSingleRisk = async (req, res) => {
  try {
    let rd = await new riskDetail.risk(req.body);
    rd.save((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
// riskController.getAllRisksAssignedToControl = (req, res) => {
//   controlDetails.control.findOne(
//     { controlId: req.body.controlId },
//     { riskId: 1 },
//     function (err, data) {
//       if (err) return handleError(err);
//       console.log("data", data);
//       let query = JSON.parse(JSON.stringify(data.riskId));
//       console.log("query:", data.riskId, query, req.body.controlId);

//       riskDetail.risk.find(
//         { riskId: { $in: query }, controlId: { $in: req.body.controlId } },
//         (err, doc) => {
//           if (err) res.status(500).json(err);
//           res.send(doc);
//         }
//       );
//     }
//   );
// };

riskController.deleteRisk = (req, res) => {
  console.log(req.body);
  riskDetail.risk.findOneAndDelete(
    { riskId: req.body.riskId },
    (err, response) => {
      if (err) return handleError(err);
      if (response) {
        // auditTrial(req.unique_request_id, "risk", req.body.riskId, req.body, "Delete", req.user.username);

        riskDetail.risk.find((err, data) => {
          if (err) return handleError(err);
          res.end(JSON.stringify(data, null, 2));
        });
      }
    }
  );
};
//get all risk Assigned to control
riskController.getAllRisksAssignedToControl = (req, res) => {
  controlDetails.control.findOne(
    { controlId: req.body.controlId },
    { riskId: 1 },
    async (err, data) => {
      if (err) return handleError(err);
      if (data) {
        let risks = [];
        for (let i = 0; i < data.riskId.length; i++) {
          await riskDetail.risk.findOne(
            { riskId: data.riskId[i], controlId: { $in: req.body.controlId } },
            (err, response) => {
              if (err) res.status(500).json(err);
              console.log("response:", response);
              risks.push(response);
            }
          );
        }
        await console.log("risks response:", risks);
        await res.send(risks);
      } else {
        res.status(500).json(err);
      }
    }
  );
};
//get all controls assigned to risk
riskController.getControls = (req, res) => {
  riskDetail.risk.findOne(
    { riskId: req.body.riskId },
    { controlId: 1 },
    async (err, data) => {
      if (err) return handleError(err);
      if (data) {
        let controls = [];
        for (let i = 0; i < data.controlId.length; i++) {
          await controlDetails.control.findOne(
            {
              controlId: data.controlId[i],
              riskId: { $in: req.body.riskId },
            },
            (err, doc) => {
              if (err) res.status(500).json(err);
              controls.push(doc);
            }
          );
        }
        await console.log("controls response:", controls);
        await res.send(controls);
      }
    }
  );
};

riskController.getAllRisksRecords = (req, res) => {
  try {
    riskDetail.risk.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
riskController.getAllUnassignRisks = (req, res) => {
  try {
    riskDetail.risk.find(
      { controlId: { $exists: true, $size: 0 } },
      (err, data) => {
        if (err) {
          res.status(404).end("Record not found");
        } else {
          res.end(JSON.stringify(data, null, 2));
        }
      }
    );
  } catch (e) {
    return res.status(406).send(e);
  }
};
////////////////////// --------- masters of dropdowns -------- ////////////

riskController.getAllMasters = (req, res) => {
  try {
    riskMasters.masterRiskStatus.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        riskMasters.masterRiskSource.find((err, sourceData) => {
          if (err) {
            res.status(404).end("Record not found");
          } else {
            let masterData = {
                masterRiskStatus: data,
                masterRiskSource: sourceData,
              };
            res.end(JSON.stringify(masterData, null, 2));
          }
        });
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterOfRiskStatus = (req, res) => {
  try {
    riskMasters.masterRiskStatus.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
riskController.getMasterOfRiskSource = (req, res) => {
  try {
    riskMasters.masterRiskSource.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
riskController.getriskDepartment = (req, res) => {
  try {
    riskMasters.riskDepartment.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterOfRiskProcess = (req, res) => {
  try {
    riskMasters.masterRiskProcess.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterRiskCategory = (req, res) => {
  try {
    riskMasters.masterRiskCategory.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
riskController.getMasterRiskArea = (req, res) => {
  try {
    riskMasters.masterRiskArea.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterRiskApproach = (req, res) => {
  try {
    riskMasters.masteRiskApproach.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterRiskOwner = (req, res) => {
  try {
    riskMasters.masterRiskOwner.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterControlType = (req, res) => {
  try {
    riskMasters.masterControlType.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterControlDepartment = (req, res) => {
  try {
    riskMasters.masterControlDepartment.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterControlProcess = (req, res) => {
  try {
    riskMasters.masterControlProcess.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getMasterControl = (req, res) => {
  try {
    riskMasters.masterControl.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.getAllAssessData = async (req, res) => {
  try {
    riskDetail.assess
      .find({})
      .populate("riskId")
      .exec(function (err, data) {
        if (err) return handleError(err);

        res.send(data);
      });
  } catch (e) {
    return res.status(406).send(e);
  }
};

riskController.removeOneControlFromRisks = (req, res) => {
  try {
    req.body.riskId.forEach((id) => {
      riskDetail.risk.findOneAndUpdate(
        { riskId: id },
        {
          $pull: { controlId: req.body.controlId }, $set: {
            "updated_by._id": req.user.id,
            "updated_by.name": req.user.username,
            "updated_on": new Date()
          }
        }, { new: true },
        (err, response) => {
          if (err) return res.status(406).send(e);

          // auditTrial(req.unique_request_id, "risks", id, response, "Update", req.user.username);
          res.send(response);
        }
      );
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
module.exports = riskController;
