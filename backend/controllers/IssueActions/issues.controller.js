const issueDetail = require("../../models/IssueActions/issue.schema");
const actionModel = require("../../models/IssueActions/actions.schema");
const internalAudit = require("../../models/InternalAudit/internalAudit.schema");
const auditTrial = require("../../models/auditTrial/auditTrial.schema");
const controlDetail = require("../../models/RiskManagement/riskcontrol.schema");
const logger = require("../../utils/logger").logger;

const issueController = {};

//GET ACCOCIATE ISSUES FROM CONTROL
issueController.getAccociatedControl = (req, res) => {
  issueDetail.issue.findOne(
    { issueId: req.body.issueId },
    { controlId: 1 },
    function (err, data) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }
      let query;
      if (data.controlId) {
        query = JSON.parse(JSON.stringify(data.controlId));
      } else {
        query = [];
      }

      controlDetail.control.find(
        { controlId: { $in: query }, issueId: { $in: req.body.issueId } },
        (err, doc) => {
          if (err) {
            logger.pushError(err);
            res.status(500).json(err);
          }
          res.send(doc);
        }
      );
    }
  );
};

issueController.getAllIssuesRecords = (req, res) => {
  try {
    issueDetail.issue.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

issueController.getNumberofIssues = (req, res) => {
  try {
    issueDetail.issue.count((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
//////////// get all actions assign to issue ///////////
issueController.getActionsIn = (req, res) => {
  issueDetail.issue.findOne(
    { issueId: req.body.issueId },
    { actionId: 1 },
    async (err, data) => {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }
      if (data) {
        let actions = [];
        for (let i = 0; i < data.actionId.length; i++) {
          await actionModel.action.findOne(
            {
              actionId: data.actionId[i],
              issueId: { $in: req.body.issueId },
            },
            (err, doc) => {
              if (err) {
                logger.pushError(err);
                res.status(500).json(err);
              }
              actions.push(doc);
            }
          );
        }
        await logger.pushConsoleInfo("action response:", actions);
        await res.send(actions);
      }
    }
  );
};

//get action in issues //

issueController.getActions = (req, res) => {
  issueDetail.issue.findOne(
    { issueId: req.body.issueId },
    { actionId: 1 },
    function (err, data) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }
      logger.pushConsoleInfo("data", data);
      let query = JSON.parse(JSON.stringify(data.actionId));
      actionModel.action.find(
        { actionId: { $in: query }, issueId: { $in: req.body.issueId } },
        (err, doc) => {
          if (err) {
            logger.pushError(err);
            res.status(500).json(err);
          }
          res.send(doc);
        }
      );
    }
  );
};

//get one Issue //
issueController.getOneIssues = async (req, res) => {
  try {
    issueDetail.issue.findOne({ issueId: req.body.issueId }, function (
      err,
      data
    ) {
      if (err) {
        logger.pushError(err);
        return handleError(err);
      }

      res.send(data);
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

// UPDATE ONE ISSUE FORM //

issueController.updateOneIssue = async (req, res) => {
  try {
    issueDetail.issue.updateOne(
      { issueId: req.body.issueId },
      { $set: req.body },
      function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          // auditTrial(
          //   req.unique_request_id,
          //   "issues",
          //   req.body.issueId,
          //   req.body,
          //   "Update",
          //   req.user.username
          // );
          logger.pushConsoleInfo("doc", doc);
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//update issue and action
issueController.updateIssueAndAction = async (req, res) => {
  try {
    let issueId = req.body.issueId;
    issueDetail.issue.updateOne(
      { issueId: req.body.issueId },
      { $set: req.body },
      async function (err, doc) {
        if (err) {
          logger.pushError(err);
          res.status(404).end({ message: "Record not found", error: err });
        } else {
          if (req.body.actionId) {
            let actionIds = req.body.actionId;
            for (let i = 0; i < actionIds.length; i++) {
              await actionModel.action.findOneAndUpdate(
                { actionId: actionIds[i] },
                { $addToSet: { issueId: issueId } },
                { new: true }
              );
            }
          }
          if (req.body.controlId) {
            let controlId = req.body.controlId;
            for (let i = 0; i < controlId.length; i++) {
              let a = await controlDetail.control.findOneAndUpdate(
                { controlId: controlId[i] },
                { $addToSet: { issueId: issueId } },
                { new: true }
              );
              logger.pushConsoleInfo(a);
            }
          }
          res.send(doc);
        }
      }
    );
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

///.......save issue ..........///
issueController.saveIssue = async (req, res) => {
  let rd = await new issueDetail.issue(req.body);
  await rd.save(async (err, issueDetail) => {
    if (err) {
      logger.pushError(err);
      res.json(err);
    } else {
      let issueId = req.body.issueId;
      if (req.body.controlId && req.body.actionId.length) {
        let actionIds = req.body.actionId;
        for (let i = 0; i < actionIds.length; i++) {
          await actionModel.action.findOneAndUpdate(
            { actionId: actionIds[i] },
            { $addToSet: { issueId: issueId } },
            { new: true }
          );
        }
      }
      if (req.body.controlId && req.body.controlId.length) {
        let controlId = req.body.controlId;
        for (let i = 0; i < controlId.length; i++) {
          await controlDetail.control.findOneAndUpdate(
            { controlId: controlId[i] },
            { $addToSet: { issueId: issueId } },
            { new: true }
          );
        }
      }
      // let response = saveIssueId(req.body);
      saveIssueId(req.body);
      // if (response) {
      //   res.json(issueDetail);
      // } else {
      res.json(issueDetail);
      // }
    }
  });
};
function saveIssueId(data) {
  logger.pushConsoleInfo(data);
  internalAudit.auditMasters.updateOne(
    { auditId: data.auditId },
    { $push: { issueIds: data.issueId } },
    function (err, doc) {
      if (err) {
        logger.pushError(err);
        logger.pushConsoleInfo(err);
        // res.status(404).end({ message: "Record not found", error: err });
      } else {
        logger.pushConsoleInfo("doc", doc);
        return doc;
      }
    }
  );
}

///.......get multiple Issues by ID for Agile Audit Space ..........///
issueController.getMultipleIssues = async (req, res) => {
  logger.pushConsoleInfo("Body: ", req.body);
  issueDetail.issue.find({ issueId: { $in: req.body.issueId } }, (err, doc) => {
    if (err) {
      logger.pushError(err);
      res.status(500).json(err);
    }
    if (doc) res.send(doc);
  });
};

issueController.removeActionFromIssue = (req, res) => {
  try {
    logger.pushConsoleInfo("incoming data-->", {
      issueId: req.body.issueId,
      actionId: req.body.actionId,
    });
    req.body.issueId.forEach((id) => {
      issueDetail.issue.update(
        { issueId: id },
        {
          $pull: { actionId: req.body.actionId },
        },
        (err, response) => {
          logger.pushConsoleInfo("response-->", response);
          if (err) {
            logger.pushError(err);
            return res.status(406).send(err);
          }
        }
      );
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};
module.exports = issueController;