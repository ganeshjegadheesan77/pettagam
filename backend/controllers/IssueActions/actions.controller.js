const actionDetail = require('../../models/IssueActions/actions.schema');
const issueModel = require('../../models/IssueActions/issue.schema');
const actionController = {};

//num of allrecords for both tables //
actionController.getAllRecord = async (req, res) => {
  try {
    actionDetail.action.aggregate([
      {

        $lookup:
        {
          from: 'issues',
          localField: 'issueId',
          foreignField: 'issueId',
          as: 'actiondata'
        }

      }
    ]).exec(function (err, data) {
      if (err) {
        res.send('message:Error to get record', err)
      }
      data = data.filter(d=>d.issueId.length > 0);
      console.log(data);
      res.json(data)
    });
  } catch (e) {
    return res.status(406).send(e);
  }
}


actionController.getAllActionRecords = (req, res) => {

  try {
    actionDetail.action.find((err, data) => {
      if (err) {
        res.status(404).end("Record not found");

      } else {
        res.end(JSON.stringify(data, null, 2)

        );
      }

    });
  } catch (e) {
    return res.status(406).send(e);
  }


}




actionController.getNumberofActions = (req, res) => {

  try {
    actionDetail.action.count((err, data) => {
      if (err) {
        res.status(404).end("Record not found");

      } else {
        res.end(JSON.stringify(data, null, 2)

        );
      }

    });
  } catch (e) {
    return res.status(406).send(e);
  }


}




//get one action //
actionController.getOneAction = async (req, res) => {
  try {
    actionDetail.action.findOne({ actionId: req.body.actionId }, function (err, data) {
      if (err) return handleError(err);

      res.send(data);
    })
  }
  catch (e) {
    return res.status(406).send(e);
  }

}


//get issues in action //
// 

actionController.getIssue = (req, res) => {
  actionDetail.action.findOne({ actionId: req.body.actionId }, { issueId: 1 }, function (err, data) {
    if (err) return handleError(err);
    let query = JSON.parse(JSON.stringify(data.issueId))
    issueModel.issue.find({ issueId: { $in: query }, actionId: { $in: req.body.actionId } },
      (err, doc) => {
        if (err) res.status(500).json(err);
        res.send(doc);
      });

  }
  )
}
actionController.getLocationAction = async (req, res) => {
  try {
    actionDetail.action.find({ location: req.body.location }, function (err, data) {
      if (err) return handleError(err);

      res.send(data);
    })
  }
  catch (e) {
    return res.status(406).send(e);
  }

}
// UPDATE ONE ACTION FORM //

actionController.updateOneAction = async (req, res) => {

  try {
    let actionId = req.body.actionId;
    actionDetail.action.updateOne(
      { "actionId": actionId },
      { "$set": req.body },
      function (err, doc) {

        if (err) {
          res.status(404).end({ message: "Record not found", error: err });

        } else {
          let issueIds = req.body.issueId ? req.body.issueId : [];
          if (issueIds.length) {
            issueIds.forEach(async (id) => {
              await issueModel.issue.findOneAndUpdate({ "issueId": id }, { $addToSet: { "actionId": actionId } }, { new: true });
            })
          }
          res.send(doc)
        }

      });
  } catch (e) {
    return res.status(406).send(e);
  }


}

actionController.deleteAIFile = async (req, res) => {

  try {
    actionDetail.action.updateOne(
      { actionId: req.body.actionId },
      { $pull: { attachment: req.body.fileName } },
      function (err, doc) {

        if (err) {
          res.status(404).end({ message: "Record not found", error: err });

        } else {
          res.send(doc)
        }

      });
  } catch (e) {
    return res.status(406).send(e);
  }
}

/////////////save actions/////
actionController.saveAction = (req, res) => {
  console.log("req.body--->", req.body);
  let rd = new actionDetail.action(req.body);
  rd.save((err, actionDetail) => {
    if (err) {
      res.json(err);
    } else {
      let issueIds = req.body.issueId ? req.body.issueId : [];
      let actionId = req.body.actionId;
      if (issueIds.length) {
        for (let i = 0; i < issueIds.length; i++) {
          issueModel.issue.findOneAndUpdate({ "issueId": issueIds[i] }, { $addToSet: { "actionId": actionId } }, { new: true })
          .then((r) => {
          })
          .catch((e) => { console.log("in catch-->", e) })
        }
      }

      res.json(actionDetail);
    }
  });
}
actionController.getActioninArray = async (req, res) => {
  try {
    actionDetail.action.find({ actionId: { $in: [req.query.actionId] } }, function (err, data) {
      if (err) return handleError(err);

      res.send(data);
    })
  }
  catch (e) {
    return res.status(406).send(e);
  }

}
actionController.getOneWF = async (req, res) => {
  try {
    actionDetail.actionwf.findOne({ actionWFId: req.body.actionWFId }, function (err, data) {
      if (err) return handleError(err);

      res.send(data);
    })
  }
  catch (e) {
    return res.status(406).send(e);
  }

}
actionController.saveActionWF = async (req, res) => {

  let rd = await new actionDetail.actionwf(req.body);
  await rd.save((err, actionDetail) => {
    if (err) {
      res.json(err);
    } else {
      res.json(actionDetail);
    }
  });
}
actionController.updateOneActionWF = async (req, res) => {

  try {
    actionDetail.actionwf.updateOne(
      { "actionWFId": req.body.actionWFId },
      { "$set": req.body },
      function (err, doc) {

        if (err) {
          res.status(404).end({ message: "Record not found", error: err });

        } else {
          res.send(doc)
        }

      });
  } catch (e) {
    return res.status(406).send(e);
  }
}
actionController.removeOneIssueFromAction = (req, res) => {
  try {
    console.log("removeOneIssueFromAction parmas" ,{actionId : req.body.actionId, issueId : req.body.issueId });
    req.body.actionId.forEach((id) => {
      actionDetail.action.update(
        { actionId: id },
        {
          $pull: { issueId: req.body.issueId }
        },
        (err, response) => {
          if (err) return res.status(406).send(e);
          console.log("removeOneIssueFromAction-->",response);
        }
      );
    });
  } catch (e) {
    return res.status(406).send(e);
  }
};
module.exports = actionController;