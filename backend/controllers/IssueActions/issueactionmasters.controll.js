const master = require("../../models/IssueActions/masters.schema");
const logger = require("../../utils/logger").logger;

const masterController = {};

//ACTION SOURCE MASTER
masterController.actionStatusMaster = (req, res) => {
  try {
    master.masterActionStatus.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//ACTION rating MASTER
masterController.actionRatingMaster = (req, res) => {
  try {
    master.masterActionRating.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

masterController.masterArea = (req, res) => {
  try {
    master.masterarea.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

masterController.issuePriorityMaster = (req, res) => {
  try {
    master.masterissuepriority.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//ISSUE STATUS MASTER
masterController.issueStatusMaster = (req, res) => {
  try {
    master.masterIssueStatus.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//ISSUE TYPE MASTER
masterController.issueTypeMaster = (req, res) => {
  try {
    master.masterIssueType.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

module.exports = masterController;