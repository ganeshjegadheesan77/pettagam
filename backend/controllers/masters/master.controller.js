const master = require("../../models/RiskManagement/riskmasters.schema");
const logger = require("../../utils/logger").logger;

const masterController = {};

//RISK AREA MASTER
masterController.riskAreaMaster = (req, res) => {
  try {
    master.masterRiskArea.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//RISK SOURCE MASTER
masterController.riskSourceMaster = (req, res) => {
  try {
    master.masterRiskSource.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//RISK CATEGORY MASTER
masterController.riskCategoryMaster = (req, res) => {
  try {
    master.masterRiskCategory.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//RISK DEPARTMENT MASTER
masterController.riskDepartmentMaster = (req, res) => {
  try {
    master.riskDepartment.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//RISK PROCESS MASTER
masterController.riskProcessMaster = (req, res) => {
  try {
    master.masterRiskProcess.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

//RISK APPROCH MASTER
masterController.riskApprochMaster = (req, res) => {
  try {
    master.masterRiskApproach.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

masterController.frequencyMaster = (req, res) => {
  try {
    master.masterFrequency.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

masterController.masterGuidance = (req, res) => {
  try {
    master.mastercontrolguidance.find((err, data) => {
      if (err) {
        logger.pushError(err);
        res.status(404).end("Record not found");
      } else {
        res.end(JSON.stringify(data, null, 2));
      }
    });
  } catch (e) {
    logger.pushError(e);
    return res.status(406).send(e);
  }
};

module.exports = masterController;