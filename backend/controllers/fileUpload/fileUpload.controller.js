const FileUpload = require("../../models/fileUpload/fileUpload.schema");
exports.createFileRoom = (data, io) => {  
  // Create a room
  const room = new FileUpload({
    title: data.title,
    usernames: data.usernames,
    owner: data.owner,
    createdAt: new Date()
  });

  // Save room in the database
  room
    .save()
    .then(res => {
      FileUpload.find({ owner: data.owner }).then(fileRooms => {
        io.emit("getFileRoomsList", fileRooms);
      });
      // io.emit('roomList', data);
    })
    .catch(err => {
      io.emit(
        "getFileRoomsList",
        err.message || "Some error occurred while creating the fileRooms."
      );
    });
};
exports.findFileRooms = user => {
  return new Promise((resolve, reject) => {
    FileUpload.find({ usersName: { $in: [user] } })
      .then(rooms => {
        FileUpload.find({ owner: user })
          .then(ownerRooms => {
            // console.log("Rooms List:", ownerRooms);
            resolve(rooms.concat(ownerRooms));
          })
          .catch(err => reject(err));
      })
      .catch(err => {
        reject(err);
      });
  });
};
exports.delete = (data, io) => {
  return new Promise((resolve, reject) => {
    FileUpload.deleteOne({ _id: data })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
exports.addUsersToFileroom = (data, io) => {
  if (!data.username) {
    io.emit("addUsersToRoom", "adding users to room failed");
  }
  // console.log("data::", data);
  FileUpload.findOneAndUpdate(
    { _id: data.roomId },
    { $push: { usersName: data.username } },
    { new: true }
  )
    .then(r => {
      FileUpload.find({ _id: data.roomId })
        .then(room => {
          io.emit("getRoomDetails", room);
        })
        .catch(err => console.log(err));
    })
    .catch(err => {
      io.emit("getRoomDetails", err);
    });
};
exports.newFile = (data, io) => {
  // Validate request
  if (!data) {
    io.emit("addFilesToRoom", "adding files to room failed");
  }
  console.log("file Data:", data);

  FileUpload.findOneAndUpdate(
    { _id: data.rooId },
    { $push: { fileName: data.fileName } },
    { new: true }
  )
    .then(r => {
      FileUpload.find({ _id: data.rooId })
        .then(room => {
          io.emit("getRoomDetails", room);
        })
        .catch(err => console.log(err));
    })
    .catch(err => {
      io.emit("getRoomDetails", err);
    });
};
exports.updateFileroom = (data, io) => {
  if (!data.fileName) {
    io.emit("addFilesToRoom", "adding files to room failed");
  }
  FileUpload.findOneAndUpdate(
    { _id: data.roomId },
    { $push: { fileName: data.fileName } },
    { new: true }
  )
    .then(r => {
      FileUpload.find({ _id: data.roomId })
        .then(room => {
          io.emit("getFileRoomDetails", room);
        })
        .catch(err => console.log(err));
    })
    .catch(err => {
      io.emit("getFileRoomDetails", err);
    });
};

exports.getRoomDetailsById = id => {
  if (!id) {
    console.log(err)
    // io.emit("addFilesToRoom", "adding files to room failed");
  }
  return new Promise((resolve, reject) => {
    FileUpload.find({ _id: id })
      .then(room => {
        resolve(room);
      })
      .catch(err => reject(err));
  });
};
