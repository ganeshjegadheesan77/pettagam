const riskDetail = require('../../models/RiskManagement/riskcontrol.schema');
const dashboardController = {};


dashboardController.getNumberofRisk = async  (req, res) => {
    try{

        riskDetail.risk.count((err, data) => {
            if (err) {
              res.status(404).end("Record not found");
        
            } else {
              res.end(JSON.stringify(data, null, 2)
               
              );
            }
        });

    }catch(e){
        return res.status(406).send(e);
    }
 
}

module.exports = dashboardController;