const sdbox = require("../../models/digilocker/sdbox.schema");
const sdboxController = {};

sdboxController.getAllSdboxRecords = async (req, res) => {
    try {
        sdbox.sdbox.find().then((sdboxData)=>{
        // await req.user.remove();
        console.log("output", sdboxData);
        return res.json(sdboxData);
        });
    }catch (e) {
        console.error(e);
        return res.status(406).send(e);
    }
};

module.exports = sdboxController;