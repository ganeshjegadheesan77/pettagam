module.exports = {
    httpCode : {
        success : 200,
        notFound : 404,
        unauthorized : 401,
        error : 500,
        badRequest : 400
    },
    docType : {
        workPaper : "workPaper",
        referenceDoc : "referenceDoc"
    }

}