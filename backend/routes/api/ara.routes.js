const ARAController = require('./../../controllers/internalAudit/ARA.controller');

module.exports = (router) => {
    router.post('/ara/getOldAuditDetails', ARAController.getOldAuditDetails);
    router.post('/ara/getIssueDetails', ARAController.getIssueDetails);
    router.post('/ara/saveARA', ARAController.saveARA);
    router.post('/ara/getARADetails' , ARAController.getARADetails);
    router.post('/ara/updateARADetails' , ARAController.updateARADetails);
}