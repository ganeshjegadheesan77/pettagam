const riskController = require("../../controllers/Risk Management/risk.controller");

module.exports = (router) => {
  //----------------Risk Functionality ---------//
  //-- get One Risk --//
  router.post("/forms/risk/getOneRisk", riskController.getOneRisk);

  //-- get All Risk --//
  router.get("/forms/risk/getAllRecords", riskController.getAllRecords);

  //-- get Risk Count --//
  router.get("/forms/risk/getRiskCount", riskController.getNumberofRisk);
  router.get(
    "/forms/risk/getAllUnassignRisks",
    riskController.getAllUnassignRisks
  );
  //-- get Risk updateOne --//
  router.post("/forms/risk/updateOneRisk", riskController.updateOneRisk);
 

  //get assigned Risks to Control
  router.post(
    "/forms/risk/getAssignedRisks",
    riskController.getAllRisksAssignedToControl
  );

  //-- Save Risk with Control  --//
  router.post("/forms/risk/saveRisk", riskController.postRisk);
  //----- removeOneControlFromRisks --------///
  router.post(
    "/forms/risk/removeOneControlFromRisks",
    riskController.removeOneControlFromRisks
  );

  //Save Only Simple Risk
  router.post("/forms/risk/saveRiskOnly", riskController.saveSingleRisk);

  //-- Save Controls  --//
  router.post("/forms/risk/controls", riskController.getControls);
  //----Delete One Risk ----------//
  router.post("/forms/risk/deleteRisk", riskController.deleteRisk);


  //----Risk Assessment Update--//
   router.put("/forms/risk/updateOneRiskAssessment", riskController.updateOneRiskAssessment);

  //-- Add New Control to Risk  --//
  router.post(
    "/forms/risk/addNewControlToRisk",
    riskController.addNewControlToRisk
  );

  //-- get AllRisk  --//
  router.get(
    "/forms/risk/getAllRisksRecords",
    riskController.getAllRisksRecords
  );

  router.post(
    "/forms/risk/getRiskWithAssessment",
    riskController.getRiskWithAssessment
  );

  router.post(
    "/forms/risk/postOneAssessmentRisk",
    riskController.postOneAssessmentRisk
  );

  router.put(
    "/forms/risk/updateOneAssessmentRisk",
    riskController.updateOneAssessmentRisk
  );

  router.get(
    "/forms/riskassessment/getAllRiskAssessment",
    riskController.getAllRiskAssessment
  );

  router.get(
    "/forms/risk/getAllRiskAssessmentList",
    riskController.getAllRiskAssessmentList
  );

  router.post(
    "/forms/riskAssessment/getAssessmentDetails",
    riskController.getAssessmentDetails
  );

  router.post(
    "/forms/riskAssessment/getRiskAssessmentDetails",
    riskController.getRiskAssessmentDetails
  );

  router.post(
    "/forms/riskassessment/getOneRiskAssessment",
    riskController.getOneRiskAssessment
  );

  router.post(
    "/forms/riskassessment/getAllAssessData",
    riskController.getAllAssessData
  );

  //------------------Masters -------------------//

  router.get("/riskforms/master/getallMasters", riskController.getAllMasters);

  router.get(
    "/riskforms/master/getMasterOfRiskStatus",
    riskController.getMasterOfRiskStatus
  );

  router.get(
    "/riskforms/master/getMasterOfRiskSource",
    riskController.getMasterOfRiskSource
  );

  router.get(
    "/riskforms/master/getriskDepartment",
    riskController.getriskDepartment
  );

  router.get(
    "/riskforms/master/getMasterOfRiskProcess",
    riskController.getMasterOfRiskProcess
  );

  router.get(
    "/riskforms/master/getMasterRiskCategory",
    riskController.getMasterRiskCategory
  );

  router.get(
    "/riskforms/master/getMasterRiskArea",
    riskController.getMasterRiskArea
  );

  router.get(
    "/riskforms/master/getMasterRiskApproach",
    riskController.getMasterRiskApproach
  );

  router.get(
    "/riskforms/master/getMasterRiskOwner",
    riskController.getMasterRiskOwner
  );

  router.get(
    "/riskforms/master/getMasterControlType",
    riskController.getMasterControlType
  );

  router.get(
    "/riskforms/master/getMasterControlDepartment",
    riskController.getMasterControlDepartment
  );

  router.get(
    "/riskforms/master/getMasterControlProcess",
    riskController.getMasterControlProcess
  );

  router.get(
    "/riskforms/master/getMasterControl",
    riskController.getMasterControl
  );
};
