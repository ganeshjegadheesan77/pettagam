const internalAuditController = require("../../controllers/internalAudit/internalAudit.controller");

//RoomController
const roomController = require("../../controllers/chatSystem/room.controller");

// postARAForm
const ARAForm = require("../../controllers/internalAudit/ARA.controller");
// collab chat system
const collabChatSystem = require("../../controllers/chatSystem/collabChatSystem.controller");
module.exports = (router) => {
  // ---------------------------------------------------------------------------------------------//
  // router.post("/ia/postARAForm", ARAForm.postARAForm);
  // router.post("/ia/getARAForm", ARAForm.getARAForm);
  // router.post("/ia/updateARAForm", ARAForm.updateARAForm);

  // ---------------------------------------------------------------------------------------------//
  router.post("/ia/uni/getCommodity", internalAuditController.getCommodity);
  router.post("/ia/uni/getRegion", internalAuditController.getRegion);
  router.post("/ia/uni/getLocation", internalAuditController.getLocation);
  router.post("/ia/uni/getSite", internalAuditController.getSite);
  // ---------------------------------------------------------------------------------------------//

  router.get(
    "/ia/generateCurrentYearAudits",
    internalAuditController.generateCurrentYearAudits
  );
  router.get(
    "/ia/getCurrentYearAudits",
    internalAuditController.getCurrentYearAudits
  );
  router.get(
    "/ia/getLastIdCollection",
    internalAuditController.getLastIdCollection
  );
  //-----------------------------------------------------------------//
  router.get(
    "/ia/masters/getMasterDivision",
    internalAuditController.getMasterDivision
  );
  router.get(
    "/ia/masters/getMasterDepartment",
    internalAuditController.getMasterDepartment
  );
  router.get(
    "/ia/masters/getMasterLeadContact",
    internalAuditController.getMasterLeadContact
  );
  router.get(
    "/ia/masters/getMasterProcess",
    internalAuditController.getMasterProcess
  );
  // ---------------------------------------------------------------------------------------------//

  router.post(
    "/ia/getAuditDataForRiskAssess",
    internalAuditController.getAuditDataForRiskAssess
  );
  router.post("/ia/getIssueList", internalAuditController.getIssueList);

  router.post(
    "/ia/saveAuditPlanMemo",
    internalAuditController.saveAuditPlanMemo
  );
  router.post(
    "/ia/updateAuditPlanMemo",
    internalAuditController.updateAuditPlanMemo
  );

  router.post(
    "/ia/saveAuditOpeningMeeting",
    internalAuditController.saveAuditOpeningMeeting
  );
  router.post(
    "/ia/updateAuditOpeningMeeting",
    internalAuditController.updateAuditOpeningMeeting
  );

  router.post(
    "/ia/saveAuditAnnounce",
    internalAuditController.saveAuditAnnounce
  );
  router.post(
    "/ia/updateAuditAnnounce",
    internalAuditController.updateAuditAnnounce
  );

  router.post(
    "/ia/saveAuditFeldWork",
    internalAuditController.saveAuditFeldWork
  );

  router.post(
    "/ia/saveAuditDraftReport",
    internalAuditController.saveAuditDraftReport
  );
  router.post(
    "/ia/updateAuditDraftReport",
    internalAuditController.updateAuditDraftReport
  );

  router.post("/ia/saveAuditPBC", internalAuditController.saveAuditPBC);
  router.post("/ia/saveAuditIssues", internalAuditController.saveAuditIssues);
  router.post(
    "/ia/saveAuditClosingMeeting",
    internalAuditController.saveAuditClosingMeeting
  );

  router.post("/ia/getAuditData", internalAuditController.getAuditData);

  router.post("/ia/getTestAuditData", internalAuditController.getTestAuditData);
  router.post("/ia/postData", internalAuditController.postAuditData);

  router.post(
    "/ia/addNewAuditInUniverse",
    internalAuditController.addNewAuditInUniverse
  );

  router.post(
    "/ia/getAuditListFieldWork",
    internalAuditController.getAuditListFieldWork
  );
  router.post("/ia/updateUniverse", internalAuditController.updateUniverse);
  router.post("/ia/findFieldWorks", internalAuditController.findFieldWorks);
  router.post("/ia/updateAudit", internalAuditController.updateAudit);

  router.post("/ia/getAuditMemo", internalAuditController.getAuditMemo);
  router.post("/ia/updateOpenMemo", internalAuditController.updateOpenMemo);

  router.post(
    "/ia/getOpeningMeeting",
    internalAuditController.getOpeningMeeting
  );
  router.post("/ia/updateOpenMeet", internalAuditController.updateOpenMeet);

  router.post("/ia/getAnnouncement", internalAuditController.getAnnouncement);
  router.post(
    "/ia/updateAnnouncement",
    internalAuditController.updateAnnouncement
  );

  router.post("/ia/getFieldWorkData", internalAuditController.getFieldWorkData);
  router.post("/ia/updateFieldWork", internalAuditController.updateFieldWork);

  router.post(
    "/ia/getSpecificNewAudit",
    internalAuditController.getSpecificNewAudit
  );

  router.post("/ia/getDraftReport", internalAuditController.getDraftReport);

  router.post(
    "/ia/updateDraftReport",
    internalAuditController.updateDraftReport
  );

  router.post("/getLastId", internalAuditController.getLastGeneratedId);
  router.post("/updateLastId", internalAuditController.updateLastId);

  router.post(
    "/ia/field-work/dashboard/getNoOfAudits",
    internalAuditController.getNoOfAudits
  );

  router.post("/ia/issues/updateIssue", internalAuditController.updateIssue);
  router.post("/ia/issues/updatePBC", internalAuditController.updatePBC);

  router.post("/ia/getPBC", internalAuditController.getPBC);
  router.post("/ia/updatePBC", internalAuditController.updateSinglePBC);

  router.post(
    "/ia/updateCMIntoAuditMaster",
    internalAuditController.updateCMIntoAuditMaster
  );
  router.post("/ia/updateSingleCM", internalAuditController.updateSingleCM);

  // router.post('/ia/field-work/dashboard/getNoOfAnnounced' , internalAuditController.getNoOfAnnounced);
  //getInterviews
  router.get("/ia/getInteview", internalAuditController.getInterviews);
  router.post("/ia/updateOne", internalAuditController.updateInterview);
  router.get("/ia/getCountData", internalAuditController.getCountData);
  router.post("/ia/updateCountData", internalAuditController.updateCountData);
  router.post("/ia/saveInterview", internalAuditController.saveInterview);
  router.post("/ia/deleteInterview", internalAuditController.deleteInterview);
  router.post("/ia/getRoomId", roomController.findRoomByAudit);
  router.post("/ia/createRoom", roomController.createRoom);
  router.post("/ia/createNewChatroom", collabChatSystem.createNewChatroom);
  router.post("/ia/getRooms", collabChatSystem.findRoomById);
};
