const internalAuditController = require("../../controllers/internalAudit/internalAudit.controller");

const fieldWorkController = require('./../../controllers/internalAudit/fieldWork.controller');

module.exports = (router) => {

  router.get('/fw/getActiveAudits', fieldWorkController.getActiveAudits);
  router.get('/ar/getLocation' , fieldWorkController.getLocation);
  router.post('/ar/getSites' , fieldWorkController.getSites);
  router.get('/ar/getCompletedAudit' , fieldWorkController.getCompletedAudit);
  
  router.post('/fw/getIssuesAttached' , fieldWorkController.getIssuesAttached);

  router.post('/fw/updateOverallProgress' , fieldWorkController.updateOverallProgress);
  router.post('/fw/updateUniverseData' , fieldWorkController.updateUniverseData);
  
  
  router.post('/fw/getTeamForPbcAndFW' , fieldWorkController.getTeamForPbcAndFW);
  router.get('/fw/auditsForyearCronJob', fieldWorkController.generateCurrentYearAudits);
  router.post('/fw/getAuditDetail', fieldWorkController.getAuditDetail);
  router.post('/fw/updateAudit',fieldWorkController.updateAudit);
  router.post('/fw/getAnnouncementData',fieldWorkController.getAnnouncementData);
  router.post('/fw/saveAnnouncementData',fieldWorkController.saveAnnouncementData);
  router.post('/fw/getClosingMeetingData',fieldWorkController.getClosingMeetingData);
  router.post('/fw/getOpenMeetUpData',fieldWorkController.getOpenMeetUpData);  
  router.post('/fw/getDraftRepoData',fieldWorkController.getDraftRepoData); 
  router.post('/fw/saveOpenMeetUptData',fieldWorkController.saveOpenMeetUptData);
  router.post('/fw/saveMemorandumData',fieldWorkController.saveMemorandumData);
  router.post('/fw/getOpenMemotUpData',fieldWorkController.getOpenMemotUpData);
  router.post('/fw/saveDraftRepoData',fieldWorkController.saveDraftRepoData);
  router.post('/fw/saveCloseMeetData',fieldWorkController.saveCloseMeetData);
  router.post('/fw/getAuditDetailForRP',fieldWorkController.getAuditDetailForRP);
  router.post('/fw/updateLastIdCollection',fieldWorkController.updateLastIdCollection);
  router.post('/fw/auditsCount',fieldWorkController.auditCount);
  router.post('/fw/savePBC' , fieldWorkController.savePBC);
  router.post('/fw/getPBCDesc',fieldWorkController.getPBCDesc);
  router.post('/fw/getPBCDetails',fieldWorkController.getPBCDetails);
  router.post('/fw/updatePBCDetails',fieldWorkController.updatePBCDetails);
  router.post('/fw/addToCurrentYear' , fieldWorkController.addToCurrentYear);
  router.get('/fw/generateCurrentYearAudits' , fieldWorkController.generateCurrentYearAudits);

  router.get('/fw/getAudits',fieldWorkController.getAudits);
  router.get('/fw/getSubprocesses',fieldWorkController.getSubproceses);
  router.post('/fw/getRisks',fieldWorkController.getRisks);
  router.post('/fw/getFWDesc',fieldWorkController.getFWDesc);
  router.post('/fw/saveFW',fieldWorkController.saveFW);
  router.post('/fw/getFWDetails',fieldWorkController.getFWDetails);
  router.post('/fw/updateFWDetails',fieldWorkController.updateFWDetails);


  
}