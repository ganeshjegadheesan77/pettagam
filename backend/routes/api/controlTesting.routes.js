const controlTestingController = require('../../controllers/controlTesting/controlTesting.controller');

module.exports = (router) => {


     //-- get ControlTesting --//
     router.post('/forms/controlTesting/getOneControlTesting',controlTestingController.getOneControlTesting);

     //updateControlTesting
     router.post('/forms/controlTesting/updateOneControlTesting', controlTestingController.updateOneControlTesting)
     //getAllControlTestRecords
     router.post('/forms/controlTesting/getAllControlTestRecords', controlTestingController.getAllControlTestRecords)
     //createnew Control
     router.post('/forms/controlTesting/saveControlTest',controlTestingController.saveControlTest)
     //updateFile
     router.post('/forms/controlTesting/deleteFile',controlTestingController.deleteCTFile)
     //deleteIssue
     router.post('/forms/controlTesting/deleteIssueFromControlTesting',controlTestingController.deleteIssueFromControlTesting);
}