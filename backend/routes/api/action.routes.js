const actionController = require('../../controllers/IssueActions/actions.controller');
const actionsController = require('../../controllers/IssueActions/issueactionmasters.controll');

module.exports = (router) => {
  //-- get All actions & issues --//
  router.get('/actions/getAllRecord',actionController.getAllRecord);

    //-- get All actions --//  
    router.get('/actions/getAllActionRecords',actionController.getAllActionRecords);

    //-- get No. Of  actions --// 
    router.get('/actions/getNumberofActions',actionController.getNumberofActions);

      //-- get One. action --// 
      router.post('/actions/getOneAction',actionController.getOneAction);




       //-- GET STATUS MASTER--// 
      router.get('/actions/actionStatusMaster',actionsController.actionStatusMaster);

      //-- GET RATING MASTER--// 
    router.get('/actions/actionRatingMaster',actionsController.actionRatingMaster);

     //-- area master --//
    router.get('/actions/masterArea',actionsController.masterArea);


    router.post('/actions/getIssue',actionController.getIssue);

         //-- update one Action--// 
    router.post('/actions/updateOneAction',actionController.updateOneAction);
        //-- get one Action acc to loctaion--// 
    router.post('/actions/getLocationAction',actionController.getLocationAction);
    //--- REMOVE ISSUES FROM ACTION
    router.post('/actions/removeOneIssueFromAction', actionController.removeOneIssueFromAction );

       /////-- save action--/////
    router.post('/actions/saveAction',actionController.saveAction);

    router.get('/actions/getActioninArray',actionController.getActioninArray)

    router.post('/actions/deleteFile',actionController.deleteAIFile)
    router.post('/actions/getOneActionWorkflow',actionController.getOneWF)
    router.post('/actions/updateActionflow',actionController.updateOneActionWF)
    router.post('/actions/saveActionflow',actionController.saveActionWF)

}