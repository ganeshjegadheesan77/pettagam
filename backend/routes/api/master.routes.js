const masterController = require("../../controllers/masters/master.controller");

module.exports = router => {
    
    //RISK AREA
    router.get("/master/riskArea", masterController.riskAreaMaster);
    //RISK SOURCE
    router.get("/master/riskSource", masterController.riskSourceMaster);
    //RISK CATEGORY
    router.get("/master/riskCategory", masterController.riskCategoryMaster);
    //RISK DEPARTMENT
    router.get("/master/riskDepartment", masterController.riskDepartmentMaster);
    //RISK PROCESS
    router.get("/master/riskProcess", masterController.riskProcessMaster);
    //RISK APPROCH
    router.get("/master/riskApproch", masterController.riskApprochMaster);

    //CONTROL FREQUENCY     
    router.get("/master/controlFrequency", masterController.frequencyMaster);
    router.get("/master/controlGuidance", masterController.masterGuidance);
};  

