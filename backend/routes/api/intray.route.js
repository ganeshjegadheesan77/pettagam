let intryController = require('../../controllers/chatSystem/intray.controller');
module.exports = (router) => {
   router.get('/intray/getWorkpaperNumber',intryController.getWpNumber);
   router.post('/intray/setWorkPaperNumber', intryController.updateWpNumber);
   router.post('/intray/getFileDetails', intryController.getFileDetails);
   router.post('/intray/updateIntrayDoc', intryController.updateIntrayDoc);
   router.post('/intray/deleteFileIntray', intryController.deleteFileIntray);
   router.post('/intray/updateFileNameInRoom', intryController.updateFileNameInRoom)

  };