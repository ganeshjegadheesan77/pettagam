const controlController = require("../../controllers/Risk Management/control.controller");

module.exports = (router) => {
  //----------------Control Functionality ---------//

  //-- get All Controls Count --//
  router.get(
    "/forms/control/getControlCount",
    controlController.getNumberofControl
  );

  //
  router.post("/control/removeIssuesFromControl", controlController.removeIssuesFromControl);

  //  router.post('/forms/control/getControlsForControl',controlController.getControlsForControl);

  //--get One Control --//
  router.post("/forms/control/getOneControl", controlController.getOneControl);
  //----- delete one control -------//
  router.post("/forms/risk/deleteControl", controlController.deleteControl);
  //----- removeOneRiskFromControls -------//
  router.post(
    "/forms/risk/removeOneRiskFromControls",
    controlController.removeOneRiskFromControls
  );

  ////////////////////////workflow routes//////////////////////////

  router.post('/forms/control/getOneWF', controlController.getOneWF)


  router.get('/forms/control/getAllcontrolWFlist', controlController.getAllcontrolWFlist)


  router.post('/forms/control/saveControlWF', controlController.saveControlWF)


  router.post('/forms/control/updateOneControlls', controlController.updateOneControlls)


  router.get("/forms/control/getMasterCWFrequency", controlController.getMasterCWFrequency);
 
  // router.post('/forms/control/getAreaInRisk',controlController.getAreaInRisk);  


  // router.post('/forms/control/updateOneControlWF',controlController.updateOneControlWF)  

  router.post('/forms/control/updateOneControlWF', controlController.updateOneControlWF)

  //////////////////////////////////////////////////////////////////

  //--update One Control --//
  router.post(
    "/forms/control/updateOneControl",
    controlController.updateControl
  );
  router.post(
    "/forms/control/updateOneControlforTest",
    controlController.updateControlforTest
  );
  //--Save One Control --//
  router.post("/forms/risk/saveControls", controlController.saveControls);

  //-- get AllControls  --//
  router.get(
    "/forms/control/getAllControlRecords",
    controlController.getAllControlRecords
  );
  router.get("/forms/control/getCRMRecords", controlController.getCRMRecords);

  //-- get AllUnassignControls  --//
  router.get(
    "/forms/control/getUnassignControlCount",
    controlController.getUnassignControlCount
  );
  router.get(
    "/forms/control/getAllUnassignControls",
    controlController.getAllUnassignControls
  );

  router.get(
    "/riskforms/master/getMasterControlStatus",
    controlController.getMasterControlStatus
  );
  router.get(
    "/riskforms/master/getMasterControlType",
    controlController.getMasterControlType
  );

  router.get(
    "/riskforms/master/getMasterControlKeyProcess",
    controlController.getMasterControlKeyProcess
  );
};
