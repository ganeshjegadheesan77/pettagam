const auditController = require("../../controllers/Audit/audit.controller");
const agileDashboard = require("../../controllers/agileDashboard/agileDashboard.controller");

module.exports = (router) => {
  //-- get All audit --//
  router.get("/audits/getAllAuditRecords", auditController.getAllAuditRecords);

  //-- get one Action acc to loctaion--//
  router.post("/audits/getLocationAudit", auditController.getLocationAudit);

  // get all action reports //
  router.get("/audits/getAllAuditReport", auditController.getAllAuditReport);

  router.post("/audits/getOneRp", auditController.getOneRP);

  //resource planning
  router.get("/audits/getAllRP", auditController.getAllRPrecords);
  router.post("/audits/updateRP", auditController.updateRp);
  router.post("/audits/saveRP", auditController.saveRp);
  router.post("/audits/updateOneAudit", auditController.updateOneAudit);
  //Agile Audit
  router.post("/audits/getMyARADetails", agileDashboard.getMyARADetails);
  router.post("/audits/getMyAuditDetails", agileDashboard.getMyAuditDetails);
};
