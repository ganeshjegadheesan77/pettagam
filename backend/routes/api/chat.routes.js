const collabController = require("../../controllers/chatSystem/collabChatMessages.controller");

module.exports = (router) => {
    //-- get All audit --//
    router.post("/chat/generateTokenForAuditee", collabController.generateTokenForAuditee);

    router.post("/chat/checkAuditeeToken", collabController.checkAuditeeToken);
    router.post("/chat/closeAuditeeSession", collabController.closeAuditeeSession);
    
}