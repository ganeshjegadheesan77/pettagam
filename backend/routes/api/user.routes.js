const UserController = require("../../controllers/users/user.controller");
const { identification } = require("../../middleware");

module.exports = router => {
  // create user
  router.post("/user", UserController.create);

  // login
  router.post("/user/login", UserController.login);

  router.post("/user/delete", UserController.delete);
  router.get("/user/getAllUsers", UserController.getAllUsers);
};
