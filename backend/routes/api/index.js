const { Router } = require('express');

const setupPostRoutes = require('./posts.routes');
const setupUserRoutes = require('./user.routes');

const setupRiskFormsRoutes = require('./riskforms.routes');
const setupDigiLockerFormsRoutes = require('./sdboxforms.routes');
const setupActionRoutes = require('./action.routes');
const setupIssueRoutes = require('./issues.routes');
const setupControlFormsRoutes = require('./controlforms.routes');
const setupMasters = require('./master.routes');
const setupControlTestingRoutes = require('./controlTesting.routes');
const setupRiskUniverseRoutes = require('./universe.routes');
const setupAuditRoutes = require('./audit.routes');
const setUpIntrayRoutes = require('./intray.route');
const setupChatRoutes = require('./chat.routes');

//Sagar changes
const setupInterAuditRoutes = require('./internalAudit.routes');
const setupFieldWorkRoutes = require('./fieldwork.routes');
const setupARARoutes = require('./ara.routes');
const setupDmsRoutes = require('./dms.routes');

const apiRouter = new Router();

//
// setupPostRoutes(apiRouter);
setupUserRoutes(apiRouter);
setupRiskFormsRoutes(apiRouter);
setupDigiLockerFormsRoutes(apiRouter);
setupActionRoutes(apiRouter);
setupIssueRoutes(apiRouter);
setupControlFormsRoutes(apiRouter);
setupMasters(apiRouter);
setupControlTestingRoutes(apiRouter)
setupRiskUniverseRoutes(apiRouter);
setupAuditRoutes(apiRouter)
setupInterAuditRoutes(apiRouter)
setupFieldWorkRoutes(apiRouter)
setupARARoutes(apiRouter);
setupDmsRoutes(apiRouter);
setUpIntrayRoutes(apiRouter);
setupChatRoutes(apiRouter);

module.exports = apiRouter;




