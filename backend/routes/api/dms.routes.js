const dmsController = require('../../controllers/dms/dms.controller')
const uploadFieldWork = require('../../controllers/dms/dmsUpload');
const { dms } = require('../../config/config');


module.exports = (router) => {
    //Basic Operation
    router.post('/dms/dmsConnect',dmsController.dmsConnect);
    router.post('/dms/dmsDisConnect',dmsController.dmsDisConnect);
    
    router.post('/dms/checkDmsSession' , dmsController.checkDmsSession);
    //generate DMS session for auditee
    router.post('/dms/generateDmsSessionForAuditee' , dmsController.generateDmsSessionForAuditee);
    router.post('/dms/generateDmsSessionForAuditor' , dmsController.generateDmsSessionForAuditor);
    
    //Operations
    //upload document
    router.post('/dms/upload',uploadFieldWork.array('file'),dmsController.upload);
    //Read document
    router.post('/dms/retrieveDocument',dmsController.retrieveDocument);
    //checkout document/ edit start
    router.post('/dms/checkOutDocument',dmsController.checkOut);
    //replace docuement / upload edited document again
    router.post('/dms/replaceDocuments',uploadFieldWork.array('file'),dmsController.replaceDocuments);
    //check in / after edit save(update) and available for edit others.
    router.post('/dms/checkInDocument',dmsController.checkIn);
    //get all doc//
    router.get('/dms/getAll',dmsController.getAll);

    //Check file already exist for audit
    router.post('/dms/checkFileExist' , dmsController.checkFileExist);

    //check in/out history
    router.post('/dms/checkInOutHistory',dmsController.checkInOutHistory);
     //check contentSearch
     router.post('/dms/contentSearch',dmsController.contentSearch);
     //Rename file
     router.post('/dms/renameFile' , dmsController.renameFile);
     //Delete File
     router.post('/dms/deleteDocument' , dmsController.deleteDocument);

     router.post('/dms/fileSearch', dmsController.fileSearch);
     router.post('/dms/globalSearch', dmsController.globalSearch);

    router.post('/dms/saveAsDocument', dmsController.saveAsDocument);
     

     router.post('/dms/uploadVideoUrl',dmsController.uploadVideoUrl);

    
}