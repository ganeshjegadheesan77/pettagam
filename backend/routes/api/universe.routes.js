const UniverseController = require("../../controllers/auditUniverse/auditUniverse.controller");

const auditController = require("../../controllers/auditUniverse/createaudit.controller");

module.exports = (router) => {
  //------------------Audit Universe Data -------------------//

  router.get(
    "/auditUniverse/getUniverseData",
    UniverseController.getuniverseData
  );
  router.post(
    "/auditUniverse/postuniverseData",
    UniverseController.postuniverseData
  );
  router.get(
    "/auditUniverse/getAllTableDataByRiskLevel",
    UniverseController.getAllTableDataByRiskLevel
  );
  router.post(
    "/auditUniverse/setCurrentYearAuditValue",
    UniverseController.setCurrentYearAuditValue
  );
  router.post("/auditUniverse/getTableData", UniverseController.getTableData);

  router.get("/audit/getAllCreateAudit", auditController.getAllCreateAudit);

  router.post("/auditUniverse/getGalaxyData", UniverseController.getGalaxyData);
  
};
