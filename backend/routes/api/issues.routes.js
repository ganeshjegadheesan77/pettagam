const issueController = require("../../controllers/IssueActions/issues.controller");
const issuesController = require("../../controllers/IssueActions/issueactionmasters.controll");

module.exports = (router) => {
  //-- get All issues --//
  router.get("/issue/getAllIssuesRecords", issueController.getAllIssuesRecords);
  //
  router.post("/issue/removeActionFromIssue", issueController.removeActionFromIssue);
  //-- get No. Of issues --//
  router.get("/issue/getNumberofIssues", issueController.getNumberofIssues);

  //-- get One. Issue --//
  router.post("/issues/getOneIssues", issueController.getOneIssues);

  //-- GET STATUS MASTER--//
  router.get("/issues/issueStatusMaster", issuesController.issueStatusMaster);

  //-- GET TYPE MASTER--//
  router.get("/issues/issueTypeMaster", issuesController.issueTypeMaster);

  router.post("/issues/getActionsIn", issueController.getActionsIn);

  //-- GET PRIORITY MASTER--//
  router.get(
    "/issues/issuePriorityMaster",
    issuesController.issuePriorityMaster
  );

  //-- GET ACTION  IN ISSUES--//
  router.post("/issues/getActions", issueController.getActions);
  //-- GET CONTROLS DATA ASSOCIATED TO ISSUE
  router.post("/issues/getControls",issueController.getAccociatedControl);
  //-- GET ACTION  IN ISSUES--//
  router.post("/issues/updateOneIssue", issueController.updateOneIssue);
  //--  UPDATE ISSUE WITH ACTIONS
  router.post("/issues/updatesIssueAndAction", issueController.updateIssueAndAction);

  router.post("/issues/saveIssue", issueController.saveIssue);
  //-- GET MULTIPLE ISSUES BY ID---//
  router.post("/issues/getMultipleIssues", issueController.getMultipleIssues);
};
