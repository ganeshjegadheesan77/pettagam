const epController = require('./../../controllers/enia/engagementPlanning/engagementPlanning.controller');

module.exports = (router) => {

  //TESTING PURPOSE API'S
  router.post('/ep/test', epController.test);
  router.post('/ep/createAudit', epController.createAudit);

  //ASHRAF API
  //for fetching audit team and pdb contact details
  router.get('/ep/auditTeamData', epController.getAuditTeam);
  //for fetching audit progress bar configuration
  router.get('/ep/getProgressBarConfiguration', epController.getProgressBarConfig);
  //for fetching current year active audit
  router.get('/ep/getCurrentYearAuditList', epController.getCurrentYearAuditList);
  //get selected audit info
  router.get('/ep/getSelectedAuditInfo', epController.getSelectedAuditInfo);


  //USED API'S
  //team and timeline
  router.post('/ep/getTeamAndTimeLine', epController.getTeamAndTimeLine);
  //planning and memorandum
  router.post('/ep/getMemorandumData', epController.getMemorandumData);
  //Announcement data
  router.post('/ep/getAnnouncementData', epController.getAnnouncementData);
  //Opening meeting data
  router.post('/ep/getOpenMeetUpData' , epController.getOpenMeetUpData);
  //Closing meeting data
  router.post('/ep/getCloseMeetData' , epController.getCloseMeetData);

  //---------------------------------------------------
  //update team and timeline
  router.post('/ep/saveTeamAndTimeLine' , epController.saveTeamAndTimeLine);
  //update memorandum
  router.post('/ep/saveMemorandumData' , epController.saveMemorandumData);
  //update opening meeting
  router.post('/ep/saveOpenMeetUpData' , epController.saveOpenMeetUpData);
  //update announcement
  router.post('/ep/saveAnnouncementData' , epController.saveAnnouncementData);
  //update closing meeting
  router.post('/ep/saveCloseMeetData' , epController.saveCloseMeetData);

  
}