const valueMasterController = require("../../controllers/common/valueMaster.controller");
// const valueMastersController = require("../../controllers/common/valueMaster.controller");
module.exports = router => {
    // create user
    router.get("/au/getAuditMasters", valueMasterController.getAuditMasters);
    router.post("/comman/getValueSetMaster", valueMasterController.getValueSetMaster);
};