const auditMasterController = require("../../controllers/enia/auditUniverse/auditUniverse.controller");
const auditUniverseController = require("../../controllers/enia/auditUniverse/auditUniverse.controller");
// const { identification } = require("../../middleware");
module.exports = router => {
  // audit CRUD
  router.get("/au/getAudit/:auditId", auditUniverseController.getAudit);
  router.post("/au/createAudit", auditUniverseController.createAudit);
  router.put("/au/editAudit", auditUniverseController.editAudit);
  router.delete("/au/deleteAudit/:auditId", auditUniverseController.deleteAudit);

  router.put("/au/updateStartDate", auditUniverseController.updateStartDate);
  router.put("/au/updateConfirmToggle", auditUniverseController.updateConfirmToggle);

  router.post("/au/addToCurrentYear", auditUniverseController.addToCurrentYear);
  router.post("/au/duplicateAudit", auditUniverseController.duplicateAudit);

  router.post("/au/getTableData", auditUniverseController.getTableData);

  router.post("/au/getGalaxyData",auditMasterController.getGalaxyData);
};