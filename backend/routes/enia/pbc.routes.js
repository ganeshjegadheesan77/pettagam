const PbcData = require("../../controllers/enia/engagementPlanning/pbc.controller");


module.exports = router => {
    router.post('/pbc/updatePbcDoc', PbcData.updatePbcDoc);

    router.post('/pbc/getPbcData', PbcData.getPbcData);

    router.post('/pbc/updatePbcAttachment', PbcData.updatePbcAttachment);



    router.post('/pbc/updatePbcRemark', PbcData.updatePbcRemark);

    router.post('/pbc/updatePbcPopup', PbcData.updatePbcPopup);


    router.post('/pbc/deletePbcEntry', PbcData.deletePbcEntry);

    router.post('/pbc/deletePbcAttachment', PbcData.deletePbcAttachment);

    router.post('/pbc/updatePbcInfo', PbcData.updatePbcInfo);

    router.post('/pbc/generateTokenForPBC', PbcData.generateTokenForPBC);

    router.post('/pbc/checkPBCToken', PbcData.checkPBCToken);
};