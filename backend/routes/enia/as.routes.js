const { constant } = require("lodash");
const agileAuditSpaceController = require("../../controllers/enia/agileAuditSpace/agileAuditSpace.controller");

module.exports = router => {

  // Pratik START //
  ////////////////////////////////// save Reference Document ////////////////////////////////////

  router.post('/workstep/updateReferenceDocument', agileAuditSpaceController.updateReferenceDocument);

  ////////////////////////////////// save WorkingPaper  /////////////////////////////////////////

  router.post('/workstep/updateWorkingPapers', agileAuditSpaceController.updateWorkingPapers);

  ////////////////////////////////// Delete Referece Document  ////////////////////////////////////

  router.post('/workstep/deleteReferenceDocument', agileAuditSpaceController.deleteReferenceDocument);

  ////////////////////////////////// Delete WorkingPaper //////////////////////////////////////////

  router.post('/workstep/deleteWorkingPaper', agileAuditSpaceController.deleteWorkingPaper);






  // Pratik END //



  // Sagar START //
  router.post('/as/getPreparerList', agileAuditSpaceController.getPreparerList);
  router.post('/as/getViewData', agileAuditSpaceController.getViewData);
  router.post('/as/applyAndOverwriteTemplates', agileAuditSpaceController.applyAndOverwriteTemplates);
  router.post('/as/getAllIssuesOfAudit' , agileAuditSpaceController.getAllIssuesOfAudit);
  router.post('/as/getAllIssuesOfWorkStep' , agileAuditSpaceController.getAllIssuesOfWorkStep);
  // Sagar END //



  // Yash START //
  router.get('/as/getWorkStep/:workStepId', agileAuditSpaceController.getWorkStep);
  router.post('/as/createWorkStep', agileAuditSpaceController.createWorkStep);
  router.put('/as/updateWorkStep', agileAuditSpaceController.updateWorkStep);
  router.delete('/as/deleteWorkStep/:workStepId', agileAuditSpaceController.deleteWorkStep);
  router.get('/as/getIssue/:issueId', agileAuditSpaceController.getIssue);
  router.post('/as/createIssue', agileAuditSpaceController.createIssue);
  router.put('/as/updateIssue', agileAuditSpaceController.updateIssue);
  // Yash END //


  // Gopal START // api/enia/as/getAudits
  router.get("/as/getAudits", agileAuditSpaceController.getAudits);
  router.post("/as/getAuditsTeam", agileAuditSpaceController.getAuditsTeam);

  // Gopal END //


  // Ashraf START //
  router.get("/as/getTemplateList", agileAuditSpaceController.getTemplateList);
  router.delete("/as/deleteIssue/:issueId", agileAuditSpaceController.deleteIssue);
  // Ashraf END //

    //Tarkesh start //
  
    router.get("/as/getAuditIssueList", agileAuditSpaceController.getAuditIssueList);
    router.get("/as/getworkstepIssueList", agileAuditSpaceController.getworkstepIssueList);
    //Tarkesh End //

};