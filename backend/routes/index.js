const apiRouter = require('./api');
const eniaRouter = require('./enia');
const jwtMiddleware = require("./../middleware/auth/identification");
module.exports = app => {
    app.use('/api', jwtMiddleware('header'), apiRouter);
    app.use('/api/enia', jwtMiddleware('header'), eniaRouter);
};