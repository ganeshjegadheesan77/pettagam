const bodyParser = require('body-parser');
// const identification = require('./auth/identification');
const extractor = require('./resources/resourceExtractor');

const prefixMiddleware = app => {
    app.use(bodyParser.json());
};

module.exports = {
    prefixMiddleware,
    // identification,
    extractor
};