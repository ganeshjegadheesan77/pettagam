const verifyJWT = require('../../utils/verifyJWT');
const User = require("../../models/users");
const config = require("../../config/config");
const rsHelper = require("../../utils/responseHelper");

const privateKey = config.key.privateKey;

const auth = (type) => {
    return async (req, res, next) => {
        try {
            if (req.headers && req.headers.authorization) {
                let token = req.headers.authorization;
                if (type === 'header') {
                    const header = req.headers['authorization'];
                    token = header;
                }

                if (token && token.length) {
                    const decoded = await verifyJWT(token, privateKey);
                    req.user = await User.findOne({ username: decoded.username, cuid: decoded.id });
                }
                next();
                return;
            }
            else if(req.path.includes('login')  || req.path.includes('dms') ||req.path.includes('uploads') || req.path.includes('pbc'))
            {
                next();
                return;
            }
            else {
                rsHelper.manage(res,null,null,401,"JWT Token Not Found");
                return;
            }
        } catch (e) {
            rsHelper.manage(res,e,null,401,"JWT Token Not Found / Expired");
            return;
        }
    };
};

module.exports = auth;