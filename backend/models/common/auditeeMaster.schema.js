const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const auditeeMaster = new Schema(
    {
        name: {
            type: String,
            required: true
        },

        company: {
            type: "String"
        },

        role: {
            type: "String",
            default: "auditee"
        },

        city: {
            type: "String"
        },

        email: {
            type: String,
            unique: true,
            required: true
        },
        businessProcess: {
            type: String,
        },
        auditableEntity: {
            type: String,
        },
        image : String

    },
    { collection: "auditeeMaster", timestamps: true }
);

auditeeMaster.options.toJSON = {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        return ret;
    }
};

const AuditeeMaster = mongoose.model("auditeeMaster", auditeeMaster);

module.exports = AuditeeMaster;
