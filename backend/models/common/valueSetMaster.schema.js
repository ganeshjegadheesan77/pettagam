const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const valueSetMasterSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    code: {
        type: String,
        unique: true,
        required: true
    },
    valueSet: [{
        name: String,
        code: String,
        parentValueCode : String,
        isDeleted: {
            type: Boolean,
            default: false
        },
        
    }],
    parentCode: String,
    description: String,
    active: {
        type : Boolean,
        default : true
    },
    maintainBy: {
        type : String,
        enum : ['system','admin'],
        default : 'system'

    }
    
},
    {
        timestamps: true,
        collection: "valueSetMaster",
        minimize: false, versionKey: false
    },
);

const ValueSetMaster = mongoose.model("valueSetMaster", valueSetMasterSchema);
module.exports = ValueSetMaster;

