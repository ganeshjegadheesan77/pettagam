const mongoose = require('mongoose')
excel_data = mongoose.model('excel_data',
{
        Postcode:{type:Number},
        Sales_Rep_ID:{type:Number},
        Sales_Rep_Name: {type:String},
        Year:{type:Number},
        Value: {type:Number},
});

module.exports ={excel_data}