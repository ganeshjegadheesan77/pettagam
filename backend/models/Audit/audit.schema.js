const mongoose = require('mongoose');
const { Schema } = mongoose;


var auditInfo = Schema({
    auditId: {
        type: String
    },
    audits: {
        type: String
    },
    scope: {
        type: String
    },
    auditLead: {
        type: String
    },

    startDate: {
        type: String
    },
    department: {
        type: String
    },
    site: {
        type: String
    },
    location: {
        type: String
    },

    riskProgress:{
        type:String
    },
    color:{
        type:String
    },
    rpId:{
        type: String
    },
    oldAuditId:[{ type: Schema.Types.ObjectId, ref: 'audit_master' }]
    // audit_plan_memo:[{ type: Schema.Types.ObjectId, ref: 'audit_plan_memo' }],
    // audit_opening_meeting:[{ type: Schema.Types.ObjectId, ref: 'audit_opening_meeting' }],
    // audit_announce:[{ type: Schema.Types.ObjectId, ref: 'audit_announce' }],
    // audit_field_work:[{ type: Schema.Types.ObjectId, ref: 'audit_field_work' }],
    // audit_draft_report:[{ type: Schema.Types.ObjectId, ref: 'audit_draft_report' }],
    // audit_PBC:[{ type: Schema.Types.ObjectId, ref: 'audit_PBC' }],
    // audit_issues:[{ type: Schema.Types.ObjectId, ref: 'audit_issues' }],
    // audit_closing_meeting:[{ type: Schema.Types.ObjectId, ref: 'audit_closing_meeting' }]
    })

    var rpInfo = Schema({
        budget:{
            type: Number
        },
        planning:{
            type: Number
        },
        fieldWork:{
            type: Number
        },
        reporting:{
            type: Number
        },
        rpId:{
            type: String
        },
        pAssignee:[String],
        papAssignee:[String],
        fAssignee:[String],
         fapAssignee:[String],
        rAssignee:[String],
        rapAssignee:[String],
        execution:{
            type: Number
        },
        approval:{
            type: Number
        },
        exStart: {
            type: String
        },
        exEnd: {
            type: String
        },
        exDuration:{
            type: Number
        },
        apStart: {
            type: String
        },
        apEnd: {
            type: String
        },
        apDuration:{
            type:Number
        },
        fexecution:{
            type: Number
        },
        fapproval:{
            type: Number
        },
        fexStart: {
            type: String
        },
        fexEnd: {
            type: String
        },
        fexDuration:{
            type: Number
        },
        fapStart: {
            type: String
        },
        fapEnd: {
            type: String
        },
        rexecution:{
            type: Number
        },
        rapproval:{
            type: Number
        },
        rexStart: {
            type: String
        },
        rexEnd: {
            type: String
        },
        rexDuration:{
            type:Number
        },
        rapStart: {
            type: String
        },
        rapEnd: {
            type: String
        },
        auditId: {
            type: String
        },
        fapDuration:{
            type:Number
        },
        rapDuration:{
            type:Number
        },
        submittedOn:{
            type:String
        },
        lastChangeOn:{
            type:String
        },
        auditTeam:[String],
        auditOwner:{
            type:String
        },
        auditOwnerId:{
            type:String
        },
        auditSubmitter:{
            type:String
        },
        auditSubId:{
            type:String
        },
        auditApprover:{
            type:String
        },
        auditAppId:{
            type:String
        },
        auditLead:{
            type:String
        },
        overallImpact:{
            type:Number
        }
        
    })
exports.audits = mongoose.model('auditsdatas', auditInfo);
exports.rps = mongoose.model('resourceplannings', rpInfo);