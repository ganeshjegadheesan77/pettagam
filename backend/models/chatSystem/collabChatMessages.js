var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var CollabChatMessagesSchema = new Schema({
  roomId: { type: String },
  messages: [
    {
      username: String,
      createdAt: Date,
      message: String,
      masterRoom: Boolean,
      newJoined: {
        type: Boolean,
        default: false,
      },
      usertype: {
        type: String,
        enum: ["host", "participant"],
        default: "host",
      },
    },
  ],
});

var CollabChatMessages = mongoose.model(
  "CollabChatMessages",
  CollabChatMessagesSchema
);
module.exports = CollabChatMessages;
