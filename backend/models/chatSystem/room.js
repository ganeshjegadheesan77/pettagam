var mongoose = require("mongoose");

var Rooms = mongoose.Schema({
  usernames: [{ type: String }],
  fileName: [{ name: String, isOpen: Boolean }],
  title: String,
  createdAt: Date,
  owner: { type: String },
  auditId:{type:String}
});

module.exports = mongoose.model("Room", Rooms);


// owner: { type: String },
// usersName: [{ type: String }],
// title: { type: String }