var mongoose = require('mongoose');

const User = mongoose.Schema({
	username: String,
	online: Boolean,
});



module.exports = mongoose.model('User', User);