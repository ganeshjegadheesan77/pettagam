const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OnlineUser = new Schema({
  roomId: { type: String },
  username: { type: String },
  status: { type: Boolean },
});

const OnlineUsers = mongoose.model("OnlineUsers", OnlineUser);
module.exports = OnlineUsers;
