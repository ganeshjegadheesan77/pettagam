var mongoose = require('mongoose');

let lastIdInfo = mongoose.Schema(
    {
      forName: {
        type: String,
      },
      prefix: {
        type: String,
      },
      lastId: {
        type: String,
      }
    },
    { collection: "lastIdTracker", timestamps: true }
  );
  exports.lastIdInfo = mongoose.model(
    "lastIdTracker",
    lastIdInfo
  );