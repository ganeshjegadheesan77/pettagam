var mongoose = require('mongoose');

var Message = mongoose.Schema({
    message: {type: String},
    username: String,
	createdAt: Date,
	groupId: { type: String }
});

module.exports = mongoose.model('Message', Message);