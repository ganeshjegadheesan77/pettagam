var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var CollabChatSystemSchema = new Schema({
  roomId: { type: String },
  title: { type: String },
  createdAt: { type: Date },
  participants: [
    {
      username: { type: String },
      invited_on: { type: Date },
      invite_token: { type: String },
      intrayDocuments: [
        {
          fileName: { type: String },
          room: { type: String }
        }
      ],
    },
  ],
  hosts: [
    {
      username: { type: String },
      status: { type: Boolean },
      intrayDocuments: [
        {
          fileName: { type: String },
          room: { type: String }
        }
      ],
    },
  ],
  auditId: { type: String },
  sessionStartedOn: { type: Date },
  sessionEndedOn: { type: Date },
});

var CollabChatSystem = mongoose.model(
  "CollabChatSystem",
  CollabChatSystemSchema
);
module.exports = CollabChatSystem;
