const mongoose = require("mongoose");
const { Schema } = mongoose;

var audit_ara_info =Schema(
  {
    auditId:{type:String , ref:'audit_master'},
    araId:{type:String , ref:'universes'},
    leadAuditor:{type:String},
    impact_financialRisk: { type: String },
    impact_complianceRisk: { type: String },
    impact_overall: { type: String },
    likelihood_processRisk: { type: String },
    likelihood_internalControls: { type: String },
    likelihood_overall: { type: String },
    overallRiskScore: { type: String },
    riskAssessmentSubmitter: { type: String },
    riskAssessmentSubmitID: { type: String },
    riskAssessmentModifiedBy: { type: String },
    riskAssessmentApprovedBy: { type: String },
    riskAssessmentIdentifiedOn: { type: String },
    riskAssessmentApprovedOn: { type: String },
    riskAssessedOn: { type: String },
    riskModifiedOn: { type: String },

  },
   { collection: "audit_ara_master", timestamps: true }

);
exports.araMaster = mongoose.model("audit_ara_master",audit_ara_info);

// audit_risk_assessment_info = Schema(
//   {
//     auditId: { type: String },
//     riskLevel: { type: String },
//     auditName: { type: String },
//     auditLocation: { type: String },
//     issueIds: [{ type: String }],
//     riskAssessmentId: { type: String },
//     leadAuditor: { type: String },
//     rotation: { type: String },
//     nextAuditYear: { type: String },
//     impact_financialRisk: { type: String },
//     impact_complianceRisk: { type: String },
//     impact_overall: { type: String },
//     likelihood_processRisk: { type: String },
//     likelihood_internalControls: { type: String },
//     likelihood_overall: { type: String },
//     overallRiskScore: { type: String },
//     riskAssessmentSubmitter: { type: String },
//     riskAssessmentSubmitID: { type: String },
//     riskAssessmentModifiedBy: { type: String },
//     riskAssessmentApprovedBy: { type: String },
//     riskAssessmentIdentifiedOn: { type: String },
//     riskAssessmentApprovedOn: { type: String },
//     riskAssessedOn: { type: String },
//     riskModifiedOn: { type: String },
//   },
//   { collection: "audit_risk_assessment_master", timestamps: true }
// );

// exports.araForm = mongoose.model(
//   "audit_risk_assessment_master",
//   audit_risk_assessment_info
// );
