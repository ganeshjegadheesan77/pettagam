const mongoose = require("mongoose");
const { Schema } = mongoose;

subProcessRiskInfo = Schema(
  {
    sub_id: {
      type: String,
    },
    subProcess_name: {
      type: String,
    },
    risks: [
      {
        sRiskId: { type: String },
        sRiskName: { type: String },
      },
    ],
  },
  { collection: "master_subProcessRisks", timestamps: true }
);
exports.subProcessRiskInfo = mongoose.model(
  "master_subProcessRisks",
  subProcessRiskInfo
);

lastIdInfo = Schema(
  {
    forName: {
      type: String,
    },
    prefix: {
      type: String,
    },
    lastId: {
      type: String,
    },
  },
  { collection: "unique_id_creation_collection", timestamps: true }
);
exports.lastIdCollection = mongoose.model(
  "unique_id_creation_collection",
  lastIdInfo
);

masterDivisionInfo = Schema(
  {
    id: {
      type: String,
    },
    divisionName: {
      type: String,
    },
  },
  { collection: "master_division", timestamps: true }
);
exports.master_division = mongoose.model("master_division", masterDivisionInfo);

masterLeadContactInfo = Schema(
  {
    id: {
      type: String,
    },
    leadName: {
      type: String,
    },
  },
  { collection: "master_leadContact", timestamps: true }
);
exports.masterLeadContactInfo = mongoose.model(
  "master_leadContact",
  masterLeadContactInfo
);

masterProcessInfo = Schema(
  {
    id: {
      type: String,
    },
    processName: {
      type: String,
    },
  },
  { collection: "master_process", timestamps: true }
);
exports.masterProcessInfo = mongoose.model("master_process", masterProcessInfo);

masterDepartmentInfo = Schema(
  {
    id: {
      type: String,
    },
    departmentName: {
      type: String,
    },
  },
  { collection: "master_department", timestamps: true }
);
exports.masterDepartmentInfo = mongoose.model(
  "master_department",
  masterDepartmentInfo
);

//---------------------------------------------------------//

var auditMasterInfo = Schema(
  {
    auditId: { type: String, ref: "universes" },
    uID: { type: String, ref: "universes" },
    parentUId: { type: String, ref: "universes" },
    year: { type: String },
    auditableEntity: { type: String, ref: "universes" },
    auditComplete: { type: Boolean },
    auditName: { type: String },
    audit_engagement_plan: { type: String, default: null },
    audit_program: { type: String, default: null },
    rotation: { type: String },
    isNewAudit: { type: Boolean, default: true },
    duplicateAuditFlag: { type: Boolean, default: false },
    isCompleted:{type:Boolean , default:false},
    auditProgress:{type:Number , default:0},
    scope : String,
    auditFieldWork: [
      {
        fw_id: { type: Number },
        auditFw_id: { type: String, ref: "audit_field_work" },
        afw_name: { type: String },
        status: {
          type: String,
          default: "assigned",
        },
        assigneeStatus: {
          type: String,
          default: "assigned"
        },
        approverStatus: {
          type: String,
        }
      },
      { default: null },
    ],
    auditPbc: [
      {
        pbc_id: { type: Number, ref: "audit_PBC" },
        auditPBC_id: { type: String },
        pbc_name: { type: String },
        status: {
          type: String,
          default: "assigned",
        },
        assigneeStatus: {
          type: String,
          default: "assigned"
        },
        approverStatus: {
          type: String,
        }
      },
    ],
    issueIds: [{ type: String, ref: "issues" }],
    periodInScope: [{ type: String }],
    performancePeriod: [{ type: String }],
    updateMeetingDate: { type: String,  },
    openingMeetingDate: { type: String, },
    closingMeetingDate: { type: String, },
    auditOwner: { type: String },
    auditOwnerId: { type: String },
    auditSubmitter: { type: String },
    auditSubmitterId: { type: String },
    auditApprovedBy: { type: String },
    auditApproverId: { type: String },
    rpId: { type: String , ref:'resourceplannings' },
    openingMeeting: {
      OMisSet: { type: Boolean },
      aom_id: {
        type: String,
      },
      aom_name: {
        type: String,
      },
      aom_scope: {
        type: String,
      },
      aom_agenda: {
        type: String,
      },
      aom_qa: {
        type: String,
      },
      aom_audit_team: [
        {
          type: String,
        },
      ],
      aom_interview_schedule: [String],
      aom_created_by: {
        type: String,
      },
      aom_updated_by: {
        type: String,
      },
      status: {
        type: String,
        default: "assigned",
      },
      assigneeStatus: {
        type: String,
        default: "assigned"
      },
      approverStatus: {
        type: String,
      },
      dup_aom_Flag: { type: Boolean, default: false },
    },
    planningMemorandum: {
      PMisSet: { type: Boolean },
      apm_id: {
        type: String,
      },
      apm_name: {
        type: String,
      },
      apm_division: {
        type: String,
      },
      apm_audit_scope: {
        type: String,
      },
      apm_audit_objective: {
        type: String,
      },
      apm_lead_contact: {
        type: String,
      },
      apm_dept: {
        type: String,
      },
      apm_process: {
        type: String,
      },
      apm_audit_team: [
        {
          type: String,
        },
      ],
      apm_out_of_scope: {
        type: String,
      },
      apm_attachments: [
        {
          type: String,
        },
      ],
      apm_created_by: {
        type: String,
      },
      apm_updated_by: {
        type: String,
      },
      apm_updateCount: {
        type: Number,
      },
      status: {
        type: String,
        default: "assigned",
      },
      assigneeStatus: {
        type: String,
        default: "assigned"
      },
      approverStatus: {
        type: String,
      },
      dup_apm_Flag: { type: Boolean, default: false },
    },
    announcement: {
      AAisSet: { type: Boolean },
      aa_id: {
        type: String,
      },
      aa_name: {
        type: String,
      },
      aa_text: {
        type: String,
      },
      aa_agenda: {
        type: String,
      },
      aa_text2: {
        type: String,
      },
      aa_closing: {
        type: String,
      },
      aa_attachments: [
        {
          type: String,
        },
      ],
      aa_created_by: {
        type: String,
      },
      aa_updated_by: {
        type: String,
      },
      status: {
        type: String,
        default: "assigned",
      },
      assigneeStatus: {
        type: String,
        default: "assigned"
      },
      approverStatus: {
        type: String,
      },
      dup_aa_Flag: { type: Boolean, default: false },
    },
    draftReport: {
      DRisSet: { type: Boolean },
      adr_id: {
        type: String,
      },
      adr_name: {
        type: String,
      },
      adr_division: {
        type: String,
      },
      adr_gen_observation: {
        type: String,
      },
      adr_audit_objective: {
        type: String,
      },
      adr_lead_contact: {
        type: String,
      },
      adr_mgmt_feedback: {
        type: String,
      },
      adr_rating: {
        type: String,
        default: null,
      },
      adr_department: {
        type: String,
      },
      adr_process: {
        type: String,
      },
      adr_corrective_action: {
        type: String,
      },
      adr_audit_team: [
        {
          type: String,
        },
      ],
      adr_comments: {
        type: String,
      },
      adr_attachments: [
        {
          type: String,
        },
      ],
      adr_created_by: {
        type: String,
      },
      adr_updated_by: {
        type: String,
      },
      status: {
        type: String,
        default: "assigned",
      },
      assigneeStatus: {
        type: String,
        default: "assigned"
      },
      approverStatus: {
        type: String,
      },
      dup_adr_Flag: { type: Boolean, default: false },
    },
    closingMeeting: {
      CMisSet: { type: Boolean },
      acm_id: {
        type: String,
      },
      acm_name: {
        type: String,
      },
      acm_agenda: {
        type: String,
      },
      acm_issue_action_review: {
        type: String,
      },
      acm_qa: {
        type: String,
      },
      acm_audit_team: [
        {
          type: String,
        },
      ],
      acm_comment: {
        type: String,
      },
      acm_attachments: [
        {
          type: String,
        },
      ],
      acm_created_by: {
        type: String,
      },
      acm_updated_by: {
        type: String,
      },
      status: {
        type: String,
        default: "assigned",
      },
      assigneeStatus: {
        type: String,
        default: "assigned"
      },
      approverStatus: {
        type: String,
      },
      dup_acm_Flag: { type: Boolean, default: false },
    },
  },
  { collection: "audit_master", timestamps: true }
);
exports.auditMasters = mongoose.model("audit_master", auditMasterInfo);

var audit_plan_memo_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    apm_id: {
      type: String,
    },
    apm_name: {
      type: String,
    },
    apm_division: {
      type: String,
    },
    apm_audit_scope: {
      type: String,
    },
    apm_audit_objective: {
      type: String,
    },
    apm_lead_contact: {
      type: String,
    },
    apm_dept: {
      type: String,
    },
    apm_process: {
      type: String,
    },
    apm_audit_team: [
      {
        type: String,
      },
    ],
    apm_out_of_scope: {
      type: String,
    },
    apm_attachments: [
      {
        type: String,
      },
    ],
    apm_created_by: {
      type: String,
    },
    apm_updated_by: {
      type: String,
    },
    apm_updateCount: {
      type: Number,
    },
  },
  { collection: "audit_plan_memo", timestamps: true }
);

exports.audit_plan_memos = mongoose.model(
  "audit_plan_memo",
  audit_plan_memo_info
);

var audit_opening_meeting_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    aom_id: {
      type: String,
    },
    aom_name: {
      type: String,
    },
    aom_scope: {
      type: String,
    },
    aom_agenda: {
      type: String,
    },
    aom_qa: {
      type: String,
    },
    aom_audit_team: [
      {
        type: String,
      },
    ],
    aom_interview_schedule: [String],
    aom_created_by: {
      type: String,
    },
    aom_updated_by: {
      type: String,
    },
  },
  { collection: "audit_opening_meeting", timestamps: true }
);

exports.audit_opening_meeting = mongoose.model(
  "audit_opening_meeting",
  audit_opening_meeting_info
);

var audit_announce_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    aa_id: {
      type: String,
    },
    aa_name: {
      type: String,
    },
    aa_text: {
      type: String,
    },
    aa_agenda: {
      type: String,
    },
    aa_text2: {
      type: String,
    },
    aa_closing: {
      type: String,
    },
    aa_attachments: [
      {
        type: String,
      },
    ],
    aa_created_by: {
      type: String,
    },
    aa_updated_by: {
      type: String,
    },
  },
  { collection: "audit_announce", timestamps: true }
);

exports.audit_announce = mongoose.model("audit_announce", audit_announce_info);

var audit_field_work_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    afw_audit_pgm_id: {
      type: String,
    },
    afw_name: {
      type: String,
    },
    afw_id: {
      type: String,
    },
    afw_subprocess: { type: String },
    afw_risk: { type: String },
    auditFw_id: { type: String, ref: "audit_master" },
    afw_tester: {
      type: String,
    },
    afw_reviewer: {
      type: String,
    },
    afw_signoff_period: {
      type: String,
    },
    afw_audit_pgm_title: {
      type: String,
    },
    afw_test_procedure: {
      type: String,
    },
    afw_results: {
      type: String,
    },
    afw_status: {
      type: String,
    },
    afw_attachments: [
      {
        type: String,
      },
    ],
    afw_created_by: {
      type: String,
    },
    afw_updated_by: {
      type: String,
    },
  },
  { collection: "audit_field_work", timestamps: true }
);

exports.audit_field_work = mongoose.model(
  "audit_field_work",
  audit_field_work_info
);

//audit_draft_report
var audit_draft_report_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    adr_id: {
      type: String,
    },
    adr_name: {
      type: String,
    },
    adr_division: {
      type: String,
    },
    adr_gen_observation: {
      type: String,
    },
    adr_audit_objective: {
      type: String,
    },
    adr_lead_contact: {
      type: String,
    },
    adr_mgmt_feedback: {
      type: String,
    },
    adr_rating: {
      type: String,
    },
    adr_department: {
      type: String,
    },
    adr_process: {
      type: String,
    },
    adr_corrective_action: {
      type: String,
    },
    adr_audit_team: [
      {
        type: String,
      },
    ],
    adr_comments: {
      type: String,
    },
    adr_attachments: [
      {
        type: String,
      },
    ],
    adr_created_by: {
      type: String,
    },
    adr_updated_by: {
      type: String,
    },
  },
  { collection: "audit_draft_report", timestamps: true }
);

exports.audit_draft_report = mongoose.model(
  "audit_draft_report",
  audit_draft_report_info
);

//audit_PBC
var audit_PBC_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "audit_master",
    },
    apbc_id: {
      type: String,
    },
    auditPbc_id: {
      type: String,
    },
    apbc_name: {
      type: String,
    },
    apbc_audit_pgm_id: {
      type: String,
    },
    apbc_preparer: {
      type: String,
    },
    apbc_reviewer: {
      type: String,
    },
    apbc_signoff_period: {
      type: String,
    },
    apbc_audit_pgm_title: {
      type: String,
    },
    apbc_work_step: {
      type: String,
    },
    apbc_status: {
      type: String,
    },
    apbc_document_req: {
      type: String,
    },
    apbc_attachments: [
      {
        type: String,
      },
    ],
    apbc_created_by: {
      type: String,
    },
    apbc_updated_by: {
      type: String,
    },
  },
  { collection: "audit_PBC", timestamps: true }
);

exports.audit_PBC = mongoose.model("audit_PBC", audit_PBC_info);

//audit_issues
var audit_issues_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    ai_id: {
      type: String,
    },
    ai_name: {
      type: String,
    },
    ai_status: {
      type: String,
    },
    ai_descripton: {
      type: String,
    },
    ai_review_comment: {
      type: String,
    },
    ai_recommendation: {
      type: String,
    },
    ai_issue_type: {
      type: String,
    },
    ai_mgmt_response: {
      type: String,
    },
    ai_control: {
      type: String,
    },
    ai_priority: {
      type: String,
    },
    ai_updated_by: {
      type: String,
    },
  },
  { collection: "audit_issues", timestamps: true }
);

exports.audit_issues = mongoose.model("audit_issues", audit_issues_info);

//audit_closing_meeting
var audit_closing_meeting_info = Schema(
  {
    master_audit_id: {
      type: String,
      ref: "auditsdatas",
    },
    acm_id: {
      type: String,
    },
    acm_name: {
      type: String,
    },
    acm_agenda: {
      type: String,
    },
    acm_issue_action_review: {
      type: String,
    },
    acm_qa: {
      type: String,
    },
    acm_audit_team: [
      {
        type: String,
      },
    ],
    acm_comment: {
      type: String,
    },
    acm_attachments: [
      {
        type: String,
      },
    ],
    acm_created_by: {
      type: String,
    },
    acm_updated_by: {
      type: String,
    },
  },
  { collection: "audit_closing_meeting", timestamps: true }
);

exports.audit_closing_meeting = mongoose.model(
  "audit_closing_meeting",
  audit_closing_meeting_info
);

var aom_interviews = Schema(
  {
    isId: {
      type: String,
    },
    day: {
      type: String,
    },
    time: {
      type: String,
    },
    subject: {
      type: String,
    },
    location: {
      type: String,
    },
    pfa: {
      type: String,
    },
    comment: {
      type: String,
    },
  },
  { collection: "aom_Interviews", timestamps: true }
);

exports.aom_interview = mongoose.model("aom_Interviews", aom_interviews);
var countData = Schema(
  {
    count: {
      type: String,
    },
    om: {
      type: Number,
    },
    announced: {
      type: Number,
    },
    memo: {
      type: Number,
    },
  },
  { collection: "countDatas", timestamps: true }
);

exports.countData = mongoose.model("countDatas", countData);
