const mongoose = require("mongoose");
const { Schema } = mongoose;

const dmsSessionTrackerInfo = Schema(
  {
    username: { type: String },
    dmsSessionId: { type: String },
    sessionDate: { type: String },
  },
  { collection: "DmsSessionTracker", timestamps: true }
);
exports.dmsSessionTrackerInfo = mongoose.model(
  "DmsSessionTracker",
  dmsSessionTrackerInfo
);
