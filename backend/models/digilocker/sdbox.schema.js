const mongoose = require("mongoose");
const { Schema } = mongoose;

const sdboxInfo = Schema({
  boxId: {
    type: String,
  },
  type: {
    type: String,
  },
  locationCode: {
    type: String,
  },
  branchCode: {
    type: String,
  },

  customerLink: [
    {
        customerId: {
            type: String,
            required: false
        },
        effectiveFrom: {
            type: Date,
            required: false
        },
        effectiveTill: {
            type: Date,
            required: false
        }
    }
  ],
  createdBy: {
    type: String,
    required: false
  },
  createdOn: {
    type: Date,
    required: false
  },
  updatedBy: {
    type: String,
    required: false
  },
  updatedOn: {
    type: Date,
    required: false
  }},
  { collection: "sdbox" }
);
sdboxInfo.index({ fileName: "text" });
exports.sdbox = mongoose.model("sdbox", sdboxInfo);
