var mongoose = require("mongoose");

var file = mongoose.Schema({
  owner: { type: String },
  fileName: [{ type: String }],
  usersName: [{ type: String }],
  title: { type: String }
});
module.exports = mongoose.model("files", file);
