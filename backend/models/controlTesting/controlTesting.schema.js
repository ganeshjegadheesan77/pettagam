const mongoose = require("mongoose");
const { Schema } = mongoose;

const controlTest = Schema({
  ctlTestingId: {
    type: String,
  },
  ctFrequency: {
    type: String,
  },
  ctName: {
    type: String,
  },
  ctGuidance: {
    type: String,
  },
  ctAutomatic: {
    type: Boolean,
  },
  ctPreventing: {
    type: Boolean,
  },
  tests: {
    type: Boolean,
  },
  ctKeyControl: {
    type: Boolean,
  },
  testResults: {
    type: String,
  },
  steptick: [Number],
  controlRisk: {
    type: Number,
  },
  controlId: [
    {
      type: String,
      ref: "riskcontrols",
    },
  ],
  issueId: [String],
  actionId: [String],
  automaticVal: {
    type: Boolean,
  },
  preventingVal: {
    type: Boolean,
  },
  attachment: [String],
});
exports.ct = mongoose.model("controltestings", controlTest);
