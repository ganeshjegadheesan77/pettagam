const mongoose = require("mongoose");

const universeTableData = mongoose.Schema({
    auditableEntity: {
        type: String,
        require: true
    },
    riskLevel: {
        type: String,
        require: true
    },
    rotation: {
        type: String,
        require: true
    }, 
    lastAudit: {
        type: Date
    }   
    
});
module.exports = mongoose.model('auditUniverseTable', universeTableData);