const mongoose = require("mongoose");
const { Schema } = mongoose;

const createauditInfo = Schema({
  auditId: {
    type: String,
  },

  planning: {
    type: String,
  },

  fieldWork: {
    type: String,
  },
});

exports.audit = mongoose.model("createaudits", createauditInfo);
