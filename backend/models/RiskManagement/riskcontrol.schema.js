const mongoose = require("mongoose");
const { Schema } = mongoose;

const controlInfo = Schema({
  controlId: {
    type: String,
  },

  riskId: [
    {
      type: String,
      ref: "risks",
    },
  ],
  issueId: [
    {
      type: String,
      ref: "issues",
    },
  ],
  controlName: {
    type: String,
  },
  controlStatus: {
    type: String,
  },

  controlDescription: {
    type: String,
  },

  controlType: {
    type: String,
  },
  controlDepartment: {
    type: String,
  },
  controlDepartmentProcess: {
    type: String,
  },
  controlLegalReference: {
    type: String,
  },
  controlKeyProcess: {
    type: String,
  },

  controlGuidance: {
    type: String,
  },
  controlObjective: {
    type: String,
  },
  controlAddedDate: {
    type: String,
  },
  controlApprovedDate: {
    type: String,
  },
  controlLastChangeDate: {
    type: String,
  },
  controlOwnerName: {
    type: String,
  },
  controlOwnerId: {
    type: String,
  },
  controlApprovedBy: {
    type: String,
  },
  controlApprovedId: {
    type: String,
  },
  controlSubmitter: {
    type: String,
  },
  controlSubmitterId: {
    type: String,
  },
  controlKeyFlag: {
    type: Boolean,
  },
  controlOwnerFlag: {
    type: Boolean,
  },
  controlTesting: {
    type: Boolean,
  },
  schedule: {
    type: Boolean,
  },
  assignToTask: {
    type: Boolean,
  },
  controlRisk: {
    type: Number,
  },
  ctlTestingId: [String],
  controlWFId: {
    type: String,
  },
  created_by: {
    _id: String,
    name: String,
  },
  created_on: Date,
  updated_by: {
    _id: String,
    name: String,
  },
  updated_on: Date,
});

const controlWF = Schema({
  controlWFId: {
    type: String,
  },
  execution: {
    type: Number,
  },
  controlWorkFlowName: {
    type: String,
  },
  exAssignee: [String],
  exStart: {
    type: String,
  },
  exEnd: {
    type: String,
  },
  exDuration: {
    type: Number,
  },
  approval: {
    type: Number,
  },
  apAssignee: [String],
  apStart: {
    type: String,
  },
  apEnd: {
    type: String,
  },
  apDuration: {
    type: Number,
  },
  corporate: {
    type: String,
  },
  companyName: {
    type: String,
  },
  buName: {
    type: String,
  },
  addedOn: {
    type: String,
  },
  lastChangeOn: {
    type: String,
  },
  approvedBy: {
    type: String,
  },
  approvedId: {
    type: String,
  },
  businessProcess: {
    type: String,
  },
  controlIds: [String],
  beforefive: {
    type: String,
  },
  beforefiveAp: {
    type: String,
  },
  afterfive: {
    type: String,
  },
  afterfiveAp: {
    type: String,
  },
  afterfourteen: {
    type: String,
  },
  afterfourteenAp: {
    type: String,
  },
  approvedOn: {
    type: String,
  },
  frequency: {
    type: String,
  },
  created_by: {
    _id: String,
    name: String,
  },
  created_on: Date,
  updated_by: {
    _id: String,
    name: String,
  },
  updated_on: Date,
});

exports.control = mongoose.model("riskcontrols", controlInfo);

exports.controlwf = mongoose.model("controlwfs", controlWF);
