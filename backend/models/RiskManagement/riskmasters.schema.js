const mongoose = require("mongoose");
const { Schema } = mongoose;
const masterRiskStatusInfo = Schema(
  {
    riskId: {
      type: String,
    },

    status: {
      type: String,
    },
  },
  { collection: "masterriskstatus" }
);

const masterRiskSourceInfo = Schema(
  {
    sourceId: {
      type: String,
    },

    sourceStatus: {
      type: String,
    },
  },
  { collection: "masterrisksource" }
);

const riskDepartmentInfo = Schema(
  {
    depatId: {
      type: String,
    },

    departmentName: {
      type: String,
    },
  },
  { collection: "riskdepartment" }
);

const masterRiskProcessInfo = Schema(
  {
    processId: {
      type: String,
    },

    processName: {
      type: String,
    },
  },
  { collection: "masterriskprocess" }
);

const masterRiskCategoryInfo = Schema(
  {
    categoryId: {
      type: String,
    },

    categoryName: {
      type: String,
    },
  },
  { collection: "masterriskcategory" }
);

const masterRiskAreaInfo = Schema(
  {
    riskAreaId: {
      type: String,
    },

    riskArea: {
      type: String,
    },
  },
  { collection: "masterriskarea" }
);

const masterRiskApproachInfo = Schema(
  {
    riskappId: {
      type: String,
    },

    riskApproach: {
      type: String,
    },
  },
  { collection: "masterriskapproach" }
);

const masterRiskOwnerInfo = Schema(
  {
    riskOwnerId: {
      type: String,
    },

    riskOwnerName: {
      type: String,
    },
  },
  { collection: "masterriskowner" }
);

const masterControlTypeInfo = Schema(
  {
    controlId: {
      type: String,
    },

    controlName: {
      type: String,
    },
  },
  { collection: "mastercontroltype" }
);

const masterWfFrequencyInfo = Schema(
  {
    cwfId: {
      type: String,
    },

    status: {
      type: String,
    },
  },
  { collection: "masterWfFrequency" }
);

const masterControlDepartmentInfo = Schema(
  {
    controlId: {
      type: String,
    },

    controlDepartment: {
      type: String,
    },
  },
  { collection: "mastercontroldepartment" }
);

const masterControlProcessInfo = Schema(
  {
    controlId: {
      type: String,
    },

    controlProcess: {
      type: String,
    },
  },
  { collection: "mastercontrolprocess" }
);

const masterControlInfo = Schema(
  {
    controlId: {
      type: String,
    },

    controlName: {
      type: String,
    },
  },
  { collection: "mastercontrol" }
);

const masterControlStatusInfo = Schema(
  {
    controlId: {
      type: String,
    },

    controlStatus: {
      type: String,
    },
  },
  { collection: "mastercontrolstatus" }
);

const masterControlKeyProcessInfo = Schema(
  {
    controlKeyId: {
      type: String,
    },

    controlkeyprocess: {
      type: String,
    },
  },
  { collection: "mastercontrolkeyprocess" }
);

const masterFrequencyInfo = Schema(
  {
    frequencyId: {
      type: String,
    },

    frequency: {
      type: String,
    },
  },
  { collection: "masterfrequency" }
);

const mastercontrolguidanceInfo = Schema({
  stepId: {
    type: Number,
  },

  step: {
    type: String,
  },
});

exports.masterRiskStatus = mongoose.model(
  "masterriskstatus",
  masterRiskStatusInfo
);
exports.masterRiskSource = mongoose.model(
  "masterrisksource",
  masterRiskSourceInfo
);
exports.riskDepartment = mongoose.model("riskdepartment", riskDepartmentInfo);
exports.masterRiskProcess = mongoose.model(
  "masterriskprocess",
  masterRiskProcessInfo
);
exports.masterRiskCategory = mongoose.model(
  "masterriskcategory",
  masterRiskCategoryInfo
);
exports.masterRiskArea = mongoose.model("masterriskarea", masterRiskAreaInfo);
exports.masterRiskApproach = mongoose.model(
  "masterriskapproach",
  masterRiskApproachInfo
);
exports.masterRiskOwner = mongoose.model(
  "masterriskowner",
  masterRiskOwnerInfo
);
exports.masterControlType = mongoose.model(
  "mastercontroltype",
  masterControlTypeInfo
);
exports.masterControlDepartment = mongoose.model(
  "mastercontroldepartment",
  masterControlDepartmentInfo
);
exports.masterControlProcess = mongoose.model(
  "mastercontrolprocess",
  masterControlProcessInfo
);
exports.masterControl = mongoose.model("mastercontrol", masterControlInfo);
exports.masterControlStatus = mongoose.model(
  "mastercontrolstatus",
  masterControlStatusInfo
);
exports.masterControlKeyProcess = mongoose.model(
  "mastercontrolkeyprocess",
  masterControlKeyProcessInfo
);
exports.masterWfFrequency = mongoose.model(
  "masterWfFrequency",
  masterWfFrequencyInfo
);
exports.masterFrequency = mongoose.model(
  "masterFrequency",
  masterFrequencyInfo
);
exports.mastercontrolguidance = mongoose.model(
  "mastercontrolguidances",
  mastercontrolguidanceInfo
);
