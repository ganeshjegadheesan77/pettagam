const mongoose = require("mongoose");
const { Schema } = mongoose;

const riskInfo = Schema({
  riskId: {
    type: String,
  },

  controlId: [
    {
      type: String,
      ref: "riskControls",
    },
  ],

  riskName: {
    type: String,
  },

  riskStatus: {
    type: String,
    ref: "masterriskstatus",
  },

  riskDescription: {
    type: String,
  },

  riskSource: {
    type: String,
  },
  riskDepartment: {
    type: String,
  },
  riskProcess: {
    type: String,
  },
  riskCategory: {
    type: String,
  },
  riskArea: {
    type: String,
  },

  riskApproach: {
    type: String,
  },

  riskOwner: {
    type: String,
  },
  riskOwnerFlag: {
    type: Boolean,
  },
  riskPotentialoutcome: {
    type: String,
  },

  riskIdentifiedDate: {
    type: String,
  },

  riskSubmittedDate: {
    type: String,
  },

  riskLastChangeDate: {
    type: String,
  },
  riskLastApprovedDate: {
    type: String,
  },
  riskOwnerName: {
    type: String,
  },

  riskOwnerId: {
    type: String,
  },

  riskSubmitter: {
    type: String,
  },
  riskSubmitterId: {
    type: String,
  },
  riskApprovedBy: {
    type: String,
  },
  riskApproverId: {
    type: String,
  },
  controlRiskProgress: {
    type: String,
    ref: "riskcontrols",
  },
  controlType: {
    type: String,
    ref: "riskcontrols",
  },
  controlDepartment: {
    type: String,
    ref: "riskcontrols",
  },
  controlProcess: {
    type: String,
    ref: "riskcontrols",
  },
  control: {
    type: String,
    ref: "riskcontrols",
  },
  scheduleFlag: {
    type: Boolean,
  },
  addControlFlag: {
    type: Boolean,
  },
  intrinsicRisk: {
    type: Number,
  },
  riskAssessmentId: [
    {
      type: String,
      // ref: 'assess'
    },
  ],
  created_by: {
    _id: String,
    name: String,
  },
  created_on: Date,
  updated_by: {
    _id: String,
    name: String,
  },
  updated_on: Date,
});

const assessInfo = Schema(
  {
    isAuditable: {
      type: Boolean,
    },

    fieldAudit: {
      type: Boolean,
    },

    automatedControls: {
      type: Boolean,
    },
    timePeriod: {
      type: String,
    },
    riskDepartment: {
      type: String,
    },
    intrinsicRisk: [
      {
        riskType: String,
        likelyhood: Number,
        impact: Number,
        result: Number,
      },
    ],
    residualRisk: [
      {
        riskType: String,
        likelyhood: Number,
        impact: Number,
        result: Number,
      },
    ],

    riskAssementId: {
      type: String,
    },

    riskId: {
      type: Schema.Types.ObjectId,
      ref: "risks",
    },
    isCopied: { type: Boolean, default: false },
    copyCount: { type: Number, default: 0 },
  },
  { timestamps: true }
);

exports.risk = mongoose.model("risks", riskInfo);
exports.assess = mongoose.model("assess", assessInfo);
