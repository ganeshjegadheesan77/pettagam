var mongoose = require('mongoose');
const config = require('../../config/config');
var conn = mongoose.createConnection(config.auditDatabase.url);
var auditTrialSchema = new mongoose.Schema({
    request_id: String,
    module: String,
    module_id: String,
    data: Object,
    action: String,
    performed_by: String,
    performed_on: Date
}).pre('save', function (next) {
    var audit = this;
    // audit.request_id = "pre save id";
    console.log(audit.request_id);
    console.log("pre save");
    next();
});

var auditTrial = conn.model('audit_trial', auditTrialSchema)
module.exports =async function (request_id, module, module_id, data, action, performed_by) {
    // var ad = new auditTrial();
    // ad.request_id = request_id;
    // ad.module = module;
    // ad.module_id = module_id;
    // ad.data = data;
    // ad.action = action;
    // ad.performed_by = performed_by;
    // ad.performed_on = new Date();
    // ad.save();
};