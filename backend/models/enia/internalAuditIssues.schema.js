const mongoose = require('mongoose');

var internalAuditIssues = new mongoose.Schema({
    auditId:{
        type: String,
        required: false,
    },
    workStepIds:[{
        type: String,
        required: false,
    }],
    issueId: {
        type: String,
        required: false,
    },
    issueName:{
        type:String,
        required: false
    },
    issueNumber:{
        type:Number,
        required: false
    },
    preparer:{
        username: {
            type: String,
            required: false
        },
        profileUrl: {
            type: String,
            required: false
        },
        preparedOn: {
            type: Date,
            required: false
        },
        status: {
            type: String,
            required: false
        },
    },
    reviewer:{
        username: {
            type: String,
            required: false
        },
        profileUrl: {
            type: String,
            required: false
        },
        reviewedOn: {
            type: Date,
            required: false
        },
        status: {
            type: String,
            required: false
        },
    },
    issueStatus:{
        type:String,
        required: false
    },
    issuePriority:{
        type:String,
        required: false
    },
    shortDescription:{
        type:String,
        required: false
    },
    reviewComments:{
        type:String,
        required: false
    },
    longDescription:{
        type:String,
        required: false
    },
    recommendations:{
        type:String,
        required: false
    },
    managementResponse:{
        type:String,
        required: false
    },
    attachments: [
        {
            documentUrlToken: {
                type: String,
                required: false
            },
            documentName: {
                type: String,
                required: false
            },
            dateUploaded: {
                type: Date,
                required: false
            },
            documentNo: {
                type: String,
                required: false
            },
            uploadedBy:{
                type: String,
                required: false
            }
        }
    ],
    rejectionComments:{
        type:String,
        required: false
    },
    createdBy: {
        type: String,
        required: false
    },
    updatedBy: {
        type: String,
        required: false
    }
}, { collection: 'internalAuditIssues', timestamps: true, minimize: false, versionKey: false })

exports.internalAuditIssuesSchema = mongoose.model('internalAuditIssues', internalAuditIssues);