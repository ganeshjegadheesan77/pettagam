const mongoose = require("mongoose");
const { Schema } = mongoose;


const progressBarCriteria = Schema({
    stepName : { type: String, unique : true },
    criteria : [{
        type : String
    }]
},
{ collection: "progressBarCriteria", timestamps: true });

exports.progressBarCriteria = mongoose.model("progressBarCriteria", progressBarCriteria);