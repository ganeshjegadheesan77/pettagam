const mongoose = require('mongoose');

var workSteps = new mongoose.Schema({
    auditId: {
        type: String,
        required: false,
        ref: "auditMasters"
    },
    issueIds: [{
        type: String,
        required: false,
        ref:"internalAuditIssues"
    }],
    workStepId: {
        type: String,
        required: false
    },
    workStepName: {
        type: String,
        required: false
    },
    workStepNumber: {
        type: Number,
        required: false
    },
    uniqueTemplateId: {
        type: String,
        required: false
    },
    taskDescription: {
        type: String,
        required: false
    },
    workPerformed: {
        type: String,
        required: false
    },
    summary: {
        type: String,
        required: false
    },
    rejectionReason: {
        type: String,
        required: false
    },
    referenceDocuments: [
        {
            documentUrlToken: {
                type: String,
                required: false
            },
            documentName: {
                type: String,
                required: false
            },
            dateUploaded: {
                type: Date,
                required: false
            },
            documentNo: {
                type: String,
                required: false
            }
        }
    ],
    workingPapers: [
        {
            documentUrlToken: {
                type: String,
                required: false
            },
            documentName: {
                type: String,
                required: false
            },
            dateUploaded: {
                type: Date,
                required: false
            },
            documentNo: {
                type: String,
                required: false
            }
        }
    ],
    hasIssues: {
        type: Boolean,
        required: false
    },
    origin: {
        type: String,
        required: false
    },
    preparer: {
        username: {
            type: String,
            required: false
        },
        profileUrl: {
            type: String,
            required: false
        },
        preparedOn: {
            type: Date,
            required: false
        },
        status: {
            type: String,
            required: false
        },
    },
    reviewer: {
        username: {
            type: String,
            required: false
        },
        profileUrl: {
            type: String,
            required: false
        },
        reviewedOn: {
            type: Date,
            required: false
        },
        status: {
            type: String,
            required: false
        },
    },
    createdBy: {
        type: String,
        required: false
    },
    updatedBy: {
        type: String,
        required: false
    }
}, { collection: 'workSteps', timestamps: true, minimize: false, versionKey: false })

exports.workStepSchema = mongoose.model('workSteps', workSteps);