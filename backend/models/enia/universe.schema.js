const mongoose = require("mongoose");

const universeSchema = mongoose.Schema({
    id: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    parent: {
        type: String,
    },
    value: {
        type: Number,
    },
    type: {
        type: String,
        required: true,
    },
    childrenId: [
        {
            type: String,
            required: false
        },
    ],
    displayName: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model("universe", universeSchema);