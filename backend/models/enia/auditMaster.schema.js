const mongoose = require('mongoose');

var auditMaster = mongoose.Schema({
    auditId: {
        type: String,
        required: false,
    },
    auditNumber: {
        type: Number,
        required: false,
    },
    auditableEntity: {
        type: String,
        required: false,
    },
    rotation: {
        type: Number,
        required: false,
    },
    year: {
        type: Number,
        required: false,
    },
    isRemote: {
        type: Boolean,
        required: false,
    },
    startDate: {
        type: Date,
        required: false,
    },
    isActive: {
        type: Boolean,
        required: false
    },
    isDuplicated: {
        type: Boolean,
        required: false,
    },
    isCompleted: {
        type: Boolean,
        required: false,
    },
    issueIds: [{
        type: String,
        required: false,
        ref:"internalAuditIssues"
    }],
    category: {
        levelOne: { name: { type: String, required: false }, id: { type: String, required: false }, data: { type: String, required: false } },
        levelTwo: { name: { type: String, required: false }, id: { type: String, required: false }, data: { type: String, required: false } },
        levelThree: { name: { type: String, required: false }, id: { type: String, required: false }, data: { type: String, required: false } },
        levelFour: { name: { type: String, required: false }, id: { type: String, required: false }, data: { type: String, required: false } },
        levelFive: { name: { type: String, required: false }, id: { type: String, required: false }, data: { type: String, required: false } },
        businessProcess: {
            type: String,
            required: false
        }
    },
    createdBy: {
        type: String,
        required: false
    },
    teamAndTimeline: {
        isCompleted: {
            type: Boolean,
            required: false
        },
        openingMeetingDate: {
            type: Date,
            required: false
        },
        closingMeetingDate: {
            type: Date,
            required: false
        },
        periodInScope: [{
            type: Date,
            required: false
        }],
        updateMeetingDate2: {
            type: Date,
            required: false
        },
        finalMeetingDate: {
            type: Date,
            required: false
        },
        updateMeetingDate: {
            type: Date,
            required: false
        },
        auditLead: [
            {
                type: String,
                required: false,
                ref: "users"
            }
        ],
        auditors: [
            {
                type: String,
                required: false,
                ref: "users"
            }
        ],
        pbc: [
            {
                type: String,
                required: false,
                ref: "users"
            }
        ],
        auditees: [
            {
                type: String,
                required: false
            }
        ]
    },
    openingMeeting: {
        isCompleted: {
            type: Boolean,
            required: false
        },
        auditScope: {
            type: String,
            required: false
        },
        agenda: {
            type: String,
            required: false
        },
        quesAndAns: {
            type: String,
            required: false
        },
        sentOn: {
            type: Date,
            required: false
        },
        preparedBy: {
            type: String,
            required: false
        },
        sentBy: {
            type: String,
            required: false
        },
        attachments: [
            {
                documentNo: {
                    type: String,
                    required: false
                },
                documentName: {
                    type: String,
                    required: false
                },
                documentUrlToken: {
                    type: String,
                    required: false
                }
            }
        ]
    },
    planningMemorandum: {
        auditObjectives: {
            type: String,
            required: false
        },
        auditScope: {
            type: String,
            required: false
        },
        outOfScope: {
            type: String,
            required: false
        },
        sentOn: {
            type: Date,
            required: false
        },
        preparedBy: {
            type: String,
            required: false
        },
        sentBy: {
            type: String,
            required: false
        },
        attachments: [
            {
                documentNo: {
                    type: String,
                    required: false
                },
                documentName: {
                    type: String,
                    required: false
                },
                documentUrlToken: {
                    type: String,
                    required: false
                }
            }
        ],
        isCompleted: {
            type: Boolean,
            required: false
        }
    },
    auditAnnouncement: {
        text: {
            type: String,
            required: false
        }, //////// temporary naming convention ////////
        text2: {
            type: String,
            required: false
        }, //////// temporary naming convention ////////
        agenda: {
            type: String,
            required: false
        },
        closing: {
            type: String,
            required: false
        },
        sentOn: {
            type: Date,
            required: false
        },
        preparedBy: {
            type: String,
            required: false
        },
        sentBy: {
            type: String,
            required: false
        },
        attachments: [
            {
                documentNo: {
                    type: String,
                    required: false
                },
                documentName: {
                    type: String,
                    required: false
                },
                documentUrlToken: {
                    type: String,
                    required: false
                }
            }
        ],
        isCompleted: {
            type: Boolean,
            required: false
        }
    },
    auditPbc: {
        preparer: [{
            type: String,
            required: false
        }],
        reviewer: [{
            type: String,
            required: false
        }],
        auditScope: {
            type: String,
            required: false
        },
        documentRequest: {
            type: String,
            required: false
        },
        documentList: [
            {
                docRequested: {
                    type: String,
                    required: false
                },
                dueDate: {
                    type: Date,
                    required: false
                },
                remarks: {
                    type: String,
                    required: false
                },
                dmsInfo: {
                    documentNo: {
                        type: String,
                        required: false
                    },
                    documentName: {
                        type: String,
                        required: false
                    },
                    documentUrlToken: {
                        type: String,
                        required: false
                    }
                },
                dateUploaded: {
                    type: Date,
                    required: false
                },
                isApproved: { //false
                    type: Boolean,
                    required: false
                },
                isRejected: { //false
                    type: Boolean,
                    required: false
                },
                isLocked: {
                    type: Boolean,
                    required: false
                },
                uploadedBy: {
                    type: String,
                    required: false
                }
            }
        ],
        isCompleted: {
            type: Boolean,
            required: false
        },
        pbcAuditeeURLToken: {
            type: String,
            required: false,
        }
    },
    draftReport: {
        auditTeam: [{
            type: String,
            required: false
        }],
        attachments: [
            {
                documentNo: {
                    type: String,
                    required: false
                },
                documentName: {
                    type: String,
                    required: false
                },
                documentUrlToken: {
                    type: String,
                    required: false
                }
            }
        ],
        auditObjectives: {
            type: String,
            required: false
        },
        generalObservations: {
            type: String,
            required: false
        },
        managementFeedback: {
            type: String,
            required: false
        },
        correctiveActions: {
            type: String,
            required: false
        },
        rating: {
            type: String,
            required: false
        },
        comments: {
            type: String,
            required: false
        },
        isCompleted: {
            type: Boolean,
            required: false
        }
    },
    closingMeeting: {
        attachments: [
            {
                documentNo: {
                    type: String,
                    required: false
                },
                documentName: {
                    type: String,
                    required: false
                },
                documentUrlToken: {
                    type: String,
                    required: false
                }
            }
        ],
        agenda: {
            type: String,
            required: false
        },
        quesAndAns: {
            type: String,
            required: false
        },
        issueActionReview: {
            type: String,
            required: false
        },
        comments: {
            type: String,
            required: false
        },
        isCompleted: {
            type: Boolean,
            required: false
        }
    },
    workStepIds: [{
        type: String,
        required: false,
        ref: "workSteps"
    }]
}, { collection: 'auditMaster', timestamps: true, minimize: false, versionKey: false });

exports.auditMasterSchema = mongoose.model('auditMaster', auditMaster);