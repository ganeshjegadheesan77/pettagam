const mongoose = require('mongoose');

var workStepsTemplate = new mongoose.Schema({
    templateId: {
        type: String,
        required: false
    },
    templateName: {
        type: String,
        required: false
    },
    status: {
        type: Boolean,
        required: false
    },
    workSteps: [{
        workStepId: {
            type: String,
            required: false
        },
        workStepName: {
            type: String,
            required: false
        },
        workStepNumber: {
            type: Number,
            required: false
        },
        uniqueTemplateId: {
            type: String,
            required: false
        },
        taskDescription: {
            type: String,
            required: false
        }
    }],
    createdBy: {
        type: String,
        required: false
    },
    updatedBy: {
        type: String,
        required: false
    }
}, { collection: 'workStepsTemplate', timestamps: true, minimize: false, versionKey: false })

exports.workStepsTemplateSchema = mongoose.model('workStepsTemplate', workStepsTemplate);