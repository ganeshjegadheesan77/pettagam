const mongoose = require("mongoose");
const { Schema } = mongoose;

const documentsInfo = Schema(
  {
    documentNo: { type: String },
    metadata: { type: String },
    routerLink: { type: String },
    fileName: { type: String },
    moduleName: { type: String },
    subModuleName: { type: String },
    auditId: { type: String },
    auditYear: { type: String },
    userType: { type: String },
    auditName: { type: String },
    checkedinDate: { type: String },
    checkedInBy: { type: String },
    roomType: { type: String },
    urlToken: { type: String },
    checkoutStatus: { type: String },
    checkOutBy: { type: String },
    workPaperNumber: { type: String, default: "" },
  },
  { collection: "DmsDocumentRepository", timestamps: true }
);
documentsInfo.index({ fileName: "text" });
exports.documentsInfo = mongoose.model("DmsDocumentRepository", documentsInfo);
