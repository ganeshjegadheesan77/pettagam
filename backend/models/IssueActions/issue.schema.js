const mongoose = require("mongoose");
const { Schema } = mongoose;

var issuesInfo = Schema({
  issueName: {
    type: String,
  },
  controlId : [{
    type : String,
    ref : "riskcontrols"
  }],
  issueId: {
    type: String,
  },
  auditId: {
    type: String,
  },

  actionId: [
    {
      type: String,
      ref: "actions",
    },
  ],
  status: {
    type: String,
    default: "assigned",
  },
  issueDescription: {
    type: String,
  },

  action: {
    type: String,
  },
  reviewComments: {
    type: String,
  },
  recommendation: {
    type: String,
  },
  managementResponse: {
    type: String,
  },
  priority: {
    type: String,
  },
  identifiedBy: {
    type: String,
  },
  identifierId: {
    type: String,
  },
  dateIdentified: {
    type: String,
  },
  approvedBy: {
    type: String,
  },
  approverId: {
    type: String,
  },
  dateApproved: {
    type: String,
  },
  issueStatus: {
    type: String,
  },
  issuePriority: {
    type: String,
  },
  issueType: {
    type: String,
  },
  area: {
    type: String,
  },
  department: {
    type: String,
  },
  keyIsuue: {
    type: String,
  },
});

exports.issue = mongoose.model("issues", issuesInfo);
