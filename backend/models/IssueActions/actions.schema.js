const mongoose = require('mongoose');
const { Schema } = mongoose;


var actionInfo = Schema({

    actionName: {
        type: String
    },
    actionId: {
        type: String
    },
    actionDescription: {
        type: String
    },

    issueId: [{
        type: String,
        ref: 'issues'
    }],

    performancePeriod: {
        type: String
    },

    signOffPeriod: {
        type: String
    },
    correctiveAction: {
        type: String
    },
    guidance: {
        type: String
    },
    comments: {
        type: String
    },
    // ac_attachments:{
    //     type:String
    // },
    executedBy: {
        type: String
    },
    executerId: {
        type: String
    },
    dateExecuted: {
        type: String
    },
    approvedBy: {
        type: String
    },
    approvedId: {
        type: String
    },
    dateApproved: {
        type: String
    },
    area: {
        type: String
    },
    stepsCompleted: {
        type: String
    },
    onTrack: {
        type: Boolean
    },
    rating:{
        type: String
    },
    actionStatus: {
        type: String
    },
    delay: {
        type: String
    },
    ac_attachments: [
        {
          type: String,
        },
      ],
    actionWFId:{
        type: String 
    }
    })
    var actionWF = Schema({
        actionWFId:{
            type: String 
        },
        execution:{
            type:Number,
        },
        actionWorkFlowName:{
            type:String,
        },
        exAssignee :[String],
        exStart : {
            type:String,
        },
        exEnd : {
            type:String,
        },
        exDuration : {
            type:Number,
        },
        approval : {
            type:Number,
        },
        apAssignee : [String],
        apStart:  {
            type:String,
        },
        apEnd :  {
            type:String,
        },
        apDuration : {
            type:Number,
        },
        corporate :  {
            type:String,
        },
        companyName : {
            type:String,
        },
        buName :  {
            type:String,
        },
        addedOn : {
            type:String,
        },
        lastChangeOn :  {
            type:String,
        },
        approvedBy : {
            type:String,
        },
        approvedId :  {
            type:String,
        },
        businessProcess:{
            type:String,
        },
        actionIds:[String],
        beforefive:{
            type:String,
        },
        beforefiveAp:{
            type:String,
        },
        afterfive:{
            type:String,
        },
        afterfiveAp:{
            type:String,
        },
        afterfourteen:{
            type:String,
        },
        afterfourteenAp:{
            type:String,
        },
        approvedOn:{
            type:String,
        }
        })
    
    exports.action = mongoose.model('actions', actionInfo);
    
    exports.actionwf = mongoose.model('actionwfs', actionWF);