const mongoose = require("mongoose");
const { Schema } = mongoose;
const masterActionStatusInfo = Schema(
  {
    actionId: {
      type: String,
    },

    status: {
      type: String,
    },
  },
  { collection: "masteractionstatus" }
);

const masterActionRatingInfo = Schema(
  {
    actionId: {
      type: String,
    },

    status: {
      type: String,
    },
  },
  { collection: "masteractionrating" }
);

const masterIssueStatusInfo = Schema(
  {
    issueId: {
      type: String,
    },

    status: {
      type: String,
    },
  },
  { collection: "masterissuestatus" }
);

const masterIssueTypeInfo = Schema(
  {
    issueId: {
      type: String,
    },

    issueType: {
      type: String,
    },
  },
  { collection: "masterissuetype" }
);

const masterissuepriorityInfo = Schema(
  {
    issueId: {
      type: String,
    },

    issuePriority: {
      type: String,
    },
  },
  { collection: "masterissuepriority" }
);

const masterareaInfo = Schema(
  {
    actionId: {
      type: String,
    },

    area: {
      type: String,
    },
  },
  { collection: "masterarea" }
);

exports.masterActionStatus = mongoose.model(
  "masteractionstatus",
  masterActionStatusInfo
);
exports.masterActionRating = mongoose.model(
  "masteractionrating",
  masterActionRatingInfo
);
exports.masterIssueStatus = mongoose.model(
  "masterissuestatus",
  masterIssueStatusInfo
);
exports.masterIssueType = mongoose.model(
  "masterissuetype",
  masterIssueTypeInfo
);
exports.masterissuepriority = mongoose.model(
  "masterissuepriority",
  masterissuepriorityInfo
);
exports.masterarea = mongoose.model("masterarea", masterareaInfo);
